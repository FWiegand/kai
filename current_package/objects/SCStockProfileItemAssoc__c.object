<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
The association between stock item and stock profile item is n:n - related. A stock profile item defines the stock items of all stocks, which are based on this stock profile. On the other hand, a stock item can be based on several stock profiles items, because a stock can be related to several stock profiles and because different stock pro-files can contain the same parts. This n:n relation is implemented in this table.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>external unique identifier used in interfaces and data migration</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>StockItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the stock item. Used to update the stock items when the quantity (min, max, replenishment)  of a stock profile changes.</description>
        <externalId>false</externalId>
        <label>Stock Item</label>
        <referenceTo>SCStockItem__c</referenceTo>
        <relationshipName>SCStockProfileItemAssoc</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>StockProfileItem__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the stock profile item. Used to update the stock items when the quantity (min, max, replenishment)  of a stock profile changes.</description>
        <externalId>false</externalId>
        <label>Stock Profile Item</label>
        <referenceTo>SCStockProfileItem__c</referenceTo>
        <relationshipName>SCStockProfileItemAssoc</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <gender>Feminine</gender>
    <label>Lagerortprofilpositionszuordnung</label>
    <nameField>
        <displayFormat>STPIA-{0000000000}</displayFormat>
        <label>Stock Profile Item Assoc</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Lagerortprofilpositionszuordnungen</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>StockProfileItem__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StockItem__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>StockProfileItem__c</lookupFilterFields>
        <lookupFilterFields>StockItem__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>StockProfileItem__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StockItem__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>StockProfileItem__c</searchFilterFields>
        <searchFilterFields>StockItem__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Non_empty_StockItem</fullName>
        <active>true</active>
        <errorConditionFormula>LEN(  StockItem__r.Name  ) == 0</errorConditionFormula>
        <errorMessage>Please select a valid stock item.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Non_empty_StockProfileItem</fullName>
        <active>true</active>
        <errorConditionFormula>LEN(   StockProfileItem__r.Name   ) == 0</errorConditionFormula>
        <errorMessage>Please select a valid stock profile item.</errorMessage>
    </validationRules>
</CustomObject>
