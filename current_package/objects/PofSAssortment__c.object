<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>ArticleEANCode__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;&quot;</formula>
        <label>Article EAN Code</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleGroupRelation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Article Group</label>
        <referenceTo>ArticleGroup__c</referenceTo>
        <relationshipLabel>Assortment</relationshipLabel>
        <relationshipName>Assortment</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ArticleGroup__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
TEXT( Produkt__r.Group__c ),
TEXT(ArticleGroupRelation__r.Group__c )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Group</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleKindOfPackage__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
TEXT( Produkt__r.Kind_of_Package__c ),
TEXT( ArticleGroupRelation__r.KindOfPackage__c )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Kind of Package</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleMeasuringUnit__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
TEXT( Produkt__r.measuring_unit__c ),
TEXT( ArticleGroupRelation__r.MeasuringUnit__c )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article measuring unit</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleName__c</fullName>
        <externalId>false</externalId>
        <formula>&quot;&quot;</formula>
        <label>Article Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticlePackagingSize__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
TEXT( Produkt__r.Packaging_size__c ),
TEXT( ArticleGroupRelation__r.Packaging_size__c)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Packaging size</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticlePicture__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
Produkt__r.Picture__c,
ArticleGroupRelation__r.Picture__c 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Picture</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleSubGroup__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )),
    IF(
        TEXT(Produkt__r.Sub_group__c )=&apos;Standard&apos;,
        &apos;&apos;,
        TEXT(Produkt__r.Sub_group__c )
    ),
    IF(
        TEXT(ArticleGroupRelation__r.SubGroup__c)=&apos;Standard&apos;,
        &apos;&apos;,
        TEXT(ArticleGroupRelation__r.SubGroup__c)
    )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Sub group</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HasReplacementArticle__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Has replacement Article</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ManualSurvey__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>If the article is marked as &apos;manual survey&apos; this article will be asked for in shop surveys.</inlineHelpText>
        <label>Manual Survey</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PictureOfSuccess__c</fullName>
        <externalId>false</externalId>
        <label>Picture of Success</label>
        <referenceTo>PictureOfSuccess__c</referenceTo>
        <relationshipLabel>Assortment Articles</relationshipLabel>
        <relationshipName>Assortment</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Produkt__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Produkt</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Assortments</relationshipLabel>
        <relationshipName>Assortments</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>RowNumber__c</fullName>
        <externalId>false</externalId>
        <label>Row Number</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>preview__c</fullName>
        <externalId>false</externalId>
        <formula>IF(NOT( ISBLANK( Produkt__c )), 
    Produkt__r.Preview__c,
    ArticleGroupRelation__r.Preview__c 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>preview</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Assortment</label>
    <nameField>
        <displayFormat>PSA{000000}</displayFormat>
        <label>Assortment Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Assortment</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>ArticleGroup__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ArticleEANCode__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>HasReplacementArticle__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ArticleGroupRelation__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ArticleName__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
