<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <content>SCInventoryPreventNew</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Service organisations have to count the spare parts in the stocks (warehouses/van stocks). When the stock taking beginns an inventory record is created for the corre-sponding stock. It is possible to make a full inventory (counting all parts on a specific day) or a floating inventory (counting only specific parts).</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Description__c</fullName>
        <description>Detailed Description</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>ERPInventoryCountNumber__c</fullName>
        <description>SAP Inventory Count Number.</description>
        <externalId>false</externalId>
        <inlineHelpText>SAP Inventory Count Number.</inlineHelpText>
        <label>ERPInventoryCountNumber</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ERPResultDate__c</fullName>
        <description>Date of the transmition to and through between ClockPort and SAP.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date of the transmition to and through between ClockPort and SAP.</inlineHelpText>
        <label>ERPResultDate</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ERPStatusArchiveDocumentInsert__c</fullName>
        <description>The status of handshaking between ClockPort and SAP.</description>
        <externalId>false</externalId>
        <inlineHelpText>The status of handshaking between ClockPort and SAP.</inlineHelpText>
        <label>ERPStatusArchiveDocumentInsert</label>
        <picklist>
            <picklistValues>
                <fullName>none</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>pending</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ok</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>error</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ERPStatus__c</fullName>
        <description>Status of handshaking between ClockPort and SAP.</description>
        <externalId>false</externalId>
        <inlineHelpText>Status of handshaking between ClockPort and SAP.</inlineHelpText>
        <label>ERPStatus</label>
        <picklist>
            <picklistValues>
                <fullName>none</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>pending</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>error</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ok</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>FiscalYear__c</fullName>
        <defaultValue>TEXT(YEAR(TODAY()))</defaultValue>
        <description>The year used to book the stock counting (defaulted with the current year)</description>
        <externalId>false</externalId>
        <label>Fiscal Year</label>
        <length>4</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Full__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This flag is used to control the dialog functions.
true: full inventory (counting all parts on a specific day)
false: floating inventory (counting only specific parts).</description>
        <externalId>false</externalId>
        <label>Full</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>InventoryDate__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <description>Date when the inventory is done.</description>
        <externalId>false</externalId>
        <label>Inventory Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ItemCount__c</fullName>
        <description>number of total items in the inventory</description>
        <externalId>false</externalId>
        <inlineHelpText>number of total  inventory items</inlineHelpText>
        <label>Item Count</label>
        <summaryForeignKey>SCInventoryItem__c.Inventory__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>PlannedCountDate__c</fullName>
        <description>The date when the counting has to be completed</description>
        <externalId>false</externalId>
        <label>Planned Count Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Plant__c</fullName>
        <externalId>false</externalId>
        <formula>Stock__r.Plant__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Plant</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>The current status of the inventory process. Set automatically when creating, closing or cancelling an inventory / stocktaking process.
DOM_INVENTORY_STATUS</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>created</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>closed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>cancelled</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>planned</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Stock__c</fullName>
        <description>Reference to the stock.</description>
        <externalId>false</externalId>
        <label>Stock</label>
        <referenceTo>SCStock__c</referenceTo>
        <relationshipName>StockInventory</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <gender>Feminine</gender>
    <label>Inventur</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>X0349_VG_Reinland_abgeschlossen</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0349</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>closed</value>
        </filters>
        <label>0349 VG-Reinland (abgeschlossen)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0349_VG_Reinland_alle</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0349</value>
        </filters>
        <label>0349 VG-Reinland (alle)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0349_VG_Reinland_gel_scht</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0349</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>cancelled</value>
        </filters>
        <label>0349 VG-Reinland (gelöscht)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0349_VG_Reinland_offen</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0349</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>created,planned</value>
        </filters>
        <label>0349 VG-Reinland (offen)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0353_VG_BAW_alle</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0353</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>created,closed,cancelled,planned</value>
        </filters>
        <label>0353 VG-BAWÜ (alle)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0360_VG_NordWest_alle</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0360</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>created,closed,cancelled,planned</value>
        </filters>
        <label>0360 VG-NordWest (alle)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0360_VG_NordWest_gel_scht</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0360</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>cancelled</value>
        </filters>
        <label>0360 VG-NordWest (gelöscht)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0360_VG_NordWest_geschlossen</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0360</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>closed</value>
        </filters>
        <label>0360 VG-NordWest (abgeschlossen)</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>X0360_VG_NordWest_offen</fullName>
        <columns>NAME</columns>
        <columns>Plant__c</columns>
        <columns>Stock__c</columns>
        <columns>InventoryDate__c</columns>
        <columns>Status__c</columns>
        <columns>ItemCount__c</columns>
        <columns>Full__c</columns>
        <columns>Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Plant__c</field>
            <operation>equals</operation>
            <value>0360</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>created,planned</value>
        </filters>
        <label>0360 VG-NordWest (offen)</label>
        <language>de</language>
    </listViews>
    <nameField>
        <displayFormat>ITY-{0000000000}</displayFormat>
        <label>Inventory</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Inventuren</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Plant__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stock__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>InventoryDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Full__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PlannedCountDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>FiscalYear__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <excludedStandardButtons>New</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Stock__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Full__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>InventoryDate__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Stock__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Full__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>InventoryDate__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>OBJECT_ID</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Stock__c</searchFilterFields>
        <searchFilterFields>Full__c</searchFilterFields>
        <searchFilterFields>InventoryDate__c</searchFilterFields>
        <searchResultsAdditionalFields>Plant__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Stock__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>InventoryDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Full__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Non_empty_Stock</fullName>
        <active>true</active>
        <errorConditionFormula>LEN(  Stock__r.Name  ) == 0</errorConditionFormula>
        <errorMessage>Please select a valid stock.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>SCbtnArchiveInventoryDocuments</fullName>
        <availability>online</availability>
        <description>Submits the inventory Attachments to CCE SAP.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Archive Documents</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/10.0/apex.js&quot;)}

var NamespacePrefix = &apos;{!$Setup.SCApplicationSettings__c.NAMESPACE_PREFIX__c}&apos;;

if((NamespacePrefix != null) &amp;&amp; (NamespacePrefix.length&gt;2))
{
NamespacePrefix = NamespacePrefix.replace(/__/i,&apos;.&apos;);
}
else
{
NamespacePrefix = &apos;&apos;;
}

var msg = &quot;&quot; + sforce.apex.execute(NamespacePrefix + &quot;CCWCArchiveDocumentInsert&quot;,&quot;wsInvCallout&quot;, {invID:&quot;{!SCInventory__c.Id}&quot;});

alert(msg);</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnDownloadAsExcel</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Download as Excel</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.location = &quot;/apex/SCInventoryToExcel?invId={!SCInventory__c.Id}&quot;; 
//window.open(&quot;/apex/SCInventoryToExcel?invId={!SCInventory__c.Id}&quot;);</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnDownloadInventoryAsPDF</fullName>
        <availability>online</availability>
        <description>Exports the inventory as PDF file.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Download as PDF</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.location = &quot;/apex/SCInventoryToPDF?invId={!SCInventory__c.Id}&quot;; 
//window.open(&quot;/apex/SCInventoryToPDF?invId={!SCInventory__c.Id}&quot;);</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnInventoryCancel</fullName>
        <availability>online</availability>
        <description>Cancells the selected inventory.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Cancel</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>// First checking whether the inventory can be cancelled 
if(&quot;{!SCInventory__c.Status__c}&quot; != &quot;cancelled&quot; &amp;&amp; &quot;{!SCInventory__c.Status__c}&quot; != &quot;closed&quot;) 
{ 
window.location = &apos;/apex/SCInventoryCancel?oid={!SCInventory__c.Id}&amp;cancel=true&apos;; 
} 
else 
{ 
alert(&apos;{!$Label.SC_msg_InventoryCancelError1}&apos;);
}</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnInventoryClose</fullName>
        <availability>online</availability>
        <description>Close the selected inventory.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Close</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>// First checking whether the inventory can be closed 
if(confirm(&apos;{!$Label.SC_msg_CloseInventory}&apos;))
{
  if(&quot;{!SCInventory__c.Status__c}&quot; != &quot;closed&quot;) 
  { 
    window.location = &apos;/apex/SCInventoryClose?oid={!SCInventory__c.Id}&amp;close=true&apos;; 
  } 
  else 
  { 
    alert(&apos;{!$Label.SC_msg_InventoryClosedError1}&apos;);
  }
}</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnInventoryUpload</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Upload</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>// First checking whether the inventory can be uploaded
if(&quot;{!SCInventory__c.Status__c}&quot; != &quot;closed&quot;){
    window.location = &quot;/apex/SCInventoryUpload?oid={!SCInventory__c.Id}&quot;;
}
else
{
    alert(&apos;{!$Label.SC_msg_InventoryUploadError1}&apos;);
}</url>
    </webLinks>
</CustomObject>
