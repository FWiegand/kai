<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Used to assign required qualification to an article (product). If the article is assigned to the order item of an order the qualification is copied into the order qualification. The scheduling engine uses this information to select the resources that have this qualification.
Use case: specific security certificates that are required to access the location (e.g. air ports, military bases, ...) for maintenance contracts.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Article__c</fullName>
        <description>Reference to the article (product).</description>
        <externalId>false</externalId>
        <label>Article</label>
        <referenceTo>SCArticle__c</referenceTo>
        <relationshipName>Qualification_for_Article</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Optional short description of the qualification.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>250</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>External identifier. Used by interfaces and migration.</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Level__c</fullName>
        <description>The qualification level is used in the scheduling engine as a filter.
5001 Basic (30%) 
5002 Advanced (60%)
5003 Expert (100%)
DOM_QUALIFICATION_LEVEL</description>
        <externalId>false</externalId>
        <label>Level</label>
        <picklist>
            <picklistValues>
                <fullName>5001</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>5002</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5003</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Qualification__c</fullName>
        <description>Reference to the qualification.</description>
        <externalId>false</externalId>
        <label>Qualification</label>
        <referenceTo>SCQualification__c</referenceTo>
        <relationshipName>Qualification_for_Article</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <gender>Masculine</gender>
    <label>Qualifikation / Artikel</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>QP-{0000000000}</displayFormat>
        <label>No</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Qualifikationen / Artikel</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
