<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Maintenance template used for creating maintenance plans for installed bases.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Description__c</fullName>
        <description>Description of the maintenance plan</description>
        <externalId>false</externalId>
        <inlineHelpText>Description of the maintenance plan</inlineHelpText>
        <label>Description</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Duration__c</fullName>
        <defaultValue>45</defaultValue>
        <description>Duration in minutes. Used for setting in the order and order item.</description>
        <externalId>false</externalId>
        <inlineHelpText>Duration in minutes. Used for setting in the order and order item.</inlineHelpText>
        <label>Duration</label>
        <precision>3</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>external unique key used in interfaces and for data migration</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Interval__c</fullName>
        <description>Interval in days between the last and the next maintenance</description>
        <externalId>false</externalId>
        <inlineHelpText>Interval in days between the last and the next maintenance</inlineHelpText>
        <label>Interval (days)</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenanceActionDesc__c</fullName>
        <description>Specific maintenance action description in textual form</description>
        <externalId>false</externalId>
        <inlineHelpText>Specific maintenance action description in textual form</inlineHelpText>
        <label>Maintenance Action Desc</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>MaintenanceAfterDue__c</fullName>
        <defaultValue>14</defaultValue>
        <description>Defines the default time window after the calculated next maintenance date. The customer has to be visited in this time window. Evaluated in the operative planning and order creation process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Defines the default time window after the calculated next maintenance date. The customer has to be visited in this time window. Evaluated in the operative planning and order creation process.</inlineHelpText>
        <label>Time window after (days)</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenanceBeforeDue__c</fullName>
        <defaultValue>14</defaultValue>
        <description>Defines the default time window before the calculated next maintenance date. The customer has to be visited in this time window. Evaluated in the operative planning and order creation process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Defines the default time window before the calculated next maintenance date. The customer has to be visited in this time window. Evaluated in the operative planning and order creation process.</inlineHelpText>
        <label>Time window before (days)</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenanceType__c</fullName>
        <description>The type of the maintenance.</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of the maintenance.</inlineHelpText>
        <label>Maintenance Type</label>
        <picklist>
            <picklistValues>
                <fullName>SANITATION</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>WARTUNG</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>QUALITÄTSINSPEKTION</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>OrderCreationLeadtime__c</fullName>
        <defaultValue>30</defaultValue>
        <description>Default lead time used to create orders from contracts. The order will be created n days before the calculated next maintenance date.</description>
        <externalId>false</externalId>
        <inlineHelpText>Default lead time used to create orders from contracts. The order will be created n days before the calculated next maintenance date.</inlineHelpText>
        <label>Order Creation Leadtime (days)</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderPrio__c</fullName>
        <description>The priority of an order created from the maintenance plan.</description>
        <externalId>false</externalId>
        <inlineHelpText>The priority of an order created from the maintenance plan.</inlineHelpText>
        <label>Order Prio</label>
        <picklist>
            <picklistValues>
                <fullName>2101</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>OrderType__c</fullName>
        <description>The type of an order that is to be created from the maintenance plan</description>
        <externalId>false</externalId>
        <inlineHelpText>The type of an order that is to be created from the maintenance plan</inlineHelpText>
        <label>Order Type</label>
        <picklist>
            <picklistValues>
                <fullName>5707</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PriceListDefault__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Price list of the 5th maintenance or more in the calendar year</description>
        <externalId>false</externalId>
        <inlineHelpText>Price list of the 5th maintenance or more in the calendar year</inlineHelpText>
        <label>Price list default</label>
        <referenceTo>SCPriceList__c</referenceTo>
        <relationshipLabel>Maintenance Plans (Preisliste default)</relationshipLabel>
        <relationshipName>Maintenance_Plans</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pricelist1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Price list of the first maintenance in the calendar year</description>
        <externalId>false</externalId>
        <inlineHelpText>Price list of the first maintenance in the calendar year - can be used to define the first maintenance without service charge</inlineHelpText>
        <label>Pricelist 1</label>
        <referenceTo>SCPriceList__c</referenceTo>
        <relationshipLabel>Maintenance1</relationshipLabel>
        <relationshipName>Maintenance1</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pricelist2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Price list of the second maintenance in the calendar year</description>
        <externalId>false</externalId>
        <label>Pricelist 2</label>
        <referenceTo>SCPriceList__c</referenceTo>
        <relationshipLabel>Maintenance2</relationshipLabel>
        <relationshipName>Maintenance2</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pricelist3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Price list of the third maintenance in the calendar year</description>
        <externalId>false</externalId>
        <label>Pricelist 3</label>
        <referenceTo>SCPriceList__c</referenceTo>
        <relationshipLabel>Maintenance3</relationshipLabel>
        <relationshipName>Maintenance3</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pricelist4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Price list of the 4th maintenance in the calendar year</description>
        <externalId>false</externalId>
        <label>Pricelist 4</label>
        <referenceTo>SCPriceList__c</referenceTo>
        <relationshipLabel>Maintenance4</relationshipLabel>
        <relationshipName>Maintenance4</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <gender>Masculine</gender>
    <label>Wartungsplan</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>ID2__c</columns>
        <columns>Interval__c</columns>
        <columns>Duration__c</columns>
        <columns>Description__c</columns>
        <columns>OrderPrio__c</columns>
        <columns>OrderType__c</columns>
        <columns>MaintenanceType__c</columns>
        <columns>OrderCreationLeadtime__c</columns>
        <columns>MaintenanceAfterDue__c</columns>
        <columns>MaintenanceBeforeDue__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>de</language>
    </listViews>
    <nameField>
        <label>Maintenance Plan Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Wartungspläne</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Interval__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OrderPrio__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OrderType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>MaintenanceType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OrderCreationLeadtime__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>MaintenanceAfterDue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>MaintenanceBeforeDue__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>OrderType__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>PriceListDefault__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Pricelist1__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Pricelist2__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Pricelist3__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Pricelist4__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>MaintenanceType__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>OrderType__c</lookupFilterFields>
        <lookupFilterFields>OrderPrio__c</lookupFilterFields>
        <lookupFilterFields>MaintenanceType__c</lookupFilterFields>
        <lookupFilterFields>CURRENCY_ISO_CODE</lookupFilterFields>
        <lookupFilterFields>Duration__c</lookupFilterFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>OrderType__c</searchFilterFields>
        <searchFilterFields>OrderPrio__c</searchFilterFields>
        <searchFilterFields>MaintenanceType__c</searchFilterFields>
        <searchFilterFields>CURRENCY_ISO_CODE</searchFilterFields>
        <searchResultsAdditionalFields>OrderType__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>OrderPrio__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>PriceListDefault__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Pricelist1__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Pricelist2__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Pricelist3__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Pricelist4__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>MaintenanceType__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
