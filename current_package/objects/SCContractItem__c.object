<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
The contract item is used to assign the appliances of the installed base of a customer to the contract. For maintenance contracts the item also contains information about the maintenance interval, the costs, last / next maintenance. Contract items are used for creating maintenance orders depending on the maintenance interval. A contract can have multiple contract items. The items can have a reference to the same installed base location.   
Frame contracts do not have contract items. But they can have contracts assigned (bulk contract).</description>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Contract__c</fullName>
        <description>Reference to the contract.</description>
        <externalId>false</externalId>
        <label>Contract</label>
        <referenceTo>SCContract__c</referenceTo>
        <relationshipName>Service_Contract_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>external identifier. Used in Interfaces</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>InstalledBase__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the appliance.</description>
        <externalId>false</externalId>
        <label>Installed Base</label>
        <referenceTo>SCInstalledBase__c</referenceTo>
        <relationshipName>ServiceContractItems</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>MaintenanceDiscount__c</fullName>
        <externalId>false</externalId>
        <formula>(MaintenancePrice__c - MaintenancePriceCalc__c) / MaintenancePriceCalc__c * 100</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Discount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>MaintenanceDuration__c</fullName>
        <description>Planned maintenance duration for this item in minutes. This value is used to calculate the total time required for the maintenance. Evaluated in the order creation process. If this field is not specified the default value on contract level will be used.</description>
        <externalId>false</externalId>
        <label>Duration (minutes)</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenanceInterval__c</fullName>
        <description>Planned maintenance interval in months. Evaluated to calculate the next maintenance date. If this field is not specified the default value on contract level will be used.</description>
        <externalId>false</externalId>
        <label>Interval (months)</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenanceLastDate__c</fullName>
        <description>Last maintenance (redundant). Automatically set when the maintenance order has been completed. Used for selecting contracts and information purposes. 
see SCContractVisit__c and SCOrder__c for details.</description>
        <externalId>false</externalId>
        <label>Last maintenance</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>MaintenanceNextDate__c</fullName>
        <description>Next planned maintenance (redundant). Automatically set when the maintenance order is created or scheduled. This field is evaluated by the capacity planning.
see SCContractVisit__c and SCOrder__c for details.</description>
        <externalId>false</externalId>
        <label>Next maintenance</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>MaintenancePriceCalc__c</fullName>
        <description>The original price from the contract calculation tool. Used as default for the price field.</description>
        <externalId>false</externalId>
        <label>Price (calculated)</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaintenancePrice__c</fullName>
        <description>The actual maintenance price that will be used for invoicing per executed order. This price is set to the same value like PriceCalc__c if you use the contract calculation tool. It can manually be modified.</description>
        <externalId>false</externalId>
        <label>Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PriceManual__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Checks whether the price (MaintenancePrice) was entered manually by the user</description>
        <externalId>false</externalId>
        <inlineHelpText>Checks whether the price (MaintenancePrice) was entered manually by the user</inlineHelpText>
        <label>Manual Price</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PriceModule__c</fullName>
        <description>The module price for this contract item</description>
        <externalId>false</externalId>
        <label>Price (Module)</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ValidFrom__c</fullName>
        <description>start date - if null: no date based restriction.</description>
        <externalId>false</externalId>
        <label>Valid from</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ValidTo__c</fullName>
        <description>end date - if null: no date based restriction</description>
        <externalId>false</externalId>
        <label>Valid to</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>util_Valid__c</fullName>
        <description>status icon: green if the TODAY between ValidFrom and ValidTo or if no date based restrictions are relevant.</description>
        <externalId>false</externalId>
        <formula>IMAGE(IF((TODAY() &gt;  ValidFrom__c  &amp;&amp; TODAY() &lt;  ValidTo__c ), &quot;/img/msg_icons/confirm16.png&quot;, &quot;/img/msg_icons/warning16.png&quot;), IF((TODAY() &gt;  ValidFrom__c  &amp;&amp; TODAY() &lt;  ValidTo__c ), &quot;Valid&quot;, &quot;Not Valid&quot;), 16, 16)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Valid</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_brand__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.Brand__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Brand</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_department__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.Department__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>IB Department</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_idInt__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.IdInt__c + &apos; / &apos; +  InstalledBase__r.IdExt__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Id Int. / Ext.</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_product_model__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.ProductModel__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Model</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_room__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.Room__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>IB Room</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>util_ib_serialno__c</fullName>
        <externalId>false</externalId>
        <formula>InstalledBase__r.SerialNo__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Serial No.</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <gender>Feminine</gender>
    <label>Vertragsposition</label>
    <nameField>
        <displayFormat>CONI-{000000000}</displayFormat>
        <label>Contract Item Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Vertragspositionen</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>New_Contract_Item</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>New Contract Item</masterLabel>
        <openType>sidebar</openType>
        <page>SCAddContractItem</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>SCContractItemsDelete</fullName>
        <availability>online</availability>
        <description>Delete selected contract items.</description>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Delete</masterLabel>
        <openType>sidebar</openType>
        <page>SCContractItemDelete</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
