<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Used to create outbound messages that are processed by an external message handler. The table is filled by workflows or programmatically.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>AppointmentEndOld__c</fullName>
        <description>Appointment end date/time before the last transaction</description>
        <externalId>false</externalId>
        <label>AppointmentEndOld</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>AppointmentEnd__c</fullName>
        <description>Current appointment end date/time</description>
        <externalId>false</externalId>
        <label>AppointmentEnd</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>AppointmentOld__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the old appointment before the last transaction (e.g. automatically set when an appointment is transferred/moved from one engineer to another)</description>
        <externalId>false</externalId>
        <inlineHelpText>Reference to the old appointment (automatically set when an appointment is transferred/moved from one engineer to another)</inlineHelpText>
        <label>AppointmentOld</label>
        <referenceTo>SCAppointment__c</referenceTo>
        <relationshipLabel>Messages (AppointmentOld)</relationshipLabel>
        <relationshipName>AppointmentOld</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AppointmentStartOld__c</fullName>
        <description>Appointment start date/time before the last transaction</description>
        <externalId>false</externalId>
        <label>AppointmentStartOld</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>AppointmentStart__c</fullName>
        <description>Current appointment start date/time</description>
        <externalId>false</externalId>
        <label>AppointmentStart</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Appointment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the appointment</description>
        <externalId>false</externalId>
        <label>Referenced Appointment</label>
        <referenceTo>SCAppointment__c</referenceTo>
        <relationshipName>Messages</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AssignmentStatusOld__c</fullName>
        <description>The previous status before the update / cancel or delete operation</description>
        <externalId>false</externalId>
        <inlineHelpText>5502	Planned
5503	Transferred
5505	Completed offline
5506	Completed
5507	Cancelled
5509	Planned for precheck
5510	Released for processing</inlineHelpText>
        <label>AssignmentStatusOld</label>
        <picklist>
            <picklistValues>
                <fullName>5502</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5503</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5505</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5506</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5507</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5509</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5510</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AssignmentStatus__c</fullName>
        <description>The assignment status of the current assignment</description>
        <externalId>false</externalId>
        <inlineHelpText>5502	Planned
5503	Transferred
5505	Completed offline
5506	Completed
5507	Cancelled
5509	Planned for precheck
5510	Released for processing</inlineHelpText>
        <label>AssignmentStatus</label>
        <picklist>
            <picklistValues>
                <fullName>5502</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5503</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5505</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5506</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5507</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5509</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5510</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Bc__c</fullName>
        <description>List of additionan invisible message recipients</description>
        <externalId>false</externalId>
        <label>Bc</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cc__c</fullName>
        <description>List of aditional message recipients</description>
        <externalId>false</externalId>
        <label>Cc</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <description>Country - 2 digit iso code (DE, GB)
DOM_COUNTRY</description>
        <externalId>false</externalId>
        <label>Country</label>
        <picklist>
            <picklistValues>
                <fullName>AT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CZ</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ES</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GB</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TR</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ErrorText__c</fullName>
        <description>Staus error description</description>
        <externalId>false</externalId>
        <label>Error text</label>
        <length>2000</length>
        <type>LongTextArea</type>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Event__c</fullName>
        <description>The event hat raised this message. Currently implemented: 
Order.Created 
Order.Cancelled 
Appointment.CreatedOnToday</description>
        <externalId>false</externalId>
        <label>Event</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Format__c</fullName>
        <description>Sets output format
- TEXT
- HTML
- PDF
- LETTER
- WF01
- WF02
- WF03
- WF04
- WF05</description>
        <externalId>false</externalId>
        <label>Format</label>
        <picklist>
            <controllingField>Type__c</controllingField>
            <picklistValues>
                <fullName>TEXT</fullName>
                <controllingFieldValues>EMAIL</controllingFieldValues>
                <controllingFieldValues>SMS</controllingFieldValues>
                <controllingFieldValues>TODO</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HTML</fullName>
                <controllingFieldValues>EMAIL</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PDF</fullName>
                <controllingFieldValues>EMAIL</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LETTER</fullName>
                <controllingFieldValues>EMAIL</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WF01</fullName>
                <controllingFieldValues>WORKFLOW</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WF02</fullName>
                <controllingFieldValues>WORKFLOW</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WF03</fullName>
                <controllingFieldValues>WORKFLOW</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WF04</fullName>
                <controllingFieldValues>WORKFLOW</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WF05</fullName>
                <controllingFieldValues>WORKFLOW</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>From__c</fullName>
        <description>Message sender</description>
        <externalId>true</externalId>
        <label>From</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>external identifier. Used in Interfaces</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Optional referenced order</description>
        <externalId>false</externalId>
        <label>Referenced Order</label>
        <referenceTo>SCOrder__c</referenceTo>
        <relationshipName>Messages</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProcessDate__c</fullName>
        <description>Data were processed by the external tool</description>
        <externalId>false</externalId>
        <label>Process date</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ProcessResult__c</fullName>
        <externalId>false</externalId>
        <label>Process Result</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>ResourceOld__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the old resource before the last transaction (e.g. automatically set when an appointment is moved from one engineer to a new engineer)</description>
        <externalId>false</externalId>
        <label>ResourceOld</label>
        <referenceTo>SCResource__c</referenceTo>
        <relationshipLabel>Messages (ResourceOld)</relationshipLabel>
        <relationshipName>ResourceOld</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Resource__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Referenced Resource</label>
        <referenceTo>SCResource__c</referenceTo>
        <relationshipName>Messages</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>The message processing status:
- Open new message to be processed
- Processed the message was sent
- Error process error</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Open</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Processed</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Error</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Subject__c</fullName>
        <description>Subjet of the message</description>
        <externalId>false</externalId>
        <label>Subject</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text__c</fullName>
        <description>Message body</description>
        <externalId>false</externalId>
        <label>Text</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>To__c</fullName>
        <description>List of message recipients</description>
        <externalId>true</externalId>
        <label>To</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Describes the message type. The following values are currently used:
- SMS	short message
- EMAIL	e-mail
- WORKFLOW external processing step</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>SMS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>EMAIL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TODO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WORKFLOW</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <gender>Feminine</gender>
    <label>Nachricht</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Country__c</columns>
        <columns>Event__c</columns>
        <columns>Type__c</columns>
        <columns>Format__c</columns>
        <columns>Status__c</columns>
        <columns>ProcessDate__c</columns>
        <columns>Subject__c</columns>
        <columns>Order__c</columns>
        <columns>ProcessResult__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>IFMSG-{0000000000}</displayFormat>
        <label>Message Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Nachrichten</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Country__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ProcessDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>From__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>To__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Subject__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Format__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Country__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ProcessDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>From__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>To__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Subject__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Format__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Country__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ProcessDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>From__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>To__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Subject__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Format__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Country__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>ProcessDate__c</searchFilterFields>
        <searchFilterFields>From__c</searchFilterFields>
        <searchFilterFields>To__c</searchFilterFields>
        <searchFilterFields>Subject__c</searchFilterFields>
        <searchFilterFields>Type__c</searchFilterFields>
        <searchFilterFields>Format__c</searchFilterFields>
        <searchResultsAdditionalFields>Country__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ProcessDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>From__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Subject__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Format__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
