<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Defines one qualification that can be used in qualification profiles. The qualification can be assigned to domain values (like unit class, unit type, order types) or to individual accounts, contracts, articles. 
The auto number field ID &quot;Q-{0000000000}&quot; is required in the scheduling engine. Please do not change the format.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Description__c</fullName>
        <description>Optional description of the qualification</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>2000</length>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>External identifier. Used by interfaces and migration.</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Info__c</fullName>
        <description>The name of the qualification.</description>
        <externalId>false</externalId>
        <label>Info</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reference1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the value. Used by the order creation business logic to determine the corresponding qualification</description>
        <externalId>false</externalId>
        <label>Reference1</label>
        <referenceTo>SCDomainValue__c</referenceTo>
        <relationshipLabel>Qualifikationen</relationshipLabel>
        <relationshipName>Qualification</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>Defines the context of the qualification. Depending on the type the assignment of domain values 
- Product - Unit Class (DOM PRODUCTUNITCLASS)
- Product - Unit Type (DOM PRODUCTUNITTYPE)
- Product - Group (DOM PRODUCTGROUP)
- Product - energy (DOM PRODUCTENERGY)
- Order Type (DOM ORDERTYPE)
- Order Priority (DOM ORDERPRIO)
- Order Failure type (DOM ORDERFAILURETYPE)
- Language (DOM LANGUAGE)
- Customer
- Contract
- Article
- Others

DOM_QUALIFICATION_TYPE</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>DOM_PRODUCTUNITCLASS</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_PRODUCTUNITTYPE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_PRODUCTGROUP</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_PRODUCTENERGY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_PRODUCT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_ORDERTYPE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_ORDERPRIO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_ORDERFAILURETYPE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DOM_LANGUAGE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Customer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Contract</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Article</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Others</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ProductSkill</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <gender>Masculine</gender>
    <label>Qualifikation</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Type__c</columns>
        <columns>Reference1__c</columns>
        <columns>Info__c</columns>
        <columns>Description__c</columns>
        <columns>ID2__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>Q-{0000000000}</displayFormat>
        <label>ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Qualifikationen</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Reference1__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Info__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ID2__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Reference1__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Info__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ID2__c</lookupDialogsAdditionalFields>
        <lookupFilterFields>NAME</lookupFilterFields>
        <lookupFilterFields>Type__c</lookupFilterFields>
        <lookupFilterFields>Reference1__c</lookupFilterFields>
        <lookupFilterFields>Info__c</lookupFilterFields>
        <lookupFilterFields>ID2__c</lookupFilterFields>
        <lookupPhoneDialogsAdditionalFields>Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Reference1__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Info__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ID2__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Type__c</searchFilterFields>
        <searchFilterFields>Reference1__c</searchFilterFields>
        <searchFilterFields>Info__c</searchFilterFields>
        <searchFilterFields>ID2__c</searchFilterFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Reference1__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Info__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ID2__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
