<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Activity__c</fullName>
        <description>I = Insert
Q = With quantity
U = Update without quantity</description>
        <externalId>false</externalId>
        <inlineHelpText>I = Insert
Q = With quantity
U = Update without quantity</inlineHelpText>
        <label>Activity</label>
        <picklist>
            <picklistValues>
                <fullName>I</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>U</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Q</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ArticleLockType__c</fullName>
        <description>Used to check by a filter which articles are already locked and still in the stock items
50201 *	Active
50202 *	Indirect alternative available
50203	Locked (dead part)
50204	Consume, then locked
50205	Part has been replaced
50206	Expires, substitution
50207	Technical issue / quality stop / Technical Service Manager
50208	Trial Part</description>
        <externalId>false</externalId>
        <formula>TEXT(Article__r.LockType__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>50201 *	Active
50202 *	Indirect alternative available
50203	Locked (dead part)
50204	Consume, then locked
50205	Part has been replaced
50206	Expires, substitution
50207	Technical issue / quality stop / Technical Service Manager
50208	Trial Part</inlineHelpText>
        <label>Article Lock Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ArticleNameCalc__c</fullName>
        <description>Formula used to show the spare part description (see SCArticle)</description>
        <externalId>false</externalId>
        <formula>Article__r.ArticleNameCalc__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Article Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Article__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the article in tis stock.</description>
        <externalId>false</externalId>
        <label>Article</label>
        <referenceTo>SCArticle__c</referenceTo>
        <relationshipName>SC_Stock_Item</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ERPQty__c</fullName>
        <description>Used to store the original ERP quantity that was loaded during the migration. Used for review purposes.
CCEAG: 24.01.2012 KU: keine Nachkommastellen verwenden (dezimal 4 auf 0 gesetzt)</description>
        <externalId>false</externalId>
        <inlineHelpText>Used to store the original ERP quantity that was loaded during the migration. Used for review purposes.</inlineHelpText>
        <label>ERP Quantity</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>interface id</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>InventoryDate__c</fullName>
        <description>Date of the last stock taking.</description>
        <externalId>false</externalId>
        <label>Inventory Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>InventoryQty__c</fullName>
        <description>Quantity of the last stock taking.
CCEAG: 24.01.2012 KU: keine Nachkommastellen verwenden (dezimal 4 auf 0 gesetzt)</description>
        <externalId>false</externalId>
        <label>Inventory Qty</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MapDate__c</fullName>
        <description>(reserved) Moving Average Price - Date
Date of the last adjustment of the moving average price. Set by the material movement booking job.</description>
        <externalId>false</externalId>
        <label>Moving Average Price Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>MapPrice__c</fullName>
        <description>(reserved) Moving Average Price
Used for the annual stock valuation. Moving average price is calculated during the bokking of the material movements. The following formula is used: SO.MAPRICE = (SO.MAPRICE*SO.MAPQTY + PO.RAWPRICE * PO.QTY) / (SO.MAPQTY + PO.QTY) PO = PARTORDER (Material movements) SO = STOCKOBJ (Material stock)</description>
        <externalId>false</externalId>
        <label>Moving Average Price</label>
        <precision>14</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MapQty__c</fullName>
        <description>(reserved) Moving Average Price - Quantity
Stock quantity so far (accumulated total quantity). Increases by the receipt of the articles after updating the moving average price. MAPQTY = MAPQTY + PO.QTY</description>
        <externalId>false</externalId>
        <label>Moving Average Price Qty</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaxQty__c</fullName>
        <description>Replenishment - Maximum quantity
The technician gets an alert message if he tries to order additional parts that will exceed the MaxQty. This field is set automatically if the stock profile is assigned / maintained.</description>
        <externalId>false</externalId>
        <label>Max Qty</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MinQty__c</fullName>
        <description>Replenishment - Minimum quantity 
If the current quantity falls below the MinQty the automatically replenishment job will reorder the article. See ReplenishmentQty for more details.</description>
        <externalId>false</externalId>
        <label>Min Qty</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Permanent__c</fullName>
        <defaultValue>true</defaultValue>
        <description>All articles that belong permanently to a stock have this flag set to true. For articles that are booked only once for a particular service order into the stock the flag is set to false. This flag is evaluated in the material management functions to determine parts that have to be returned as they were ordered only for a specific service order.</description>
        <externalId>false</externalId>
        <label>Permanent</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Plant__c</fullName>
        <description>The name of the related plant - can be used to select country specific stock items</description>
        <externalId>false</externalId>
        <formula>Stock__r.Plant__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Plant</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Qty__c</fullName>
        <description>Quantity of the available article items (actual balance)</description>
        <externalId>false</externalId>
        <label>Qty</label>
        <precision>10</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ReplenishmentDelta__c</fullName>
        <description>Formula: Number of items that have to be replenished (not including already ordered items) = Qty__c -  MinQty__c
CCEAG: 24.01.2012 KU: keine Nachkommastellen verwenden (dezimal 4 auf 0 gesetzt)

21.10.2013: old: Qty__c -  MAX(MinQty__c, ReplenishmentQty__c)
21.10.2013: AB new: Qty__c -  MinQty__c</description>
        <externalId>false</externalId>
        <formula>Qty__c - MinQty__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Replenishment Delta</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ReplenishmentDisabled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Replenishment - If an article is removed from the stock profile this flag is set to indicate that the article is no longer to be replenished.</description>
        <externalId>false</externalId>
        <label>Replenishment Disabled</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ReplenishmentQty__c</fullName>
        <description>Replenishment - Quantity
The automatic replenishment uses this value (if set) instead of the minumum qty to calculate the number of parts to order. The replenishment quantity is optional. If this field is not set the minumum quantity is used instead. The replenishment quantity has to be greater than the MinQty and less or equal to the MaxQty.
CCEAG: 24.01.2012 KU: keine Nachkommastellen verwenden (dezimal 4 auf 0 gesetzt)</description>
        <externalId>false</externalId>
        <label>Replenishment Qty</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StockArticleId__c</fullName>
        <description>Internal helper field that is used to process stock / article related material movements in interfaces. Not relevant for interactive users. Used in WSMaterialDelivery to book parts to the correct stock item.</description>
        <externalId>false</externalId>
        <formula>Stock__c &amp; &apos;-&apos; &amp;  Article__c &amp; &apos;-&apos; &amp;  TEXT(ValuationType__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>StockArticleId</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Stock__c</fullName>
        <description>References to the stock, to which this information belongs.</description>
        <externalId>false</externalId>
        <label>Stock</label>
        <referenceTo>SCStock__c</referenceTo>
        <relationshipName>StockItem</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ValuationType__c</fullName>
        <description>The valuation type is used to classify the article in the stock items and forms a primary key with the article number. Only active if the stock and article are valuation type enabled.
ZNEU - new spare part
ZGEBROW - used spare part without value
ZGEBRMW - used spare part with value</description>
        <externalId>false</externalId>
        <label>Valuation Type</label>
        <picklist>
            <picklistValues>
                <fullName>ZNEU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ZGEBRMW</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ZGEBROW</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>util_StockItemSelector__c</fullName>
        <externalId>false</externalId>
        <formula>Plant__c &amp; &apos;-&apos; &amp;  Stock__r.Name &amp; &apos;-&apos; &amp; Article__r.Name &amp; &apos;-&apos; &amp; TEXT(ValuationType__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Stock Item Selector</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <gender>Feminine</gender>
    <label>Lagerortposition</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Article__c</columns>
        <columns>Qty__c</columns>
        <columns>Stock__c</columns>
        <columns>MaxQty__c</columns>
        <columns>MinQty__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>ID2</fullName>
        <columns>NAME</columns>
        <columns>Article__c</columns>
        <columns>Qty__c</columns>
        <columns>Stock__c</columns>
        <columns>MaxQty__c</columns>
        <columns>MinQty__c</columns>
        <columns>ID2__c</columns>
        <columns>util_StockItemSelector__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ID2__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>ID2</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>STI-{0000000000}</displayFormat>
        <label>Stock Item</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Lagerortpositionen</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Stock__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Article__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Qty__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>MinQty__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>MaxQty__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ValuationType__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ID2__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Stock__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Article__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Qty__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>MinQty__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>MaxQty__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ValuationType__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ID2__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Stock__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Article__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Qty__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>MinQty__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>MaxQty__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ValuationType__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ID2__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Stock__c</searchFilterFields>
        <searchFilterFields>Article__c</searchFilterFields>
        <searchFilterFields>Qty__c</searchFilterFields>
        <searchFilterFields>Permanent__c</searchFilterFields>
        <searchFilterFields>ValuationType__c</searchFilterFields>
        <searchFilterFields>ID2__c</searchFilterFields>
        <searchResultsAdditionalFields>Stock__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Article__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Qty__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>MinQty__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>MaxQty__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ValuationType__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ID2__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Non_empty_Stock</fullName>
        <active>true</active>
        <errorConditionFormula>LEN(   Stock__r.Name   ) == 0</errorConditionFormula>
        <errorMessage>Please select a valid stock.</errorMessage>
    </validationRules>
</CustomObject>
