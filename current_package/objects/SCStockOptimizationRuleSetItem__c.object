<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Specific optimization rule for a stock item. Extends the rules from the Stock Optimization Rule Set.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>CalculateRange__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Calculate proposal from monthly consumption based to the evaluated inverval of the rule set</description>
        <externalId>false</externalId>
        <inlineHelpText>Calculate proposal from monthly consumption based to the evaluated inverval of the rule set</inlineHelpText>
        <label>Calculate proposal from consumption</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Consummed__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Number of consummed parts within the interval</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of consummed parts within the interval</inlineHelpText>
        <label>Consummed &lt;=</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>External Key used by import from an external system</description>
        <externalId>true</externalId>
        <inlineHelpText>External Key used by import from an external system</inlineHelpText>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>ListpriceOperator__c</fullName>
        <description>The operator used to compare the list price of consummed article. The list price of consummed article is on the left of the operator. On the right there is the constant price you can edit.
E.g. consummed article list price &lt;= price</description>
        <externalId>false</externalId>
        <inlineHelpText>The operator used to compare the list price of consummed article. The list price of consummed article is on the left of the operator. On the right there is the constant price you can edit
E.g. consummed article list price &lt;= price</inlineHelpText>
        <label>Listprice of consummed article is</label>
        <picklist>
            <picklistValues>
                <fullName>=</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>&gt;</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>&gt;=</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>&lt;</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>&lt;=</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Listprice__c</fullName>
        <description>Price to compare with the list price of a consummed article.</description>
        <externalId>false</externalId>
        <inlineHelpText>Price to compare with the list price of a consummed article.</inlineHelpText>
        <label>Price</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProposalRule__c</fullName>
        <externalId>false</externalId>
        <formula>IF(CalculateRange__c , 
&apos;set to average with a &apos; &amp; TEXT(Range__c) &amp; &apos; month range&apos;, 
TEXT(ProposalValue__c)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Proposal</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProposalValue__c</fullName>
        <description>Absolute number of parts that are used as proposal</description>
        <externalId>false</externalId>
        <inlineHelpText>Absolute number of parts that are used as proposal</inlineHelpText>
        <label>Proposal value</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Range__c</fullName>
        <externalId>false</externalId>
        <label>Range</label>
        <picklist>
            <controllingField>CalculateRange__c</controllingField>
            <picklistValues>
                <fullName>use proposal field</fullName>
                <controllingFieldValues>unchecked</controllingFieldValues>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>1</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>7</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>8</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>11</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>12</fullName>
                <controllingFieldValues>checked</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>StockOptimizationRuleSet__c</fullName>
        <description>The master of the Stock Optimization Rule Set Item</description>
        <externalId>false</externalId>
        <inlineHelpText>The master of the Stock Optimization Rule Set Item</inlineHelpText>
        <label>Stock Optimization Rule Set</label>
        <referenceTo>SCStockOptimizationRuleSet__c</referenceTo>
        <relationshipName>Stock_Optimization_Rule_Set_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <gender>Feminine</gender>
    <label>Lageroptimierungsregelposition</label>
    <nameField>
        <displayFormat>SORSI-{0000000000}</displayFormat>
        <label>StockOptimizationRuleSetItem Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Lageroptimierungsregelpositionen</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>DefineProposalManually</fullName>
        <active>true</active>
        <errorConditionFormula>AND(CalculateRange__c != true, OR(ISNULL( ProposalValue__c ), ProposalValue__c &lt; 0))</errorConditionFormula>
        <errorDisplayField>ProposalValue__c</errorDisplayField>
        <errorMessage>Please enter a proposal value</errorMessage>
    </validationRules>
</CustomObject>
