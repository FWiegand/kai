<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Customer related notifications that pop up during the call taking process.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>AccountNumber__c</fullName>
        <description>Account number for the confirmation email (in case of payment notification).</description>
        <externalId>false</externalId>
        <formula>Account__r.AccountNumber</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Notifications</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Action__c</fullName>
        <externalId>false</externalId>
        <label>Action</label>
        <picklist>
            <controllingField>Context__c</controllingField>
            <picklistValues>
                <fullName>Info confirm</fullName>
                <controllingFieldValues>Customer Care</controllingFieldValues>
                <controllingFieldValues>Finance</controllingFieldValues>
                <controllingFieldValues>Planning</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Transfer to user</fullName>
                <controllingFieldValues>Customer Care</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Take payment</fullName>
                <controllingFieldValues>Finance</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Show text</fullName>
                <controllingFieldValues>Customer Care</controllingFieldValues>
                <controllingFieldValues>Planning</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Show text and confirm</fullName>
                <controllingFieldValues>Customer Care</controllingFieldValues>
                <controllingFieldValues>Planning</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Resolved by customer</fullName>
                <controllingFieldValues>Planning</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Take credit card payment</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Active__c</fullName>
        <externalId>false</externalId>
        <formula>IF(IsActive__c = &apos;true&apos;, IMAGE(&quot;/img/msg_icons/warning16.png&quot;, &quot;&quot;), 
        IF( IsActive__c = &apos;false&apos;, IMAGE(&quot;/img/msg_icons/confirm16.png&quot;, &quot;&quot;), &apos;&apos; )
    )</formula>
        <label>Active</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <defaultValue>IF(CONTAINS(LOWER($RecordType.Name),&apos;code 7&apos;), $Setup.SCApplicationSettings__c.NOTIFICATION_CODE7_AMOUNT__c ,null)</defaultValue>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Answer__c</fullName>
        <externalId>false</externalId>
        <label>Answer</label>
        <picklist>
            <controllingField>Question__c</controllingField>
            <picklistValues>
                <fullName>Yes</fullName>
                <controllingFieldValues>This is the question 1</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <controllingFieldValues>This is the question 1</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Unknown</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not solved by customer</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Customer advised</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>COND1__c</fullName>
        <externalId>false</externalId>
        <formula>IF( TEXT(Account__r.Type) =&apos;Homeowner&apos;,&apos;1&apos;, &apos;0&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>COND1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ClosedDate__c</fullName>
        <description>Set, when status is set to &quot;completed&quot;</description>
        <externalId>false</externalId>
        <inlineHelpText>Set, when status is set to &quot;completed&quot;</inlineHelpText>
        <label>ClosedDate</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ConfirmedBy__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Confirmed by</label>
        <referenceTo>User</referenceTo>
        <relationshipName>User</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Confirmed__c</fullName>
        <externalId>false</externalId>
        <label>Confirmed</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Context__c</fullName>
        <externalId>false</externalId>
        <label>Context</label>
        <picklist>
            <picklistValues>
                <fullName>Customer Care</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Planning</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Finance</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Duration__c</fullName>
        <defaultValue>CASE($RecordType.DeveloperName,
&quot;N001&quot;, 30,
&quot;N002&quot;, 7,
null)</defaultValue>
        <externalId>false</externalId>
        <label>Duration (days)</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Funktion__c</fullName>
        <externalId>false</externalId>
        <label>Funktion</label>
        <picklist>
            <controllingField>Answer__c</controllingField>
            <picklistValues>
                <fullName>Enable Scheduling</fullName>
                <controllingFieldValues>Customer advised</controllingFieldValues>
                <controllingFieldValues>Unknown</controllingFieldValues>
                <controllingFieldValues>Yes</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Disable Scheduling</fullName>
                <controllingFieldValues>No</controllingFieldValues>
                <controllingFieldValues>Not solved by customer</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Alternate primary key. External identifier for this ac-count used within interfaces to update this record. The external id is by default invisible to the user.</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IFID__c</fullName>
        <defaultValue>IF( $RecordType.DeveloperName = &apos;N002&apos;, &apos;Code1&apos;, &apos;xxx&apos;)</defaultValue>
        <description>IF( $RecordType.Id == &apos;N002&apos;, &apos;Code1&apos;, &apos;xxx&apos;)</description>
        <externalId>false</externalId>
        <label>IFID</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InstalledBase__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Installed Base</label>
        <referenceTo>SCInstalledBase__c</referenceTo>
        <relationshipName>Notifications</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>InternalOnly__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Internal only</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>IsActive__c</fullName>
        <externalId>false</externalId>
        <formula>IF( (TEXT(Status__c) = &apos;Active&apos; &amp;&amp; ValidTo__c &gt;= TODAY()) || (TEXT(Status__c) = &apos;Active&apos; &amp;&amp; ISNULL(ValidTo__c)), &apos;true&apos;,
      ( IF( TEXT(Status__c) = &apos;Completed&apos;, &apos;false&apos;,&apos;false&apos;))   
      )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is active</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>SCOrder__c</referenceTo>
        <relationshipName>Notifications</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductModel__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product Model</label>
        <referenceTo>SCProductModel__c</referenceTo>
        <relationshipName>Notifications</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Question__c</fullName>
        <externalId>false</externalId>
        <label>Question</label>
        <picklist>
            <controllingField>Context__c</controllingField>
            <picklistValues>
                <fullName>This is the question 1</fullName>
                <controllingFieldValues>Customer Care</controllingFieldValues>
                <controllingFieldValues>Planning</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ValidTo__c</fullName>
        <externalId>false</externalId>
        <formula>if(ISNULL(Duration__c), 
   null,    
   if(Duration__c &gt; 0, DATEVALUE(CreatedDate) +  Duration__c,  DATE(2200, 12, 31))
)</formula>
        <label>ValidTo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <label>Hinweis zum Kunden</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Active__c</columns>
        <columns>Duration__c</columns>
        <columns>ValidTo__c</columns>
        <columns>Description__c</columns>
        <columns>InternalOnly__c</columns>
        <columns>Context__c</columns>
        <columns>Action__c</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>N-{0000000000}</displayFormat>
        <label>Notification No</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Hinweise zum Kunden</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Default</fullName>
        <active>true</active>
        <label>Default</label>
        <picklistValues>
            <picklist>Action__c</picklist>
            <values>
                <fullName>Info confirm</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Resolved by customer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Show text</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Show text and confirm</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Take credit card payment</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Take payment</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Transfer to user</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Answer__c</picklist>
            <values>
                <fullName>Customer advised</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>No</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Not solved by customer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Unknown</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Yes</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Context__c</picklist>
            <values>
                <fullName>Customer Care</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Finance</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Planning</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Funktion__c</picklist>
            <values>
                <fullName>Disable Scheduling</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Enable Scheduling</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Question__c</picklist>
            <values>
                <fullName>This is the question 1</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Active__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Answer__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Confirmed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ConfirmedBy__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>InternalOnly__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
