<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to queue incoming sap requests</description>
    <enableActivities>false</enableActivities>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Data__c</fullName>
        <description>contains the JSON encoded data field</description>
        <externalId>false</externalId>
        <label>Data</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>20</visibleLines>
    </fields>
    <fields>
        <fullName>Exception__c</fullName>
        <description>Container for the Exception that caused the interface call to be qued into the interface queue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Container for the Exception that caused the interface call to be qued into the interface queue.</inlineHelpText>
        <label>Exception</label>
        <length>32768</length>
        <trackHistory>true</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>InterfaceHandler__c</fullName>
        <description>Class name of the object that implements the processing logic</description>
        <externalId>false</externalId>
        <label>InterfaceHandler</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MaterialMovement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Corresponding MaterilMovement</description>
        <externalId>false</externalId>
        <inlineHelpText>Corresponding MaterilMovement</inlineHelpText>
        <label>Materialbewegung</label>
        <referenceTo>SCMaterialMovement__c</referenceTo>
        <relationshipLabel>Interface Queue</relationshipLabel>
        <relationshipName>Interface_Queue</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Corresponding Order</description>
        <externalId>false</externalId>
        <inlineHelpText>Corresponding Order</inlineHelpText>
        <label>Auftrag</label>
        <referenceTo>SCOrder__c</referenceTo>
        <relationshipLabel>Interface Queue</relationshipLabel>
        <relationshipName>Interface_Queue</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>used to control the processing
pending to be processed
ok successfully processed
error processed with error</description>
        <externalId>true</externalId>
        <label>Status</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>queueStartProcessing__c</fullName>
        <externalId>false</externalId>
        <formula>IF(((now() - CreatedDate) * 86400) &gt; 90, &apos;1&apos;, &apos;0&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>queueStartProcessing</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Interface Queue</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>InterfaceHandler__c</columns>
        <columns>Order__c</columns>
        <columns>MaterialMovement__c</columns>
        <columns>Status__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>Alle</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>AllError</fullName>
        <columns>NAME</columns>
        <columns>InterfaceHandler__c</columns>
        <columns>Status__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>error</value>
        </filters>
        <label>Alle Error</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>AllOK</fullName>
        <columns>NAME</columns>
        <columns>InterfaceHandler__c</columns>
        <columns>Order__c</columns>
        <columns>MaterialMovement__c</columns>
        <columns>Status__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>ok</value>
        </filters>
        <label>Alle OK</label>
        <language>de</language>
    </listViews>
    <listViews>
        <fullName>AllPending</fullName>
        <columns>NAME</columns>
        <columns>InterfaceHandler__c</columns>
        <columns>Order__c</columns>
        <columns>MaterialMovement__c</columns>
        <columns>Status__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>equals</operation>
            <value>pending</value>
        </filters>
        <label>Alle Pending</label>
        <language>de</language>
    </listViews>
    <nameField>
        <displayFormat>IC-{0000000000}</displayFormat>
        <label>SCInterfaceQueue-Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Interface Queue</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>InterfaceHandler__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UPDATEDBY_USER</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>execute</fullName>
        <availability>online</availability>
        <description>Re-exceute the interface queue entry</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Feuer</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/10.0/apex.js&quot;)}

var NamespacePrefix = &apos;{!$Setup.SCApplicationSettings__c.NAMESPACE_PREFIX__c}&apos;;

if((NamespacePrefix != null) &amp;&amp; (NamespacePrefix.length&gt;2))
{
NamespacePrefix = NamespacePrefix.replace(/__/i,&apos;.&apos;);
}
else
{
NamespacePrefix = &apos;&apos;;
}

var msg = &quot;&quot; + sforce.apex.execute(NamespacePrefix + &quot;CCWSInterfaceQueue&quot;,&quot;execute&quot;, {queueEntry:&quot;{!SCInterfaceQueue__c.Id}&quot;});

alert(msg);</url>
    </webLinks>
</CustomObject>
