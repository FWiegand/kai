<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Defines where (van) stocks are delivered from</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Default__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Marks the default entry</description>
        <externalId>false</externalId>
        <label>Default</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>Detailed Description.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>250</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>true</caseSensitive>
        <description>external identifier. Used in Interfaces</description>
        <externalId>false</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Plant__c</fullName>
        <description>Reference to the plant</description>
        <externalId>false</externalId>
        <formula>Stock__r.Plant__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Plant</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sort__c</fullName>
        <description>Sort sequence, in which the evaluation of the table contents takes place. Controls which combination has the highest priority.</description>
        <externalId>false</externalId>
        <label>Sort</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Stock__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Reference to the stock.</description>
        <externalId>false</externalId>
        <label>Stock</label>
        <referenceTo>SCStock__c</referenceTo>
        <relationshipName>StockOrigins</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <gender>Feminine</gender>
    <label>Stock Origin</label>
    <nameField>
        <displayFormat>SO-{0000000000}</displayFormat>
        <label>StockOrigin</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Stock Origins</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Stock__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Plant__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Default__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ID2__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Stock__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Plant__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Default__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ID2__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Stock__c</searchFilterFields>
        <searchFilterFields>Plant__c</searchFilterFields>
        <searchFilterFields>Default__c</searchFilterFields>
        <searchFilterFields>ID2__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Non_empty_Stock</fullName>
        <active>true</active>
        <errorConditionFormula>LEN( Stock__r.Name ) == 0</errorConditionFormula>
        <errorMessage>Please select a valid stock.</errorMessage>
    </validationRules>
</CustomObject>
