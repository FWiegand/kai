<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
Contains the location address of the installed base. The systems, devices, accessories or other parts are attached to the location. The location has a geocoded address and is used for the scheduling  and tour optimization. A customer (owner, user, ...) can have multiple locations at the same time. Accounts or Person Accounts are assigned to the installed base location by the SCInstalledBaseRole.
Implementation note: for service orders where scheduling is relevant the location address is determined from the SCInstalledBaseLocation depending on the assigned order items (devices of the installed base). The scheduling engine does not evaluate the billint address! For new accounts within the order processing the billing address will be used as a default for the installed base location address.
A location can have multiple contact partners assigned. They are accessable within the order processing and on the mobile system.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>AcquisitionCurrency__c</fullName>
        <description>The currency unit for the acquirement.</description>
        <externalId>false</externalId>
        <inlineHelpText>The currency unit for the acquirement.</inlineHelpText>
        <label>Acquisition Currency</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AcquisitionValue__c</fullName>
        <description>The acquisition value.</description>
        <externalId>false</externalId>
        <inlineHelpText>The acquisition value.</inlineHelpText>
        <label>Acquisition value</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Address__c</fullName>
        <externalId>false</externalId>
        <formula>CASE(Country__c,
&quot;DE&quot;,
Street__c &amp; &quot; &quot; &amp; HouseNo__c &amp; BR() &amp; PostalCode__c &amp; &quot; &quot; &amp; City__c &amp; &quot; &quot; &amp; IF(City__c = District__c, &quot;&quot;, District__c),
&quot;NL&quot;,
Street__c &amp; &quot; &quot; &amp; HouseNo__c &amp; &quot; &quot; &amp; Extension__c &amp; BR() &amp; PostalCode__c &amp; &quot; &quot; &amp; City__c &amp; &quot; &quot; &amp; IF(City__c = District__c, &quot;&quot;, District__c),
&quot;GB&quot;,
HouseNo__c &amp; &quot; &quot; &amp; Street__c &amp; &quot;, &quot; &amp; Floor__c &amp; &quot;/&quot; &amp; FlatNo__c &amp; &quot; &quot; &amp; BR() &amp; City__c &amp; &quot; &quot; &amp; PostalCode__c &amp; &quot; &quot; ,
&quot;AT&quot;,
Street__c &amp; &quot; &quot; &amp; HouseNo__c &amp; Extension__c &amp; &quot; &quot; &amp; Floor__c &amp; &quot;/&quot; &amp; FlatNo__c &amp; BR() &amp; PostalCode__c &amp; &quot; &quot; &amp; City__c &amp; &quot; &quot; &amp; IF(City__c = District__c, &quot;&quot;, District__c),

HouseNo__c &amp; &quot; &quot; &amp; Street__c &amp; BR() &amp; City__c &amp; &quot; &quot; &amp; PostalCode__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AuditHint__c</fullName>
        <description>Optional information about the audit. Hints to the last audit.</description>
        <externalId>false</externalId>
        <inlineHelpText>Optional information about the audit. Hints to the last audit.</inlineHelpText>
        <label>Audit hint</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>AuditLast__c</fullName>
        <description>Contains date of the last audit. Used by the inhouse users to check when the next audit is required.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contains date of the last audit. Used by the inhouse users to check when the next audit is required.</inlineHelpText>
        <label>Last Audit</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>BookValueCurrency__c</fullName>
        <description>The currency unit for the book value.</description>
        <externalId>false</externalId>
        <inlineHelpText>The currency unit for the book value.</inlineHelpText>
        <label>Book value currency</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BookValue__c</fullName>
        <description>The book value.</description>
        <externalId>false</externalId>
        <inlineHelpText>The book value.</inlineHelpText>
        <label>Book value</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BuildingAge__c</fullName>
        <description>the age of the building in years: calculated by TODAY - BuildingDate</description>
        <externalId>false</externalId>
        <formula>IF(YEAR(TODAY()) - YEAR(BuildingDate__c) &gt;= 0, YEAR(TODAY()) - YEAR(BuildingDate__c), 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Building Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BuildingDate__c</fullName>
        <description>The date when the buliding was built. Required to calculate reduces tax rates depending on the age of the building (e.g. in countries like Belgium).</description>
        <externalId>false</externalId>
        <label>Building Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <description>Location Address - City  - filled by the address validation service</description>
        <externalId>false</externalId>
        <label>City</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CountryState__c</fullName>
        <description>Location Address - Country State Picklist (depends on Country, used for the address validation, reserved for countries like USA)
DOM_COUNTRYSTATE</description>
        <externalId>false</externalId>
        <label>State</label>
        <picklist>
            <controllingField>Country__c</controllingField>
            <picklistValues>
                <fullName>-</fullName>
                <controllingFieldValues>AT</controllingFieldValues>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>AK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AZ</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ID</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>KS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>KY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MD</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ME</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MO</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ND</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NJ</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NM</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SC</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SD</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TX</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>UT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>VT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WI</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WV</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WY</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <description>Location Address - Country Picklist (used for the address validation)
DOM_COUNTRY</description>
        <externalId>false</externalId>
        <label>Country</label>
        <picklist>
            <picklistValues>
                <fullName>AT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CZ</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ES</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GB</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>County__c</fullName>
        <description>Location Address - County - filled by the address validation (required for GB)</description>
        <externalId>false</externalId>
        <label>County</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>an optional describing text of the installed base location</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32000</length>
        <trackHistory>false</trackHistory>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>District__c</fullName>
        <description>Location Address - District (of the city) - optionally filled by the address validation</description>
        <externalId>false</externalId>
        <label>District</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Extension__c</fullName>
        <description>Location Address extension - Additional address information (part of the postal address) Required for some countries (like NL or USA) to for delivery of goods / letters. This information field contains: e.g. building, flat or apportment.</description>
        <externalId>false</externalId>
        <label>Extension</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>FlatNo__c</fullName>
        <description>Address Flat (for big houses)</description>
        <externalId>false</externalId>
        <label>FlatNo</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Floor__c</fullName>
        <description>Address Floor</description>
        <externalId>false</externalId>
        <label>Floor</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GeoApprox__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This flag can be used to select approximated addresses that do not reflect the cor-rect location (longitude/latutide). The flag is set by the geocoding component when the user can&apos;t find the exact address. True: the geox and geoy values of this ad-dress were approximated by searching for a neighbouring address. False: the geox and geoy values are exact geocoded.</description>
        <externalId>false</externalId>
        <label>Geo approximated</label>
        <trackHistory>false</trackHistory>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>GeoStatus__c</fullName>
        <description>Status of Geo Coding</description>
        <externalId>false</externalId>
        <inlineHelpText>Status of Geo Coding</inlineHelpText>
        <label>Geo Status</label>
        <picklist>
            <picklistValues>
                <fullName>Geocoded</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Invalid</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>GeoX__c</fullName>
        <description>Longitude - geo coordinate of the customer deter-mined from the billing address
(example 8.123° East -&gt; GeoX = -8.123). Used to dis-play the customer on maps. For routing and tour opti-mization the SCInstalledBase.GeoX value is used.</description>
        <externalId>false</externalId>
        <label>Geo Longitude</label>
        <precision>9</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GeoY__c</fullName>
        <description>Latitude - geo coordinate of the customer determined from the billing address
(example 51,234 North -&gt; GeoY = 51,234). Used to display the customer on maps. For routing and tour optimization the SCInstalledBase.GeoY value is used.</description>
        <externalId>false</externalId>
        <label>Geo Latitude</label>
        <precision>8</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>true</trackHistory>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>HouseNo__c</fullName>
        <description>Address house no - contains only the house number part of the street</description>
        <externalId>false</externalId>
        <label>HouseNo</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>External reference for this record (for migrating legacy data and within interfaces)</description>
        <externalId>true</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>LocName__c</fullName>
        <description>Optional name of the location (e.g. Building ABC) that is not covered by the address.</description>
        <externalId>false</externalId>
        <label>Location Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PostalCode__c</fullName>
        <description>Location Address - Postal Code</description>
        <externalId>true</externalId>
        <label>Postal Code</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Status of the location (active, temporary, deleted) Evaluated by the selection function.
DOM_INSTALLEDBASELOC_STATUS</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Deleted</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Street__c</fullName>
        <description>Location Address - Street replaces the internal field  (without house number)</description>
        <externalId>false</externalId>
        <label>Street</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <gender>Masculine</gender>
    <label>Equipment Standort</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>LocName__c</columns>
        <columns>Country__c</columns>
        <columns>Address__c</columns>
        <columns>ID2__c</columns>
        <columns>GeoY__c</columns>
        <columns>GeoX__c</columns>
        <columns>GeoStatus__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Changed_Last</fullName>
        <columns>NAME</columns>
        <columns>LocName__c</columns>
        <columns>Country__c</columns>
        <columns>Address__c</columns>
        <columns>ID2__c</columns>
        <columns>GeoY__c</columns>
        <columns>GeoX__c</columns>
        <columns>GeoStatus__c</columns>
        <columns>Status__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>Changed Last</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Empty_Locations</fullName>
        <columns>NAME</columns>
        <columns>LocName__c</columns>
        <columns>Country__c</columns>
        <columns>Address__c</columns>
        <columns>ID2__c</columns>
        <columns>GeoY__c</columns>
        <columns>GeoX__c</columns>
        <columns>GeoStatus__c</columns>
        <columns>Status__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>PostalCode__c</field>
            <operation>equals</operation>
        </filters>
        <label>Empty Locations</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Missing_Geocodes</fullName>
        <columns>NAME</columns>
        <columns>LocName__c</columns>
        <columns>Country__c</columns>
        <columns>Address__c</columns>
        <columns>ID2__c</columns>
        <columns>GeoY__c</columns>
        <columns>GeoX__c</columns>
        <columns>GeoStatus__c</columns>
        <columns>Status__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>GeoX__c</field>
            <operation>equals</operation>
        </filters>
        <label>Missing Geocodes</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>IBL-{0000000000}</displayFormat>
        <label>Installed Base Location</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Equipment Standorte</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>LocName__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Address__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BuildingAge__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BuildingDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Country__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ID2__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>LocName__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Address__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BuildingAge__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BuildingDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Country__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ID2__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>LocName__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Address__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Status__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BuildingAge__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BuildingDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Country__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ID2__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>LocName__c</searchFilterFields>
        <searchFilterFields>Address__c</searchFilterFields>
        <searchFilterFields>Status__c</searchFilterFields>
        <searchFilterFields>Country__c</searchFilterFields>
        <searchFilterFields>ID2__c</searchFilterFields>
        <searchResultsAdditionalFields>LocName__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Address__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>BuildingAge__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>BuildingDate__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Country__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ID2__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>SCbtnGeocode</fullName>
        <availability>online</availability>
        <description>Geocodes one or more installed base locations</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Geocode</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&apos;/resource/&apos; &amp; LEFT(SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(TEXT(NOW()),&apos;:&apos;,&apos;&apos;),&apos;-&apos;,&apos;&apos;),&apos; &apos;,&apos;&apos;),10) &amp; &apos;000/SCRes/lib/jquery/js/jquery-1.6.2.min.js&apos;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/20.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/10.0/apex.js&quot;)}

var records = {!GETRECORDIDS($ObjectType.SCInstalledBaseLocation__c)};

// first check if the user has selected at least one entry
if (records.length == 0)
{
    alert(&apos;{!$Label.SC_msg_PleaseSelectItems}&apos;);
}
else
{
    // disable the button to prevent the second click
    jQuery(this).removeClass(&apos;btn&apos;).addClass(&apos;btnDisabled&apos;);

    sforce.apex.execute(&quot;SCbtcGeocodeInstalledBaseLocation&quot;, &quot;syncGeocodeLocations&quot;, {locationIdList:records});
    window.location.reload();

    // enable the button
    jQuery(this).removeClass(&apos;btnDisabled&apos;).addClass(&apos;btn&apos;);
}</url>
    </webLinks>
    <webLinks>
        <fullName>SCbtnGeocodeLocation</fullName>
        <availability>online</availability>
        <description>Open an dialog window to geocode installed base location.</description>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Geocode Location</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>window.location = &quot;/apex/SCEditLocation?locationId={!SCInstalledBaseLocation__c.Id}&amp;sfLayout=true&quot;;</url>
    </webLinks>
</CustomObject>
