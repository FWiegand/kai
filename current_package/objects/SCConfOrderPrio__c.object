<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>&lt;Review completed 01.02.2013&gt; 
This table defines the entries for the automatic determination of the order priority and the start date. The entries will be used to be displayed in a dynamically created dialog, which will be displayed during the order creation process.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <fields>
        <fullName>Category__c</fullName>
        <description>A category goups the entries and will be show in a column in the dialog.</description>
        <externalId>false</externalId>
        <label>Category</label>
        <picklist>
            <picklistValues>
                <fullName>Partner</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Situation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FailureType</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Config</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <description>Country
DOM_COUNTRY</description>
        <externalId>false</externalId>
        <label>Country</label>
        <picklist>
            <picklistValues>
                <fullName>AT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CH</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CN</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CZ</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ES</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GB</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>LU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PL</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RU</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SK</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TR</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <description>An optional describing text</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>2000</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Factor__c</fullName>
        <description>A factor which will be multiplicated with the sum of the scores.</description>
        <externalId>false</externalId>
        <label>Factor</label>
        <precision>3</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field__c</fullName>
        <externalId>false</externalId>
        <formula>CASE($User.CurrentLanguage__c,
&apos;en_US&apos;, Text_en__c,
&apos;de&apos;, Text_de__c,
&apos;fr&apos;, Text_fr__c,
&apos;nl_NL&apos;, Text_nl__c,
&apos;cs&apos;, Text_cs__c,
&apos;da&apos;, Text_da__c,
&apos;es&apos;, Text_es__c,
&apos;hr_HR&apos;, Text_hr__c,
&apos;hu&apos;, Text_hu__c,
&apos;it&apos;, Text_it__c,
&apos;pl&apos;, Text_pl__c,
&apos;ro&apos;, Text_ro__c,
&apos;ru&apos;, Text_ru__c,
&apos;sk_SK&apos;, Text_sk__c,
&apos;sl_SI&apos;, Text_sl__c,
&apos;tr&apos;, Text_tr__c,
&apos;uk&apos;, Text_uk__c,
&apos;zh_CN&apos;, Text_zh__c,
Text_en__c &amp; &apos; (&apos; &amp; $User.CurrentLanguage__c &amp; &apos;) missing&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Field</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ID2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>external identifier. Used in Interfaces</description>
        <externalId>false</externalId>
        <label>ID2</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>KoCriteria__c</fullName>
        <defaultValue>false</defaultValue>
        <description>A indication if the field gets a special handling during calculation of the total score.</description>
        <externalId>false</externalId>
        <label>Ko Criteria</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Offset__c</fullName>
        <description>The offset (in days) from today for the desired start date set in the order. Only used when Category is set to &apos;Config&apos;.</description>
        <externalId>false</externalId>
        <label>Offset</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderPrio__c</fullName>
        <description>A value of the domain DOM_ORDERPRIO. Only used when Category is set to &apos;Config&apos;.</description>
        <externalId>false</externalId>
        <label>Order Prio</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PreSelField__c</fullName>
        <description>The preselection field defines which field from the sobject should be used for preselection. Will only be evaluate when Type__c = item.</description>
        <externalId>false</externalId>
        <label>Preselection Field</label>
        <picklist>
            <controllingField>PreSelObj__c</controllingField>
            <picklistValues>
                <fullName>ErrCond</fullName>
                <controllingFieldValues>OrderItem</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ErrSym1</fullName>
                <controllingFieldValues>OrderItem</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FailureType</fullName>
                <controllingFieldValues>Order</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PartnerType</fullName>
                <controllingFieldValues>RoleIR</controllingFieldValues>
                <controllingFieldValues>RoleSR</controllingFieldValues>
                <default>false</default>
            </picklistValues>
            <sorted>true</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PreSelObj__c</fullName>
        <description>The preselection object defines which sobject should be used for the preselection of an item. Will only be evaluate when Type__c = item.</description>
        <externalId>false</externalId>
        <label>Preselection Object</label>
        <picklist>
            <picklistValues>
                <fullName>RoleSR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RoleIR</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Order</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OrderItem</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>PreSelValue__c</fullName>
        <description>The preselection values will be compared with the value from the value of the sobject field, if they are equal, the preselection is set. Will only be evaluate when Type__c = item.</description>
        <externalId>false</externalId>
        <label>Preselection Value</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PrioValue__c</fullName>
        <description>The prio value from this up to the next the order prio of this entry should be set in the order. Only used when Category is set to &apos;Config&apos;.</description>
        <externalId>false</externalId>
        <label>Prio Value</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Score__c</fullName>
        <description>The score for the item. A list of values from 1 to 10, so no wrong score can be set.</description>
        <externalId>false</externalId>
        <label>Score</label>
        <picklist>
            <picklistValues>
                <fullName>1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>3</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>4</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>5</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>7</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>8</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>9</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>10</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SelectType__c</fullName>
        <description>The select type must be set if Type = Item. It defines whether the item will be displyed on the page as an option box or a radio button.</description>
        <externalId>false</externalId>
        <label>Select Type</label>
        <picklist>
            <picklistValues>
                <fullName>Single select</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Multiple select</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>SortOrder__c</fullName>
        <description>Sort sequence (ascending, smallest value first) for each category. Controls the display of the dialog.</description>
        <externalId>false</externalId>
        <label>Sort</label>
        <precision>4</precision>
        <required>true</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_cs__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Czech</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_da__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Danish</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_de__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text German</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_en__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text English</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_es__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Spanish</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_fr__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text French</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_hr__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Croatian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_hu__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Hungarian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_it__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Italian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_nl__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Dutch</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_pl__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Polish</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_ro__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Romanian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_ru__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Russian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_sk__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Slovak</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_sl__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Slovenian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_tr__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Turkish</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_uk__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Ukrainian</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Text_zh__c</fullName>
        <description>Localisation of field - see Formula Field</description>
        <externalId>false</externalId>
        <label>Text Chinese</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <description>The type defines whether the entry is a title, a subtitle or an item.</description>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>Title</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Subtitle</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Item</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Separator</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <gender>Feminine</gender>
    <label>Konfiguration Auftragspriorität</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Country__c</columns>
        <columns>Category__c</columns>
        <columns>Type__c</columns>
        <columns>SelectType__c</columns>
        <columns>Field__c</columns>
        <columns>Score__c</columns>
        <columns>Factor__c</columns>
        <columns>KoCriteria__c</columns>
        <columns>PreSelField__c</columns>
        <columns>PreSelObj__c</columns>
        <columns>PreSelValue__c</columns>
        <columns>PrioValue__c</columns>
        <columns>SortOrder__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>COP-{0000000000}</displayFormat>
        <label>No</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Konfiguration Auftragsprioritäten</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Country__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Field__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SortOrder__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Score__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>KoCriteria__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Factor__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Offset__c</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <listViewButtons>ManageItems</listViewButtons>
        <searchFilterFields>NAME</searchFilterFields>
        <searchResultsAdditionalFields>Country__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Field__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SortOrder__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Score__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>KoCriteria__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Factor__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Offset__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Non_Empty_Category</fullName>
        <active>true</active>
        <description>Category must be filled.</description>
        <errorConditionFormula>ISBLANK(TEXT(Category__c))</errorConditionFormula>
        <errorMessage>Please select a valid category.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Non_Empty_ConfigData</fullName>
        <active>true</active>
        <description>Order Prio, Prio Value and Offset must be filled when Category__c is &apos;Config&apos;</description>
        <errorConditionFormula>AND(TEXT(Category__c) == &apos;Config&apos;, OrderPrio__c == null, PrioValue__c == null, Offset__c == null)</errorConditionFormula>
        <errorMessage>Please enter data for order prio, prio value and offset if you have seleted &apos;Config&apos; as a category.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Non_Empty_Country</fullName>
        <active>true</active>
        <description>Country must be filled.</description>
        <errorConditionFormula>ISBLANK(TEXT(Country__c))</errorConditionFormula>
        <errorMessage>Please selct a valid country.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Non_Empty_ScoreAndFactor</fullName>
        <active>true</active>
        <description>Score or Factor must be filled when Type__c is Item</description>
        <errorConditionFormula>AND(TEXT(Type__c) == &apos;Item&apos;, ISBLANK(TEXT(Score__c)), Factor__c == null)</errorConditionFormula>
        <errorMessage>Please select a valid score or enter a factor if you have seleted &apos;Item&apos; as a type.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Non_filled_ScoreAndFactor</fullName>
        <active>true</active>
        <description>Score and Factor may not be fiiled at the same time</description>
        <errorConditionFormula>AND(NOT(ISBLANK(TEXT(Score__c))), Factor__c != null)</errorConditionFormula>
        <errorMessage>Please select a valid score or enter a factor.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>ManageItems</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Manage Items</masterLabel>
        <openType>sidebar</openType>
        <page>SCConfOrderPrio</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
