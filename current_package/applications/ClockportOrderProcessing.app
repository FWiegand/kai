<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>SCOrder__c</defaultLandingTab>
    <description>Clockport Service Management Suite - service order management</description>
    <label>Clockport Order Processing</label>
    <logo>SharedDocuments/CCE_logo.png</logo>
    <tab>SCOrderSearch</tab>
    <tab>CustomerSearch</tab>
    <tab>standard-Account</tab>
    <tab>SCOrder__c</tab>
    <tab>SCPlanningboard</tab>
    <tab>SCMap</tab>
    <tab>Tools</tab>
    <tab>SCInterfaceLog__c</tab>
    <tab>Inbox</tab>
    <tab>SCInstalledBase__c</tab>
    <tab>InterfaceClearing</tab>
    <tab>SCMaintenancePlan__c</tab>
    <tab>SCMaintenance__c</tab>
    <tab>SCInterfaceQueue__c</tab>
</CustomApplication>
