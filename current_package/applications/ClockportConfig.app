<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>ServiceConfiguration</defaultLandingTab>
    <description>Clockport Service Management Suite - Common configuration</description>
    <label>Clockport Config</label>
    <logo>SharedDocuments/CCE_logo.png</logo>
    <tab>standard-Chatter</tab>
    <tab>ServiceConfiguration</tab>
    <tab>SCPlanningboard</tab>
    <tab>SCInterfaceLog__c</tab>
    <tab>CCBillingIndicator__c</tab>
    <tab>SCConfOrderPrio__c</tab>
    <tab>SCCalendarItem__c</tab>
    <tab>SCQualificationProfileItem__c</tab>
    <tab>SCQualificationArticle__c</tab>
    <tab>SCQualificationAccount__c</tab>
    <tab>SCQualificationContract__c</tab>
    <tab>SCNotificationProduct__c</tab>
    <tab>SCAseControl__c</tab>
    <tab>SCSemaphore__c</tab>
</CustomApplication>
