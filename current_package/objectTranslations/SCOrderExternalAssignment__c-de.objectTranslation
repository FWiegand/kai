<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value>Externe Auftragsvergabe</value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value>Externe Auftragsvergaben</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value>Externe Auftragsvergabe</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value>Externe Auftragsvergaben</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value>Externen Auftragsvergabe</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value>Externen Auftragsvergaben</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value>Externe Auftragsvergabe</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value>Externe Auftragsvergaben</value>
    </caseValues>
    <fields>
        <label>Beschreibung</label>
        <name>Description__c</name>
    </fields>
    <fields>
        <help><!-- Contains the the SAP Purchase requisition number of the external order assigment. --></help>
        <label><!-- ERP BANF --></label>
        <name>ERPBANF__c</name>
    </fields>
    <fields>
        <help><!-- The operation id in the external system. --></help>
        <label><!-- ERP Operation ID --></label>
        <name>ERPOperationID__c</name>
    </fields>
    <fields>
        <help><!-- The date time of the last communication with SAP. --></help>
        <label><!-- ERPResultDate --></label>
        <name>ERPResultDate__c</name>
    </fields>
    <fields>
        <help><!-- External Assignment Adding Status of the handshaking between ClockPort and SAP. --></help>
        <label><!-- ERPStatusAdd --></label>
        <name>ERPStatusAdd__c</name>
        <picklistValues>
            <masterLabel>error</masterLabel>
            <translation>Fehler</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>none</masterLabel>
            <translation><!-- none --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ok</masterLabel>
            <translation>Ok</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pending</masterLabel>
            <translation>Warten...</translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- External Assignment Removing Status of the handshaking between ClockPort and SAP. --></help>
        <label><!-- ERPStatusRem --></label>
        <name>ERPStatusRem__c</name>
        <picklistValues>
            <masterLabel>error</masterLabel>
            <translation>Fehler</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>none</masterLabel>
            <translation><!-- none --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ok</masterLabel>
            <translation>Ok</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pending</masterLabel>
            <translation>Warten...</translation>
        </picklistValues>
    </fields>
    <fields>
        <label>Auftrag</label>
        <name>Order__c</name>
        <relationshipLabel>Externe Beauftragungen</relationshipLabel>
    </fields>
    <fields>
        <label>Einkaufsgruppe</label>
        <name>PurchaseGroup__c</name>
    </fields>
    <fields>
        <label>Einkaufsorganisation</label>
        <name>PurchaseOrg__c</name>
    </fields>
    <fields>
        <label>Status</label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>assigned</masterLabel>
            <translation>Zugeordnet</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>completed</masterLabel>
            <translation>Abgeschlossen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>deleted</masterLabel>
            <translation>Gelöscht</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pending</masterLabel>
            <translation>Warten...</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pendingWaitForDelete</masterLabel>
            <translation>Warten auf Löschrückmeldung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>rejected</masterLabel>
            <translation>Zurückgewiesen</translation>
        </picklistValues>
    </fields>
    <fields>
        <label>Lieferantenvertragsposition</label>
        <name>VendorContractItem__c</name>
        <relationshipLabel>Externe Beauftragungen</relationshipLabel>
    </fields>
    <fields>
        <label>Lieferantenvertrag</label>
        <name>VendorContract__c</name>
        <relationshipLabel>Externe Beauftragungen</relationshipLabel>
    </fields>
    <fields>
        <label>Lieferant</label>
        <name>Vendor__c</name>
        <relationshipLabel>Externe Beauftragungen</relationshipLabel>
    </fields>
    <fields>
        <label>Wunschdatum</label>
        <name>WishedDate__c</name>
    </fields>
    <fields>
        <label>Leistungen (gesamt)</label>
        <name>util_TotalItems__c</name>
    </fields>
    <fields>
        <label>Volumen (gesamt)</label>
        <name>util_TotalPrice__c</name>
    </fields>
    <fields>
        <label>Verkäufername</label>
        <name>util_VendorName__c</name>
    </fields>
    <gender>Feminine</gender>
    <layouts>
        <layout>Order External Assignment Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Interface --></label>
            <section>Interface</section>
        </sections>
    </layouts>
    <nameFieldLabel>Name</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- The selected order must be not completed! --></errorMessage>
        <name>OrderNotCompleted</name>
    </validationRules>
    <webLinks>
        <label>Neue externe Beauftragung</label>
        <name>SCbtnNewExternalAssignment</name>
    </webLinks>
    <webLinks>
        <label>Entfernen</label>
        <name>SCbtnRemoveOrderExternalAssignment</name>
    </webLinks>
</CustomObjectTranslation>
