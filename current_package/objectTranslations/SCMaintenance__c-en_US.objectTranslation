<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Maintenance</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Maintenances</value>
    </caseValues>
    <fields>
        <help><!-- Berechnet den Start des Zeitfensters für den Wartungsauftrag MaintenanceDateProposal__c+MaintenancePlan__r.MaintenanceAftrerDue__c-EncCorrection --></help>
        <label><!-- Correction of end of order window --></label>
        <name>EndCorrection__c</name>
    </fields>
    <fields>
        <help><!-- End of the maintenance window. This field is copied into the SCOrder.CustomerPrefEnd__c field when an order is created. It is calculated on the base of due date proposal regarding suspension or if set on the base of maintenance date to overwrite proposal --></help>
        <label><!-- End of the maintenance order window --></label>
        <name>EndDate__c</name>
    </fields>
    <fields>
        <label><!-- ID2 --></label>
        <name>ID2__c</name>
    </fields>
    <fields>
        <label><!-- Installed Base --></label>
        <name>InstalledBase__c</name>
        <relationshipLabel><!-- Maintenance --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The interval of the maintenance plan. --></help>
        <label><!-- Interval (days) --></label>
        <name>Interval__c</name>
    </fields>
    <fields>
        <help><!-- This date regards the suspension time. It is automatically calculated by the batch Job SCbtcMaintenanceDueDateSet. --></help>
        <label><!-- Due date proposal regarding suspension --></label>
        <name>MaintenanceDateProposalFormel__c</name>
    </fields>
    <fields>
        <help><!-- This date regards the suspension time. It is automatically calculated by a trigger. It is used to generate an order for this date If the Maintenance date to overwrite proposal is  not set by the user. --></help>
        <label><!-- Due date proposal regarding suspension --></label>
        <name>MaintenanceDateProposal__c</name>
    </fields>
    <fields>
        <help><!-- The date of the very first maintenance. --></help>
        <label><!-- Maintenance first date --></label>
        <name>MaintenanceFirstDate__c</name>
    </fields>
    <fields>
        <help><!-- The date of first maintenance in current year. It is set by trigger to the due date while 1. maintenance last date is empty or while 2. the due date is in the different year than the maintenance last date. Do not change it manually. --></help>
        <label><!-- Maintenance first in year date --></label>
        <name>MaintenanceFirstInYearDate__c</name>
    </fields>
    <fields>
        <help><!-- It is calculated by the trigger on behalf of the maintenance first in year date and due date proposal regarding suspension and suspension time. It is used to automatically determine the price list. --></help>
        <label><!-- Maintenance in calendar year --></label>
        <name>MaintenanceInCalendarYear__c</name>
    </fields>
    <fields>
        <help><!-- Last maintenance. Automatically set when the maintenance order has been completed. Used for calculating the first maintenance date in year, the maintenance count in year and the price list. --></help>
        <label><!-- Maintenance last date --></label>
        <name>MaintenanceLastDate__c</name>
    </fields>
    <fields>
        <label><!-- Maintenance Plan --></label>
        <name>MaintenancePlan__c</name>
        <relationshipLabel><!-- Maintenance --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Since this date an order can be automatically created by the Apex Job SCbtcMaintenanceOrderCreate. --></help>
        <label><!-- Create order on --></label>
        <name>OrderCreationDate__c</name>
    </fields>
    <fields>
        <help><!-- Order processing status. It is set to created after creating an order, and to scheduled after scheduling an order. After the order has been completed the field is set to - - NONE- -  --></help>
        <label><!-- Order processing status --></label>
        <name>OrderStatus__c</name>
        <picklistValues>
            <masterLabel>created</masterLabel>
            <translation><!-- created --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>scheduled</masterLabel>
            <translation><!-- scheduled --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Reference to the actual order. The order can be created manually or automatically on the base of the due date proposal regarding suspension or the maintenance date to overwrite proposal. After the order has been completed the reference is set null. --></help>
        <label><!-- Order --></label>
        <name>Order__c</name>
        <relationshipLabel><!-- Maintenance --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Set by trigger based on the number of maintenances in the current calender year. --></help>
        <label><!-- Price list --></label>
        <name>Pricelist__c</name>
        <relationshipLabel><!-- Maintenance --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- SerialNo --></label>
        <name>SerialNo__c</name>
    </fields>
    <fields>
        <help><!-- Wird zu Berechnung des Starts des Wartungszeitfensters MaintenanceDateProposal__c-MaintenancePlan__r.MaintenanceBeforeDue__c+StartCorrection genutzt. --></help>
        <label><!-- Correction of start of order window --></label>
        <name>StartCorrection__c</name>
    </fields>
    <fields>
        <help><!-- Start of the maintenance window. This field is copied into the SCOrder.CustomerPrefStart__c field when an order is created. Calculated on the base of the due date proposal regarding suspension or the maintenance date to overwrite proposal. --></help>
        <label><!-- Start of the maintenance order window --></label>
        <name>StartDate__c</name>
    </fields>
    <fields>
        <label><!-- Maintenance status --></label>
        <name>Status__c</name>
        <picklistValues>
            <masterLabel>active</masterLabel>
            <translation><!-- active --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>active no order</masterLabel>
            <translation><!-- active no order --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>suspended</masterLabel>
            <translation><!-- suspended --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Suspension End (Day) --></label>
        <name>SuspensionEndDay__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>10</masterLabel>
            <translation><!-- 10 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>11</masterLabel>
            <translation><!-- 11 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>12</masterLabel>
            <translation><!-- 12 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>13</masterLabel>
            <translation><!-- 13 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>14</masterLabel>
            <translation><!-- 14 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>15</masterLabel>
            <translation><!-- 15 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>16</masterLabel>
            <translation><!-- 16 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>17</masterLabel>
            <translation><!-- 17 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>18</masterLabel>
            <translation><!-- 18 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>19</masterLabel>
            <translation><!-- 19 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>20</masterLabel>
            <translation><!-- 20 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>21</masterLabel>
            <translation><!-- 21 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>22</masterLabel>
            <translation><!-- 22 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>23</masterLabel>
            <translation><!-- 23 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>24</masterLabel>
            <translation><!-- 24 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>25</masterLabel>
            <translation><!-- 25 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>26</masterLabel>
            <translation><!-- 26 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>27</masterLabel>
            <translation><!-- 27 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>28</masterLabel>
            <translation><!-- 28 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>29</masterLabel>
            <translation><!-- 29 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>30</masterLabel>
            <translation><!-- 30 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>31</masterLabel>
            <translation><!-- 31 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>7</masterLabel>
            <translation><!-- 7 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>9</masterLabel>
            <translation><!-- 9 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Suspension End (Month) --></label>
        <name>SuspensionEndMonth__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>10</masterLabel>
            <translation><!-- 10 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>11</masterLabel>
            <translation><!-- 11 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>12</masterLabel>
            <translation><!-- 12 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>7</masterLabel>
            <translation><!-- 7 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>9</masterLabel>
            <translation><!-- 9 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Start day of the suspension. --></help>
        <label><!-- Suspension Start (Day) --></label>
        <name>SuspensionStartDay__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>10</masterLabel>
            <translation><!-- 10 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>11</masterLabel>
            <translation><!-- 11 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>12</masterLabel>
            <translation><!-- 12 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>13</masterLabel>
            <translation><!-- 13 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>14</masterLabel>
            <translation><!-- 14 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>15</masterLabel>
            <translation><!-- 15 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>16</masterLabel>
            <translation><!-- 16 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>17</masterLabel>
            <translation><!-- 17 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>18</masterLabel>
            <translation><!-- 18 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>19</masterLabel>
            <translation><!-- 19 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>20</masterLabel>
            <translation><!-- 20 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>21</masterLabel>
            <translation><!-- 21 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>22</masterLabel>
            <translation><!-- 22 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>23</masterLabel>
            <translation><!-- 23 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>24</masterLabel>
            <translation><!-- 24 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>25</masterLabel>
            <translation><!-- 25 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>26</masterLabel>
            <translation><!-- 26 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>27</masterLabel>
            <translation><!-- 27 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>28</masterLabel>
            <translation><!-- 28 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>29</masterLabel>
            <translation><!-- 29 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>30</masterLabel>
            <translation><!-- 30 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>31</masterLabel>
            <translation><!-- 31 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>7</masterLabel>
            <translation><!-- 7 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>9</masterLabel>
            <translation><!-- 9 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The month of the suspension start. --></help>
        <label><!-- Suspension Start (Month) --></label>
        <name>SuspensionStartMonth__c</name>
        <picklistValues>
            <masterLabel>1</masterLabel>
            <translation><!-- 1 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>10</masterLabel>
            <translation><!-- 10 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>11</masterLabel>
            <translation><!-- 11 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>12</masterLabel>
            <translation><!-- 12 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>2</masterLabel>
            <translation><!-- 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>3</masterLabel>
            <translation><!-- 3 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>4</masterLabel>
            <translation><!-- 4 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>5</masterLabel>
            <translation><!-- 5 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>6</masterLabel>
            <translation><!-- 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>7</masterLabel>
            <translation><!-- 7 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>8</masterLabel>
            <translation><!-- 8 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>9</masterLabel>
            <translation><!-- 9 --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- CDE Type 9 --></label>
        <name>cce_CDE_Type_9__c</name>
    </fields>
    <fields>
        <label><!-- CDE Type M --></label>
        <name>cce_CDE_Type_M__c</name>
    </fields>
    <fields>
        <help><!-- The date of first maintenance in current year. It is set automatically. --></help>
        <label><!-- Maintenance first in year date --></label>
        <name>utilMaintenanceFirstInYearDate__c</name>
    </fields>
    <fields>
        <help><!-- It is automaticaly calculated by batch job SCbtcMaintenanceDueDateSet on behalf of the maintenance first in year date and due date proposal regarding suspension and suspension time. It is used to automatically determine the price list. --></help>
        <label><!-- Maintenance in calendar year --></label>
        <name>utilMaintenanceInCalendarYear__c</name>
    </fields>
    <fields>
        <help><!-- Set by batch Job SCbtcMaintenanceDueDateSet based on the number of maintenances in the current calender year. --></help>
        <label><!-- Price List --></label>
        <name>utilPricelist__c</name>
    </fields>
    <layouts>
        <layout>Installed Base Maintenance Plan-Layout</layout>
        <sections>
            <label><!-- Auftrag --></label>
            <section>Auftrag</section>
        </sections>
        <sections>
            <label><!-- Aussetzphase --></label>
            <section>Aussetzphase</section>
        </sections>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Equipment --></label>
            <section>Equipment</section>
        </sections>
    </layouts>
    <nameFieldLabel>Maintenance Name</nameFieldLabel>
    <startsWith>Consonant</startsWith>
    <webLinks>
        <label><!-- SCMaintenanceCreateOrder --></label>
        <name>SCMaintenanceCreateOrder</name>
    </webLinks>
    <webLinks>
        <label><!-- SCMaintenanceNextDueDate --></label>
        <name>SCMaintenanceNextDueDate</name>
    </webLinks>
</CustomObjectTranslation>
