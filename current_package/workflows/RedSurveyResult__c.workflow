<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetRedActivation</fullName>
        <description>Sets the field RedActivation__c of the Account</description>
        <field>RedActivation__c</field>
        <formula>ActivationScore__c</formula>
        <name>Set Red Activation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRedArea</fullName>
        <description>Sets the field SetRedArea__c of the Account with the value of the RedScore</description>
        <field>RedArea__c</field>
        <formula>AreaScore__c</formula>
        <name>Set Red Area</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRedAssortment</fullName>
        <description>Sets the field RedAssortment__c of the Account with the value of the RedScore</description>
        <field>RedAssortment__c</field>
        <formula>AssortmentScore__c</formula>
        <name>Set Red Assortment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRedCooler</fullName>
        <description>Sets the field RedCooler__c of the Account with the value of the RedScore</description>
        <field>RedCooler__c</field>
        <formula>CoolerScore__c</formula>
        <name>Set Red Cooler</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRedScore</fullName>
        <description>Sets the field RedScore__c of the Account with the value of the RedScore</description>
        <field>RedScore__c</field>
        <formula>RedScore__c</formula>
        <name>Set Red Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetRedScoreDate</fullName>
        <description>Sets the field RedScoreDate__c of the Account with the value of the RedScore</description>
        <field>RedScoreDate__c</field>
        <formula>DATEVALUE(CreatedDate)</formula>
        <name>Set Red Score Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Red_Score_Date</fullName>
        <description>Sets the field RedScoreDate__c of the Account with the value of the RedScore</description>
        <field>RedScoreDate__c</field>
        <formula>RedResultDate__c</formula>
        <name>Set Red Score Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Red_Score_Trend</fullName>
        <description>Writes the RedScoreTrend from the RedSurveyResult to the field on the Account</description>
        <field>REDScoreTrend__c</field>
        <formula>RedScoreTrend__c</formula>
        <name>Set Red Score Trend</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>WriteRedSurveyResultToAccount</fullName>
        <actions>
            <name>SetRedActivation</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetRedArea</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetRedAssortment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetRedCooler</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetRedScore</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SetRedScoreDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Red_Score_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Red_Score_Trend</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Writes all relevant red survey results back to the Account ones a new red survey result was saved.</description>
        <formula>NOT( ISBLANK( RedScore__c ) ) &amp;&amp;  RedResultDate__c &gt;  Account__r.RedScoreDate__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
