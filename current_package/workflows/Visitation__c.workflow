<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_last_Visitation_on_Account</fullName>
        <field>SalesLastVisit__c</field>
        <formula>DATEVALUE( Time__c )</formula>
        <name>Update last Visitation on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update last Visitation on Account</fullName>
        <actions>
            <name>Update_last_Visitation_on_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the last visitation field on the Account, with the date part of the field time from the visitation ones a visitation is set to completed</description>
        <formula>Completed__c = true &amp;&amp; (  DATEVALUE( Time__c )  &gt;  Account__r.SalesLastVisit__c ||  ISBLANK(Account__r.SalesLastVisit__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
