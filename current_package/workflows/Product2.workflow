<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Product2_Name_update</fullName>
        <field>Name</field>
        <formula>if(
Text(Sub_group__c)&lt;&gt;&apos;Standard&apos;,
TEXT(Group__c)  &amp; &apos; &apos; &amp;  TEXT(Sub_group__c) &amp; &apos; &apos; &amp;  TEXT(PackingSize__c) &amp;  TEXT(measuring_unit__c) &amp; &apos; &apos; &amp;  TEXT(PackingType__c),
TEXT(Group__c)  &amp; &apos; &apos; &amp;  TEXT(PackingSize__c) &amp;  TEXT(measuring_unit__c) &amp; &apos; &apos; &amp;  TEXT(PackingType__c)
)</formula>
        <name>Product2 Name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Nameupdate Product2</fullName>
        <actions>
            <name>Product2_Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product2.Group__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.Sub_group__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PackingSize__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.PackingType__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Product2.measuring_unit__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Name of a Product according to the Settings</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
