<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Clickarea_set_icon_Id</fullName>
        <description>Sets the Icon Id based on the selected type</description>
        <field>Icon__c</field>
        <formula>CASE(TEXT( Type__c ), 
&apos;Activation&apos;, &apos;015c00000009vVVAAY&apos;, 
&apos;Additional Placement&apos;,&apos;015M0000000A8JNIA0&apos;,
&apos;Assortment&apos;, &apos;015M0000000A8JLIA0&apos;,
&apos;Devices&apos;,&apos;015M0000000A8JJIA0&apos;,
&apos;Focus topics&apos;,&apos;015M0000000A8JHIA0&apos;,
&apos;Promotions&apos;,&apos;015M0000000A8JEIA0&apos;,
&apos;Combination offer&apos;, &apos;015M0000000A8JPIA0&apos;,
&apos;Counter Activation&apos;, &apos;015M0000000A8JTIA0&apos;,
&apos;Exclusive Brand&apos;, &apos;015M0000000A8JUIA0&apos;,
&apos;KO Products on first Position&apos;, &apos;015M0000000A8JQIA0&apos;,
&apos;Menue&apos;,&apos;015M0000000A8JSIA0&apos;,
&apos;Outside Activation&apos;,&apos;015M0000000A8JIIA0&apos;,
&apos;Packaging size&apos;,&apos;015M0000000A8JVIA0&apos;,
&apos;School Policy&apos;,&apos;015M0000000A8JWIA0&apos;,
&apos;Size Concept&apos;,&apos;015M0000000A8JRIA0&apos;,
&apos;Area&apos;,&apos;015M0000000A8JOIA0&apos;,
&apos;Kombiangebot&apos;,&apos;015c0000000A63TAAS&apos;,
&apos;&apos;)</formula>
        <name>Clickarea set icon Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_0_value</fullName>
        <description>Deletes the value of the field &quot;Picture&quot; if there are only 0 values in</description>
        <field>Picture__c</field>
        <name>Delete 0 value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Delet 0 values</fullName>
        <actions>
            <name>Delete_0_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PofSClickarea__c.Picture__c</field>
            <operation>equals</operation>
            <value>000000000000000</value>
        </criteriaItems>
        <description>Deletes the value of the field &quot;Picture&quot; if there are only 0 values in</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set icon Id</fullName>
        <actions>
            <name>Clickarea_set_icon_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PofSClickarea__c.Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets the icon Id based on the selected Type of the click area</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
