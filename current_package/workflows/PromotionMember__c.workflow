<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Create_UniqueKey_for_PromotionMember</fullName>
        <field>AccountPromotonID__c</field>
        <formula>CASESAFEID(Account__c)+ CASESAFEID(Promotion__c)</formula>
        <name>Create UniqueKey for PromotionMember</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Create UniqueKey PromotionMember</fullName>
        <actions>
            <name>Create_UniqueKey_for_PromotionMember</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets a external ID field to a combination of Account ID and Promotion Id to guarantee no duplicates on Promotion memeber</description>
        <formula>NOT(ISBLANK(Account__c)) &amp;&amp; NOT(ISBLANK(Promotion__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
