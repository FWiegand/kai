<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Complaint_Set_Record_Type_Beschw</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Beschwerde</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Complaint - Set Record Type Beschw</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Complaint_Set_Record_Type_Fahrer</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Fahrer_Hotline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Complaint - Set Record Type Fahrer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Complaint_Set_Record_Type_Pro_Ak</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Pro_aktive_Kundeninformation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Complaint - Set Record Type Pro-Ak</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Complaint_Set_Record_Type_VB_Hot</fullName>
        <field>RecordTypeId</field>
        <lookupValue>VB_Hotline</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case: Complaint - Set Record Type VB-Hot</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Copy_Filterzeitraum</fullName>
        <field>Zeitraumsfilter_statisch__c</field>
        <formula>Zeitraumsfilter__c</formula>
        <name>Case: Copy Filterzeitraum</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Copy_Zeitraumberechnung</fullName>
        <field>Erfassungszeitraum__c</field>
        <formula>Zeitraum__c</formula>
        <name>Case: Copy Zeitraumberechnung</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Unify_Date_Filter</fullName>
        <field>Erfassungsdatum__c</field>
        <formula>IF(ISBLANK( Von__c ),  DATEVALUE(CreatedDate ) , Von__c)</formula>
        <name>Case: Unify Date Filter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Case%3A Complaint - Set Record Type Beschwerde</fullName>
        <actions>
            <name>Case_Complaint_Set_Record_Type_Beschw</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Fragebogen__c</field>
            <operation>equals</operation>
            <value>Beschwerde</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Set Record Type Fahrer-Hotline</fullName>
        <actions>
            <name>Case_Complaint_Set_Record_Type_Fahrer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Fragebogen__c</field>
            <operation>equals</operation>
            <value>Fahrer-Hotline</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Set Record Type Pro-Aktive Services</fullName>
        <actions>
            <name>Case_Complaint_Set_Record_Type_Pro_Ak</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Fragebogen__c</field>
            <operation>equals</operation>
            <value>Pro-aktive Services</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Set Record Type VB-Hotline</fullName>
        <actions>
            <name>Case_Complaint_Set_Record_Type_VB_Hot</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Fragebogen__c</field>
            <operation>equals</operation>
            <value>VB-Hotline</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Update Report Fields Step 1</fullName>
        <actions>
            <name>Case_Unify_Date_Filter</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Beschwerde,Pro-aktive Kundeninformation,VB-Hotline,Fahrer-Hotline,Bestellungen</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Update Report Fields Step 2</fullName>
        <actions>
            <name>Case_Copy_Zeitraumberechnung</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(  AND(     OR( 		ISPICKVAL(Fragebogen__c, &quot;Beschwerde&quot;) , 		ISPICKVAL(Fragebogen__c, &quot;Pro-aktive Services&quot;), 		ISPICKVAL(Fragebogen__c, &quot;VB-Hotline&quot;), 		ISPICKVAL(Fragebogen__c, &quot;Fahrer-Hotline&quot;), 		ISPICKVAL(Fragebogen__c, &quot;Bestellungen&quot;) 		), 	OR ( 		NOT(ISBLANK(Erfassungsdatum__c )), 		ISCHANGED(Erfassungsdatum__c )  		)      ),  true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case%3A Complaint - Update Report Fields Step 3</fullName>
        <actions>
            <name>Case_Copy_Filterzeitraum</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(  AND(     OR( 		ISPICKVAL(Fragebogen__c, &quot;Beschwerde&quot;) , 		ISPICKVAL(Fragebogen__c, &quot;Pro-aktive Services&quot;), 		ISPICKVAL(Fragebogen__c, &quot;VB-Hotline&quot;), 		ISPICKVAL(Fragebogen__c, &quot;Fahrer-Hotline&quot;), 		ISPICKVAL(Fragebogen__c, &quot;Bestellungen&quot;) 		), 	OR ( 		NOT(ISBLANK( Erfassungszeitraum__c  )), 		ISCHANGED( Erfassungszeitraum__c  )  		)      ),  true, false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
