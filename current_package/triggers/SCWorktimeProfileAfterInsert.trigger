// Creates the worktime items for the specified weeks
trigger SCWorktimeProfileAfterInsert on SCWorktimeProfile__c (after insert) 
{
    //create default worktime items for every week after inserting worktimeprofile
    //System.debug('#### worktimeProfile : AfterInsert()');
    for(SCWorktimeProfile__c profile: Trigger.new)
    {
        //check for order relation
        if(profile.weeks__c != null)
        {
            List<SCWorktime__c> worktimes = new List<SCWorktime__c>();    
            for (integer i = 1; i <= profile.weeks__c  ; i++)  
            {
                for (integer day = 1; day <= 5; day ++)
                {
                    //create new assignment
                    SCWorktime__c worktimeItem = new SCWorktime__c();
                    worktimeItem.week__c = i;
                    worktimeItem.profile__c = profile.ID;
                    //// Domäne WeekDay (8800)
                    integer nextDay = 8800 + day;
                    worktimeItem.Day__c = ''+ nextDay;
                    worktimes.add(worktimeItem);                
                }
             }
             insert worktimes;
             //System.debug('#### worktimeProfileItems : new Items after insert ' + worktimes);
        } 
    }
}