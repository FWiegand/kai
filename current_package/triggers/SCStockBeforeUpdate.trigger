/**
 * This trigger sets the activity flag to "U".
 * 
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCStockBeforeUpdate on SCStock__c (before update) 
{
	if (SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCStockAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
    
	for(Integer i = 0 ; i < Trigger.new.size(); i++ )
	{
		SCStock__c stockNew = Trigger.new.get(i);
		SCStock__c stockOld = Trigger.old.get(i);
		
		system.debug('Stock New: ' + stockNew);
		system.debug('Stock Old: ' + stockOld);
		
		//Nothing to do if stock not modified
		// "==" -> equals
		// "===" -> reference equals
		if(stockNew == stockOld)
		{
			system.debug('Stocks are equal: ' + stockNew);
			continue;
		}
		
		//ignore trigger on reset activity
		// U -> NULL; I -> NULL
		if
		(	stockNew.Activity__c == NULL && 
			(
				stockOld.Activity__c == 'U' ||
				stockOld.Activity__c == 'I' 
			)
		)
		{
			continue;
		}
		
		//disable activity update if already set to 'I'
		if(stockOld.Activity__c == 'I')
		{
			stockNew.Activity__c = stockOld.Activity__c;
			
		}
		//Set activity to 'U' if totalarticles not changed 
		// TotalArticales__c changes only by booking material movements
		else if(stockNew.TotalArticles__c == stockOld.TotalArticles__c)
		{
			stockNew.Activity__c = 'U';
		}
	}
	 
}