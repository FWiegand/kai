trigger fSFA_PofS_validation_trigger on PictureOfSuccess__c (before insert, before update) {
	String mode = 'update';
	if(Trigger.isInsert){ 
		mode='insert';
	}
	
	fSFA_PofS_validation pofsval = new fSFA_PofS_validation();
	pofsval.validateData(Trigger.new, mode);
}