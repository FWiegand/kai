trigger SolutionTrigger on Solution (before update, before insert) {
	if (Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)) {
		SolutionTriggerMethods.doBeforeUpdateInsert(Trigger.New);
	}
}