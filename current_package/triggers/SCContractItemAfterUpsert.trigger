/*
 * @(#)SCContractItemAfterUpsert
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This trigger updates the field contractRef__c in the installed base,
 * which contains this contract item.
 *
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCContractItemAfterUpsert on SCContractItem__c (after insert, after update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCContractItemAfterUpsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        for (Integer cnt=0; cnt<Trigger.new.size(); cnt++)
        {
            if (SCBase.isSet(Trigger.new.get(cnt).InstalledBase__c))
            {
                SCboInstalledBase.updateContractRef(Trigger.new.get(cnt).InstalledBase__c);
            }
        }
    }
}