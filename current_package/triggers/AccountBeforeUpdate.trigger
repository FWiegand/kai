trigger AccountBeforeUpdate on Account (before update) {
    for (Account acc : Trigger.new) {
        Account accOld;
        if (acc.BillingState != acc.BillingCountryState__c)
        {
            if (acc.BillingState == null) { acc.BillingState = acc.BillingCountryState__c; }
            else
            {
                if (accOld == null) { for (Account a : Trigger.old) { if (a.Id == acc.Id) { accOld = a; } } }
                if ((accOld == null) || (accOld.BillingState != acc.BillingState)) { acc.BillingCountryState__c = acc.BillingState; }
                acc.BillingState = acc.BillingCountryState__c; 
            }
        }
        if (acc.BillingCountry != acc.BillingCountry__c)
        {
            if (acc.BillingCountry == null) { acc.BillingCountry = acc.BillingCountry__c; }
            else
            {
                if (accOld == null) { for (Account a : Trigger.old) { if (a.Id == acc.Id) { accOld = a; } } }
                if ((accOld == null) || (accOld.BillingCountry != acc.BillingCountry)) { acc.BillingCountry__c = acc.BillingCountry; }
                acc.BillingCountry = acc.BillingCountry__c;
            }
        }
        if (acc.ShippingState != acc.ShippingCountryState__c)
        {
            if (acc.ShippingState == null) { acc.ShippingState = acc.ShippingCountryState__c; }
            else
            {
                if (accOld == null) { for (Account a : Trigger.old) { if (a.Id == acc.Id) { accOld = a; } } }
                if ((accOld == null) || (accOld.ShippingState != acc.ShippingState)) { acc.ShippingCountryState__c = acc.ShippingState; }
                acc.ShippingState = acc.ShippingCountryState__c;
            }
        }
        if (acc.ShippingCountry != acc.ShippingCountry__c)
        {
            if (acc.ShippingCountry == null) { acc.ShippingCountry = acc.ShippingCountry__c; }
            else
            {
                if (accOld == null) { for (Account a : Trigger.old) { if (a.Id == acc.Id) { accOld = a; } } }
                if ((accOld == null) || (accOld.ShippingCountry != acc.ShippingCountry)) { acc.ShippingCountry__c = acc.ShippingCountry; }
                acc.ShippingCountry = acc.ShippingCountry__c;
            }
        }
        if (acc.isPersonAccount__c)
        {
            if (acc.FirstName__c != null) { acc.Name = acc.FirstName__c + ' ' + acc.LastName__c; }
            else { acc.Name = acc.LastName__c; }
        }
    }
}