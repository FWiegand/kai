/* 
 * Trigger 
 * After a new order has been created this method initiates an asynchronous callout to the 
 * SAP system to create the service notification / order.
 */
trigger SCOrder_AI_CallSapWebService on SCOrder__c (after insert) 
{
    CCSettings__c ccset = CCSettings__c.getInstance();
    if ((ccset != null) && (ccset.IFEnableTriggerOrderCreate__c > 0))
    {
        for(SCOrder__c order : trigger.new) 
        {
            if (order.ERPAutoOrderCreate__c != 'C0-Active')
            {
                // Always to be executed asynchronously to ensure that all dependent objects (order items, ...) have be created 
                Boolean async = true;             // ccset.IFEnableTriggerOrderCreate__c == 2;
    
                if(order.Maintenance__c != null && order.Channel__c == SCfwConstants.DOMVAL_CHANNEL_FROM_CONTRACT_OR_MAINTENANCE)
                {   
                      // we can't use a future call in a future call (SCbtcMaintenanceOrderCreate) from jenkins (external job scheduler)
                      // so we have to move the callout to the SCbtcMaintenanceOrderCreate method after the order has been saved
                }    
                else if(order.ERPStatusOrderCreate__c == 'none' && (order.ERPOrderNo__c == null || order.ERPOrderNo__c == ''))
                {   
                    CCWCOrderCreate.callout(order.Id, async, false);
                }
            }//order.ERPAutoOrderCreate__c != 'C0-Active'
        }
    }
    else
    {
        System.debug('warning: SCOrder_AI_CallSapWebService trigger is disabled - see CCSettings.DISABLETRIGGER');    
    }
}