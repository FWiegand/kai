/*
    trigger to support CCEAG Admins (Kenneth.Hendler)
*/

trigger CCEAG_Admin_Usercheck on User (after insert, after update) {


    String body = '';
    String mailTo = 'kenneth.hendler@cceag.de';
    String subject = 'CCEAGQA :: User changed';
        
    try
    {
 //   String adminMail = CCEAG_Admin_Settings__c.getOrgDefaults().Admin_Mail_Adresse__c;
 //     System.debug('### mail to : ' + adminMail );
  
    Map<Id, UserRole> allUserRoles 
        = new Map<Id, UserRole>([Select a.Id, a.Name From UserRole a ]);
  
    Map<Id, Profile> allProfiles
        = new Map<Id, Profile>([Select Id, Name From Profile ]);
        
  if( Trigger.isInsert )
  {
      System.debug('### new user triggered');
      
      for( User u : Trigger.new )
      {
          body += 'new user: ' + u.Username + '\r\n';
      }
   } else if(Trigger.isUpdate)
   {
        
      String modifiedBy = null;
      User oldU = null;
      for( User u : Trigger.new )
      {
         modifiedBy = '' + u.LastModifiedById;
         body += '### Geändert von ID: \r\n';
         body += u.LastModifiedById + '\r\n';
         // an dieser Stelle müßtest du den User selektieren
         //
//             User mu = [Select Id, Name From User WHERE Id = u.LastModifiedById];
//              if( mu != null )
//                  modifiedBy = mu.Name;
//              else modifiedBy = 'noch unbekannter';
         
             body += '                  \r\n';
             body += 'Neuer Datensatz: ' + u + '\r\n';
             body += '                  \r\n';
         
          oldU = Trigger.oldMap.get(u.Id);
             body += 'Alter Datensatz: ' + oldU + '\r\n';
             body += '                  \r\n';
             
          if( oldU != null )
          {
           body += '####Start der Abfrageschleife !!! \r\n';
              body += '                  \r\n';
              body += 'Neuer Wert Mail: ' + u.Email + '\r\n';
              body += 'Alter Wert Mail: ' + oldU.Email + '\r\n';
              if( !u.Email.equals(oldU.Email) )
              {
                  body += 'Abfrage Änderung der Mailadresse \r\n';
                  body += 'user: ' + u.Username + ' has changed mail from: ' + oldU.Email + ' to: ' + u.Email + '\r\n';
              }
              body += 'Neuer Wert IsActive: ' + u.IsActive + '\r\n';
              body += 'Alter Wert IsActive: ' + oldU.IsActive + '\r\n';
              
              if( u.IsActive != oldU.IsActive )
              {
                  body += 'Abfrage Änderung der Useraktivierung \r\n';
                  body += 'user : ' + u.Username + ' was changed from: ' + oldU.IsActive + ' to: ' + u.IsActive + ' by ' + modifiedBy +'\r\n';    
              }
   
              body += 'Neuer Wert RoleID: ' + u.UserRoleId + '\r\n';
              body += 'Alter Wert RoleID: ' + oldU.UserRoleId + '\r\n'; 
                
              if( u.UserRoleId != oldU.UserRoleId )
              {
                    String ur = 'undefined';
                    if( allUserRoles.containsKey(u.UserRoleId)) ur = ((UserRole)allUserRoles.get(u.UserRoleId)).Name;
                    
                    String our = 'undefined';
                    if( allUserRoles.containsKey(oldU.UserRoleId)) our = allUserRoles.get(oldU.UserRoleId).Name;
                  
                  body += 'Neuer Wert von Role: ' + ur + '\r\n';
                  body += 'Alter Wert von Role: ' + our + '\r\n';  
                  body += 'user : ' + u.Username + ' has changed role from: ' + our + ' to : ' + ur + '\r\n';
                    
              }

              body += 'Neuer Wert Profil: ' + u.ProfileId + '\r\n';
              body += 'Alter Wert Profil: ' + oldU.ProfileId + '\r\n'; 
               
              if( u.ProfileId != oldU.ProfileId )
              {
                    String ur = 'undefined';
                    if( allProfiles.containsKey(u.ProfileId)) ur = allProfiles.get(u.ProfileId).Name;
                    
                    String our = 'undefined';
                    if( allProfiles.containsKey(oldU.ProfileId)) our = allProfiles.get(oldU.ProfileId).Name;
                    
                  body += 'user : ' + u.Username + ' has changed profile from: ' + our + ' to : ' + ur + '\r\n';
              }
 
             body += 'Neuer Wert ID2: ' + u.ID2__c + '\r\n';
             body += 'Alter Wert ID2: ' + oldU.ID2__c + '\r\n';  
             
             string uid2 = u.ID2__c;
              
             body += 'String uid2 enthält den Wert: ' + uid2 + '\r\n';
      
          try
          {    
              if( !u.ID2__c.equals(oldU.ID2__c) )
              {
                  body += 'Abfrage Änderung der ID2 \r\n';
                  body += 'A1 user : ' + u.Username + ' has changed ID2 from: ' + oldU.ID2__c + ' to: ' + u.ID2__c + '\r\n';
              } 
  
          } catch( Exception ex )
              {
                system.debug('Trigger :: Abfrage ID2 failed ! ' + ex.getStackTraceString());
              }
              
               if( uid2 == null )
              {
                  body += 'Abfrage Änderung der ID2 \r\n';
                  body += 'A2 user :' + u.Username + ' has changed ID2 to: ' + u.ID2__c + '\r\n';
              }  
              
              
              body += 'Neuer Wert FED-ID: ' + u.FederationIdentifier + '\r\n';
              body += 'Alter Wert FED-ID: ' + oldU.FederationIdentifier + '\r\n'; 
              
              string fedid = u.FederationIdentifier;
              
              body += 'String fedid enthält den Wert: ' + fedid + '\r\n'; 
             
           try
           {   
              if( !u.FederationIdentifier.equals(oldU.FederationIdentifier) )
              {
                  body += 'Abfrage Änderung der FED-ID \r\n';
                  body += 'A1 user : ' + u.Username + ' has changed Verbund-ID from: ' + oldU.FederationIdentifier + ' to: ' + u.FederationIdentifier + '\r\n';
              }
              
            } catch( Exception ex )  
              
            {
                system.debug('Trigger :: Abfrage FED-ID failed! ' + ex.getStackTraceString());
            }
            
              if( fedid == null )
              {
                  body += 'Abfrage Änderung der FED-ID \r\n';
                  body += 'A2 user :' + u.Username + ' has changed FED-ID to: ' + u.FederationIdentifier + '\r\n';
              } 
              
          }
      }
   }    
  } catch( Exception ex )
     {
         System.debug( 'Trigger :: Create_User failed! ' + ex.getStackTraceString() );
     }                       
// First, reserve email capacity for the current Apex transaction to ensure

// that we won't exceed our daily email limits when sending email after
                        
// the current transaction is committed.
Messaging.reserveSingleEmailCapacity(1);

// Processes and actions involved in the Apex transaction occur next,
// which conclude with sending a single email.

// Now create a new single email message object
// that will send out a single email to the addresses in the To, CC & BCC list.
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

// Strings to hold the email addresses to which you are sending the email.
String[] toAddresses = new String[] {mailTo}; 
// String[] ccAddresses = new String[] {'smith@gmail.com'};
  

// Assign the addresses for the To and CC lists to the mail object.
mail.setToAddresses(toAddresses);
// mail.setCcAddresses(ccAddresses);

// Specify the address used when the recipients reply to the email. 
// mail.setReplyTo('support@acme.com');

// Specify the name used as the display name.
mail.setSenderDisplayName('CCEAG Admin Salesforce');

// Specify the subject line for your email address.
mail.setSubject(subject);

// Set to True if you want to BCC yourself on the email.
mail.setBccSender(false);

// Optionally append the salesforce.com email signature to the email.
// The email address of the user executing the Apex Code will be used.
mail.setUseSignature(false);

// Specify the text content of the email.
mail.setPlainTextBody(body);

//mail.setHtmlBody('Your User:<b> ' + user.FirstName + ' </b>has been created.<p>'+
//     'To view your case <a href=https://na1.salesforce.com/' + user.ID + '>click here.</a>');

// Send the email you have created.
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}