/**
 * This trigger sets the activity flag to "U".
 * 
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCStockItemBeforeUpdate on SCStockItem__c (before update) 
{
	if (SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCStockAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
    
	for(Integer i = 0 ; i < Trigger.new.size(); i++ )
	{
		SCStockItem__c stockItemNew = Trigger.new.get(i);
		SCStockItem__c stockItemOld = Trigger.old.get(i);
		
		system.debug('StockItem New: ' + stockItemNew);
		system.debug('StockItem Old: ' + stockItemOld);
		//TODO if only qty changed do not set activity
		
		//Nothing to do if stock not modified
		// "==" -> equals
		// "===" -> reference equals
		if(stockItemNew == stockItemOld)
		{
			system.debug('StockItems are equal: ' + stockItemNew);
			continue;
		}
		
		//ignore trigger on reset activity
		// U -> NULL; I -> NULL; Q -> NULL
		if
		(	stockItemNew.Activity__c == NULL && 
			(
				//if activity changed from U - > Q
				stockItemOld.Activity__c == 'U' ||
				stockItemOld.Activity__c == 'I' ||
				stockItemOld.Activity__c == 'Q' 
			)
		)
		{
			continue;
		}
		
		//disable activity update if already set to 'I'
		if(stockItemOld.Activity__c == 'I' || stockItemOld.Activity__c == 'Q')
		{
			stockItemNew.Activity__c = stockItemOld.Activity__c;
			
		}
		//Set activity to 'U' if not set to 'Q' and qty__c not changed
		//qty__c changes only by booking material movements
		else if(stockItemNew.Activity__c != 'Q'  && stockItemNew.Qty__c == stockItemOld.Qty__c)
		{
			stockItemNew.Activity__c = 'U';
		}
	}
	 
}