/**
 * @(#)SCAssignmentAfterUpdate.cls    ASE1.0 hs 15.10.2010
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: SCAssignmentAfterUpdate.cls 7692 2010-10-15 16:18:41Z hschroeder $
 */
trigger SCAssignmentAfterUpdate on SCAssignment__c (after update) 
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAssignmentAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAssignment.afterUpdate(Trigger.new, Trigger.old);
    }
}