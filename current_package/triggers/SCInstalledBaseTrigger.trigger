trigger SCInstalledBaseTrigger on SCInstalledBase__c bulk (before insert,before update,after update) {
    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        SCInstalledBaseMethods.doBeforeInsertUpdate(Trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        SCInstalledBaseMethods.doAfterUpdate(Trigger.new);
    }

}