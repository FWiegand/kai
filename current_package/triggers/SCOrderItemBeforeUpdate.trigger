/**
 * HOTFIX GMSNA 01.50.2013
 * This trigger prevents that the record type is reset unexpectedly to null by the mobile gateway 
 * when uplaoding order items. Important for CCEAG orders that are processed on the mobile system
 * and have to trigger the EQ update interface 
 */
trigger SCOrderItemBeforeUpdate on SCOrderItem__c (before update) 
{
    if(SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCOrderItemBeforeUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
    // determine the record types so that we can autocorrect the order item
    List<RecordType>recordtypes = [Select Id, DeveloperName from RecordType where SobjectType = 'SCOrderItem__c'];
    String recordtypeid;
    
    String rtEQ;
    String rtSU;
    for(RecordType rt : recordtypes)
    {
        if(rt.DeveloperName == 'Equipment')
        {
            rtEQ = rt.id;
        }
        else if(rt.DeveloperName == 'SubEquipment')
        {
            rtSU = rt.id;
        }
    }
        
    
    for(Integer i = 0 ; i < Trigger.new.size(); i++ )
    {
        SCOrderItem__c itemNew = Trigger.new.get(i);
        SCOrderItem__c itemOld = Trigger.old.get(i);
        
        system.debug('Order item New: ' + itemNew);
        system.debug('Order item Old: ' + itemOld);
        
        // Nothing to do if records are not modified
        // "==" -> equals
        // "===" -> reference equals
        if(itemNew == itemOld && itemNew.RecordTypeId != null)
        {
            system.debug('Order items are equal: ' + itemNew);
            continue;
        }
        
        // Do not allow to reset the record type to null - kepp the old record type
        // This is currently (01.05.2013) required as the mobile system marks even unchanged order
        // items with "U" for update and the gateway does not handle the recordtype separately
        if(itemNew.RecordTypeId == null && itemOld.RecordTypeId != null)
        {
            itemNew.RecordTypeId = itemOld.RecordTypeId;
        }
        // If no record type exists then set the record type to "Equiment" or "Subequipment" depending on the installed base
        else if(itemNew.RecordTypeId == null && itemOld.RecordTypeId == null)
        {
            if(itemNew.cce_EquipmentTypeFilter__c == 'SU')
            {
                itemNew.RecordTypeId = rtSU;
            }        
            else
            {
                // if nothing else is defined use EQ as default
                itemNew.RecordTypeId = rtEQ;
            }
        }
    } // for...
}