trigger SCAppointmentAfterUpdate on SCAppointment__c (after update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset != null && appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAppointmentAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAppointment boAppointment = new SCboAppointment();
        boAppointment.AfterUpdate(Trigger.new, Trigger.old);
    }
}