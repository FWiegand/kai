/*
 * @(#)SCContractAfterUpdate
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This trigger updates the contract itmes of the updated contracts.
 * So the field ContractRef__c in the installed base will be updated
 * by the trigger SCContractItemAfterUpsert.
 *
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCContractAfterUpdate on SCContract__c (after update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCContractAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        List<Id> contrIds = new List<Id>();
        
        for (Integer cnt=0; cnt<Trigger.new.size(); cnt++)
        {
            // only if the status, the start date or the end date is changed in the contract
            // add it to the list
            if ((Trigger.old.get(cnt).Status__c != Trigger.new.get(cnt).Status__c) || 
                (Trigger.old.get(cnt).StartDate__c != Trigger.new.get(cnt).StartDate__c) || 
                (Trigger.old.get(cnt).EndDate__c != Trigger.new.get(cnt).EndDate__c))
            {
                contrIds.add(Trigger.new.get(cnt).Id);
            }
        }
        
        if (contrIds.size() > 0)
        {
            // read all contract items for the contracts from the list
            List<SCContractItem__c> items = [Select Id, Name, InstalledBase__c from SCContractitem__c where Contract__c in :contrIds];
            // and update them, this called the trigger SCContractItemAfterUpsert
            // and updates the fields ContractRef__c and ContractStart__c in the installed base
            update items;
        }
    }
}