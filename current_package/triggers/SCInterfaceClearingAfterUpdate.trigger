trigger SCInterfaceClearingAfterUpdate on SCInterfaceClearing__c (after update) 
{
    if (SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCInterfaceClearingAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
    system.debug('#### SCInterfaceClearingAfterUpdate. Try to save imported data. Ids: ' + Trigger.newMap.keySet());
    
    Set<Id> idsSync = new Set<Id>();
    Set<Id> idsASync = new Set<Id>();
    for(SCInterfaceClearing__c clearing : Trigger.new)
    {
        if(clearing.Status__c.equals(SCboInterfaceClearing.STATUS_PENDING))
        {
            
            if( clearing.Type__c.equals(SCboInterfaceClearing.TYPE_ORDERASSIGNMENT) ||  
                clearing.Type__c.equals(SCboInterfaceClearing.TYPE_MOBILEORDER)     ||
                clearing.Type__c.equals(SCboInterfaceClearing.TYPE_MATERIALMOVEMENT)) 
            {
                idsSync.add(clearing.Id);
            }
            else
            {
                idsAsync.add(clearing.Id);
            }
        }
    }
    if(idsAsync.size() > 0)
    {
        SCboInterfaceClearing.afterUpdateAsync(idsAsync); 
    }
    
    if(idsSync.size() > 0)
    {
        SCboInterfaceClearing.afterUpdateSync(idsSync);
    }

}