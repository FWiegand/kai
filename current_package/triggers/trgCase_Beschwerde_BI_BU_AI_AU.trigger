// /* **************************************************************
// Created by: Chris Sandra Schautt
// Description: Trigger on Cases to 
//              1. delate all the Beschwerde__c Obj before updating.
//              2. create new Beschwerde__c Obj for given Picklist values only if the Piklist values are not empty
// Testclass: trgCase_Beschwerde_Test
// Last modified by: Alexander Faust
// Last modified date: 07.03.2013
// Latests changes: 06.03.2013 enhanced trigger logic. no hardcoded fields, used ComplaintFields custom setting instead.
//					07.03.2013 created testClass
// ************************************************************** */


trigger trgCase_Beschwerde_BI_BU_AI_AU on Case (after insert, after update, before insert, before update) {
    list<Case> listCase = Trigger.new;
    Id [] ListCaseId = new List<Id>();
    list<Beschwerde__c> listDeleteBeschwerde = new list<Beschwerde__c>();
    Beschwerde__c [] listNewBeschwerde = new list<Beschwerde__c>();
    
    for(Integer i = 0; i<listCase.size();i++){
        ListCaseId.add(listCase[i].Id);
    }
    
    //
    
    if(Trigger.isBefore && !Trigger.isInsert){
    
        
        for( Case tempCase:listCase){
            listDeleteBeschwerde  =[Select Id, Name, Kundenvorgang__c FROM  Beschwerde__c WHERE Kundenvorgang__c IN: ListCaseId];
        }// End for( Case tempCase:listCase)
    
        
        if(listDeleteBeschwerde.size()>0){  
                delete listDeleteBeschwerde;
        }//End if(listDeleteBeschwerde.size()>0)
    }//end if if(Trigger.isBefore && !Trigger.isInsert)
    
    
    // if the Case is already insert, then create Beschwerde__c OBJ
    if((Trigger.isAfter)&&(Trigger.isUpdate||Trigger.isInsert)){
        Beschwerde__c tempBeschwerde= new Beschwerde__c();
        //String [] listName = new list<String>();
        
        //custom setting vodoo starts here
        
        //get API Names of all fields to check in custom setting
        List<ComplaintFields__c> listFieldsToCheck = ComplaintFields__c.getAll().values();
     
        
        for(Integer i= 0; i<listCase.size();i++){
            
            for(ComplaintFields__c c: listFieldsToCheck){
            
            	if(listCase[i].get(c.ComplaintFieldName__c) != null && listCase[i].get(c.ComplaintFieldName__c) != ''){
            		List<String> listName = String.valueOf(listCase[i].get(c.ComplaintFieldName__c)).split(';');
            		
            		if(listName.size()>0){
	                    
	                    for(Integer stringIndex = 0;stringIndex <listName.size();stringIndex++ ){
	                        tempBeschwerde.Name = listName[stringIndex ];
	                        tempBeschwerde.Additional_Info__c = c.Additional_Info__c;
	                        tempBeschwerde.Kundenvorgang__c= listCase[i].Id;
	                        listNewBeschwerde.add(tempBeschwerde);
	                        tempBeschwerde= new Beschwerde__c();
	                    }// end for
	                    //listName = new list<String>();
                	}//End if(s.size()>0)
            	}//END if(listCase[i].get(c.ComplaintFieldName__c) != null && listCase[i].get(c.ComplaintFieldName__c) != ''
            }//END for(ComplaintFields__c c: listFieldsToCheck)
            
         //custom setting vodoo ends here
          
        }// End for(Integer i= 0; i<listCase.size();i++)
        
        if(listNewBeschwerde.size()>0){          
        	insert listNewBeschwerde;
        }// End if(listNewBeschwerde.size()>0)
    }// end if(Trigger.isAfter)
}// end Trigger