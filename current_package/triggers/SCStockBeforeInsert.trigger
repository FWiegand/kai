/**
 * This trigger sets the activity flag to "I".
 * 
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCStockBeforeInsert on SCStock__c (before insert) 
{
	if (SCApplicationSettings__c.getInstance().DISABLETRIGGER__c)
    {
        System.debug('warning: SCStockAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');
        return;    
    }
    
	for(SCStock__c stock : Trigger.new)
	{
		stock.Activity__c = 'I';
	}
	 
}