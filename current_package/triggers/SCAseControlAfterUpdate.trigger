/*
 * @(#)SCAseControlAfterUpdate.cls SCCloud    hs 27.04.2011
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date
 */
 trigger SCAseControlAfterUpdate on SCAseControl__c (after update) {
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAseControlAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        List<SCAseControl__c> oldItems = Trigger.old; 
        List<SCAseControl__c> newItems = Trigger.new; 
        
        for (Integer i = 0; i < newItems.size(); i++)
        {
            SCAseControl__c newItem = newItems.get(i);    
            SCAseControl__c oldItem = oldItems.get(i);

            // transition U -> null, job completed, send email
            if (oldItem.activity__c == 'U' && (newItem.activity__c == '' || newItem.activity__c == null))
            {
                // determine template
                List<EmailTemplate> templateList = [ select id, developername from EmailTemplate where developername = 'ASE_Status_E_Mail' ];
                
                User user = [ select id, email from user where id = :oldItem.createdById limit 1];
                // determine ase contact
                Contact contact = null;
                List<Contact> items = [select id, email from contact where lastname = 'Advanced Scheduling Engine'];

                if (items.size() == 0)
                {
                    contact = new Contact();
                }
                else
                {
                    contact = items[0];
                }
                 
                contact.lastname = 'Advanced Scheduling Engine';
                contact.email = user.email;
                Database.UpsertResult upsertResult = Database.upsert(contact);
                System.debug(upsertResult);
        
                String contactID = contact.id;
                
                if (contact.id == null)
                {
                    contactID = upsertResult.getId();
                }
                    
                // inform per email about the ASE job status
                for (EmailTemplate template : templateList)
                {
                    System.debug('Job ' + oldItem.name + ' finished');
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(contactID);
                    mail.setTemplateId(template.id);
                    mail.setSaveAsActivity(false);
                    mail.setEmailPriority('High');
                    mail.setWhatId(oldItem.id);
                    
                    try
                    {
                        List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
                        for (Messaging.SendEmailResult result : results)
                        {
                            if (!result.IsSuccess())
                            {
                                List<Messaging.SendEmailError> emailErrors = result.getErrors();
                                
                                for (Messaging.SendEmailError emailError : emailErrors)
                                {
                                    System.debug('Send Email failed with error: ' + emailError); 
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        System.debug(e);
                    }
                }
            }
        }
    }
}