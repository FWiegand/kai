trigger SCAppointmentBeforeUpdate on SCAppointment__c (before update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset != null && appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAppointmentBeforeUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAppointment boAppointment = new SCboAppointment();
        boAppointment.BeforeUpdate(Trigger.new, Trigger.old);
    }
}