trigger SCMaterialMovementAfterUpdate on SCMaterialMovement__c (after update)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCMaterialMovementAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboMatStatUpdate matStatUpdate = new SCboMatStatUpdate();
        matStatUpdate.AfterUpdate(Trigger.new, Trigger.old);
    }
}