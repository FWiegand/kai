//### Quickfix only required until the mobil system sets the delivery stock 
trigger SCMaterialMovementBeforeInsert on SCMaterialMovement__c (before insert) 
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCMaterialMovementBeforeInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        beforeInsert(Trigger.new);
    }
    
    public void beforeInsert(List<SCMaterialMovement__c> items)
    {
        // first determine all stocks where the delivering plant is missing
        Map<String, String> stocks = new Map<String, String>();
        for(SCMaterialMovement__c  mov : items)
        {
            // check for reservations if the delivering stock is missing
            if(mov.type__c == SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL 
               // not relevant || mov.type__c == SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION
            )              
            {
                if(mov.source__c == 'mobile')
                {
                    // TODO GMSNA 09.04.2013 BUGFIX: Mobile system sets the deliverystock to itself - error
                    if(mov.DeliveryStock__c == mov.stock__c)
                    {
                        // we reset the stock so that it will be determined from the resource assignment
                        mov.DeliveryStock__c = null;
                    }
                    if(mov.DeliveryStock__c == null)
                    {
                        stocks.put(mov.stock__c, ''); // stock, deliveringstock
                    }
                }
    
                // ensure that the requisition date is available (required by SAP)
                if(mov.RequisitionDate__c == null)
                {
                    mov.RequisitionDate__c = date.Today();
                }
            }            
        }                    
        if(stocks.size() > 0)
        {
            // now read the delivering stock from the resource assignment and 
            List<SCResourceAssignment__c> ras = [select id, name, stock__c, department__r.stock__c, 
                                                 department__r.stock__r.name,validfrom__c, validto__c from SCResourceAssignment__c where 
                                                stock__c in :stocks.keySet() and validfrom__c < TODAY and validto__c >= TODAY];
            if(ras != null && ras.size() > 0)
            {                                    
                for(SCResourceAssignment__c ra : ras)
                {
                    stocks.put(ra.stock__c, ra.department__r.stock__c); // stock, deliveringstock    
                }
            }
            for(SCMaterialMovement__c  mov : items)
            {
                if(mov.type__c == SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL 
                   // not relevant ||mov.type__c == SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION
                   )
                { 
                    // ensure that the deliering stock is always set
                    if(mov.source__c == 'mobile' && mov.DeliveryStock__c == null)
                    {
                        String delivstock = stocks.get(mov.stock__c);
                        if(delivstock != null)
                        {
                            mov.DeliveryStock__c = delivstock;
                        }
                    }
                }    
            }                   
        }            
    }
}