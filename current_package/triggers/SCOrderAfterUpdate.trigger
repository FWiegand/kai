/**
 * This trigger sets the info fields for the last service order.
 * 
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCOrderAfterUpdate on SCOrder__c (after update) 
{
    Map<String, String> mapStats = new Map<String, String>();
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_OPEN, 'schedule');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED, 'scheduled');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED, 'scheduled');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED, 'completed');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED, 'skip');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL, 'completed');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK, 'scheduled');
    mapStats.put(SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING, 'scheduled');

    // for setting SCMaintenance__c.Order__c = null for completed orders
    List<String> finishedOrderStatusList = new List<String>();
    finishedOrderStatusList.add(SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED);
    finishedOrderStatusList.add(SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED);
  
    
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if (appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCOrderAfterUpdate trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else if (SCboOrder.ignoreTrigger)
    {
        // prevent loop if the trigger updates data that will retrigger an update
        return;
    }
    else
    {
        //###### CCE ###### -->
        List<SCOrder__c> updlongtexts = new List<SCOrder__c>();
        Map<String, SCOrder__c> updlongtextIds = new Map<String, SCOrder__c>();
        //###### CCE ###### <--
        
        
        List<Id> orderIds = new List<Id>();
        Map<Id, String> orderStats = new Map<Id, String>();
        for (SCOrder__c order :trigger.new) 
        {
            //###### CCE ###### -->
            SCOrder__c old = Trigger.oldMap.get(order.id);
            if(old.ERPLongtext__c != order.ERPLongtext__c && order.ERPStatusOrderClose__c == 'none')
            {
                // field was updated, mark for update
                //### GMSNA 24.05.2013 
                //### updlongtexts.add(order);                
                //### updlongtextIds.put(order.id, order);
            }     
            //###### CCE ###### <--
        
            // only set the fields at service orders
            if (('Service' == order.UsedBy__c) && (SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT == order.Type__c))
            {
                orderIds.add(order.Id);
            } 
            
            // Prepare the update of contract and maintenances
            // if we have a service order and the status is set, add the order to the map (exclude all sales orders!)
            if (('Service' == order.UsedBy__c) && SCBase.isSet(order.Status__c))
            {
                orderStats.put(order.Id, order.Status__c);
            } 
        } // for (SCOrder__c order..
        
        // update the account counters (with the repair orders)
        if (orderIds.size() > 0)
        {
            SCboOrder.setLastOrderInfosEx(orderIds);
        }
        
        // process all service relavant orders and check if contract visits have to be updated
        if (orderStats.size() > 0)
        {
            // read all contract visits for the orders
            List<SCContractVisit__c> visits = [select Id, Order__c, Order__r.Closed__c, Status__c from SCContractVisit__c 
                                                where Order__c in :orderStats.keySet()];
            // update the order status inside the contract visit
            for (SCContractVisit__c item :visits)
            {
                item.Status__c = mapStats.get(orderStats.get(item.Order__c));
                // GMSAW: for safety do this only, if we change only a few ordersm that is ok
                if (('completed' == item.Status__c) && (orderStats.size() < 5))
                {
                    SCboOrder boOrd = new SCboOrder();
                    boOrd.readById(item.Order__c);
                    if (SCBase.isSet(boOrd.order.Contract__c))
                    {
                        boOrd.updateContractInfosEx(item.Id);
                    }
                }
            }
            update visits;
            
            
            //--<CCE Mainteance>---------------------------------------------------
            // Read all maintenances accociated with this orders and update the status in the maintenace object            
            // for already finished order delete the order info from maintenances and reset the resulats of the SCbtcMaintenanceDueDateSet job
            List<SCMaintenance__c> maintenances = [select Id, Order__c, Order__r.Closed__c from SCMaintenance__c 
                                                   where  Order__c in :orderStats.keySet() and Order__r.Status__c in : finishedOrderStatusList];

            List<ID> maintenanceToCalculateIdList = new List<ID>();                                            
            for(SCMaintenance__c item: maintenances)
            {
                // 1. completed -> new appointment
                String ordstat = orderStats.get(item.Order__c);
                
                if(ordstat != null && ordstat == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED)
                {
                    if(item.Order__r.Closed__c != null) 
                    {
                        maintenanceToCalculateIdList.add(item.Id);
                        item.MaintenanceLastDate__c = item.Order__r.Closed__c.date();
                        item.MaintenanceDateProposal__c = null;
                        item.Order__c = null;
                        item.OrderStatus__c = null;
                        item.Pricelist__c = null;
                        item.MaintenanceInCalendarYear__c = null;
                    }    
                }
                // cancelled
                else if(ordstat != null && ordstat == SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED)
                {
                    // the previously calculated appointment remains. Only the order information are removed to allow
                    // the renew generation of an order for the same appointment.
                    item.Order__c = null;
                    item.OrderStatus__c = null;
                }
                // If there is a staus equals to closed final the controlling is already run by the handling of the status completed.
            }
            update maintenances;
            if(maintenanceToCalculateIdList.size() > 0)
            {
                SCbtcMaintenanceDueDateSet.asyncCalculateList(maintenanceToCalculateIdList); 
            }
        }
    
    
    
        // now create the scmessage objects (used for informing the customer)
        SCboOrder boOrder = new SCboOrder();
        boOrder.AfterUpdate(Trigger.new, Trigger.old);

        //###### CCE ###### -->

        if(updlongtexts.size() > 0)
        {
            String oid = null;
            List<SCOrderItem__c> items = [select Id, order__r.id, RecordTypeId from SCOrderItem__c where Order__c in :updlongtextIds.keySet()];
            for(SCOrderItem__c i :items)
            {
                SCOrder__c o = updlongtextIds.get(i.order__r.id);    
                i.ErrorText__c = o.ERPLongText__c;    
                
                //### tmp demo only
                oid = i.order__r.id;
                break;
            }
            update items;
            if(updlongtextIds != null)
            {
                CCWCOrderEquipmentUpdate.callout(oid, true, false);
            }
        }
     
        // Delete or modify maintenaces of orders
        // The function deletes maintenances of orders with Type = 5704 ZC09-Return having status read from the CCSettings.MantenanceDeleteOnReturn__c.
        // If the setting is empty the status 5508 closed final is taken as default status.
        // Only orders with channel DOMVAL_CHANNEL_FROM_CONTRACT_OR_MAINTENANCE are regarded.
        
        //	
        //  In ZC16-Austausch (5711) [DOMVAL_ORDERTYPE_REPLACEMENT] the maintenances will be moved from old installed base to new installed base.
     
        SCboOrder.changeMaintenance(trigger.new);   
        //###### CCE ###### <--
        
    }
}