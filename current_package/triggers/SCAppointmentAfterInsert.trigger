trigger SCAppointmentAfterInsert on SCAppointment__c (after insert)
{
    SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCAppointmentAfterInsert trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        SCboAppointment boAppointment = new SCboAppointment();
        boAppointment.AfterInsert(Trigger.new);
    }
}