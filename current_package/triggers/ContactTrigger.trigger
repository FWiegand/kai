/*
Creater: Alexander Placidi
Description: Deletes all the PrimarySalesPerson-Flags of the contacts from the associated account when a new primary-sales-contact is coming. 
			 If more than one of the incoming contacts have the primary-sales flag, only the first contact in the list stay primary.
			 Uses the ContactAvoidTriggerRecursion class to prevent the trigger from calling himself. This occurs, because old contacts must be updated. 
Object: Contact
TestClass: ContactsPrimarySales
Last Modified: 20.01.2014
*/
trigger ContactTrigger on Contact (before insert, before update) {

	if(ContactAvoidTriggerRecursion.runOnce()) {
	 	if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
	 		ContactMethods.onInsertUpdate(Trigger.New);
	 	}
	 		
	 }
	 
	 
}