trigger SCInstalledBaseBeforeInsertUpdate on SCInstalledBase__c (before insert, before update) {

    //  ProductModel ID, ProductModel
    Map<Id, SCProductModel__c> productModels = new Map<Id, SCProductModel__c>();
    
    // prepare a list of all required product models 
    for (SCInstalledBase__c ib : Trigger.new )
    {
        if(ib.ProductModel__c != null)
        {
            productModels.put(ib.ProductModel__c, null);
        }
    }
    if(productModels.size() > 0)
    {
        // now read the product model details 
        for (SCProductModel__c product : [select Id, Group__c, UnitClass__c, 
                                                 UnitType__c, Brand__c, Power__c
                                            from SCProductModel__c
                                           where Id in :productModels.keySet()])
        {
            productModels.put(product.Id, product);
        }
    
        // fill the fields with the product model data
        for (SCInstalledBase__c ib : Trigger.new )
        {
            if (productModels.containsKey(ib.ProductModel__c))
            {
                SCProductModel__c product = productModels.get(ib.ProductModel__c);
                
                if (product == null)
                {
                    continue;
                }
                
                ib.ProductUnitClass__c = product.UnitClass__c;
                ib.ProductUnitType__c  = product.UnitType__c;
                ib.Brand__c         = product.Brand__c;
                ib.ProductGroup__c  = product.Group__c;
                ib.ProductPower__c  = product.Power__c;
            }
        }
    }
}