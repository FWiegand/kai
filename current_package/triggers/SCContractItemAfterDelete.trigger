/*
 * @(#)SCContractItemAfterDelete
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This trigger updates the field contractRef__c in the installed base,
 * which contains this contract item.
 *
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
trigger SCContractItemAfterDelete on SCContractItem__c (after delete)
{
   SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
    if(appset.DISABLETRIGGER__c)
    {
        System.debug('warning: SCContractItemAfterDelete trigger is disabled - see SCApplicationSettings.DISABLETRIGGER');    
    }
    else
    {
        for (Integer cnt=0; cnt<Trigger.old.size(); cnt++)
        {
            SCboInstalledBase.updateContractRef(Trigger.old.get(cnt).InstalledBase__c);
        }
    }
}