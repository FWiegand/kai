trigger AccountBeforeInsert on Account (before insert) {
    for (Account acc : Trigger.new) {
        if ((acc.BillingCountryState__c != null) && (acc.BillingState != acc.BillingCountryState__c))
            { acc.BillingState = acc.BillingCountryState__c; }
        if ((acc.BillingCountry__c != null) && (acc.BillingCountry != acc.BillingCountry__c))
            { acc.BillingCountry = acc.BillingCountry__c; }
        if ((acc.ShippingCountryState__c != null) && (acc.ShippingState != acc.ShippingCountryState__c))
            { acc.ShippingState = acc.ShippingCountryState__c; }
        if ((acc.ShippingCountry__c != null) && (acc.ShippingCountry != acc.ShippingCountry__c))
            { acc.ShippingCountry = acc.ShippingCountry__c; }
        if (acc.LastName__c != null)
        {
            acc.isPersonAccount__c = true;
            if (acc.FirstName__c != null) { acc.Name = acc.FirstName__c + ' ' + acc.LastName__c; }
            else { acc.Name = acc.LastName__c; }
        }
    }
}