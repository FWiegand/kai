/*
 * @(#)WSboOrderTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Basic test of the web service api for SCboOrder
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class WSboOrderTest
{
    /**
     * Tests the cancel assignment methods
     */
    static testMethod void test1CancelAssignments()
    {
        SCHelperTestClass.createOrderTestSet3(true);        
        Test.startTest();

        String res = WSboOrder.canCancelAssignments(SCHelperTestClass.order.Id);
        System.assert(res.startsWith('OK:'));

        WSboOrder.cancelAssignments(SCHelperTestClass.order.Id, 'cancelled by me');

        Test.stopTest();

        // now count the cancelled (DOMVAL_ORDERSTATUS_CANCELLED) assignments
        Integer cancelledItems = database.countquery('SELECT count() from from SCAssignment__c where order__c = :SCHelperTestClass.order.Id and status__c = \'5507\'' );
        System.Assert(cancelledItems > 0);
    }

    /**
     * Tests the cancel order methods
     */
    static testMethod void test2CancelOrderAndAssignments()
    {
        SCHelperTestClass.createOrderTestSet3(true);        
        Test.startTest();

        // first check if we can cancel the order
        String res = WSboOrder.canCancelOrder(SCHelperTestClass.order.Id);
        System.debug('WSboOrder.canCancelOrder:' + res);
        System.assert(res.startsWith('OK'));

        // now cancel the order
        WSboOrder.cancelOrder(SCHelperTestClass.order.Id, '286001', 'cancelled by me');    

        // as the order is already cancelled
        res = WSboOrder.canCancelOrder(SCHelperTestClass.order.Id);
        System.debug('WSboOrder.canCancelOrder:' + res);
        System.assert(!res.startsWith('OK'));

        Test.stopTest();
        
        // the order status must be "cancelled"       
        SCOrder__c ord = [select status__c from SCOrder__c where id = :SCHelperTestClass.order.Id];
        System.AssertEquals(SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED, ord.status__c);

        // and all assignments/appointments must be cancelled
        // now count the cancelled (DOMVAL_ORDERSTATUS_CANCELLED) assignments
        Integer cancelledItems = database.countquery('SELECT count() from from SCAssignment__c where order__c = :SCHelperTestClass.order.Id and status__c = \'5507\'' );
        System.Assert(cancelledItems > 0);
    }
}