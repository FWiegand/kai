/*
 * @(#)SCboOrderLogTest.cls
 *  
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboOrderLogTest
{
    static testMethod void testOrderLog()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);
        
        SCboOrderLog boLog = new SCboOrderLog();

        // try to insert a text which is to long for the database field
        String info = 'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.' + 
                      'This is a very very very very very very very very very very very very very very very very long text.';
        System.assertEquals(true, info.length() > 2000);
                
        SCboOrderLog.createLog(SCHelperTestClass.order.Id, '11735', '', info);
        
        List<SCOrderLog__c> logs = [Select Id, Name from SCOrderLog__c where SCOrderId__c = :SCHelperTestClass.order.Id];
        System.assertEquals(1, logs.size());
    }
}