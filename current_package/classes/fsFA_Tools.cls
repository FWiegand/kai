/**
* @author           Oliver Preuschl
*                   H&W Consult GmbH
*                   Bahnhofstr. 3
*                   21244 Buchholz i.d.N.
*                   http://www.hundw.com
*
* @description      Provides several tools that are often needed.
*
* @date             04.02.2013 
*
* Timeline:
* Name              DateTime                Version        Description
* Oliver Preuschl   04.02.2013-09:00(GMT+1) *1.0*          Created the class
*/

public class fsFA_Tools{

    public fsFA_Tools(){
    
    }

    //RecordTypes
    private static Map<String, Map<String, Id>> GM_RecordTypes;
    
    
    /*//Get RecordType for specified Object
    public static Id getRecordTypeId(String PV_ObjectName, String PV_DeveloperName){
        //Initialize Map
        if (GM_RecordTypes == null){
            GM_RecordTypes = new Map<String, Map<String, Id>>();
        }
            
        //Get Record Types
        if(GM_RecordTypes.get(PV_ObjectName) == null){
            Map<String, Id> LM_ObjectRecordTypes = new Map<String, Id>();
            for (RecordType LO_RecordType: [Select Id, DeveloperName From RecordType Where (SObjectType =: PV_ObjectName)]){
                LM_ObjectRecordTypes.put(LO_RecordType.DeveloperName, LO_RecordType.Id);
            }
            GM_RecordTypes.put(PV_ObjectName, LM_ObjectRecordTypes);
        }
            
        return GM_RecordTypes.get(PV_ObjectName).get(PV_DeveloperName);
    }

    //Get RecordType for specified Object
    public static Set<Id> getRecordTypeIds(String PV_ObjectName, String PV_DeveloperName){
        //Initialize Map
        if (GM_RecordTypes == null){
            GM_RecordTypes = new Map<String, Map<String, Id>>();
        }
            
        //Get Record Types
        if(GM_RecordTypes.get(PV_ObjectName) == null){
            Map<String, Id> LM_ObjectRecordTypes = new Map<String, Id>();
            for (RecordType LO_RecordType: [Select Id, DeveloperName From RecordType Where (SObjectType =: PV_ObjectName)]){
                LM_ObjectRecordTypes.put(LO_RecordType.DeveloperName, LO_RecordType.Id);
            }
            GM_RecordTypes.put(PV_ObjectName, LM_ObjectRecordTypes);
        }
            
        Set<Id> LL_Ids = new Set<Id>();
        for (String LV_Name: GM_RecordTypes.get(PV_ObjectName).KeySet()){
            Id LV_Id = GM_RecordTypes.get(PV_ObjectName).get(LV_Name);
            if (LV_Name.startsWith(PV_DeveloperName)){
                LL_Ids.add(LV_Id);
            }
        }
        
        return LL_Ids;
    }*/
            
    //Get a record with all available fields for a given id
    public static SObject getRecord(Id PV_Id){
        //Get Object Type
        Schema.SObjectType LO_ObjectType = PV_Id.getSObjectType();
        //Get Object Name
        String LV_ObjectName = LO_ObjectType.getDescribe().getName();
        //Get Field Names
        Set<String> LL_Fields = LO_ObjectType.getDescribe().fields.getMap().keySet();
        
        //Generate Field String
        String LV_FieldString = '';
        String LV_Separator = '';
        for(String LV_Field: LL_Fields){
            LV_FieldString += LV_Separator + LV_Field;
            if (LV_Separator == '') LV_Separator = ',';
        }
        
        //Get record with all fields
        String LV_QueryString = 'SELECT ' + LV_FieldString + ' FROM ' + LV_ObjectName + ' WHERE Id = \'' + PV_Id + '\'';
        SObject LO_Record = Database.query(LV_QueryString);
        
        return LO_Record;
    }
    
    /*//Clone a record with all available fields for a given id
    public static SObject cloneRecord(Id PV_Id){
        //Get Object Type
        Schema.SObjectType LO_ObjectType = PV_Id.getSObjectType();
        //Get Object Name
        String LV_ObjectName = LO_ObjectType.getDescribe().getName();
        //Get Field Names
        Set<String> LL_Fields = LO_ObjectType.getDescribe().fields.getMap().keySet();
        
        //Generate Field String
        String LV_FieldString = '';
        String LV_Separator = '';
        for(String LV_Field: LL_Fields){
            LV_FieldString += LV_Separator + LV_Field;
            if (LV_Separator == '') LV_Separator = ',';
        }
        
        //Get record with all fields
        String LV_QueryString = 'SELECT ' + LV_FieldString + ' FROM ' + LV_ObjectName + ' WHERE Id = \'' + PV_Id + '\'';
        SObject LO_Record = Database.query(LV_QueryString);
        
        //Clone record
        SObject LO_RecordClone = LO_Record.clone(false, true);
        
        return LO_RecordClone;
        
    }*/
    
    //Get all records of type PV_ObjectName where Field PV_ParentName = PV_ParentId
    public static Map<Id, SObject> getRecordsFlat(String PV_ObjectName, String PV_ParentIdName, Id PV_ParentId){
        Map< Id,  Map<Id, SObject> > LM_RecordsMap = getRecords(PV_ObjectName, PV_ParentIdName, new List<Id>{PV_ParentId} );
        return flattenIdObjectMap( LM_RecordsMap );
    }

	/*//Get all records of type PV_ObjectName where Field PV_ParentName = PV_ParentId
    public static Map<Id, SObject> getRecordsFlat(String PV_ObjectName, String PV_ParentIdName, Id PV_ParentId, String PV_SortField){
        Map< Id,  Map<Id, SObject> > LM_RecordsMap = getRecords(PV_ObjectName, PV_ParentIdName, new List<Id>{PV_ParentId} );
        return flattenIdObjectMap( LM_RecordsMap );
    }

    //Get all records of type PV_ObjectName where Field PV_ParentName = PV_ParentId
    public static Map< Id,  Map<Id, SObject> > getRecords(String PV_ObjectName, String PV_ParentIdName, Id PV_ParentId){
        return getRecords(PV_ObjectNAme, PV_ParentIdName, new List<Id>{PV_ParentId} );
    }

    //Get all records of type PV_ObjectName where Field PV_ParentName is in PV_ParentId
    public static Map< Id, Map<Id, SObject> > getRecords(String PV_ObjectName, String PV_ParentIdName, Set<Id> PS_ParentIds){
        List<Id> LL_ParentIds = new List<Id>();
        LL_ParentIds.addAll(PS_ParentIds);
        return getRecords(PV_ObjectName, PV_ParentIdName, LL_ParentIds );
    }*/
        
    //Get all records of type PV_ObjectName where Field PV_ParentName is in PV_ParentId
    public static Map< Id, Map<Id, SObject> > getRecords(String PV_ObjectName, String PV_ParentIdName, List<Id> PS_ParentIds){
        List<SObject>                  LL_SObjects    = new List<SObject>();
        Map<Id,  Map<Id, SObject> >    LM_SObjects    = new Map<Id,  Map<Id, SObject> >();

        //Get Object Type
        Schema.SObjectType LO_ObjectType = Schema.getGlobalDescribe().get(PV_ObjectName);
        //Get Field Names
        Map<String, Schema.SObjectField> LM_Fields = LO_ObjectType.getDescribe().fields.getMap();        
        
        //Build Query
        String LV_Query = 'SELECT ';
        for(Schema.SObjectField LV_Field : LM_Fields.values()) 
        {
            if(LV_Field.getDescribe().getName() != 'Id')
            {
                LV_Query += LV_Field.getDescribe().getName() + ', ';
            }
        }
        LV_Query += 'Id FROM ' + PV_ObjectName + ' WHERE ' + PV_ParentIdName + ' IN: PS_ParentIds ';
       
        //Execute Query
        LL_SObjects = Database.query(LV_Query);
        
        //Generate Result Map
        if(!LL_SObjects.isEmpty())
        {
            for(SObject LO_SObject: LL_SObjects)
            {
                if(!LM_SObjects.containsKey( (ID) LO_SObject.get(PV_ParentIdName) ) )
                {
                    LM_SObjects.put( (ID) LO_SObject.get(PV_ParentIdName), new Map<Id, SObject>() );
                }
                if(LM_SObjects.containsKey( (ID) LO_SObject.get(PV_ParentIdName) ) )
                {
                    LM_SObjects.get( (ID) LO_SObject.get(PV_ParentIdName) ).put(LO_SObject.Id, LO_SObject);
                }
            }
        }
        return LM_SObjects;    
    }

    /*//Get all records of type PV_ObjectName where Field PV_ParentName is in PV_ParentId
    public static Map< Id, Map<Id, SObject> > getRecords(String PV_ObjectName, String PV_ParentIdName, List<Id> PS_ParentIds, String PV_SortField){
        List<SObject>                  LL_SObjects    = new List<SObject>();
        Map<Id,  Map<Id, SObject> >    LM_SObjects    = new Map<Id,  Map<Id, SObject> >();

        //Get Object Type
        Schema.SObjectType LO_ObjectType = Schema.getGlobalDescribe().get(PV_ObjectName);
        //Get Field Names
        Map<String, Schema.SObjectField> LM_Fields = LO_ObjectType.getDescribe().fields.getMap();        
        
        //Build Query
        String LV_Query = 'SELECT ';
        for(Schema.SObjectField LV_Field : LM_Fields.values()) 
        {
            if(LV_Field.getDescribe().getName() != 'Id')
            {
                LV_Query += LV_Field.getDescribe().getName() + ', ';
            }
        }
        LV_Query += 'Id FROM ' + PV_ObjectName + ' WHERE ' + PV_ParentIdName + ' IN: PS_ParentIds ORDER BY ' + PV_SortField + '  ASC';
        //Execute Query
        LL_SObjects = Database.query(LV_Query);
        
        //Generate Result Map
        if(!LL_SObjects.isEmpty())
        {
            for(SObject LO_SObject: LL_SObjects)
            {
                if(!LM_SObjects.containsKey( (ID) LO_SObject.get(PV_ParentIdName) ) )
                {
                    LM_SObjects.put( (ID) LO_SObject.get(PV_ParentIdName), new Map<Id, SObject>() );
                }
                if(LM_SObjects.containsKey( (ID) LO_SObject.get(PV_ParentIdName) ) )
                {
                    LM_SObjects.get( (ID) LO_SObject.get(PV_ParentIdName) ).put(LO_SObject.Id, LO_SObject);
                }
            }
        }
        return LM_SObjects;    
    }*/
	
    //Get a Map of the RecordIds and Records of the second level of a Map<Id, Map<Id, SObject> >
    public static Map<Id, SObject> flattenIdObjectMap(Map< Id, Map<Id, SObject> > PM_IdObjects){
        Map<Id, SObject> LM_FlatRecords = new Map<Id, SObject>();
        for(Map<Id, SObject> LM_Records: PM_IdObjects.Values()){
                LM_FlatRecords.putAll(LM_Records);
        }
        return LM_FlatRecords;
    }

    /*//Get a List of Records of the second level of a Map<Id, List<SObject> >
    public static List<SObject> flattenIdObjectMap(Map< Id, List<SObject> > PM_IdObjects){
        List<SObject> LL_FlatRecords = new List<SObject>();
        for(List<SObject> LL_Records: PM_IdObjects.Values()){
                LL_FlatRecords.addAll(LL_Records);
        }
        return LL_FlatRecords;
    }
    
    public static Boolean printSaveErrors(List<Database.SaveResult> PL_SaveResults){
        //print insert, update, upsert errors in vf message area
        Boolean LB_UpdateError = False;
        for(Database.SaveResult LO_UpdateResult: PL_SaveResults){
            if (!LO_UpdateResult.isSuccess()){
                LB_UpdateError = True;
                String LV_ErrorMessage= '';
                String LV_Seperator = '';
                for (Database.Error LO_DBError: LO_UpdateResult.getErrors()){
                    LV_ErrorMessage += LV_Seperator + LO_DBError.getMessage();
                    LV_Seperator = ', ';
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while updating record with id "' + LO_UpdateResult.getId() + '": ' + LV_ErrorMessage));
            }
        }
        return LB_UpdateError;
    }
    
    //Returns the Ids of a list of SObject Records
    public static List<Id> getIdList(List<SObject> PL_Records){
        List<Id> LL_Ids = new List<Id>();
        for(SObject LO_Record: PL_Records){
            LL_Ids.add(LO_Record.Id);
        }
        return LL_Ids;
    }

    //Returns a Map of Ids and Records of a list of SObject Records
    public static Map<Id, SObject> getIdObjectMap(List<SObject> PL_Records){
        Map<Id, SObject> LM_ObjectsIds = new Map<Id, SObject>();
        for(SObject LO_Record: PL_Records){
            LM_ObjectsIds.put(LO_Record.Id, LO_Record);
        }
        return LM_ObjectsIds;
    }*/
    
}