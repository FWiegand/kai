/*
 * @(#)SCDemandTest.cls SCCloud    dh 20.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCDemandTest
{  
    /**
     * Scheduling under test
     */
    private static SCDemand demand;
    
    static testMethod void testDemand()
    {
        demand = new SCDemand();
        Date myDate = date.newinstance(2010, 9, 20);
        demand.cwstart = myDate;
        
        System.assert(demand.cwstart == myDate);
    }
}