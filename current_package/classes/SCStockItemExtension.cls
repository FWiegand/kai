/*
 * @(#)SCStockItemExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCStockItemExtension extends SCMatMoveBaseExtension
{
    public SCStock__c stock;
    public String view { get; set;}
    public List<SCMaterialMovement__c> artMatMoves { get; set;}
    public List<ReplenishmentMaterialInfo> stockMatInfos { get; set;}
    public String articleId { get; set;}
    public String valuationType { get; set;}
    public Boolean isStockItems { get; private set;}
    public Boolean isMatMoves { get; private set;}
    public Boolean isStockMatItems { get; private set;}
    

    public List<SelectOption> getViewList()
    {
        return getStockItemListViews(stock.Id);
    }

    /**
     * Reads the material movements or part infos of a stock.
     *
     * @param    controller    the standard controller
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCStockItemExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        
        /*
         * DH: Dependent on app settings,
         * set as default the filter "(please select a filter)"
         * and keep the list empty 
         */        
        SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
        if(appset.STOCK_ITEM_FILTER_EMPTY__c)
        {
            this.view = '0';
        }
        else
        {
            this.view = '4';
        }
        fillList();
    } // SCStockItemExtension

    /**
     * Reads the material movements for one article of a stock.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference readArticleMatMoves()
    {
        System.debug('#### readArticleMatMoves(): articleId     -> ' + articleId);
        System.debug('#### readArticleMatMoves(): valuationType -> ' + valuationType);
        artMatMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                              CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                              Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, Name,
                              Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, DeliveryNote__c
                         from SCMaterialMovement__c 
                         where Stock__c = :stock.Id 
                         and Article__c = :articleId 
                         and ValuationType__c = :valuationType
                         order by CreatedDate desc, name desc];
        return null;
    } // readArticleMatMoves
    
    /**
     * Fills the list depending on the selected view.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference fillList()
    {
        System.debug('#### fillList(): view -> ' + view);

        isStockItems = false;
        isMatMoves = false;
        isStockMatItems = false;
        setController = null;
        
        if (view.equals('1'))
        {
            isStockMatItems = true;
            // next code line is implemented, so that the getter for setController ist called
            // and the replenishment infos are read
            ApexPages.StandardSetController contr = setController;
        } 
        else if (view.equals('2') || view.equals('3') || view.equals('4'))
        {
            isStockItems = true;
        } 
        else if (view.equals('5') || view.equals('6') || view.equals('7') || view.equals('8') || view.equals('9') || view.equals('10'))
        {
            isMatMoves = true;
        } 
        else
        {
            isStockItems = true;
        }
        
        return null;
    } // fillList

    /**
     * Initialize setController and return a list of records
     */
    public ApexPages.StandardSetController setController
    {
        get
        {
            List<SCStockItem__c> stockItems;
            List<SCMaterialMovement__c> matMoves;

            if (null == setController)
            {
                try
                {
                    //GMSET 25.09.2013: QueryLocation optimization. Reduces ViewState.
                    String stockItemStmtHead = 
                    	'SELECT Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ' +
						'	 Article__r.ReplacedBy__r.Name, Article__r.LockType__c,  ' +
						'	 Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c, ' +
						'	 ReplenishmentQty__c, InventoryQty__c, InventoryDate__c,  ' +
						'	 MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate, Name ' +
						'FROM SCStockItem__c  ';
                    
                    String materialMovementStmtHead = 
                    	'SELECT Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ' +
						'	CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name,  ' +
						'	Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  ' +
						'	Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name ' +
						'FROM SCMaterialMovement__c ';
                    // Parts - Replenishment
                    if (view.equals('1'))
                    {
                        List<Id> stockList = new List<Id>();
                        stockList.add(stock.Id);
                        
                        Map<Id, List<ReplenishmentMaterialInfo>> mapStockMatInfos = new Map<Id, List<ReplenishmentMaterialInfo>>();
                        getReplenishmentMaterial(stockList, Date.today(), mapStockMatInfos);
                        stockMatInfos = mapStockMatInfos.get(stock.Id);
                        setController = null;
                    } 
                    // Parts - with stock
                    else if (view.equals('2'))
                    {
                        /*stockItems = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                             Article__r.ReplacedBy__r.Name, Article__r.LockType__c, 
                                             Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c,
                                             ReplenishmentQty__c, InventoryQty__c, InventoryDate__c, 
                                             MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate, Name
                                      from SCStockItem__c 
                                      where Stock__c = :stock.Id and 
                                            Qty__c > 0 
                                      order by Article__r.Name];
                        setController = new ApexPages.StandardSetController(stockItems);*/
                        String stockItemStmt = stockItemStmtHead +
											'	WHERE Stock__c = \'' + stock.Id + '\' AND ' + 
                                            '	Qty__c > 0  ' +
                                      		'	ORDER BY Article__r.Name  ' + 
												'LIMIT 10000';
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(stockItemStmt));
                        
                    } // else if (view.equals('2'))
                    // Parts - active
                    else if (view.equals('3'))
                    {
                        /*stockItems = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                             Article__r.ReplacedBy__r.Name, Article__r.LockType__c, 
                                             Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c,
                                             ReplenishmentQty__c, InventoryQty__c, InventoryDate__c, 
                                             MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate, Name
                                      from SCStockItem__c 
                                      where Stock__c = :stock.Id and 
                                            Permanent__c = true 
                                      order by Article__r.Name];
                        setController = new ApexPages.StandardSetController(stockItems);
                        */
                        String stockItemStmt = stockItemStmtHead +
											'	WHERE Stock__c = \'' + stock.Id + '\' AND ' + 
                                            '	Permanent__c = true  ' +
                                      		'	ORDER BY Article__r.Name  ' + 
												'LIMIT 10000';
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(stockItemStmt));
                        
                    } // else if (view.equals('3'))
                    // Parts - all
                    else if (view.equals('4'))
                    {
                        /*stockItems = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                             Article__r.ReplacedBy__r.Name, Article__r.LockType__c, 
                                             Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c,
                                             ReplenishmentQty__c, InventoryQty__c, InventoryDate__c, 
                                             MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate, Name
                                      from SCStockItem__c 
                                      where Stock__c = :stock.Id 
                                      order by Article__r.Name];
                        */
                        String stockItemStmt = stockItemStmtHead +
												'WHERE Stock__c = \'' + stock.Id + '\' ' +
												'ORDER BY Article__r.Name ' + 
												'LIMIT 10000';
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(stockItemStmt));
                    } // else if (view.equals('4'))
                    // Movements - today
                    else if (view.equals('5'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c, 
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id and 
                                            CreatedDate >= :Date.today().AddDays(-1)  
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'	AND ' + 
                                            	'   CreatedDate >= LAST_N_DAYS:1 ' +  
                                      			'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                        
                    } // else if (view.equals('5'))
                    // Movements - last 10 days
                    else if (view.equals('6'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id and 
                                            CreatedDate >= :Date.today().AddDays(-10) 
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'	AND ' + 
                                            	'   CreatedDate >= LAST_N_DAYS:10 ' +  
                                      			'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                        
                    } // else if (view.equals('6'))
                    // Movements - last 30 days
                    else if (view.equals('7'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id and 
                                            CreatedDate >= :Date.today().AddDays(-30) 
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'	AND ' + 
                                            	'   CreatedDate >= LAST_N_DAYS:30 ' +  
                                      			'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                        
                    } // else if (view.equals('7'))
                    // Movements - last 90 days
                    else if (view.equals('8'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id and 
                                            CreatedDate >= :Date.today().AddDays(-90) 
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'	AND ' + 
                                            	'   CreatedDate >= LAST_N_DAYS:90 ' +  
                                      			'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                        
                    } // else if (view.equals('8'))
                    // Movements - all
                    else if (view.equals('9'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id 
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                        
                    } // else if (view.equals('9'))
                    // added by GMSGB 15.02.2013
                    // Movements - Ordered
                    else if (view.equals('10'))
                    {
                        /*matMoves = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                           CreatedDate, Type__c, Status__c, Qty__c, QtyOld__c, QtyNew__c, DeliveryStock__r.Name, 
                                           Order__r.Name, RequisitionDate__c, ListPrice__c, PrimeCost__c, DeliveryNote__c,  
                                           Replenishment__r.Id, Replenishment__r.Name, ValuationType__c, Name
                                      from SCMaterialMovement__c 
                                      where Stock__c = :stock.Id and Status__c in('5402','5403') // order and ordered (angefordert) 
                                      order by CreatedDate desc, name desc];
                        setController = new ApexPages.StandardSetController(matMoves);*/
                        
                        String materialMovementStmt = materialMovementStmtHead +
												'WHERE ' +
												'	Stock__c = \'' + stock.Id + '\' ' +
												'	AND ' +
												'	Status__c IN (\'5402\',\'5403\')' +
												'ORDER BY CreatedDate desc, name DESC ' + 
												'LIMIT 10000';
												
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(materialMovementStmt));
                      
                    } // else if (view.equals('10'))
                    
                    // stock profile
                    else
                    {
                    	
                        stockItems = [Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                             Article__r.ReplacedBy__r.Name, Article__r.LockType__c, 
                                             Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c,
                                             ReplenishmentQty__c, InventoryQty__c, InventoryDate__c, 
                                             MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate, Name
                                      from SCStockItem__c 
                                      where Stock__c = :stock.Id and 
                                            Article__c in (Select Article__c from SCStockProfileItem__c where StockProfile__c = :view) 
                                      order by Article__r.Name];
                        setController = new ApexPages.StandardSetController(stockItems);
                        
                        /*String stockItemStmt = stockItemStmtHead +
											'	WHERE Stock__c = \'' + stock.Id + '\' AND ' + 
                                            '	Article__c in (Select Article__c from SCStockProfileItem__c where StockProfile__c = \'' + view + '\')' +
                                      		'	ORDER BY Article__r.Name  ';
                        setController = new ApexPages.StandardSetController(Database.getQueryLocator(stockItemStmt));
                        */
                    }
                                
                    setController.setPageSize(100);
                    System.debug('#### try setController -> ' + setController);
                }
                catch(Exception e)
                {
                }
            }
            return setController;
        }
        set;
    } // setController

    /**
     * Getter for found stock items
     */
    public List<SCStockItem__c> getStockItemsList()
    {
        if (isStockItems)
        {
            return (List<SCStockItem__c>)setController.getRecords();
        }
        else
        {
            return null;
        }
    }

    /**
     * Getter for found material movements
     */
    public List<SCMaterialMovement__c> getMatMoveList()
    {
        if (isMatMoves)
        {
            return (List<SCMaterialMovement__c>)setController.getRecords();
        }
        else
        {
            return null;
        }
    }
} // SCStockItemExtension