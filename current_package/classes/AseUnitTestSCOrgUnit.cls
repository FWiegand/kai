/**
 * AseUnitTestSCCalendar.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /*
  * Testclass
  */
@isTest
private class AseUnitTestSCOrgUnit {

    static integer dataType = AseCalloutConstants.ASE_TYPE_ORGUNIT;

        
    @isTest
    public static void testSetParaString()
    {
        AseServiceSettings.isTest = true;
        AseServiceManager.set(AseCalloutConstants.TEST_TENANT, dataType, '<dummy>');
    }
    
    @isTest
    public static void testSetAll()
    {
        AseServiceSettings.isTest = true;
        AseServiceManager.setAll(AseCalloutConstants.TEST_TENANT, dataType);
    }
    
    @isTest
    public static void testIsTest()
    {
        AseCalloutSCOrgUnit ou = new AseCalloutSCOrgUnit();
        ou.getIsTest();
    }

   
    @isTest
    public static void testRemoveAll()
    {
        AseServiceManager.removeAll(AseCalloutConstants.TEST_TENANT, dataType);
    }
  
    @isTest
    public static void testPrintDebugGetAll()
    {
        AseServiceManager.printDebugGetAll(AseCalloutConstants.TEST_TENANT, dataType);
    }

}