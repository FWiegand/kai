/*
 * @(#)SCboContractTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private class SCboContractTest
{
/*
    public static testMethod void createContractFromTemplate()
    {
        SCboContract boContract = new SCboContract();
        ID contractTemplateID = 'a0CP0000000C7JSMA0';
        if(boContract.createFromTemplate(contractTemplateID))
        {
            insert boContract.contract;
            System.debug('................contract.id after insert->' + boContract.contract.Id);
            System.debug('................contract.Name after insert->' + boContract.contract.Name);
            
        }
        SCContract__c template = boContract.readTemplate(contractTemplateID);
        System.debug('................contract->' + template);
    }
*/
    static testMethod void testCreateLocalCopySCInsuranceDataOut()
    {
        Boolean leave = false;
        if(leave)
        {
            return;
        }
        // create the contract
        SCContractTestHelper contractTestHelper = new SCContractTestHelper();    
        contractTestHelper.prepareForBtcContractVisitCreateTest();
        
        SCboContract boContract = new SCboContract();
        SCInsuranceDataOut insData = new SCInsuranceDataOut(SCInsuranceDataOut.FunctionName.insertPolicy, 1);
        insData.inputEpsData = new SCInsuranceDataOut.EpsData();
        //insData.inputEpsData.installedBaseID = 'a0bP00000004NYOIA2';
        //insData.inputEpsData.contractTemplateID = 'a0CP00000009eZiMAI';
        //insData.inputEpsData.customerID = '001P000000FDJZkIAP';
        insData.inputEpsData.installedBaseID = contractTestHelper.appliance11.Id;
        insData.inputEpsData.contractTemplateID = contractTestHelper.template.Id;
        insData.inputEpsData.customerID = contractTestHelper.account.Id;
        //insData.inputEpsData.contractID = contractTestHelper.contract.Id;
        
        System.debug('insData.inputEpsData.installedBaseID: '+insData.inputEpsData.installedBaseID);
        System.debug('insData.inputEpsData.contractTemplateID: '+insData.inputEpsData.contractTemplateID);
        System.debug('insData.inputEpsData.customerID: '+insData.inputEpsData.customerID);                
        
        //InsertPolicyResponse insPolResp
        insData.insPolResp = new SCInsuranceDataOut.InsertPolicyResponse();
        insData.insPolResp.policies = new SCInsuranceDataOut.InsertOnePolicyResponse[1];
        insData.insPolResp.policies[0] = new SCInsuranceDataOut.InsertOnePolicyResponse();

        insData.insPolResp.policies[0].policyRef = 'PR2';
        insData.insPolResp.policies[0].mailCode = 'MC1';
        insData.insPolResp.policies[0].areaCode = 'AC1';
        try
        {
            insData.insPolResp.policies[0].salePrice = Double.valueOf('100.0');
            insData.insPolResp.policies[0].policyLength = Integer.valueOf('12');
            insData.insPolResp.policies[0].delayDays = Integer.valueOf('15');
        }
        catch(Exception e) {}
        insData.insPolResp.policies[0].policyTypeDesc = 'HS * one star';
        insData.insPolResp.policies[0].payMethod = 'PM';
        insData.insPolResp.policies[0].periodFrom = SCbase.formatSOQLDate(Date.today());
        insData.insPolResp.policies[0].periodTo = SCbase.formatSOQLDate(Date.today().addYears(1));
        
        // positiv test for the method createLocalCopy()
        boContract.createLocalCopy(insData);
        
        SCContract__c contract = [Select Id, Name, Status__c, StartDate__c, EndDate__c from SCContract__c where Id = :insData.inputEpsData.contractID];
        SCInstalledBase__c instBase = [Select Id, name, ContractRef__c from SCInstalledBase__c where Id = :contractTestHelper.appliance11.Id];
        System.assert(instBase.ContractRef__c.indexOf(contract.Name) > 0);
        
        // positiv test for the method validateLocalCopy()
        
        insData.validatePolResp = new SCInsuranceDataOut.ValidatePolicyResponse();
        insData.validatePolResp.policy = new SCInsuranceDataOut.ValidatedCustPolicy[1];
        insData.validatePolResp.policy[0] = new SCInsuranceDataOut.ValidatedCustPolicy();
        insData.validatePolResp.policy[0].policyRef = 'policyRef';
        insData.validatePolResp.policy[0].customerRef = 'customerRef';
        insData.validatePolResp.policy[0].policyStatus = 'Active';
        insData.validatePolResp.policy[0].policyPeriodFrom = Date.today();
        insData.validatePolResp.policy[0].policyPeriodTo = Date.today();
        
        
        
        boContract.validateLocalCopy(insData);
        
        // positiv test for the method hasItems()
        boContract.hasItems();
    } // testCreateLocalCopy 


    static testMethod void testValidateLocalCopyNegative()
    {
        SCboContract boContract = new SCboContract();
        SCInsuranceDataOut insData = new SCInsuranceDataOut(SCInsuranceDataOut.FunctionName.insertPolicy, 1);
        Boolean ret = boContract.validateLocalCopy(insData);
        System.debug('###...insData.validatePolResp.errorDescription: ' + insData.validatePolResp.errorDescription);
        System.assertEquals(ret, false);
        
    }

    /*
     *  Positiv test for the SCboContract
    */
    static testMethod void SCboContractPositiv()
    {
        Boolean leave = true;
        if(leave)
        {
            return;
        }
        // create the contract    
        SCContractTestHelper contractTestHelper = new SCContractTestHelper();
        contractTestHelper.prepareForBtcContractVisitCreateTest();
        
        test.startTest();
        
        SCboContract boContract = new SCboContract(contractTestHelper.contract);
        System.assertNotEquals(null, boContract.getContractId());
        boContract.save();
        
        // positiv test for the method readTemplate()
        boContract.readTemplate(contractTestHelper.contract.Name);
        
        test.stopTest();
    }
    
    /*
     *  Positiv test for the policy data holder
    */
    /*static testMethod void testCreateLocalCopyWSInsuranceUK()
    {
        // create the contract    
        SCContractTestHelper contractTestHelper = new SCContractTestHelper();
        contractTestHelper.prepareForBtcContractVisitCreateTest();
        
        test.startTest();

        WSInsuranceUK.CreatePolicyDataHolder insData = new WSInsuranceUK.CreatePolicyDataHolder();
        fillWSInsuranceUK(insData, contractTestHelper);
        System.debug('###insData afer fill: ' + insData);
        
        SCboContract boContract = new SCboContract();

        // positiv test for the method createLocalCopy(WSInsuranceUK.CreatePolicyDataHolder insData)
        boContract.createLocalCopy(insData);
        
        SCContract__c contract = [Select Id, Name, Status__c, StartDate__c, EndDate__c from SCContract__c where Id = :insData.Policies.Policy[0].EPSContractId];
        SCInstalledBase__c instBase = [Select Id, name, ContractRef__c from SCInstalledBase__c where Id = :contractTestHelper.appliance11.Id];
        System.assert(instBase.ContractRef__c.indexOf(contract.Name) > 0);
     
        test.stopTest();
    }
    
    public static void fillWSInsuranceUK(WSInsuranceUK.CreatePolicyDataHolder insData,
                                   SCContractTestHelper contractTestHelper)
    {
        insData.Customer = new WSInsuranceUK.CustomerDetails();
        insData.Landlord = new WSInsuranceUK.CustomerDetails();
        insData.Installer = new WSInsuranceUK.CustomerDetails();
        insData.installerAccount = contractTestHelper.account;
        insData.customerAccount = contractTestHelper.account;
        insData.landlordAccount = contractTestHelper.account;
        insData.customerCreated = false;
        insData.landlordCreated = false;
        insData.rolesLocationIdList = null;
        insData.location = null;
        insData.locationCreated = false;
        insData.role = null; // InstalledBaseRole;
        insData.installedBase = contractTestHelper.appliance11;
        insData.installedBaseCreated = false;
        insData.contractTemplate = contractTestHelper.template;
        // don't set this flag, we excpect the status 'Active' after the contract is created
        // but if the flag is set, the contract has the status 'Check'
        // insData.contractTemplateCreated = true;
        insData.contract = null;
        insData.response = new WSInsuranceUK.CreatePolicyResponse();
        insData.Policies = new WSInsuranceUK.PoliciesContainer();
        insData.Policies.Policy = new  WSInsuranceUK.NewPolicy[1];
        insData.Policies.Policy[0] = new WSInsuranceUK.NewPolicy();
        insData.Policies.Policy[0].PolicyRef = 'PolicyRef';
        insData.Policies.Policy[0].CustomerRefNo = 'CustomerRefNo';
        insData.Policies.Policy[0].SalePrice = 100;
        insData.Policies.Policy[0].StartDate = Date.today();
        insData.Policies.Policy[0].EndDate = Date.today().addYears(1);
        insData.Policies.Policy[0].OnRiskDate = Date.today();
        // set the status who it will be returned from homeserve
        // it will be mapped in WSInsuranceUK.getEPSStatus
        insData.Policies.Policy[0].Status = '00';
        System.debug('### insData.Policies: ' + insData.Policies);
        String policyRef = insData.Policies.Policy[0].PolicyRef;
        // fill response
        insData.response.OneClientChannel = 'OneClientChannel' ;    // to identify the request
        insData.response.TransactionID = 'TransactionID';           // to identify the request
        insData.response.CustomerRef = 'CustRef';      // to identify the customer
        insData.response.LandlordRef = 'Landlord.CustomerRefNo';      // to identify the landlord
        insData.response.InstallerRef = 'Installer.CustomerRefNo';     // to identify the installer
        
        // initialise the response policies sequence
        WSInsuranceUK.PoliciesResponseContainer Policies = new WSInsuranceUK.PoliciesResponseContainer();
        
        insData.response.Policies = Policies;    // the array of responses for creation
                                    // a policy. Each item contains the
                                    // the policy and possible array of errors

        Integer numberOfPolicies = insData.Policies.Policy.size();
        Policies.PolicyResponse = new WSInsuranceUK.PolicyResponse[numberOfPolicies];
        Integer i = 0;
        for(WSInsuranceUK.NewPolicy p:  insData.Policies.Policy)
        {
            Policies.PolicyResponse[i] = new WSInsuranceUK.PolicyResponse();
            Policies.PolicyResponse[i].Policy = p;
            Policies.PolicyResponse[i].TransactionID = 'TransactionID';
            i++;
        }//for   
    }*/
    
    static testMethod void testGetHasItems()    
    {
        SCboContract bo = new SCboContract();
        bo.getHasItems();
    }   
    
    static testMethod void testHasItems()    
    {
        SCboContract bo = new SCboContract();
        bo.hasItems();
    }   
    
    static testMethod void testSave()
    {       
        SCContractTestHelper contractTestHelper = new SCContractTestHelper();
        contractTestHelper.prepareForBtcContractVisitCreateTest();
        try
        {
            SCboContract bo = new SCboContract(contractTestHelper.contract);
            // roles        
            bo.addContractRole('50301', 
                            null, //contractTestHelper.contract.ID, 
                            null, 
                            null, //contractTestHelper.account.ID, 
                            null);
            // role RE
            bo.addContractRole('50303', 
                            contractTestHelper.contract.ID, 
                            null, 
                            contractTestHelper.account.ID, 
                            null);
                            
            // contract items
            SCboContractItem contractItem = new SCboContractItem(contractTestHelper.contractItem1);
            bo.boContractItems.add(contractItem);                
            
            // deletion items
            //bo.deletionItems.add(contractItem);
            
            bo.save();
            bo.getRole('50301');
            bo.getRole('50302');
       }
       catch(Exception e)
       {}
    }
    
    static testMethod void testChangeStatusFromActiveToCreated()
    {
        SCContract__c c = new SCContract__c();
        c.StartDate__c = Date.today().addMonths(1);
        c.Status__c = 'Active';
        SCboContract.changeStatusFromActiveToCreated(c);
        System.assertEquals('Created', c.Status__c);
    }
    
} // SCboContractTest