/*
 * @(#)SCboStock.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboStock 
{
	/**
	 * Mark the given stock / stockitems for backend reload
	 * (stock.activiy = U and stockitems.activiy = Q )
	 *
	 * @param resourceId	a stock id to update 
	 */
	public static void reloadMobileStockById(String stockId)
	{
		reloadMobileStockById(new List<String>{stockId});
	}
	
	
	/**
	 * Mark the given stocks / stockitems for backend reload
	 * (stocks.activiy = U and stockitems.activiy = Q )
	 *
	 * @param resourceIds	a list of stock ids to update 
	 */
	public static void reloadMobileStockById(List<String> stockIds)
	{
		system.debug('#### mark stocks to backend reload: ' + stockIds);
		SCStock__c[] stocks = 
		[
		    SELECT 
		        Id,  
		        (  
		            SELECT 
		                Id  
		            FROM  
		                StockItem__r  
		        ) 
		    FROM  
		        SCStock__c  
		    WHERE  
		        Id IN :stockIds 
		        
		];
		system.debug('#### selected stocks: ' + stocks);
		
		
		//set activity 'U' to the selected stocks
		updateActivityOfStocks(stocks, 'U');
		
		//set activity 'Q' to the selected stockitems
		updateActivityOfStockItems(stocks, 'Q');
		
		system.debug('#### stock / stockitems after update: ' + stocks);
	}
	
	/**
	 * Mark the stocks / stockitems of the given resource for backend reload
	 * (stocks.activiy = U and stockitems.activiy = Q )
	 *
	 * @param resourceId	a resource id to update 
	 */
	
	public static void reloadMobileStockByResource(String resourceId)
	{
		reloadMobileStockByResource(new List<String>{resourceId});
	}
	
	/**
	 * Mark the stocks / stockitems of the given resources for backend reload
	 * (stocks.activiy = U and stockitems.activiy = Q )
	 *
	 * @param resourceIds	a list of resource ids to update 
	 */
	public static void reloadMobileStockByResource(List<String> resourceIds)
	{
		system.debug('#### mark resource stocks to backend reload: ' + resourceIds);
		
		SCStock__c[] stocks = 
		[
		    SELECT 
		        Id,  
		        (  
		            SELECT 
		                Id  
		            FROM  
		                StockItem__r  
		        ) 
		    FROM  
		        SCStock__c  
		    WHERE  
		        Id IN  
		        (
		            SELECT 
		                Stock__c
		            FROM 
		                SCResourceAssignment__c
		            WHERE 
		                Resource__r.id IN :resourceIds
		                AND 
		                ValidFrom__c <= TODAY 
		                AND
		                ValidTo__c >= TODAY
		                AND
		                Stock__c <> NULL 
		        )
		];
		
		system.debug('#### selected stocks: ' + stocks);
		
		
		//set activity 'U' to the selected stocks
		updateActivityOfStocks(stocks, 'U');
		
		//set activity 'Q' to the selected stockitems
		updateActivityOfStockItems(stocks, 'Q');
	
		system.debug('#### stock / stockitems after update: ' + stocks);
	
	}
	
	/**
	 * Updates the activity flag of stocks
	 *
	 * @param stocks	the stock objects to update
	 * @param activity	the value to set (U)
	 */
	 
	private static void updateActivityOfStocks(List<SCStock__c> stocks, String activity)
	{
		system.debug('#### stocks before update: ' + stocks);
		
		for(SCStock__c stock : stocks)
		{
		    stock.Activity__c = activity;
		}
		update stocks;
		
		system.debug('#### stocks after update: ' + stocks);
	}
	
	
	/**
	 * Updates the activity flag of all stockitems assigned to stocks
	 *
	 * @param stocks	the stock objects to update (with stockitems as childrelation)
	 * @param activity	the value to set (Q,U)
	 */
	 
	private static void updateActivityOfStockItems(List<SCStock__c> stocks, String activity)
	{
		for(SCStock__c stock : stocks)
		{
		    SCStockItem__c[] stockItems = stock.stockitem__r;
		    system.debug('#### stockitems before update: ' + stockItems);
		    for(SCStockItem__c item : stockItems)
		    {
		        item.activity__c = activity;
		    }
		    update stockItems;
		    system.debug('#### stockitems after update: ' + stockItems);
		    
		}
	}
}