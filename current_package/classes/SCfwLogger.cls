/*
 * @(#)SCLogger.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Extended logging framework to ease the logging of messages in a define format
 *
 * @author <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCfwLogger {

    private String fileName;
    private System.LoggingLevel logLevel;

    /**
     * Private constructor
     */
    private SCfwLogger() 
    {
        logger = this;
    }
    

    /**
     * A mechanism to log message to the file.
     *
     * @param p Priority
     * @param message String
     */
    private void logMsg(System.LoggingLevel logLevel, String message) 
    {
        System.debug(logLevel, message);
    }
    
    
    /**
     * Create a simple message of level warning nicely formatted.
     *
     * @param fixed message part
     * @param variable to add and print
     */
    public void debugVariable(String message, String variable)
    {
        String logMessage = '### ' + message + ' -> ' + variable;
        logMsg(LoggingLevel.DEBUG, logMessage);
    }
    
    
    /**
     * this method initialises the logger, creates an object
     */
    public static void initialize() 
    {
        if (logger == null) 
        {
            logger = new SCfwLogger();
        }
    }

    // singleton - pattern
    private static SCfwLogger logger;
    public static SCfwLogger getLogger() 
    {
        initialize();
        return logger;
    }
    
}// End of class