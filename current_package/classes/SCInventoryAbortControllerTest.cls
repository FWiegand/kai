/*
 * @(#)SCInventoryAbortControllerTest.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class tests the functionality of the main class
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest (SeeAllData = true)
private class SCInventoryAbortControllerTest
{
    private static SCInventoryAbortController iac;

    // Testing close function
    static testMethod void inventoryAbortControllePositiv1() 
    {
        // Creating a test inventory
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
        ApexPages.currentPage().getParameters().put('close', 'true');
        
        iac = new SCInventoryAbortController();
        
        iac.init();
    }
    
    // Testing cancel function
    static testMethod void inventoryAbortControllePositiv2() 
    {
        // Creating a test inventory
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
        ApexPages.currentPage().getParameters().put('oid', inventoryId);
        ApexPages.currentPage().getParameters().put('cancel', 'true');
        
        iac = new SCInventoryAbortController();
        
        iac.init();
        iac.goBack();
    }
}