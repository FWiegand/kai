/*
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This class is for testing only one test method.
 * It is not required that this test class is successful.
 * @author SS <sschrage@gms-online.de>
 */
 
public with sharing class GMS_SS_Test
{
    String Print = ' ';
    List<SCDomainValue__c> listDomVal = new List<SCDomainValue__c>();
    
    public GMS_SS_Test(ApexPages.StandardController c) { }
    public GMS_SS_Test() { }
    
    public List<String> getPrint() { return Print.split('\n'); }
    
    public List<SCDomainValue__c> getListDomVals() { return listDomVal; }
    
    public void start()
    {
        String str1 = URL.getCurrentRequestUrl().toExternalForm();
        String str2 = URL.getSalesforceBaseUrl().toExternalForm();
        String str3 = 'https://cs7-api.salesforce.com/services/Soap/class/SCPrintOrderController';
        
        Print += '\n' + UserInfo.getSessionId();
        Print += '\n' + str1;
        Print += '\n' + str2;
        Print += '\n' + str3;
        Print += '\n' + str3.compareTo(str1);
        
        listDomVal = [SELECT Id, ID2__c, tmp_De__c, tmp_En__c FROM SCDomainValue__c WHERE tmp_De__c < 1 AND tmp_En__c >= 1];
        
        listDomVal = (listDomVal != null && listDomVal.size() > 0) ? listDomVal : new List<SCDomainValue__c>();
    }
    
    
    
    public static testMethod void TestThisClass() 
    {
        GMS_SS_Test tmp1 = new GMS_SS_Test();
        System.assertEquals(tmp1.getPrint(),' '.split('\n'));
        
        GMS_SS_Test tmp2 = new GMS_SS_Test(new ApexPages.StandardController(new Document()));
        tmp2.start();
        System.assertNotEquals(null,tmp2.getPrint());
    }
}