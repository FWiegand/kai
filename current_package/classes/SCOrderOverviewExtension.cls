/* 
 * @(#)SCOrderOverviewExtension.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.  
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use  
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author narmbruster@gms-online.de
 */

/*
 * Reads the field sets for the order tabs in the overview page.
 * Please note that this is a simple implementation with minimum configuration support. 
 */
public with sharing class SCOrderOverviewExtension 
{
    public String activeTab {get; set;}

    public SCOrder__c ord {get; set;}
    

   /*
    * Constructor - reads the required data
    */
    public SCOrderOverviewExtension(ApexPages.StandardController controller) 
    {
        String oid = controller.getRecord().id;

        init(oid);
    }

    public void init(String oid)
    {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        ord = [select id, name, type__c, status__c, createddate,
                 (select id, name1__c, postalcode__c, city__c, street__c, houseno__c from OrderRole__r where OrderRole__c = '50301' limit 1) // service recipient
               from  SCOrder__c where id = :oid];
               
    }    

    public String getAccountAddress()
    {
        if(ord != null && ord.OrderRole__r != null && ord.OrderRole__r.size() > 0)
        {
            SCOrderRole__c sr = ord.OrderRole__r[0]; // service recipient
            String result = '' + sr.name1__c;
            if(sr.postalcode__c != null)
            {
              result += ' ' + sr.postalcode__c;
            } 
            if(sr.city__c != null)
            {
              result += ' ' + sr.city__c;
            }
            if(sr.street__c != null)
            {   
                result += + ' ' + sr.street__c;
            }      
            if(sr.houseno__c != null)
            {
                result += ' ' + sr.houseno__c;
            }
            return result;
        }
        return '';
    }
    

    //--<Order Roles>----------------------------------------------------------
    List<SCOrderRole__c> roles;
    
    // load on demand    
    public List<SObject> getRoles()
    {
        if(roles == null && ord != null)
        {
            roles = readRoles(ord.id);
        }
        return roles;
    }
    
    public List<Schema.FieldSetMember> getRoleFields() 
    {
        return SObjectType.SCOrderRole__c.FieldSets.orderoverviewlist.getFields();
        
    }
    
    List<SCOrderRole__c> readRoles(String oid)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getRoleFields()) 
        {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM SCOrderRole__c where order__c = :oid order by type__c';
        return Database.query(query);
    }
    
    List<SObject> readRoles2(String oid)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getRoleFields()) 
        {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM SCOrderRole__c where order__c = :oid order by type__c';
        return Database.query(query);
    }
    
        




    //--<External operation>----------------------------------------------------------
    List<SCOrderExternalAssignment__c> extops;
    
    // load on demand    
    public List<SCOrderExternalAssignment__c> getExternalOps()
    {
        if(extops == null && ord != null)
        {
            extops = readExternalOps(ord.id);
        }
        return extops;
    }
    
    public List<Schema.FieldSetMember> getExternalOpsFields() 
    {
        return SObjectType.SCOrderExternalAssignment__c.FieldSets.orderoverviewlist.getFields();
    }
    
    List<SCOrderExternalAssignment__c> readExternalOps(String oid)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getExternalOpsFields()) 
        {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM SCOrderExternalAssignment__c where order__c = :oid order by name desc';
        return Database.query(query);
    }
    

    
    //--<Order repair codes>----------------------------------------------------------
    List<SCOrderRepairCode__c> repcode;
    
    // load on demand    
    public List<SCOrderRepairCode__c> getRepCodes()
    {
        if(repcode == null && ord != null)
        {
            repcode = readRepCodes(ord.id);
        }
        return repcode;
    }
    
    public List<Schema.FieldSetMember> getRepCodeFields() 
    {
        return SObjectType.SCOrderRepairCode__c.FieldSets.orderoverviewlist.getFields();
    }
    
    List<SCOrderRepairCode__c> readRepCodes(String oid)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getRepCodeFields()) 
        {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM SCOrderRepairCode__c where order__c = :oid order by name desc';
        return Database.query(query);
    }
    

    //--<Order items>----------------------------------------------------------
    List<SCOrderItem__c> orderitems;
    
    // load on demand    
    public List<SCOrderItem__c> getOrderItems()
    {
        if(orderitems == null && ord != null)
        {
            orderitems = readOrderItems(ord.id);
        }
        return orderitems;
    }
    
    public List<Schema.FieldSetMember> getOrderItemFields() 
    {
        return SObjectType.SCOrderItem__c.FieldSets.orderoverviewlist.getFields();
    }
    
    List<SCOrderItem__c> readOrderItems(String oid)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getOrderItemFields()) 
        {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id FROM SCOrderItem__c where order__c = :oid order by name asc';
        return Database.query(query);
    }
    
    public String getSid()
    {
        return UserInfo.getSessionId();
    }
    
    public pageReference onTestMe()
    {
        SCPrintOrderController.createPDFAttachmentAsyncWS(UserInfo.getSessionId(), ord.id, 'gmsna'); 
        return null;
    }


}