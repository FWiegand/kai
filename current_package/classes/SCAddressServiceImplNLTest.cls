/*
 * @(#)SCAddressServiceImplNLTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/** 
 * Additional tests for the webserivce.nl integration. 
 */
@isTest
private class SCAddressServiceImplNLTest {
    
    // basic tests - see SCAddressServiceImplNL


    // ensure testcoverage of web service wrapper 
    static testMethod void checkServiceDummy(){

        // web services can't be tested directly - the following code is just for 
        // increasing coverage no real test possible here
        WSWebserviceNL s = new WSWebserviceNL();
        
        WSWebserviceNL.geoLocationInternationalAddressCoordinatesLatLonRequestType a1 = new WSWebserviceNL.geoLocationInternationalAddressCoordinatesLatLonRequestType();
        WSWebserviceNL.internationalAddressSearchV2RequestType a2 = new WSWebserviceNL.internationalAddressSearchV2RequestType();
        WSWebserviceNL.LatLonCoordinatesInternationalAddressArray a3 = new WSWebserviceNL.LatLonCoordinatesInternationalAddressArray();
        WSWebserviceNL.InternationalAddressSearchV2Result a4 = new WSWebserviceNL.InternationalAddressSearchV2Result();
        WSWebserviceNL.internationalAddressSearchV2ResponseType a5 = new WSWebserviceNL.internationalAddressSearchV2ResponseType();
        WSWebserviceNL.InternationalV2 a6 = new WSWebserviceNL.InternationalV2();
        WSWebserviceNL.InternationalFormattedAddress a7 = new WSWebserviceNL.InternationalFormattedAddress();
        WSWebserviceNL.loginResponseType a8 = new WSWebserviceNL.loginResponseType();
        WSWebserviceNL.Webservices_nlPort a9 = new WSWebserviceNL.Webservices_nlPort();
        WSWebserviceNL.loginRequestType a10 = new WSWebserviceNL.loginRequestType();
        WSWebserviceNL.InternationalV2Array a11 = new WSWebserviceNL.InternationalV2Array();
        WSWebserviceNL.LatLonCoordinatesInternationalAddress a12 = new WSWebserviceNL.LatLonCoordinatesInternationalAddress();
        WSWebserviceNL.HeaderLoginType a13 = new WSWebserviceNL.HeaderLoginType();
        WSWebserviceNL.geoLocationInternationalAddressCoordinatesLatLonResponseType a14 = new WSWebserviceNL.geoLocationInternationalAddressCoordinatesLatLonResponseType();
        WSWebserviceNL.HeaderAuthenticateType a15 = new WSWebserviceNL.HeaderAuthenticateType();
        WSWebserviceNL.addressReeksPostcodeSearchRequestType  a16 = new WSWebserviceNL.addressReeksPostcodeSearchRequestType();
        WSWebserviceNL.PCReeks a17 = new WSWebserviceNL.PCReeks();
        WSWebserviceNL.addressReeksPostcodeSearchResponseType a18 = new WSWebserviceNL.addressReeksPostcodeSearchResponseType();
        WSWebserviceNL.geoLocationAddressCoordinatesLatLonRequestType a19 = new WSWebserviceNL.geoLocationAddressCoordinatesLatLonRequestType();
        WSWebserviceNL.geoLocationAddressCoordinatesLatLonResponseType a20 = new WSWebserviceNL.geoLocationAddressCoordinatesLatLonResponseType();
        WSWebserviceNL.PCReeksSearchPartsPagedResult a21 = new WSWebserviceNL.PCReeksSearchPartsPagedResult();
		WSWebserviceNL.PCReeksArray a22 = new WSWebserviceNL.PCReeksArray();
		WSWebserviceNL.ResultInfo a23 = new WSWebserviceNL.ResultInfo();
		WSWebserviceNL.SearchParts a24 = new WSWebserviceNL.SearchParts();
		WSWebserviceNL.LatLonCoordinatesMatch a25 = new WSWebserviceNL.LatLonCoordinatesMatch();
		WSWebserviceNL.addressReeksParameterSearchResponseType a26 = new WSWebserviceNL.addressReeksParameterSearchResponseType();  
		         

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.login('invalid', 'ignore');
        }
        catch(Exception e){}

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.internationalAddressSearchV2('a', 'b', 'street', 'housenr', 'pobox', 'locality', 'postcode',
                    'province', 'country',  'language', 'country_format');
        }
        catch(Exception e){}

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.geoLocationInternationalAddressCoordinatesLatLon('street',1, 'city', 'province', 'country', 'language');
        }
        catch(Exception e){}

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.addressReeksPostcodeSearch('1234567');
        }
        catch(Exception e){}

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.addressReeksParameterSearch('province', 'district', 'city', 'street', 123, 'a', 1);
        }
        catch(Exception e){}

        try
        {
            WSWebserviceNL.Webservices_nlPort a = new WSWebserviceNL.Webservices_nlPort();
            a.geoLocationAddressCoordinatesLatLon('postcode', 'city' , 'street', 123);
        }
        catch(Exception e){}
   }
}