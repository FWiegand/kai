/**********************************************************************
Name:  fsfaAlertBatch
======================================================
Purpose:                                                            
Runs each night and sends out an Alert to the Owner of an Picture of succsess in case it expiers in two weeks                                                     
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
11/26/2013 Bernd Werner	<b.werner@yoursl.de>           
***********************************************************************/

global class fsfaContentAlertBatch  implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	String limitstring ='';
    	Integer qsize = 1000;
    	if (qsize != 0){
    		limitstring = ' LIMIT '+qsize;
    	}
    	// Calculate the dateof expiration
    	date firedate = System.today()+14; 
		String query = 'SELECT Valid_until__c, Title, Owner.Email, OwnerId, IsLatest, Id FROM ContentVersion WHERE IsLatest=true AND Valid_until__c='+ string.ValueOf(firedate);
    	query = query + limitstring;
    	// system.debug('##### Query: '+query);
    	return Database.getQueryLocator(query);  
	}

	global void execute(Database.BatchableContext BC, List<ContentVersion> scope)
	{
		System.debug('####Scope: '+scope);
		Map <String,List<ContentVersion>> alertMap = new Map<String,List<ContentVersion>>();
    	for (ContentVersion convers : scope)
    	{
    		if(!alertMap.containsKey(convers.Owner.Email))
    		{
    			List <ContentVersion> tmpList = new List <ContentVersion>();
    			tmpList.add(convers);
    			alertMap.put(convers.Owner.Email,tmpList);
    		}
    		else
    		{
    			alertMap.get(convers.Owner.Email).add(convers);
    		}
    	}
    	System.debug('###Alert Map: '+alertMap);
    	
    	// Reserve Mail capacity
    	Messaging.reserveSingleEmailCapacity(alertMap.size());
    	// Collect Data and send Mail
    	for (String mailadd:alertMap.keySet()){
    		String BodyText='Attention, \n\rthe follwoing Content will expire in two weeks:\n\r';
    		for (ContentVersion tmpPic:alertMap.get(mailadd))
    		{
    			BodyText = BodyText + '- ' + tmpPic.Title+'\n\r';
    		}
    		// Compose Mail:
    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    		// Define Address:
    		String[] toAddresses = new String[] {mailadd};
    		// Assign the addresses for the To to the mail object.
			mail.setToAddresses(toAddresses);
			// Specify the address used when the recipients reply to the email. 
			mail.setReplyTo('support@test.com');
			// Specify the name used as the display name.
			mail.setSenderDisplayName('Content monitoring');
			// Specify the subject line for your email address.
			mail.setSubject('Content expiration warning');
			// Specify the text content of the email.
			mail.setPlainTextBody(BodyText);
			// Send the email you have created.
			System.debug('###Mail: '+mail);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    	}
    	
    	
	}

	global void finish(Database.BatchableContext BC)
	{
		System.debug('Batch Process Complete');
	}
}


// fsfaContentAlertBatch batch = new fsfaContentAlertBatch();
// Id batchId = Database.executeBatch(batch);