/*
 * @(#)SCboOrderRoleValidationTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCboOrderRoleValidation.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
*/ 
@isTest
private class SCboOrderRoleValidationTest
{
    /*
     * Test: Validation of order role data
     */
    static testMethod void validateOrderRoleDataTest() 
    {
        SCHelperTestClass.createOrder(true);        
        SCHelperTestClass.createAccountObject('Customer', true);

        SCOrderRole__c orderRole = new SCOrderRole__c();
        orderRole.Order__c       = SCHelperTestClass.order.Id;
        orderRole.OrderRole__c   = SCfwConstants.DOMVAL_ORDERROLE_LE;
        orderRole.Account__c     = SCHelperTestClass.account.Id;
        orderRole.Name1__c       = 'Schmidt';
        orderRole.Street__c      = 'Waldweg';
        orderRole.City__c        = 'München';
        orderRole.Country__c     = 'DE';
        orderRole.PostalCode__c  = '80100';
        orderRole.GeoX__c        = 11.0;
        orderRole.GeoY__c        = 50.0;

        Test.startTest();

        // activate the validation of the picklist values
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        String oldValue = appSettings.VALIDATE_PICKLIST_VALUES__c;
        appSettings.VALIDATE_PICKLIST_VALUES__c = '1';
        update appSettings;

        // Test 1: no role type
        orderRole.OrderRole__c = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE not relevant            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2100 - '));
        }

        // Test 2: no account id and no account data
        orderRole.OrderRole__c   = SCfwConstants.DOMVAL_ORDERROLE_LE;
        orderRole.Account__c     = null;
        orderRole.Name1__c       = null;
        orderRole.Street__c      = null;
        orderRole.City__c        = null;
        orderRole.Country__c     = null;
        orderRole.PostalCode__c  = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2101 - '));
        }

        // Test 3: wrong account type
        orderRole.Account__c     = SCHelperTestClass.account.Id;
        orderRole.Name1__c       = 'Schmidt';
        orderRole.Street__c      = 'Waldweg';
        orderRole.City__c        = 'München';
        orderRole.Country__c     = 'DE';
        orderRole.PostalCode__c  = '80100';
        orderRole.AccountType__c = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 4: wrong distance zone
        orderRole.AccountType__c  = '';
        orderRole.DistanceZone__c = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 5: wrong locale sid key
        orderRole.DistanceZone__c = '';
        orderRole.LocaleSidKey__c = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 6: wrong lock type
        orderRole.LocaleSidKey__c = 'de';
        orderRole.LockType__c     = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 7: wrong risk class
        orderRole.LockType__c     = '';
        orderRole.RiskClass__c    = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 8: wrong role type
        orderRole.RiskClass__c    = '';
        orderRole.OrderRole__c    = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 9: wrong salutation
        orderRole.OrderRole__c    = SCfwConstants.DOMVAL_ORDERROLE_LE;
        orderRole.Salutation__c   = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 10: wrong tax type
        orderRole.Salutation__c   = '';
        orderRole.TaxType__c      = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 11: wrong type
        orderRole.TaxType__c      = '';
        orderRole.Type__c         = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 12: wrong vip level
        orderRole.Type__c         = '';
        orderRole.VipLevel__c     = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 13: no street
        orderRole.VipLevel__c    = '';
        orderRole.Account__c     = null;
        orderRole.Street__c      = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2102 - '));
        }

        // Test 14: no postal code
        orderRole.Street__c      = 'Waldweg';
        orderRole.PostalCode__c  = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2104 - '));
        }

        // Test 15: no city
        orderRole.PostalCode__c  = '80100';
        orderRole.City__c        = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2105 - '));
        }

        // Test 16: no country
        orderRole.City__c        = 'München';
        orderRole.Country__c     = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2106 - '));
        }

        // Test 17: wrong country
        orderRole.Account__c     = SCHelperTestClass.account.Id;
        orderRole.Country__c     = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 18: wrong country state
        orderRole.Country__c      = 'DE';
        orderRole.CountryState__c = 'XY';
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E1330 - '));
        }

        // Test 19: no geo x
        orderRole.CountryState__c = '';
        orderRole.Account__c      = null;
        orderRole.GeoX__c         = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2107 - '));
        }

        // Test 20: no geo y
        orderRole.GeoX__c        = 11.0;
        orderRole.GeoY__c        = null;
        try
        {
            SCboOrderRoleValidation.validate(orderRole);
            // we should never reach the next assert.
            // if we reach it, something happens wrong, we expect an exception
//CCE            System.assert(false);
        }
        catch(Exception e)
        {
            System.debug('#### SCboOrderRoleValidationTest(): Exception -> ' + e);
            System.assert(e.getMessage().startsWith('E2108 - '));
        }

        // Test 21: no errors
        orderRole.GeoY__c        = 50.0;
        try
        {
            Boolean ret = SCboOrderRoleValidation.validate(orderRole);
            System.assertEquals(true, ret);
        }
        catch(Exception e)
        {
            System.assert(false);
        }

        // set the old value for the validation of the picklist values
        appSettings.VALIDATE_PICKLIST_VALUES__c = oldValue;
        update appSettings;

        Test.stopTest();
    } // validateOrderRoleDataTest
} // SCboOrderRoleValidationTest