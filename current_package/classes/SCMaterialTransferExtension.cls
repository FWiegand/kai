/*
 * @(#)SCMaterialTransferExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$ 
 */
public with sharing class SCMaterialTransferExtension extends SCMatMoveBaseExtension
{
    public static SCfwDomain domMatStat = new SCfwDomain('DOM_MATERIALMOVEMENT_STATUS');

    private Boolean setStockItemsValType = false;
    private String receiverValType = 'none';
    private SCStock__c stock;
    private String domMatTransfer = SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_OUT;
    private List<SelectOption> valTypeList = new List<SelectOption>();
    
    public List<SelectOption> receiverList;
    public Id receiverStock { get; set;}
    public String matMoveType { get; set;}
    public Boolean transferOk { get; private set; }
    public List<TransferArticleInfo> transInfoList { get; private set; }
    
    public Boolean getReceiverValType() { return (receiverValType.equalsIgnoreCase('true') ? true : false); }
    public Boolean getSetStockItemsValType () { return setStockItemsValType; }
    
    /**
     * Wrapper class for the items in the list displayed on the page.
     * The list shows all articles for transferring.
     */
    public class TransferArticleInfo
    {
        public Boolean isSet { get; set;}
        public Boolean hasError { get; set;}
        public Double qty { get; set;}
        public SCStockItem__c stockItem { get; set;}
        public String valuationType { get; set;}
        public String originalValType { get; set;}
        public Boolean existValType { get; set;}
        public Boolean hasArticleValType { get; set;}
        
        public String getStockItemValTypeTranslation() { return ValuationTypeTranslation(stockItem.ValuationType__c); }
        
        public TransferArticleInfo()
        {
            isSet = false;
            hasError = false;
            stockItem = null;
            qty = 0.0;
            existValType = false;
            valuationType = null;
            originalValType = null;
            hasArticleValType = false;
        } // TransferArticleInfo
    } // class TransferArticleInfo

    private static Map<String,String> mapValuationTypeTranslation = new Map<String,String>();
    private static String ValuationTypeTranslation(String value)
    {
        if ((mapValuationTypeTranslation == null) || (mapValuationTypeTranslation.size() == 0))
        {
            for (Schema.PicklistEntry schemaPickEntry : SCStockItem__c.ValuationType__c.getDescribe().getPicklistValues())
            {
                mapValuationTypeTranslation.put(schemaPickEntry.getValue().toLowerCase(), schemaPickEntry.getLabel());
            }
        }
        
        if ((value != null) && (mapValuationTypeTranslation.containsKey(value.toLowerCase())))
        {
            return mapValuationTypeTranslation.get(value.toLowerCase());
        }
        
        return value;
    }

    /**
     * Gets a list of possible material movements types.
     */
    public List<SelectOption> getTypeList()
    {
        String paramV5;
        String paramV15;
        Integer nV5;
        Integer nV15;
        String label;
        String language = UserInfo.getLanguage();
        List<SelectOption> domValList = new List<SelectOption>();
        
        for (SCfwDomainValue domainValue :domMatMove.getDomainValues())
        {
            paramV5 = domainValue.getControlParameter('V5');
            paramV15 = domainValue.getControlParameter('V15');
            nV5 = ((null != paramV5) && (paramV5.length() > 0)) ? 
                        Integer.valueOf(paramV5) : 0;
            nV15 = ((null != paramV15) && (paramV15.length() > 0)) ? 
                        Integer.valueOf(paramV15) : 0;
                        
            if (((1 == nV5) && (nV15 > 1)) || 
                domainValue.getId().equals(domMatTransfer))
            {
                label = domainValue.getText(language);
                domValList.add(new SelectOption(domainValue.getId(), 
                                                (null == label) ? domainValue.getId() : label));
            } // if (((1 == nV5) && (nV15 > 1)) || ...
        } // for (SCfwDomainValue domainValue : domMatMove.getDomainValues())
        return domValList;
    } // getTypeList
    
    public List<SelectOption> getValTypeList()
    {
        if ((valTypeList == null) || (valTypeList.size() < 1))
        {
            valTypeList = new List<SelectOption>();
            for (Schema.PicklistEntry schemaPickEntry : SCStockItem__c.ValuationType__c.getDescribe().getPicklistValues())
            {
                valTypeList.add(new SelectOption(schemaPickEntry.getValue(), schemaPickEntry.getLabel()));
            }
        }
        return valTypeList;
    }

    /**
     * Gets a list of possible receiver stocks.
     */
    public List<SelectOption> getReceiverList()
    {
        if (null == receiverList)
        {
        	String key;
            String label;
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            receiverList = new List<SelectOption>();
            
        	//PMS 36113: Lagerort SC45: 0357 - Martin Hollinger: wenn ich im Lager umbuche, sieht er nur die Lager aus 0356, 
        	// old: and Stock__r.Plant__r.Name = :appSettings.DEFAULT_PLANT__c 
        	// new: and Stock__r.Plant__r.Name = :plantName
        	String plantName = ''; 
        	CCWSUtil u = new CCWSUtil();
        	if(u.user != null && u.user.ERPWorkCenterPlant__c !=  null)
        	{
        		plantName = u.user.ERPWorkCenterPlant__c;
        	}
        	else
        	{
        		plantName = appSettings.DEFAULT_PLANT__c;
        	}
            
            // fill thelist with the infos from the assignment
            for (SCResourceAssignment__c assignments: 
                    [Select Id, Name, Stock__c, Stock__r.Plant__r.Name, Stock__r.Name, Stock__r.ValuationType__c,
                            Resource__r.Alias_txt__c, Resource__r.Name, Resource__r.FirstName_txt__c, Resource__r.LastName_txt__c
                       from SCResourceAssignment__c 
                      where Stock__c != :stock.Id 
                        and Stock__r.Plant__r.Name = :plantName
                        and ValidFrom__c <= :Date.today() 
                        and ValidTo__c >= :Date.today()])
            {
                // if the assigment has a stock, add them to the receiver map
                if (null != assignments.Stock__c)
                {
                    key = assignments.Stock__r.Plant__r.Name + '/' + 
                          assignments.Stock__r.Name;
                    label = assignments.Resource__r.Alias_txt__c + ' ' + 
                            assignments.Resource__r.FirstName_txt__c + ' ' + 
                            assignments.Resource__r.LastName_txt__c + 
                            ' (' + key + ')';
                    receiverList.add(new SelectOption(assignments.Stock__c, label));
                } // if (null != assignments.Stock__c)
            } // for (SCResourceAssignment__c assignment: [Select Id, ... from SCResourceAssignment__c where Stock__c = ...])
        } // if (null == receiverList)
        return receiverList;
    } // getReceiverList

    
    /**
     * SCMaterialTransferExtension
     */
    public SCMaterialTransferExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        transferOk = false;
        
        initArticleInfos();
    } // SCMaterialTransferExtension

    /**
     * This method initialize a list with the infos of the articles which can be transferred.
     */
    private void initArticleInfos()
    {
        transInfoList = new List<TransferArticleInfo>();
        
        for (SCStockItem__c stockItem :[Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, 
                                               Article__r.ReplacedBy__r.Name, Article__r.LockType__c, Article__r.ValuationType__c,
                                               Qty__c, MinQty__c, MaxQty__c, Permanent__c, ValuationType__c,
                                               ReplenishmentQty__c, InventoryQty__c, InventoryDate__c, 
                                               MapQty__c, MapPrice__c, MapDate__c, LastModifiedDate 
                                        from SCStockItem__c 
                                        where Stock__c = :stock.Id 
                                          and Qty__c > 0 
                                        order by Article__r.Name])
        {
            TransferArticleInfo artInfo = new TransferArticleInfo();
            artInfo.stockItem = stockItem;
            artInfo.originalvalType = stockItem.ValuationType__c;
            artInfo.valuationType = stockItem.ValuationType__c;
            artInfo.existValType = (stockItem.ValuationType__c != null ? true : false);
            artInfo.hasArticleValType = stockItem.Article__r.ValuationType__c;
            transInfoList.add(artInfo);
        } // for (SCStockItem__C stockItem :[Select Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ...
    } // initArticleInfos
    
    /**
     * If the receivers stock valuation type is true and there exists a transfer valuation type equals to null
     * the user has to choose the valuation types for the transfer items.
     * This function disables this mode.
     */
    public void disableChooseValuationType()
    {
        setStockItemsValType = false;
        
        for (TransferArticleInfo artInfo :transInfoList)
        {
            if (artInfo.existValType == false) { artInfo.valuationType = null; }
        } // for (TransferArticleInfo artInfo :transInfoList)
    }
    
    /**
     * Validates the input. Checks:
     * - if any entry is selected.
     * - the transfer qty is greater than 0
     * - the transfer qty is not greater than the stock qty
     * - if the receivers stock valuation type is true: the transfer valuation type is not null
     * 
     * If the receivers stock valuation type is true and there exists a transfer valuation type equals to null
     * the user has to choose the valuation types for the transfer items.
     */
    private Boolean validate()
    {
        Boolean ret = true;
        Boolean oneSet = false;
        
        receiverValType = 'none';
        try
        {
            receiverValType = ([SELECT ValuationType__c FROM SCStock__c WHERE Id = :receiverStock LIMIT 1].ValuationType__c ? 'true' : 'false');
        }
        catch(Exception e) { }
        
        for (TransferArticleInfo artInfo :transInfoList)
        {
            artInfo.hasError = false;
            if (artInfo.isSet)
            {
                oneSet = true;
                
                if ((receiverValType.equalsIgnoreCase('true')) && (artInfo.hasArticleValType) && (artInfo.ValuationType == null))
                {
                    artInfo.hasError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                               System.Label.SC_msg_TransMatWrongValType));
                    setStockItemsValType = true;
                    ret = false;
                }
                else if (artInfo.qty <= 0)
                {
                    artInfo.hasError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                               System.Label.SC_msg_TransMatNoQty));
                    ret = false;
                }
                else if (artInfo.qty > artInfo.stockItem.Qty__c)
                {
                    artInfo.hasError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                               System.Label.SC_msg_TransMatWrongQty));
                    ret = false;
                }
            } // if (artInfo.isSet)
        } // for (TransferArticleInfo artInfo :transInfoList)
        
        if (!oneSet)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       System.Label.SC_msg_TransMatNoSelection));
            ret = false;
        }
        return ret;
    } // validate
    
    /**
     * Methode is called, when the user clicks [OK].
     * After the validion of the quantities the entries in SCMaterialMovement 
     * are created. If this is successful, entries in SCMateralMovement for
     * the current stock will be processed.
     */
    public PageReference transferMat()
    {
        
        if (validate())
        {
            List<RequestArticleInfo> artInfoSenderList = new List<RequestArticleInfo>();
            List<RequestArticleInfo> artInfoReceiverList = new List<RequestArticleInfo>();
            
            // copy the selected articles in a list used to create the material movements
            for (TransferArticleInfo transInfo :transInfoList)
            {
                if (transInfo.isSet)
                {
                    RequestArticleInfo artInfoSender = new RequestArticleInfo();
                    artInfoSender.article = transInfo.stockItem.Article__r;
                    artInfoSender.valuationType = transInfo.originalvalType;
                    artInfoSender.qty = transInfo.qty;
                    artInfoSenderList.add(artInfoSender);
                    
                    RequestArticleInfo artInfoReceiver = new RequestArticleInfo();
                    artInfoReceiver.article = transInfo.stockItem.Article__r;
                    artInfoReceiver.valuationType = transInfo.ValuationType;
                    artInfoReceiver.qty = transInfo.qty;
                    artInfoReceiverList.add(artInfoReceiver);
                    
                    if ( (receiverValType.equalsIgnoreCase('false')) || (transInfo.hasArticleValType == false) )
                    {
                        artInfoReceiver.valuationType = null;
                    }
                    
                }
            } // for (TransferArticleInfo transInfo :transInfoList)
            
            // first create the material movements for transfer out
            // then create the material movements for transfer in
            // finally book the material movements
            if (createMatMoves(artInfoSenderList, stock.Id, stock.Id, Date.today(), null, 
                               SCfwConstants.DOMVAL_MATSTAT_BOOK, matMoveType, null) &&  
                createMatMoves(artInfoReceiverList, stock.Id, receiverStock, Date.today(), null, 
                               SCfwConstants.DOMVAL_MATSTAT_BOOK, SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_IN, null) && 
                bookMaterial(stock.Id, null) & bookMaterial(receiverStock, null)) 
            {
                transferOk = true;
            }
        }
        
        return null;
    } // transferMat
} // SCMaterialRequestExtension