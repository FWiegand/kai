/*
 * @(#)CCWCMaterialPackageReceivedTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
public class CCWCMaterialPackageReceivedTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    static String MODE_STOCK_ID = 'MODE_STOCK_ID';
    static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
    static String MODE_PLANT = 'MODE_PLANT';
    static String MODE_STOCK = 'MODE_STOCK';



    static testMethod void positiveTestCaseStock1_ID()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK_ID, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void positiveTestCaseStock2_ID()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK_ID, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCaseStock1_ID()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK_ID, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCaseStock2_ID()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK_ID, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void positiveTestCasePlantAndStock1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       


    static testMethod void positiveTestCasePlantAndStock2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCasePlantAndStock1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCasePlantAndStock2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       


    static testMethod void positiveTestCasePlant1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void positiveTestCasePlant2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCasePlant1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCasePlant2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       


    static testMethod void positiveTestCaseStock1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void positiveTestCaseStock2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCaseStock1()
    {
        Test.StartTest();
        String matStatus = '5408';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       

    static testMethod void negativeTestCaseStock2()
    {
        Test.StartTest();
        String matStatus = '5407';
        String matMoveType = '5232'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
        Test.StopTest();
    }       


    static void positiveTestCaseOrder(String mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);
        SCOrder__c order = null;
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();
        if(mode == MODE_STOCK_ID)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
            /*
            ValuationType = ZNEU - new spare part
                            ZGEBROW - used spare part without value
                            ZGEBRMW - used spare part with value
            */
            CCWCTestBase.createMaterialMovementsForPackageReceived(mmList, order.Id, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }
        else
        {
            CCWCTestBase.createMaterialMovementsForPackageReceived(mmList, null, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }   

        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        System.assertEquals(true, plantList.size() > 0);
        
        List<SCStock__c> stockListForName = [select name, id from SCStock__c];
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            static String MODE_STOCK_ID = 'MODE_STOCK_ID';
            static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
            static String MODE_PLANT = 'MODE_PLANT';
            static String MODE_STOCK = 'MODE_STOCK';
        */
        if(mode == MODE_STOCK_ID)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(stockID, async, testMode);      
        }
        else if(mode == MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(plantName, stockName, modeOfReservation, async, testMode);       
        }
        else if(mode == MODE_PLANT)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(plantName, null, modeOfReservation, async, testMode);        
        }   
        else if(mode == MODE_STOCK)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(null, stockName, modeOfReservation, async, testMode);        
        }   
        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        debug('material movement after web call: ' + mmList2);
        System.assertEquals(mmList2.size(), 1);
        //TODO System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'PackageReceived';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'IDoc number';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Material package received created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        debug('material movement after response: ' + mmList3);
        System.assertEquals(mmList3.size(), 1);
        //TODO System.assertEquals('ok', mmList3[0].ERPStatus__c); 
        //TODO System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        //TODO System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        //TODO System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }

    static void negativeTestCaseOrder(String mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);
        SCOrder__c order = null;
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();
        if(mode == MODE_STOCK_ID)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
            /*
            ValuationType = ZNEU - new spare part
                            ZGEBROW - used spare part without value
                            ZGEBRMW - used spare part with value
            */
            CCWCTestBase.createMaterialMovementsForPackageReceived(mmList, order.Id, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }
        else
        {
            CCWCTestBase.createMaterialMovementsForPackageReceived(mmList, null, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }   

        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        System.assertEquals(true, plantList.size() > 0);
        
        List<SCStock__c> stockListForName = [select name, id from SCStock__c];
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            static String MODE_STOCK_ID = 'MODE_STOCK_ID';
            static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
            static String MODE_PLANT = 'MODE_PLANT';
            static String MODE_STOCK = 'MODE_STOCK';
        */
        if(mode == MODE_STOCK_ID)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(stockId, async, testMode);      
        }
        else if(mode == MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(plantName, stockName, modeOfReservation, async, testMode);       
        }
        else if(mode == MODE_PLANT)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(plantName, null, modeOfReservation, async, testMode);        
        }   
        else if(mode == MODE_STOCK)
        {
            requestMessageID = CCWCMaterialPackageReceived.callout(null, stockName, modeOfReservation, async, testMode);        
        }   
        
        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        debug('material movement after web call: ' + mmList2);
        System.assertEquals(mmList2.size(), 1);
        //TODO System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'PackageReceived';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'IDoc number';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Material package received not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        debug('material movement after response: ' + mmList3);
        System.assertEquals(mmList3.size(), 1);
        //TODO System.assertEquals('error', mmList3[0].ERPStatus__c); 
        //TODO System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        //TODO System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        //TODO System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }


/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}