/** ===================================================================
 *  @SCHelperTestClass3.cls
 *  @author Alexander Wagner <awagner@gms-online.de>
 *  @email......: awagner@gms-online.de
 *  @version....: V0.1
 *  Date.......: 23.05.2012
 *  Description: This class includes methods to create testdata.
 *               They return a map of objects, which can be catched with objectname
 *               -  You can call this method with:
 *                  Map<String,SObject> ObjectMap =
 *                  SCHelperTestClass.CreateTestData(boolean);
 *                  SObject a = ObjectMap.get('Account');
 *  Objects....: Account,eps_Loyalty__c,eps_Loyalty_Booking__c
 *  ===================================================================
 */

public class SCHelperTestClass3
{
    public static SCApplicationSettings__c appSettings { get; private set; }
    public static SCServiceProcessSettings__c servSettings { get; private set; }

    static
    {
        appSettings = new SCApplicationSettings__c();
        servSettings = new SCServiceProcessSettings__c();
    }

    /**
     * Create custom settings needed for testing.
     *
     * @country  String  value determines for what country the settings should be created
     * @doInsert Boolean value for insert
     */
    public static void createCustomSettings(String country, boolean doInsert)
    {
        // first create data which are used in the settings
        /*if (0 == SCHelperTestClass.articles.size())
        {
            
            SCHelperTestClass.createTestArticles(true);
        }*/
        if (null == SCHelperTestClass.brand)
        {
            SCHelperTestClass.createBrandObject(false, true);
        }
        if (null == SCHelperTestClass.plant)
        {
            SCHelperTestClass.createTestPlant(true);
        }
        if (0 == SCHelperTestClass.stocks.size())
        {
            SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        }
        if (null == SCHelperTestClass.calendar)
        {
            SCHelperTestClass.createTestCalendar(true);
        }
        if (null == SCHelperTestClass.businessUnit)
        {
            SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[0].Id, 
                                                   SCHelperTestClass.calendar.Id, true);
        }
        
        if (null == SCHelperTestClass.priceList)
        {
            SCHelperTestClass.createPricelistEx(true);
        }
        
        // now set the values for the main entries
        appSettings = new SCApplicationSettings__c(
                ADD_REPAIRCODE_TEXT__c                     = '1000100000', 
                ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c      = false, 
                CONTRACT_ACCOUNTING_COSTS_PER_MINUTE__c    = 1.8, 
                CONTRACT_ACCOUNTING_COSTS_TRAVEL__c        = 48.0, 
                CONTRACT_BTCS_MAX_CYCLES__c                = 0, 
                CONTRACT_SCHEDULING_DAYS__c                = 7, 
                //CREDITNOTE_ARTNO_TRANSACTION_FEE__c        = SCHelperTestClass.articles[7].Name, 
                CREDITNOTE_ARTNO_TRANSACTION_FEE__c        = '899907',
                CurrencyIsoCode                            = 'EUR', 
                CUSTOMERPROPERTY_NOPARKING_FOLLOWUP1__c    = '2855', 
                CUSTOMERPROPERTY_NOPARKING_FOLLOWUP2__c    = '2955', 
                CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP1__c     = '2855', 
                CUSTOMERPROPERTY_NOPERMIT_FOLLOWUP2__c     = '2956', 
                CUSTOMERPROPERTY__c                        = '0', 
                DEFAULT_ARTICLESEARCH_RESULT_LIMT__c       = 500, 
                DEFAULT_BRAND__c                           = SCHelperTestClass.brand.ID2__c, 
                DEFAULT_BUSINESSUNITNO__c                  = SCHelperTestClass.businessUnit.Name, 
                DEFAULT_COUNTRY__c                         = 'NL', 
                DEFAULT_CUSTOMERPREFINTERVALL__c           = 30, 
                DEFAULT_CUSTOMERPREFSTART__c               = 0, 
                DEFAULT_CUSTOMERPRIO__c                    = '2102', 
                DEFAULT_CUSTOMERTIMEWINDOW__c              = '12401', 
                DEFAULT_DISTANCEZONE__c                    = null, 
                DEFAULT_FAILURETYPE__c                     = '3601', 
                DEFAULT_INBOX_ORDERFILTER__c               = '5501,5503,5509,5510', 
                DEFAULT_INBOX_TIMEFILTER__c                = '20', 
                DEFAULT_INVOICINGSUBTYPE__c                = '180001', 
                DEFAULT_INVOICINGTYPE__c                   = '170001', 
                //DEFAULT_MAINTENANCE_ARTICLE_NO__c        = '899999',
                DEFAULT_ORDERITEM_DURATION__c              = 60, 
                 
                DEFAULT_ORDERTYPE__c                       = '5701', 
                DEFAULT_PAYMENTTYPE__c                     = '13202', 
                DEFAULT_PLANT__c                           = SCHelperTestClass.plant.name, 
                DEFAULT_PRICELIST__c                       = SCHelperTestClass.priceList.name, 
                DEFAULT_REPLENISHMENT_TYPE__c              = '375002', 
                DEFAULT_ROUNDMINUTES_GRID__c               = 15, 
                DEFAULT_SERIALNUMBERTIMER__c               = 3000, 
                 
                DISABLETRIGGER_ORDERAFTERUPDATE__c         = false, 
                DISABLETRIGGER__c                          = false, 
                DISTANCE_FACTOR__c                         = 1.0, 
                DISTANCE_UNIT__c                           = 0, 
                ENGINEERSTANDBY_EVALUATION__c              = true, 
                GOOGLE_GEOCODE_CLIENTID__c                 = 'ClientID',
                GOOGLE_GEOCODE_ENDPOINT__c                 = 'http://maps.google.com/maps/api/geocode',
                GOOGLE_GEOCODE_PRIVATE_KEY__c              = 'Password',
                ID2__c                                     = 'EPS Test Application Settings', 
                INSTBASE_INSTDATE_DAYSINFUTURE__c          = 14, 
                INSTBASE_INSTDATE_EARLIESTDATE__c          = Date.newInstance(2000, 1, 1), 
                INSTBASE_MODIFY_TYPE__c                    = 5.0, 
                INSTBASE_PURCHDATE_DAYSINFUTURE__c         = 14, 
                INSTBASE_PURCHDATE_EARLIESTDATE__c         = Date.newInstance(2000, 1, 1), 
                INSURANCE_HOMESERVE_FOLLOWUP2__c           = null, 
                INSURANCE_HOMESERVE_FOLLOWUP__c            = null, 
                INVOICINGTYPE_PAYCOSTS__c                  = '170001=1|170002=|170003=|170004=|170005=|170006=', 
                LOCKTYPE_IMAGES__c                         = '50001=0|50002=1|50003=1', 
                MAPVIEW_MAXSEARCHRESULTS__c                = 100, 
                MATERIALAVAILABILITY__c                    = '2',
                MATERIALAVAILABILITY_SERVICE_ENDPOINT__c   = null, 
                MATERIALAVAILABILITY_SERVICE_USER__c       = null, 
                MATERIALAVAILABILITY_SERVICE_PWD__c        = null, 
                MATERIALAVAILABILITY_SERVICE_TYPE__c       = 'SAP', 
                MATERIALAVAILABILITY_SERVICE_MODE__c       = null, 
                MATERIAL_BOOKING__c                        = '0',  
                MATERIAL_ENABLE_DELIVCONF__c               = true, 
                MATERIAL_REORDER_LOCKTYPE__c               = '50201, 50202', 
                MESSAGEONTODAY__c                          = false, 
                NOTIFICATION_CODE7_AMOUNT__c               = 87.0, 
                ORDERCREATION_APPOINTMENTWARNING__c        = '5701,5707', 
                ORDERCREATION_COMBOX__c                    = '0', 
                ORDERCREATION_CUSTPREFSTARTS__c            = '5707=14', 
                ORDERCREATION_MINDURATION__c               = 15, 
                ORDERCREATION_OPENINSTALLEDBASE__c         = 2, 
                ORDERCREATION_ORDTYPES_FOR_CASH_PAYMENT__c = '5701,5707', 
                ORDERCREATION_PAGE__c                      = 'SCOrderCreationDE', 
                ORDERCREATION_PRIO_DETERMINATION_DEBUG__c  = false, 
                ORDERCREATION_PRIO_DETERMINATION__c        = 0, 
                ORDERCREATION_SHOW_ERRSYMS__c              = false, 
                ORDERCREATION_SHOW_HISTORY__c              = false, 
                ORDERCREATION_USE_PRODUCTSKILL__c          = 0, 
                ORDERFEEDBACK_ARTNO_TRAVELTIME__c          = '899101',
                ORDERFEEDBACK_ARTNO_WORKTIME__c            = '899102',
                ORDERFEEDBACK_AUTOPARTBOOKING__c           = 2, 
                ORDERFEEDBACK_AUTOREPLENISHMENTTYPE__c     = '375001', 
                ORDERFEEDBACK_FOLLOWUP1__c                 = null, 
                ORDERFEEDBACK_FOLLOWUP2__c                 = null, 
                ORDERFEEDBACK_TIMEREPORT__c                = false, 
                ORDERLINE_ACCESS_RIGHTS__c                 = 'ADDPARTS,ADDCHARGES,ADDPAYMENT,DELLINE,SELPRICELIST,DOPAYMENT,CHGQTY,CHGNETPRICE,CHGDISCOUNT,CHGINVTYPE,CHGTAX,ORDERMAT', 
                ORDERLINE_ARTNO_PAYMENT__c                 = '899995', 
                ORDERLINE_QTY_THRESHOLD__c                 = 9, 
                ORDERLINE_VIEW_TYPES__c                    = '52702,52714', 
                OrderRoleMode__c                           = 'CASR', 
                ORDERSEARCH_CUSTOMORDERPAGE__c             = '1', 
                ORDERSEARCH_MAXRESULTS__c                  = 300, 
                PAYMENTSERVICE_FOLLOWUP2__c                = null, 
                PAYMENTSERVICE_FOLLOWUP__c                 = null, 
                POSTCODEAREA_EVALUATION__c                 = true, 
                PRICING_MAX_DISCOUNT__c                    = 100, 
                PRINTINGTEMPLATE_CONTRACT__c               = null, 
                PRINTINGTEMPLATE_ORDER__c                  = null, 
                QUALIFICATION_CREATE__c                    = 'PRODUCTMODEL', 
                QUALIFICATION_DISABLEBRAND__c              = false, 
                QUALIFICATION_DISABLELANGUAGE__c           = false, 
                QUALIFICATION_DISABLEORDTYPE__c            = false, 
                QUALIFICATION_UNITCLASSEXCEPTIONS__c       = null, 
                QUALIFICATION_UNITTYPEEXCEPTIONS__c        = null, 
                QUALIFICATION_USE_ACCS_SCHEDULING__c       = 1, 
                RESOURCE_CHANGE_ALLOWED__c                 = 0, 
                RISKCLASS_IMAGES__c                        = '51001=0|51002=3', 
                SCHEDULING_MULTIPLE_APPOINTMENTS__c        = '1', 
                SEARCHLOG_MINTIME__c                       = 1000, 
                STOCK_ALLOWNEGATIVESTOCK__c                = true, 
                STOCK_ITEM_FILTER_EMPTY__c                 = true, 
                TAXACCOUNT__c                              = '50501=1|50502=0|50503=2|50504=0', 
                TIMEREPORT_CHRONOLOGICAL__c                = true, 
                TIMEREPORT_DEFAULTDURATION__c              = 60, 
                TIMEREPORT_EDITCLOSED__c                   = false, 
                TIMEREPORT_MAXDURATION__c                  = 1200, 
                TIMEREPORT_MAXFUTURE__c                    = 600, 
                TIMEREPORT_MAXOPENDAYS__c                  = 1, 
                TIMEREPORT_MULTIPERDAY__c                  = true, 
                TIMEREPORT_OPENDAYBUTTON__c                = true, 
                TIMEREPORT_OPENDAYSINTHEPAST__c            = true, 
                TIMEREPORT_OPTIMIZEBUTTON__c               = false, 
                TIMEREPORT_PAST_PROCESS__c                 = false, 
                VALIDATE_PICKLIST_VALUES__c                = '1', 
                VIPLEVEL_IMAGES__c                         = 'silver=2|gold=3|platin=3', 
                WizardPersonAccount__c                     = false
            );
        
        servSettings = new SCServiceProcessSettings__c(
                ID2__c                                     = 'EPS Test Service Process Settings', 
                ORDCREATIONPAGE_CONTRACT__c                = true, 
                ORDCREATIONPAGE_DISPMAN__c                 = 1, 
                ORDCREATIONPAGE_ERRORTEXT__c               = 1, 
                ORDPROCESSPAGE_REPCODE_ERRSYM1__c          = true, 
                PAYMENT_MODE__c                            = 'POSTPAYMENT'
            );
        
        // finally set the country specific values
        if (country.equalsIgnoreCase('DE'))
        {
            appSettings.DEFAULT_COUNTRY__c                      = 'DE'; 
            appSettings.ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c   = true;
            appSettings.CONTRACT_ACCOUNTING_COSTS_PER_MINUTE__c = 1.0;
            appSettings.CONTRACT_ACCOUNTING_COSTS_TRAVEL__c     = 50.0;
            appSettings.DEFAULT_CUSTOMERPREFINTERVALL__c        = 7;
            appSettings.ENGINEERSTANDBY_EVALUATION__c           = false;
            appSettings.MATERIAL_ENABLE_DELIVCONF__c            = false;
            appSettings.ORDERCREATION_SHOW_ERRSYMS__c           = true;
            appSettings.ORDERFEEDBACK_TIMEREPORT__c             = true;
            appSettings.QUALIFICATION_CREATE__c                 = 'UNITTYP';
            appSettings.QUALIFICATION_DISABLEBRAND__c           = true;
            appSettings.QUALIFICATION_DISABLELANGUAGE__c        = true;
            appSettings.QUALIFICATION_DISABLEORDTYPE__c         = true;
            appSettings.QUALIFICATION_UNITTYPEEXCEPTIONS__c     = '021,022,103,121';
            appSettings.TIMEREPORT_CHRONOLOGICAL__c             = false;
            appSettings.TIMEREPORT_DEFAULTDURATION__c           = 30;
            appSettings.TIMEREPORT_EDITCLOSED__c                = true;
            appSettings.VALIDATE_PICKLIST_VALUES__c             = '0';
        } // if (country.equalsIgnoreCase('DE'))
        else if (country.equalsIgnoreCase('NL'))
        {
            appSettings.DEFAULT_COUNTRY__c                      = 'NL'; 
            appSettings.ADD_REPAIRCODE_TEXT__c                  = '1100100000';
            appSettings.DEFAULT_CUSTOMERPREFINTERVALL__c        = 7;
            appSettings.DEFAULT_CUSTOMERPRIO__c                 = '2101';
            appSettings.DEFAULT_INBOX_TIMEFILTER__c             = '11';
            appSettings.ENGINEERSTANDBY_EVALUATION__c           = false;
            appSettings.MATERIAL_ENABLE_DELIVCONF__c            = false;
            appSettings.ORDERCREATION_PAGE__c                   = 'SCOrderCreation';
            appSettings.ORDERCREATION_SHOW_ERRSYMS__c           = true;
            appSettings.ORDERCREATION_SHOW_HISTORY__c           = true;
            appSettings.POSTCODEAREA_EVALUATION__c              = false;
            appSettings.TIMEREPORT_MULTIPERDAY__c               = false;
            appSettings.TIMEREPORT_OPENDAYSINTHEPAST__c         = false;
            appSettings.VALIDATE_PICKLIST_VALUES__c             = '0';
        } //else if (country.equalsIgnoreCase('NL'))
        else if (country.equalsIgnoreCase('GB'))
        {
            appSettings.DEFAULT_COUNTRY__c                      = 'GB'; 
            appSettings.ADD_REPAIRCODE_TEXT__c                  = '1010100000';
            appSettings.ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c   = true;
            appSettings.CUSTOMERPROPERTY__c                     = '1';
            appSettings.CUSTOMORDERPAGE_GB__c                   = 1;
            appSettings.DEFAULT_CUSTOMERPREFINTERVALL__c        = 7;
            appSettings.DEFAULT_CUSTOMERPRIO__c                 = '2101';
            appSettings.DEFAULT_DISTANCEZONE__c                 = '907';
            appSettings.DEFAULT_INBOX_TIMEFILTER__c             = '11';
            appSettings.DISTANCE_UNIT__c                        = 1;
            appSettings.ENGINEERSTANDBY_EVALUATION__c           = false;
            appSettings.MATERIAL_BOOKING__c                     = '1';  
            appSettings.MATERIAL_ENABLE_DELIVCONF__c            = false;
            appSettings.ORDERCREATION_PAGE__c                   = 'SCOrderCreationGB';
            appSettings.ORDERCREATION_SHOW_ERRSYMS__c           = true;
            appSettings.OrderRoleMode__c                        = 'SRCA';
            appSettings.POSTCODEAREA_EVALUATION__c              = false;
            appSettings.QUALIFICATION_CREATE__c                 = 'UNITTYP';
            appSettings.QUALIFICATION_DISABLEBRAND__c           = true;
            appSettings.QUALIFICATION_DISABLELANGUAGE__c        = true;
            appSettings.QUALIFICATION_DISABLEORDTYPE__c         = true;
            appSettings.QUALIFICATION_UNITTYPEEXCEPTIONS__c     = '021,022,103,121';
            appSettings.QUALIFICATION_USE_ACCS_SCHEDULING__c    = 0;
            appSettings.STOCK_ALLOWNEGATIVESTOCK__c             = false;
            appSettings.TIMEREPORT_OPENDAYBUTTON__c             = false;
            appSettings.VALIDATE_PICKLIST_VALUES__c             = '0';
            
            servSettings.ORDCREATIONPAGE_DISPMAN__c             = 0;
            servSettings.ORDCREATIONPAGE_ERRORTEXT__c           = 0;
            servSettings.PAYMENT_MODE__c                        = 'PREPAYMENT';
        } // else if (country.equalsIgnoreCase('GB'))
        else if (country.equalsIgnoreCase('AT'))
        {
            appSettings.DEFAULT_COUNTRY__c                      = 'AT'; 
            appSettings.ARTICLESEARCH_FROMSTOCK_AUTOSEARCH__c   = true;
            appSettings.CONTRACT_ACCOUNTING_COSTS_PER_MINUTE__c = 1.0;
            appSettings.CONTRACT_ACCOUNTING_COSTS_TRAVEL__c     = 50.0;
            appSettings.DEFAULT_CUSTOMERPREFINTERVALL__c        = 7;
            appSettings.ENGINEERSTANDBY_EVALUATION__c           = false;
            appSettings.MATERIAL_ENABLE_DELIVCONF__c            = false;
            appSettings.ORDERCREATION_PAGE__c                   = 'SCOrderCreation';
            appSettings.ORDERCREATION_SHOW_ERRSYMS__c           = true;
            appSettings.ORDERCREATION_SHOW_HISTORY__c           = true;
            appSettings.ORDERFEEDBACK_TIMEREPORT__c             = true;
            appSettings.OrderRoleMode__c                        = 'CAIR';
            appSettings.POSTCODEAREA_EVALUATION__c              = false;
            appSettings.QUALIFICATION_CREATE__c                 = 'UNITTYP';
            appSettings.QUALIFICATION_DISABLEBRAND__c           = true;
            appSettings.QUALIFICATION_DISABLELANGUAGE__c        = true;
            appSettings.QUALIFICATION_DISABLEORDTYPE__c         = true;
            appSettings.QUALIFICATION_UNITTYPEEXCEPTIONS__c     = '021,022,103,121';
            appSettings.QUALIFICATION_USE_ACCS_SCHEDULING__c    = 0;
            appSettings.STOCK_ITEM_FILTER_EMPTY__c              = false;
            appSettings.TIMEREPORT_CHRONOLOGICAL__c             = false;
            appSettings.TIMEREPORT_DEFAULTDURATION__c           = 30;
            appSettings.TIMEREPORT_EDITCLOSED__c                = true;
            appSettings.VALIDATE_PICKLIST_VALUES__c             = '0';
        } // else if (country.equalsIgnoreCase('AT'))
        
        if (doInsert)
        {
            insert appSettings;
            insert servSettings;
        }

        // setting CCSettings__c.MantenanceDeleteOnReturn__c
        // used by the trigger SCOrderAfterUpdate
        CCSettings__c ccSettings = new CCSettings__c();
        Boolean seeAllData = false;
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
    }
} // SCHelperTestClass3