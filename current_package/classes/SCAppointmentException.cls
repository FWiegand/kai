/**
 * @(#)SCAppointmentException.cls    ASE1.0 hs 20.09.2010
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: SCAppointmentException.cls 7692 2010-01-18 16:18:41Z hschroeder $
 * 
 */
/**
  * Exception for scheduling errors
  */
public class SCAppointmentException extends Exception 
{
    /** 
     * Return exceptions for the specified error codes in the SOAP exception
     *
     * @param e  the ase SOAP exception
     * @return  exceptions for use with "addMessage"
     * @author  hs
     */
    public static SCAppointmentException[] getExceptions(Exception e)
    {
        String msg = e.getMessage();
        String errCodes = 'errcode(s):';
        SCAppointmentException[] exceptions;
        
        if (msg.contains(errCodes))
        {
            // parse ASE message for error codes
            Integer pos0 = msg.indexOf(errCodes);
            Integer pos1 = msg.lastIndexOf('faultcode');

            msg = msg.substring(pos0 + errCodes.length(), pos1);
            String[] codes = msg.split(',');

            exceptions = new SCAppointmentException[codes.size()];
        
            Integer i = 0;
            for (String code : codes)
            {
                exceptions[i++] = new SCAppointmentException(getLabel(code.trim()));
            }            
        }
        else
        {
            // issue callout exception directly
            exceptions = new SCAppointmentException[1];
            exceptions[0] = new SCAppointmentException(e);
        }
        return Exceptions;
    }
    
    /**
     * Map error codes to messages via custom labels
     *
     * @param code  the error code
     * @return  the error message
     * @author  hs
     */
    public static String getLabel(String code)
    {
        if (code == '201') return System.Label.SC_msg_X201;
        if (code == '202') return System.Label.SC_msg_X202;
        if (code == '203') return System.Label.SC_msg_X203;
        if (code == '204') return System.Label.SC_msg_X204;
        if (code == '205') return System.Label.SC_msg_X205;
        if (code == '206') return System.Label.SC_msg_X206;
        if (code == '207') return System.Label.SC_msg_X207;
        if (code == '208') return System.Label.SC_msg_X208;
        if (code == '209') return System.Label.SC_msg_X209;
        if (code == '210') return System.Label.SC_msg_X210;
        if (code == '211') return System.Label.SC_msg_X211;
        if (code == '212') return System.Label.SC_msg_X212;
        if (code == '213') return System.Label.SC_msg_X213;
        if (code == '214') return System.Label.SC_msg_X214;
        if (code == '215') return System.Label.SC_msg_X215;
        if (code == '216') return System.Label.SC_msg_X216;
        if (code == '217') return System.Label.SC_msg_X217;
        if (code == '218') return System.Label.SC_msg_X218;
        if (code == '219') return System.Label.SC_msg_X219;
        if (code == '220') return System.Label.SC_msg_X220;
        if (code == '221') return System.Label.SC_msg_X221;
        if (code == '224') return System.Label.SC_msg_X224;
        if (code == '225') return System.Label.SC_msg_X225;
        if (code == '226') return System.Label.SC_msg_X226;
        if (code == '227') return System.Label.SC_msg_X227;
        if (code == '228') return System.Label.SC_msg_X228;
        
        if (code == '301') return System.Label.SC_msg_X301;
        if (code == '302') return System.Label.SC_msg_X302;
        
        return 'Unknown custom label code ' + code + '.';
    }
}