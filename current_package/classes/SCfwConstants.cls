public class SCfwConstants {

//Namespace
public static String NamespacePrefix
{
    get
    {
        if (NamespacePrefix == null)
        {
            Type t = SCfwConstants.class;
            String s = t.getName();
            s = (s.contains('.')) ? s.split('\\.')[0] : null;
            
            NamespacePrefix = (s == null) ? '' : (s + '__');
        }
        return NamespacePrefix;
    }
    private set;
}

// Domäne Unit (200)
//=================
public final static String DOMVAL_UNIT_PIECE = '201';
public final static String DOMVAL_UNIT_HOURS = '202';
public final static String DOMVAL_UNIT_KILOMETERS = '203';
public final static String DOMVAL_UNIT_KG = '204';
public final static String DOMVAL_UNIT_LITERS = '205';
public final static String DOMVAL_UNIT_SQUAREMETERS = '206';
public final static String DOMVAL_UNIT_METERS = '207';


// Domäne APPOINTMENTPLANNINGTYPE (800)
//=====================
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_MANUAL = '801';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_AUTO = '802';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_AUTOPRIO = '803';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED = '804';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED = '805';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_MAN_MOVED = '806';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_AUTO_EMPLFIX = '807';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_MAP = '808';
public final static String DOMVAL_APPOINTMENTPLANNINGTYPE_MAP_MOVED = '809';


// Domäne DistZone (900)
//======================
public final static String DOMVAL_DISTZONE_DEFAULT = '901';
public final static String DOMVAL_DISTZONE_NO_CALCULATION = '901';


// Domäne ContrStat (1000)
//=======================
public final static String DOMVAL_CONTRSTAT_CREATED = '1001';
public final static String DOMVAL_CONTRSTAT_ACTIV = '1002';
public final static String DOMVAL_CONTRSTAT_CANCELED = '1003';
public final static String DOMVAL_CONTRSTAT_FINISHED = '1004';


// Domäne Curr (2000)
//==================
public final static String DOMVAL_CURR_GBP = 'GBP';
public final static String DOMVAL_CURR_USD = 'USD';
public final static String DOMVAL_CURR_XEU = 'EUR';


// Domäne OrderPrio (2100)
//======================
public final static String DOMVAL_ORDERPRIO_DEFAULT = '2101';


// Domäne FollowUp1
//=================
public final static String DOMVAL_FOLLOWUP1_NONE = '2801';


// Domäne FollowUp2 (2900)
//======================
public final static String DOMVAL_FOLLOWUP2_NONE = '2901';


// Domäne CashBookTyp (4900)
//=========================
public final static String DOMVAL_CASHBOOKTYP_CLOSE = '4901';
public final static String DOMVAL_CASHBOOKTYP_TRANSIT = '4902';
public final static String DOMVAL_CASHBOOKTYP_COLLECTION = '4903';
public final static String DOMVAL_CASHBOOKTYP_PURCHASE = '4904';
public final static String DOMVAL_CASHBOOKTYP_STORNO = '4905';
public final static String DOMVAL_CASHBOOKTYP_PERSONELL_CLOSE = '4906';


// Domäne MatMoveTyp (5200)
//========================
public final static String DOMVAL_MATMOVETYP_MATREQUEST_CUST = '5201';
public final static String DOMVAL_MATMOVETYP_MATREQUEST_EMPL = '5202';
public final static String DOMVAL_MATMOVETYP_CONSUMPTION = '5204';
public final static String DOMVAL_MATMOVETYP_DELIVERY = '5207';
public final static String DOMVAL_MATMOVETYP_NEW = '5208';
public final static String DOMVAL_MATMOVETYP_SCRAP = '5209';
public final static String DOMVAL_MATMOVETYP_TRANSFER_IN = '5214';
public final static String DOMVAL_MATMOVETYP_TRANSFER_OUT = '5215';
public final static String DOMVAL_MATMOVETYP_RETURN_OLD = '5216';
public final static String DOMVAL_MATMOVETYP_RETURN_NEW = '5217';
public final static String DOMVAL_MATMOVETYP_CONSUMPTION_UNDO = '5218';
public final static String DOMVAL_MATMOVETYP_CORRECTION_IN = '5219';
public final static String DOMVAL_MATMOVETYP_CORRECTION_OUT = '5220';
public final static String DOMVAL_MATMOVETYP_REPLENISHMENT = '5222';
public final static String DOMVAL_MATMOVETYP_INVENTORY_IN = '5225';
public final static String DOMVAL_MATMOVETYP_INVENTORY_OUT = '5226';
public final static String DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION = '5227';
public final static String DOMVAL_MATMOVETYP_RESETSTOCKBALANCE = '5228';
public final static String DOMVAL_MATMOVETYP_CONVERSION_IN = '5229';
public final static String DOMVAL_MATMOVETYP_CONVERSION_OUT = '5230';
public final static String DOMVAL_MATMOVETYP_TRANSFER_DELIVERY = '5231';
public final static String DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED = '5232';


// Domäne Channel (5300)
//=====================
public final static String DOMVAL_CHANNEL_LETTER = '5301';
public final static String DOMVAL_CHANNEL_FAX = '5302';
public final static String DOMVAL_CHANNEL_PHONE = '5303';
public final static String DOMVAL_CHANNEL_MIGRATION_FROM_EXTERN = '5304';
public final static String DOMVAL_CHANNEL_IMPORT_FROM_EXTERN = '5305';
public final static String DOMVAL_CHANNEL_EMAIL = '5306';
public final static String DOMVAL_CHANNEL_FROM_CONTRACT_OR_MAINTENANCE = '5307';
public final static String DOMVAL_CHANNEL_OFFLINE_FROM_RESOURCE = '5308';
public final static String DOMVAL_CHANNEL_REPORTED_BY_INSTALLED_BASE = '5309';
public final static String DOMVAL_CHANNEL_SALES = '5310';
public final static String DOMVAL_CHANNEL_POSTPROCESSED = '5311';
public final static String DOMVAL_CHANNEL_FOLLOW_UP_ORDER = '5315';


// Domäne MatStat (5400)
//=====================
public final static String DOMVAL_MATSTAT_NOMATERIAL = '5401';
public final static String DOMVAL_MATSTAT_ORDER = '5402';
public final static String DOMVAL_MATSTAT_ORDERED = '5403';
public final static String DOMVAL_MATSTAT_DELIVERED = '5405';
public final static String DOMVAL_MATSTAT_BOOK = '5407';
public final static String DOMVAL_MATSTAT_BOOKED = '5408';
public final static String DOMVAL_MATSTAT_CANCEL = '5409';
public final static String DOMVAL_MATSTAT_CANCELLED = '5410';
public final static String DOMVAL_MATSTAT_ORDERABLE = '5413';
public final static String DOMVAL_MATSTAT_PARTIALDELIVERY = '5418';
public final static String DOMVAL_MATSTAT_IN_TRANSIT = '5423';


// Domäne OrderStatus (5500)
//==========================
public final static String DOMVAL_ORDERSTATUS_OPEN = '5501';
public final static String DOMVAL_ORDERSTATUS_PLANNED = '5502';
public final static String DOMVAL_ORDERSTATUS_ACCEPTED = '5503';
public final static String DOMVAL_ORDERSTATUS_COMPLETED = '5506';
public final static String DOMVAL_ORDERSTATUS_CANCELLED = '5507';
public final static String DOMVAL_ORDERSTATUS_CLOSEDFINAL = '5508';
public final static String DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK = '5509';
public final static String DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING = '5510';

//ET: PMS 33340 28.08.2012
public final static String DOMVAL_ORDERSTATUS_TRANSFERRED_MOBILE = '5511';
public final static String DOMVAL_ORDERSTATUS_PROCESSING_MOBILE = '5512';
public final static String DOMVAL_ORDERSTATUS_FINISHED_MOBILE = '5513';
public final static String DOMVAL_ORDERSTATUS_CANCEL_MOBILE = '5514';

public final static String DOMVAL_ORDERSTATUS_PENDING_ORDERS = '5501,5503,5509,5510';
public final static String DOMVAL_ORDERSTATUS_NOT_COMPLETED = '5501,5502,5503,5509,5510';
public final static String DOMVAL_ORDERSTATUS_NOT_SCHEDULEABLE = '5507,5508';

public final static String DOMVAL_ORDERSTATUS_FOR_WORKSHOP_APP = '5501,5502,5503,5509,5510';
public final static String DOMVAL_ORDERSTATUS_FOR_WORKSHOP_APP_CLOSED = '5506,5508';
public final static String DOMVAL_ORDERSTATUS_FOR_ORDER_CLOSE_EX = '5506,5508';

//SALES
public final static String DOMVAL_ORDERSTATUS_APPROVALPENDING = '5520';                 // SALES ORDERS ONLY (!!) 
public final static String DOMVAL_ORDERSTATUS_PARTIALDELIVERY = '5521';                 // SALES ORDERS ONLY (!!) 


// Domäne OrderType (5700)
//========================
public final static String DOMVAL_ORDERTYPE_DEFAULT           = '5701';
public final static String DOMVAL_ORDERTYPE_INHOUSE           = '5702';
public final static String DOMVAL_ORDERTYPE_SALESORDER        = '5703'; // reserved
public final static String DOMVAL_ORDERTYPE_CONSULTING        = '5704'; // reserved
public final static String DOMVAL_ORDERTYPE_COMISSIONING      = '5705';
public final static String DOMVAL_ORDERTYPE_SPAREPARTSALES    = '5706';
public final static String DOMVAL_ORDERTYPE_MAINTENANCE       = '5707';
public final static String DOMVAL_ORDERTYPE_CREDIT            = '5708';
public final static String DOMVAL_ORDERTYPE_INVOICE_COPY      = '5709';
public final static String DOMVAL_ORDERTYPE_INVOICE_COLLECTIVE          = '5710'; // reserved        
public final static String DOMVAL_ORDERTYPE_INVOICE_CONTRACT            = '5711'; // reserved        
public final static String DOMVAL_ORDERTYPE_INVOICE_CONTRACT_COLLECTIVE = '5712'; // reserved        
public final static String DOMVAL_ORDERTYPE_INVOICE_INSURANCE           = '5713'; // reserved        


// GMSGB Replacement public final static String DOMVAL_ORDERTYPE_NOT_SCHEDULEABLE = '5703,5708,5709,5710,5711,5712,5713';
public final static String DOMVAL_ORDERTYPE_NOT_SCHEDULEABLE = '5703,5708,5709,5710,5712,5713';

public final static String DOMVAL_ORDERTYPE_FOR_CREDITNOTE = '5701,5702,5705,5706,5707,5709,5726';

//CCEAG
//5701 ZC02-1-Reparatur
//5731 ZC02-2 Preisumstellung
//5732 ZC02-3 Produktumstellung
//5733 ZC02-4 Standortwechsel
//5734 ZC02-5 Umbau PEM/POM
//5735 ZC02-6 Wiederinbetriebnahme
//5736 ZC02-7 Ausserbetriebnahme
//5737 ZC02-8 sonstige Änderungen
public final static String DOMVAL_ORDERTYPE_ALL_ZC02 = '5701,5731,5732,5733,5734,5735,5736,5737';

//CCEAG 
//5738 ZC04-1 Ortsbesichtigung
//CCE will doch keine Ortsbesichtigung public final static String DOMVAL_ORDERTYPE_SITESURVEY = '5738';

//CCEAG 

//5701 ZC02-1-Repair                                            
public final static String DOMVAL_ORDERTYPE_REPAIR = '5701';

//5702 ZC15-Workshop                                            
public final static String DOMVAL_ORDERTYPE_WORKSHOP = '5702';

//5705 ZC04-Aufstellung
public final static String DOMVAL_ORDERTYPE_INSTALLATION = '5705';

//5711 ZC16 Austausch
public final static String DOMVAL_ORDERTYPE_REPLACEMENT = '5711';




//--> old values (to be removed after deployment 01.07.2011)
public final static String DOMVAL_ORDERTYPE_MAINTENANCE_ORDER = '5707';
//<--




// DOM_APPOINTMENTCLASS (7300)
//=======================
public final static String DOMVAL_APPOINTMENTCLASS_ORDER = '7301';
public final static String DOMVAL_APPOINTMENTCLASS_EMPLOYEE = '7302';


// DOM_APPOINTMENTTYP (7400)
//=====================
public final static String DOMVAL_APPOINTMENTTYP_JOB= '7401';
public final static String DOMVAL_APPOINTMENTTYP_HOMERIDE = '7402';
public final static String DOMVAL_APPOINTMENTTYP_WORKTIME = '7403';
public final static String DOMVAL_APPOINTMENTTYP_BREAKTIME = '7404';
public final static String DOMVAL_APPOINTMENTTYP_DRIVETIME = '7405';
public final static String DOMVAL_APPOINTMENTTYP_OVERNIGHT = '7406';
public final static String DOMVAL_APPOINTMENTTYP_STANDBY = '7407';
public final static String DOMVAL_APPOINTMENTTYP_MATSUPPLY = '7409';
public final static String DOMVAL_APPOINTMENTTYP_DAYSTART = '7410';
public final static String DOMVAL_APPOINTMENTTYP_DAYEND = '7411';


// DOM_TIMEREPORTTYPE (7500)
//========================
public final static String DOMVAL_TIMEREPORTTYPE_DAYSTART = '7501';
public final static String DOMVAL_TIMEREPORTTYPE_DAYEND = '7502';
public final static String DOMVAL_TIMEREPORTTYPE_WORKTIME = '7503';
public final static String DOMVAL_TIMEREPORTTYPE_BREAKTIME = '7504';
public final static String DOMVAL_TIMEREPORTTYPE_DRIVETIME = '7505';
public final static String DOMVAL_TIMEREPORTTYPE_STANDBY = '7506';
public final static String DOMVAL_TIMEREPORTTYPE_OTHERS = '7507';


// DOM_TIMEREPORTSTATUS (7600)
//============================
public final static String DOMVAL_TIMEREPORTSTATUS_OPENED = '7601';
public final static String DOMVAL_TIMEREPORTSTATUS_CLOSED = '7602';
public final static String DOMVAL_TIMEREPORTSTATUS_RELEASED = '7603';
public final static String DOMVAL_TIMEREPORTSTATUS_BOOKED = '7604';


// DOM_TIMEREPORTCOMPENSATION (7700)
//==================================
public final static String DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT = '7701';
public final static String DOMVAL_TIMEREPORTCOMPENSATION_STANDBY = '7702';
public final static String DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY = '7703';
public final static String DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY = '7704';
public final static String DOMVAL_TIMEREPORTCOMPENSATION_PUBLIC_HOLIDAY= '7705';


// Domäne IntervallTime (9100)
//===========================
public final static String DOMVAL_INTERVALLTIME_MINUTE = '9101';
public final static String DOMVAL_INTERVALLTIME_HOUR = '9102';
public final static String DOMVAL_INTERVALLTIME_WORKDAY = '9103';
public final static String DOMVAL_INTERVALLTIME_CALDAY = '9104';
public final static String DOMVAL_INTERVALLTIME_WORKVAL = '9105';
public final static String DOMVAL_INTERVALLTIME_AMOUNT = '9106';

// Domäne ACCOUNT_TYPE (11600)
public final static String DOMVAL_ACCOUNT_TYPE_CUSTOMER = 'Customer';


// Domäne ProductUnitClass (11900)
//========================
public final static String DOMVAL_PRODUCTUNITCLASS_STANDARD = '00';


// Domäne CustomerTimeWindow (12400)
//==================================
public final static String DOMVAL_CUSTOMERTIMEWINDOW_DEFAULT = '12401';
public final static String DOMVAL_CUSTOMERTIMEWINDOW_ALL_DAY = '12401';


// Domäne PaymentType (13200)
//===========================
public final static String DOMVAL_PAYMENTTYPE_CASH = '13201';
public final static String DOMVAL_PAYMENTTYPE_INVOICE = '13202';
public final static String DOMVAL_PAYMENTTYPE_CHEQUE = '13204';
public final static String DOMVAL_PAYMENTTYPE_DEBITCARD = '13206';
public final static String DOMVAL_PAYMENTTYPE_CREDITCARD = '13208';
public final static String DOMVAL_PAYMENTTYPE_NOT_RELEVANT = '13212';
public final static String DOMVAL_PAYMENTTYPE_LOYALTYPOINTS = '13223';
public final static String DOMVAL_PAYMENTTYPE_CONTRACT = '13224';


// Domäne CreditCardType (13600)
//============================
public final static String DOMVAL_CREDITCARDTYPE_AMEX = '13601';
public final static String DOMVAL_CREDITCARDTYPE_VISA = '13602';
public final static String DOMVAL_CREDITCARDTYPE_DINERS = '13603';
public final static String DOMVAL_CREDITCARDTYPE_MASTER = '13604';


// Domäne ArticleClass (14800)
//===========================
public final static String DOMVAL_ARTICLECLASS_PRODUCT = '14801';
public final static String DOMVAL_ARTICLECLASS_SPAREPART = '14802';
public final static String DOMVAL_ARTICLECLASS_SERVICE = '14803';
public final static String DOMVAL_ARTICLECLASS_CONSUMABLE = '14804';
public final static String DOMVAL_ARTICLECLASS_DOCUMENTATION = '14805';
public final static String DOMVAL_ARTICLECLASS_ACCESSORY = '14806';
public final static String DOMVAL_ARTICLECLASS_CONTRACT = '14807';
public final static String DOMVAL_ARTICLECLASS_KIT = '14808';
public final static String DOMVAL_ARTICLECLASS_TAXRATE = '14809';
public final static String DOMVAL_ARTICLECLASS_OTHER = '14810';
public final static String DOMVAL_ARTICLECLASS_TRAINING = '14811';


// Domäne ProductUnitType (20000)
//======================
public final static String DOMVAL_PRODUCTUNITTYPE_DEFAULT = '000';


// Domäne ArticleType (30000) ###rename
//=========================
public final static String DOMVAL_ARTICLETYP_PRODUCT = '30001';
public final static String DOMVAL_ARTICLETYP_NO_TYP = '30002';
public final static String DOMVAL_ARTICLETYP_AW = '30003';
public final static String DOMVAL_ARTICLETYP_DRIVINGTIME = '30004';
public final static String DOMVAL_ARTICLETYP_KFZ = '30005';
public final static String DOMVAL_ARTICLETYP_K = '30006';
public final static String DOMVAL_ARTICLETYP_F = '30007';
public final static String DOMVAL_ARTICLETYP_G = '30008';
public final static String DOMVAL_ARTICLETYP_FACT = '30009';
public final static String DOMVAL_ARTICLETYP_KUL = '30010';
public final static String DOMVAL_ARTICLETYP_WORKTIME = '30011';
public final static String DOMVAL_ARTICLETYP_DOCUMENTATION = '30012';
public final static String DOMVAL_ARTICLETYP_ADDON = '30013';
public final static String DOMVAL_ARTICLETYP_HAWA = '30014';
public final static String DOMVAL_ARTICLETYP_ZHWA = '30015';
public final static String DOMVAL_ARTICLETYP_SERVICE = '30017';
public final static String DOMVAL_ARTICLETYP_TRAVELTIME = '30018';
public final static String DOMVAL_ARTICLETYP_FIXED = '30019';
public final static String DOMVAL_ARTICLETYP_SDFIXED = '30020';
public final static String DOMVAL_ARTICLETYP_BANF = '30021';
public final static String DOMVAL_ARTICLETYP_PROCEEDSCLIENT = '30022';
public final static String DOMVAL_ARTICLETYP_PROCEEDSSUPPLIER = '30023';
public final static String DOMVAL_ARTICLETYP_ACCESSORIES = '30024';
public final static String DOMVAL_ARTICLETYP_CONTRPRICEDET = '30025';
public final static String DOMVAL_ARTICLETYP_CONTR = '30026';
public final static String DOMVAL_ARTICLETYP_CONTRFACT = '30027';
public final static String DOMVAL_ARTICLETYP_DIRECT = '30028';
public final static String DOMVAL_ARTICLETYP_MAINTENANCE = '30029';
public final static String DOMVAL_ARTICLETYP_TAX = '30030';


// Domäne LockTyp (50000) ###rename
//======================
public final static String DOMVAL_LOCKTYP_NONE = '50001';
public final static String DOMVAL_LOCKTYP_CASH = '50002';
public final static String DOMVAL_LOCKTYP_LOCKED = '50003';


// Domäne Riskclass (51001)
//======================
public final static String DOMVAL_RISKCLASS_NONE = '51001';
public final static String DOMVAL_RISKCLASS_REMINDERS = '51002';



// Domäne ArtLockTyp (50200) ###rename
//=========================
public final static String DOMVAL_ARTLOCKTYP_NO = '50201';
public final static String DOMVAL_ARTLOCKTYP_TEMP = '50202';
public final static String DOMVAL_ARTLOCKTYP_YES = '50203';
public final static String DOMVAL_ARTLOCKTYP_TILLZERO = '50204';


// Domäne AccountRole (50300)
//==========================
public final static String DOMVAL_ACCOUNT_ROLE_INVOICE_RECIPIENT = '50303';
public final static String DOMVAL_ACCOUNT_ROLE_PAYER = '50304';


// Domäne OrderRole (50300) ###rename
//==========================
public final static String DOMVAL_ORDERROLE_LE = '50301';
public final static String DOMVAL_ORDERROLE_AG = '50302';
public final static String DOMVAL_ORDERROLE_RE = '50303';
public final static String DOMVAL_ORDERROLE_RG = '50304';
public final static String DOMVAL_ORDERROLE_AP = '50305';
public final static String DOMVAL_ORDERROLE_SP = '50306';
public final static String DOMVAL_ORDERROLE_GS = '50307';
public final static String DOMVAL_ORDERROLE_ME = '50308';
public final static String DOMVAL_ORDERROLE_GH = '50309';
public final static String DOMVAL_ORDERROLE_EG = '50310';
public final static String DOMVAL_ORDERROLE_HD = '50312';
public final static String DOMVAL_ORDERROLE_VN = '50313';
public final static String DOMVAL_ORDERROLE_CO = '50314';
public final static String DOMVAL_ORDERROLE_HW = '50315';
public final static String DOMVAL_ORDERROLE_APHD = '50316';
public final static String DOMVAL_ORDERROLE_L2 = '50317';
public final static String DOMVAL_ORDERROLE_AA = '50322';
public final static String DOMVAL_ORDERROLE_ISP = '50323';
public final static String DOMVAL_ORDERROLE_VE = '50324';
public final static String DOMVAL_ORDERROLE_MM = '50325';
public final static String DOMVAL_ORDERROLE_APS = '50326';
public final static String DOMVAL_ORDERROLE_VP = '50327';


// Domäne MaterialDispo (50400)
//============================
public final static String DOMVAL_MATERIALDISPO_A = '50401';
public final static String DOMVAL_MATERIALDISPO_HL = '50402';
public final static String DOMVAL_MATERIALDISPO_SAP = '50403';


// Domäne TaxPartner (50500)
//=========================
public final static String DOMVAL_TAXPARTNER_TAX = '50501';
public final static String DOMVAL_TAXPARTNER_NOTAX = '50502';
public final static String DOMVAL_TAXPARTNER_ORGAN = '50503';
public final static String DOMVAL_TAXPARTNER_REDUCED = '50504';


// Domäne TaxAccount (50500)
//=========================
public final static String DOMVAL_TAXACCOUNT_TAX = '50501';
public final static String DOMVAL_TAXACCOUNT_NOTAX = '50502';
public final static String DOMVAL_TAXACCOUNT_ORGAN = '50503';
public final static String DOMVAL_TAXACCOUNT_REDUCED = '50504';


// Domäne TAXARTICLE (50600)
//=========================
public final static String DOMVAL_TAXARTICLE_FULL = '50601';
public final static String DOMVAL_TAXARTICLE_HALF = '50602';
public final static String DOMVAL_TAXARTICLE_NONE = '50603';


// Domäne CondType (51600)
//======================
public final static String DOMVAL_CONDTYPE_STD = '51601';
public final static String DOMVAL_CONDTYPE_VALIDPROD = '51604';


// Domäne CondValueType (52200)
//===========================
public final static String DOMVAL_CONDVALUETYPE_FIXED = '52201';
public final static String DOMVAL_CONDVALUETYPE_RELPERCENT = '52202';
public final static String DOMVAL_CONDVALUETYPE_RELABS = '52203';
public final static String DOMVAL_CONDVALUETYPE_LP = '52204';
public final static String DOMVAL_CONDVALUETYPE_LUMPSUM = '52205';
public final static String DOMVAL_CONDVALUETYPE_EXCESS = '52206';
public final static String DOMVAL_CONDVALUETYPE_MAXFREE = '52207';
public final static String DOMVAL_CONDVALUETYPE_MAXLIABLE = '52208';
public final static String DOMVAL_CONDVALUETYPE_CALCPART = '52209';
public final static String DOMVAL_CONDVALUETYPE_RAWPRICE = '52210';


// Domäne CondPriceType (52300)
//===========================
public final static String DOMVAL_CONDPRICETYPE_PRICE = '52301';
public final static String DOMVAL_CONDPRICETYPE_PRODPRICE = '52302';
public final static String DOMVAL_CONDPRICETYPE_RAWPRICE = '52303';
public final static String DOMVAL_CONDPRICETYPE_SERVICEPRICE = '52304';
public final static String DOMVAL_CONDPRICETYPE_SWAPPRICE = '52305';


// Domäne InvoicingStatus (52400)
//===============================
public final static String DOMVAL_INVOICING_STATUS_OPEN = '52401';
public final static String DOMVAL_INVOICING_STATUS_READYFORINVOICING = '52402';
public final static String DOMVAL_INVOICING_STATUS_INVOICED = '52403';
public final static String DOMVAL_INVOICING_STATUS_BOOKED = '52404';
public final static String DOMVAL_INVOICING_STATUS_CANCELLED = '52405';

//--> old constants - to be removed after deployment 01.07.2011
public final static String DOMVAL_INVOICINGSTATUS_OPEN = '52401';
public final static String DOMVAL_INVOICINGSTATUS_INVOICED = '52402';
public final static String DOMVAL_INVOICINGSTATUS_CLEARING = '52403';
//<---


// Domäne MaterialRequired (52500)
//===============================
public final static String DOMVAL_MATERIALREQUIRED_NO = '52501';
public final static String DOMVAL_MATERIALREQUIRED_YES = '52502';


// Domäne ArticleOrderTyp (52700)
//==============================
public final static String DOMVAL_ORDERLINETYPE_KVA = '52701';
public final static String DOMVAL_ORDERLINETYPE_INV = '52702';
public final static String DOMVAL_ORDERLINETYPE_INCCL = '52703';
public final static String DOMVAL_ORDERLINETYPE_SUPP = '52704';
public final static String DOMVAL_ORDERLINETYPE_SUMM = '52705';
public final static String DOMVAL_ORDERLINETYPE_INVOICED = '52706';
public final static String DOMVAL_ORDERLINETYPE_PARTGUAR = '52707';
public final static String DOMVAL_ORDERLINETYPE_CONTRACT = '52708';
//GMSGB move DOMVAL_ORDERLINETYPE_SALESOFFER to DOMVAL_ORDERLINETYPE_MATERIAL_REQUIREMENT
//public final static String DOMVAL_ORDERLINETYPE_SALESOFFER = '52709';
public final static String DOMVAL_ORDERLINETYPE_MATERIAL_REQUIREMENT = '52709'; 
public final static String DOMVAL_ORDERLINETYPE_CONSULTATIO = '52710';
public final static String DOMVAL_ORDERLINETYPE_STD = '52711';
public final static String DOMVAL_ORDERLINETYPE_TAX = '52712';
public final static String DOMVAL_ORDERLINETYPE_THIRD_PARTY = '52713';
public final static String DOMVAL_ORDERLINETYPE_CUSTOMER = '52714';


// Domäne CondClass (54000)
//========================
public final static String DOMVAL_CONDCLASS_PRICEDETDEFAULT = '54001';
public final static String DOMVAL_CONDCLASS_PRICEDETCONTRSUMRATE = '54002';
public final static String DOMVAL_CONDCLASS_PRICEDETCONTRPARTSUMRATE = '54003';
public final static String DOMVAL_CONDCLASS_CONTRFACTFEE = '54004';
public final static String DOMVAL_CONDCLASS_CONTRFACTPRICE = '54005';
public final static String DOMVAL_CONDCLASS_CONTRINVADMINFEE = '54006';
public final static String DOMVAL_CONDCLASS_CONTRVALIDPROD = '54007';
public final static String DOMVAL_CONDCLASS_DATEFEE = '54008';


// Domain AutoPosType
public final static String DOMVAL_AUTOPOSTYPE_WORKING = '56701';
public final static String DOMVAL_AUTOPOSTYPE_DRIVING = '56702';
public final static String DOMVAL_AUTOPOSTYPE_DISTANCE = '56703';
public final static String DOMVAL_AUTOPOSTYPE_COMBINED = '56704';


// Domäne InvoicingType (170000)
//=============================
public final static String DOMVAL_INVOICINGTYPE_DEFAULT = '170001'; //
public final static String DOMVAL_INVOICINGTYPE_GUARANTEE = '170002'; //
public final static String DOMVAL_INVOICINGTYPE_GOODWILL = '170003'; //
public final static String DOMVAL_INVOICINGTYPE_NOCHARGE = '170004';
public final static String DOMVAL_INVOICINGTYPE_CONTRACT = '170005'; //


// Domäne InvoicingSubType (180000)
//===========================
public final static String DOMVAL_INVOICINGSUBTYPE_DEFAULT = '180001'; //
public final static String DOMVAL_INVOICINGSUBTYPE_STANDARD = '180007'; //
public final static String DOMVAL_INVOICINGSUBTYPE_SERVICING = '180012'; //
public final static String DOMVAL_INVOICINGSUBTYPE_CONTRACT = '180013'; //


// DOM_INSTALLEDBASELOC_STATUS (281000)
//======================================
public final static String DOMVAL_INSTALLEDBASELOC_STATUS_ACTIVE = 'Active'; //


// DOM_COMPLETIONSTATUS (58700)
//======================================
public final static String DOMVAL_COMPLETIONSTATUS_DEFAULT = '58701'; 


// DOM_MATERIALREPLENISHMENT_TYPE (375000)
//========================================
public final static String DOMVAL_MATERIALREPLENISHMENT_TYPE_NONE = '375001'; 
public final static String DOMVAL_MATERIALREPLENISHMENT_TYPE_NORMAL = '375002'; 
public final static String DOMVAL_MATERIALREPLENISHMENT_TYPE_QUICK = '375003'; 


// Domäne ValStat (53100) ### deprecatd - will be removed
//======================
public final static String DOMVAL_VALSTAT_OK = '53101';
public final static String DOMVAL_VALSTAT_CHECK = '53102';
public final static String DOMVAL_VALSTAT_ERROR = '53103';
public final static String DOMVAL_VALSTAT_INPROCESS = '53104';
public final static String DOMVAL_VALSTAT_CREDITCHECK_OK = '53105';


// Domäne ErrLoc(190000) ### deprecatd - will be removed
//======================
public final static String DOMVAL_ERRLOC2_CHANGEBRANDALLOWED = '2050';


// Domäne ContractStatus 
//======================
public final static String DOMVAL_CONTRACT_STATUS_CREATED = 'Created';
public final static String DOMVAL_CONTRACT_STATUS_ACTIVE = 'Active';
public final static String DOMVAL_CONTRACT_STATUS_CHECK = 'Check';
public final static String DOMVAL_CONTRACT_STATUS_SUSPENDED = 'Suspended';
public final static String DOMVAL_CONTRACT_STATUS_EXPIRED = 'Expired';
public final static String DOMVAL_CONTRACT_STATUS_CANCELLED = 'Cancelled';
public final static String DOMVAL_CONTRACT_STATUS_PRECANCELLED = 'Pre-Cancelled';

public final static String DOMVAL_CONTRACT_STATUS_SCHEDULEABLE = 'Created,Active';

}