/*
 * @(#)SCutilFormatAddress.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCutilFormatAddress
{
    /**
     * Formats an account address.
     * @param role the account to format
     * @return the address in the format of the country
     */
    public static String formatAddress(Account acc)
    {
        String address = '';
        
        if ((null != acc) && (null != acc.BillingCountry__c) && 
            acc.BillingCountry__c.equals('DE'))
        {
            address = add(address, acc.BillingStreet);
            address = add(address, acc.BillingHouseNo__c);
            address = add(address, '<br>');
            address = add(address, acc.BillingPostalCode);
            address = add(address, acc.BillingCity);
            if (acc.BillingCity != null && acc.BillingDistrict__c != null && 
               !acc.BillingCity.equals(acc.BillingDistrict__c))
            {
                address = add(address, acc.BillingDistrict__c);
            } 
        }
        else if ((null != acc) && (null != acc.BillingCountry__c) && 
                 acc.BillingCountry__c.equals('NL'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, acc.BillingStreet);
            address = add(address, acc.BillingHouseNo__c);
            address = add(address, acc.BillingExtension__c, false);
            address = add(address, '<br>');
            address = add(address, acc.BillingPostalCode);
            address = add(address, acc.BillingCity);
            if (acc.BillingCity != null && acc.BillingDistrict__c != null && 
               !acc.BillingCity.equals(acc.BillingDistrict__c))
            {
                address = add(address, acc.BillingDistrict__c);
            } 
        } 
        else if ((null != acc) && (null != acc.BillingCountry__c) && 
                 acc.BillingCountry__c.equals('GB'))
        {
            address = add(address, acc.BillingHouseNo__c);
            address = add(address, acc.BillingStreet);
            if (null != acc.BillingFloor__c)
            {
                address = add(address, ',', false);
                address = add(address, acc.BillingFloor__c);
            }
            if (null != acc.BillingFlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, acc.BillingFlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, acc.BillingCity);
            address = add(address, acc.BillingPostalCode);
        }
        else if ((null != acc) && (null != acc.BillingCountry__c) && 
                 acc.BillingCountry__c.equals('AT'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, acc.BillingStreet);
            address = add(address, acc.BillingHouseNo__c);
            address = add(address, acc.BillingExtension__c, false);
            if (null != acc.BillingFloor__c)
            {
                address = add(address, acc.BillingFloor__c);
            }
            if (null != acc.BillingFlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, acc.BillingFlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, acc.BillingPostalCode);
            address = add(address, acc.BillingCity);
            if (acc.BillingCity != null && acc.BillingDistrict__c != null && 
               !acc.BillingCity.equals(acc.BillingDistrict__c))
            {
                address = add(address, acc.BillingDistrict__c);
            } 
        } 
        else if (null != acc)
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, acc.BillingStreet);
            address = add(address, acc.BillingHouseNo__c);
            address = add(address, acc.BillingExtension__c, false);
            address = add(address, '<br>');
            address = add(address, acc.BillingPostalCode);
            address = add(address, acc.BillingCity);
            if (acc.BillingCity != null && acc.BillingDistrict__c != null && 
               !acc.BillingCity.equals(acc.BillingDistrict__c))
            {
                address = add(address, acc.BillingDistrict__c);
            } 
        } 
        return address;
    } // formatAddress
    
    /**
     * Formats an order role address.
     * @param role the order role to format
     * @return the address in the format of the country
     */
    public static String formatAddress(SCOrderRole__c role)
    {
        String address = '';
        
        if ((null != role) && (null != role.Country__c) && 
            role.Country__c.equals('DE'))
        {
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, role.PostalCode__c);
            address = add(address, role.City__c);
            if (role.City__c != null && role.District__c != null && !role.City__c.equals(role.District__c))
            {
                address = add(address, role.District__c);
            } 
        } 
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('GB'))
        {
            address = add(address, role.HouseNo__c);
            address = add(address, role.Street__c);
            if (null != role.Floor__c)
            {
                address = add(address, ',', false);
                address = add(address, role.Floor__c);
            }
            if (null != role.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, role.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, role.City__c);
            address = add(address, role.PostalCode__c);
        }
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('NL'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, role.Extension__c, false);
            address = add(address, '<br>');
            address = add(address, role.PostalCode__c);
            address = add(address, role.City__c);
            if (role.City__c != null && role.District__c != null && !role.City__c.equals(role.District__c))
            {
                address = add(address, role.District__c);
            } 
        }        
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('AT'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, role.Extension__c, false);
            if (null != role.Floor__c)
            {
                address = add(address, role.Floor__c);
            }
            if (null != role.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, role.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, role.PostalCode__c);
            address = add(address, role.City__c);
            if (role.City__c != null && role.District__c != null && 
               !role.City__c.equals(role.District__c))
            {
                address = add(address, role.District__c);
            } 
        } 
        else if (null != role)
        {
            // default
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, role.PostalCode__c);
            address = add(address, role.City__c);
            if (role.City__c != null && role.District__c != null && !role.City__c.equals(role.District__c))
            {
                address = add(address, role.District__c);
            } 
        }
        return address;
    } // formatAddress

    /**
     * Formats an order role address.
     * @param role the order role to format
     * @return the address in the format of the country
     */
    public static String formatStreetAndHouseno(SCOrderRole__c role)
    {
        String address = '';
        
        if ((null != role) && (null != role.Country__c) && 
            role.Country__c.equals('DE'))
        {
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
        } 
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('GB'))
        {
            address = add(address, role.HouseNo__c);
            address = add(address, role.Street__c);
            if (null != role.Floor__c)
            {
                address = add(address, ',', false);
                address = add(address, role.Floor__c);
            }
            if (null != role.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, role.FlatNo__c, false);
            }
        }
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('NL'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, role.Extension__c, false);
        }        
        else if ((null != role) && (null != role.Country__c) && 
                 role.Country__c.equals('AT'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
            address = add(address, role.Extension__c, false);
            if (null != role.Floor__c)
            {
                address = add(address, role.Floor__c);
            }
            if (null != role.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, role.FlatNo__c, false);
            }
        }        
        else if (null != role)
        {
            // default
            address = add(address, role.Street__c);
            address = add(address, role.HouseNo__c);
        }
        return address;
    } // formatStreetAndHouseNo

    /**
     * Formats an installed base location address.
     * @param loc the installed base location to format
     * @return the address in the format of the country
     */
    public static String formatAddress(SCInstalledBaseLocation__c loc)
    {
        String address = '';
        
        if ((null != loc) && (null != loc.Country__c) && 
            loc.Country__c.equals('DE'))
        {
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        } 
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('GB'))
        {
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Street__c);
            if (null != loc.Floor__c)
            {
                address = add(address, ',', false);
                address = add(address, loc.Floor__c);
            }
            if (null != loc.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, loc.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, loc.City__c);
            address = add(address, loc.PostalCode__c);
        }
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('NL'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Extension__c, false);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        }        
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('AT'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Extension__c, false);
            if (null != loc.Floor__c)
            {
                address = add(address, loc.Floor__c);
            }
            if (null != loc.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, loc.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && 
               !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        } 
        else if (null != loc)
        {
            // default
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        }
        return address;
    } // formatAddress


    /**
     * Formats an resource address.
     * @param ressource assignment 
     * @return the address in the format of the country
     */
    public static String formatAddress(SCResourceAssignment__c loc)
    {
        String address = '';
        
        if ((null != loc) && (null != loc.Country__c) && 
            loc.Country__c.equals('DE'))
        {
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        } 
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('GB'))
        {
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Street__c);
            if (null != loc.Floor__c)
            {
                address = add(address, ',', false);
                address = add(address, loc.Floor__c);
            }
            if (null != loc.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, loc.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, loc.City__c);
            address = add(address, loc.PostalCode__c);
        }
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('NL'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Extension__c, false);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        }        
        else if ((null != loc) && (null != loc.Country__c) && 
                 loc.Country__c.equals('AT'))
        {
            // Name,  street, No (+extension) zip city telefon 1 + 2 and mobile
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, loc.Extension__c, false);
            if (null != loc.Floor__c)
            {
                address = add(address, loc.Floor__c);
            }
            if (null != loc.FlatNo__c)
            {
                address = add(address, '/', false);
                address = add(address, loc.FlatNo__c, false);
            }
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && 
               !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        } 
        else if (null != loc)
        {
            // default
            address = add(address, loc.Street__c);
            address = add(address, loc.HouseNo__c);
            address = add(address, '<br>');
            address = add(address, loc.PostalCode__c);
            address = add(address, loc.City__c);
            if (loc.City__c != null && loc.District__c != null && !loc.City__c.equals(loc.District__c))
            {
                address = add(address, loc.District__c);
            } 
        }
        return address;
    } // formatAddress
    
    /**
     * Adds the inField value to the inAddr and uses
     * a space separator if both fields are no null
     * @param  inAddr  the address 
     * @param  inField the new value to add to inAddr
     * @return see obove
     */
    public static String add(String inAddr, String inField)
    {
        return add(inAddr, inField, true);
    }
    public static String add(String inAddr, String inField, Boolean addBlank)
    {
        if(inField != null)
        {
            if(inAddr != null)
            {
                 return (addBlank ? (inAddr + ' ' + inField) : (inAddr + inField));
            }
            return inField;
        }
        else if(inAddr != null)
        {
            return inAddr;
        }
        return '';
    }
} // SCutilFormatAddress