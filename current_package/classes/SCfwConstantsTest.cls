/*
 * @(#)SCfwConstantsTest.cls
 *  
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
@isTest
private class SCfwConstantsTest
{
    static testMethod void testTestMode() 
    {
        System.assertNotEquals(null, SCfwConstants.NamespacePrefix);
        
        // Domäne Unit (200)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_PIECE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_HOURS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_KILOMETERS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_KG);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_LITERS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_SQUAREMETERS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_UNIT_METERS);
        
        // Domäne APPOINTMENTPLANNINGTYPE (800)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_MANUAL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_AUTO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_AUTOPRIO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_MAN_MOVED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_AUTO_EMPLFIX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_MAP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_MAP_MOVED);
        
        // Domäne DistZone (900)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_DISTZONE_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_DISTZONE_NO_CALCULATION);
        
        // Domäne ContrStat (1000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRSTAT_CREATED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRSTAT_ACTIV);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRSTAT_CANCELED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRSTAT_FINISHED);
        
        // Domäne Curr (2000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CURR_GBP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CURR_USD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CURR_XEU);
        
        // Domäne OrderPrio (2100)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERPRIO_DEFAULT);
        
        // Domäne FollowUp1
        System.assertNotEquals(null, SCfwConstants.DOMVAL_FOLLOWUP1_NONE);
        
        // Domäne FollowUp2 (2900)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_FOLLOWUP2_NONE);
        
        // Domäne CashBookTyp (4900)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_CLOSE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_TRANSIT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_COLLECTION);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_PURCHASE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_STORNO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CASHBOOKTYP_PERSONELL_CLOSE);
        
        // Domäne MatMoveTyp (5200)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_CUST);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_DELIVERY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_NEW);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_SCRAP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_IN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_OUT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_RETURN_OLD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_RETURN_NEW);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CONSUMPTION_UNDO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_REPLENISHMENT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_INVENTORY_IN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_INVENTORY_OUT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_RESETSTOCKBALANCE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CONVERSION_IN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_CONVERSION_OUT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_DELIVERY);
        
        // Domäne MatStat (5400)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_NOMATERIAL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_ORDER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_ORDERED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_DELIVERED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_BOOK);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_BOOKED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_CANCEL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_CANCELLED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_ORDERABLE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_PARTIALDELIVERY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATSTAT_IN_TRANSIT);
        
        // Domäne OrderStatus (5500)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_OPEN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_TRANSFERRED_MOBILE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_PROCESSING_MOBILE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_FINISHED_MOBILE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_CANCEL_MOBILE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_PENDING_ORDERS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_NOT_SCHEDULEABLE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_FOR_WORKSHOP_APP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_APPROVALPENDING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERSTATUS_PARTIALDELIVERY);
        
        // Domäne OrderType (5700)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INHOUSE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_SALESORDER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_CONSULTING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_COMISSIONING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_SPAREPARTSALES);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_MAINTENANCE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_CREDIT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_COPY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_COLLECTIVE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_CONTRACT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_CONTRACT_COLLECTIVE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_INSURANCE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_NOT_SCHEDULEABLE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_FOR_CREDITNOTE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERTYPE_MAINTENANCE_ORDER);
        
        // DOM_APPOINTMENTCLASS (7300)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTCLASS_ORDER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTCLASS_EMPLOYEE);
        
        // DOM_APPOINTMENTTYP (7400)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_JOB);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_HOMERIDE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_WORKTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_BREAKTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_DRIVETIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_OVERNIGHT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_STANDBY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_MATSUPPLY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_DAYSTART);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_APPOINTMENTTYP_DAYEND);
        
        // DOM_TIMEREPORTTYPE (7500)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_BREAKTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_STANDBY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTTYPE_OTHERS);
        
        // DOM_TIMEREPORTSTATUS (7600)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTSTATUS_RELEASED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTSTATUS_BOOKED);
        
        // DOM_TIMEREPORTCOMPENSATION (7700)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_STANDBY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_PUBLIC_HOLIDAY);
        
        // Domäne IntervallTime (9100)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_MINUTE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_HOUR);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_WORKDAY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_CALDAY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_WORKVAL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INTERVALLTIME_AMOUNT);
        
        // Domäne ACCOUNT_TYPE (11600)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER);
        
        // Domäne ProductUnitClass (11900)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD);
        
        // Domäne CustomerTimeWindow (12400)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CUSTOMERTIMEWINDOW_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CUSTOMERTIMEWINDOW_ALL_DAY);
        
        // Domäne PaymentType (13200)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_CASH);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_INVOICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_CHEQUE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_DEBITCARD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_CREDITCARD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_NOT_RELEVANT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_LOYALTYPOINTS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PAYMENTTYPE_CONTRACT);
        
        // Domäne CreditCardType (13600)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CREDITCARDTYPE_AMEX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CREDITCARDTYPE_VISA);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CREDITCARDTYPE_DINERS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CREDITCARDTYPE_MASTER);
        
        // Domäne ArticleClass (14800)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_SERVICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_CONSUMABLE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_DOCUMENTATION);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_ACCESSORY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_CONTRACT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_KIT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_TAXRATE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_OTHER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLECLASS_TRAINING);
        
        // Domäne ProductUnitType (20000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT);
        
        // Domäne ArticleType (30000) ###rename
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_PRODUCT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_AW);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_DRIVINGTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_KFZ);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_K);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_F);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_G);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_FACT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_KUL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_WORKTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_DOCUMENTATION);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_ADDON);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_HAWA);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_ZHWA);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_SERVICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_TRAVELTIME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_FIXED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_SDFIXED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_BANF);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_PROCEEDSCLIENT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_PROCEEDSSUPPLIER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_ACCESSORIES);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_CONTRPRICEDET);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_CONTR);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_CONTRFACT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_DIRECT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_MAINTENANCE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTICLETYP_TAX);
        
        // Domäne LockTyp (50000) ###rename
        System.assertNotEquals(null, SCfwConstants.DOMVAL_LOCKTYP_NONE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_LOCKTYP_CASH);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_LOCKTYP_LOCKED);
        
        // Domäne Riskclass (51001)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_RISKCLASS_NONE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_RISKCLASS_REMINDERS);
        
        // Domäne ArtLockTyp (50200) ###rename
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTLOCKTYP_NO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTLOCKTYP_TEMP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTLOCKTYP_YES);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ARTLOCKTYP_TILLZERO);
        
        // Domäne AccountRole (50300)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ACCOUNT_ROLE_INVOICE_RECIPIENT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ACCOUNT_ROLE_PAYER);
        
        // Domäne OrderRole (50300) ###rename
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_LE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_AG);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_RE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_RG);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_AP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_SP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_GS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_ME);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_GH);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_EG);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_HD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_VN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_CO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_HW);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_APHD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_L2);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_AA);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_ISP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_VE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_MM);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_APS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERROLE_VP);
        
        // Domäne MaterialDispo (50400)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALDISPO_A);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALDISPO_HL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALDISPO_SAP);
        
        // Domäne TaxPartner (50500)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXPARTNER_TAX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXPARTNER_NOTAX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXPARTNER_ORGAN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXPARTNER_REDUCED);
        
        // Domäne TaxAccount (50500)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXACCOUNT_TAX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXACCOUNT_NOTAX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXACCOUNT_ORGAN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXACCOUNT_REDUCED);
        
        // Domäne TAXARTICLE (50600)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXARTICLE_FULL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXARTICLE_HALF);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_TAXARTICLE_NONE);
        
        // Domäne CondType (51600)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDTYPE_STD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDTYPE_VALIDPROD);
        
        // Domäne CondValueType (52200)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_FIXED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_RELPERCENT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_RELABS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_LP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_LUMPSUM);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_EXCESS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_MAXFREE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_MAXLIABLE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_CALCPART);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDVALUETYPE_RAWPRICE);
        
        // Domäne CondPriceType (52300)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDPRICETYPE_PRODPRICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDPRICETYPE_RAWPRICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDPRICETYPE_SERVICEPRICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDPRICETYPE_SWAPPRICE);
        
        // Domäne InvoicingStatus (52400)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICING_STATUS_OPEN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICING_STATUS_READYFORINVOICING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICING_STATUS_INVOICED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICING_STATUS_CANCELLED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSTATUS_OPEN);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSTATUS_INVOICED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSTATUS_CLEARING);
        
        // Domäne MaterialRequired (52500)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALREQUIRED_NO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALREQUIRED_YES);
        
        // Domäne ArticleOrderTyp (52700)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_KVA);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_INV);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_INCCL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_SUPP);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_SUMM);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_INVOICED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_PARTGUAR);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_CONTRACT);
        //GMSGB move DOMVAL_ORDERLINETYPE_SALESOFFER to DOMVAL_ORDERLINETYPE_MATERIAL_REQUIREMENT
        //System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_SALESOFFER);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_MATERIAL_REQUIREMENT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_CONSULTATIO);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_STD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_TAX);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_THIRD_PARTY);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ORDERLINETYPE_CUSTOMER);
        
        // Domäne CondClass (54000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_PRICEDETDEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_PRICEDETCONTRSUMRATE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_PRICEDETCONTRPARTSUMRATE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_CONTRFACTFEE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_CONTRFACTPRICE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_CONTRINVADMINFEE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_CONTRVALIDPROD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONDCLASS_DATEFEE);
        
        // Domain AutoPosType
        System.assertNotEquals(null, SCfwConstants.DOMVAL_AUTOPOSTYPE_WORKING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_AUTOPOSTYPE_DRIVING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_AUTOPOSTYPE_DISTANCE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_AUTOPOSTYPE_COMBINED);
        
        // Domäne InvoicingType (170000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGTYPE_GUARANTEE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGTYPE_GOODWILL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGTYPE_CONTRACT);
        
        // Domäne InvoicingSubType (180000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSUBTYPE_DEFAULT);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSUBTYPE_STANDARD);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSUBTYPE_SERVICING);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INVOICINGSUBTYPE_CONTRACT);
        
        // DOM_INSTALLEDBASELOC_STATUS (281000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_INSTALLEDBASELOC_STATUS_ACTIVE);
        
        // DOM_COMPLETIONSTATUS (58700)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_COMPLETIONSTATUS_DEFAULT);
        
        // DOM_MATERIALREPLENISHMENT_TYPE (375000)
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NONE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_NORMAL);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_MATERIALREPLENISHMENT_TYPE_QUICK);
        
        // Domäne ValStat (53100) ### deprecatd - will be removed
        System.assertNotEquals(null, SCfwConstants.DOMVAL_VALSTAT_OK);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_VALSTAT_CHECK);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_VALSTAT_ERROR);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_VALSTAT_INPROCESS);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_VALSTAT_CREDITCHECK_OK);
        
        // Domäne ErrLoc(190000) ### deprecatd - will be removed
        System.assertNotEquals(null, SCfwConstants.DOMVAL_ERRLOC2_CHANGEBRANDALLOWED);
        
        // Domäne ContractStatus 
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_CREATED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_ACTIVE);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_CHECK);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_SUSPENDED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_EXPIRED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_CANCELLED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_PRECANCELLED);
        System.assertNotEquals(null, SCfwConstants.DOMVAL_CONTRACT_STATUS_SCHEDULEABLE);
    }
}