/*
 * @(#)SCbtcGeocodeAccountTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcGeocodeAccountTest
{
    static testMethod void testDEPositive()
    {
        Test.StartTest();
        List<Account> la = [Select id from Account where BillingCountry__c = 'DE' limit 1];
        if(la.size() > 0)
        {
            Id accountId = la[0].Id;
            String strId = accountId + '';
            SCbtcGeocodeAccount.syncGeocodeAccount(strId);
            List<String> accountIdList = new List<String>();
            accountIdList.add(strId);
            SCbtcGeocodeAccount.syncGeocodeAccounts(accountIdList);
        }
        SCbtcGeocodeAccount.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }

    static testMethod void testDEPositive2()
    {
        Test.StartTest();
        List<Account> la = [Select id from Account where BillingCountry__c = 'DE' limit 1];
        if(la.size() > 0)
        {
            Id accountId = la[0].Id;
            String strId = accountId + '';
            SCbtcGeocodeAccount.asyncGeocodeAccount(strId, 'trace');
            List<String> accountIdList = new List<String>();
            accountIdList.add(strId);
            SCbtcGeocodeAccount.asyncGeocodeAccounts(accountIdList);
        }
        SCbtcGeocodeAccount.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }


    static testMethod void testNLPositive()
    {
        Test.StartTest();
        List<Account> la = [Select id from Account where BillingCountry__c = 'NL' limit 1];
        if(la.size() > 0)
        {
            Id accountId = la[0].Id;
            String strId = accountId + '';
            SCbtcGeocodeAccount.syncGeocodeAccount(strId);
            List<String> accountIdList = new List<String>();
            accountIdList.add(strId);
            SCbtcGeocodeAccount.syncGeocodeAccounts(accountIdList);
        }
        SCbtcGeocodeAccount.asyncGeocodeAll('ALL', 'NL', null,  1, 'trace');    
        Test.StopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        Account acc = new Account(Name = 'Test Accountname');
        Database.insert(acc);
        
        SCInstalledBaseLocation__c location = new SCInstalledBaseLocation__c();
        Database.insert(location);
        
        
        Test.startTest();
        
        
        try { SCbtcGeocodeAccount.asyncGeocodeAccount(null, 'test'); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.asyncGeocodeAccount((String) acc.Id, 'test'); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.asyncGeocodeAccounts(new List<String>()); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.asyncGeocodeAccounts(new List<String>{acc.Id}); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.syncGeocodeAccounts(new List<String>()); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.syncGeocodeAccounts(new List<String>{acc.Id}); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.syncGeocodeAccount(null); } catch(Exception e) {}
        try { SCbtcGeocodeAccount.syncGeocodeAccount((String) acc.Id); } catch(Exception e) {}
        
        SCbtcGeocodeAccount obj = new SCbtcGeocodeAccount(new List<String>{acc.Id}, 'test');
        AvsAddress add = obj.getAddr(acc);
        
        try { obj.execute(null, new List<sObject>{acc}); } catch(Exception e) {}
        try { obj.geocode(acc, true, new List<SCInterfaceLog__c>()); } catch(Exception e) {}
        try { obj.geocode(acc, false, new List<SCInterfaceLog__c>()); } catch(Exception e) {}
        try { obj.setGeoCode(false, false, add, null, null, acc); } catch(Exception e) {}
        try { obj.geocodeInstalledBaseLocations(acc); } catch(Exception e) {}
        try { obj.isTheSameAddress(acc,location); } catch(Exception e) {}
        try { obj.isEqual(null,null); } catch(Exception e) {}
        try { obj.isEqual('-','-'); } catch(Exception e) {}
        try { obj.isEqual('a','b'); } catch(Exception e) {}
        
        
        Test.stopTest();
    }  

}