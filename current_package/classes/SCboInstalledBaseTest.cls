/*
 * @(#)SCboInstalledBaseTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCboInstalledBaseTest 
{
    static testMethod void createInstalledBase() 
    {
        SCboInstalledBase ib = new SCboInstalledBase();
        System.Assert(ib != null);
        
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        boOrder.createDefaultRoles(SCHelperTestClass.account);
        SCboInstalledBase ib2 = new SCboInstalledBase(boOrder); 
        System.Assert(ib2 != null);
 
    }
    static testmethod void saveInstalledbase()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        boOrder.createDefaultRoles(SCHelperTestClass.account);
        SCboInstalledBase ib = new SCboInstalledBase(boOrder); 
        ib.save();    
        System.Assert(ib.ibLocation != null);
    }
    
    static testmethod void getInstalledbase()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        boOrder.createDefaultRoles(SCHelperTestClass.account);
        SCboInstalledBase ib = new SCboInstalledBase(boOrder); 
        List<SCfwInstalledBase> ibList = ib.getInstalledBase(SCHelperTestClass.account.Id);
        System.Assert(ibList.size() > 0);
    }
    static testmethod void getAllocation()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.Order);
        boOrder.createDefaultRoles(SCHelperTestClass.account);
        SCboInstalledBase ib = new SCboInstalledBase(boOrder); 
        List<SCInstalledBaseLocation__c> allocList = ib.getAllLocation(SCHelperTestClass.account.Id);
        System.debug('***ListSize' + allocList.size());
        System.Assert(allocList.size() > 0);
    }

    static testmethod void readForOrderItem()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboInstalledBase ib = new SCboInstalledBase(); 
        SCInstalledBase__c ib2 = ib.readForOrderItem(SCHelperTestClass.installedBaseSingle.Id);
        System.Assert(ib2 != null);
        
        List<Id> ids = new List<Id>();
        ids.add(SCHelperTestClass.installedBaseSingle.Id);
        List<SCInstalledBase__c> ibList = ib.readForOrderItem(ids);
        System.Assert(ibList.size() == 1);
    }

    static testmethod void readOwner()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCInstalledBaseRole__c role = SCboInstalledBase.readOwner(SCHelperTestClass.installedBaseSingle.Id);
        System.Assert(role != null);
    }

    static testmethod void initInstBase()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboInstalledBase ib = new SCboInstalledBase(SCHelperTestClass.installedBaseSingle.Id); 
        System.Assert(ib.allIB.size() == 1);
        System.Assert(ib.ibLocation.Street__c != null);
    }

    static testmethod void addInstBase()
    {
        SCboInstalledBase ib = new SCboInstalledBase(); 
        ib.addInstalledBase(new SCfwInstalledBase());
        ib.addOrderRelated(new SCfwInstalledBase());
        ib.addOrderRelated(0);
        System.Assert(ib.allIB.size() == 1);
        System.Assert(ib.orderRelated.size() == 2);
    }

    static testmethod void findInstalledBase()
    {
        SCHelperTestClass.createInstalledBase(true);
        SCQualification__c qualification = new SCQualification__c(
                                            Type__c = 'ProductSkill',
                                            Info__c = 'PS3');
        insert qualification;
        SCProductModel__c prodModel = new SCProductModel__c(
                                            Brand__c = SCHelperTestClass.brand.Id, 
                                            Country__c = 'NL', 
                                            UnitClass__c = '11950',
                                            UnitType__c = '20010',
                                            Group__c = '269', 
                                            ProductSkill__c = qualification.Id, 
                                            Name = 'Air 3000', 
                                            Power__c = '650');
        insert prodModel;
        SCInstalledBase__c instBase = new SCInstalledBase__c(
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductModel__c = prodModel.Id, 
                                            ProductUnitClass__c = '11950',
                                            ProductUnitType__c = '20010',
                                            ProductGroup__c = '269', 
                                            ProductSkill__c = 'PS3', 
                                            Brand__c = SCHelperTestClass.brand.Id);
        insert instBase;
        Id accId = SCHelperTestClass.account.Id;
        
        Test.startTest();

        // test 1: look via serial number
        SCInstalledBase__c srcInstBase = new SCinstalledBase__c();
        srcInstBase.SerialNo__c = SCHelperTestClass.installedBaseSingle.SerialNo__c;

        SCInstalledBase__c foundInstBase = 
                SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null != foundInstBase);
        
        // test 2: look via serial number, that doesn't exist
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.SerialNo__c = '12345';

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null == foundInstBase);
        
        // test 3: look via product model
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.ProductModel__c = prodModel.Id;

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null != foundInstBase);
        
        // test 4: look via product unittype
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.ProductUnitType__c = SCHelperTestClass.installedBaseSingle.ProductUnitType__c;

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null == foundInstBase);
        
        // test 5: look via product skill
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.ProductSkill__c = 'PS1';

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null == foundInstBase);
        
        // test 6: look via product data
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.ProductUnitClass__c = SCHelperTestClass.installedBaseSingle.ProductUnitClass__c;
        srcInstBase.ProductUnitType__c = SCHelperTestClass.installedBaseSingle.ProductUnitType__c;
        srcInstBase.ProductGroup__c = SCHelperTestClass.installedBaseSingle.ProductGroup__c;
        srcInstBase.ProductPower__c = SCHelperTestClass.installedBaseSingle.ProductPower__c;
        srcInstBase.Brand__c = SCHelperTestClass.installedBaseSingle.Brand__c;

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null == foundInstBase);

        // test 7: look via serial number and location
        srcInstBase = new SCinstalledBase__c();
        srcInstBase.SerialNo__c = SCHelperTestClass.installedBaseSingle.SerialNo__c;
        srcInstBase.InstalledBaseLocation__c = SCHelperTestClass.location.Id;

        foundInstBase = SCboInstalledBase.findInstalledBase(srcInstBase, accId);
        System.Assert(null != foundInstBase);
        Test.stopTest();
    }

    static testmethod void findInstalledBaseLocation()
    {
        SCHelperTestClass.createInstalledBaseLocation(true);
        Id accId = SCHelperTestClass.account.Id;
        
        Test.startTest();

        // test 1: look for an existing address
        SCInstalledBaseLocation__c srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = SCHelperTestClass.location.Street__c;
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        SCInstalledBaseLocation__c foundInstBaseLoc = 
                SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 2: look for an existing address with 'strasse'
        SCInstalledBaseLocation__c loc = new SCInstalledBaseLocation__c(Id = SCHelperTestClass.location.Id);
        loc.Street__c = 'Moormanstr.';
        upsert loc;
        
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstrasse';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 3: look for an existing address with 'str'
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstr';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 4: look for an existing address with 'straße' inside
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstraße';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 5: look for an existing address with 'strasse' inside
        loc = new SCInstalledBaseLocation__c(Id = SCHelperTestClass.location.Id);
        loc.Street__c = 'Moormanstraße';
        upsert loc;
        
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstrasse';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 6: look for an existing address with 'str'
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstr.';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);

        // test 7: look for an existing address with 'straße' inside
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Moormanstraße';
        srcInstBaseLoc.HouseNo__c = SCHelperTestClass.location.HouseNo__c;
        srcInstBaseLoc.PostalCode__c = SCHelperTestClass.location.PostalCode__c;
        srcInstBaseLoc.City__c = SCHelperTestClass.location.City__c;
        srcInstBaseLoc.Country__c = SCHelperTestClass.location.Country__c;

        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null != foundInstBaseLoc);
        
        // test 8: look for an address, that doesn't exist
        srcInstBaseLoc = new SCInstalledBaseLocation__c();
        srcInstBaseLoc.Street__c = 'Altdorfer Str.';
        srcInstBaseLoc.HouseNo__c = '12';
        srcInstBaseLoc.PostalCode__c = '91207';
        srcInstBaseLoc.City__c = 'Lauf';
        srcInstBaseLoc.Country__c = 'DE';
        
        foundInstBaseLoc = SCboInstalledBase.findInstalledBaseLocation(srcInstBaseLoc, accId);
        System.Assert(null == foundInstBaseLoc);
        Test.stopTest();
    }

    static testmethod void readBySerialNo()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCInstalledBase__c ib = SCboInstalledBase.readBySerialNo(SCHelperTestClass.installedBaseSingle.SerialNo__c);
        System.Assert(ib != null);
    }

    static testmethod void getInstBaseAllDepOrders()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        List<SCOrder__c> orders = SCboInstalledBase.getInstBaseAllDepOrders(SCHelperTestClass.installedBaseSingle.Id);
        
        System.Assert(orders.size() > 0);
    }

    static testmethod void getInstBaseDepOrders()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        List<SCOrder__c> orders = SCboInstalledBase.getInstBaseDepOrders(SCHelperTestClass.installedBaseSingle.Id);
        
        System.Assert(orders.size() > 0);
    }

    static testmethod void getInstLocDepOrders()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        List<SCOrder__c> orders = SCboInstalledBase.getInstLocDepOrders(SCHelperTestClass.location.Id);
        
        System.Assert(orders.size() > 0);
    }

    static testmethod void readContracts()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
                                        
        List<SCContract__c> contracts = SCboInstalledBase.readContracts(SCHelperTestClass.installedBaseSingle.Id);
        System.AssertEquals(1, contracts.size());
                                        
        contracts = SCboInstalledBase.readContracts(SCHelperTestClass.installedBaseSingle.Id, true);
        System.AssertEquals(1, contracts.size());
    }

    static testmethod void updateContractRef()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        SCHelperTestClass2.createContractTemplates(true);
        
        // create contract and contract item, trigger will update the installed base entry
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
                                        
        SCInstalledBase__c instBase = [Select Id, Name, ContractRef__c, ContractStartDate__c 
                                         from SCInstalledBase__c 
                                        where Id = :SCHelperTestClass.installedBaseSingle.Id];
        
        // contract ref should now be filled
        System.AssertNotEquals(null, instBase.ContractRef__c);
        System.AssertNotEquals(0, instBase.ContractRef__c.length());
        System.AssertEquals(Date.today(), instBase.ContractStartDate__c);
        
        // set start and end date of the contract in the past
        SCContract__c contract = new SCContract__c(Id = SCHelperTestClass2.contracts[0].Id, 
                                                   StartDate__c = Date.today() - 30, 
                                                   EndDate__c = Date.today() - 10);
        update contract;
        // update the contract item
        SCContractItem__c contrItem = [Select Id, Name from SCContractItem__c
                                        where InstalledBase__c = :SCHelperTestClass.installedBaseSingle.Id];
        update contrItem;
                                        
        instBase = [Select Id, Name, ContractRef__c, ContractStartDate__c from SCInstalledBase__c 
                     where Id = :SCHelperTestClass.installedBaseSingle.Id];
        
        // contract ref should now be empty
        System.AssertEquals(null, instBase.ContractRef__c);
        System.AssertEquals(null, instBase.ContractStartDate__c);
        
        // set start and end date of the contract to the original values
        contract.StartDate__c = Date.today();
        contract.EndDate__c = Date.today().addMonths(12);
        update contract;
        // update the contract item
        update contrItem;
                                        
        instBase = [Select Id, Name, ContractRef__c, ContractStartDate__c from SCInstalledBase__c 
                     where Id = :SCHelperTestClass.installedBaseSingle.Id];
        
        // contract ref should now be filled again
        System.AssertNotEquals(null, instBase.ContractRef__c);
        System.AssertNotEquals(0, instBase.ContractRef__c.length());
        System.AssertEquals(Date.today(), instBase.ContractStartDate__c);
        
        // set start and end date of the contract in the future
        contract.StartDate__c = Date.today() + 10;
        contract.EndDate__c = Date.today().addMonths(12);
        update contract;
        // update the contract item
        update contrItem;
                                        
        instBase = [Select Id, Name, ContractRef__c, ContractStartDate__c from SCInstalledBase__c 
                     where Id = :SCHelperTestClass.installedBaseSingle.Id];
        
        // contract ref should now be filled again
        System.AssertNotEquals(null, instBase.ContractRef__c);
        System.AssertNotEquals(0, instBase.ContractRef__c.length());
        System.AssertEquals(Date.today() + 10, instBase.ContractStartDate__c);

        // delete the contract item
        delete contrItem;
                                        
        instBase = [Select Id, Name, ContractRef__c, ContractStartDate__c from SCInstalledBase__c 
                     where Id = :SCHelperTestClass.installedBaseSingle.Id];
        
        // contract ref should now be empty
        System.AssertEquals(null, instBase.ContractRef__c);
        System.AssertEquals(null, instBase.ContractStartDate__c);
    }

    static testmethod void deleteInstallesBase()
    {
        SCHelperTestClass.createOrderTestSet2(true);
        
        SCInstalledBase__c instBase;
        SCInstalledBaseRole__c role;
        SCInstalledBaseLocation__c location;
        
        SCboInstalledBase.deleteInstBase(SCHelperTestClass.installedBaseSingle.Id);
        
        try
        {
            // the installed base has no be deleted, so the select should retrieve an error
            instBase = [Select Id from SCInstalledBase__c 
                         where Id = :SCHelperTestClass.installedBaseSingle.Id];
            System.assert(false);
        }
        catch (Exception e)
        {
            // do nothing, this error is excepted
        }

        try
        {
            // reading the role and the location should be no problem
            // they are not deleted at this time
            role = [Select Id from SCInstalledBaseRole__c 
                     where InstalledBaseLocation__c = :SCHelperTestClass.location.Id];
            location = [Select Id from SCInstalledBaseLocation__c 
                         where Id = :SCHelperTestClass.location.Id];
        }
        catch (Exception e)
        {
            System.assert(false);
        }
        
        SCboInstalledBase.deleteInstBase(SCHelperTestClass.location.Id);
        
        try
        {
            // the installed base role has no be deleted, so the select should retrieve an error
            role = [Select Id from SCInstalledBaseRole__c 
                     where InstalledBaseLocation__c = :SCHelperTestClass.location.Id];
            System.assert(false);
        }
        catch (Exception e)
        {
            // do nothing, this error is excepted
        }

        try
        {
            // the installed base location has no be deleted, so the select should retrieve an error
            location = [Select Id from SCInstalledBaseLocation__c 
                         where Id = :SCHelperTestClass.location.Id];
            System.assert(false);
        }
        catch (Exception e)
        {
            // do nothing, this error is excepted
        }
    }

    /*
     * Test the formula in the field SCInstalledBase__c.GuaranteeUntil__c.
     *
     * The formula should be:
     * if(OR(ISNULL(GuaranteeExtended__c), InstallationDate__c > GuaranteeExtended__c), 
     *    if(TODAY() > GuaranteeStandard__c, NULL, GuaranteeStandard__c),
     *    if(TODAY() > GuaranteeExtended__c, NULL, GuaranteeExtended__c))
     */
    /* GMSAW: auskommentiert, weil die Formel ständig geändert wird und sowieso keine Codefunktionalität damit getestet wird
    static testmethod void testWarrantyFormula()
    {
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createProductModel(true);
        SCInstalledBase__c instBase = new SCInstalledBase__c(Status__c = 'active',
                                                             InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                                             Type__c = 'Appliance',
                                                             ProductModel__c = SCHelperTestClass.prodModel.Id, 
                                                             ProductUnitClass__c = SCHelperTestClass.prodModel.UnitClass__c, 
                                                             ProductUnitType__c = SCHelperTestClass.prodModel.UnitType__c, 
                                                             ProductGroup__c = SCHelperTestClass.prodModel.Group__c, 
                                                             ProductPower__c = SCHelperTestClass.prodModel.Power__c, 
                                                             Brand__c = SCHelperTestClass.prodModel.Brand__c);
        insert instBase;
        
        Date instDate1 = Date.today().addYears(-3);
        Date instDate2 = Date.today().addMonths(-6);
        Date guarExt1 = Date.today().addMonths(3).addYears(2);
        Date guarExt2 = Date.today().addMonths(3).addYears(-1);
        Date guarantee = Date.today().addMonths(-6).addYears(2);

        Test.startTest();

        // Test 1: no installation date and no guarantee extend -> no guarantee
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(null, instBase.GuaranteeUntil__c);

        // Test 2: installation date2 and no guarantee extend -> guarantee = installation date2 + 2 years
        instBase.InstallationDate__c = instDate2;
        update instBase;
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(guarantee, instBase.GuaranteeUntil__c);

        // Test 3: installation date1 and no guarantee extend -> no guarantee
        instBase.InstallationDate__c = instDate1;
        update instBase;
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(null, instBase.GuaranteeUntil__c);

        // Test 4: no installation date and guarantee extend1 -> guarantee = guarantee extend1
        instBase.InstallationDate__c = null;
        instBase.GuaranteeExtended__c = guarExt1;
        update instBase;
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(guarExt1, instBase.GuaranteeUntil__c);

        // Test 5: installation date2 and guarantee extend1 -> guarantee = guarantee extend2
        instBase.InstallationDate__c = instDate2;
        instBase.GuaranteeExtended__c = guarExt1;
        update instBase;
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(guarExt1, instBase.GuaranteeUntil__c);

        // Test 6: installation date2 and guarantee extend2 -> guarantee = installation date2 + 2 years
        instBase.InstallationDate__c = instDate2;
        instBase.GuaranteeExtended__c = guarExt2;
        update instBase;
        instBase = [Select Id, GuaranteeUntil__c from SCInstalledBase__c where Id = :instBase.Id];
        System.assertEquals(guarantee, instBase.GuaranteeUntil__c);
        
        Test.stopTest();
    }
    */
}