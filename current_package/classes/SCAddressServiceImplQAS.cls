/*
 * @(#)SCAddressServiceImplQAS.cls 
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the address validation service that accesses 
 * the 'https://ws.ondemand.qas.com/ProOnDemand/V2/ProOnDemandService.asmx'. 
 * This service is activated
 * depending on the country by SCAddressValidation.
 *
 *    SCAddressValidation v = new SCAddressValidation();
 *    List<AvsAddress> foundaddr;
 *    foundaddr = v.check('GB', 'West Midlands, Sutton Coldfield, 549 Chester Road, B73 5HU');
 *    System.debug('result:' + foundaddr);
*/

public with sharing class SCAddressServiceImplQAS implements SCAddressService
{
    private static String productive = 'productive';
    private static String devTest = 'developer';
    private static String statTest = 'test';
    String mode = productive;     // for productive set productive !!!!

    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    String country = 'GX2';    // for the VaillantLayout    
    // Engine properties for check
    boolean checkFlatten = true;
    String checkIntensity = 'Close';     // It could be set to Exact to get quicker results but 
                                         // perhaps not all addresses if in the search string
                                         // where some mistakes
    String checkPromptSet = 'Default';
    Integer checkThreshold = 50;        // the maximal number of the results
    Integer checkTimeout = 10000;
    String checkLayout = 'VaillantBN1';   // a layout for the flatten mode is not important
    String geocodeLayout = 'VaillantBN1'; // to get longitude and latitude
                                          // previously it was VaillantLayout  
    
    /**
    * Authenticates the web service
    * on behalf of hard coded user and password.
    *
    * @param     wsQAS     the webservice instance
    */
    public void authentication(wwwQasComOndemand201001.QASOnDemand wsQAS)
    {
        wwwQasComOndemand201001.QAQueryHeader qAQueryHeader = new wwwQasComOndemand201001.QAQueryHeader();
        wwwQasComOndemand201001.QAAuthentication qAAuthentication =  new wwwQasComOndemand201001.QAAuthentication();
        
        if (mode == devTest)
        {
            QAAuthentication.Username = '3145eca2-c41';
            QAAuthentication.Password = 'qaseps2011';
        }
        else 
        {
            QAAuthentication.Username = appSettings.ADDRESS_SERVICE_QAS_U__c;
            QAAuthentication.Password = appSettings.ADDRESS_SERVICE_QAS_PW__c;
        }
        QAQueryHeader.QAAuthentication = qAAuthentication;
        wsQAS.QAQueryHeader = qAQueryHeader;
    }
    /**
     * Check the query directly and return the result set
     * 
     * @param addr address object with all needed data
     * @return a list of possible addresses
     */
    public AvsResult check(AvsAddress addr)
    {
        if(mode == statTest)
        {
            return checkEmulation(addr);
        }
        else
        {
            return checkReal(addr);
        }
    }
     
    /**
     * Check the query directly on the web service and return the result set
     * 
     * @param addr address object with all needed data
     * @return a list of possible addresses
     */
    public AvsResult checkReal(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            // instantiate the web service
            wwwQasComOndemand201001.QASOnDemand wsQAS = new wwwQasComOndemand201001.QASOnDemand();
            
            // authenticate
            authentication(wsQAS);
            
            // instatiate the search engine   
            wwwQasComOndemand201001.EngineType engine = new wwwQasComOndemand201001.EngineType();
            engine.Flatten = checkFlatten;
            engine.Intensity = checkIntensity;
            engine.PromptSet = checkPromptSet;
            engine.Threshold = checkThreshold;
            engine.Timeout = checkTimeout;
    
            // make the search string
            String search = '';
            // the country and country state are not allowed
            // if they there in the address no item is found
            
            search = addItemToSearch(search, addr.county);
            search = addItemToSearch(search, addr.postalcode);
            search = addItemToSearch(search, addr.city);
            search = addItemToSearch(search, addr.district);
            search = addItemToSearch(search, addr.housenumber);
            search = addItemToSearch(search, addr.street);
            System.debug(search);
    
            // call search in the web service
            wwwQasComOndemand201001.QASearchResult_element wsResult = 
                                wsQAS.DoSearch(country, engine, checkLayout, search);
            // Are there any results?
            if (wsResult == null || wsResult.QAPicklist == null 
                || wsResult.QAPicklist.PicklistEntry == null)
            {
                // there no results
                result.status = AvsResult.STATUS_EMPTY;
                result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                /*
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                System.Label.SC_app_AddressValidationEmpty);
                ApexPages.addMessage(myMsg);
                */
                return result;
            }
            // There are results
            // loop over all results
            wwwQasComOndemand201001.PicklistEntryType[] picklistEntryArr = wsResult.QAPicklist.PicklistEntry;
            for( wwwQasComOndemand201001.PicklistEntryType i: wsResult.QAPicklist.PicklistEntry)
            {
                // work out of one entry in the list
                wwwQasComOndemand201001.PicklistEntryType pickListEntry = i;
                
                System.debug('#### qas entry: ' + i);
                
                // get address from the picklist of one entry
                String picklist = i.Picklist;
                System.debug('#### picklist from qas: ' + picklist);
                
                // get moniker
                String moniker = i.Moniker;
                
                // transfer the result into the return object
                AvsAddress item = new AvsAddress();

                // the address is set in the city 
                item.city = picklist;

                // postcode
                item.postalcode = i.Postcode;
                            
                // set the geo codes to zeros for the sake of the functioning of 
                // the address validation framework 
                item.geoX = 0;
                item.geoY = 0;
                item.country = 'GB';
                
                // set the moniker for the next call for getting geo codes
                item.moniker = moniker;
                
                // add the found address to the result list
                result.items.add(item);
            }//for            
            
            // now set the status values for the framework of address validation
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
        }
        catch(Exception e)
        {
            System.debug(e);
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e;
        }
        return result;        
    }
    
    /**
    *    adds an item to the search string
    */
    private String addItemToSearch(String search, String item)
    {
        String ret = search;
        if(item != null && !item.equals(''))
        {
            if(!search.equals(''))
            {
                ret += ', ';
            }
            ret += item;
        }
        return ret;
    }

    /**
     * Emulates the check of query for the test statistics and return the result set.
     * 
     * @param addr address object with all needed data
     * @return a list of possible addresses
     */
    public AvsResult checkEmulation(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        AvsAddress item = new AvsAddress();
        item.city = 'West Midlands, Sutton Coldfield, 549 Chester Road, B73 5HU';
        item.geoX = 0;
        item.geoY = 0;
        item.country = 'GB';
        // add the found address to the result list
        result.items.add(item);
        // now set the status values
        if(result.items.size() == 1)
        {
            result.status = AvsResult.STATUS_EXACT;
        } 
        else if(result.items.size() > 1)
        {
            result.status = AvsResult.STATUS_MULTI;
        }
        else
        {
            result.status = AvsResult.STATUS_BLANK;
        }
        return result;
    }
    /**
     * Emulates determining of longitude and latitude for an address for 
     * the test purposes.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public AvsResult geocode(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        if(mode == statTest)
        {
            result = geocodeEmulation(addr);
        }
        else
        {
            result = geocodeReal(addr);
        }       
        return result;
    }
    /**
     * Determines the longitude and latitude for an address on the real web service.
     * Also sets the address fields.
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public AvsResult geocodeReal(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            // instantiate the web service
            wwwQasComOndemand201001.QASOnDemand wsQAS = new wwwQasComOndemand201001.QASOnDemand();
            
            // authenticate
            authentication(wsQAS);   
            
            // check moniker
            if(addr.moniker != null && !addr.moniker.equals(''))
            {
                // call the web service to get an address on behalf of moniker
                // and the GBRGrids layout
                wwwQasComOndemand201001.QAAddressType wsResult = wsQAS.DoGetAddress(geocodeLayout,addr.moniker);
                
                // Are there any results?
                if (wsResult == null || wsResult.AddressLine == null)
                {
                    // There are no results
                    result.status = AvsResult.STATUS_EMPTY;
                    result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                    /*
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                    System.Label.SC_app_AddressValidationEmpty);
                    ApexPages.addMessage(myMsg);
                    */
                    return result;
                }

                // transfer the result into the return object
                AvsAddress item = new AvsAddress();
                
                // add the found address to the result list
                result.items.add(item);

                // loop over the lines of the address returned
                wwwQasComOndemand201001.AddressLineType[] addressLineTypeArr = wsResult.AddressLine;
                Integer count = 0;
                Integer cntEmptyLabel = 0;
                item.housenumber = null;
                String dependentThoroughfare = null;
                for( wwwQasComOndemand201001.AddressLineType i: wsResult.AddressLine)
                {
                    // work out the line of the address
                    wwwQasComOndemand201001.AddressLineType  addressLineReal = i;
                    String label = i.Label;
                    label = label.trim();
                    
                    String line = i.Line;
                    line = line.trim();
                    if(mode == devTest)
                    {
                        System.debug('\n................' + label);
                        System.debug('\n................' + line);
                    }
                    if(label != null && label.equals('Title'))
                    {
                        // item.title = line; later remove 
                        item.salutation = line;
                    }
                    if(label != null && label.equals('Forename'))
                    {
                        item.firstname = line;
                    }
                    if(label != null && label.equals('Surname'))
                    {
                        item.surname = line;
                    }
                    if(label != null && label.equals('Organisation'))
                    {
                        item.organisation = line;
                    }
                    if(label != null && label.equals('Telephone Number'))
                    {
                        item.telephoneNumber = line;
                    }
                    if(label != null && label.equals('Fax Number'))
                    {
                        item.faxNumber = line;
                    }
                    if(label != null && label.equals('NAM Date of Birth'))
                    {
                        try
                        {
                            item.birthdate = date.parse(line);
                        }
                        catch(Exception e)
                        {
                            System.debug('###Parsing of date ' + line + ' failed!');
                        }    
                    }

                    if(label == null || label != null && label.equals(''))
                    {
                        item.district = line;
                    }
                    
                    if(label != null && label.equals('Thoroughfare'))
                    {
                        item.street = line;
                    }
                    
                    if(label != null && label.equals('Dependent thoroughfare'))
                    {
                        dependentThoroughfare = line;
                    }

                    if(label != null  
                       && ( label.equals('Building number')
                               || label.equals('Building name')
                          )
                       )
                    {
                        if(item.housenumber == null || item.housenumber == '')
                        {
                            item.housenumber = line;
                        }
                        else
                        {
                            if(line != null && line != '')
                            {
                                item.housenumber += ', ' + line;
                            }    
                        }    
                    }
                    
                    if(label != null && label.equals('Town'))
                    {
                        item.city = line;
                    }
                    if(label != null && label.equals('County'))
                    {
                        item.county = line;
                        System.debug('County: ' + item.county);
                    }
                    if(label != null && label.equals('Postcode'))
                    {
                        item.postalcode = line;
                    }
                    if(label != null && label.equals('100m Longitude'))
                    {
                        item.geoX = double.valueOf(line);
                    }
                    if(label != null && label.equals('100m Latitude'))
                    {
                        item.geoY = double.valueOf(line);
                    }
                    item.country = 'GB';
                    count++;
                }//for
                if(dependentThoroughfare != null && dependentThoroughfare != '')
                {
                    item.street = dependentThoroughfare;
                }
            }
            else
            {
                // There are no moniker
                System.debug('No moniker for the address' + addr);
                result.status = AvsResult.STATUS_EMPTY;
                result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                /*
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                System.Label.SC_app_AddressValidationEmpty);
                ApexPages.addMessage(myMsg);
                */
                return result;
                
            }
            // now set the status values
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
            // set the originial status info of the service
            result.statusInfo = 'webservices.qas: -'; 
        }
        catch(Exception e)
        {
            System.debug(e);
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e;
        }
        return result;        
    }//geocodeReal
    
    /**
    *    Emulates geocoding of an address. Implemented for the
    *    purpose of improving test statistics.
    */
    public AvsResult geocodeEmulation(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        // transfer the result into the return object
        AvsAddress item = new AvsAddress();
        // add the found address to the result list
        result.items.add(item);

        item.street = '549 Chester Road';
        item.city = 'Sutton Coldfield';
        item.county = 'West Midlands';
        item.postalcode = 'B73 5HU';
        item.geoX = double.valueOf('-1.839254');
        item.geoY = double.valueOf('52.537576');
        item.country = 'GB';
       // now set the status values
        if(result.items.size() == 1) 
        {
            result.status = AvsResult.STATUS_EXACT;
        } 
        else if(result.items.size() > 1)
        {
            result.status = AvsResult.STATUS_MULTI;
        }
        else
        {
            result.status = AvsResult.STATUS_BLANK;
        }
        // set the originial status info of the service
        result.statusInfo = 'webservices.qas: -'; 
        return result;        
  }//geocode
 
}