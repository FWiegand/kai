/*
 * @(#)SCboOrderRepairCode.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderRepairCode
{
    public SCOrderRepairCode__c repairCode { get; private set; }

    public SCboOrderRepairCode()
    {
        this.repairCode = new SCOrderRepairCode__c();
    } // SCboOrderRepairCode
    
    public SCboOrderRepairCode(SCOrderRepairCode__c repairCode)
    {
        this.repairCode = repairCode;
    } // SCboOrderRepairCode

    /**
     * Save all 
     *
     */
    public void save()
    {
        try
        {
            //#### Update repair code ###
            upsert repairCode;
        }
        catch (DmlException e)
        {
            throw new SCfwOrderException('Unable to save repair code ' + e.getMessage());
        }
    } // save
} // class SCboOrderRepairCode