/*
 * @(#)SCTimereportItemEditControllerTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCTimereportItemEditControllerTest
{
    private static SCTimereportItemEditController itemEditController;

    /**
     * ItemEditController under positiv test 1
     */
    static testMethod void itemEditControllerPositiv1()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for day start in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(0).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();
        itemEditController.addTimeRepItemWithMode();

        // tid and mode should be not empty
        System.assertequals(true, itemEditController.getRenderDialog());
    }

    /**
     * ItemEditController under positiv test 2
     */
    static testMethod void itemEditControllerPositiv2()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(1).Id);

        // call parameter for secound entry into time report (add mode)
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(1).Id);
        ApexPages.currentPage().getParameters().put('mode', 'addMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();
        itemEditController.addTimeRepItemWithMode();
        itemEditController.getTimeRepTypes();
    }

    /**
     * ItemEditController under positiv test 3
     */
    static testMethod void itemEditControllerPositiv3()
    {
        SCTimereportViewController timeRepViewController = new SCTimereportViewController ();

        SCHelperTestClass2.createTimeReportSet(true);

        // call parameter for secound entry into time report (add mode)
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(1).Id);
        ApexPages.currentPage().getParameters().put('mode', 'addMode');

        // test init edit controller with SCTimereportViewController
        itemEditController = new SCTimereportItemEditController(timeRepViewController);
    }

    /**
     * ItemEditController under negativ test 1
     */
    static testMethod void itemEditControllerNegativ1()
    {
        //SCHelperTestClass2.createTimeReportSet(true);

        // set empty apex pages parameter
        ApexPages.currentPage().getParameters().put('tid', null);
        ApexPages.currentPage().getParameters().put('mode', null);

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // tid and mode should be empty
        System.assertequals(false, itemEditController.getRenderDialog());
    }

    /**
     * ItemEditController under negativ test 2
     */
    static testMethod void itemEditControllerNegativ2()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for day start in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(0).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // set end time < start time --> 8:00 to 7:45
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 7, 45, 0);

        itemEditController.addTimeRepItemWithMode();

        // end and start time should be equal
        System.assertequals(itemEditController.timereportSingle.End__c,
                            itemEditController.timereportSingle.Start__c);
    }

    /**
     * ItemEditController under negativ test 3
     */
    static testMethod void itemEditControllerNegativ3()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for 4th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(3).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // set end time < start time --> 11:30 to 9:15
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 9, 15, 0);

        itemEditController.addTimeRepItemWithMode();

        // end and start time should be equal
        //System.assertequals(itemEditController.timereportSingle.End__c,
        //                    itemEditController.timereportSingle.Start__c);
        itemEditController.addTimeRepItemWithMode();
        System.assertEquals(false, itemEditController.allOk);       
    }

    /**
     * ItemEditController under negativ test 4
     */
    static testMethod void itemEditControllerNegativ4()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for 4th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(7).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // set the secound day start into time report before --> 9:30 to 9:15
        // --> overlaps another time report
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 2, 9, 15, 0);

        //PageReference testPageReference = itemEditController.addTimeRepItemWithMode();
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());
        
        itemEditController.addTimeRepItemWithMode();
        //System.assertEquals(false, itemEditController.allOk);
    }

    /**
     * ItemEditController under negativ test 5
     */
    static testMethod void itemEditControllerNegativ5()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for 2th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(1).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // set the time before time report starts --> 8:00 to 6:00
        // --> day start after time report entry
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 1, 6, 0, 0);
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 6, 30, 0);

        // ApexPages.Severity.ERROR
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());

        // reset the time values
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 1, 8, 0, 0);
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 8, 30, 0);
        // set time report end < start
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 1, 8, 0, 0);
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 7, 30, 0);

        // ApexPages.Severity.ERROR
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());

        // reset the time values
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 1, 8, 0, 0);
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 8, 30, 0);
        // set mileage negativ
        itemEditController.timereportSingle.Mileage__c = - 10;
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());

        // set distance negativ
        itemEditController.timereportSingle.Mileage__c = 12987;
        itemEditController.timereportSingle.Distance__c = - 10;
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());
        
        itemEditController.addTimeRepItemWithMode();
        //System.assertEquals(false, itemEditController.allOk);     
    }

    /**
     * ItemEditController under negativ test 6
     */
    static testMethod void itemEditControllerNegativ6()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // System.debug('#### itemEditControllerPositiv ():'+ SCHelperTestClass2.timeReportDay1.get(0).Id);

        // call parameter for 2th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(12).Id);
        ApexPages.currentPage().getParameters().put('mode', 'addMode');
        ApexPages.currentPage().getParameters().put('isNewEntry', 'timeReportNew');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // the mileage also checks into the page with a java script function
        // set mileage less than mileage before, 12997 + 40 = 13037 --> less than 12997   
        System.debug('### Mileage is: '+itemEditController.timereportSingle.Mileage__c);
        System.debug('### last Mileage is: '+itemEditController.lastMileageEntry);        
        itemEditController.addTimeRepItemWithMode();
        //CCE System.assertEquals(false, itemEditController.allOk);
        
        // the duration is greater than 24 hours
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(13).Id);
        itemEditController = new SCTimereportItemEditController();
        itemEditController.addTimeRepItemWithMode();
        System.assertEquals(false, itemEditController.allOk);
        
        // day start after the current time report found
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(16).Id);
        itemEditController = new SCTimereportItemEditController();
        itemEditController.addTimeRepItemWithMode();
        //CCE System.assertEquals(false, itemEditController.allOk);
        
        // time report type equals null
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(18).Id);
        itemEditController = new SCTimereportItemEditController();
        itemEditController.addTimeRepItemWithMode();
        System.assertEquals(false, itemEditController.allOk);           
    }

    /**
     * ItemEditController under negativ test 7
     */
    static testMethod void itemEditControllerNegativ7()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // call parameter for 2th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(8).Id);
        ApexPages.currentPage().getParameters().put('mode', 'editMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        // set the time before time report starts --> 8:00 to 6:00
        // --> day start after time report entry
        itemEditController.timereportSingle.Start__c = Datetime.newInstance(2010, 9, 1, 6, 0, 0);
        itemEditController.timereportSingle.End__c = Datetime.newInstance(2010, 9, 1, 6, 30, 0);
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());
        itemEditController.addTimeRepItemWithMode();
        //System.assertEquals(false, itemEditController.allOk);
    }

    /**
     * ItemEditController under positiv test -> add function
     */
    static testMethod void itemEditControllerPositiv4()
    {
        SCHelperTestClass2.createTimeReportSet(true);

        // call parameter for 2th in edit mode
        ApexPages.currentPage().getParameters().put('tid', SCHelperTestClass2.timeReportDay1.get(8).Id);
        ApexPages.currentPage().getParameters().put('mode', 'addMode');

        // test init edit controller
        itemEditController = new SCTimereportItemEditController();

        itemEditController.newEntryTimeReport.Start__c = Datetime.newInstance(2010, 9, 2, 9, 30, 0);
        itemEditController.newEntryTimeReport.End__c = Datetime.newInstance(2010, 9, 2, 10, 30, 0);
        itemEditController.newEntryTimeReport.Mileage__c = 13047;
        itemEditController.newEntryTimeReport.Distance__c = 10;
        
        System.assertequals(null, itemEditController.addTimeRepItemWithMode());
        
        itemEditController.newEntryTimeReport.Start__c = Datetime.newInstance(2010, 9, 2, 12, 30, 0);
        itemEditController.newEntryTimeReport.End__c = Datetime.newInstance(2010, 9, 2, 12, 45, 0);
        
        System.assertEquals (SCHelperTestClass2.timeReportDay1.get(8).ReportLink__c,
                             itemEditController.newEntryTimeReport.ReportLink__c);
        itemEditController.addTimeRepItemWithMode();
        // time report should be added --> allOk
        //System.assertEquals(true, itemEditController.allOk);
    }
}