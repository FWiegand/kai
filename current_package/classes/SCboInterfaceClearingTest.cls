/**
 * @(#)SCInterfaceClearingTest
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboInterfaceClearingTest 
{

    static testMethod void testValidation() 
    {
        createOrders();
        Test.startTest();
        
        SCInterfaceClearing__c clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'materialmovement'); 
        insert clearing;
        
        //MaterialMovement
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks.get(0).Id; 
        
        addAttachment(matmove1, clearing.id);
        
        SCboInterfaceClearing boClearing = new SCboInterfaceClearing(clearing);
        //TODO system.assert(boClearing.save(),'Materialmovement not valid');
        
        //TODO system.assert(SCboInterfaceClearing.validate(clearing.Id),'Materialmovement not valid');
        
        //InstalledBase
        
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'installedbase'); 
        insert clearing;
        
        SCInstalledBase__c iBase = new SCInstalledBase__c
        (
            SerialNo__c = '00305005',
            Status__c = 'active',
            Type__c = 'Appliance',
            ProductSkill__c = 'PS1'
            //Brand__c = SCHelperTestClass.brand.Id 
        );
        insert iBase;
        
        //Not Valid
        addAttachment(iBase, clearing.id);
        //CCE disabled system.assert(!SCboInterfaceClearing.save(String.valueOf(clearing.id)), 'Installed Base should not be valid');
        
        //Valid
        iBase.Brand__c = SCHelperTestClass.brand.Id;
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'installedbase'); 
        insert clearing;
        addAttachment(iBase, clearing.id);
        system.assert(SCboInterfaceClearing.save(String.valueOf(clearing.id)), 'Installed Base not saved');
        
        //timereport
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(1).Id, Type__c = 'timereport'); 
        insert clearing;
        
        Boolean dayStart = false;
        Boolean dayEnd = false;
            
        for(SCTimeReport__c report : SCHelperTestClass2.timeReportDay1 )
        {
            if(report.Resource__c == SCHelperTestClass.resources.get(1).Id && report.Type__c != null)
            {
                //allow only one daystart and one dayend
                if(report.Type__c.equals(SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART))
                {
                    if(dayStart)
                    {
                        continue;   
                    }
                    dayStart = true;
                }
                else if(report.Type__c.equals(SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND))
                {
                    if(dayEnd)
                    {
                        continue;   
                    }
                    dayEnd = true;
                }
                
                addAttachment(report, clearing.Id, 'I');
            }
        }
        
        //implicit save by trigger Async 
        clearing.Status__c = SCboInterfaceClearing.STATUS_PENDING;
        update clearing;
        
        //system.assert(SCboInterfaceClearing.save(clearing,false),'TimeReport not saved');
        
        
        //mobileorder without subobjects
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'mobileorder'); 
        insert clearing;
        
        addAttachment(SCHelperTestClass.order, clearing.Id, 'U');
        
        boClearing = new SCboInterfaceClearing(clearing.Id);
        //TODO system.assert(boClearing.save(),'Order not saved: ID=' + SCHelperTestClass.order.Id);
        
        //mobileorder with subobjects
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'mobileorder'); 
        insert clearing;
        
        SCOrderItem__c orderItem = new SCOrderItem__c
        (
            Duration__c = 60,
            CompletionStatus__c = '58701',
            DurationUnit__c = '9101',
            ErrorCondition1__c = '01',
            ErrorCondition2__c = '',
            ErrorSymptom1__c = '07',
            ErrorSymptom2__c = '',
            ErrorSymptom3__c = '',
            ErrorText__c = 'not hot enough',
            Order__c = SCHelperTestClass.order.Id,
            InstalledBase__c = iBase.Id
        );
        
        insert orderItem;
        
        SCOrderLine__c orderLine = new SCOrderLine__c();
        orderLine.ListPrice__c = 0;
        orderLine.Price__c = 0;
        orderLine.PositionPrice__c = 0;
        orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        orderLine.Tax__c = 0;
        orderLine.TaxRate__c = 0;
        orderLine.Discount__c = 0;
        orderLine.RelativeDiscount__c = 0;
        orderLine.UnitNetPrice__c = 0;
        orderLine.UnitPrice__c = 0;
        orderLine.PositionPrice__c = 0;
        orderLine.Order__c = SCHelperTestClass.order.Id;
        
        orderLine.Article__c = SCHelperTestClass.article.Id;
        orderLine.Qty__c = 1;
        orderLine.MaterialStatus__c = '5408';
        orderLine.PriceType__c = '52304';
        orderLine.InvoicingType__c = '170002';
        orderLine.InvoicingSubtype__c = '180008';
        orderLine.ContractRelated__c = false;
        
        
        insert orderLine;
        
        
        SCOrderLine__c orderLine2 = orderLine.clone();
        orderLine2.OrderItem__c = orderItem.Id;
        
        //insert orderLine2;
        //Order repair code
        SCOrderRepairCode__c rCode = new SCOrderRepairCode__c(Order__c = SCHelperTestClass.order.Id, OrderItem__c = orderItem.Id, ErrorSymptom1__c = '03');
        
        
        addAttachment(SCHelperTestClass.order, clearing.Id, 'U');
        addAttachment(iBase, clearing.Id, 'U');
        addAttachment(orderItem, clearing.Id, 'U');
        addAttachment(orderLine, clearing.Id, 'U');
        addAttachment(orderLine2, clearing.Id, 'I');
        addAttachment(SCHelperTestClass.orderRoles.get(0), clearing.Id, 'U');
        addAttachment(rCode, clearing.Id, 'I');
        
        
        boClearing = new SCboInterfaceClearing(clearing.Id);
        //TODO system.assert(boClearing.save(),'Order not saved: ID=' + SCHelperTestClass.order.Id);
        
        //orderassignment status change
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'orderassignment'); 
        insert clearing;
        
        SCAssignment__c assignment = SCHelperTestClass.assignmentMap.values().get(0);
        assignment.Status__c = '5503';
        addAttachment(assignment, clearing.Id, 'U');
        
        clearing.Status__c = SCboInterfaceClearing.STATUS_PENDING;
        update clearing;
        
        clearing = [SELECT Status__c FROM SCInterfaceClearing__c WHERE ID =:clearing.Id LIMIT 1];
        //SCboInterfaceClearing.save(clearing,false)
        //TODO system.assert(clearing.Status__c.equals(SCboInterfaceClearing.STATUS_COMPLETED),'Assignment could not be updated');
        
        //only for Codecoverage
        //MaterialMovement
        clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id, Type__c = 'materialmovement'); 
        insert clearing;
        
        matmove1.Order__c       = null;
        matmove1.OrderLine__c   = null;
        matmove1.Article__c     = null;
        matmove1.Qty__c         = null;
        matmove1.RequisitionDate__c = null;
        matmove1.Status__c      = null;
        matmove1.Type__c        = null;
        matmove1.Stock__c       = null;
        
        addAttachment(matmove1, clearing.id, 'I');
        
        boClearing = new SCboInterfaceClearing(clearing);
        //###  system.assert(!boClearing.validate(),'Materialmovement should not be valid');
         
        Test.stopTest();
        
    }
    
    static testMethod void testObjectConversion() 
    {
        
        createOrders();
        
        
        
        SCInterfaceClearing__c clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(0).Id); 
        insert clearing;
        
        //InstalledBase
        SCInstalledBase__c iBase = new SCInstalledBase__c
        (
            SerialNo__c = '00305005',
            Status__c = 'active',
            Type__c = 'Appliance',
            ProductSkill__c = 'PS1' 
        );
        
        //OrderLine
        SCOrderLine__c orderLine = new SCOrderLine__c();
        orderLine.ListPrice__c = 0;
        orderLine.Price__c = 0;
        orderLine.PositionPrice__c = 0;
        orderLine.Type__c = SCfwConstants.DOMVAL_ORDERLINETYPE_INV;
        orderLine.Tax__c = 0;
        orderLine.TaxRate__c = 0;
        orderLine.Discount__c = 0;
        orderLine.RelativeDiscount__c = 0;
        orderLine.UnitNetPrice__c = 0;
        orderLine.UnitPrice__c = 0;
        orderLine.PositionPrice__c = 0;
       
        //OrderItem
        SCOrderItem__c orderItem = new SCOrderItem__c
        (
            Duration__c = 60,
            CompletionStatus__c = '58701',
            DurationUnit__c = '9101',
            ErrorCondition1__c = '01',
            ErrorCondition2__c = '',
            ErrorSymptom1__c = '07',
            ErrorSymptom2__c = '',
            ErrorSymptom3__c = '',
            ErrorText__c = 'not hot enough'
        );
        
        //Appointment
        SCAppointment__c appointment = new SCAppointment__c
        (
            Start__c = Date.today(),
            End__c =   Date.today()+1,
            Assignmentstatus__c = SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED,
            Description__c = 'Date1',
            PlanningType__c = '802',
            Class__c = '7301'
        ); 
        
        //MaterialMovement
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks.get(0).Id;
        
        //Test the Class itself
        Test.startTest();
        readWriteAttachment(SCHelperTestClass.order, clearing.Id);
        readWriteAttachment(iBase, clearing.Id);
        readWriteAttachment(orderLine, clearing.Id);
        readWriteAttachment(orderItem, clearing.Id);
        readWriteAttachment(SCHelperTestClass.orderRoles.get(0), clearing.Id);
        readWriteAttachment(SCHelperTestClass.assignmentMap.values().get(0), clearing.Id);
        readWriteAttachment(appointment, clearing.Id);
        readWriteAttachment(SCHelperTestClass2.timeReportDay1.get(0), clearing.Id);
        readWriteAttachment(matmove1, clearing.Id);
        SCboInterfaceClearing.getAttachments(clearing.Id);
        SCboInterfaceClearing.getObjects(clearing.Id);
        Test.stopTest();
    }
    
    
    
    /**
     * converts the sObject to JSON and back to sObject.
     * 
     */ 
    static void readWriteAttachment(sObject obj, String clearingId)
    {
        //avoid trigger modifications
        sObject objCopy = obj.clone();
        Attachment att = new Attachment(parentId=clearingId, Name='unitTest', Body=Blob.valueOf('unitTest'), Description = 'I');
        insert att;
        
        SCboInterfaceClearing.saveObjectToAttachment(att,objCopy);
        sObject o = SCboInterfaceClearing.getObjectFromAttachment(String.valueOf(att.id));
        system.assert(objCopy.getSObjectType() == o.getSObjectType(), 'Objects not equal: Original=' + objCopy + ' After convert=' + o);
    }
    
    static void addAttachment(sObject obj, String clearingId, String activity)
    {
        Attachment att = new Attachment(parentId=clearingId, Name='unitTest', Body=Blob.valueOf('unitTest'), Description = activity);
        insert att;
        
        SCboInterfaceClearing.saveObjectToAttachment(att,obj);
    }
    
    static void addAttachment(sObject obj, String clearingId)
    {
        addAttachment(obj, clearingId, 'I');
    }
    
    static void createOrders()
    {
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        SCHelperTestClass.createTestUsers(true);
        //SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);
        
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, false);
        SCHelperTestClass.createArticle(true);
    }
    
    
    //@author GMSS
    static testMethod void CodeCoverageA() 
    {
        SCHelperTestClass3.createCustomSettings('DE',true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCArticle__c article = new SCArticle__c();
        Database.insert(article);
        
        SCPlant__c plant = new SCPlant__c();
        Database.insert(plant);
        
        SCStock__c stock = new SCStock__c(Plant__c = plant.Id);
        Database.insert(stock);
        
        SCInstalledBase__c instBase = new SCInstalledBase__c();
        Database.insert(instBase);
        
        SCResource__c resource = new SCResource__c(Alias_txt__c = 'test');
        Database.insert(resource);
        
        SCOrder__c order = new SCOrder__c();
        Database.insert(order);
        
        SCMaterialMovement__c matMove = new SCMaterialMovement__c();
        SCStockItem__c stockItem = new SCStockItem__c(Stock__c = stock.Id, Article__c = article.Id);
        SCTimeReport__c timeReport = new SCTimeReport__c();
        
        SCAppointment__c appointment = new SCAppointment__c(Order__c = order.Id);
        SCAssignment__c assignment = new SCAssignment__c(Order__c = order.Id);
        SCOrderItem__c orderItem = new SCOrderItem__c(Order__c = order.Id, InstalledBase__c = instBase.Id);
        SCOrderLine__c orderLine = new SCOrderLine__c(Order__c = order.Id);
        SCOrderRepairCode__c orderRepCode = new SCOrderRepairCode__c(Order__c = order.Id);
        SCOrderRole__c orderRole = new SCOrderRole__c(Order__c = order.Id);
        
        SCInterfaceClearing__c interfaceClearing = new SCInterfaceClearing__c(Resource__c = resource.Id);
        Database.insert(interfaceClearing);
        
        
        Test.startTest();
        
        
        SCboInterfaceClearing obj = new SCboInterfaceClearing(interfaceClearing.Id);
        
        obj.getErrorMessages();
        obj.writeMessageNotValid(null);
        obj.writeMessageNotValid('test');
        
        Test.setCurrentPage(Page.SCAppointmentPageContract);
        
        obj.writeMessageNotValid('test');
        
        
        for (sObject sObj : new List<sObject>{matMove, orderRole, appointment, assignment, instBase,
                                              orderLine, orderItem, orderRepCode, order, timeReport,
                                              stock, stockItem})
        {
            SCboInterfaceClearing.ObjectWrapper objWrapper = new SCboInterfaceClearing.ObjectWrapper(sObj);
            sObject tmp = objWrapper.getObject();
            try { objWrapper.upsertObject(); } catch(Exception e) { }
        }
        
        try { SCboInterfaceClearing.ObjectWrapper tmp = new SCboInterfaceClearing.ObjectWrapper(new Account()); } catch(Exception e) { }
        
        SCboInterfaceClearing.ObjectWrapper matMoveWrapper = new SCboInterfaceClearing.ObjectWrapper(matMove);
        matMoveWrapper.materialmovement = null;
        matMoveWrapper.getObject();
        
        Test.stopTest();
    }
}