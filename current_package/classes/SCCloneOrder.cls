/* 
 * @(#)SCCloneOrder.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.  
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use  
 * it only in accordance with the terms of the license agreement
 * you entered into with GM
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
 
global without sharing class SCCloneOrder
{
    WebService static String cloneOrder(String oid)
    {
        Database.SaveResult sr;
        Id clonedOrderId;
        SCOrder__c order = new SCOrder__c();
        
        order = [SELECT id, Name, ValidationStatus__c, UsedBy__c, Type__c, Standby__c,
                        PriceList__c, PaymentType__c, OwnerId, 
                        InvoicingType__c, InvoicingSubtype__c, InsurancePolicy__c, Info__c,
                        IdOld__c, IdExt__c, FailureType__c, FailureSince__c, EstimationType__c,
                        ERPOrderNo__c, Duration__c, DurationUnit__c, DistanceZone__c, Description__c,
                        DepartmentTurnover__c, DepartmentResponsible__c, DepartmentCurrent__c,
                        CustomerTimewindow__c, CustomerPriority__c, Country__c, Contract__c,
                        Channel__c, Brand__c, BrandName__c, AdditionalEmployee__c, CreatedDate,
                        (SELECT VipLevel__c, Type__c, TaxType__c, TaxCode2__c, TaxCode1__c, TaxCode0__c,
                                SystemModstamp, Street__c, SpecialCustomer__c, SearchCode__c, Salutation__c,
                                RiskClass__c, PostalCode__c, PoBox__c, PoBoxPostCode__c, PoBoxCity__c,
                                Phone__c, Phone2__c, PersonTitle__c, Order__c, OrderRole__c, Name3__c,
                                Name2__c, Name1__c, Name, MobilePhone__c, LockType__c, LocaleSidKey__c,
                                LastModifiedDate, LastModifiedById, IsDeleted, Id, ID2__c, HouseNo__c,
                                HomePhone__c, GeoY__c, GeoX__c, GeoApprox__c, Floor__c, FlatNo__c, Fax__c,
                                Extension__c, Email__c, District__c, DistanceZone__c, DiscountGroup__c,
                                Description__c, CurrencyIsoCode, CreatedDate, CreatedById, County__c, Country__c,
                                CountryState__c, Contact__c, City__c, Address__c, AddressFull__c, Account__c,
                                AccountType__c, AccountNumber__c
                        FROM OrderRole__r),
                        (SELECT SystemModstamp, Qualification__c, QualificationInfo__c, Order__c, OrderItem__c,
                                Name, Level__c, LastModifiedDate, LastModifiedById, IsDeleted, Id, CurrencyIsoCode,
                                CreatedDate, CreatedById
                        FROM OrderQualification__r),
                        (SELECT SystemModstamp, ProductNameCalc__c, Order__c, Name, MaintenancePrice__c, LastModifiedDate,
                                LastModifiedById, IsDeleted, InstalledBase__c, Id, ID2__c, ErrorText__c, ErrorSymptom3__c,
                                ErrorSymptom2__c, ErrorSymptom1__c, ErrorCondition2__c, ErrorCondition1__c, Employee_REQ__c,
                                Employee_NCW__c, Employee_CW__c, Duration__c, DurationUnit__c, Description__c, CurrencyIsoCode,
                                CreatedDate, CreatedById, CompletionStatus__c, Completed__c, Assignment__c, Address__c
                        FROM OrderItem__r)
                FROM SCOrder__c
                WHERE Id = :oid];
        
        
        /*
        * Orders that are older than four weeks may not be cloned.
        * Edit: Only Orders with an picklist value of 5701 (Service) or 5707 (Maintenance) in Type__c may be cloned.
        */
        Datetime today = Datetime.now();
        Datetime day = order.CreatedDate.addDays( 28 );
        
        if ( (order.Type__c != '5701') && (order.Type__c != '5707') )
        {
            return ('cloneIsNotAllowed_type');
        }
        if ( (today > day))
        {
            return ('cloneIsNotAllowed_date');
        }
        

        /*
        * Clone the Order
        *
        */
        SCOrder__c clonedOrder = order.clone(false);
        if(order.FailureType__c == null)
        {
            clonedOrder.FailureType__c = null;
        }
        
        /*
        * Set InvoicingStatus to Open
        */
        clonedOrder.InvoicingStatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_OPEN;
        sr = Database.insert( clonedOrder );
        clonedOrderId = sr.getId();
        System.debug('###clonedOrder Id: ' + clonedOrderId);
        
        /*
        * Set DEFAULT-Values
        */
        clonedOrder.Description__c = SCOrder__c.Description__c.getDescribe().getDefaultValueFormula();

        clonedOrder.Channel__c = NULL;
        for ( Schema.PicklistEntry i: SCOrder__c.Channel__c.getDescribe().getPicklistValues() ) {
          if ( (i.isDefaultValue()) && (clonedOrder.Channel__c==NULL) ) {
            clonedOrder.Channel__c = i.getValue();
          }
        }
        
        clonedOrder.FailureType__c = NULL;
        for ( Schema.PicklistEntry i: SCOrder__c.FailureType__c.getDescribe().getPicklistValues() ) {
          if ( (i.isDefaultValue()) && (clonedOrder.FailureType__c==NULL) ) {
            clonedOrder.FailureType__c = i.getValue();
          }
        }
        
        clonedOrder.IdOLD__c = SCOrder__c.IdOLD__c.getDescribe().getDefaultValueFormula();

        // set the info field for order history, so we can see from which order the copy was made        
        clonedOrder.Info__c = System.Label.SC_msg_CloneOrderMessage + ' ' + order.name; 
        upsert clonedOrder;
        

        /*
        * Clone all OrderRole
        *
        */
        List<SCOrderRole__c> allRoles = new List<SCOrderRole__c>();
        for (SCOrderRole__c role : order.OrderRole__r)
        {
            SCOrderRole__c clonedOrderRole = role.clone(false);
            clonedOrderRole.Order__c = clonedOrderId;
            
            allRoles.add(clonedOrderRole);
        }
        insert allRoles;

        /*
        * Clone all OrderItems
        *
        */
        Map<Id, Id> mapIds = new Map<Id, Id>();
        for ( SCOrderItem__c item : order.OrderItem__r )
        {
            Id oldId = item.Id;
            SCOrderItem__c clonedOrderItem = item.clone(false);
            clonedOrderItem.Order__c = clonedOrderId;
            
            clonedOrderItem.Completed__c = null;
            clonedOrderItem.CompletionStatus__c = SCfwConstants.DOMVAL_COMPLETIONSTATUS_DEFAULT;
       
            clonedOrderItem.ErrorText__c = SCOrderItem__c.ErrorText__c.getDescribe().getDefaultValueFormula();

            insert clonedOrderItem;
            mapIds.put(item.Id, clonedOrderItem.Id);
        }

        /*
        * Clone all OrderQualification
        *
        */
        List<SCOrderQualification__c> allQualifications = new List<SCOrderQualification__c>();
        for (SCOrderQualification__c qualification : order.OrderQualification__r)
        {
            SCOrderQualification__c clonedOrderQualification = qualification.clone(false);
            clonedOrderQualification.Order__c = clonedOrderId;
            clonedOrderQualification.OrderItem__c = mapIds.get(clonedOrderQualification.OrderItem__c);

            allQualifications.add(clonedOrderQualification);
        }
        insert allQualifications;
        
        return clonedOrderId;
    }
    
}