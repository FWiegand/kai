/*
 * @(#)SCbtcWorkshopOrderCreate.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The base class for the workshop order creation at the ERP backend.
 *
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * 
 *
 * Example: 
 * ord.ERPAutoOrderCreate__c = 'C0-Active'; // deactivate trigger for callout
 * update ord;
 * List<String> orderids = new List<String>();
 * orderids.add(ord.id);   // 'a0rM0000000GiHa'
 * String email = 'gbirkenheuer@gms-online.de';
 *
 * SCbtcWorkshopOrderCreate obj = new SCbtcWorkshopOrderCreate(orderids, email);
 * ID batchprocessid = Database.executeBatch(obj, 5);
 */
global class SCbtcWorkshopOrderCreate implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts//, Database.Dmloptions 
{
    global final List<String> orderids;
    global final String email;
    global String result;
    global Integer ordercnt;
    global final String currentSiteUrl;
          
    global SCbtcWorkshopOrderCreate(List<String> orderids, String email, String currentSiteUrl)
    {
        this.orderids = orderids;
        this.email = email; //'narmbruster@gms-online.de';
        this.ordercnt = 0;
        this.currentSiteUrl = currentSiteUrl;
    }            
    
    global SCbtcWorkshopOrderCreate(List<String> orderids, String email)
    {
        this.orderids = orderids;
        this.email = email; //'narmbruster@gms-online.de';
        this.ordercnt = 0;
        this.currentSiteUrl = '';
    } 
                
    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([select id, ERPAutoOrderCreate__c from SCOrder__c where id in :orderids]);
    }
    
                
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        List<SCOrder__c> orders = (List<SCOrder__c>)scope;
                          
        for(SCOrder__c ord : orders)
        {
            //C0-Active            //### must be set in order as ERPAutoOrderCreate__c before calling batch job ###
            //C1-AddExtOp                                               
            
            System.debug('##### SCbtcWorkshopOrderCreate(): Callout for order:' + ord.ID);
            
            //callout Create synchron and not testing  
            CCWCOrderCreate.callout(ord.ID, false, false);      
                                               
            ordercnt++;
        } 
    }
                
    global void finish(Database.BatchableContext BC)
    {
        if (email.length() > 5)
        {   // a valid e mail address. Else normally a test case without permission to send an email
            
            try
            {
                List<SCOrder__c> newOrders = new List<SCorder__c>();
                if(orderids != null && !orderids.isEmpty())
                {
                    newOrders = [Select Id, Name From SCOrder__c Where Id IN : orderids];
                }
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                String emailBody = ''; 
                String subj = 'Clockport Automatische Auftragserstellung: ';
                
                emailBody  = '<b>Der Batch-Prozess ist abgeschlossen.</b><br><br>';
                
                String plainBody = 'Der Batch-Prozess ist abgeschlossen. \n ';
                
                if(ordercnt == 1)
                {
                    emailBody += ordercnt + ' Werkstattauftrag wurde erstellt und extern vergeben. <br><br>';
                    subj += '1 Auftrag erstellt.';
                    plainBody += '1 Auftrag erstellt. \n ';
                }
                if(ordercnt > 1)
                {
                    emailBody += ordercnt + ' Werkstattaufträge wurden erstellt und extern vergeben. <br><br>';
                    
                    subj += ordercnt + ' Aufträge erstellt.';
                    plainBody += ordercnt + ' Aufträge erstellt. \n ';
                }
                Integer counter = 1;
                for(SCOrder__c o : newOrders)
                {
                    emailBody += 'Auftrag ' + counter + ': ';
                    emailBody += '<a href="' + currentSiteUrl + '/' + o.id + '" target="_blank">' + o.name + '</a><br>';
                    plainBody += 'Auftrag ' + counter + ': ' + o.name + ' \n ';
                    
                    counter++;
                }
                
                mail.setToAddresses(new String[] {email});
                mail.setReplyTo('info@gms-online.de');
                mail.setSenderDisplayName('Clockport Wizard');
                mail.setSubject(subj);
                mail.setHtmlBody(emailBody);                
                mail.setPlainTextBody(plainBody);
                                      
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            catch (Exception e)
            {
                System.debug('##### SCbtcWorkshopOrderCreate(): Batch completed but sending an Email to address [' + email + '] failed: ' + e.getMessage());
            }
        }
        System.debug('##### SCbtcWorkshopOrderCreate(): Batch Process has completed. Order count: ' + ordercnt + ' Email to: ' + email);
    }                

}