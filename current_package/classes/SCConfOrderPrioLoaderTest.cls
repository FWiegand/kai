/*
 * @(#)SCConfOrderPrioLoaderTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Used to initialize the country specific matrix for the order priorisation.
 * The matrix is evaluated during the order creation to determine the 
 * earliest necesssary start date for scheduling engineers depending on the 
 * current context (customer, situation, failure type). 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private with sharing class SCConfOrderPrioLoaderTest
{
   /*
    * A simple test method to get the test coverage.
    */
    public static testMethod void testConfig() 
    {
        String country = 'testonly';
    
        Test.StartTest();
        
        SCConfOrderPrioLoader.initialize(country);    
        
        Test.StopTest();

        // we just test here if there are any values created at all - the content does not care
        Integer count = [select count() from SCConfOrderPrio__c where country__c = :country];
        System.AssertEquals(true, count > 0);
   }
    
}