/*
 * @(#)SCboAppointmentTest.cls SCCloud    dh 21.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboAppointmentTest 
{  
    private static SCboAppointment boAppointment;



    /**
     * SCboAppointmentTest under positiv test
     * Methods for update (before update)
     */       

    static testMethod void testSCboAppointmentUpdateStatus()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createAppointments(true);
        
        System.Debug('App Size' + SCHelperTestClass.appointments.size());             
        boAppointment = new SCboAppointment();
        boAppointment.AfterUpdate(SCHelperTestClass.appointments, SCHelperTestClass.appointments);
    }

    /**
     * SCboAppointmentTest under positiv test
     * Methods for update (before update)
     */       
    static testMethod void testSCboAppointmentUpdateStatusChangeEmployee()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createAppointments(true);

        List<SCAppointment__c> appointmentsChangeEmpl  = new List<SCAppointment__c> ();
        for(Integer i = SCHelperTestClass.appointments.size()-1 ; i >= 0; i--)
        {
            appointmentsChangeEmpl.add(SCHelperTestClass.appointments[i]);
        }

        System.Debug('apps size' +  SCHelperTestClass.appointments.size());
        System.Debug('appsChange size' +  appointmentsChangeEmpl.size());

        boAppointment = new SCboAppointment();
        boAppointment.AfterUpdate(SCHelperTestClass.appointments, appointmentsChangeEmpl);
    }
    /**
     * SCboAppointmentTest under positiv test
     * Methods for insert and update (before and after)
     */       
    static testMethod void testSCboAppointmentInsert()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createAppointments(true);
        boAppointment = new SCboAppointment();
        boAppointment.AfterInsert(SCHelperTestClass.appointments);
    }

    /**
     * SCboAppointmentTest under positiv test
     * Methods for update (before update)
     */       
    static testMethod void testSCboAppointmentBeforeUpdate()
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        boAppointment = new SCboAppointment();
                
        boAppointment.BeforeUpdate(SCHelperTestClass.appointments, SCHelperTestClass.appointments);
        boAppointment.BeforeUpdate(SCHelperTestClass.appointmentsSingle1, SCHelperTestClass.appointmentsSingle2);
    }

    /**
     * SCboAppointmentTest under positiv test
     * Methods for delete (before and after delete)
     */              
    static testMethod void testSCboAppointmentDelete()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createAppointments(true);
        boAppointment = new SCboAppointment();
        boAppointment.BeforeDelete(SCHelperTestClass.appointments);
        boAppointment.AfterDelete(SCHelperTestClass.appointments);
    }
    static testMethod void testSCboAppointmentDeleteTrigger()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createOneAppointment(true);
        
        delete SCHelperTestClass.appointments[0];
    }

    static testMethod void testReadOpenDays()
    {
        SCHelperTestClass.createOrderTestSet3(true);
        
        boAppointment = new SCboAppointment();
        for (SCResource__c res :SCHelperTestClass.resources)
        {        
            datetime now = datetime.now();
            boAppointment.readAllAppointments(res.Id, now, now);
            boAppointment.readAllEmplAppointments(res.Id, now, now);
        }   
    }
    
    static testMethod void testroundMinutes()
    {
        datetime now = datetime.now();
        SCboAppointment.roundMinutes(now, 15);
    }
    
    static testMethod void testReadByOrder()
    {
    /* CCE web service issue
        SCHelperTestClass.createOrderTestSet3(true);

        List<SCAppointment__c> appList;
        
        Test.startTest();
        
        appList = SCboAppointment.readByOrder(SCHelperTestClass.order.Id, true);
        System.assertEquals(1, appList.size());
        appList = SCboAppointment.readByOrder(SCHelperTestClass.order.Id, false);
        System.assertEquals(1, appList.size());
        
        SCboOrder.cancelAssignments(SCHelperTestClass.order.Id);
        appList = SCboAppointment.readByOrder(SCHelperTestClass.order.Id, true);
        System.assertEquals(0, appList.size());
        appList = SCboAppointment.readByOrder(SCHelperTestClass.order.Id, false);
        System.assertEquals(0, appList.size());

        Test.stopTest();
        */
    }
}