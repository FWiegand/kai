/*
 * @(#)AseRights.cls SCCloud    hs 15.06.2011
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
public class AseRights 
{
    /**
     * Definition of user rights (set in custom setting SCPermissions)
     */
    public static final Integer OPEN_RIGHTS = 1;
    public static final Integer WRITE_RIGHTS = 2;
    public static final Integer ADMIN_FILTER_RIGHTS = 4;
    public static final Integer MONITORING_RIGHTS = 8;

    /**
     * Returns ASE rights
     * @author HS <hschroeder@gms-online.de>
     */
    public static String getAccess()
    {
        Integer access = 0;
        SCPermissions__c permissions = SCPermissions__c.getInstance();
        
        if (permissions.asePlanningboardRO__c)
        {
            access = access | OPEN_RIGHTS;
        }

        if (permissions.asePlanningboardRW__c)
        {
            access = access | WRITE_RIGHTS;
        }

        if (permissions.aseManageFilters__c)
        {
            access = access | ADMIN_FILTER_RIGHTS;
        }
        
        if (permissions.aseMonitoring__c)
        {
            access = access | MONITORING_RIGHTS;
        }
                
        system.debug('GetRights: ' + access);
        return '' + access;
    }
}