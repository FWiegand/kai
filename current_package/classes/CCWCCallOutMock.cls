/*
 * @(#)CCWCCallOutMock.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This class serves only for getting the controlling over the lines of the classes generated from WSDL files
 */
@isTest 
global without sharing class CCWCCallOutMock implements WebServiceMock 
{
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
  {
		System.debug('CCWCCallOutMock has been called.');
        // Create response element from the autogenerated class.
        // Populate response element.
        // Add response element to the response parameter, as follows:
        SFDCResponse_element responseElement = new SFDCResponse_element();
        response.put('response_x', responseElement); 
   }
   public class SFDCResponse_element 
   {
        private String[] apex_schema_type_info = new String[]{'http://pi.cceag.de/SFDC','false','false'};
        private String[] field_order_type_info = new String[]{};
   }
}