/**
 * SCPayment.cls    jp 21.02.2011
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */


/**
 * Class is used to do payments with the credit cards
 */
public class SCPayment
{
    public static String call(SCPaymentData pd)
    {
        System.debug('SCPaymentData:' + pd.toString2());
        String mode = pd.mode;    // It is set in SCPaymentData to test only by test functions.
        if( mode != null 
            && (mode.equals(SCPaymentData.MODE_SALESFORCE_TEST)
                || mode.equals(SCPaymentData.MODE_DEVELOPER_TEST)))
        {
            // Software Engineer Test from System Log
            // No normal test possible because there are outgoing calls to the external Webservices.
            // This calls can not be called from the test methods.
            // The input data pd is overwritten with a test case.
            
            String merchantCode = 'VGUKLTD';
            String endpoint = 'https://secure-test.wp3.rbsworldpay.com/jsp/merchant/xml/paymentService.jsp';
            String ipAddress = '195.144.130.85';
            String loginId = '6df34563d3';
            
            String cardType= 'SWITCH';//'SOLO_GB';//'ECMC';//'VISA';//'SWITCH';        
            Double amountValue= 131.60;
            String currencyCode= 'GBP';           
            String orderCode= '000214893';   
            Integer tryNum= 0;   
//            String cardNumber= '4111111111111111';         // VISA AUTHORISED   
//            String cardNumber = '3434 343434 34343';        // AMEX AUTHORISED
            String brand = 'Vaillant';
            String cardNumber = '6333333333333333336';        // SWITCH, AUTHORISED, RETURNS MAESTRO
//            String cardNumber = '5555 5555 5555 4444'; // ECMC AUTHORISED
//            String cardNumber = '6767676767676767671'; // SOLO_GB, Error Code: 2, Description: Parse error, invalid xml
            String expiryDateMonth= '10';        
            String expiryDateYear= '2011';         
            String cardHolderName= 'CAPTURED'; //'REFFERED';//'REFUSED';//'ERROR';//'REFUSED';//'AUTHORISED';         
            String cvc= '123';                    
            String firstName= 'John';              
            String lastName= 'Smith';               
//            String street= '21 Acacia Avenue';  
            String street= 'ACACIA CROFT';               
            String postalCode= 'DE56 1DS';             
            String city= 'Belper';                   
            String countryCode= 'GB';            
            String telephoneNumber= '';        
            String sessionShopperIPAddress= '195.144.130.85';
            String sessionId= '6df34563d3';              
            String startDateMonth= '10';         
            String startDateYear= '2010';
            String shopperEmailAddress = 'jpietrzyk@gms-online.de';     
            pd = new SCPaymentData(cardType,        
                                           amountValue,
                                           currencyCode,           
                                           orderCode,   
                                           tryNum,
                                           brand,           
                                           cardNumber,             
                                           expiryDateMonth,        
                                           expiryDateYear,         
                                           cardHolderName,         
                                           cvc,                    
                                           firstName,              
                                           lastName,               
                                           street,                 
                                           postalCode,             
                                           city,                   
                                           countryCode,            
                                           telephoneNumber,        
                                           startDateMonth,         
                                           startDateYear,
                                           shopperEmailAddress);
            
            //<- End of Test
        }
        SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
        String paymentType = applicationSettings.PAYMENTSERVICE_TYPE__c;
        String ret = '';
        
        System.debug('Return of call: ' + ret);
        pd.afterWebCallStart = Datetime.now();
        return ret;
    }
}//SCPayment