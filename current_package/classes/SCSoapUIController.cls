/*
 * Simple tool to test SOAP based messages from the SF side
 * (c) 2012 GMS Developemnt 
 */
public class SCSoapUIController 
{

    // tab 1 - soap ui
    public String endpoint {get;set;}
    public String request {get;set;}
    public String response  {get;set;}
    
    public String authuser {get;set;}
    public String authpwd {get;set;}
    
    public String error {get;set;}
    
    public String text {get;set;}

    // tab 2 - order test
    
    public String ordno {get;set;}
    public SCOrder__c ord {get;set;}


    public PageReference OnReadOrder()
    {
        ord = [select id, name, type__c, status__c, invoicingstatus__c from SCOrder__c where name = :ordno];
        return null;
    }

    public PageReference OnOrderCreate() 
    {
        return null;
    }

    public PageReference OnOrderUpdateEquipment() 
    {
        return null;
    }

    public PageReference OnOrderMaterial() 
    {
        return null;
    }

    public PageReference OnOrderClose() 
    {
        return null;
    }


    // tab 1
    
    public SCSoapUIController()
    {
        endpoint = '';
        request = '';
        response = '';
        setBasicAuth();
        
        ordno = 'ORD-0000004841';
        
    } 

    public void setBasicAuth()
    {
//        authuser = 'IT_SF';
//        authpwd  = 'SaFoPI#4';
        
        CCWSUtil u = new CCWSUtil();
        authuser = u.ccSettings.SAPDispBasicAuthUsername__c;
        authpwd  = u.ccSettings.SAPDispBasicAuthPassword__c;
   } 

    public void OnPreparePing()
    {
//        endpoint = 'https://wd-dev0.cceag.de:8143/sap/public/ping';
        CCWSUtil u = new CCWSUtil();
        endpoint = u.ccSettings.SAPDispServer__c + '/sap/public/ping';
        
        request = '';
        response = '';
        setBasicAuth();
    }


    public void OnPrepareCreateOrder()
    {
        // endpoint = 'http://sap-pi-dev0.dcc.cc-eag.de:8042/sap/xi/engine?type=entry&version=3.0&Sender.Service=Srv_CCEAGCAMService&Interface=http%3A%2F%2Fpi.cceag.de%2FSFDC%5ECustomerServiceOrderCreate_Out';
        // endpoint = 'https://wd-dev0.cceag.de:8143/sap/xi/engine?type=entry&version=3.0&Sender.Service=SFDC&Interface=http%3A%2F%2Fpi.cceag.de%2FSFDC%5ECustomerServiceOrderCreate_Out';
        // endpoint = 'https://wd-dev0.cceag.de:8143/XISOAPAdapter/MessageServlet?senderService=SFDC&Interface=http%3A%2F%2Fpi.cceag.de%2FSFDC%5ECustomerServiceOrderCreate_Out';
        
        // endpoint    = 'https://wd-dev0.cceag.de:8143/XISOAPAdapter/MessageServlet?channel=:SFDC:SOAP_SND_CreateCustomerServiceOrder';
        
        CCWSUtil u = new CCWSUtil();
        endpoint = u.getEndpoint('OrderCreate');
        
        request = getSoapEnvelopeCreateOrder('xx');
        response = '';
        setBasicAuth();
    }
    
    public PageReference OnMultiLine() 
    {
        OnPrepareCreateOrder();
        
        /*
    
        endpoint    = 'http://webaccess.gms-online.de:12000/mock';
        
        request = getMultiLineText(text);
        response = '';
        setBasicAuth();
        */
    
        return null;
    }
    

    public void OnCall() 
    {   
        response = '';
        error = '';
        
        HttpRequest  req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
 
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        

//Content-Type: text/xml; charset=utf-8

//text/xml

        if(authuser != null && authpwd != null)
        {
            Blob headerValue = Blob.valueOf(authuser + ':' + authpwd);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type', 'text/xml');
            
/*

        // prepare basic authentication
        String username = 'IT_SF';
        String password = 'SaFoPI#1';
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        ws.inputHttpHeaders_x = new Map<String, String>();
        ws.inputHttpHeaders_x.put('Authorization', authorizationHeader);

*/            
        }
        req.setBody(request);
//        req.setCompressed(true); // otherwise we hit a limit of 32000
 
        try
        {
            res = http.send(req);
            response = res.getBody();
        }
        catch(Exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, '' + ex);
        
            ApexPages.addMessage(myMsg );
        }
     }

    public String getSoapEnvelopeCreateOrder(String oid)
    {
         CCWSUtil u = new CCWSUtil();
    
        String env = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sfdc="http://pi.cceag.de/SFDC">\n'; 
        env += ' <soapenv:Header/>\n';
        env += ' <soapenv:Body>\n';
        env += '  <sfdc:CustomerServiceOrderCreate_Message>\n';        
        env += '   <MessageHeader>\n';        
        env += '    <MessageID>' + u.getMessageID() + '</MessageID>\n';        
        env += '    <ReferenceID>' + oid + '</ReferenceID>\n';        
        env += '    <CreationDateTime>' + u.getFormatedCreationDateTime() + '</CreationDateTime>\n';        
        env += '    <TestDataIndicator>false</TestDataIndicator>\n';        
        env += '    <SenderBusinessSystemID>' + u.getSenderBusinessSystemID() + '</SenderBusinessSystemID>\n';        
        env += '    <RecipientBusinessSystemID>' + u.getRecipientBusinessSystemID() + '</RecipientBusinessSystemID>\n';        
        env += '   </MessageHeader>\n';        
        env += '   <CustomerServiceOrder>\n';        
        env += '    <OrderType>' + 'ZC02' + '</OrderType>\n';        
        env += '    <MaintenancePlanningPlant>' + '0359' + '</MaintenancePlanningPlant>\n';        
        env += '    <ShortText>' + EncodingUtil.urlEncode('Dies ist ein statischer test', 'UTF-8') + '</ShortText>\n';        
        env += '    <MainWorkcenter>' + 'CDEDESPT' + '</MainWorkcenter>\n';        
        env += '    <WorkCenterPlant>' + '0359' + '</WorkCenterPlant>\n';        
        env += '    <BasicStartDate>' + '2012-10-30' + '</BasicStartDate>\n';        
        env += '    <EquipmentNumber>' + '10004907' + '</EquipmentNumber>\n';        
        //env += '    <Assembly>' + '' + '</Assembly>\n';        
        env += '    <SalesData>\n';        
        env += '     <SalesOrg>' + '0120' + '</SalesOrg>\n';        
        env += '     <DistributionChannel>' + 'Z1' + '</DistributionChannel>\n';        
        env += '     <SalesDivision>' + 'Z0' + '</SalesDivision>\n';        
        env += '     <SalesOffice>' + '0367' + '</SalesOffice>\n';        
        env += '    </SalesData>\n';        
        env += '    <PriceList>' + 'DE301' + '</PriceList>\n';        
        env += '    <ReferenceNumber>' + 'ORD-StaticTest001' + '</ReferenceNumber>\n';        
        env += '    <Partners>\n';        
        env += '      <PartnerFunction>' + 'AG' + '</PartnerFunction>\n';        
        env += '      <PartnerNumber>' + '500000299' + '</PartnerNumber>\n';        
        env += '    </Partners>\n';        
        env += '    <Partners>\n';        
        env += '      <PartnerFunction>' + 'WE' + '</PartnerFunction>\n';        
        env += '      <PartnerNumber>' + '500000299' + '</PartnerNumber>\n';        
        env += '    </Partners>\n';        
        
        env += '    <NotificationData>\n';        
        env += '     <LongText>' + text + '</LongText>\n';        
//        env += '     <CodeGroup>' + '##' + '</CodeGroup>\n';        
//        env += '     <CodeID>' + '##' + '</CodeID>\n';        
        env += '    </NotificationData>\n';        
        
//        env += EncodingUtil.urlEncode('test', 'UTF-8');
        
        env += '   </CustomerServiceOrder>\n';        
        env += '  </sfdc:CustomerServiceOrderCreate_Message>\n';   
        env += ' </soapenv:Body>\n';
        env += '</soapenv:Envelope>';
 
        return env;  
    }


        //'name='+c+'&city='+EncodingUtil.urlEncode(city, 'UTF-8'));

    public String getMultiLineText(String ptext)
    {
        CCWSUtil u = new CCWSUtil();
    
        String env = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\n'; 
        env += ' <soapenv:Header/>\n';
        env += ' <soapenv:Body>\n';
        env += '   <OnProcess>\n';
        env += '    <Text>' + ptext + '</Text>\n';        
        env += '   </OnProcess>\n';
        env += ' </soapenv:Body>\n';
        env += '</soapenv:Envelope>';
 
        return env;  
    }
}