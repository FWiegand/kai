public with sharing class SCNotificationController  extends SCfwOrderControllerBase
{
    public String aid { get; set; }
    public SCboOrder parboOrder { get; set;}
    public SCboOrderItem parboOrderItem { get; set; }
    public List<SCNotification__c> notification;
    public List<NotificationProdObj> notificationProd;
    public Boolean actionComplete { get; set; }
    public SCfwComponentControllerBase componentController { get; set;}
    public Id oid { get; set;}
    public Boolean showPyment { get; set; }
    public Boolean hasNotifications { get; set; }
    public Boolean hasNotificationsProd { get; set; }
    public String notificationAnswer { get; set; }
    public String currentNotificationId { get; set; }
    public String pass { get; set; }
//    public SCApplicationSettings__c aSettings = SCApplicationSettings__c.getInstance(); 
    public String overwritePass;// = aSettingsSCApplicationSettings__c.getInstance().NOTIFICATION_OVERWRITE_PASS__c;
    public String overwriteComplete { get; set; }
    public Boolean showCompleted { get; set; }
    public String callAvoidance { get; set; }
    public String fee { get; set; }
    public String nid { get; set; }
    public String currencyCode { get; set; }
    public String notificationToCancel { get; set; }
    
    public SCNotificationController()
    {

// ###TODO: GMSSU remove   
//        SCApplicationSettings__c aSettings = SCApplicationSettings__c.getInstance(); 
//        overwritePass = aSettings.NOTIFICATION_OVERWRITE_PASS__c == '1' ? '1' : '0';
//        aSettings = null;

        actionComplete = false;
        componentController = null;
        pass = null;
        showPyment = false;
        showCompleted = false;
        hasNotifications = true;
        hasNotificationsProd = true;
    }
    
    /**
     * Wrapper class for an order role for displaying in a list on a page.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public class NotificationProdObj
    {
        // the product notification itself
        public SCNotificationProduct__c notifProd { get; set;}
        // the product notification result
        public SCNotificationProductResult__c notifProdResult { get; set;}
        
        /**
         * Contructor
         *
         * @param role Order role to use
         */
        public NotificationProdObj(SCNotificationProduct__c notifProd, SCboOrder boOrder)
        {
            this.notifProd = notifProd;

            System.debug('#### NotificationProdObj(): mapProductResults -> ' + boOrder.mapProductResults);
            if (boOrder.mapProductResults.containsKey(notifProd.Id))
            {
                this.notifProdResult = boOrder.mapProductResults.get(notifProd.Id);
            }
            else
            {
                System.debug('#### NotificationProdObj(): new object -> ' + notifProd);
                this.notifProdResult = new SCNotificationProductResult__c(NotificationProduct__c = notifProd.Id);
                this.notifProdResult.Action__c = '';
                boOrder.productResults.add(notifProdResult);
                boOrder.mapProductResults.put(notifProd.Id, notifProdResult);
            }
        } // NotificationProdObj

        /**
         * Return the list of action for the product notification.
         */
        public List<SelectOption> getActionList()
        {
            List<SelectOption> actionList = new List<SelectOption>();
            actionList.add(new SelectOption('', System.Label.SC_flt_None));

            if (null != notifProd)
            {
                for (String action :notifProd.Action__c.split(';'))
                {
                    actionList.add(new SelectOption(action, SCBase.getPicklistText(SCNotificationProduct__c.Action__c, action)));
                }
            }
            return actionList;
        } // getActionList
    } // class NotificationProdObj

    /**
     * Reads all notifications for the current account
     *
     * @param     aid  current account id
     * @return    true, if notification for a product will be shown
     */
    public List<SCNotification__c> getNotification()
    {
        if(aid != null)
        {    
            if(showCompleted)
            {
                notification = new List<SCNotification__c>([ Select ValidTo__c, Status__c, toLabel(Question__c), ProductModel__c, 
                                                                    Order__c, Name, InternalOnly__c, InstalledBase__c, Id, IFID__c, ID2__c, 
                                                                    Funktion__c, Duration__c, Description__c, CreatedDate, CreatedBy.Name, CreatedById,
                                                                    Context__c, COND1__c, Answer__c, Amount__c, Active__c, Action__c, Account__c,
                                                                    Account__r.FirstName__c, Account__r.LastName__c, 
                                                                    RecordType.Description, RecordType.DeveloperName, RecordType.Name, Confirmed__c, ConfirmedBy__c, ClosedDate__c,
                                                                    CurrencyIsoCode
                                                             From SCNotification__c  
                                                             Where Account__c = :aid 
                                                             Order By CreatedDate Desc]);
            }
            else
            {
                notification = new List<SCNotification__c>([ Select ValidTo__c, Status__c, toLabel(Question__c), ProductModel__c, 
                                                                    Order__c, Name, InternalOnly__c, InstalledBase__c, Id, IFID__c, ID2__c, 
                                                                    Funktion__c, Duration__c, Description__c, CreatedDate, CreatedBy.Name, CreatedById,
                                                                    Context__c, COND1__c, Answer__c, Amount__c, Active__c, Action__c, Account__c, 
                                                                    Account__r.FirstName__c, Account__r.LastName__c,
                                                                    RecordType.Description, RecordType.DeveloperName, RecordType.Name, Confirmed__c, ConfirmedBy__c, ClosedDate__c,
                                                                    CurrencyIsoCode
                                                             From SCNotification__c  
                                                             Where Account__c = :aid 
                                                             AND Status__c = 'Active'
                                                             Order By CreatedDate Desc]);
            }
            
            showCompleted = false;
                                                         
            System.debug('#### notif: ' + notification);
            
            if(notification.size() > 0 && notification != null)
            {
                hasNotifications = true;
                return notification;
            }
            else 
            {
                hasNotifications = false;
                notification = new List<SCNotification__c>();
                return notification;
            }
        }
        
        return null;
    }
    
    /**
     * Getter for the indicator that shows if we are displaying notifications
     * for a product or for an account.
     *
     * @return    true, if notification for a product will be shown
     */
    public Boolean isNotificationProduct
    {
        get
        {
            if (null == isNotificationProduct)
            {
                isNotificationProduct = ((null == aid) && (null != parboOrder) && (null != parboOrderItem));
            }
            return isNotificationProduct;
        }
        private set;
    } // isNotificationProduct
    
    /**
     * Return a list of notification for a product.
     *
     * @return    the list of product notification objects
     */
    public List<NotificationProdObj> getNotificationProd()
    {
        Set<Id> readNotifs = new Set<Id>();
        notificationProd = new List<NotificationProdObj>();
        hasNotificationsProd = false;
        
        if ((null != parboOrder) && (null != parboOrderItem))
        {
            List<String> postCodes = new List<String>();
            Date today = Date.today();
            String custTimeWindow = null;
    
            List<String> codes = new List<String>();
            String postalCode = parboOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
            if (SCBase.isSet(postalCode))
            {
                codes.add(postalCode);
                for (Integer i=postalCode.length(); i>1; i--)
                {
                    codes.add(postalCode.substring(0, i-1));
                }
            }
            // System.debug('#### getNotificationProd(): loc postcode -> ' + postalCode);
            // System.debug('#### getNotificationProd(): loc codelist -> ' + codes);
    
            for (SCNotificationProduct__c notifi :[Select Id, Name, Description__c, Action__c, OrderCustomerTimewindow__c, RecordType.Name, PostCodes__c  
                                                     from SCNotificationProduct__c 
                                                    where ValidFrom__c <= :today 
                                                      and (ValidTo__c = null or ValidTo__c >= today) 
                                                      and (ProductModel__c = null or ProductModel__c = :parboOrderItem.orderItem.InstalledBase__r.ProductModel__c) 
                                                      and (ProductUnitClass__c = null or ProductUnitClass__c includes (:parboOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c)) 
                                                      and (ProductUnitType__c = null or ProductUnitType__c includes (:parboOrderItem.orderItem.InstalledBase__r.ProductUnitType__c)) 
                                                      and (ProductGroup__c = null or ProductGroup__c includes (:parboOrderItem.orderItem.InstalledBase__r.ProductGroup__c)) 
                                                      and (ErrorSymptom1__c = null or ErrorSymptom1__c includes (:parboOrderItem.orderItem.ErrorSymptom1__c)) 
                                                      and (ErrorSymptom2__c = null or ErrorSymptom2__c includes (:parboOrderItem.orderItem.ErrorSymptom2__c)) 
                                                      and (FailureType__c = null or FailureType__c includes (:parboOrder.order.FailureType__c)) 
                                                    order by Sort__c])
            {
                postCodes = (null == notifi.PostCodes__c) ? new List<String>() : notifi.PostCodes__c.split(',');
                // System.debug('#### getNotificationProd(): not codelist -> ' + postCodes);
                
                // if the installed base location has no postal code or
                // the product notification has no postal code information or
                // the postal code information of the product notification contains parts of the postal code from the location
                if ((codes.size() == 0) || (postCodes.size() == 0) || SCfwCollectionUtils.isContained(codes, postCodes))
                {
                    notificationProd.add(new NotificationProdObj(notifi, parboOrder));
                    readNotifs.add(notifi.Id);
                
                    if (SCBase.isSet(notifi.OrderCustomerTimewindow__c))
                    {
                        custTimeWindow = notifi.OrderCustomerTimewindow__c;
                    }
                } // if ((codes.size() == 0) || (postCodes.size() == 0) || SCfwCollectionUtils.isContained(codes, postCodes))
            } // for (SCNotificationProduct__c notifi :[Select ... from SCNotificationProduct__c
            System.debug('#### getNotificationProd(): notificationProducts -> ' + notificationProd);
            System.debug('#### getNotificationProd(): custTimeWindow -> ' + custTimeWindow);

            if (SCBase.isSet(custTimeWindow))
            {
                parboOrder.order.CustomerTimewindow__c = custTimeWindow;
            }
            
            // now check if there are notifications in the order, that are not read 
            if (parboOrder.productResults.size() > 0)
            {
                for (Integer i=parboOrder.productResults.size()-1; i>=0; i--)
                {
                    if (!readNotifs.contains(parboOrder.productResults[i].NotificationProduct__c))
                    {
                        parboOrder.mapProductResults.remove(parboOrder.productResults[i].NotificationProduct__c);
                        parboOrder.productResults.remove(i);
                    }
                } // for (Integer i=parboOrder.productResults.size()-1; i>=0; i--)
            } // if (parboOrder.productResults.size() > 0)
            
            hasNotificationsProd = (notificationProd.size() > 0);
        } // if ((null != parboOrder) && (null != parboOrderItem))
        
        return notificationProd;
    } // getNotificationProd
    
    /**
     * Releases the blocked screen so the user can process the order cration
     * without to need confirm the notifications
     *
     * @param     pass the password
     * @return    the page reference
     */
    public PageReference overwriteNotification()
    {      
        SCNotification__c notif = null;
        
        if(notificationToCancel != null && notificationToCancel.trim() != '')
        {
            notif = [ Select Id, Answer__c, Confirmed__c, ConfirmedBy__c, Status__c, ClosedDate__c
                      From SCNotification__c
                      Where Id = : notificationToCancel ];
        }
        
        if(pass != null && pass.trim() != '')
        {
            String[] passData = pass.split(':', 2);
            User userData = null;
            
            try
            {
                 userData = [ Select Id, Name, UserName, NotificationPass__c From User Where UserName = :passData[0] ];
            }
            catch (Exception e)
            {
                System.debug('#### Cant select user: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                
                overwriteComplete = 'n';
                return null;
            }
            
            if(userData != null && pass == (userData.UserName+':'+userData.NotificationPass__c))
            {
                overwriteComplete = 'y';
                System.debug('#### y'); 
                
                // Creating / updating a notification Log                   
                if(notif != null)
                {
                    SCNotificationLog__c nLog = new SCNotificationLog__c();
                    nLog.User__c              = UserInfo.getUserId();
                    nLog.Notification__c      = notif.Id;
                    nLog.ConfirmationDate__c  = Datetime.Now();
                    
                    nLog.Description__c = userData.Name + ' ' + Label.SC_app_NotificationSkipped + ' ' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                    
                    try
                    {
                        upsert nLog;
                    }
                    catch (DmlException e)
                    {
                        System.debug('#### NotificationLog upsert error: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                    }  
                }
            }
        }
        
        return null;
    }
    
    /**
     * Confirms the notification and saves the notification log
     *
     * @param     currentNotificationId the current notification id
     * @return    the page reference
     */
    public PageReference confirmNotification()
    {
        System.debug('#### currentNotificationId: ' + currentNotificationId);
        System.debug('#### notificationAnswer: ' + notificationAnswer);
        
        if(currentNotificationId != null && currentNotificationId.trim() != '')
        {
            SCNotification__c notif = [ Select Id, Answer__c, Confirmed__c, ConfirmedBy__c, Status__c, ClosedDate__c
                                        From SCNotification__c
                                        Where Id = : currentNotificationId ];
            
            if(notificationAnswer != null && notificationAnswer.trim() != '')
            {
                notif.Answer__c = notificationAnswer;
                notif.Confirmed__c = Datetime.now();
                notif.ConfirmedBy__c = UserInfo.getUserId();
                notif.ClosedDate__c = Datetime.now();
                
                if(notificationAnswer == 'Yes')
                {
                    String conf = [ Select RecordType.DeveloperName From SCNotification__c Where Id = : currentNotificationId ].RecordType.DeveloperName;
                    
                    if(conf.startsWith('CODE'))
                    {
                        notif.Status__c = 'Completed';
                    }
                }
                
                if(notificationAnswer == 'No')
                {
                    String conf = [ Select RecordType.DeveloperName From SCNotification__c Where Id = : currentNotificationId ].RecordType.DeveloperName;
                    
                    if(conf.startsWith('CODE'))
                    {
                        notif.Status__c = 'Active';
                    }
                }
                
                try 
                {
                    upsert notif;
                    actionComplete = true;
                    
                    // Creating / updating a notification Log                   
                    SCNotificationLog__c nLog = new SCNotificationLog__c();
                    nLog.User__c              = UserInfo.getUserId();
                    nLog.Notification__c      = notif.Id;
                    nLog.ConfirmationDate__c  = notif.Confirmed__c;
                    nLog.Answer__c            = notif.Answer__c;
                        
                    nLog.Description__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ' confirmed Notification with answer ' + notif.Answer__c;
                    
                    try
                    {
                        upsert nLog;
                    }
                    catch (DmlException e)
                    {
                        System.debug('#### NotificationLog upsert error: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                    }
                    
                } 
                catch (DmlException e)
                {
                    System.debug('#### Notification upsert error: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
                }
            }
        }
        return null;
    }
    
    /**
     * Shows the payment mask and sets variables
     *
     * @param     paymentNotif the current notification object
     * @return    the page reference
     */
    public PageReference doPayment()
    {
        showPyment = true;
        brandName = '-';
        
        // Reseting the values
        errMsg = null;
        isPaymentOk = null;
        cardType = null;
        cardNumber = null;
        securityCode = null;
        startDate = null;
        expiryDate = null;
        
        return null;
    }
    
    /**
     * Closes the payment mask at the page
     *
     * @return    the page reference
     */
    public PageReference closePaymentPanel()
    {
        showPyment = false;
        return null;
    }
    
    /**
     * Resets the flag,that indocates, that the payment was ok.
     * Needed for calling the payment dialog more than once.
     *
     * @return    the page reference
     */
    public PageReference onResetPaymentFlag()
    {
        isPaymentOk = false;
        return null;
    }
    
    /**
     * Starts the payment service and do all necessary steps after.
     *
     * @return    the page reference
     */
    public PageReference doPaymentService()
    {
        doPaymentService(boOrder);

        if (isPaymentOk)
        {
            boOrder = null;
        }
        return null;
    }
    
    /**
     * Starts the payment service in notification context
     *
     * @param     nid  payment notification id
     * @return    the page reference
     */
    public PageReference doPaymentServiceNotification()
    {
        SCNotification__c n = new SCNotification__c();
        n = [ Select ValidTo__c, Status__c, toLabel(Question__c), ProductModel__c, 
                     Order__c, Name, InternalOnly__c, InstalledBase__c, Id, IFID__c, ID2__c, 
                     Funktion__c, Duration__c, Description__c, CreatedDate, CreatedBy.Name, CreatedById,
                     Context__c, COND1__c, Answer__c, Amount__c, Active__c, Action__c, Account__c, 
                     Account__r.FirstName__c, Account__r.LastName__c, Account__r.Id, Account__r.BillingCountry__c,
                     Account__r.BillingPostalCode, Account__r.Phone, Account__r.Email__c, Account__r.AccountNumber, Account__r.BillingCity, 
                     RecordType.Description, RecordType.DeveloperName, RecordType.Name, Confirmed__c, ConfirmedBy__c, ClosedDate__c,
                     CurrencyIsoCode
              From  SCNotification__c  
              Where Id = :nid ];
        
        doPaymentService(n,null);
        
        return null;
    }
    
    public SCboOrder boOrder
    {
        get
        {
            if (null == boOrder)
            {   System.debug('#### oid: ' + oid);
                if (SCBase.isSet(oid))
                {
                    boOrder = new SCboOrder();
                    boOrder.readbyId(oid);
                }
                else if (null != parboOrder)
                {
                    boOrder = parboOrder;
                }
                
                if (!SCBase.isSet(brandName) && boOrder.boOrderItems.size() > 0)
                {
                    brandName = boOrder.boOrderItems[0].orderItem.InstalledBase__r.Brand__r.Name;
                } // if (!SCBase.isSet(brandName) && order.boOrderItems.size() > 0)

                SCOrderRole__c roleIR = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_RE);
                cardHolder = '';
                if (SCBase.isSet(roleIR.Account__r.FirstName__c))
                {
                    cardHolder = roleIR.Account__r.FirstName__c + ' ';
                } // if (SCBase.isSet(roleIR.Account__r.FirstName__c))
                cardHolder += roleIR.Account__r.LastName__c;

                onCalculateSums();
            }
            return boOrder;
        }
        set;
    }
    
    /**
     * Calculate the tax sum of shown order lines. Used after changing 
     * the order line type in the page.
     */
    public PageReference onCalculateSums()
    {
        calculateSums(boOrder, getBoOrderLines(boOrder));
        return null;
    } // onCalculateSum
    
    /**
     * Gets the payment fee. 
     * The sum of all prices and taxes of all order lines with a selected flag.
     *
     * @return    the page reference
     */
    public String getPaymentFee()
    {
        return getPaymentFee(boOrder);
    } // getPaymentFee
    
    /**
     * Set the flag for the call avoidance and an order text.
     *
     * @return    the page reference
     */
    public void setCallAvoidance()
    {
        if (null != parboOrder)
        {
            parboOrder.order.CallAvoidance__c = ('1' == callAvoidance);
            parboOrder.order.Description__c = ('1' == callAvoidance) ? System.Label.SC_app_CallAvoidance : System.Label.SC_app_CallAvoidanceFailed;
        }
    } // setCallAvoidance
}