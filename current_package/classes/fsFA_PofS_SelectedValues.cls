/**
	* fsFA_PofS_SelectedValues - <description>
	* @author: Bernd Werner
	* @version: 1.0
*/

public with sharing class fsFA_PofS_SelectedValues{
	
		public List< SelectOption > GL_ChosenValues{ 
		Get{
			if( GL_ChosenValues == null ){
				GL_ChosenValues = new List< SelectOption >();
			}
			return GL_ChosenValues;
		}
		Set;
	}
		
}