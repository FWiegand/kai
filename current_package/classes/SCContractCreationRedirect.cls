/*
 * @(#)SCContractCreationRedirect.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Redirects the call to the contract creation wizard
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCContractCreationRedirect
{
    private Id id;
    
    public SCContractCreationRedirect(ApexPages.StandardController controller) 
    {
        this.id = controller.getId();
    } // SCContractCreationRedirect

    public PageReference redirectToContractWizard()
    {
        PageReference page = new PageReference('/apex/ScContractCreationStep1?aid=' + this.id + '&ref=' + this.id);
        
        return page;
    } // redirectToContractWizard
    

    public static testMethod void testBasic() 
    {
        Account newAccount = new Account (name='Test Dummy Account');
        insert newAccount;
        
        ApexPages.StandardController sc = new ApexPages.standardController(newAccount);
        
        SCContractCreationRedirect obj = new SCContractCreationRedirect(sc);
        PageReference page = obj.redirectToContractWizard();
    }    
    
} // SCContractCreationRedirect