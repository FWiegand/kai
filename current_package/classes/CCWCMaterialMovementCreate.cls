/*
 * @(#)CCWCMaterialMovementCreate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order close information into SAP
 * 
 * The entry method is: 
 *      callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCMaterialMovementCreate extends SCInterfaceExportBase 
{

    public static String PROD_NAMES = 'PROD_NAMES';
    public ID orderID = null;

    public CCWCMaterialMovementCreate()
    {
    }
    public CCWCMaterialMovementCreate(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    // If there is a four parameter constructor, Salesforce expects three parameter constructor
    public CCWCMaterialMovementCreate(String interfaceName, String interfaceHandler, String refType)
    {
        super(interfaceName, interfaceHandler, refType, 'Dummy');
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * @param oid       object id (order if order related movements are to be transferred)
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     *
     * @return  messageID, important for the test class. Do not change 
     */
    public static String callout(String oid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_MATERIAL_MOVEMENT';
        String interfaceHandler = 'CCWCMaterialMovementCreate';
        String refType = null;
        CCWCMaterialMovementCreate oc = new CCWCMaterialMovementCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialMovementCreate');
    } // callout   

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param plantName   the plant name
     * @param stockName This is the stockName if the plant is set. 
     *                  If the plant name is null, ONLY then the stockName will be the stock id!  
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @return  messageID, important for the test class. Do not change 
     */
    public static String callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
    {
            ID oid = null;
            String interfaceName = 'SAP_MATERIAL_MOVEMENT';
            String interfaceHandler = 'CCWCMaterialMovementCreate';
            String refType = null;
            CCWCMaterialMovementCreate oc = new CCWCMaterialMovementCreate(interfaceName, interfaceHandler, refType);
            return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialMovementCreate',
                           plantName, stockName, mode);
    } // callout   

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @return  messageID, important for the test class. Do not change 
     */
    public static String callout(List<String> movementIDList, boolean async, boolean testMode)
    {
            ID oid = null;
            String interfaceName = 'SAP_MATERIAL_MOVEMENT';
            String interfaceHandler = 'CCWCMaterialMovementCreate';
            String refType = null;
            String plantName = null;
            String stockName = null;
            String mode = null;
            CCWCMaterialMovementCreate oc = new CCWCMaterialMovementCreate(interfaceName, interfaceHandler, refType);
            return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialMovementCreate',
                           plantName, stockName, mode, movementIDList);
    } // callout   



    /**
     * Reads an material rservation to send
     *
     * @param objectId the order id or null
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String objectID, Map<String, Object> retValue, Boolean testMode)
    {
        // 5204 consumption
        // 5215 Transfer (out)
        // 5229 Conversion (in)  ???
        
        // 5407    Book
        // 5408    Booked
        debug('plantName: ' + plantName + ', stockName: ' + stockName);
        List<SCMaterialMovement__c> mml = null;

        if(originIdOrNameList == null)
        {
            if(objectID != null && objectID != '')
            {
                // Read all order related consumptions (status = 5407 book)
                mml = getMMbyOrder(objectID);
                setReferenceID(objectID);
                setReferenceType('SCOrder__c');
                orderID = objectID;
                retValue.put(OBJECT_ID, objectID); 
            }  
            else if(plantName != null && plantName != ''
              && stockName != null && stockName != '')
            {
                mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c,
                        SerialNo__c, DeliveryBrand__r.ID2__c  
                        from SCMaterialMovement__c
                        where Plant__c = :plantName and Stock__r.Name = : stockName
						and (Order__c = null or (Order__c <> null and OrderLine__c <> null))
                        and Type__c in ('5204', '5215', '5229', '5227')
                        and (ERPStatus__c = 'none')
                        and Article__c <> null
                        order by Stock__c];
            }  
            else if(plantName != null && plantName != '')
            {
                mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c,
                        SerialNo__c, DeliveryBrand__r.ID2__c  
                        from SCMaterialMovement__c
                        where Plant__c = :plantName 
						and (Order__c = null or (Order__c <> null and OrderLine__c <> null))
                        and Type__c in ('5204', '5215', '5229', '5227')
                        and (ERPStatus__c = 'none')
                        and Article__c <> null
                        order by Stock__c];
                
            }
            else if (stockName != null && stockName != '')
            {
                mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c,
                        SerialNo__c , DeliveryBrand__r.ID2__c 
                        from SCMaterialMovement__c
                        where Stock__c = :stockName // do not change in stockName there is stockID
						and (Order__c = null or (Order__c <> null and OrderLine__c <> null))
                        and Type__c in ('5204', '5215', '5229', '5227')
                        and (ERPStatus__c = 'none')
                        and Article__c <> null
                        order by Stock__c];
            }
            /* GMSNA wir wollen nicht alles verbuchen!
            else
            {
                mml =  [Select Id, Name, Type__c, Plant__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                                            Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c
                                            from SCMaterialMovement__c
                                            where Type__c in ('5204', '5215', '5229')
                                            and (ERPStatus__c = 'none')
                                            order by Stock__c];
            }*/
        }
        else
        {
            // read movements with ids in the list
            debug('orignIdOrNameList: ' + originIdOrNameList);
            mml = [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                   Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c,
                   SerialNo__c, DeliveryBrand__r.ID2__c 
                    from SCMaterialMovement__c
                    where id in :originIdOrNameList
//					and (Order__c = null or (Order__c <> null and OrderLine__c <> null)) // TODO: ask Norbert if it is correct
                    order by Stock__c];
            
        }                   
        retValue.put(ROOT, mml);
        debug('material movement list: ' + mml);         
        // read the SerialNo__c out of the Material Movements.
        // if a serialNo is set, a Equipment is moved.
        // For the SAP processing we nee the ProductModel__r.Name
        List<String> eqSerialNos = new List<String>();
        for(SCMaterialMovement__c mm : mml)
        {
            if(mm.SerialNo__c != null)
            {
                eqSerialNos.add(mm.SerialNo__c);
            }
        }
        Map<String,SCInstalledBase__c> mapSerialMoIB = new Map<String,SCInstalledBase__c>();
        if(eqSerialNos.size() > 0)
        {
            mapSerialMoIB = selectEquipments(eqSerialNos);
        }
        retValue.put(PROD_NAMES, mapSerialMoIB);                                       
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    /* 
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)  
    {
        Boolean retValue = true;
        List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
        if(mml != null && mml.size() > 0)
        {
            // instantiate the client of the web service
            piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port ws = new piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port();
    
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('MaterialMovementCreate');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
            
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader();
                
                // instantiate the body of the call
                piCceagDeSfdcCSMaterialMovementCreate.MovementItem[] movementArr = setMovements(mml,u, testMode);
                if(movementArr.size() > 0)
                {
                    setMessageHeader(messageHeader, mml, messageID, u, testMode);
                    
        
                    String jsonInputMessageHeader = JSON.serialize(messageHeader);
                    String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                    debug('from json Message Header: ' + fromJSONMapMessageHeader);
        
                    String jsonInputMovementArray = JSON.serialize(movementArr);
                    String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                    debug('from json material array: ' + fromJSONMapMovementArray);
                    
                    rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapMovementArray;
                    // check if there are missing mandatory fields
                    if(rs.message_v3 == null || rs.message_v3 == '')
                    {
                        // All mandatory fields are filled
                        // callout
                        if(!testMode)
                        {
                            // It is not the test from the test class
                            // go
                            rs.setCounter(movementArr.size());
                            debug('count:' + rs.getCounter());
                            ws.MaterialMovementCreate_Out(messageHeader, movementArr);
                            rs.Message_v2 = 'void';
                        }
                    }
                }
                else
                {
                    retValue = false;
                    // Interface Log must not be written
                }    
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        else
        {
            retValue = false;
        }
        return retValue;    
    }
    */
    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     * @param ilpList the list with the changable part of the interface log fields
     *
     */
    public override Boolean fillAndSendERPDataWithManyInterfaceLogs(Map<String, Object> dataMap, String messageID, 
                                        CCWSUtil u, Boolean testMode, SCReturnStruct rs, List<InterfaceLogPart> ilpList)  
    {
        Boolean retValue = true;
        List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
        Map<String,SCInstalledBase__c> mapSerialNoIB = (Map<String,SCInstalledBase__c>)dataMap.get(PROD_NAMES);
        
        if(mml != null && mml.size() > 0)
        {
            // instantiate the client of the web service
            piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port ws = new piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port();
    
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('MaterialMovementCreate');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
			ws.timeout_x = u.getTimeOut();
            
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader();
                
                // instantiate the body of the call
                piCceagDeSfdcCSMaterialMovementCreate.MovementItem[] movementArr = setMovements(mml,u, testMode, ilpList, mapSerialNoIB);
                if(movementArr.size() > 0)
                {
                    setMessageHeader(messageHeader, mml, messageID, u, testMode);

                    String jsonInputMessageHeader = JSON.serialize(messageHeader);
                    String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                    debug('from json Message Header: ' + fromJSONMapMessageHeader);
        
                    String jsonInputMovementArray = JSON.serialize(movementArr);
                    String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                    debug('from json material array: ' + fromJSONMapMovementArray);

                    // add message header to the interface log parts
                    for(InterfaceLogPart ilp: ilpList)
                    {
                        ilp.data = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ilp.data;
                    }
                    
                    rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader 
                               + ',\n\nmessageData: ' + fromJSONMapMovementArray;
                    // check if there are missing mandatory fields
                    if(rs.message_v3 == null || rs.message_v3 == '')
                    {
                        // All mandatory fields are filled
                        // callout
                        if(!testMode)
                        {
                            // It is not the test from the test class
                            // go
                            rs.setCounter(movementArr.size());
                            debug('count:' + rs.getCounter());
                            ws.MaterialMovementCreate_Out(messageHeader, movementArr);
                            rs.Message_v2 = 'void';
                        }
                    }
                }
                else
                {
                    // Interface Log must not be written
                    retValue = false;

                    // Only for movements with an order
                    String orderIdHead = (String)dataMap.get(OBJECT_ID);
                    if(orderIdHead != null)
                    {
                        //updateOrder(orderIdHead, 'none');
                        CCWCOrderCloseEx.processNextAfterMoveMat(orderIdHead);
                    }               
                }    
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        else
        {
            // Interface Log must not be written

            retValue = false;
            // Only for movements with an order
            String orderIdHead = (String)dataMap.get(OBJECT_ID);
            if(orderIdHead != null)
            {
                //updateOrder(orderIdHead, 'none');
                CCWCOrderCloseEx.processNextAfterMoveMat(orderIdHead);
            }               
        }
        return retValue;    
    }

    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
     
    public void  setMessageHeader(piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader mh, 
                                         List<SCMaterialMovement__c> mml, String messageID, CCWSUtil u, Boolean testMode)
    {
        if(mml != null && mml.size() > 0)
        {
            mh.MessageID = messageID;
    //      mh.MessageUUID;
            mh.ReferenceID = mml[0].Name;
    //      mh.ReferenceUUID;
            mh.CreationDateTime = u.getFormatedCreationDateTime();
            if(testMode)
            {
                mh.TestDataIndicator = 'Test';
            }    
            mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
            mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
            debug('Message Header: ' + mh);
        }    
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param mml list of material movements
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     * @return MovementItem[] 
     *
     */
    /* 
    public piCceagDeSfdcCSMaterialMovementCreate.MovementItem[] setMovements(List<SCMaterialMovement__c> mml, CCWSUtil u, Boolean testMode)
    {
        List<piCceagDeSfdcCSMaterialMovementCreate.MovementItem> retValue = new List<piCceagDeSfdcCSMaterialMovementCreate.MovementItem>();
        if(mml != null)
        {
            String step = '';
            try
            {
                for(SCMaterialMovement__c mm: mml)
                {
                    piCceagDeSfdcCSMaterialMovementCreate.MovementItem mi = new piCceagDeSfdcCSMaterialMovementCreate.MovementItem();
                    mi.MovementType = u.getSAPMaterialMovementType(mm.Type__c);
                    
                    // 5204 - SAP 261 Goods Issue against Order
                    if(mm.Type__c == '5204') // Consumption
                    {
                        // the engineer's van stock is the material movement relevant stock
                        mi.Plant = mm.Plant__c;
                        mi.StorageLocation = mm.Stock__r.Name;
                    }
                    else if(mm.Type__c == '5215')
                    {
                        // 5215 Transfer out - SAP 311 Transfer Posting
                        mi.ReceivingPlant = mm.Plant__c;
                        mi.ReceivingStorageLocation = mm.Stock__r.Name;
                        mi.Plant = mm.DeliveryPlant__c;
                        mi.StorageLocation = mm.DeliveryStock__r.Name;
                    }
                    // 5229?    315 PackageItem Received Sent
                    else
                    {
                        mi.ReceivingPlant = mm.Plant__c;
                        mi.ReceivingStorageLocation = mm.Stock__r.Name;
                        mi.Plant = mm.DeliveryPlant__c;
                        mi.StorageLocation = mm.DeliveryStock__r.Name;
                    }
                    //mi.Batch;
                    mi.MaterialID = mm.Article__r.Name;
                    mi.UnitOfMeasure = u.getSAPUnitOfMeasure(mm.Article__r.Unit__c);
                    
                    mi.Quantity = '0';
                    if(mm.Qty__c != null)
                    {
                        mi.Quantity = '' + mm.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                    }   
                    mi.ReferenceNumber = mm.Name;
                    //mi.OrderNumber = null;
                    if(mm.Order__r.ERPOrderNo__c != null)
                    {
                        mi.OrderNumber = mm.Order__r.ERPOrderNo__c;
                    }
                    mi.ActivityNumber = '0010'; 
                    mi.ValuationType = mm.ValuationType__c;
                    mi.PostingDate = Date.today();
                    retValue.add(mi);
                }
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'setMovements: ' + step + ' ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }    
        return retValue;    
    }
    */
    public piCceagDeSfdcCSMaterialMovementCreate.MovementItem[] setMovements(List<SCMaterialMovement__c> mml, CCWSUtil u, Boolean testMode,
                                                                    List<InterfaceLogPart> ilpList,
                                                                    Map<String,SCInstalledBase__c> mapSerialNoIB)
    {
        List<piCceagDeSfdcCSMaterialMovementCreate.MovementItem> retValue = new List<piCceagDeSfdcCSMaterialMovementCreate.MovementItem>();
        if(mml != null)
        {
            String step = '';
            try
            {
                ID stockPrev = null;
                InterfaceLogPart ilpCurrent = null;
                piCceagDeSfdcCSMaterialMovementCreate.MovementItem[] partArrOfMovementItem = null;              
                for(SCMaterialMovement__c mm: mml)
                {
                    debug('stockPrev: ' + stockPrev);
                    if(stockPrev == null || stockPrev != null && mm.Stock__c != null && stockPrev != mm.Stock__c)
                    {
                        if(stockPrev != null)
                        {
                            // set the data of the previous stock to interface log part
                            String jsonInputMovementArray = JSON.serialize(partArrOfMovementItem);
                            String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                            debug('from json material array: ' + fromJSONMapMovementArray);
                            ilpCurrent.data = ',\n\nmessageData: ' + fromJSONMapMovementArray;
                        }
                        partArrOfMovementItem = new List<piCceagDeSfdcCSMaterialMovementCreate.MovementItem>();             
                        ilpCurrent = new InterfaceLogPart();
                        ilpList.add(ilpCurrent);
                        ilpCurrent.referenceID = mm.Stock__c; 
                        ilpCurrent.refType = 'SCStock__c';
                        if(refType != null && refType != 'SCStock__c' && referenceID != null)
                        {
                            ilpCurrent.refType2 = refType;
                            ilpCurrent.referenceID2 = referenceID;
                        }   
                        stockPrev = mm.Stock__c;
                    }

                    ilpCurrent.count++;
                    piCceagDeSfdcCSMaterialMovementCreate.MovementItem mi = new piCceagDeSfdcCSMaterialMovementCreate.MovementItem();
                    mi.MovementType = u.getSAPMaterialMovementType(mm.Type__c);
                    
                    // 5204 - SAP 261 Goods Issue against Order
                    if(mm.Type__c == '5204') // Consumption
                    {
                        // the engineer's van stock is the material movement relevant stock
                        mi.Plant = mm.Plant__c;
                        mi.StorageLocation = mm.Stock__r.Name;
                    }
                    else if(mm.Type__c == '5215') // Transfer out
                    {
                        // 5215 Transfer out - SAP 311 Transfer Posting
                        mi.ReceivingPlant = mm.Plant__c;
                        mi.ReceivingStorageLocation = mm.Stock__r.Name;
                        mi.Plant = mm.DeliveryPlant__c;
                        mi.StorageLocation = mm.DeliveryStock__r.Name;

                    }
                    else if(mm.Type__c == '5227') // 315 PackageItem Received Sent
                    {
                        mi.ReceivingPlant = mm.Plant__c;
                        mi.ReceivingStorageLocation = mm.Stock__r.Name;
                        mi.Plant = mm.DeliveryPlant__c;
                        mi.StorageLocation = mm.DeliveryStock__r.Name;
                    }
                    else
                    {
                        mi.ReceivingPlant = mm.Plant__c;
                        mi.ReceivingStorageLocation = mm.Stock__r.Name;
                        mi.Plant = mm.DeliveryPlant__c;
                        mi.StorageLocation = mm.DeliveryStock__r.Name;
                    }
                    // added for EquipmentMovement (19.02.13 / GMSGB)
                    // equipment is moved of Brand is changed
                    if(mm.SerialNo__c != null && mm.Type__c == '5215')
                    {
                        mi.SerialNumber = mm.SerialNo__c;
                        mi.Quantity = '1';
                        mi.OrderNumber = null;
                        mi.ValuationType = null;
                        // If the MM moves an Equipment - then the MaterialID shouzld be ProductModel__r.Name  
                        SCInstalledBase__c eq = mapSerialNoIB.get(mm.SerialNo__c);
                        mi.MaterialID = eq.ProductModel__r.Name;
                        
                        // GMSGB Change for CCE ???
                        /*if(eq.Brand__c != null)
                        {
                        	mi.Batch = eq.Brand__r.ID2__c; 
                        	mi.ReceivingBatch = eq.Brand__r.ID2__c;
                        }
                        else
                        {
                        	mi.Batch = null;
                        }*/
                        
                        
                        if(mm.DeliveryBrand__c != null)
                        {
                            // Brand Change
                            mi.Batch = mm.DeliveryBrand__r.ID2__c; 
                            mi.ReceivingBatch = eq.Brand__r.ID2__c;    
                                
                        }
                        else
                        {
                            // Equipment movement
                            mi.Batch = eq.Brand__r.ID2__c; 
                        }

                    }
                    else // SparePart is moved
                    {
                        mi.Quantity = '0';
                        if(mm.Qty__c != null)
                        {
                            mi.Quantity = '' + mm.Qty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places;
                        } 
                        if(mm.Order__r.ERPOrderNo__c != null)
                        {
                            mi.OrderNumber = mm.Order__r.ERPOrderNo__c;
                        }
                        mi.ValuationType = mm.ValuationType__c;
                        mi.MaterialID = mm.Article__r.Name;
                    }                       
                    //mi.Batch;
                    
                    mi.UnitOfMeasure = u.getSAPUnitOfMeasure(mm.Article__r.Unit__c); 
                    mi.ReferenceNumber = mm.Name;
                    mi.ActivityNumber = '0010';                     
                    mi.PostingDate = Date.today();
                    retValue.add(mi);
                    partArrOfMovementItem.add(mi);
                }// for
                // logging for last stock
                if(stockPrev != null)
                {
                    // set the data of the previous stock to interface log part
                    String jsonInputMovementArray = JSON.serialize(partArrOfMovementItem);
                    String fromJSONMapMovementArray = getDataFromJSON(jsonInputMovementArray);
                    debug('from json material array: ' + fromJSONMapMovementArray);
                    ilpCurrent.data = ',\n\nmessageData: ' + fromJSONMapMovementArray;
                }
                
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'setMovements: ' + step + ' ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }    
        return retValue;    
    }


    /**
     * Sets the ERPStatus in the root object list to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        debug('error: ' + error);
        debug('dataMap: ' + dataMap);
        if(dataMap != null)
        {
            List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
            if(mml != null)
            {
                debug('mml: ' + mml);
                String status = null;
                // default interface Log
                ID defInterfaceLogId = null;
                if(interfaceLogList.size() > 0 && interfaceLogList[0].id != null)
                {
                    defInterfaceLogId = interfaceLogList[0].id; 
                }
                for(SCMaterialMovement__c mm: mml)
                {
                    debug('material movement: ' + mm);
                    ID loopInterfaceLogId = getInterfaceLogId(interfaceLogList, mm.Stock__c);
                    if(loopInterfaceLogId == null)
                    {
                        loopInterfaceLogId = defInterfaceLogId; 
                    }
                    if(loopInterfaceLogId != null)
                    {
                        mm.InterfaceLog__c = loopInterfaceLogId;
                    }
                    debug('prev mm.ERPStatus__c: ' + mm.ERPStatus__c);
                    if(error)
                    {
                        mm.ERPStatus__c = 'error';
                    }
                    else
                    {   
                        mm.ERPStatus__c = 'pending';
                    }
                        debug('after 1 mm.ERPStatus__c: ' + mm.ERPStatus__c);
                    // we do not override error status with pending
                    // if there is only one error in material movement then the flag in order for all is also set to error
                    if(status == null || status != 'error')
                    {
                        status = mm.ERPStatus__c;                       
                    }
                    
                }// for       
                update mml;
                debug('materal movement after update: ' + mml);
                if(orderId != null)
                {
                    updateOrder(orderId, status);
                }               
            }
        }        
    }

    public void updateOrder(ID orderID, String status)
    {
        if(orderID != null)
        {
            List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
            if(orderList != null && orderList.size() > 0)
            {
                for(SCOrder__c o: orderList)
                {
                    if(status != null)
                    {
                        o.ERPStatusMaterialMovement__c = status;
                    }       
                }
            }
            update orderList;
        }       
    }

    public static Map<String,SCInstalledBase__c> selectEquipments(List<String> eqSerialNos)
    {
        Map<String,SCInstalledBase__c> mapSerialNoIB = new Map<String,SCInstalledBase__c>(); 
        List<SCInstalledBase__c> items = [select ID,SerialNo__c,ProductModel__r.Name, Brand__r.ID2__c from SCInstalledBase__c 
            where SerialNo__c in: eqSerialNos ];
        for (SCInstalledBase__c ib : items)
        {
            mapSerialNoIB.put(ib.SerialNo__c,ib);
        }
        return mapSerialNoIB;
    }
    
    public static List<SCMaterialMovement__c> getMMbyOrder(String objectID)
    {
        // Read all order related consumptions (status = 5407 book)
       List<SCMaterialMovement__c> mml = [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, ERPStatus__c,
                SerialNo__c, DeliveryBrand__c 
                from SCMaterialMovement__c
                where Order__c = :objectID
                and ((Type__c = '5204' and Status__c = '5408') 
                    or (Type__c = '5227' and Status__c = '5407')) 
                and (ERPStatus__c = 'none')
                and Article__c <> null
				and (Order__c = null or (Order__c <> null and OrderLine__c <> null)) // TODO: ask Norbert
                order by Stock__c];
        return mml;
    }

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}