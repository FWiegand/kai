/*
 * @(#)SCInterfaceExportBase.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * SCInterfaceExportBase is a basic class for export asnychronous web services
 *
 * Because we can not override the static methods ( a future method must be static) we must do some tricks
 * to get the subclassing working.
 * Constructors and their parameters by dynamic creation and future methods of instances are without meaning, because
 * we can not override the future functions.  
 *
 * Example:
 *  Call static method from a trigger CCWCOrderCreate.callout(oid, async, testMode);
 *  CCWCOrderCreate.callout(oid, async, testMode) calls
 *  String interfaceName = 'SAP_ORDER_CREATE';
 *           String interfaceHandler = 'CCWCOrderCreate';
 *           String refType = 'SCOrder__c';
 *           CCWCOrderCreate oc = new CCWCOrderCreate(interfaceName, interfaceHandler, refType); 
 *           oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCOrderCreate');
 * oc.calloutCore calls
 *           SCInterfaceExportBase.doCalloutAsync(oid, testMode, interfaceNamePar, 
 *                                      interfaceHandlerPar, refTypePar, subclassNamePar); 
 *
 * SCInterfaceExportBase.doCalloutAsync(...) calls
 *          Type t = Type.forName(subclassName); // CCWCOrderCreate
 *          SCInterfaceExportBase ieb = (SCInterfaceExportBase)t.newInstance();
 *          ieb.setInterfaceName(interfaceName);
 *          ieb.setInterfaceHandler(interfaceHandler);
 *          ieb.setReferenceType(refType);
 *          ieb.setSubclassName(subclassName);        
 *          ieb.plantName = plantNamePar;
 *          ieb.stockName = stockNamePar;
 *          ieb.mode = modePar;  
 *          ieb.doCalloutSync(objectId, testMode);  
 *
 * 
 *
 *          
 * Algorithm:
 * First in the static subclass function ### callout (see ###CCWCOrderCreate) we instantiate the subclass 
 * with defined interfaceName, interfaceHandler and refType for an interface log head reference.
 * Then in the calloutCore we call the static async future function. This function dynamically creates the instance of a subclass.
 * On that subclass then is called the synchronous framework function for an export. So we get the subclass behaviour with override functions
 *
 * The same way we go if we need to start the job in the synchronous way. 
 *  
 * 
 * Methods to override: 
 *  fillAndSendERPData(...)
 *  fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCInterfaceExportBase extends SCInterfaceBase
{ 
    public static String ROOT = 'ROOT';
    public static String KEY_LIST = 'LIST';
    public static String OBJECT_ID = 'OBJECT_ID';
    public List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
    public String interfaceName;
    public String interfaceHandler;
    public String subclassName; 
    public String refType;
    public ID referenceID = null;
    public ID referenceID2 = null;
    public String refType2 = '';
    public ID responseID = null;
    
    public String plantName;
    public String stockName;
    public String mode;
    public String interfaceNameReprocess;
    public String action;
    public String idocNumber;
    public List<String> originIdOrNameList;
            
    public void setInterfaceName(String interfaceName)
    {
        this.interfaceName = interfaceName;
    }
    public void setInterfaceHandler(String interfaceHandler)
    {
        this.interfaceHandler = interfaceHandler;
    }
    public void setSubclassName(String subclassName)
    {
        this.subclassName = subclassName;
    }

    public void setReferenceType(String refType)
    {
        this.refType = refType;
    }
    public void setReferenceID(ID referenceId)
    {
        this.referenceID = referenceId;
    }

    public void setReferenceType2(String refType)
    {
        this.refType2 = refType;
    }
    public void setReferenceID2(ID referenceId)
    {
        this.referenceID2 = referenceId;
    }


    public SCInterfaceExportBase()
    {
    }

    public SCInterfaceExportBase(String interfaceName, String interfaceHandler, String refType, String subclassNamePar)
    {
        this.interfaceName = interfaceName;
        this.interfaceHandler = interfaceHandler;
        this.refType = refType;
        this.subclassName = subclassNamePar;
    }


    public String calloutCore(String oid, boolean async, boolean testMode, 
                              String interfaceNamePar, String interfaceHandlerPar, String refTypePar, String subclassNamePar)
    {
        String plantNamePar = null;
        String stockNamePar = null; 
        String modePar = null;
        return calloutCore(oid, async, testMode, interfaceNamePar, interfaceHandlerPar, refTypePar, 
                              subclassNamePar, plantNamePar, stockNamePar, modePar);
    }                           

    public String calloutCore(String oid, boolean async, boolean testMode, 
                              String interfaceNamePar, String interfaceHandlerPar, String refTypePar, 
                              String subclassNamePar, String plantNamePar, String stockNamePar, String modePar)
    {
        String interfaceNameReprocessPar = null;
        String actionPar = null;
        String idocNumberPar = null;
        List<String> origList = null;
        return calloutCore(oid, async, testMode, 
                              interfaceNamePar, interfaceHandlerPar, refTypePar, 
                              subclassNamePar, plantNamePar, stockNamePar, modePar,
                              interfaceNameReprocessPar, actionPar, idocNumberPar, origList);
        
    }

    public String calloutCore(String oid, boolean async, boolean testMode, 
                              String interfaceNamePar, String interfaceHandlerPar, String refTypePar, 
                              String subclassNamePar, String plantNamePar, String stockNamePar, String modePar, List<String> origList)
    {
        String interfaceNameReprocessPar = null;
        String actionPar = null;
        String idocNumberPar = null;
        return calloutCore(oid, async, testMode, 
                              interfaceNamePar, interfaceHandlerPar, refTypePar, 
                              subclassNamePar, plantNamePar, stockNamePar, modePar,
                              interfaceNameReprocessPar, actionPar, idocNumberPar, origList);
        
    }

    public String calloutCore(String oid, boolean async, boolean testMode, 
                              String interfaceNamePar, String interfaceHandlerPar, String refTypePar, 
                              String subclassNamePar, String plantNamePar, String stockNamePar, String modePar,
                              String interfaceNameReprocessPar, String actionPar, String idocNumberPar, List<String> origList)
    {
        String retValue = '';
        // now to the callout to the advanced scheduling engine
        if(async)
        {
            SCInterfaceExportBase.doCalloutAsync(oid, testMode, interfaceNamePar, 
                                        interfaceHandlerPar, refTypePar, subclassNamePar, plantNamePar, stockNamePar, modePar,
                                        interfaceNameReprocessPar, actionPar, idocNumberPar, origList); 
        }
        else
        {
            subclassName = subclassNamePar;

            System.debug('###....doCalloutAsync objectID: ' + oid);
            System.debug('###....subclassName: ' + subclassName);
            Type t = Type.forName(subclassName);
            System.debug('###....type: ' + t);

            SCInterfaceExportBase ieb = (SCInterfaceExportBase)t.newInstance();
            System.debug('###....ieb instance: ' + ieb);
                
            ieb.setInterfaceName(interfaceNamePar);
            ieb.setInterfaceHandler(interfaceHandlerPar);
            ieb.setReferenceType(refTypePar);
            ieb.setSubclassName(subclassNamePar);        
            ieb.plantName = plantNamePar;
            ieb.stockName = stockNamePar;
            ieb.mode = modePar;
            ieb.interfaceNameReprocess = interfaceNameReprocessPar;
            ieb.action = actionPar;
            ieb.idocNumber = idocNumberPar;
            if(interfaceHandlerPar == 'CCWCMaterialMovementCreate'
               || interfaceHandlerPar == 'CCWCMaterialReservationCreate')
            {
                ieb.originIdOrNameList = origList;
                retValue = ieb.doCalloutSyncWithManyInterfaceLogs(oid, testMode);   
            }
            else
            {  
                retValue = ieb.doCalloutSync(oid, testMode);
            }   
        }
        return retValue;
    } // callout   
   /*
    * Asynchronous execution of the callout to the SAP system. 
    * Details see: doCalloutSync
    * @param oid   objectId
    * @param async     true: use a future method to call 
    *                  false: do a synchrounous callout
    * @param testMode true: emulate call
    */
    @future (callout=true)
    public static void doCalloutAsync(String objectId, Boolean testMode, String interfaceName, 
                                        String interfaceHandler, String refType, String subclassName,
                                        String plantName, String stockName, String mode, String interfaceNameReprocess, 
                                        String action, String idocNumber, List<String> origList) 
    {
        String retValue = null;
        System.debug('###....doCalloutAsync objectID: ' + objectId);
        System.debug('###....subclassName: ' + subclassName);
        Type t = Type.forName(subclassName);

        SCInterfaceExportBase ieb = (SCInterfaceExportBase)t.newInstance();
        System.debug('###....ieb instance: ' + ieb);
        
        ieb.setInterfaceName(interfaceName);
        ieb.setInterfaceHandler(interfaceHandler);
        ieb.setReferenceType(refType);
        ieb.setSubclassName(subclassName);        
        ieb.plantName = plantName;
        ieb.stockName = stockName;
        ieb.mode = mode;  
        ieb.interfaceNameReprocess = interfaceNameReprocess;
        ieb.action = action;
        ieb.idocNumber = idocNumber;
        if(ieb.interfaceHandler == 'CCWCMaterialMovementCreate'
           || interfaceHandler == 'CCWCMaterialReservationCreate')
        {
            ieb.originIdOrNameList = origList;
            retValue = ieb.doCalloutSyncWithManyInterfaceLogs(objectId, testMode);  
        }
        else
        {  
            retValue = ieb.doCalloutSync(objectId, testMode);
        }   
    }
    
    /**
     * Synchronous execution of the creating order
     *
     * @param objectId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public String doCalloutSync(String objectId, Boolean testMode)
    {
        debug('doCalloutSync orderID: ' + objectId + ', testMode: ' + testMode);
        CCWSUtil u = new CCWSUtil();
        String messageID = u.getMessageID();
        String interfaceName = this.interfaceName;
        String type = 'OUTBOUND';
        String direction = 'outbound';
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        String data = '';
        Datetime start = DateTime.now(); 
        Integer count = 0;
        SCReturnStruct rs = new SCReturnStruct(); // used for delivering of error messages
        Map<String, Object> dataMap = new Map<String, Object>();
//        Savepoint sp = Database.setSavepoint();
//        You have uncommitted work pending. Please commit or rollback before calling out
//        It is not possible to make a call out and make a DML operation in the same class using save point
        Boolean writeInterfaceLog = false;
        Boolean exceptionThrown = false;
        Boolean error = false; 
        try
        {
            // read object
            readSalesforceData(objectId, dataMap, testMode);
            
            debug('dataMap: ' + dataMap);
            
            // get reference for the interface log object
            Object obj = dataMap.get(ROOT);
            if(obj != null)
            {
                // if we ask directly if an obj is an instanceof SObject 
                // and the object is of type List<SObject> then we get
                // Invalid conversion from runtime type LIST<SCMaterialMovement__c> to SObject
                if(!(obj instanceof List<SObject>))
                {
                    if(obj instanceof SObject)
                    {
                        if(refType == 'SCOrder__c' && obj instanceof SCOrder__c)
                        {
                            referenceID = ((SObject)obj).ID;    // for interface log
                        }   
                        else if(refType == 'SCMaterialReplenishment__c' && obj instanceof SCMaterialReplenishment__c)
                        {
                            referenceID = ((SObject)obj).ID;    // for interface log
                        }   
                        else if(refType == 'SCInventory__c' && obj instanceof SCInventory__c)
                        {
                            referenceID = ((SObject)obj).ID;    // for interface log
                            if(refType2 == 'SCStock__c')
                            {
                                referenceID2 = ((SCInventory__c)obj).Stock__c;
                            }
                        }
                        // 04.01.13 added by GMSGB for the CCWCAssetInventoryDataRequest interface 
                        else if(refType == 'SCInstalledBase__c' && obj instanceof SCInstalledBase__c)
                        {
                            referenceID = ((SCInstalledBase__c)obj).ID;    // for interface log
                        }   
                        // 21.01.13 added by GMSGB for the SparePartPackageRecieved interface 
                        else if(refType == 'SCMaterialMovement__c' && obj instanceof SCMaterialMovement__c)
                        {
                            //refType = 'SCMaterialMovement__c';
                            referenceID = ((SCMaterialMovement__c)obj).ID;    // for interface log
                        }
                    }   
                }
                else
                {
/*
                    referenceID and type has been set in the subclasses of this class
                    if(obj instanceof List<SObject>)
                    {
                        // List of SCMaterialMovement__c
                        if(((List<SObject>)obj).size() > 0)
                        {
                            SObject so = ((List<SObject>)obj)[0]; 
                            if(so != null && so instanceof SCMaterialMovement__c)
                            {
                                if(refType == 'SCOrder__c' && obj instanceof SCOrder__c)
                                {
                                    referenceID = ((SCMaterialMovement__c)so).Order__c;    // for interface log
                                }   
                                else if(refType == 'SCMaterialReplenishment__c')
                                {
                                    referenceID = ((SCMaterialMovement__c)so).Replenishment__c;    // for interface log
                                }   
                                if(refType2 == 'SCOrder__c' && obj instanceof SCOrder__c)
                                {
                                    referenceID2 = ((SCMaterialMovement__c)so).Order__c;    // for interface log
                                }   
                                else if(refType2 == 'SCMaterialReplenishment__c')
                                {
                                    referenceID2 = ((SCMaterialMovement__c)so).Replenishment__c;    // for interface log
                                }   
                            }   
                        }       
                    }   
*/
                }   
            }
            else 
            {
                if(refType != null)
                {
                    throw new SCfwException('No data has been read!');
                }   
            }   
             
            // fill and send ERP data  
            debug('before fillAndSendERPData');
            writeInterfaceLog = fillAndSendERPData(dataMap, messageId, u, testMode, rs);
            ///////////////////////////////////
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // Look beneath in catch branch because we come here if no web call has been called in case of zero objects
            // After the WEB call we go always into the catch branch
            ///////////////////////////////////
            debug('writeInterfaceLog: ' + writeInterfaceLog);            
            
            // check if all mandatory fields are filled
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                if(rs.Message_v2 != null && rs.Message_v2 != '')
                {
                    data += '\nResponse from SAP:\n' + rs.Message_v2 + '\n\n';
                }   
                if(rs.Message_v4 == null || rs.Message_v4 == '')
                {
                    data += 'Data sent to SAP: ' + rs.Message;
                    debug(data);
                }
                else
                {
                    resultCode = rs.Message_v4;
                    resultInfo = rs.Message;
                }                
                setERPStatus(dataMap, messageID, u, testMode, rs, error);
                count = (Integer)rs.getCounter();
            }
            else
            {
                data = 'Mandatory data is missing: ' + rs.Message_v3;
                debug(data);
                resultCode = 'E101';
                resultInfo = 'Error: Mandatory data is missing.';
            }
        }
        catch(Exception e)
        {
           /* !!! HACK GMSNA/JP 07.11.2012 !!!
            * The asynchronous SAP PI interfaces do not return any reponse. 
            * To be able to import the SAP wsdl files into SF the SFDC:SFDCResponse was added manually.
            * When calling the SAP PI interface the following response is returnded:
            * <SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'><SOAP:Header/><SOAP:Body/></SOAP:Envelope>
            * The SAP response can't be parsed by the SF framework (we tried multiple options to modify the SF wrapper but this does not work) 
            * Therefore we catch the exception here and when an empty response is returned.
            * In case of sematic or authorisation issues the SAP PI returns an HTML pages. In this case we throw the exception.
            */
            debug('exception: ' + e);        
            String msg = e.getMessage();
            debug('WebCallException: ' + msg);
            // we take indexOf because in the subclass some text could be added to the message.
            if(msg != null && msg.indexOf('Web service callout failed: Unexpected element. Parser was expecting element \'http://pi.cceag.de/SFDC:SFDCResponse\' but found \'http://schemas.xmlsoap.org/soap/envelope/:Body\'') > -1)
            {
                // We must set the info that the order has been sent.
                if(rs.Message_v2 != null && rs.Message_v2 != '')
                {
                    data += '\nResponse from SAP:\n' + rs.Message_v2 + '\n\n'; 
                }   
                data += 'Data sent to SAP: ' + rs.Message;
                debug(data);
                writeInterfaceLog = true;
                count = (Integer)rs.getCounter();
            }
            else
            {            
                exceptionThrown = true;
                // Database.rollback(sp);
                if(e instanceof System.CalloutException)
                {
                    resultCode = 'E100';
                }
                else
                {
                    resultCode = 'E101';
                }   
                resultInfo = 'Error:' + e.getMessage();
                data = SCfwException.getExceptionInfo(e);
                debug('rs: ' + rs);
                if(rs.Message_v1 != null && rs.Message_v1 != '') 
                {
                    data += '\n\nEndpoint: '+ rs.Message_v1;
                }
                if(rs.Message_v2 != null && rs.Message_v2 != '')
                {
                    data += '\nResponse from SAP:\n' + rs.Message_v2 + '\n\n';
                }   
                if(rs.Message != null && rs.Message != '')
                {   
                    data += '\n\nData sent to SAP: ' + rs.Message;
                }
                error = true; 
            }
            debug('exceptionThrown: ' + exceptionThrown);
        }
        finally
        {
            debug('finally, interfaceHandler:' + interfaceHandler);
            debug('finally, interfaceName: ' + interfaceName);
            debug('finally writeInterfaceLog: ' + writeInterfaceLog);
            if(writeInterfaceLog || exceptionThrown)
            {
                if(data.length() > 32000)
                {
                    data = data.substring(0, 31900) + '\n### too many data, truncated';
                }
                SCInterfaceLog__c il = null;
                try
                {
                    il = SCInterfaceBase.writeInterfaceLog(interfaceName, interfaceHandler, type, direction, messageID,
                                            referenceID, refType, referenceID2, refType2, responseID, resultCode, resultInfo, 
                                            data, start, count);
                }
                catch(Exception e1)
                {
                    data = SCfwException.getExceptionInfo(e1) + '\n ' +  data;
                    il = SCInterfaceBase.writeInterfaceLog(interfaceName, interfaceHandler, type, direction, messageID,
                                            null, null, null, null, responseID, 'E101', 'Error by writting an interface log object.', 
                                            data, start, count);
                    upsert il;
                }   
                if(il != null)
                {                           
                    interfaceLogList.add(il);
                }                           
                ///////////////////////////////////////////////////////
                // If an exception occurs  the writting into interface log delivers id of the interface log but the object is after that removed from the database
                ///////////////////////////////////////////////////////
                try
                {
                    setERPStatus(dataMap, messageID, u, testMode, rs, error);
                }
                catch(Exception e2)
                {
                    debug('Exception by setting ERPStatus: ' + SCfwException.getExceptionInfo(e2));
                    resultCode = 'E101';
                    resultInfo = 'Error by setting of ERPStatus after the web call to SAP';
                    il.Data__c = SCfwException.getExceptionInfo(e2) + '\n ' + il.Data__c;
                    update il;
                }   
            }                                 
            else
            {
                debug('writeInterfaceLog: ' + writeInterfaceLog + ', exceptionThrown: ' + exceptionThrown);
            }                                 
       }
       return messageId;    
    }

    public String doCalloutSyncWithManyInterfaceLogs(String objectId, Boolean testMode)
    {
        debug('doCalloutSync orderID: ' + objectId + ', testMode: ' + testMode);
        CCWSUtil u = new CCWSUtil();
        String messageID = u.getMessageID();
        String interfaceName = this.interfaceName;
        String type = 'OUTBOUND';
        String direction = 'outbound';
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        String data = '';
        Datetime start = DateTime.now(); 
        Integer count = 0;
        SCReturnStruct rsHead = new SCReturnStruct();
        List<InterfaceLogPart> ilpList = new List<InterfaceLogPart>(); // used for delivering of error messages
        Map<String, Object> dataMap = new Map<String, Object>();
//        Savepoint sp = Database.setSavepoint();
//        You have uncommitted work pending. Please commit or rollback before calling out
//        It is not possible to make a call out and make a DML operation in the same class using save point
        Boolean writeInterfaceLog = false;
        Boolean exceptionThrown = false;
        Boolean error = false;
        try
        {
            // read object
            readSalesforceData(objectId, dataMap, testMode);
            debug('dataMap: ' + dataMap);
            
            // get reference for the interface log object
            Object obj = dataMap.get(ROOT);

            // fill and send ERP data  
            debug('before fillAndSendERPData');
            writeInterfaceLog = fillAndSendERPDataWithManyInterfaceLogs(dataMap, messageId, u, testMode, rsHead, ilpList);
            ///////////////////////////////////
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // Look beneath in catch branch because we come here if no web call has been called in case of zero objects
            // After the WEB call we go always into the catch branch
            ///////////////////////////////////
            debug('writeInterfaceLog: ' + writeInterfaceLog);            
            
            // check if all mandatory fields are filled
            if(rsHead.message_v3 == null || rsHead.message_v3 == '')
            {
                if(rsHead.Message_v2 != null && rsHead.Message_v2 != '')
                {
                    data = '\nReturn value:' + rsHead.Message_v2 + '\n\n';
                }   
                
                if(rsHead.Message_v4 == null || rsHead.Message_v4 == '')
                {
                    data += 'Data sent to SAP: ' + rsHead.Message;
                    debug(data);
                }
                else
                {
                    resultCode = rsHead.Message_v4;
                    resultInfo = rsHead.Message;
                }                
                setERPStatus(dataMap, messageID, u, testMode, rsHead, error);
                count = (Integer)rsHead.getCounter();
            }
            else
            {
                data = 'Mandatory data is missing: ' + rsHead.Message_v3;
                debug(data);
                resultCode = 'E101';
                resultInfo = 'Error: Mandatory data is missing.';
            }

        }
        catch(Exception e)
        {
           /* !!! HACK GMSNA/JP 07.11.2012 !!!
            * The asynchronous SAP PI interfaces do not return any reponse. 
            * To be able to import the SAP wsdl files into SF the SFDC:SFDCResponse was added manually.
            * When calling the SAP PI interface the following response is returnded:
            * <SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'><SOAP:Header/><SOAP:Body/></SOAP:Envelope>
            * The SAP response can't be parsed by the SF framework (we tried multiple options to modify the SF wrapper but this does not work) 
            * Therefore we catch the exception here and when an empty response is returned.
            * In case of sematic or authorisation issues the SAP PI returns an HTML pages. In this case we throw the exception.
            */
            debug('exception: ' + e);        
            String msg = e.getMessage();
            debug('WebCallException: ' + msg);
            // we take indexOf because in the subclass some text could be added to the message.
            if(msg != null && msg.indexOf('Web service callout failed: Unexpected element. Parser was expecting element \'http://pi.cceag.de/SFDC:SFDCResponse\' but found \'http://schemas.xmlsoap.org/soap/envelope/:Body\'') > -1)
            {
                
                // We must set the info that the order has been sent.
                if(rsHead.Message_v2 != null && rsHead.Message_v2 != '')
                {
                    data = '\nReturn value:' + rsHead.Message_v2 + '\n\n'; 
                }   
                data += 'Data sent to SAP: ' + rsHead.Message;
                debug(data);
                writeInterfaceLog = true;
                count = (Integer)rsHead.getCounter();
            }
            else
            {            
                exceptionThrown = true;
                // Database.rollback(sp);
                if(e instanceof System.CalloutException)
                {
                    resultCode = 'E100';
                }
                else
                {
                    resultCode = 'E101';
                }   
                resultInfo = 'Error:' + e.getMessage();
                data = SCfwException.getExceptionInfo(e);
                debug('rs: ' + rsHead);
                if(rsHead.Message_v1 != null && rsHead.Message_v1 != '')
                {
                    data += '\n\nEndpoint: '+ rsHead.Message_v1;
                }
                if(rsHead.Message != null && rsHead.Message != '')
                {   
                    data += '\n\nData sent to SAP: ' + rsHead.Message;
                }
                error = true; 
            }
            debug('exceptionThrown: ' + exceptionThrown);
        }
        finally
        {
            debug('finally, interfaceHandler:' + interfaceHandler);
            debug('finally, interfaceName: ' + interfaceName);
            debug('finally writeInterfaceLog: ' + writeInterfaceLog);
            if(writeInterfaceLog || exceptionThrown)
            {
                if(data.length() > 32000)
                {
                    data = data.substring(0, 31900) + '\n### too many data, truncated';
                }
                if(ilpList != null && ilpList.size() > 0)
                {
    
                    List<SCInterfaceLog__c> intLogList = null;
                    try
                    {
                        intLogList = SCInterfaceBase.writeInterfaceLogs(interfaceName, interfaceHandler, type, direction, messageID,
                                          responseID, resultCode, resultInfo, 
                                          start, ilpList);
                    }
                    catch(Exception e1)
                    {
                        for(InterfaceLogPart ilp: ilpList)
                        {
                            ilp.data = SCfwException.getExceptionInfo(e1) + '\n ' +  ilp.data;
                        }   
                        intLogList = SCInterfaceBase.writeInterfaceLogs(interfaceName, interfaceHandler, type, direction, messageID,
                                      responseID, 'E101', 'Error by writting an interface log object.', 
                                      start, ilpList);
                        upsert intLogList;
                    }   
                    if(intLogList != null)
                    {                           
                        interfaceLogList.addAll(intLogList);
                    }                           
                    ///////////////////////////////////////////////////////
                    // If an exception occurs  the writting into interface log delivers id of the interface log but the object is after that removed from the database
                    ///////////////////////////////////////////////////////
                    try
                    {
                        setERPStatus(dataMap, messageID, u, testMode, rsHead, error);
                    }
                    catch(Exception e2)
                    {
                        debug('Exception by setting ERPStatus: ' + SCfwException.getExceptionInfo(e2));
                        resultCode = 'E101';
                        resultInfo = 'Error by setting of ERPStatus after the web call to SAP';
                        for(SCInterfaceLog__c il: intLogList)
                        {
                            il.Data__c = SCfwException.getExceptionInfo(e2) + '\n ' + il.Data__c;
                        }   
                        update intLogList;
                    } 
                }
                else
                {
                    SCInterfaceLog__c il = null;
                    try
                    {
                        il = SCInterfaceBase.writeInterfaceLog(interfaceName, interfaceHandler, type, direction, messageID,
                                                referenceID, refType, referenceID2, refType2, responseID, resultCode, resultInfo, 
                                                data, start, count);
                    }
                    catch(Exception e1)
                    {
                        data = SCfwException.getExceptionInfo(e1) + '\n ' +  data;
                        il = SCInterfaceBase.writeInterfaceLog(interfaceName, interfaceHandler, type, direction, messageID,
                                                null, null, null, null, responseID, 'E101', 'Error by writting an interface log object.', 
                                                data, start, count);
                        upsert il;
                    }   
                    if(il != null)
                    {                           
                        interfaceLogList.add(il);
                    }                           
                    ///////////////////////////////////////////////////////
                    // If an exception occurs  the writting into interface log delivers id of the interface log but the object is after that removed from the database
                    ///////////////////////////////////////////////////////
                    try
                    {
                        setERPStatus(dataMap, messageID, u, testMode, rsHead, error);
                    }
                    catch(Exception e2)
                    {
                        debug('Exception by setting ERPStatus: ' + SCfwException.getExceptionInfo(e2));
                        resultCode = 'E101';
                        resultInfo = 'Error by setting of ERPStatus after the web call to SAP';
                        il.Data__c = SCfwException.getExceptionInfo(e2) + '\n ' + il.Data__c;
                        update il;
                    }   
                }      
            }
            else
            {
                debug('writeInterfaceLog: ' + writeInterfaceLog + ', exceptionThrown: ' + exceptionThrown);
            }                                 
       }
       return messageId;    
    }//doCalloutSyncWithManyInterfaceLogs


    /**
     * Reads data for an outgoing web service 
     * ### please over write this function in the appropriate subclass
     *
     * @param orderId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     * @return a map of SObject needed for filling the outgoing web service xml stream
     *
     */
    public virtual Map<String, Object> readSalesforceData(String objectID, Boolean testMode)
    {
        String msg = 'Please overwrite the function SCInterfaceExportBase.readSalesforceData in an appropriate subclass!';
        debug(msg);
        throw new SCfwException(msg);
    }

    /**
     * Reads data for an outgoing web service 
     * ### please over write this function in the appropriate subclass
     *
     * @param orderId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     * @return a map of SObject needed for filling the outgoing web service xml stream
     *
     */
    public virtual void readSalesforceData(String objectID, Map<String, Object> dataMap, Boolean testMode)
    {
        String msg = 'Please overwrite the function SCInterfaceExportBase.readSalesforceData in an appropriate subclass!';
        debug(msg);
        throw new SCfwException(msg);
    }


    /**
     * Fills the send structure and sends the call to SAP
     *
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     *
     */
    public virtual Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        String msg = 'Please overwrite the function SCInterfaceExportBase.fillAndSendERPData in an appropriate subclass!';
        debug(msg);
        throw new SCfwException(msg);
    }
    /**
     * Fills the send structure and sends the call to SAP
     *
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param ilpList the list with the changable part of the interface log fields
     *
     */
    public virtual Boolean fillAndSendERPDataWithManyInterfaceLogs(Map<String, Object> dataMap, String messageID, 
                    CCWSUtil u, Boolean testMode, SCReturnStruct rs, List<InterfaceLogPart> ilpList) 
    {
        String msg = 'Please overwrite the function SCInterfaceExportBase.fillAndSendERPDataWithManyInterfaceLogs in an appropriate subclass!';
        debug(msg);
        throw new SCfwException(msg);
    }
    

    /**
     * Sets the ERPStatus... in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public virtual void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, 
                    SCReturnStruct rs, Boolean error)
    {
        String msg = 'Please overwrite the function SCInterfaceExportBase.setERPStatus in an appropriate subclass!';
        debug(msg);
        throw new SCfwException(msg);
    }
    /**
     * checks if a mandatory field is filled
     *
     * @param prevRet the previous error msg
     * @param checkItem the item to be checked
     * @param fieldName the name of the field or another text describing the filed, that is missed 
     * 
     * @return the prevRet + current field missed
     */
    public String checkMandatory(String prevRet, String checkItem, String fieldName)
    {
        String retValue = prevRet;
        if(checkItem == null || checkItem == '')
        {
            if(prevRet != null && prevRet != '')
            {
                retValue += ', ';
            }
            retValue += fieldName;
        } 
        return retValue;
    }                
    
    public ID getInterfaceLogId(List<SCInterfaceLog__c> interfaceLogList, ID stockID)
    {
        ID retValue = null;
        for(SCInterfaceLog__c il: interfaceLogList)
        {
            if(il.Stock__c == stockID)
            {
                retValue = il.Id;
            }
        }
        return retValue;
    }
    
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}