/*
 * @(#)SCMaterialTransferExtensionTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for material transfer.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCMaterialTransferExtensionTest
{
    /**
     * testMaterialTransfer
     */
    static testMethod void testMaterialTransfer()
    {
        // first we create the test data, this will be
        // - stocks belonging to one plant
        // - articles
        // - business unit with stock1
        // - employees with their resource
        // - resource assigments for each employee
        // - stock items
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[0].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, 
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id, true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles,
                                             SCHelperTestClass.stocks[1].Id, 
                                             SCHelperTestClass.stocks[2].Id, false);
        SCHelperTestClass.stockItems[0].Qty__c = 5.0;
        SCHelperTestClass.stockItems[1].Qty__c = 5.0;
        SCHelperTestClass.stockItems[2].Qty__c = 5.0;
        SCHelperTestClass.stockItems[3].Qty__c = 5.0;
        insert SCHelperTestClass.stockItems;
        
        try
        {
            User curUser = [SELECT Id, ERPWorkCenterPlant__c FROM User WHERE Id = :UserInfo.getUserId()];
            curUser.ERPWorkCenterPlant__c = null;
            Database.update(curUser);
        }
        catch (Exception e)
        {
        }
        
        
        Test.startTest();
        
        ApexPages.StandardController con = new ApexPages.StandardController(SCHelperTestClass.stocks[1]);
        SCMaterialTransferExtension ext = new SCMaterialTransferExtension(con);
        System.assertEquals(false, ext.transferOk);
        
        List<SelectOption> typeList = ext.getTypeList();
        System.assertNotEquals(0, typeList.size());
        ext.matMoveType = typeList[0].getValue();
        
        List<SelectOption> receiverList = ext.getReceiverList();
        System.assertNotEquals(0, receiverList.size());
        ext.receiverStock = SCHelperTestClass.stocks[2].Id;
        
        System.assertNotEquals(0, ext.transInfoList.size());

        // start transfer without selection any entry        
        ext.transferMat();
        // we expect an error
        System.assertEquals(false, ext.transferOk);

        // start transfer with selection of the first entry, but no quantity
        ext.transInfoList[0].isSet = true;
        ext.transferMat();
        // we expect an error
        System.assertEquals(false, ext.transferOk);

        // start transfer with selection of the first entry, but wrong quantity
        ext.transInfoList[0].isSet = true;
        ext.transInfoList[0].qty = 10;
        ext.transferMat();
        // we expect an error
        System.assertEquals(false, ext.transferOk);

        // start transfer with selection of the first entry and correct quantity
        ext.transInfoList[0].isSet = true;
        ext.transInfoList[0].qty = 1;
        ext.transferMat();
        // we expect no error
        System.assertEquals(true, ext.transferOk);
        
        Test.stopTest();
        
        SCMaterialMovement__c matMove1 = [Select Id, Type__c, Qty__c 
                                            from SCMaterialMovement__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_OUT, matMove1.Type__c);
        System.assertEquals(1, matMove1.Qty__c);
        
        SCMaterialMovement__c matMove2 = [Select Id, Type__c, Qty__c 
                                            from SCMaterialMovement__c 
                                           where Stock__c = :SCHelperTestClass.stocks[2].Id];
        System.assertEquals(SCfwConstants.DOMVAL_MATMOVETYP_TRANSFER_IN, matMove2.Type__c);
        System.assertEquals(1, matMove2.Qty__c);
        
        SCStockItem__c stockItem1 = [Select Id, Qty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[1].Id 
                                             and Article__c = :SCHelperTestClass.articles[0].Id];
        System.assertEquals(4, stockItem1.Qty__c);
        
        SCStockItem__c stockItem2 = [Select Id, Qty__c 
                                            from SCStockItem__c 
                                           where Stock__c = :SCHelperTestClass.stocks[2].Id 
                                             and Article__c = :SCHelperTestClass.articles[0].Id];
        System.assertEquals(1, stockItem2.Qty__c);
    } // testMaterialTransfer
} // SCMaterialTransferExtensionTest