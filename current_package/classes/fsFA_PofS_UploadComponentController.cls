public class fsFA_PofS_UploadComponentController{
    public ID GV_ImageID{Get; Set;}
    public ID GV_FolderID{Get;Set;}
    public Boolean GV_ShowImage{Get;Set;}
    public String GV_DocumentURL{Get;Set;}
    
    public fsFA_PofS_UploadComponentController(){
        GV_DocumentURL = 'https://c.cs14.content.force.com/servlet/servlet.FileDownload?file=';   
    }
}