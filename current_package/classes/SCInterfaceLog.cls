/*
 * @(#)SCInterfaceLog.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/*
* Helper to insert interface log records in interfaces
*/
public class SCInterfaceLog
{
    public SCInterfaceLog__c interfaceLog;
    public String exceptionInfo;
    public static void logBatchInternal(String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        
        insert ic;
    }       

    public static void logBatchInternalExt(String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CONTRACT_VISIT')
        {
            ic.Contract__c = referenceID;
        }
        else if(interfaceName == 'CONTRACT_ORDER')
        {
            ic.Contract__c = referenceID;
            ic.Order__c = referenceID2;
        }
        else if(interfaceName == 'CONTRACT_ORDER_DISPATCH')
        {
            ic.Order__c = referenceID;
            ic.Contract__c = referenceID2;
        }
        else if(interfaceName == 'MAINTENANCE_VISIT')
        {
            ic.Maintenance__c = referenceID;
        }
        else if(interfaceName == 'MAINTENANCE_ORDER')
        {
            ic.Order__c = referenceID2;
            ic.Maintenance__c = referenceID;
        }
        insert ic;
    }       

    public static void logError(String interfaceName, String interfaceHandler,
                                Id accId, Id ordId, String resultCode, String resultInfo, 
                                String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'ERROR';
        ic.Count__c = count;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = start;
        ic.Account__c = accId;
        ic.Order__c = ordId;
        
        insert ic;
    }       



}