/*
 * @(#)SCOrderConfirmationController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the controller for simple hovers that can be used to 
 * display context sensitive information.
 * The page displays the order details after recording the order data.
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */

public with sharing class SCOrderConfirmationController
{
    public SCboOrder ord { get; private set; }
    public String orderRoleRecipient { get; private set; }
    public String orderRoleCaller { get; private set; }
    
    public String orderRolePayer { get; private set; }
    public String orderRoleInvoiceRecipient { get; private set; }
    public String product { get; private set; }
    public String employee { get; private set; }
    public SCInstalledBase__c orderItemInstalledBase { get; private set; }
    public SCTimeReport__c timeReportData { get; set; }
    public boolean appFound { get; set; }
    public String reload { get; set; }
    public Integer counter = 0;
    public String startDate { get; set; }
    public String irid { get; private set; }
    public String mode{get; set;}  
    public String itemHeader{get;set;}
    public String itemRemark{get;set;}  
    public ID oid{get; set; }    
    public List<SCAppointment__c> appointments { get; set; }
    public Boolean showPageHeader { get; set; }
    
    
    
    
    public String productSkill { get; set; }
    
    public String arrivalTime {get; private set;} 

    /*
     *  Reads the order information from the database.
     */
    public SCOrderConfirmationController()
    {
        // Read the parameter to switch VF Page title / sidebar on or off
        if(ApexPages.currentPage().getParameters().containsKey('showHeader'))
        {
            showPageHeader = Boolean.valueOf(ApexPages.currentPage().getParameters().get('showHeader'));
        }
        else
        {
            showPageHeader = true;
        }
        
        timeReportData = new SCTimeReport__c ();
        onInit();
    }
    
    private String formatRole(String strmobilePhone, String strphone, String strstreet, String strcity, String strpostal, String strhouseno, String strname, 
                              String strExtension, String strFloor, String strFlatNo, String strAccNumber)
    {
        String outputString = '';
        String mobilePhone = '';
        String phone = '';
        String street = '';
        String city = '';
        String postal = '';
        String houseno = '';
        String extension = '';
        String accNumber = '';
        String iname = strname;
        productSkill = '';
        
        
        if(strmobilePhone != null){
            mobilePhone = sObjectType.SCOrderRole__c.fields.MobilePhone__c.label + ': ' + strmobilePhone + ' ';
        }
        
        if(strphone != null){
            phone = sObjectType.SCOrderRole__c.fields.Phone__c.label + ': ' + strphone;
        }
        
        if(strstreet != null){
            street = strstreet;
        }
        
        if(strcity != null){
            city = strcity;
        }
        
        if(strpostal != null){
            postal = strpostal;
        }
        
        if(strhouseno != null){
            houseno = strhouseno;
        }
        
        if(strExtension != null){
            extension = strExtension;
        }
        
        if (null != strAccNumber)
        {
            accNumber = '(' + strAccNumber + ') ';
        }
        
        String brakline = '';
        
        if(strmobilePhone != null || strphone != null)
        {
            brakline = '<br/>';
        }
         
        outputString = accNumber + iname + ', ' + street + ' ' + houseno + extension + ' ';
        if (SCBase.isSet(strFloor))
        {
            outputString += strFloor;
        }
        if (SCbase.isSet(strFlatNo))
        {
            outputString += '/' + strFlatNo;
        }
        outputString += ', ' + postal + ' ' + city + ' ' + brakline + mobilePhone + phone;
        return outputString;
    }
    
    public PageReference onInit()
    {
        if (ApexPages.currentPage().getParameters().containsKey('oid'))
        {
            ID oid = ApexPages.currentPage().getParameters().get('oid');
            reload = ApexPages.currentPage().getParameters().get('reload');
            
            if(reload == 'false')
            {
                appFound = true;
            }

            ord = new SCboOrder();
            ord.readById(oid);
            
            // format the order roles view for recipient and caller
            for (Integer i = 0; i <= ord.allRole.size()-1; i++)
            {
                system.debug('#### current role: ' + ord.allRole[i].OrderRole__c);
                
                if (ord.allRole[i].OrderRole__c == '50301')
                {
                
                    system.debug('Service Recipient found');
                    
                    orderRoleRecipient = formatRole(ord.allRole[i].MobilePhone__c,
                                                    ord.allRole[i].Phone__c, 
                                                    ord.allRole[i].Street__c, 
                                                    ord.allRole[i].City__c, 
                                                    ord.allRole[i].PostalCode__c, 
                                                    ord.allRole[i].HouseNo__c, 
                                                    ord.allRole[i].Name1__c, 
                                                    ord.allRole[i].Extension__c, 
                                                    ord.allRole[i].Floor__c, 
                                                    ord.allRole[i].FlatNo__c, 
                                                    ord.allRole[i].Account__r.AccountNumber);
                                                    
                    
                }

                if (ord.allRole[i].OrderRole__c == '50302')
                {
                    system.debug('Caller found');
                     
                    orderRoleCaller    = formatRole(ord.allRole[i].MobilePhone__c,
                                                    ord.allRole[i].Phone__c, 
                                                    ord.allRole[i].Street__c, 
                                                    ord.allRole[i].City__c, 
                                                    ord.allRole[i].PostalCode__c, 
                                                    ord.allRole[i].HouseNo__c, 
                                                    ord.allRole[i].Name1__c, 
                                                    ord.allRole[i].Extension__c, 
                                                    ord.allRole[i].Floor__c, 
                                                    ord.allRole[i].FlatNo__c, 
                                                    ord.allRole[i].Account__r.AccountNumber);
                    
                    
                }
                
                if (ord.allRole[i].OrderRole__c == '50303')
                {
                    system.debug('IR found');
                     
                    orderRoleInvoiceRecipient = formatRole(ord.allRole[i].MobilePhone__c,
                                                           ord.allRole[i].Phone__c, 
                                                           ord.allRole[i].Street__c, 
                                                           ord.allRole[i].City__c, 
                                                           ord.allRole[i].PostalCode__c, 
                                                           ord.allRole[i].HouseNo__c, 
                                                           ord.allRole[i].Name1__c, 
                                                           ord.allRole[i].Extension__c, 
                                                           ord.allRole[i].Floor__c, 
                                                           ord.allRole[i].FlatNo__c, 
                                                           ord.allRole[i].Account__r.AccountNumber);
        
                    

                }
                
                if (ord.allRole[i].OrderRole__c == '50304')
                {
                    system.debug('Payer found');
                     
                    orderRolePayer = formatRole(ord.allRole[i].MobilePhone__c,
                                                ord.allRole[i].Phone__c, 
                                                ord.allRole[i].Street__c, 
                                                ord.allRole[i].City__c, 
                                                ord.allRole[i].PostalCode__c, 
                                                ord.allRole[i].HouseNo__c, 
                                                ord.allRole[i].Name1__c, 
                                                ord.allRole[i].Extension__c, 
                                                ord.allRole[i].Floor__c, 
                                                ord.allRole[i].FlatNo__c, 
                                                ord.allRole[i].Account__r.AccountNumber);
                                                
                    
                }
            }

            //for (SCboOrder__c order :ord)
            //{
            if (ord.boOrderItems.size() > 0)
            {
                System.debug('**** ord.boOrderItems[0]: '+ord.boOrderItems[0]);
            
                try
                {
                    orderItemInstalledBase = [select Id,
                                    SerialNo__c,
                                    SerialNoException__c,
                                    InstallationDate__c,
                                    GuaranteeUntil__c,
                                    CommissioningDate__c,
                                    CommissioningStatus__c,
                                    ConstructionWeek__c,
                                    ConstructionYear__c,
                                    Name,
                                    ID2__c,
                                    IdExt__c,
                                    Article__c,
                                    ArticleEAN__c,
                                    ArticleName__c,
                                    ArticleNo__c,
                                    Brand__c,
                                    Brand__r.Name,
                                    System__c,
                                    System__r.Name,
                                    ProductModel__c,
                                    ProductModel__r.Name,
                                    ProductModel__r.Group__c,
                                    ProductUnitClass__c,
                                    ProductUnitType__c,
                                    ProductGroup__c,
                                    ProductPower__c,
                                    ProductEnergy__c,
                                    ProductNameCalc__c,
                                    Type__c,
                                    Status__c,
                                    SafetyAction__c,
                                    Safety1__c,
                                    Safety2__c,
                                    toLabel(ProductSkill__c),
                                    SafetyNote__c,
                                    SafetyStatus__c,
                                    InstalledBaseLocation__c,
                                    InstalledBaseLocation__r.ID2__c,
                                    InstalledBaseLocation__r.GeoX__c,
                                    InstalledBaseLocation__r.GeoY__c,
                                    InstalledBaseLocation__r.GeoApprox__c,
                                    InstalledBaseLocation__r.LocName__c,
                                    InstalledBaseLocation__r.Address__c,
                                    InstalledBaseLocation__r.FlatNo__c,
                                    InstalledBaseLocation__r.Floor__c,
                                    InstalledBaseLocation__r.Street__c,
                                    InstalledBaseLocation__r.HouseNo__c,
                                    InstalledBaseLocation__r.Extension__c,
                                    InstalledBaseLocation__r.PostalCode__c,
                                    InstalledBaseLocation__r.City__c,
                                    InstalledBaseLocation__r.District__c,
                                    InstalledBaseLocation__r.County__c,
                                    InstalledBaseLocation__r.CountryState__c,
                                    InstalledBaseLocation__r.Country__c
                               from SCInstalledBase__c where Id =:ord.boOrderItems[0].orderItem.InstalledBase__c];
                }
                catch (Exception e)
                {
                    // no installed base founded
                    orderItemInstalledBase = null;
                }
                system.debug('### SCInstalledBase: '+ orderItemInstalledBase );
            }
            
            if ((ord != null) && (orderItemInstalledBase != null))
            {
                if (orderItemInstalledBase.ProductModel__c != null)
                {
                    product = orderItemInstalledBase.ProductNameCalc__c;
                }
                else if (orderItemInstalledBase.ProductUnitClass__c != null)
                {
                    product = SCBase.getPicklistText(SCInstalledBase__c.ProductUnitClass__c, 
                                                     orderItemInstalledBase.ProductUnitClass__c);
                }
                else if (orderItemInstalledBase.ProductUnitType__c != null)
                {
                    product = SCBase.getPicklistText(SCInstalledBase__c.ProductUnitType__c, 
                                                     orderItemInstalledBase.ProductUnitType__c);
                }
                
                if(orderItemInstalledBase.ProductSkill__c != null)
                {
                    productSkill = orderItemInstalledBase.ProductSkill__c;
                }

                appFound = false;
                searchAppointment();
            }
            else
            {
                appFound = true;
            }

            system.debug(ord);
        }
        
     return null;   
        
    }
    
    /*
     *  Try to read the appointment from the database.
     */
    private void searchAppointment()
    {    
        try
        {
            appointments = new List<SCAppointment__c>();
            
            if ((ord.order.Status__c != '5501') && (ord.appointments.size() > 0))
            {
                System.debug('### refreshEmployee part 2, ord: '+ord);
                System.debug('### appointment found');
                
                for (SCAppointment__c app :ord.appointments)
                {
                    if (SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED != app.Assignment__r.Status__c)
                    {
                        appointments.add(app);
                    }
                }
                
                /*
                assignment = [SELECT Id, Description__c,
                                                Employee__c,
                                                // EnableDownload__c,
                                                // EnablePrecheck__c,
                                                Followup1__c,
                                                Followup2__c,
                                                ID2__c,
                                                LastModifiedById,
                                                LastModifiedDate,
                                                Name,
                                                Order__c,
                                                Phone__c,
                                                Mobile__c,
                                                Resource__c,
                                                Responsible__c,
                                                Status__c
                                               FROM  SCAssignment__c
                                               WHERE Order__c = :ord.order.Id
                                               AND Status__c != '5507' ];
                */
    
                //employee = assignment.Employee__c + ', '+ ord.appointments[0].Start__c;
                // format the date dependent on user
                appFound = true;
                //timeReportData.Start__c = ord.appointments[0].Start__c;

                startDate = '';
                if(ord.appointments[0].Start__c != null)
                {
                    startDate = ord.appointments[0].Start__c.format();
                }
                
                this.arrivalTime = '';
                if(ord.appointments[0].ArrivalTimeFrom__c != null && ord.appointments[0].ArrivalTimeTo__c != null)
                {
                    DateTime aFrom = ord.appointments[0].ArrivalTimeFrom__c;
                    if(aFrom.time().hour() < 7)
                    {
                        aFrom = DateTime.newInstance(aFrom.date(), Time.newInstance(7,0,0,0));
                    }
                    DateTime aTo = ord.appointments[0].ArrivalTimeTo__c;
                    this.arrivalTime = aFrom.format('HH:mm');
                    this.arrivalTime += ' - ';
                    this.arrivalTime += aTo.format('HH:mm');
                    
                    
                }
            }
        }
        catch (Exception e)
        {
           System.debug('### appointment found');
           //appFound = true;
          // no appointment founded
        }
        
        counter++;
        if(counter > 1)
        {
            appFound = true;
        }
    }
    
    
}