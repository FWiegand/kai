/* 
 * @(#)CCWCOrderCloseEx.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 * CCWCOrderCloseEx
 * 
 * @author SS <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 *
 *
 * The entry methods are: 
 *      process(String oid)
 *      processNext(String oid)
 *      processNextAfterMoveMat(String oid) //if no call to SAP is created!
 */
global without sharing class CCWCOrderCloseEx
{
    private static SCOrder__c order;
    
    /**
     * readOrder - get the order from the database.
     * Set the global variable "order"
     * @param oid -> OrderNumber of the order
     * @return    -> TRUE - There was an order-entry in the database
     **/
    private static Boolean readOrder(String oid)
    {
        List<SCOrder__c> listOrder = [SELECT Id, ERPOrderNo__c, ERPStatusOrderCreate__c, ERPStatusEquipmentUpdate__c,
                                             ERPStatusMaterialReservation__c, ERPStatusMaterialMovement__c,
                                             ERPStatusExternalAssignmentAdd__c, ERPStatusExternalAssignmentRem__c,
                                             ERPStatusOrderClose__c, ERPStatusArchiveDocumentInsert__c,
                                             ERPAutoOrderClose__c, Name, Status__c 
                                        FROM SCOrder__c
                                       WHERE Id = :oid];
        
        if ((listOrder != null) && (listOrder.size() > 0))
        {
            order = listOrder.get(0);
            return true;
        }
        return false;
    }//readOrder
    
    /**
     * controlOrder - Controls the Properties of the order
     * @param oid -> OrderNumber of the order
     * @return    +-> TRUE - The next callout to SAP can be started
     *            +-> FALSE - The next callout to SAP can NOT be started
     **/
    private static Boolean controlOrder()
    {
        if (order == null) { return false; }
        
        try
        {
            Set<String> setStatusAccept = new Set<String>();
            
            if (SCfwConstants.DOMVAL_ORDERSTATUS_FOR_ORDER_CLOSE_EX != null)
            {
                for (String status : SCfwConstants.DOMVAL_ORDERSTATUS_FOR_ORDER_CLOSE_EX.split(','))
                {
                    if (status != null) { setStatusAccept.add(status); }
                }
            }
            
            
            if (   (setStatusAccept.contains(order.Status__c))
                && (order.ERPAutoOrderClose__c != null)    && (order.ERPAutoOrderClose__c.startsWith('S'))
                && (order.ERPOrderNo__c != null)           && (order.ERPOrderNo__c != '')
                && (order.ERPStatusOrderCreate__c != null) && (order.ERPStatusOrderCreate__c.equalsIgnoreCase('ok'))
                && (   (order.ERPStatusEquipmentUpdate__c == null)
                    || (order.ERPStatusEquipmentUpdate__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusEquipmentUpdate__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusMaterialReservation__c == null)
                    || (order.ERPStatusMaterialReservation__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusMaterialReservation__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusMaterialMovement__c == null)
                    || (order.ERPStatusMaterialMovement__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusMaterialMovement__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusExternalAssignmentAdd__c == null)
                    || (order.ERPStatusExternalAssignmentAdd__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusExternalAssignmentAdd__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusExternalAssignmentRem__c == null)
                    || (order.ERPStatusExternalAssignmentRem__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusExternalAssignmentRem__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusOrderClose__c == null)
                    || (order.ERPStatusOrderClose__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusOrderClose__c.equalsIgnoreCase('ok')) )
                && (   (order.ERPStatusArchiveDocumentInsert__c == null)
                    || (order.ERPStatusArchiveDocumentInsert__c.equalsIgnoreCase('none'))
                    || (order.ERPStatusArchiveDocumentInsert__c.equalsIgnoreCase('ok')) )
               )
            {
                System.debug('+++###+++ CCWCOrderCloseEx_controlOrder is true; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
                
                return true;
            }
        }
        catch (Exception e)
        {
            System.debug('+++###+++ CCWCOrderCloseEx_controlOrder has an Error; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp() + '; Exception: ' + e);
            
            return false;
        }
        
        System.debug('+++###+++ CCWCOrderCloseEx_controlOrder is false; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
            
        return false;
    }//controlOrder
    
    
    
    
    /**
     * process - Starts the automatic procedure for closing the order in SAP
     * The status-field of the order has to be equal to '5508' (closed final; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     * @return    -> TRUE - The necessary properties were set successfully
     **/
    public static Boolean processForceClose(String oid)
    {
        if (readOrder(oid))
        {
            if ((order.ERPAutoOrderClose__c != null) && (order.ERPAutoOrderClose__c.equalsIgnoreCase('Finished')))
            {
                return false;
            }
            
            if ((order.ERPAutoOrderClose__c == null) || (order.ERPAutoOrderClose__c.equalsIgnoreCase('Inactive')))
            {
                order.ERPAutoOrderClose__c = 'S0-Active';
                //GMSGB & GMSET moved to SCboAppintment.updateOrderStatus order.InvoicingReleased__c = true;    // the item can be released if the orderstatus is completed
                //GMSGB & GMSET moved to SCboAppintment.updateOrderStatus order.InvoicingStatus__c = '52402';   // ready for invoicing
                Database.update(order);
            }
            
            processNext(oid);
            
            return true;
        }
        return false;
    }
    
    
    
    /**
     * process - Starts the automatic procedure for closing the order in SAP
     * The status-field of the order has to be equal to '5508' (closed final; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     * @return    -> TRUE - The necessary properties were set successfully
     **/
    public static Boolean process(String oid)
    {
        if (readOrder(oid))
        {
            if ((order.ERPAutoOrderClose__c != null) && (order.ERPAutoOrderClose__c.equalsIgnoreCase('Finished')))
            {
                return false;
            }
            
            if ((order.ERPAutoOrderClose__c == null) || (order.ERPAutoOrderClose__c.equalsIgnoreCase('Inactive')))
            {
                order.ERPAutoOrderClose__c = 'S0-Active';
                Database.update(order);
            }
            
            processNext(oid);
            
            return true;
        }
        return false;
    }
    
    /**
     * processNext - Continues the automatic procedure for closing the order in SAP
     * The status-field of the order has to be equal to '5508' (closed final; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     **/
    public static void processNext(String oid)
    {
        if (order == null)
        {
            readOrder(oid);
        }//order == null
        
        System.debug('+++###+++ CCWCOrderCloseEx_processNext before controlorder; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        
        //order != null and no SAP-Job is running or in Error
        if (controlOrder())
        {
            System.debug('+++###+++ CCWCOrderCloseEx_processNext after controlorder; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
            //Step 1 - Order Create was successful
            if (   (order.ERPAutoOrderClose__c != null)
                && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S0-Active')) )
            {
                processStep1UpdateOrder();
            }
            
            //Step 2 - Order Update was successful
            else if (   (order.ERPAutoOrderClose__c != null)
                     && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S1-Update')) )
            {
                processStep2MoveMat();
            }
            
            //Step 3 - Move Material was successful
            else if (   (order.ERPAutoOrderClose__c != null)
                     && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S2-MoveMat')) )
            {
                processStep3OrderClose(false);
            }
            
            //Step 4 - Order Close was successful
            else if (   (order.ERPAutoOrderClose__c != null)
                     && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S3-OrderClose')) )
            {
                processStep4Archive();
            }
            
            //Step 5 - Archive was successful
            else if (   (order.ERPAutoOrderClose__c != null)
                     && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S4-Archive')) )
            {
                processStep5Finished();
            }
        }//order != null and no SAP-Job is running or in Error
    }//processNext
    
    /**
     * processNextAfterMoveMat - Continues the automatic procedure for closing the order in SAP
     * This method is called from "CCWCMaterialMovementCreate", if no call is created.
     * The status-field of the order has to be equal to '5508' (closed final; SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL)
     * @param oid -> OrderNumber of the order
     **/
    public static void processNextAfterMoveMat(String oid) //if no call is created!
    {
        if (order == null)
        {
            readOrder(oid);
        }//order == null
        
        System.debug('+++###+++ processNextAfterMoveMat before control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        
        //GMSSS: comment because we get an error while closing the order in SAP
        //order != null and no SAP-Job is running or in Error
        if (   (controlOrder())
            && (order.ERPAutoOrderClose__c != null)
            && (order.ERPAutoOrderClose__c.equalsIgnoreCase('S2-MoveMat')))
        {
            System.debug('+++###+++ processNextAfterMoveMat after control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
            processStep3OrderClose(true);
        }
    }//processNextAfterMoveMat
    
    
    
    
    
    /**
     * processStep1UpdateOrder - Starts the SAP-Job "Update Order"
     **/
    private static void processStep1UpdateOrder()
    {
        System.debug('+++###+++ processStep1UpdateOrder control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        if (   (order.ERPOrderNo__c != null)           && (order.ERPOrderNo__c != '')
            && (order.ERPStatusOrderCreate__c != null) && (order.ERPStatusOrderCreate__c.equalsIgnoreCase('ok')) )
        {
            order.ERPAutoOrderClose__c = 'S1-Update';
            Database.update(order);
            
            System.debug('+++###+++ processStep1UpdateOrder start; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
            CCWCOrderEquipmentUpdate.callout(order.id, true, Test.isRunningTest());
        }
    }//processStep1UpdateOrder
    
    /**
     * processStep1UpdateOrder - Starts the SAP-Job "Move Material"
     **/
    private static void processStep2MoveMat()
    {
        System.debug('+++###+++ processStep2MoveMat control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        
        if ((order.ERPStatusEquipmentUpdate__c != null) && (order.ERPStatusEquipmentUpdate__c.equalsIgnoreCase('ok')))
        {
            order.ERPAutoOrderClose__c = 'S2-MoveMat';
            Database.update(order);
            
            //List<SCMaterialMovement__c> listMatMove = CCWCMaterialMovementCreate.getMMbyOrder(order.id);
            
            //if ((listMatMove != null) && (listMatMove.size() > 0))
            //{
                System.debug('+++###+++ processStep2MoveMat start; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
                CCWCMaterialMovementCreate.callout(order.id, true, Test.isRunningTest());
            //}
            //else
            //{
            //    processStep3OrderClose(true);
            //}
        }
    }//processStep2MoveMat
    
    /**
     * processStep3OrderClose - Starts the SAP-Job "Order Close"
     **/
    //private static void processStep3OrderClose(Boolean makeCallout)
    private static void processStep3OrderClose(Boolean isInFutureCall)
    {
        System.debug('+++###+++ processStep3OrderClose isInFutureCall: ' + isInFutureCall + '; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        
        if (   false//(makeCallout)
            || (   (isInFutureCall == false)
                && (order.ERPStatusMaterialMovement__c != null)
                && (order.ERPStatusMaterialMovement__c.equalsIgnoreCase('ok')))
            || (   (isInFutureCall == true)
                && (order.ERPStatusEquipmentUpdate__c != null)
                && (order.ERPStatusEquipmentUpdate__c.equalsIgnoreCase('ok')) )  )
        {
            if (isInFutureCall)
            {
                System.debug('+++###+++ processStep3OrderClose start synchron; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
                CCWCOrderClose.callout(order.id, false,  Test.isRunningTest());
            }
            else
            {
                System.debug('+++###+++ processStep3OrderClose start assynchron; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
                CCWCOrderClose.callout(order.id, true,  Test.isRunningTest());
            }
            
            readOrder(order.Id);
            order.ERPAutoOrderClose__c = 'S3-OrderClose';
            Database.update(order);
        }
    }//processStep3OrderClose
    
    /**
     * processStep4Archive - Starts the SAP-Job "Archive Document"
     **/
    private static void processStep4Archive()
    {
        System.debug('+++###+++ processStep4Archive control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        
        if ((order.ERPStatusOrderClose__c != null) && (order.ERPStatusOrderClose__c.equalsIgnoreCase('ok')))
        {
            order.ERPAutoOrderClose__c = 'S4-Archive';
            Database.update(order);
            
            System.debug('+++###+++ processStep4Archive start; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
            CCWCArchiveDocumentInsert.calloutOid(order.id, true, Test.isRunningTest());
        }
    }//processStep4Archive 
    
    /**
     * processStep5Finished - Ends the AutoCloseOrder
     **/
    private static void processStep5Finished()
    {
        System.debug('+++###+++ processStep5Finished control; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        if ((order.ERPStatusArchiveDocumentInsert__c != null) && (order.ERPStatusArchiveDocumentInsert__c.equalsIgnoreCase('ok')))
        {
            order.ERPAutoOrderClose__c = 'Finished';
            Database.update(order);
            
            System.debug('+++###+++ processStep5Finished end; ERPAutoOrderClose__c: ' + order.ERPAutoOrderClose__c + '; TimeStamp__c: ' + createTimeStamp());
        }
    }//processStep5Finished
    
    private static String createTimeStamp()
    {
        DateTime timeNow = Datetime.now();
        String timeStamp = timeNow.yearGMT() + '/' + timeNow.monthGMT() + '/' + timeNow.dayGMT() + ' ';
        timeStamp += (timeNow.hourGMT() < 10 ? '0' : '') + timeNow.hourGMT();
        timeStamp += ':' + (timeNow.minuteGMT() < 10 ? '0' : '') + timeNow.minuteGMT();
        timeStamp += ':' + (timeNow.secondGMT() < 10 ? '0' : '') + timeNow.secondGMT();
        timeStamp += ',' + (timeNow.millisecondGMT() < 10 ? '0' : '') + (timeNow.millisecondGMT() < 100 ? '0' : '') + timeNow.millisecondGMT();
        return timeStamp;
    }
}