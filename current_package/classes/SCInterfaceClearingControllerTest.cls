/**
 * @(#)SCInterfaceClearingControllerTest
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCInterfaceClearingControllerTest {

    static testMethod void controllerTest() {
        
        SCHelperTestClass3.createCustomSettings('DE',true);
    	
    	SCHelperTestClass.createTestUsers(true);
    	//SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
    	
    	SCHelperTestClass.createDomsForOrderCreation();
    	SCHelperTestClass.createOrderTestSet3(true);
    	
    	SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, false);
    	
    	
        SCInterfaceClearing__c clearing = new SCInterfaceClearing__c(Resource__c = SCHelperTestClass.resources.get(1).Id, Type__c = 'timereport', Status__c = SCboInterfaceClearing.STATUS_CHECK); 
    	insert clearing;
    	
    	Boolean dayStart = false;
    	Boolean dayEnd = false;
    		
    	for(SCTimeReport__c report : SCHelperTestClass2.timeReportDay1 )
    	{
    		if(report.Resource__c == SCHelperTestClass.resources.get(1).Id && report.Type__c != null)
    		{
    			//allow only one daystart and one dayend
    			if(report.Type__c.equals(SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART))
    			{
    				if(dayStart)
    				{
    					continue;	
    				}
    				dayStart = true;
    			}
    			else if(report.Type__c.equals(SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND))
    			{
    				if(dayEnd)
    				{
    					continue;	
    				}
    				dayEnd = true;
    			}
    			
    			Attachment att = new Attachment(parentId=clearing.Id, Name='unitTest', Description = 'I');
		    	SCboInterfaceClearing.saveObjectToAttachment(att,report);
    		}
    	}
    	
    	Test.startTest();
    	
    	//Handle Clearing Items
    	SCInterfaceClearingController c = new SCInterfaceClearingController();
        system.assert(c.getClearingList().size() > 0,'Clearing list is empty');
        
        c.selectedClearingItemId = clearing.Id;
        
        c.onSelectClearingItem();
        
        system.assert(c.selectedClearingItem != null,'Clearing Item not found');
        c.onValidate();
        system.assert(c.isValid,'Clearing Item not valid');
        c.onRefreshSelection();
        
        c.onSave();
        
        
        clearing.Status__c = SCboInterfaceClearing.STATUS_CHECK;
        update clearing;
        
        //Delete Clearing Items
        c = new SCInterfaceClearingController();
        
        c.selectedClearingItemId = clearing.Id;
        c.onSelectClearingItem();
        
        Integer attSize = c.attachmentList.size();
        c.attToDeleteId = c.attachmentList.get(0).Id;
        c.onDeleteAttachment();
        system.assert(c.attachmentList.size() < attSize,'Attachment not deleted');
        
        Integer cSize = c.getClearingList().size();
        c.onDelete();
        system.assert(c.getClearingList().size() < cSize,'Clearing Item not deleted');
        
        c.onDeleteAll();
        
        //For Codecoverage only
        Map<String,Boolean> tmpAttValMap = c.attValidationMap;
        Boolean showError = c.showTopErrorMessage;
        c.onCancel();
    	
    	Test.stopTest();
        
    }
}