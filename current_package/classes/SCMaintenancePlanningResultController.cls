/*
 * @(#)SCMaintenancePlanningResultController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller shows the results of the maintenance.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 public with sharing class SCMaintenancePlanningResultController
{  
    public String contractScheduleId { get; set; } 
    public List<SCContractSchedule__c> contractSchedule { get; set; }
    public SCContractSchedule__c cons { get; set; }
    public String cName { get; set; }
    public String cStatus { get; set; }
    public Boolean canCancelAppointments { get; set; }
    public List<String> cancellableAppointments;
    public List<SCAppointment__c> apps;
    public List<ContractScheduleItem> items { get; set; }
    public String selectedProcess { get; set; }
    public Boolean isError { get; set; }
    public Integer allItems { get; set; }
    public Integer readyItems { get; set; }
    public String selectedFilter { get; set; }
    
    public ApexPages.StandardSetController resultsSetController { get; set; }
    
    // Planned informations calculated by apex
    public Integer numberOfSMOrders { get; set; }
    public Integer capacitySMOrders { get; set; }
    public Integer numberOfSMEngineers { get; set; }
    public Integer numberOfCMOrders { get; set; }
    public Integer capacityCMOrders { get; set; }
    public Integer numberOfCMEngineers { get; set; }
    public Integer workHours { get; set; }
    
    // The information from ASE
    public Integer result1 { get; set; }
    public Integer result2 { get; set; }
    public Integer result3 { get; set; }

    public SCMaintenancePlanningResultController() 
    {
        selectedProcess = '';
        selectedFilter = 'all';
        
        init(true);
    }
    
    public void init(Boolean takeIdFromURL)
    {
        //readyItems = 0;
        allItems = 0;
        
        cons = new SCContractSchedule__c();
        
        isError = false;
        
        if(takeIdFromURL)
        {
            if(ApexPages.currentPage().getParameters().containsKey('id'))
            {
                contractScheduleId = ApexPages.currentPage().getParameters().get('id');
                
                System.debug('#### id: ' + contractScheduleId);
                
                try
                {
                    cons = [ Select Id, Name, ProcessStatus__c, PlannedCapacity__c, ResultCapacity__c, 
                                    (Select Id, Status__c, Info__c From ContractScheduleItems__r) 
                             From SCContractSchedule__c Where Id = :contractScheduleId ];
                             
                    cName = cons.name;
                    cStatus = cons.ProcessStatus__c;
                    System.debug('#### cs: ' + cons);
                    allItems = cons.ContractScheduleItems__r.size();
                    
                    // Planned capacity
                    if(SCBase.isSet(cons.PlannedCapacity__c))
                    {
                        List<String> plannedNumbers = (cons.PlannedCapacity__c).split(';');
                        
                        numberOfSMOrders    = Integer.valueOf(plannedNumbers[0]);
                        capacitySMOrders    = Integer.valueOf(plannedNumbers[1]);
                        numberOfSMEngineers = Integer.valueOf(plannedNumbers[2]);
                        numberOfCMOrders    = Integer.valueOf(plannedNumbers[3]);
                        capacityCMOrders    = Integer.valueOf(plannedNumbers[4]);
                        numberOfCMEngineers = Integer.valueOf(plannedNumbers[5]);
                        workHours           = Integer.valueOf(plannedNumbers[6]);
                    }
                    else
                    {
                        numberOfSMOrders    = 0;
                        capacitySMOrders    = 0;
                        numberOfSMEngineers = 0;
                        numberOfCMOrders    = 0;
                        capacityCMOrders    = 0;
                        numberOfCMEngineers = 0;
                        workHours           = 0;
                    }
                    
                    // Result capacity
                    result1 = 0;
                    result2 = 0;
                    result3 = 0;
                    
                    getCon();
                                        
                }
                catch(Exception e)
                {
                    isError = true;
                    System.debug('#### ID is not an Salesforce ID: ' + e);
                }
            }
            else
            {
                isError = true;
            }
        }
        else
        {
            contractScheduleId = selectedProcess;
            
            System.debug('#### id: ' + contractScheduleId);
            
            try
            {
                cons = [ Select Id, Name, ProcessStatus__c From SCContractSchedule__c Where Id = :contractScheduleId ];
                cName = cons.name;
                cStatus = cons.ProcessStatus__c;
                System.debug('#### cs: ' + cons);
            }
            catch(Exception e)
            {
                isError = true;
                System.debug('#### ID is not an Salesforce ID: ' + e);
            }
        }
    }
    
    public PageReference reloadSetController()
    {
        getCon();
        
        return null;
    }
    
    public ApexPages.StandardSetController getCon()
    {
        readyItems = 0;
        
        // First getting current status of the process
        cons = [ Select Id, Name, ProcessStatus__c, (Select Id, Status__c, Info__c From ContractScheduleItems__r) 
                 From SCContractSchedule__c Where Id = :contractScheduleId ];
        cStatus = cons.ProcessStatus__c;
            
        for(SCContractScheduleItem__c i : cons.ContractScheduleItems__r)
        {
            if(i.Status__c != null || i.Info__c != null)
                readyItems++;
        }       
                
        // Now reading items depending on the prozess id
        String filter = '';
        if(!SCBase.isSet(selectedFilter))
            selectedFilter = 'all';
        if(selectedFilter == 'all')
            filter = ' AND (Info__c != null OR Status__c != null) ';          
        if(selectedFilter == 'success')
            filter = ' AND Status__c = \'Success\' ';
        if(selectedFilter == 'info')
            filter = ' AND Status__c = \'Info\' ';
            
        String query = ' Select Id, Name, CreatedDate, BulkId__c, MaintenanceType__c, ContractSchedule__c,  ';
        query += ' SchedulingDateFrom__c, Resource__c, Status__c, Info__c, Duration__c,  ';
        query += ' SchedulingDateTo__c, Sort__c,  ';
        query += ' Contract__c, Contract__r.Id, Contract__r.Account__r.Id, Contract__r.Account__r.Name2__c,  ';
        query += ' Contract__r.Account__r.Name, Contract__r.Account__r.BillingStreet,  ';
        query += ' Contract__r.Account__r.BillingHouseNo__c, Contract__r.Account__r.BillingCity,  ';
        query += ' Contract__r.Account__r.BillingPostalCode,  ';
        query += ' Contract__r.Account__r.Phone, Contract__r.Account__r.Mobile__c, Contract__r.BulkId__c,  ';
        query += ' Contract__r.Name, ';
        query += ' Order__c, Order__r.Id, Order__r.BulkId__c, Order__r.Name, Order__r.Status__c, ';
        query += ' AppointmentID2__c ';
        query += ' From SCContractScheduleItem__c ';
        query += ' Where ContractSchedule__c = :contractScheduleId  ';
        query += ' ' + filter + ' ';
        query += ' Order By Sort__c asc  ';
        
        System.debug('#### query: ' + query);
        
        // Now creating set controller for pagination and output list
        resultsSetController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        resultsSetController.setPageSize(50);
        
        return resultsSetController;
    }
    
    /**
     * Calcels appointments 
     */
    public PageReference cancelAppointments()
    {
        System.debug('#### cancellableAppointments: ' + cancellableAppointments);
        
        if(!cancellableAppointments.isEmpty())
        {
            //AseSetStatus ase = new AseSetStatus();
            
            String id2s = '';
            
            for(String s : cancellableAppointments)
            {
                id2s += s + ',';    
            }
            
            AseSetStatus.callout(id2s,'','5507');
            
            return reloadPage();
        }
    
        return null;
    }
    
    
    public Class ContractScheduleItem
    {
        public SCContractScheduleItem__c constractScheduleItem { get; set; }
        public SCAppointment__c appointment { get; set; }
        public Boolean isCancellable { get; set; }
        
        contractScheduleItem(SCContractScheduleItem__c constractScheduleItem, SCAppointment__c appointment, Boolean isCancellable)
        {
            this.constractScheduleItem = constractScheduleItem;
            this.appointment = appointment;
            this.isCancellable = isCancellable;
        }
    }
    
    public List<SelectOption> getSelectionFilter()
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('all',System.Label.SC_flt_All));
        options.add(new SelectOption('success',System.Label.SC_app_Success));
        options.add(new SelectOption('info',System.Label.SC_app_WithoutAppointment));
        
        return options;
    }
    
    public List<ContractScheduleItem> getContractScheduleItem()
    {
        
        // Reading result capacity
        String rCap = [ Select ResultCapacity__c From SCContractSchedule__c Where Id = :contractScheduleId ].ResultCapacity__c;
        if(SCBase.isSet(rCap))
        {
            List<String> resultNumbers = rCap.split(',');
            result1 = Integer.valueOf(resultNumbers[0]);
            result2 = Integer.valueOf(resultNumbers[1]);
            result3 = Integer.valueOf(resultNumbers[2]);
        }
        
        List<SCContractScheduleItem__c> contractSchedule = (List<SCContractScheduleItem__c>)resultsSetController.getRecords();
                                                  
        System.debug('#### contractSchedule: ' + contractSchedule);
        System.debug('#### contractSchedule.size(): ' + contractSchedule.size());
        
        // Appointments ID2 list                                          
        List<String> appId2 = new List<String>();
        
        for(SCContractScheduleItem__c item : contractSchedule)
        {
            if(item.AppointmentID2__c != null)
            {
                appId2.add(item.AppointmentID2__c);
            }
        }
        
        System.debug('#### appId2: ' + appId2);
        
        
        // Reading appointments depending on the list with ID2's
        List<SCAppointment__c> apps = [ Select Id, Name, Assignment__c, Phone__c, Mobile__c, Employee__c, 
                                                Completed__c, Description__c, CustomerPrefStart__c, 
                                                CustomerPrefEnd__c, CustomerTimewindow__c, End__c, 
                                                Start__c, Type__c, AssignmentStatus__c, 
                                                OrderItem__c, Order__c, Resource__c,
                                                Assignment__r.Status__c, Id2__c
                                         From   SCAppointment__c
                                         Where  ID2__c IN :appId2 ];
                                         
        System.debug('#### apps: ' + apps);
        
        // Putting appointments to the map
        Map<String, SCAppointment__c> appsMap = new Map<String, SCAppointment__c>();
        
        for(SCAppointment__c a : apps)
        {
            appsMap.put(a.Id2__c, a);
        }
        
        System.debug('#### appsMap: ' + appsMap);
        
        
        // Generating the output list
        items = new List<ContractScheduleItem>();
        
        Integer cancelledApps = 0;
        cancellableAppointments = new List<String>();
        
        for(SCContractScheduleItem__c item : contractSchedule)
        {
            // Cancelled appointments
            Boolean canCancel = true;
            
            if(appsMap != null && appsMap.size() > 0)
            {
                String assStatus = '';
                try
                {
                    assStatus = appsMap.get(item.AppointmentID2__c).Assignment__r.Status__c;
                }
                catch(Exception e)
                {
                    System.debug('#### Status is null: ' + e);
                }
                
                
                if(assStatus == '5505' || assStatus == '5506' || assStatus == '5507' || assStatus == '')
                {
                    canCancel = false;
                    cancelledApps++;
                }
                else
                {
                    cancellableAppointments.add(item.AppointmentID2__c);
                    System.debug('#### canncelable appointment added: ' + item.AppointmentID2__c);
                }
                
            }
            
            // Collecting objects
            
            // Now reading items depending on the prozess id
            items.add( new contractScheduleItem(item, appsMap.get(item.AppointmentID2__c), canCancel) );
            
        }
        
        if(cancelledApps == contractSchedule.size() || apps.isEmpty())
        {
            canCancelAppointments = false;
        }
        else
        {
            canCancelAppointments = true;
        }
        
        System.debug('#### cancellableAppointments: ' + cancellableAppointments);
        
        System.debug('#### items: ' + items);
        
        return items;
    }
    
    /**
     * Read last contract schedulings of the user
     * @return    List of options for the combobox
     */ 
    public List<SelectOption> getLastProcessOfUser()
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',System.Label.SC_flt_None));
        
        try
        {
            List<SCContractSchedule__c> cs = [ Select Id, Name, CreatedDate, ProcessStatus__c
                                               From SCContractschedule__c 
                                               Where CreatedById = :UserInfo.getUserId()
                                               Order By CreatedDate desc Limit 10 ];
            if(cs != null && !cs.isEmpty())
            {
                for(SCContractSchedule__c c :cs)
                {
                    options.add( new SelectOption(c.Id,c.Name + ' (' + c.CreatedDate + ', ' + c.ProcessStatus__c + ')') );
                }
            }
        }
        catch(Exception e)
        {
            
        }
        
        return options;
    }
    
    public PageReference reloadPage() 
    {     
        
        PageReference pageRef;
        
        if(selectedProcess != null && selectedProcess.trim() != '')
        {
            contractScheduleId = selectedProcess;
            init(false);
            return null;
            
            //pageRef = new PageReference('/apex/SCMaintenancePlanningResult?id=' + selectedProcess);
            //return pageRef.setRedirect(true);
        }
        else
        {
            return null;
        }
        
    }

    
}