/*
 * @(#)WSMaterialDelivery.cls - implements the internal booking delivered spare parts
 * 
 * Copyright (c) 2012 GMS Development GmbH - all rights reserved - www.gms-online.de
 */
 
/** 
 * WSMaterialDelivery
 * When spare parts (ordered for service orders or during replenishment) are delivered,
 * this interface can be used to process the delivery note and book the material movements 
 * into the stock items.
 *
 * Pre-requisites: plant, stock and article must exist (or they are ignored)
 * 
 * Processing cases
 * 
 * Requisition     Stock item    Action Stock Item                         Action on existing material movement (order line and order or replenishment movement)
 * -------------------------------------------------------------------------------------------------------------
 * yes             yes           add delivered qty to stock item qty       set status to delivered
 * yes             no            create new stock item with delivered qty  set status to delivered
 * no              yes           add delivered qty to stock item qty       -  
 * no              no            create new stock item with delivered qty  - 
 *
 * TEST
 * - plant, stock or article does not exist
 *
 * @author GMSJP, GMSET, GMSNA  2012
 */
global without sharing class WSMaterialDelivery
{
    //-------------------------------------------------------------------------
    // API getConfiguration - Returns domainvalues and their translations
    //-------------------------------------------------------------------------    

    /** 
     * Web service that can be called to book a delivery to a stock
     * @param mode     BOOK - SCStockItem__c.qty__c will be modified 
     *                      depending on the custom setting MATERIAL_ENABLE_DELIVCONF__c
     *                      true: material movement status is set to "in transit" qty is not modified
     *                      false: material movement status is set to "delivered" and qty is updated
     *                 TEST - test mode - does not create any data
     * @return array of material movements with the resultstatus and resultinfo fields set
     */
    WebService static List<DeliveryData> process(String mode, List<DeliveryData> data)
    {
        // try to create a critical section (by select for update in SCSemaphore__c)    
        SCSemaphore__c sem = SCutilSemaphore.lock('SCStockItem__c');
        
            SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
            Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
            List<DeliveryData> result = processinternal(mode, data, enableDeliveryConfirmation);
        
        SCutilSemaphore.unlock(sem); // just update the status - when we exit the web service the for update lock is released 
        
        return result;
    }
     
    // internal helper - for test class implementation required     
    public static List<DeliveryData> processinternal(String mode, List<DeliveryData> data, Boolean enableDeliveryConfirmation)
    {            
        Set<String> ids = new Set<String>();
        
        //--<first extract the material movement ids>--------------------------
        for(DeliveryData d : data)
        {
            if(d.matmoveid != null)
            {
                ids.add(d.matmoveid);
            }
        }     
        //--<now read the material movements and related information>----------
        Map<ID, SCMaterialMovement__c> movements = new Map<ID, SCMaterialMovement__c>(
            [SELECT Id, Article__c, Assignment__c, DeliveryDate__c, DeliveryNote__c, 
            DeliveryQty__c, DeliveryStock__c, ID2__c, ListPrice__c, Name, Replenishment__c, 
            Order__c, OrderLine__c, OrderRole__c, OwnerId, Plant__c, PrimeCost__c, 
            ProcessDate__c, Qty__c, ReplenishmentType__c, RequisitionDate__c, 
            Status__c, Stock__c, Type__c, DelivAddrCity__c, DelivAddrCountry__c, DelivAddrCounty__c, 
            DelivAddrExtension__c, DelivAddrHouseNo__c, DelivAddrName__c, DelivAddrPostalCode__c, 
            DelivAddrCountryState__c, DelivAddrStreet__c, ReturnInfo__c, ReturnNo__c, SerialNo__c, 
            Order__r.MaterialStatus__c, OrderLine__r.MaterialStatus__c
            FROM SCMaterialMovement__c where id in :ids]);
            //do not read the valuationtype of the original requisition as we use the delivery valuation type!

        //--<we use the valuation type of the delivery not of the original requisition>-----
        for(DeliveryData d : data)
        {
            SCMaterialMovement__c m = movements.get(d.matmoveid);
            if(m != null)
            {
                // the valuation type of the request is ignored as the spares team decides which item to deliver
                // the valuation type is used to identify the correct stock item
                m.ValuationType__c = d.valuationtype;
            }
        }     


        //--<process the material movement update>-----------------------------
        Set<String> oids = new Set<String>();        // order ids - required to update the material status
        Set<String> olids = new Set<String>();       // order line ids - required to update the material status 
        Set<String> replids = new Set<String>();     // material movement (replenishment) - required to update the material status   
        
        // used to update the old material movement status (requisition) 
        // update GB 06.12.12 Changed List to Set         
        Set<SCMaterialMovement__c> origDeliveredSet = new Set<SCMaterialMovement__c>();
        // used to create the delivery material movements
        List<SCMaterialMovement__c> respMovementList = new List<SCMaterialMovement__c>();
        // used to identify the stock item by article and stock and valuation type
        Map<String, SCStockItem__c> stockArticleIdToStockItemMap = new Map<String, SCStockItem__c>();
        
        //ET: 07.09.2012
        // used to create the delivery material movements
        List<SCMaterialMovement__c> respMovementListWithoutId = new List<SCMaterialMovement__c>();
        
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            stockArticleIdToStockItemMap = readStockItems(movements);
        }

        // now iterate each of the delivered items and prepare the material movements        
        for(DeliveryData d : data)
        {
            if(d.matmoveid != null)
            {
                // now start with the material movement (requisition)
                SCMaterialMovement__c origMovement = movements.get(d.matmoveid);
                // extract the order and order line ids to be able to update the material status 
                if(origMovement.order__c != null)
                {
                    oids.add(origMovement.order__c);
                }
                if(origMovement.orderline__c != null)
                {
                    olids.add(origMovement.orderline__c);
                }
                if(origMovement.Replenishment__c != null)
                {
                    replids.add(origMovement.Replenishment__c);
                }
                // create a delivery material movement (this is required to document the changes and enable engieers to confirm the receipt)
                SCMaterialMovement__c responseMovement = new SCMaterialMovement__c();
                respMovementList.add(responseMovement);
                
                // fill the fields and book the delivered part to the stock item
                String errMsg = setResponseMovement(d, origMovement, responseMovement, enableDeliveryConfirmation, stockArticleIdToStockItemMap, mode);
                // evaluate the response for the web service
                if(errMsg == 'OK')
                {   
                    d.resultcode = 'SUCC';
                    d.resultinfo = '';
                    // update the material movement status of the requisition to "delivered" (requried for reporting purposes)
                    SCMaterialMovement__c delivMovement = new SCMaterialMovement__c(id = d.matmoveid, Status__c = '5405');    // delivered
                    origDeliveredSet.add(delivMovement);
                }
                else
                {
                    d.resultcode = 'ERR';
                    d.resultinfo = '' + errMsg + ' ';
                }                
                // add some debugging details
                if(origMovement.order__c != null)
                { 
                    d.resultinfo += ' ordermatstat:'  + origMovement.Order__r.MaterialStatus__c;
                }
                if(origMovement.orderline__c != null)
                {     
                    d.resultInfo += ' orderlinematstat:' + origMovement.OrderLine__r.MaterialStatus__c; 
                } 
           } 
           else 
           {    //ET 07.09.2012
                // The material movement does not exist (if parts are delivered without any requisition)
                // Create now a new movement movement of type delivery. the booking will be done ouside this loop
                respMovementListWithoutId.add(createResponseMovement(d, enableDeliveryConfirmation, mode));
                
                d.resultcode = 'SUCC';
           }                       
        } // for(DeliveryData d..
        
        // Post processing:
        // Now process the material movements where there was now corresponding material movement requisition
        // Try to find the missing stock items (the read functions ignores already existing items)
        stockArticleIdToStockItemMap.putAll(readStockItems(respMovementListWithoutId, stockArticleIdToStockItemMap));
        for(SCMaterialMovement__c newmov : respMovementListWithoutId)
        {
            // fill the fields and book the delivered part to the stock item
            setResponseMovement(newmov, enableDeliveryConfirmation, stockArticleIdToStockItemMap, mode);
            // the soap response is already set to SUCC (simplification)
        }
        
        // write the data 
        upsert respMovementList;
        // update GB 03.12.12, get List from map
        if(origDeliveredSet.size() > 0)
        {
                    List<SCMaterialMovement__c> origDeliveredList = new List<SCMaterialMovement__c>();
                    origDeliveredList.addAll(origDeliveredSet);
                    update origDeliveredList;
        }
        insert respMovementListWithoutId;
        // update the orders and related objects
        updateOrders(oids, enableDeliveryConfirmation, mode);
        updateOrderLines(olids, enableDeliveryConfirmation, mode);
        updateReplenishments(replids, enableDeliveryConfirmation, mode);
        updateStockItems(stockArticleIdToStockItemMap, enableDeliveryConfirmation, mode);        
        return data;
    }

    
   /*
    * Clones the existing material movement (requisition) and creates the corresponding delivery
    * material movement. The actual quantity is booked to the corresponding stock item.
    * @param d             delivered item (filled by the interface)
    * @param origMovement  the original material movement (requisition) that was used to order the part
    * @param respMovement  the delivery material movement to be created
    * @param enableDeliveryConfirmation true if the delivery has to be confirmed by the engineer
    * @param stockArticleIDToStockItemMap    the corresponding stock items (will be extended if stock item does not exist)
    * @param mode 
    */
    public static String setResponseMovement(DeliveryData d, SCMaterialMovement__c origMovement,
                                    SCMaterialMovement__c respMovement, Boolean enableDeliveryConfirmation,
                                    Map<String, SCStockItem__c> stockArticleIDToStockItemMap, String mode)
    {
        String ret = 'OK';
        if(origMovement == null)
        {
            return 'Original material movement could not be found!';
        }
        
        //--<use the original fields and references to the order / replenishment as default
        copyMaterialMovement(origMovement, respMovement);
                
        //--<set the delivery details>--------------------------------------        
        respMovement.Type__c = '5207';       // delivery
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            respMovement.Status__c = '5405'; // delivered (book directly to the stock without confirmation)
        }
        else
        {
            respMovement.Status__c = '5423'; // in transit (engineer has to confirm the receipt before booking)
        }    
        respMovement.Qty__c = d.qty;
        respMovement.DeliveryQty__c = d.qty;
        respMovement.DeliveryDate__c = d.deliverydate;
        respMovement.DeliveryNote__c = d.deliverynoteno;
        respMovement.SerialNo__c     = d.serialno;   
        // the spares team defines the valuation type so we use the delivery value
        respMovement.valuationtype__c = d.valuationtype;
        
        // if we have to book directly to the stock item
        // NOTE: we ignore the SCStock__c.ValuationType__c and SCArticle__c.ValuationType__c flags 
        //       and always book to the valuation type delivered by the erp system 
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            // the key is stockid-articleid-valuationtype
            String joinedKey = getJoinedKey(origMovement);
            SCStockItem__c stockItem = stockArticleIDToStockItemMap.get(joinedKey);
            if(stockItem != null)
            {
                respMovement.QtyOld__c = stockItem.Qty__c;    //ET: PMS 33277 store the old quantity
                stockItem.Qty__c += d.qty;                    // add the delivered qty
                debug('stock item found: ' + stockItem);
            }
            else
            {
                // TODO: what happens if the article does not exist ? skip the creation of the stock item
            
                // the stockitems does not exist in this stock for this article and valuation type
                respMovement.QtyOld__c = 0.0;    //ET: PMS 33277
                
                stockItem = new SCStockItem__c();
                stockItem.Stock__c = origMovement.Stock__c;
                stockItem.Article__c = d.articleid;
                stockItem.ValuationType__c = d.valuationtype;
                stockItem.Qty__c = d.qty;
                stockArticleIDToStockItemMap.put(joinedKey, stockitem);
                debug('stockItem created: ' + stockItem);
            }
            respMovement.QtyNew__c = stockItem.Qty__c; //ET: PMS 33277
        }    
        return ret;
    }

   /* ET: 07.09.2012
    * Adds the delivered parts to the stock items when no original material movements(requisition) exist.
    * Sets the stockitem and the qtyold and qtynew of the movement
    */
    public static String setResponseMovement(SCMaterialMovement__c respMovement, Boolean enableDeliveryConfirmation,
                                             Map<String, SCStockItem__c> stockArticleIDToStockItemMap, String mode)
    {
        String ret = 'OK';
        
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            String joinedKey = getJoinedKey(respMovement);
            SCStockItem__c stockItem = stockArticleIDToStockItemMap.get(joinedKey);
            
            if(stockItem != null)
            {
                //ET: PMS 33277
                respMovement.QtyOld__c = stockItem.Qty__c;
                stockItem.Qty__c += respMovement.Qty__c;
                debug('stock item found: ' + stockItem);
            }
            else
            {
                // TODO: what happens if the article does not exist ? skip the creation of the stock item
            
                // the stockitems does not exist in this stock for this article and valuation type
            
                respMovement.QtyOld__c = 0.0;    //ET: PMS 33277
                
                stockItem = new SCStockItem__c();
                stockItem.Stock__c = respMovement.Stock__c;
                stockItem.Article__c = respMovement.Article__c;
                stockItem.ValuationType__c = respMovement.ValuationType__c;
                stockItem.Qty__c = respMovement.Qty__c;
                stockArticleIDToStockItemMap.put(joinedKey, stockItem);
                debug('stockItem added: ' + stockItem);
            }
            respMovement.QtyNew__c = stockItem.Qty__c; //ET: PMS 33277
        }    
        return ret;
    }    
    
   /*
    * If parts are delivered without any corresponding material movement requisition 
    * a new material movement record has to be created here
    * @param d     delivered part
    * @param enableDeliveryConfirmation true if the engineer has to confirm the delivery
    * @param mode  (reserved) 
    */
    public static SCMaterialMovement__c createResponseMovement(DeliveryData d, Boolean enableDeliveryConfirmation, String mode)
    {
        SCMaterialMovement__c respMovement = new SCMaterialMovement__c();
        
        respMovement.Type__c = '5207';  //Delivery
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            respMovement.Status__c = '5405'; // delivered 
        }
        else
        {
            respMovement.Status__c = '5423'; // in transit 
        }    
            
        respMovement.Stock__c           = d.stockId;
        respMovement.Article__c         = d.articleid;
        respMovement.ValuationType__c   = d.valuationtype;
        respMovement.Qty__c             = d.qty;
        respMovement.ProcessDate__c     = Date.today();
        respMovement.DeliveryDate__c    = (d.deliverydate != null) ? d.deliverydate : Date.today();
        respMovement.DeliveryNote__c    = d.deliverynoteno;
        respMovement.DeliveryQty__c     = d.qty; // GMSNA the delivered quantity is used here as the same value
        //### TODO
        //respMovement.DeliveryStock__c = d.stockId;    //??? correct?

        // the stock item is added / booked outside of this method        
        return respMovement;
    }
    

    
   /*
    * Transfers the main data from the original movement into the new movement
    * @param src the source material movement
    * @dest the target material movement
    */
    public static void copyMaterialMovement(SCMaterialMovement__c src, SCMaterialMovement__c dest)
    {
        dest.Stock__c = src.Stock__c;               
        dest.Article__c =   src.Article__c;             
        dest.ValuationType__c = src.ValuationType__c;
        dest.Assignment__c = src.Assignment__c;          
        dest.DelivAddrCity__c = src.DelivAddrCity__c;       
        dest.DelivAddrCountryState__c = src.DelivAddrCountryState__c;
        dest.DelivAddrCountry__c = src.DelivAddrCountry__c;    
        dest.DelivAddrCounty__c = src.DelivAddrCounty__c;     
        dest.DelivAddrExtension__c = src.DelivAddrExtension__c;  
        dest.DelivAddrHouseNo__c = src.DelivAddrHouseNo__c;    
        dest.DelivAddrName__c = src.DelivAddrName__c;       
        dest.DelivAddrPostalCode__c = src.DelivAddrPostalCode__c; 
        dest.DelivAddrStreet__c = src.DelivAddrStreet__c;     
        dest.DeliveryDate__c = src.DeliveryDate__c;        
        dest.DeliveryNote__c = src.DeliveryNote__c;        
        dest.DeliveryQty__c = src.DeliveryQty__c;         
        dest.DeliveryStock__c = src.DeliveryStock__c;       
        dest.ListPrice__c = src.ListPrice__c;           
        dest.OrderLine__c = src.OrderLine__c;           
        dest.OrderRole__c = src.OrderRole__c;           
        dest.Order__c = src.Order__c;               
        dest.OwnerId = src.OwnerId;                
        dest.PrimeCost__c = src.PrimeCost__c;           
        dest.ProcessDate__c = src.ProcessDate__c;         
        dest.Qty__c = src.Qty__c;                 
        dest.ReplenishmentType__c = src.ReplenishmentType__c;   
        dest.Replenishment__c = src.Replenishment__c;       
        dest.RequisitionDate__c = src.RequisitionDate__c;     
        dest.ReturnInfo__c = src.ReturnInfo__c;          
        dest.ReturnNo__c = src.ReturnNo__c;            
        dest.SerialNo__c = src.SerialNo__c;             
        dest.Status__c = src.Status__c;              
        dest.Type__c = src.Type__c; 
        // added by GB
        dest.Parent__c = src.Parent__c;               
    }
    
    public static void updateOrders(Set<String> oids, Boolean enableDeliveryConfirmation, String mode)
    {
        //### TODO: if an order has multiple order lines > status must reflect the worst case (see AMS status map)!!!
    
        List<SCOrder__c> orderList = [Select Id, MaterialStatus__c from SCOrder__c where id in :oids];
        for(SCOrder__c order: orderList)
        {
            if(!enableDeliveryConfirmation || mode == 'testDelivered')
            {
                order.MaterialStatus__c = '5405'; // delivered 
            }
            else
            {
                order.MaterialStatus__c = '5423'; // in transit 
            }    
        }//for
        upsert orderList;
    }
    
    public static void updateOrderLines(Set<String> olids, Boolean enableDeliveryConfirmation, String mode)
    {
        List<SCOrderLine__c> orderLineList = [Select Id, MaterialStatus__c from SCOrderLine__c where id in :olids];
        for(SCOrderLine__c orderLine: orderLineList)
        {
            if(!enableDeliveryConfirmation || mode == 'testDelivered')
            {
                orderLine.MaterialStatus__c = '5405'; // delivered
            }
            else
            {
                orderLine.MaterialStatus__c = '5423'; // in transit 
            }    
        }//for
        upsert orderLineList;
    }
    
   /*
    * Update on material replenishment level (header) the delivered qty. This information is used for monitoring purposes.
    */
    public static void updateReplenishments(Set<String> replids, Boolean enableDeliveryConfirmation, String mode)
    {
        // TODO
        System.debug('### WSMaterialDelivery.updateReplenishments()');
        // Update the field delivered in all material replenishments
        // read all material replenishment called by this web service
        Map<Id, SCMaterialReplenishment__c> mrm = new Map<ID, SCMaterialReplenishment__c>([Select id, Delivered__c from SCMaterialReplenishment__c 
                                                where id in :replids ]);        
                                                
        // read all material movements with delivered type of the material replenishment
        List<SCMaterialMovement__c> mml = [Select id, Replenishment__c, Qty__c, Type__c from SCMaterialMovement__c
                                            where Replenishment__c in :replids and Type__c = :SCfwConstants.DOMVAL_MATMOVETYP_DELIVERY  
                                            and Status__c in (:SCfwConstants.DOMVAL_MATSTAT_DELIVERED) 
                                            order by Replenishment__c];                                                  
        
        if(mml.size() > 0)
        {
            System.debug('### there are movements of type Delivery');
            // Calculate sums
            ID currentReplenishment = null;
            Decimal sum = 0;
            for(SCMaterialMovement__c mm : mml)
            {
                if(currentReplenishment != null 
                    && currentReplenishment != mm.Replenishment__c)
                {
                    // it is skipped by the first loop
                    // set the sum of the previous movements to the previous material replenishment record
                    SCMaterialReplenishment__c mr = mrm.get(currentReplenishment);
                    mr.Delivered__c = sum;
                    System.debug('### material replenishment: ' + mr);                
                    // reset the sum
                    sum = 0;
                }     
                // mark the current group
                currentReplenishment = mm.Replenishment__c;
    
                // increment the sum of the current group
                sum += mm.Qty__c;
            }
            
            // set the sum of the last group             
            SCMaterialReplenishment__c mrLast = mrm.get(currentReplenishment);
            if(mrLast != null)
            {
                mrLast.Delivered__c = sum;
            }    
            
            // update material replenishment list
            List<SCMaterialReplenishment__c> mrl = new List<SCMaterialReplenishment__c>();
            for(Id mrId: mrm.keySet())
            {
                SCMaterialReplenishment__c mr = mrm.get(mrId);
                mrl.add(mr);
            }
            
            if(mode == null || mode != null && mode != 'Test')
            { 
                update mrl;
            }    
        }
        else
        {
            System.debug('### there were no movements of type Delivery');
        }                          
    }
    
    public static void updateStockItems(Map<String, SCStockItem__c> stockArticleIdToStockItemMap,
                                        Boolean enableDeliveryConfirmation, String mode)
    {
        if(!enableDeliveryConfirmation || mode == 'testDelivered')
        {
            List<SCStockItem__c> stockItemList = new List<SCStockItem__c>();
            for (String stockArticleId : stockArticleIdToStockItemMap.keySet())
            {
                SCStockItem__c itemLoop = stockArticleIdToStockItemMap.get(stockArticleId);
                stockItemList.add(itemLoop);
            }        
            upsert stockItemList;
        }
    }

    /*
     * Read all stock items that belong to the delivered material movements. 
     * @param movements    the delivered material movements (including the delivered valuation type used to identify the stock item)
     * @return a map where the key is the stockid-articleid-valuationtype
     */
    public static Map<String, SCStockItem__c> readStockItems(Map<ID, SCMaterialMovement__c> movements)
    {
        Map<String, SCStockItem__c> ret = new Map<String, SCStockItem__c>();
        List<String> joinedIds = new List<String>();
        for(Id movementId: movements.keySet())
        {
            SCMaterialMovement__c mov = movements.get(movementId);
            joinedIds.add(getJoinedKey(mov.Stock__c, mov.Article__c, mov.ValuationType__c));
        } 
        
        // now read the stock items based on the formula StockArticleId__c in the format: stockid-articleid-valuationtype
        List<SCStockItem__c> stockItemList = [Select StockArticleId__c, ID, Article__c, Qty__c, ValuationType__c from SCStockItem__c
                                              where StockArticleId__c in : joinedIds];
                                                //GMSNA 06.02.2013 disabled because of side effects: for update];  
        for(SCStockItem__c stockItem: stockItemList)
        {
            ret.put(stockItem.StockArticleId__c, stockItem);
        }              
        debug('map of stock items: ' + ret);                                        
        return ret;
    }
    
   /* ET: 07.09.2012
    * For delivered parts where no material requisition exists this function reads the stock items.
    * @param movements     the identified new delivery material movements 
    * @return a map containing the stock items 
    */
    public static Map<String, SCStockItem__c> readStockItems(List<SCMaterialMovement__c> movements, Map<String, SCStockItem__c> stockitems)
    {
        // determine all keys of the stock items that are required
        List<String> joinedIds = new List<String>();
        for(SCMaterialMovement__c movement: movements)
        {
            // ensure that we do not read the stock items that are already processed (existing material requisitions)
            String key = getJoinedKey(movement);
            if(!stockitems.containskey(key))
            {
                // we have to read this stock item
                joinedIds.add(key);
            }
        }
        // 05.02.13 GB: removed "for update" as it caused crash
        List<SCStockItem__c> stockItemList = [Select StockArticleId__c, ID, Article__c, Qty__c from SCStockItem__c
                                              where StockArticleId__c in : joinedIds];  

        Map<String, SCStockItem__c> ret = new Map<String, SCStockItem__c>();
        for(SCStockItem__c stockItem: stockItemList)
        {
            ret.put(stockItem.StockArticleId__c, stockItem);
        }              
        debug('map of stock items: ' + ret);                                        
        return ret;
    }
    
    
    
   /*
    * Formats the stock item technical key based on the stock id, article id and the valuation type.
    * Please ensure that the format is the same as in SCStockItem__c.StockArticleId__c
    * @param origMovement
    * @areturn a key to identify the stock item in the format stockid-articleid-valuationtype
    */
    public static String getJoinedKey(SCMaterialMovement__c mov)
    {
        return getJoinedKey(mov.Stock__c, mov.Article__c, mov.ValuationType__c);
    }

   /*
    * Formats the stock item technical key based on the stock id, article id and the valuation type.
    * Please ensure that the format is the same as in SCStockItem__c.StockArticleId__c
    */
    public static String getJoinedKey(String stockId, String articleId, String valuationtype)
    {
        String ret = null;
        if(stockId != null && articleId != null)
        {    
            // use only the first 15 chars (ignore the 18 char SF api version)
            stockId = stockId.substring(0,15);
            articleId = articleId.substring(0,15);
            ret = stockId + '-' + articleId + '-' +  (valuationtype != null ? valuationtype : '');
        }
        
        return ret;    
    }

    private static void debug(String text)
    {
       System.debug('###...................' + text);
    }
    


   /*  
    * Data object holding delivery information
    */
    global class DeliveryData
    {
        // material movement type (reserved for partial delivery and extensions)
        // please use "5207" Delivery
        WebService String type;           
    
        // If there was a corresponding material movement 5201, 5202 or 5222
        // the MaterialMovementId is a reference to this record. 
        // used to update the material status of the material request.
        // If this field is null - you must define the ArticleId, StockId and optionally the OrderLineId
        WebService String matmoveid;           // SCMaterialMovement__c.ID
        // reference to the article that is delivered 
        WebService String articleid;                    // SCArticle__c.ID
        WebService Double qty;                          // the delivered quantity
        WebService String deliverynoteno;               // delivery note number from the ERP system
        WebService Date   deliverydate;                 // delivery note date
        WebService String serialno;                     // serial number (reserved)

        // reverence to the stock (alternaively to matmoveid
        WebService String stockId;                      // SCStock__c.ID        


        // Processing result code and additional information filled by this web service 
        WebService String resultcode;
        WebService String resultinfo;
        
        // used to identify if the parts are new or used (booked into individual stock items)
        WebService String valuationtype;
    }
    
} // WSMaterialDelivery