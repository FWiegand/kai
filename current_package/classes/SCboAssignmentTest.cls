/*
 * @(#)SCboAssignmentTest.cls SCCloud    
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCboAssignmentTest {

    static testMethod void beforeUpdate() 
    {
        SCHelperTestClass.createOrderTestSet4(true);
        SCHelperTestClass.createOneAppointment(true);
        
        List<SCAssignment__c> assignments = new List<SCAssignment__c>();
        assignments.add(SCHelperTestClass.appointments[0].Assignment__r);
        SCboAssignment.BeforeUpdate(assignments, assignments);
    }
    static testMethod void afterUpdate() 
    {
        SCHelperTestClass.createOrderTestSet(true);        
        SCAppointment__c app1 = new SCAppointment__c();
        SCAppointment__c app2 = new SCAppointment__c();
        SCAppointment__c app3 = new SCAppointment__c();
        SCAppointment__c app4 = new SCAppointment__c();
        app1.Assignment__c = SCHelperTestClass.appointmentsSingle2[0].Assignment__r.Id;
        app1.Assignment__r = SCHelperTestClass.appointmentsSingle2[0].Assignment__r;
        app2.Assignment__c = SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r.Id;
        app2.Assignment__r = SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r;
        app3.Assignment__c = SCHelperTestClass.appointmentsSingle2[0].Assignment__r.Id;
        app3.Assignment__r = SCHelperTestClass.appointmentsSingle2[0].Assignment__r;
        app4.Assignment__c = SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r.Id;
        app4.Assignment__r = SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r;
        insert app1;
        insert app2;
        insert app3;
        insert app4;

        List<SCAssignment__c> assignments1 = new List<SCAssignment__c>();
        assignments1.add(SCHelperTestClass.appointmentsSingle2[0].Assignment__r);
        List<SCAssignment__c> assignments2 = new List<SCAssignment__c>();
        assignments2.add(SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r);

        assignments1.add(SCHelperTestClass.appointmentsSingle2[0].Assignment__r);
        assignments2.add(SCHelperTestClass.appointmentsSingleReleased[0].Assignment__r);


        SCboAssignment.AfterUpdate(assignments1, assignments2);
    }
}