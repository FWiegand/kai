/*
 * @(#)SCfwWizardTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Helper class to implement a wizard with [Previous] [Next] [Finish] buttons.
 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwWizardTest
{
    
   /*
    * Simple test method
    */
    public static testMethod void testSimple() 
    {    
        SCfwWizard wiz = new SCfwWizard(3, false);
        // check the initialization        
        System.AssertEquals(3, wiz.steps);
        System.AssertEquals(1, wiz.step);

        // check the navigation for page1 [Prev->off][Next->on][Finish->off]
        System.AssertEquals(false, wiz.getEnablePrev());
        System.AssertEquals(true, wiz.getEnableNext());
        System.AssertEquals(false, wiz.getEnableFinish());
        
        wiz.next();
        System.AssertEquals(2, wiz.step);

        // check the navigation for page2 [Prev->on][Next->on][Finish->off]
        System.AssertEquals(true, wiz.getEnablePrev());
        System.AssertEquals(true, wiz.getEnableNext());
        System.AssertEquals(false, wiz.getEnableFinish());

        wiz.next();
        System.AssertEquals(3, wiz.step);

        // check the navigation for page2 [Prev->on][Next->off][Finish->on]
        System.AssertEquals(true, wiz.getEnablePrev());
        System.AssertEquals(false, wiz.getEnableNext());
        System.AssertEquals(true, wiz.getEnableFinish());

        wiz.reset();
        System.AssertEquals(1, wiz.step);
        
        wiz.gotoFinish();
        System.AssertEquals(3, wiz.step);
        
        wiz.goto(2);
        System.AssertEquals(2, wiz.step);

        wiz.prev();
        System.AssertEquals(1, wiz.step);
        System.AssertEquals('1/3', wiz.getStatus());
        
        wiz.finish();
        System.AssertEquals(3, wiz.step);
        
    }

   /*
    * Advanced test method
    */
    public static testMethod void testWithSummary() 
    {    
        SCfwWizard wiz = new SCfwWizard(3, true);
        // page 1
        System.AssertEquals(1, wiz.step);
        System.AssertEquals(false, wiz.getEnablePrev());
        System.AssertEquals(true, wiz.getEnableNext());
        System.AssertEquals(false, wiz.getEnableFinish());
        System.AssertEquals(false, wiz.getShowSummary());
        
        // page 2
        wiz.next();
        System.AssertEquals(2, wiz.step);
        System.AssertEquals(true, wiz.getEnablePrev());
        System.AssertEquals(true, wiz.getEnableNext());
        System.AssertEquals(false, wiz.getEnableFinish());
        System.AssertEquals(false, wiz.getShowSummary());

        // page 3
        wiz.next();
        System.AssertEquals(3, wiz.step);
        System.AssertEquals(true, wiz.getEnablePrev());
        System.AssertEquals(false, wiz.getEnableNext());
        System.AssertEquals(true, wiz.getEnableFinish());
        System.AssertEquals(false, wiz.getShowSummary());

        // summary page - , next and finish buttons must be disabled.
        wiz.finish();
        System.AssertEquals(4, wiz.step);
        System.AssertEquals(false, wiz.getEnablePrev());
        System.AssertEquals(false, wiz.getEnableNext());
        System.AssertEquals(false, wiz.getEnableFinish());
        System.AssertEquals(true, wiz.getShowSummary());
    }    
}