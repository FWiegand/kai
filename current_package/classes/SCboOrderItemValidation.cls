/*
 * @(#)SCboOrderItemValidation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderItemValidation
{
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Validates a business object from the type order item.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean validate(SCboOrderItem orderItem)
    {
        return validate(orderItem, SCboOrderValidation.ALL);
    }
    public static Boolean validate(SCboOrderItem boOrderItem, Integer mode)
    {
        Boolean ret = true;
        
        if ((mode & SCboOrderValidation.ORDERITEM_DATA) == SCboOrderValidation.ORDERITEM_DATA)
        {
            if (null == boOrderItem.orderItem.Duration__c)
            {
                throw new SCfwException(2200);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.DurationUnit__c))
            {
                throw new SCfwException(2201);
            }
            /** following fields no longer mandatory
            if (!SCBase.isSet(boOrderItem.orderItem.CompletionStatus__c))
            {
                throw new SCfwException(2202);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.ErrorSymptom1__c))
            {
                throw new SCfwException(2203);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.ErrorSymptom2__c))
            {
                throw new SCfwException(2204);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.ErrorCondition1__c))
            {
                throw new SCfwException(2205);
            }
            */

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrderItem.orderItem.CompletionStatus__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.CompletionStatus__c, boOrderItem.orderItem.CompletionStatus__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.CompletionStatus__c, 'SCOrderItem__c.CompletionStatus__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.DurationUnit__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.DurationUnit__c, boOrderItem.orderItem.DurationUnit__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.DurationUnit__c, 'SCOrderItem__c.DurationUnit__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.ErrorCondition1__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.ErrorCondition1__c, boOrderItem.orderItem.ErrorCondition1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.ErrorCondition1__c, 'SCOrderItem__c.ErrorCondition1__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.ErrorCondition2__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.ErrorCondition2__c, boOrderItem.orderItem.ErrorCondition2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.ErrorCondition2__c, 'SCOrderItem__c.ErrorCondition2__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.ErrorSymptom1__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.ErrorSymptom1__c, boOrderItem.orderItem.ErrorSymptom1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.ErrorSymptom1__c, 'SCOrderItem__c.ErrorSymptom1__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.ErrorSymptom2__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.ErrorSymptom2__c, boOrderItem.orderItem.ErrorSymptom2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.ErrorSymptom2__c, 'SCOrderItem__c.ErrorSymptom2__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.ErrorSymptom3__c) && 
                    !SCBase.isPicklistValue(SCOrderItem__c.ErrorSymptom3__c, boOrderItem.orderItem.ErrorSymptom3__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.ErrorSymptom3__c, 'SCOrderItem__c.ErrorSymptom3__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.ORDERITEM_DATA) == SCboOrderValidation.ORDERITEM_DATA)
        
        if ((mode & SCboOrderValidation.INSTBASE_DATA) == SCboOrderValidation.INSTBASE_DATA)
        {
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__c) && 
                (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductModel__c) && 
                 !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductSkill__c) && 
                 (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c) || 
                  !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductUnitType__c))))
            {
                throw new SCfwException(2300);
            }
            
            /*
            //#### CC GMSNA quick hack 18.02.2013 - not required
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.Status__c))
            {
                throw new SCfwException(2301);
            }
            */
            
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.Type__c))
            {
                throw new SCfwException(2302);
            }
            /*
            //#### CC GMSNA quick hack 18.02.2013 - not required

            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.Brand__c))
            {
                throw new SCfwException(2303);
            }
            */

            Date earliestInstDate = appSettings.INSTBASE_INSTDATE_EARLIESTDATE__c;
            Date today = Date.today();
            Decimal instDateDays = appSettings.INSTBASE_INSTDATE_DAYSINFUTURE__c;
            if ((null != earliestInstDate) && 
                (null != boOrderItem.orderItem.InstalledBase__r.InstallationDate__c) && 
                (boOrderItem.orderItem.InstalledBase__r.InstallationDate__c < earliestInstDate))
            {
                throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(2304, earliestInstDate.format()));
            }
            if ((null != instDateDays) && 
                (null != boOrderItem.orderItem.InstalledBase__r.InstallationDate__c) && 
                (boOrderItem.orderItem.InstalledBase__r.InstallationDate__c > today.addDays(instDateDays.intValue())))
            {
                throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(2305, today.addDays(instDateDays.intValue()).format()));
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
            
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductEnergy__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductEnergy__c, boOrderItem.orderItem.InstalledBase__r.ProductEnergy__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductEnergy__c, 'SCInstalledBase__c.ProductEnergy__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductGroup__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductGroup__c, boOrderItem.orderItem.InstalledBase__r.ProductGroup__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductGroup__c, 'SCInstalledBase__c.ProductGroup__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductPower__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductPower__c, boOrderItem.orderItem.InstalledBase__r.ProductPower__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductPower__c, 'SCInstalledBase__c.ProductPower__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductSkill__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductSkill__c, boOrderItem.orderItem.InstalledBase__r.ProductSkill__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductSkill__c, 'SCInstalledBase__c.ProductSkill__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductUnitClass__c, boOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductUnitClass__c, 'SCInstalledBase__c.ProductUnitClass__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ProductUnitType__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ProductUnitType__c, boOrderItem.orderItem.InstalledBase__r.ProductUnitType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ProductUnitType__c, 'SCInstalledBase__c.ProductUnitType__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.SerialNoException__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.SerialNoException__c, boOrderItem.orderItem.InstalledBase__r.SerialNoException__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.SerialNoException__c, 'SCInstalledBase__c.SerialNoException__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.Status__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.Status__c, boOrderItem.orderItem.InstalledBase__r.Status__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.Status__c, 'SCInstalledBase__c.Status__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.Type__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.Type__c, boOrderItem.orderItem.InstalledBase__r.Type__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.Type__c, 'SCInstalledBase__c.Type__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.ValidationStatus__c) && 
                    !SCBase.isPicklistValue(SCInstalledBase__c.ValidationStatus__c, boOrderItem.orderItem.InstalledBase__r.ValidationStatus__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.ValidationStatus__c, 'SCInstalledBase__c.ValidationStatus__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.INSTBASE_DATA) == SCboOrderValidation.INSTBASE_DATA)
        
        if ((mode & SCboOrderValidation.INSTBASELOC_DATA) == SCboOrderValidation.INSTBASELOC_DATA)
        {
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.LocName__c))
            {
                throw new SCfwException(2400);
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Status__c) && 
                    !SCBase.isPicklistValue(SCInstalledBaseLocation__c.Status__c, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Status__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Status__c, 
                                                                                            'SCInstalledBaseLocation__c.Status__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.INSTBASELOC_DATA) == SCboOrderValidation.INSTBASELOC_DATA)
        
        if ((mode & SCboOrderValidation.INSTBASELOC_ADDRESS_DATA) == SCboOrderValidation.INSTBASELOC_ADDRESS_DATA)
        {
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Street__c))
            {
                throw new SCfwException(2401);
            }
            /*
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.HouseNo__c))
            {
                throw new SCfwException(2402);
            }
            */
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.PostalCode__c))
            {
                throw new SCfwException(2403);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.City__c))
            {
                throw new SCfwException(2404);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                !SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Country__c))
            {
                throw new SCfwException(2405);
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Country__c) && 
                    !SCBase.isPicklistValue(SCInstalledBaseLocation__c.Country__c, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Country__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.Country__c, 
                                                                                            'SCInstalledBaseLocation__c.Country__c'));
                }
                
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.CountryState__c) && 
                    !SCBase.isPicklistValue(SCInstalledBaseLocation__c.CountryState__c, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.CountryState__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.CountryState__c, 
                                                                                            'SCInstalledBaseLocation__c.CountryState__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.INSTBASELOC_ADDRESS_DATA) == SCboOrderValidation.INSTBASELOC_ADDRESS_DATA)
        
        if ((mode & SCboOrderValidation.INSTBASELOC_GEO_DATA) == SCboOrderValidation.INSTBASELOC_GEO_DATA)
        {
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                (null == boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.GeoX__c))
            {
                throw new SCfwException(2406);
            }
            if (!SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__c) && 
                (null == boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.GeoY__c))
            {
                throw new SCfwException(2407);
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.GeoStatus__c) && 
                    !SCBase.isPicklistValue(SCInstalledBaseLocation__c.GeoStatus__c, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.GeoStatus__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boOrderItem.orderItem.InstalledBase__r.InstalledBaseLocation__r.GeoStatus__c, 
                                                                                            'SCInstalledBaseLocation__c.GeoStatus__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.INSTBASELOC_GEO_DATA) == SCboOrderValidation.INSTBASELOC_GEO_DATA)

        if ((mode & SCboOrderValidation.ORDERLINE_DATA) == SCboOrderValidation.ORDERLINE_DATA)
        {
            for (SCboOrderLine line :boOrderItem.boOrderLines)
            {
                ret |= SCboOrderLineValidation.validate(line);
            } // for (SCboOrderLine line :boOrderItem.boOrderLines)
        } // if ((mode & SCboOrderValidation.ORDERLINE_DATA) == SCboOrderValidation.ORDERLINE_DATA)

        if ((mode & SCboOrderValidation.REPAIRCODE_DATA) == SCboOrderValidation.REPAIRCODE_DATA)
        {
            for (SCboOrderRepairCode repCode :boOrderItem.boRepairCodes)
            {
                ret |= SCboOrderRepairCodeValidation.validate(repCode);
            } // for (SCboOrderRepairCode repCode :boOrderItem.boRepairCodes)
        } // if ((mode & SCboOrderValidation.ORDERLINE_DATA) == SCboOrderValidation.ORDERLINE_DATA)
        return ret;
    } // validate
} // SCboOrderItemValidation