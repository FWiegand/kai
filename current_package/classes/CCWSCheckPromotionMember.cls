global without sharing class CCWSCheckPromotionMember {
	WebService static CheckResult checkPromotionMember(String StrPromotionId, String StrMode){
		
		// Mode:
		// if Mode = precheck, just set Checkbox for all Promotion member of the given Promotion
		// if Mode = SetStatus Set the Status of all promotion member where the Checkbox is still set to signed off
		// return true, if ready, return false if not ready.
		
		// Define List of all Objects to insert
		List<PromotionMember__c> ListPromMemUpdate 	= new List<PromotionMember__c>();
		// Define List to get Promotion
		List<Promotion__c> ListPromotion 			= new List<Promotion__c>();
		// String to hold the original Promotion ID of Salesforce.com
		Id IdPromotionSFDCId 						= null;
		// Prepare Result Object
		CheckResult ObjectResult	 				= new CheckResult();
		ObjectResult.error							= '';
		
		// Number of records calculated at one time
		Integer noRec								= 800;
		
		// +++++++++++++++++++++++++++ STEP 1: Validate and query Promotion with the given external Id +++++++++++++++++++++++++++++++++++++++++++++
		// Query promotion
		try{
			ListPromotion = [Select PromotionID__c, Id From Promotion__c WHERE PromotionID__c = : StrPromotionId];
		}catch (Exception e){
			ObjectResult.success = false;
			return ObjectResult;
		}
		// Check, if we found a promotion
		if(ListPromotion.size()==0){
			// ObjectResult.addError('No Promotion found for ID '+StrPromotionId+'.');
			ObjectResult.success = false;
			return ObjectResult;
		}
		// Check, if we found to much promotions
		else if(ListPromotion.size()>1){
			// ObjectResult.addError('Found '+ListPromotion.size()+' Promotions for PromotionID '+StrPromotionId+'.');
			ObjectResult.success = false;
			return ObjectResult;
		}
		else{
			IdPromotionSFDCId = ListPromotion[0].Id;
		}
		system.debug('####PromotionID: '+IdPromotionSFDCId);
		
		// +++++++++++++++++++++++++++ STEP 2: Call function depending on the mode given to get a list with Members to update+++++++++++++++++++++++++++++++++++++++++++++
		if (StrMode=='PreCheck'){
			for (PromotionMember__c ObjTmpPromoMember : [Select p.Id From PromotionMember__c p WHERE PotentialSignedOff__c=false AND Promotion__c = : IdPromotionSFDCId LIMIT : noRec]){
				ObjTmpPromoMember.PotentialSignedOff__c = true;
				ListPromMemUpdate.add(ObjTmpPromoMember);
			}
		}
		else if (StrMode=='SetStatus'){
			for (PromotionMember__c ObjTmpPromoMember : [Select p.Id From PromotionMember__c p WHERE PotentialSignedOff__c=true AND Promotion__c = : IdPromotionSFDCId LIMIT : noRec]){
				ObjTmpPromoMember.PotentialSignedOff__c	= false;
				ObjTmpPromoMember.Status__c				= 'Signed out';
				ListPromMemUpdate.add(ObjTmpPromoMember);
			}
		}
		else{
			ObjectResult.error 		= 'Erro while collecting data';
			ObjectResult.success 	= false;
		}
		// +++++++++++++++++++++++++++ STEP 3: If the List is less than 2000 records, we are done, otherwise not+++++++++++++++++++++++++++++++++++++++++++++
		ObjectResult.ready 			= false;
		if (ListPromMemUpdate.size()<noRec){
			ObjectResult.ready 		= true;
		}
		// Perform update
		try{
			update ListPromMemUpdate;
		}
		catch (Exception e){
			ObjectResult.error 		= e.getMessage();
			ObjectResult.success	= false;
			return ObjectResult;
		}
		return ObjectResult;
		
	}

	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class CheckResult{
		webservice String MessageUUID{get;set;}
		webservice String error{get;set;}
		webservice Boolean success{get;set;}
		webservice Boolean ready{get;set;}
	}
	
}