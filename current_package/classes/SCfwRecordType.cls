/*
 * @(#)SCfwRecordType.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCfwRecordType
{
	/**
	 * Constructor w/o any further processing
	 */
    public SCfwRecordType() 
    {

    }

	/**
	 * Constructor setting the sObject directly
	 * 
	 * @params sObjectType Set the sObject the record type should belong to
	 */
    public SCfwRecordType(String sObjectType) 
    {
		this.sObjectType = sObjectType;
    }
    
    /**
     * sObjectType the Record Type belongs to 
     */
    public String sObjectType { get; set; }
    
    /**
     * The actual record type ID as string
     */
    public String recordType { get; set; }
    
    
    /**
     * Get a list of valid Record Types for the given sObject
     *
     * @return List of available Record Types that can be used for select boxes
     */
    public List<SelectOption> getRecordTypes() {
   
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select Record Type --'));
        
        for(RecordType rt: getRecordTypesAsList() )
        {
            options.add(new SelectOption(rt.id,rt.name));    
        }
        
        return options;
    }
    
    /**
     * Get all available record types for the sObjectType as List
     *
     * @return List of all record types for sObjectType
     */
    public List<RecordType> getRecordTypesAsList()
    {
    	List<RecordType> rtList = new List<RecordType>();
    	
    	rtList = [select id, Name
    				from RecordType 
    			   where sobjecttype= :sObjectType];

    	return rtList;
    }
    
    
    /**
     * Get the first available Person Type for the current user.
     * <p>
     * It might happen that there are more than one person types available for the 
     * current user, but we pick the first one.
     *
     * @return First person record type 
     */
    public RecordType getPersonType()
    {
    	RecordType rt = new RecordType();
    	
    	try
    	{
    		rt = [select id,Name from RecordType 
    				 where sobjecttype= :sObjectType
    				 limit 1];
			return rt;
    	}
    	catch (Exception e)
    	{
    		System.debug(LoggingLevel.WARN, 
    					 'No person type found for sObject' + sObjectType);
    		return null;
    	}
    }
    
    /**
     * Check if a given Record Type is a Person Type or not
     * 
     * @return true if it's a Person Type, false otherwise
     */ 
    public boolean isPersonType(Id id)
    {
    	Boolean isPerson = false;
    	try
    	{
        	isPerson = false; //[select isPersonType from RecordType where id = :id].isPersonType;
        	return isPerson;
    	}
    	catch (QueryException e)
    	{
    		return false;
    	}
    }
}