/* 
 * @(#)SCutilSequenceExceptionTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author jpietrzyk@gms-online.de
 */
@isTest
private class SCIntrefaceLogTest 
{
    // code coverage only test    
    private static testmethod void test()
    {
    	SCInterfaceLog il = new SCInterfaceLog();
        SCInterfaceLog.logBatchInternal('MyTestInterface', 'MyInterfaceHandler',
                                null, null, 'MyresultCode', 
                                'MyResultInfo', 'MyData', Datetime.now(), 123);        
                                
        SCInterfaceLog.logBatchInternalExt('CONTRACT_ORDER_DISPATCH', 'MyInterfaceHandler',
                                null, null, 'MyresultCode', 
                                'MyResultInfo', 'MyData', Datetime.now(), 123);        
                                
        SCInterfaceLog.logBatchInternalExt('CONTRACT_VISIT', 'MyInterfaceHandler',
                                null, null, 'MyresultCode', 
                                'MyResultInfo', 'MyData', Datetime.now(), 123);        
                                
                                
        SCInterfaceLog.logError('ORDER_CREATION', 'SCOrderPageController',
                                null, null, 'MyresultCode', 'MyResultInfo', 
                                'MyData', Datetime.now(), 123);        

		SCInterfaceLog.logBatchInternalExt('CONTRACT_ORDER', 'interfaceHandler',
					  	   		null, null,'TestResultCode',
					  	   		'MyResultInfo', 'MyData', Datetime.now(), 123);		

		SCInterfaceLog.logBatchInternalExt('MAINTENANCE_ORDER', 'interfaceHandler',
					  	   		null, null, 'TestResultCode',
					  	   		'MyResultInfo', 'MyData', Datetime.now(), 123);		
		
		SCInterfaceLog.logBatchInternalExt('MAINTENANCE_VISIT', 'interfaceHandler',
					  	   		null, null, 'TestResultCode',
					  	   		'MyResultInfo', 'MyData', Datetime.now(), 123);		
    }
}