/*
 * @(#)SCHelperTestClassTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * For the CodeCoverage of the SCHelperTestClasses.
 * @author GMS_SS
 */
@isTest
private class SCHelperTestClassTest
{
    public static testMethod void Helper1testMethod1()
    {
        SCHelperTestClass.event = new Event();
        System.assertNotEquals(null, SCHelperTestClass.event);
        
        SCHelperTestClass.task = new Task();
        System.assertNotEquals(null, SCHelperTestClass.task);
        
        SCHelperTestClass.opportunity = new Opportunity();
        System.assertNotEquals(null, SCHelperTestClass.opportunity);
        
        SCHelperTestClass.articleDetails = new List<SCArticleDetail__c>();
        System.assertNotEquals(null, SCHelperTestClass.articleDetails);
        
        SCHelperTestClass.appointments2 = new List<SCAppointment__c>();
        System.assertNotEquals(null, SCHelperTestClass.appointments2);
        
        SCHelperTestClass.orderLine = new SCOrderLine__c();
        System.assertNotEquals(null, SCHelperTestClass.orderLine);
        
        SCHelperTestClass.runningAliasStr = 'test';
        System.assertNotEquals(null, SCHelperTestClass.runningAliasStr);
        
        SCHelperTestClass.createAccountRoles(new Account(), new Account(), true);
        SCHelperTestClass.createPricelist(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockProfiles(true);
    }
    
    public static testMethod void Helper1testMethod2()
    {
        SCArticle__c art = new SCArticle__c(Name = 'test0');
        Database.insert(art);
        
        SCHelperTestClass.createArticleDetail(art.Id ,false);
    }
    
    public static testMethod void Helper1testMethod3()
    {
        SCHelperTestClass.createDomsForOrderCreation();
        
        List<SCArticle__c> listArt = new List<SCArticle__c>{new SCArticle__c(Name = 'test0'),
                                                            new SCArticle__c(Name = 'test1'),
                                                            new SCArticle__c(Name = 'test2'),
                                                            new SCArticle__c(Name = 'test3'),
                                                            new SCArticle__c(Name = 'test4'),
                                                            new SCArticle__c(Name = 'test5'),
                                                            new SCArticle__c(Name = 'test6')};
        
        Database.insert(listArt);
        
        SCPlant__c plant1 = new SCPlant__c(Name = 'Test');
        Database.insert(plant1);
        
        List<SCStock__c> listStock = new List<SCStock__c>{new SCStock__c(Name = 'test0', plant__c = plant1.Id),
                                                          new SCStock__c(Name = 'test1', plant__c = plant1.Id)};
        
        Database.insert(listStock);
        
        List<SCStockProfile__c> listStockProfil = new List<SCStockProfile__c>{new SCStockProfile__c(Name = 'test0'),
                                                                              new SCStockProfile__c(Name = 'test1')};
        
        Database.insert(listStockProfil);
        
        SCHelperTestClass.createTestStockOrigins(listStock, true);
        SCHelperTestClass.createTestStockItemsForReplenishment(listArt, listStock, true);
        //SCHelperTestClass.createTestStockProfileItems(listArt, listStockProfil.get(0).Id, true);
        SCHelperTestClass.createTestStockProfileAssocs(listStock.get(0).Id, listStock.get(1).Id, listStockProfil.get(0).Id, listStockProfil.get(1).Id, true);
    }
    
    public static testMethod void Helper1testMethod4()
    {
        SCHelperTestClass.createOrder();
    }
    
    public static testMethod void Helper1testMethod5()
    {
        Account acc = new Account(Name = 'Test');
        Database.insert(acc);
        
        SCHelperTestClass.CreateEventobject(acc.Id, true);
        SCHelperTestClass.CreateTaskobject(acc.Id, true);
    }
    
    
    public static testMethod void Helper2testMethod1()
    {
        SCHelperTestClass2.timeReportDay2 = new List<SCTimeReport__c>();
        System.assertNotEquals(null, SCHelperTestClass2.timeReportDay2);
        
        SCHelperTestClass2.notificationProducts = new List<SCNotificationProduct__c>();
        System.assertNotEquals(null, SCHelperTestClass2.notificationProducts);
        
        SCHelperTestClass2.createSchedulingList();
    }
    
    public static testMethod void Helper2testMethod2()
    {
        SCHelperTestClass2.createConfOrderLine(true);
    }
    
    public static testMethod void Helper2testMethod3()
    {
        SCHelperTestClass2.createNotificationProducts(false);
    }
    
    
    
    public static testMethod void Helper3testMethodAT()
    {
        SCHelperTestClass3.createCustomSettings('AT', true);
    }
    
    public static testMethod void Helper3testMethodDE()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
    }
    
    public static testMethod void Helper3testMethodGB()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
    }
    
    public static testMethod void Helper3testMethodNL()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
    }
    
    
    
    public static testMethod void Helper4testMethod1()
    {
        SCHelperTestClass4.account = new Account(Name = 'test');
        System.assertNotEquals(null, SCHelperTestClass4.account);
        
        SCHelperTestClass4.article = new SCArticle__c();
        System.assertNotEquals(null, SCHelperTestClass4.article);
        
        SCHelperTestClass4.location = new SCInstalledBaseLocation__c();
        System.assertNotEquals(null, SCHelperTestClass4.location);
    }
    
    public static testMethod void Helper4testMethod2()
    {
        SCHelperTestClass4.createAccount();
    }
    
    public static testMethod void Helper4testMethod3()
    {
        SCHelperTestClass4.createPersonAccount();
    }
    
    public static testMethod void Helper4testMethod4()
    {
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass4.createArticle();
        SCHelperTestClass4.createInstalledBaseLocation();
        SCHelperTestClass4.createTaxRecord();
        
        List<SCArticle__c> listArt = SCHelperTestClass4.createTestArticles();
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> listStock = SCHelperTestClass4.createTestStocks(plantId);
        
        SCHelperTestClass4.createTestStockOrigins(listStock);
        SCHelperTestClass4.createTestStockItem(listStock.get(0).Id, listArt.get(0).Id);
        SCHelperTestClass4.createTestStockItems(listArt, listStock.get(0).Id, listStock.get(1).Id);
        
        List<SCStockItem__c> listStockItems = SCHelperTestClass4.createTestStockItemsForReplenishment(listArt, listStock);
        List<SCStockProfile__c> listStockProf = SCHelperTestClass4.createTestStockProfiles();
        
        //SCHelperTestClass4.createTestStockProfileItems(listArt, listStockProf.get(0).Id);
        SCHelperTestClass4.createTestStockProfileAssocs(listStock.get(0).Id, listStock.get(1).Id, listStockProf.get(0).Id, listStockProf.get(1).Id);
        
        List<User> listUser = SCHelperTestClass4.createTestUsers();
        List<SCResource__c> listRes = SCHelperTestClass4.createTestResources(listUser);
        Id calId = SCHelperTestClass4.createTestCalendar();
        Id buId = SCHelperTestClass4.createTestBusinessUnit(listStock.get(0).Id, calId);
        
        SCHelperTestClass4.createTestWorkTimeProfile();
        SCHelperTestClass4.createTestAssignments(listRes, listStock, buId);
    }
    
    public static testMethod void Helper4testMethod5()
    {
        SCHelperTestClass4.createInstalledBaseLocation();
        SCHelperTestClass4.createInstalledBase();
    }
    
    public static testMethod void Helper4testMethod6()
    {
        SCOrder__c order = SCHelperTestClass4.createOrder(true);
        
        SCHelperTestClass4.orderRolePerson(order, 'Role', true);
    }
}