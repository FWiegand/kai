/**
 * AseUnitTestTestService.cls   mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /*
  * Testclass
  */
@isTest
private class AseUnitTestUtils
{
    @isTest
    public static void testGetDataType()
    {    
        System.assertEquals(AseCalloutConstants.ASE_TYPE_UNKNOWN, AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_UNKNOWN_STR ));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR ));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_CALENDAR , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_CALENDAR_STR ));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY_STR ));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_LOCATION , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_LOCATION_STR));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_ORGUNIT , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_ORGUNIT_STR));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_RESOURCE , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_RESOURCE_STR));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_USERLOGIN , AseCallOutUtils.getDataType(AseCalloutConstants.ASE_TYPE_USERLOGIN_STR));
    }

    @isTest
    public static void testGetDataTypeAsString()
    {
        System.assertEquals(AseCalloutConstants.ASE_TYPE_UNKNOWN_STR.toLowerCase(), AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_UNKNOWN));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_CALENDAR_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_CALENDAR));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_LOCATION_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_LOCATION));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_ORGUNIT_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_ORGUNIT));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_RESOURCE_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_RESOURCE));
        System.assertEquals(AseCalloutConstants.ASE_TYPE_USERLOGIN_STR.toLowerCase() , AseCallOutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_USERLOGIN));
    }
             
    @isTest
    public static void testGetClassPara()
    { 
        System.assertEquals(null, AseCallOutUtils.getClassObject(-1));
        System.assertEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_UNKNOWN));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_SCHEDULEPARA));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_CALENDAR));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_CALENDAR_ENTRY));
        System.assertEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_LOCATION));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_ORGUNIT));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_RESOURCE));
        System.assertNotEquals(null, AseCallOutUtils.getClassObject(AseCalloutConstants.ASE_TYPE_USERLOGIN));
    }        


    @isTest
    public static void testGetAseSoap()
    { 
        AseService.aseSOAP aseService = AseCallOutUtils.getAseSoap();
        System.assertNotEquals(null, aseService);
    }        
        
    @isTest
    public static void addStringsAsKeyVal2Entry()
    {
        AseService.aseDataEntry dEntry = new AseService.aseDataEntry();
        AseCalloutUtils.addStringsAsKeyVal2Entry('key1', 'val1', dEntry);

        dEntry = new AseService.aseDataEntry();
        dEntry.keyValues = new AseService.aseKeyValueType[0];
        AseCalloutUtils.addStringsAsKeyVal2Entry('key1', 'val1', dEntry);
    }
        
    @isTest
    public static void testConvertDate2StringDT()
    {
        DateTime dt = DateTime.newInstance(Date.newInstance(2009, 10, 09), time.newInstance(0, 0, 0, 0));
        System.assertEquals('20091009', AseCalloutUtils.convertDate2String(dt));
    }
        
    @isTest
    public static void testConvertDate2StringD()
    {
        Date d = Date.newInstance(2009, 10, 09);
        System.assertEquals('20091009', AseCalloutUtils.convertDate2String(d));
    }
        
    @isTest
    public static void testDayAndMonthIsGreaterOrEqualD()
    {
        Date d1 = Date.newInstance(2009, 10, 09);
        Date d2 = Date.newInstance(2008, 11, 12);
        Date d3 = Date.newInstance(2008, 11, 12);
        System.assert(!AseCalloutUtils.dayAndMonthIsGreaterOrEqual(d1, d2));
        System.assert(AseCalloutUtils.dayAndMonthIsGreaterOrEqual(d2, d3));
    }

    @isTest
    public static void testDayAndMonthIsLess()
    {
        Date d1 = Date.newInstance(2009, 10, 09);
        Date d2 = Date.newInstance(2008, 11, 12);
        System.assert(AseCalloutUtils.dayAndMonthIsLess(d1, d2));
    }

    @isTest
    public static void testDayAndMonthIsGreaterOrEqualDT()
    {
        DateTime dt1 = DateTime.newInstance(Date.newInstance(2009, 10, 09), time.newInstance(1, 4, 7, 0));
        DateTime dt2 = DateTime.newInstance(Date.newInstance(2008, 11, 12), time.newInstance(2, 5, 8, 0));
        DateTime dt3 = DateTime.newInstance(Date.newInstance(2008, 11, 12), time.newInstance(3, 6, 9, 0));
        System.assert(AseCalloutUtils.dayAndMonthIsGreaterOrEqual(dt2, dt1));
        System.assert(AseCalloutUtils.dayAndMonthIsGreaterOrEqual(dt2, dt3));
    }

    @isTest
    public static void testDayAndMonthIsLessOrEqualD()
    {
        Date d1 = Date.newInstance(2009, 10, 09);
        Date d2 = Date.newInstance(2008, 11, 12);
        Date d3 = Date.newInstance(2008, 11, 12);
        System.assert(AseCalloutUtils.dayAndMonthIsLessOrEqual(d1, d2));
        System.assert(AseCalloutUtils.dayAndMonthIsLessOrEqual(d2, d3));
    }

    @isTest
    public static void testDayAndMonthIsLessOrEqualDT()
    {
        DateTime dt1 = DateTime.newInstance(Date.newInstance(2009, 10, 09), time.newInstance(1, 4, 7, 0));
        DateTime dt2 = DateTime.newInstance(Date.newInstance(2008, 11, 12), time.newInstance(2, 5, 8, 0));
        DateTime dt3 = DateTime.newInstance(Date.newInstance(2008, 11, 12), time.newInstance(3, 6, 9, 0));
        System.assert(AseCalloutUtils.dayAndMonthIsLessOrEqual(dt1, dt2));
        System.assert(AseCalloutUtils.dayAndMonthIsLessOrEqual(dt2, dt3));
    }
   
 }