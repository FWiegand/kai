/*
 * @(#)SCutilPerformanceLog.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class saves the search params if the search takes longer than X ms
 * The X is defined in application setting SEARCHLOG_MINTIME
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */

public class SCutilPerformanceLog {

    public Datetime start;
    public Datetime stop;
    public String interfaceHandler;
    public String interfaceString;
    public String queryType;
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance(); 

   /** Constructor
     * @param interfaceHandler The search site name
     * @param interfaceString Internal parameter
     * @param queryType Type of the query (SOQL, etc.)
     */
    public SCutilPerformanceLog(String interfaceHandler, String interfaceString, String queryType)
    {
        this.interfaceHandler = interfaceHandler;
        this.interfaceString  = interfaceString;
        this.queryType        = queryType;
        start = Datetime.now();
    }
    
   /**
     * Resets the start counter. Call this function direct before executing the time
     * critical functions. If the method is not called the default value from the 
     * constructor is used.
     */
    public void start()
    {
        start = Datetime.now();
    }


   /**
     * Calculates the call duration and creates a new SCInterfaceLog record
     */
    public String stop()
    {
        stop = Datetime.now();
        return save(null, null);
    }
    
   /**
     * Calculates the call duration and creates a new SCInterfaceLog record
     * @param count The number of founded lines
     * @param querySoql The whole query
     */
    public String stop(String querySoql, Integer count)
    {
        stop = Datetime.now();
        return save(querySoql, count);
    }

   /**
     * Calculates the call duration and creates a new SCInterfaceLog record
     * but only if the duration exceeds SCApplikcationSetting.SEARCHLOG_MINTIME__c 
     * @param count The number of founded lines
     * @param querySoql The whole query
     */
    public String stopEx(String querySoql, Integer count)
    {
        stop = Datetime.now();
        
        if((stop.getTime() - start.getTime()) >= applicationSettings.SEARCHLOG_MINTIME__c )
        {
            return save(querySoql, count);
        }
        return null;
    }

   /**
     * Create a nedw SCInterfaceLog record and saves the values.
     * @param count The number of founded lines
     * @param querySoql The whole query
     */
    private String save(String querySoql, Integer count)
    {
        SCInterfaceLog__c log;    
        try
        {
            log = new SCInterfaceLog__c(
                        Interface__c = interfaceString,
                        InterfaceHandler__c = interfaceHandler,
                        Data__c = querySoql,
                        Count__c = count,
                        start__c = start,
                        stop__c = stop,
                        duration__c = stop.getTime() - start.getTime(),
                        Type__c = queryType,
                        Direction__c = 'internal'
            );
            insert log;
        }
        catch (Exception e)
        {
        }
        return log.id;
    }
}