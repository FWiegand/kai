/*
 * @(#)SCOrderRecalculatePriceExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCOrderRecalculatePriceExtension
{
    ApexPages.StandardSetController con;

    /**
     *
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public SCOrderRecalculatePriceExtension(ApexPages.StandardSetController controller)
    {
        con = controller;
        con.setPageSize(100);
    }

    /**
     *
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     */
    public integer getSelectedCount() 
    {
        return con.getSelected().size();
    }

    /**
     *
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference onContinue()
    {
        String orderId = ApexPages.currentPage().getParameters().get('id');
        List<SCOrderLine__c> selectedOrderLines = (List<SCOrderLine__c>)con.getSelected();


        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(orderId);
        
        SCPriceCalculationCustom pricing = new SCPriceCalculationCustom(boOrder);
        
        /*
         GENERAL REMARK (TK):
            Why do we need all the loops although we get the SCOrderLine directly
            from the getSelected() call?
            
            The calculate pricing needs a couple of fields to assure that the pricing
            can be calculated correctly (e.g. Qty). In case of a standard controller 
            only fields that are mentioned in the called page are part of the object.
            You therefore would have to mention all fields that are needed in the pricing
            on your page and adjust the page in case this changes!
            
            Hence we read all order lines that belong to this order using our BO
            assuring that the required fields are part of the object and the recalculate
            the prices for the selected order lines only.
        */
        
        Set<Id> selectedLineIds = new Set<Id>();

        // Collect all order line IDs that are in our selected set
        // for easier checking
        for (SCOrderLine__c line : selectedOrderLines)
        {
            selectedLineIds.add(line.Id);
        }

        List<SCboOrderLine> orderLinesToProcess = new List<SCboOrderLine>();
        
        for(SCboOrderLine boOrderLine : boOrder.boOrderLines)
        {
            if (selectedLineIds.contains(boOrderLine.orderLine.Id))
            {
                orderLinesToProcess.add(boOrderLine);
            }
        }

        for(SCboOrderItem boOrderItem : boOrder.boOrderItems)
        {
            for (SCboOrderLine boOrderLine : boOrderItem.boOrderLines)
            {
                if (selectedLineIds.contains(boOrderLine.orderLine.Id))
                {
                    orderLinesToProcess.add(boOrderLine);
                }
            }
        }
        
        
        for (SCboOrderLine boOrderLine : orderLinesToProcess)
        {
            boOrderLine.orderLine = pricing.calculate(boOrderLine.orderLine);
        }
        
        boOrder.save();
                
        PageReference page = new PageReference('/' + orderId);
        page.setRedirect(true);
        
        return page;
    }

    /**
     *
     * @author Norbert Armbruster <narmbruster@gms-online.de>
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference onCancel()
    {
        String orderId = ApexPages.currentPage().getParameters().get('id');
        PageReference page = new PageReference('/' + orderId);
        page.setRedirect(true);
        return page;
    }
    
}