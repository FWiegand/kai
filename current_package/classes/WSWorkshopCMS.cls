/*
 * @(#)WSWorkshopCMS.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/** *
 * WSWorkshopCMS 
 * Workshop mobile service supporting business rules / implementation
 * 
 */
global without sharing class WSWorkshopCMS 
{
    //-------------------------------------------------------------------------
    // API getConfiguration - Returns domainvalues and their translations
    //-------------------------------------------------------------------------    


    WebService static String ping(String value) 
    {
        return value + '*';
    } 
    
    /** 
     * Demo web service adding two values
     * @param value1    
     * @param value2    
     * @return the sum value 1 and 2
     *                      
     */
    WebService static Integer add(Integer value1, Integer value2) 
    {
        Integer result = value1 + value2;
    
        return result;
    } 


    /** 
     * Reads the installed base based on the serial number and external id
     * @param value     the serial number, idext or id of the installed base (can be comma separated)
     * @return list of installed base records
     *                      
     */
    WebService static SCInstalledBase__c readInstalledBase(String value) 
    {
        SCInstalledBase__c[] results  = [
        select Id, Name, SerialNo__c, SerialNoException__c, InstallationDate__c, 
            IdInt__c, ProductNameCalc__c, StockInfo__c, Plant__c
        from SCInstalledBase__c where SerialNo__c = :value];
        
        return results != null && results.size() > 0 ? results[0] : new SCInstalledBase__c();
    } 
    
    /** 
     * Reads the list of brands
     * @return list of brands
     */
    WebService static Brand__c[] readAllBrands()
    {
        Brand__c[] brandList  = [ select ID2__c,Name from Brand__c ];
        return brandList;
    }
    
    /** 
     * Change the brand of an equipment
     * @param equip   the equipment number of the device selected to work with
     * @param orderNo the order number, if an existing order is to be continued. Empty for new order.
     * @return order number, status, type
     */
    WebService static SCOrder__c createOrder(String equip, String orderNo)
    {
        // TODO implement
        SCOrder__c results  = [
        select OriginalOrderNumber__c, Status__c, Type__c from SCOrder__c where Name = :orderNo];
        
        return results;
    }
    
    /** 
     * Change the brand of an equipment
     * @param equip   the equipment number of the device
     * @param brand   the new brand id of the device
     * @return nothing
     */
    WebService static void changeEquipmentBrand(String equip, String brand)
    {
        // TODO implement
        // no output
    }
}