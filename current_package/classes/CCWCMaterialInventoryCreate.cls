/*
 * @(#)CCWCMaterialInventoryCreate.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class is used to add an order external assignment in SAP of an existing order
 * 
 * To start the callout:
 *   CCWCMaterialInventoryCreate.callout(String inventoryId, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCMaterialInventoryCreate extends SCInterfaceExportBase
{

    public CCWCMaterialInventoryCreate ()
    {
    }
    public CCWCMaterialInventoryCreate (String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    public CCWCMaterialInventoryCreate (String interfaceName, String interfaceHandler, String refType)
    {
        //super(interfaceName, interfaceHandler, refType, subclassName);
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param oid   inventory id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String inventoryId, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_MATERIAL_INVENTORY';
        String interfaceHandler = 'CCWCMaterialInventoryCreate';
        String refType = 'SCInventory__c';
        CCWCMaterialInventoryCreate oc = new CCWCMaterialInventoryCreate(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(inventoryId, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialInventoryCreate');
    } // callout   

    /**
     * Reads an order to send
     *
     * @param inventoryId
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String inventoryId, Map<String, Object> retValue, Boolean testMode)
    {
        debug('inventoryId: ' + inventoryId);

        List<SCInventory__c> oeal =  [Select ID, Name, FiscalYear__c, Plant__c, Stock__c, Stock__r.Name, InventoryDate__c,
                                                    PlannedCountDate__c, 
                                                  (select ID, Name, Article__r.Name, CountedQty__c, Article__r.Unit__c, ValuationType__c
                                                  from InventoryItem__r)
                                from SCInventory__c where ID = : inventoryId];
        if(oeal.size() > 0)
        {
            retValue.put(ROOT, oeal[0]);
            debug('inventory: ' + retValue);
            setReferenceID2(oeal[0].Stock__c);
            setReferenceType2('SCStock__c');
        }
        else
        {
            String msg = 'The inventory with id: ' + inventoryId + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
     
        SCInventory__c inventoryObject = (SCInventory__c)dataMap.get(ROOT);
        List<SCOrderExternalAssignmentItem__c> itemList = (List<SCOrderExternalAssignmentItem__c>) dataMap.get(KEY_LIST);
        // instantiate the client of the web service
        piCceagDeSfdcCSMaterialInventoryCreate.HTTPS_Port ws = new piCceagDeSfdcCSMaterialInventoryCreate.HTTPS_Port();

        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('MaterialInventoryCreate');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        
        try
        {
            // instantiate a message header
            piCceagDeSfdcCSMaterialInventoryCreate.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialInventoryCreate.BusinessDocumentMessageHeader();
            setMessageHeader(messageHeader, inventoryObject, messageID, u, testMode);
            
            // instantiate the body of the call
            piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocument_element inventoryElement = new piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocument_element();
            
            // set the data to the body
            // rs.message_v3 = 
            setExternalAssignment(inventoryElement, inventoryObject, u, testMode);

            String jsonInputMessageHeader = JSON.serialize(messageHeader);
            String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
            debug('from json Message Header: ' + fromJSONMapMessageHeader);

            String jsonInputOrder = JSON.serialize(inventoryElement);
            String fromJSONMapOrder = getDataFromJSON(jsonInputOrder);
            debug('from json Order: ' + fromJSONMapOrder);
            
            rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJSONMapOrder ;
            debug('rs.message: ' + rs.message);
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                debug('message_v3 ok');
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    debug('real mode');
                    // It is not the test from the test class
                    // go
                    rs.setCounter(1);
                    ws.InventoryDocumentCreate_Out(messageHeader, inventoryElement);
                    rs.Message_v2 = 'void';
                }
            }
            debug('after sending');
        }
        catch(Exception e) 
        {
            throw e;
        }
        return retValue;
    }
    
    /**
     * Sets the ERPStatus in the root object to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        if(dataMap != null)
        {
            SCInventory__c inv = (SCInventory__c)dataMap.get(ROOT);
            if(inv != null)
            {
                if(error)
                {
                    inv.ERPStatus__c = 'error';
                }
                else
                {   
                    inv.ERPStatus__c = 'pending';
                }   
                update inv;
                debug('inventory after update: ' + inv);
            }
        }    
    }

    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSMaterialInventoryCreate.BusinessDocumentMessageHeader mh, 
                                         SCInventory__c inventoryObject, String messageID, CCWSUtil u, Boolean testMode)
    {
        mh.MessageID = messageID;
//      mh.MessageUUID;
        mh.ReferenceID = inventoryObject.Name;
//      mh.ReferenceUUID;
        mh.CreationDateTime = u.getFormatedCreationDateTime();
        if(testMode)
        {
            mh.TestDataIndicator = 'Test';
        }    
        mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
        mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
        debug('Message Header: ' + mh);
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param cso callout order sturcture
     * @param order order with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCreateTest class
     *
     */
    public String setExternalAssignment(piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocument_element invDoc, 
                                        SCInventory__c inv, 
                                        CCWSUtil u, Boolean testMode)
    {
        String retValue = '';
        String step = '';
        try
        {
            invDoc.FiscalYearID = inv.FiscalYear__c;
            invDoc.Plant = inv.Plant__c; 
            invDoc.StorageLocation = inv.Stock__r.name;
            invDoc.DocumentDate = inv.InventoryDate__c;
            invDoc.PlannedCountDate = inv.PlannedCountDate__c;
            invDoc.PhysicalInventoryReferenceNumber = inv.Name;

            invDoc.Items = new piCceagDeSfdcCSMaterialInventoryCreate.Items_element();
            invDoc.Items.Item = new List<piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentItem>();

            List<SCInventoryItem__c> itemList = inv.InventoryItem__r;
            for(SCInventoryItem__c item: itemList)
            {
                piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentItem el = 
                                                        new piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentItem();
                el.ItemID = item.Name;
                el.MaterialID = item.Article__r.Name;
                if(item.CountedQty__c != null)
                {
                    el.Quantity = '' + item.CountedQty__c.stripTrailingZeros().toPlainString();        // remove zeros and dezimal places
                }
                else
                {
                    el.Quantity = '0'; 
                }
                el.EntryUnitOfMeasure = u.getSAPUnitOfMeasure(item.Article__r.Unit__c);
                el.Batch = item.ValuationType__c;
                invDoc.Items.Item.add(el);
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setExternalAssignment: ' + step + ' ' + prevMsg;
            String stack = SCfwException.getExceptionInfo(e);
            newMsg += stack;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }

        return retValue;    
    }

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}