/* 
 * @(#)SCOrderChangeRoleTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.  
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use  
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */
 
@isTest
private class SCOrderChangeRoleTest
{
    static testMethod void testChangeRole1()
    {
        List<account> accountList = [SELECT name FROM Account LIMIT 1];
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(accountList);
        
        TestHelper testHelper = new TestHelper();
        Account account1 = testHelper.createAccountObject(1);
        testHelper.setAccount(account1);
        Account account2 = testHelper.createAccountObject(2);
        String orderStatus = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        String orderType = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        String invoicingStatus = SCfwConstants.DOMVAL_INVOICING_STATUS_OPEN;
        Id id = testHelper.createOrder( orderStatus, orderType, invoicingStatus );
        testHelper.createOrderRoles();
        ApexPages.currentPage().getParameters().put('id', id);

        SCOrderChangeRole obj = new SCOrderChangeRole(ssc);
        obj.orderRole = SCfwConstants.DOMVAL_ORDERROLE_AG;
        //Account to change
        String oldId = (String)obj.roles.get(1).Account__c;
        obj.value = (String)account2.Id;
        obj.processCustomer();
        
        //Changed order role account
        String newId = obj.roles.get(1).Account__c;
        //New Id should not be equal to old Id
        System.assertNotEquals(oldId, newId);
        System.assertEquals(true, obj.canModifyRoles );
        
        oldId = (String)obj.roles.get(1).Account__c;
        obj.value = (String)account2.Id;
        obj.changeRole();
        newId = obj.roles.get(1).Account__c;
        //New Id should not be equal to old Id
        System.assertEquals(oldId, newId);
        
        // for code coverage
        obj.onContinue();
        obj.getRoleAccountIds();
        obj.orderRole = SCfwConstants.DOMVAL_ORDERROLE_RE;
        //Account to change
        obj.value = (String)account2.Id;
        obj.processCustomer();
    }

    static testMethod void testChangeRole2()
    {
        List<account> accountList = [SELECT name FROM Account LIMIT 1];
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(accountList);
        
        TestHelper testHelper = new TestHelper();
        Account account1 = testHelper.createAccountObject(1);
        testHelper.setAccount(account1);

        String orderStatus = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL;
        String orderType = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        String invoicingStatus = SCfwConstants.DOMVAL_INVOICING_STATUS_OPEN;
        Id id = testHelper.createOrder( orderStatus, orderType, invoicingStatus );
        testHelper.createOrderRoles();
        ApexPages.currentPage().getParameters().put('id', id);
        SCOrderChangeRole obj = new SCOrderChangeRole(ssc);
        System.assertEquals(false, obj.canModifyRoles );
        
        orderStatus = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        orderType = SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_COPY;
        id = testHelper.createOrder( orderStatus, orderType, invoicingStatus );
        testHelper.createOrderRoles();
        ApexPages.currentPage().getParameters().put('id', id);
        new SCOrderChangeRole(ssc);
        
        orderStatus = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        orderType = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT;
        invoicingStatus = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
        id = testHelper.createOrder( orderStatus, orderType, invoicingStatus );
        testHelper.createOrderRoles();
        ApexPages.currentPage().getParameters().put('id', id);
        new SCOrderChangeRole(ssc);

    }
    
    public class TestHelper
    {
        private SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        private SCOrder__c order;
        private Account acc;
        private List<SCOrderRole__c> orderRoles;        
        private SCPricelist__c priceList;
        
        public Id createOrder(String orderStatus, String orderType, String invoicingStatus){
            order = new SCOrder__c
                          (
                            CustomerPrefStart__c = Date.today(),
                            CustomerPrefEnd__c = Date.today()+1,
                            CustomerTimewindow__c = SCfwConstants.DOMVAL_CUSTOMERTIMEWINDOW_DEFAULT,
                            CustomerPriority__c = SCfwConstants.DOMVAL_ORDERPRIO_DEFAULT
                          );
    
            // set default values that are global
            order.InvoicingStatus__c = invoicingStatus;
            order.Status__c = orderStatus;
            
    
            // set default values that depend on different levels
            order.InvoicingType__c = appSettings.DEFAULT_INVOICINGTYPE__c;
            order.InvoicingSubType__c = appSettings.DEFAULT_INVOICINGSUBTYPE__c;
            order.PaymentType__c = appSettings.DEFAULT_PAYMENTTYPE__c;
            order.Type__c = orderType;
            order.MaterialStatus__c = SCfwConstants.DOMVAL_MATSTAT_ORDER;
            order.Country__c = appSettings.DEFAULT_COUNTRY__c;
            
            insert order;
            
            return order.Id;
        }
        
        public void createOrderRoles()
        {
            SCboOrder boOrder = new SCboOrder(order);
            boOrder.createDefaultRoles(this.acc);
    
            orderRoles = boOrder.allRole;
    
            upsert orderRoles;
        }
    
        public Account createAccountObject(Integer test)
        {
            Account account = new Account();

            //Set Lastname for PersonAccount
            account.LastName__c = 'testaccount 00';

            // Fields GMS
            if(test == 1)
            {
                account.FirstName__c = 'TestPerson1';
            }
            else
            {
                account.FirstName__c = 'TestPerson2';
            }
    
            account.RecordTypeId = [Select Id from RecordType where DeveloperName = :'Customer'  and SobjectType = 'Account'].Id;
            account.TargetGroup__c = '001';
            account.SubTargetGroup__c = 'NL-005';
            account.BillingPostalCode = '9831 NK';
            account.BillingCountry__c = 'NL';
            account.CurrencyIsoCode = 'EUR';
    
            // Fields SF42
    
            // Fields GMS
            account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
            account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
            account.BillingStreet = 'Moormanweg';
            account.BillingHouseNo__c = '6';
            account.BillingPostalCode = '9831 NK';
            account.BillingCity = 'Aduard';
            account.BillingCountry__c = 'NL';
            account.GeoX__c = 6.454447;
            account.GeoY__c = 53.253582;

            this.createPricelist();
            if (priceList <> null && priceList.Id <> null)
            {
                account.DefaultPriceList__c = priceList.Id;
                account.DefaultPriceList__r = priceList;
            }
    
            // Fields Arlanis
            insert account;
            
            if(test == 1)
            {
                return [SELECT Id FROM Account WHERE FirstName__c = 'TestPerson1'];
            }
            else
            {
                return [SELECT Id FROM Account WHERE FirstName__c = 'TestPerson2'];
            }
            
        }
        
        public void setAccount(Account acc){
            this.acc = acc;
        }

        public void createPricelist ()
        {
            String priceListName = 'Standard';
    
            if (appSettings.DEFAULT_PRICELIST__c <> null &&
                appSettings.DEFAULT_PRICELIST__c.length() > 0)
            {
                priceListName = appSettings.DEFAULT_PRICELIST__c;
            }
    
            try
            {
                priceList = [select Id, Name from SCPriceList__c where Name = :priceListName];
            }
            catch (Exception e)
            {
                priceList = new SCPriceList__c(Name = priceListName);
                insert priceList;
            }
        }
    

    }
}