/*
 * @(#)SCConfOrderPrioLoader.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Used to initialize the country specific matrix for the order priorisation.
 * The matrix is evaluated during the order creation to determine the 
 * earliest necesssary start date for scheduling engineers depending on the 
 * current context (customer, situation, failure type). 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCConfOrderPrioLoader
{
    // used to reduce the number of db accesses 
    // private static boolean initialized = false; 

   /*
    * Initializes the SCConfOrderLine object. 
    * @param country the 2 digit country code
    */
    public static void initialize(String country)
    {    
        // only access the database the first time
        // if(!initialized)
        {
            Integer count = [select count() from SCConfOrderPrio__c where country__c = :country];
            if(count <= 0)
            {
                Data doc = new Data(country);
                
                doc.startCategory('Partner');
                doc.addTitle(10,'extern');
                doc.addItem(30,'Fachpartner Platin',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(40,'Fachpartner Gold',8,null,true,null,null,null,false,null,null,null);
                doc.addItem(50,'Fachpartner VEP',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(60,'Fachpartner Basis',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(70,'GH',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(80,'Baugesellschaft/Bauträger/WoBauGes',8,null,true,null,null,null,false,null,null,null);
                doc.addItem(90,'Endkunde / Eigentümer',7,null,true,null,null,null,false,null,null,null);
                doc.addItem(100,'Endkunde / Mieter',2,null,true,null,null,null,false,null,null,null);
                doc.addItem(110,'Sachverständige',2,null,true,null,null,null,false,null,null,null);
                doc.addItem(120,'Behörden / Kripo etc.',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(130,'Meinungsbildner',10,null,true,null,null,null,true,null,null,null);
                doc.addItem(140,'"Eskalations" - Kunde',10,null,true,null,null,null,false,null,null,null);
                doc.addTitle(150,'intern');
                doc.addItem(160,'GF',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(170,'VKL / Leiter KAM´s',8,null,true,null,null,null,false,null,null,null);
                doc.addItem(180,'VKB´s + KAM´s',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(190,'MA aus Beschwerde Management',10,null,true,null,null,null,false,null,null,null);

                doc.startCategory('Situation');
                doc.addTitle(1000,'Nutzungszweck');
                doc.addSubtitle(1010,'privat');
                doc.addItem(1020,'EFH/ZFH/Wohnung',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(1030,'MFH (ab 3 Parteien)',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(1040,'nicht bewohnte Räumlichkeiten',2,null,true,null,null,null,false,null,null,null);
                doc.addSubtitle(1050,'gewerblich');
                doc.addItem(1060,'für den Geschäftszweck notwendig',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(1070,'nicht für den Geschäftszweck notwendig',6,null,true,null,null,null,false,null,null,null);
                doc.addTitle(1080,'Wirtschaftlichkeitsziele vorhanden');
                doc.addItem(1090,'BAFA Förderung abh. vom Output',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(1100,'entgangener Erlös/Ertrag',8,null,true,null,null,null,false,null,null,null);
                doc.addItem(1110,'entgangene Energieeinsparung',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(1120,'keine Wirtschaftlichkeitsziele',1,null,true,null,null,null,false,null,null,null);
                doc.addTitle(1130,'Folgeschäden möglich');
                doc.addItem(1140,'nein',1,null,true,null,null,null,false,null,null,null);
                doc.addItem(1150,'nur am Gerät',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(1160,'auch an Wohn-/Geschäftsraumausstattung',7,null,true,null,null,null,false,null,null,null);
                doc.addItem(1170,'Absatzausfall Vaillant',8,null,true,null,null,null,true,null,null,null);
                doc.addItem(1180,'hoher Haftpflichtschaden',9,null,true,null,null,null,true,null,null,null);
                doc.addItem(1190,'Gefahr für Leib und Leben',10,null,true,null,null,null,true,null,null,null);
                doc.addSeparator(2000);
                doc.addTitle(2010,'Auf-/Abwertung');
                doc.addSubtitle(2020,'Mehfacheinsatz');
                doc.addItem(2030,'Zweiter Einsatz (innerh. 30 Tagen)',null,1.15d,true,null,null,null,false,null,null,null);
                doc.addItem(2040,'ab drittem Einsatz (innerh. 30 Tagen)',null,1.3d,true,null,null,null,false,null,null,null);
                doc.addItem(2050,'Zweiter Einsatz innerh. letzter 24 h',null,1.3d,true,null,null,null,false,null,null,null);
                doc.addSubtitle(2060,'Produkt-Lebenszyklus');
                doc.addItem(2070,'innerh. Garantie',null,1.3d,true,null,null,null,false,null,null,null);
                doc.addItem(2080,'außerhalb Garantie',null,1,true,null,null,null,false,null,null,null);
                doc.addItem(2090,'dito; Kunde hat keinen FHW',null,1.2d,true,null,null,null,false,null,null,null);
                doc.addItem(2100,'älter 3 Jahre; WKD war noch nie da',null,1.2d,true,null,null,null,false,null,null,null);
                doc.addItem(2110,'älter als 15 Jahre',null,1.2d,true,null,null,null,false,null,null,null);
                doc.addItem(2120,'monovalent',null,1,true,null,null,null,false,null,null,null);
                doc.addItem(2130,'bivalent',null,0.75d,true,null,null,null,false,null,null,null);
                doc.addSubtitle(2140,'Vaillant Wartungskunde');
                doc.addItem(2150,'hat Wartungs-/Vollservice-Vertrag',null,1.3d,true,null,null,null,false,null,null,null);

                doc.startCategory('FailureType');
                doc.addTitle(3000,'keine Heizung');
                doc.addItem(3010,'Winter',9,null,true,null,null,null,false,null,null,null);
                doc.addItem(3020,'Sommer - keine Heizung',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(3030,'kein Warmwasser',8,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3040,'keine Heizung & kein Warmwasser');
                doc.addItem(3050,'Winter',10,null,true,null,null,null,false,null,null,null);
                doc.addItem(3060,'Sommer',8,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3070,'zeitw. keine Funktion (geht auf Störung)');
                doc.addItem(3080,'selten',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(3090,'häufig',7,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3100,'undicht');
                doc.addItem(3110,'tropft leicht',2,null,true,null,null,null,false,null,null,null);
                doc.addItem(3120,'tropft stark',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(3130,'läuft',8,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3140,'Geräusche');
                doc.addSubtitle(3150,'Intensität');
                doc.addItem(3160,'leise / kaum wahrnehmbar',2,null,true,null,null,null,false,null,null,null);
                doc.addItem(3170,'mäßig / im Wohnraum wahrnehmbar',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(3180,'laut / im Wohnraum schwer erträglich',10,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3190,'geringe Leistung');
                doc.addItem(3200,'nicht die volle Temperatur',2,null,true,null,null,null,false,null,null,null);
                doc.addItem(3210,'nur mäßig warm',4,null,true,null,null,null,false,null,null,null);
                doc.addItem(3220,'fast kalt',5,null,true,null,null,null,false,null,null,null);
                doc.addItem(3230,'keine Kühlung',7,null,true,null,null,null,false,null,null,null);
                doc.addTitle(3240,'Lüftung');
                doc.addItem(3250,'keine Lüftung',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(3260,'macht Wohnung kalt',6,null,true,null,null,null,false,null,null,null);
                doc.addItem(3270,'keine Regelung',5,null,true,null,null,null,false,null,null,null);
                doc.addItem(3280,'Energieverlust',5,null,true,null,null,null,false,null,null,null);
                doc.addItem(3290,'Sonstiges',3,null,true,null,null,null,false,null,null,null);
                
                doc.startCategory('Config');
                doc.addConfig(9000, '2103',  0,3);
                doc.addConfig(9010, '2101',250,2);
                doc.addConfig(9020, '2101',500,1);
                doc.addConfig(9030, '2102',750,0);
                
                doc.write();
            
                // now initialize the data
            }
            //initialized = true;
        }
    }


   /*
    * Internal helper class to create the data.
    */    
    private class Data
    {
        // the current country (see country__c)
        String country;
        // the current category (see category__c)
        String category;
        // the list of items used to add the records
        List<SCConfOrderPrio__c> items;

       /*
        * Contructs the object and sets the default country
        * @param c the country code (see country__c)   
        */
        public Data(String c)
        {
            country = c;
            items = new List<SCConfOrderPrio__c>(); 
        }

       /*
        * Starts a new catgegory that is used for structuring the items
        * @param category (see category__c picklist) exampltes: Partner, Situation and FailureType
        */
        public void startCategory(String val)          
        {
            category = val;
        }
        
       /*
        * Add the title of items within a category
        * @param sortorder  the display index 
        * @param text the name of the item 
        */
        public void addTitle(Integer sortorder, String text)
        {
            add(sortorder, null, null, text, 'Title', false, null, null, null, false, null, null, null);
        }
       /*
        * Add a subtitle item 
        * @param sortorder  the display index 
        * @param text the name of the item 
        */
        public void addSubtitle(Integer sortorder, String text)
        {
            add(sortorder, null, null, text, 'Subtitle', false, null, null, null, false, null, null, null);
        }
       /*
        * Add a separator
        * @param sortorder  the display index 
        */
        public void addSeparator(Integer sortorder)
        {
            add(sortorder, null, null, null, 'Subtitle', false, null, null, null, false, null, null, null);
        }

       /*
        * Add the item 
        * @param sortorder  the display index 
        * @param text the name of the item 
        * @param (details about the fields - please see SCConfOrderPrio__c object)
        */
        public void addItem(Integer sortorder, String text, Integer score, Double factor, Boolean multisel, String preselobj, String preselfield, String preselvalue, boolean kocriteria, String orderprio, Integer priovalue, Integer offset)
        {
            add(sortorder, score, factor, text, 'Item', multisel, preselobj, preselfield, preselvalue, kocriteria, orderprio, priovalue, offset);
        }


        public void addConfig(Integer sortorder, String orderprio, Integer priovalue, Integer offset)
        {
            add(sortorder, null, null, null, 'Item', false, null, null, null, false, orderprio, priovalue, offset);
        }

        
       /*
        * Internal helper
        * @param sortorder  the display index 
        * @param text the name of the item 
        * @param (details about the fields - please see SCConfOrderPrio__c object)
        */
        public void add(Integer sortorder, Integer score, Double factor, String text, String typ, Boolean multisel, String preselobj, String preselfield, String preselvalue, boolean kocriteria, String orderprio, Integer priovalue, Integer offset)
        {
            SCConfOrderPrio__c i = new SCConfOrderPrio__c();

            i.country__c = country;
            i.category__c = category;
            i.type__c = typ;
            i.selecttype__c = multisel ? 'Multiple select' : 'Single select';
            
            //TODO: localize
            //i.text_cs__c;
            //i.text_da__c;
            i.text_de__c = text;
            i.text_en__c = text;
            //i.text_es__c;
            //i.text_fr__c;
            //i.text_hr__c;
            //i.text_hu__c;
            //i.text_it__c;
            //i.text_nl__c;
            //i.text_pl__c;
            //i.text_ro__c;
            //i.text_ru__c;
            //i.text_sk__c;
            //i.text_sl__c;
            //i.text_tr__c;
            //i.text_uk__c;
            //i.text_zh__c;
            //i.description__c;
            i.preselobj__c = preselobj;
            i.preselfield__c = preselfield;
            i.preselvalue__c = preselvalue;
            i.sortorder__c = sortorder;
            i.score__c = score != null ? String.valueof(score) : null;
            i.factor__c = factor;
            i.kocriteria__c = kocriteria;
            i.orderprio__c = orderprio;
            i.priovalue__c = priovalue;
            i.offset__c = offset;
            //i.id2__c;
            
            items.add(i);
        }
        
        public void write()
        {
            if(items.size() > 0)
            {
            
                insert items;        
            }
        }
    }
    
}