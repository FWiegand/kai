/**
 * @(#)AseSetValue.cls    ASE1.0 hs 14.02.2011
 * 
 * Copyright (c) 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: AseSetValue.cls 7692 2011-02-14 16:18:41Z hschroeder $
 * 
 */
global class AseSetValue
{
    private String id;
    private String key;
    private String value;
    
    /**
     * Data fields used by the scheduling engine
     */
    private String sessionID = 'gwin-0';
         
    /**
     * Context meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaContext = new String[]
    {
        'id',
        'userid'
    };
    
    /**
     * Job param meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaParam = new String[]
    {
        'id',
        'key',
        'value' 
    };
    
    /**
     * Sets callout parameters
     *
     * @author HS <hschroeder@gms-online.de>
     */
    public void setParams(String id, String key, String value)
    {
        this.id = id;
        this.key = key;
        this.value = value; 
    }
    
    /**
     * Make the webservice call out
     *
     * @author HS <hschroeder@gms-online.de>
     */
    public static void callout(String id, String key, String value)
    {
        AseSetValue job = new AseSetValue();
        job.setParams(id, key, value);
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        
        // prepare context
        AseService.aseDataType contextParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String metaKey : job.metaContext)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = metaKey;
            keyValue.value = job.getValueCtx(metaKey) != null ? job.getValueCtx(metakey) : '';
            System.debug('Context: ' + metaKey + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        contextParam.dataEntries = new AseService.aseDataEntry[0];
        contextParam.dataEntries.add(dataEntry);
        contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
        
        // job params
        AseService.aseDataType jobParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String metaKey : job.metaParam)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = metaKey;
            keyValue.value = job.getValue(metaKey) != null ? job.getValue(metaKey) : '';
            System.debug('Job param: ' + metaKey + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        jobParam.dataEntries = new AseService.aseDataEntry[0];
        jobParam.dataEntries.add(dataEntry);
        jobParam.type_x = AseCalloutConstants.ASE_TYPE_STATUS_STR;
        
        AseService.aseDataType[] params = new AseService.aseDataType[]
        { 
            contextParam, jobParam
        };
        
        AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
        aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
        aseSoap.set(tenant, params);
    }
    
    /**
     * Initialize the demand with the current user
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String getUserID()
    {
        String userID = UserInfo.getUserId();
        User user = [ select id, alias from user where user.id = :userID limit 1 ];
        return user.alias;
    }
   
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValueCtx(String key)
    {
        // meta key
        if (key == 'id') return sessionID;
        if (key == 'userid') return getUserID();

        return null;
    }
    
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValue(String key)
    {
        // param key        
        if (key == 'id') return this.id;
        if (key == 'key') return this.key;
        if (key == 'value') return '' + this.value;
      
        return null;
    }         
}