/*
 * @(#)SCbtcContractOrderDoubleCancel.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Algorithm. The SCInterfaceLog__c records are read for the Start__c between time from and to
 * and Interface__c = 'CONTRACT_ORDER'.
 * For every Order__c we check whether the order is in some SCContractVisit__c record. If it is not there
 * the order is cancelled.
 * 
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcContractOrderDoubleCancel extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private ID batchprocessid = null;
    private String mode = 'productive';
    String country = 'DE';
    Date dateFrom = null;
    Date dateTo = null;

    Integer max_cycles = 0;    
    List<String> visitIdList = null;    // visitIdList = null, normal batch call
                                        // visitIdList != null, orders are repaird only for the list
                                        // of visits    

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    private Id contractId = null;

    /**
     * Entry point for starting the apex batch repairing all contracts 
     * @param dateFrom inclusive e.g. 20120102
     * @param dateTo   inclusive e.g. 20120103
     * @param max_cycles 0 alle records from a query result
     *                   k > 0 only the first k records from a query result
     * @param mode could be 'trace', 'test' or 'productive'
     */
    public static ID cancel(String dateFrom, String dateTo, Integer max_cycles, String mode)
    {
        String department =  null;
        SCbtcContractOrderDoubleCancel btc = new SCbtcContractOrderDoubleCancel(dateFrom, dateTo, max_cycles, mode);
        btc.batchprocessid = btc.executeBatch(1);
        return btc.batchprocessid;
    } // cancel


    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcContractOrderDoubleCancel(String dateFrom, String dateTo, Integer max_cycles, String mode)
    {
        this.dateFrom = SCBase.parseDate(dateFrom);
        this.dateTo = SCBase.parseDate(dateTo);
        this.dateTo = this.dateTo.addDays(1);
        this.max_cycles = max_cycles;
        this.mode = mode;
        
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        // one order will be created from one SContractVisit__c record 
        /* To check the syntax of SOQL statement */
        String interfaceName = 'CONTRACT_ORDER';
        List<SCInterfaceLog__c> ilList = [select ID, Order__c, Contract__c
                                            from SCInterfaceLog__c 
                                            where  Interface__c = :interfaceName and CreatedDate >= :dateFrom
                                            and CreatedDate < :dateTo
                                            limit 1];
        
        debug('start: after checking statement');
        /**/
        String query = null;
        query = ' select ID, Contract__c, Order__c '
              + ' from SCInterfaceLog__c where Interface__c = \'CONTRACT_ORDER\'';

       String dateFromLoc = SCBase.formatSOQLDate(dateFrom) +  'T00:00:00.000Z';
       debug('dateFromLoc: ' + dateFromLoc);
       String dateToLoc = SCBase.formatSOQLDate(dateTo) +  'T00:00:00.000Z';
       debug('dateToLoc: ' + dateToLoc);
       query += ' and CreatedDate >= ' + dateFromLoc 
                + ' and CreatedDate < ' + dateToLoc;
        if(max_cycles > 0)
        {
            query += ' limit ' + max_cycles;
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        executeCore(BC, scope);
    } // execute

    public void executeCore(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        if(scope != null)
        {
            for(sObject res: scope)
            {
                debug('sObject: ' + res);
                SCInterfaceLog__c il = (SCInterfaceLog__c)res;
                cancelOrder(il, interfaceLogList);
            }
            insert interfaceLogList;
        }     
    }

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Cancels the order referenced by an interface record if it has no contract visit referencing to it
     * and the order is cancellable. Two methods of SCboOrder are used:
     * canCancelOrder(id), and cancelOrder(id, ...)
     *  
     */
    public void cancelOrder(SCInterfaceLog__c il, List<SCInterfaceLog__c> interfaceLogList)
    {
        debug('cancelOrder');
        debug('order: ' + il);
        debug('orderID: ' + il.Order__c);
        debug('contractID: ' + il.Contract__c);
        if(il.Order__c == null || il.Contract__c == null)
        {
            return;
        }
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;
        String step = '';
        Boolean error = false;
        Boolean cancel = false;
        Boolean notAllowed = false;
        Savepoint sp = Database.setSavepoint();
        try
        {
            step = 'determine whether the order has a contract visit';
            debug(step);

            AggregateResult[] aggRes = [Select Count(ID) from SCContractVisit__c where Contract__c = :il.Contract__c
                                     and Order__c = :il.Order__c];

            integer cnt = 1;
            if(aggRes.size() > 0)
            {
                Double dSum = (Double)aggRes[0].get('expr0');
                if(dSum != null)
                {
                    cnt = dSum.intValue();
                }
                else
                {
                    cnt = 0;
                }
            }                                    
            if(cnt == 0)
            {
                // the order is in none of the visit, so it could be cancelled.
                debug('Check whether order can be cancelled.');
                String canCancel = SCboOrder.canCancelOrder(il.Order__c);
                if(canCancel == 'OK'
                   && !hasAssignments(il.Order__c))
                {
                    debug('Order can be cancelled');
                    cancel = true;
                    String cancelReason = '286002';
                    String cancelInfo = 'The double order has been cancelled.';
                    SCboOrder.cancelOrder(il.Order__c, cancelReason, cancelInfo);
                    debug('Order has been cancelled because it has no contract visit.');
                }
                else
                {
                    notAllowed = true;
                    resultInfo = 'The order can not be cancelled. Probably it has been already scheduled. Please check it.';
                }    
            }
            else
            {
                debug('Order not cancelled because it has its contract visit.');
            }
       }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: ' + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
            Database.rollback(sp);
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException || error || notAllowed)
            {
                resultCode = 'E101';
            }
            if(cancel || notAllowed)
            {
                logBatchInternal(interfaceLogList, 'CANCEL_DOUBLE_ORDER', 'SCbtcContractOrderDoubleCancel',
                                il.Contract__c, il.Order__c, resultCode, 
                                resultInfo, null, start, count); 
            }                    
         }
    }// cancel

    private Boolean hasAssignments(Id orderId)
    {
        Boolean ret = false;
          List <SCAssignment__c> assignments = [select Id, Status__c from SCAssignment__c where Order__c = :orderId and status__c in ('5501','5502','5503','5509','5510')];
        if(assignments != null && assignments.size() > 0)
        {
            ret = true;
        }
        return ret;
    }

    public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(interfaceName == 'CANCEL_DOUBLE_ORDER')
        {
            ic.Contract__c = referenceID;
            ic.Order__c = referenceID2;
        }
    }       

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }

}