/*
 * @(#)SCfwCollectionUtils.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Utitilities that are useful for Collections like join() or map()
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCfwCollectionUtils
{
    /**
     * Join all members of a set to a string delimited by a given 
     * string. 
     * 
     * Remember that a set has no order and therefore the
     * order within the returned string can't be predicted.
     *
     * @param toJoin Set of Strings that will be joined
     * @param delimiter delimiter to use to separate each substring
     * @return String of joined set entries
     */
    public static String join(Set<String> toJoin, String delimiter)
    {
        return join(toJoin, delimiter, '');
    }
    
    /**
     * Join all members of a set to a string delimited by a given 
     * string while each single element is enclosed in a text 
     * delimiter if given.
     *
     * Remember that a set has no order and therefore the
     * order within the returned string can't be predicted.
     *
     * @param toJoin Set of Strings that will be joined
     * @param delimiter delimiter to use to separate each substring
     * @return String of joined set entries
     */
    public static String join(Set<String> toJoin, String delimiter, String textDelimiter)
    {
        String joined = '';
        
        
        for (String row : toJoin)
        {
            joined += encloseText(row, textDelimiter) + delimiter;
        }
    
        if (joined.length() > 0)
        {
            // remove the last delimter    
            joined = joined.substring(0, joined.length() - 1);
        }
        
        return joined;
    }
    
    /**
     * Join all members of a list to a string delimited by a given 
     * string.
     *
     * @param toJoin List of Strings that will be joined
     * @param delimiter delimiter to use to separate each substring
     * @return String of joined set entries
     */
    public static String join(List<String> toJoin, String delimiter)
    {
        String joined = '';
        
        
        for (String row : toJoin)
        {
            joined += row + delimiter;
        }
    
        if (joined.length() > 0)
        {
            // remove the last delimter    
            joined = joined.substring(0, joined.length() - 1);
        }
        
        return joined;
    }

    private static String encloseText(String text, String enclosing)
    {
        if (enclosing == null || enclosing.length() == 0)
        {
            return text;
        }
        
        return enclosing + text + enclosing;
    }
    
    /**
     * Checks whether the item is contained in the list.
     * Returns true if it is there, else returns false
     * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
     */     
    public static Boolean isContained(String item, List<String> itemList)
    {
        Boolean ret = false;
        item = item.trim();
        if(item != null && item != '')
        {
            for(String loopItem: itemList)
            {
                loopItem = loopItem.trim(); 
                if(loopItem == item)
                {
                    ret = true;
                    break;
                }
            }
        }    
        return ret;
    }
    
    /**
     * Checks whether one of the itmes of the first list is contained in the second list.
     * Returns true if it is there, else returns false
     * @author Alexander Wagner <awagner@gms-online.de>
     */     
    public static Boolean isContained(List<String> srcList, List<String> destList)
    {
        Boolean ret = false;

        if ((null != srcList) && (null != destList))
        {
            for (String srcItem: srcList)
            {
                srcItem = srcItem.trim(); 
                for (String destItem: destList)
                {
                    destItem = destItem.trim(); 
                    if (srcItem == destItem)
                    {
                        ret = true;
                        break;
                    }
                }
            }
        } // if ((null != srcList) && (null != destList))   
        return ret;
    }
}