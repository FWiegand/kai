/*
 * @(#)SCSchedulingTest.cls SCCloud    dh 20.09.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCSchedulingTest
{  
    /**
     * Scheduling under test
     */
    private static SCScheduling schedulingTest;
    
    static testMethod void testScheduling()
    {
        schedulingTest = new SCScheduling();
        Date myDate = date.newinstance(1960, 2, 17);
        schedulingTest.Day = myDate;
        
        System.assert(schedulingTest.Day == myDate);
        System.assertEquals(false, schedulingTest.manuallyInput);
    }
}