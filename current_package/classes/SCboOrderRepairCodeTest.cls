/*
 * @(#)SCboOrderRepairCodeTest.cls 
 *  
 * Copyright 2010 by GMS Development GmbH 
 * Karl-Schurz-Strasse 29,  33100 Paderborn 
 * All rights reserved. 
 * 
 * This software is the confidential and proprietary information 
 * of GMS Development GmbH. ("Confidential Information").  You 
 * shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement 
 * you entered into with GMS. 
 */ 
 
/** 
 * 
 * @author Thorsten Klein <tklein@gms-online.de> 
 * @version $Revision$, $Date$ */ 
@isTest 
private class SCboOrderRepairCodeTest
{
    private static testMethod void repairCodePositive1()
    {
        SCboOrderRepairCode boRepair = new SCboOrderRepairCode();
        
        System.assertNotEquals(null, boRepair);
        System.assertNotEquals(null, boRepair.repairCode);
    }
    
    
    private static testMethod void repairCodeSaveNegative1()
    {
        SCboOrderRepairCode boRepair = new SCboOrderRepairCode();
        System.assertNotEquals(null, boRepair);
        System.assertNotEquals(null, boRepair.repairCode);
        
        try
        {        
            boRepair.save();
            System.assert(false);
        }
        catch (Exception e)
        {
            System.assert(true);
        }

    }
}