/*
 * @(#)SCInventoryCreateControllerTest.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class tests the functionality of the main class
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
  @isTest (SeeAllData = true)
private class SCInventoryCreateControllerTest
{
    private static SCInventoryCreateController icc;

    static testMethod void inventoryCreateControllerPositiv1() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2012', 
                                                       true, 
                                                       Date.today());
        
        ApexPages.currentPage().getParameters().put('invId', inventoryId);
        
        icc = new SCInventoryCreateController();
        
        Test.startTest();
        
        icc.createInventory();
        icc.getTimeStamp();
        icc.goBack();
        
        Test.stopTest();
    }
    
    static testMethod void inventoryCreateControllerPositiv2() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        //ApexPages.currentPage().getParameters().put('sid', SCHelperTestClass.stocks[0].id);
        
        /*
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2012', 
                                                       true, 
                                                       Date.today());
        */
        
        SCInventory__c inv = new SCInventory__c();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(inv);
        
        icc = new SCInventoryCreateController(sc);
        
        icc.tempInventory.Stock__c = SCHelperTestClass.stocks[0].id;
        
        Test.startTest();
        
        icc.readStock();
        icc.createInventory();
        icc.goBack();
        
        Test.stopTest();
    }
}