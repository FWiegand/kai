/*
 * @(#)SCboAreaTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test for Helper for evaluating postcode areas and assigned resources
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest 
private class SCboAreaTest
{
   /*
    * Test the data access
    */
    private static testmethod void testBasic()
    {
        // create some postcode areas    
        SCArea__c area1 = new SCArea__c(name = 'My test area 1', context__c = 'Resource');
        SCArea__c area2 = new SCArea__c(name = 'My test area 2', context__c = 'Resource');
        SCArea__c area3 = new SCArea__c(name = 'My test area 3', context__c = 'Resource');
        SCArea__c area4 = new SCArea__c(name = 'My test area 4', context__c = 'Resource');
        SCArea__c area5 = new SCArea__c(name = 'My test area 4', context__c = 'Resource');
        
        insert area1;            
        insert area2;            
        insert area3;            
        insert area4;            
        insert area5;            
        
        List<SCAreaItem__c> areaitems = new List<SCAreaItem__c>();
        // positive test
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12345', prio__c = '1', description__c = 'desc 1.1'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12346', prio__c = '2', description__c = 'desc 1.2'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12347', prio__c = '3', description__c = 'desc 1.3'));
        // negative test
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12345', prio__c = '5', description__c = 'desc 1.4'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12346', prio__c = '6', description__c = 'desc 1.5'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12347', prio__c = '7', description__c = 'desc 1.6'));

        // positive test
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'NL', name = 'T12345', prio__c = '2', description__c = 'desc 2.1'));
        areaitems.add(new SCAreaItem__c(area__c = area3.id, country__c = 'NL', name = 'T12345', prio__c = '3', description__c = 'desc 3.1'));

        areaitems.add(new SCAreaItem__c(area__c = area4.id, country__c = 'NL', name = 'T12345', prio__c = '1', description__c = 'desc 4.1'));

        areaitems.add(new SCAreaItem__c(area__c = area5.id, country__c = 'GB', name = '23', prio__c = '1', description__c = 'desc 5.1'));
       
        insert areaitems;
        
        // create the resources and assignments
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, new List<SCStock__c>(), null, true);
        
        SCHelperTestClass.resourceAssignments[0].area__c = area1.id;
        SCHelperTestClass.resourceAssignments[1].area__c = area2.id;
        SCHelperTestClass.resourceAssignments[2].area__c = area3.id;
        SCHelperTestClass.resourceAssignments[3].area__c = area5.id;
        update SCHelperTestClass.resourceAssignments;    

        Test.startTest();        

        // determine the resources and their postcode areas based on the area assignment
        List<SCAreaItem__c> result = SCboArea.readByPostcode('NL', 'T12345', null);
        
        // check the test result
        System.Assert(result.size() == 3);
  
        // the priorities are ordered ascending        
        System.Assert(result[0].prio__c == '1');
        System.Assert(result[1].prio__c == '2');
        System.Assert(result[2].prio__c == '3');
        
        String resultstring = SCboArea.convert(result, 3);
        System.Assert(resultstring.contains('|'));

        resultstring = SCboArea.convert(result, 1);
        System.Assert(!resultstring.contains('|'));

        // negative test (nothing is to be found)
        result = SCboArea.readByPostcode('NL', 'T12345', Date.TODAY().addDays(- 10));
        System.Assert(result.size() == 0);

        // only one item 
        result = SCboArea.readByPostcode('DE', 'T12345', null);
        System.Assert(result.size() == 1);
        System.Assert(result[0].prio__c == '5');

        System.debug('####result2:' + result);

        // check UK
        result = SCboArea.readByPostcode('GB', '23 456', null);
        System.Assert(result.size() == 1);
        System.Assert(result[0].prio__c == '1');

        Test.stopTest();
    }
    
   /*
    * Test the data access
    */
    private static testmethod void testBusinessUnitAreas()
    {
        // create some postcode areas    
        SCArea__c area1 = new SCArea__c(name = 'Business Unit 1', context__c = 'Business Unit');
        SCArea__c area2 = new SCArea__c(name = 'Business Unit 2', context__c = 'Business Unit');
        SCArea__c area3 = new SCArea__c(name = 'My test area 1', context__c = 'Resource');
        SCArea__c area4 = new SCArea__c(name = 'Business Unit 3', context__c = 'Business Unit');
        
        insert area1;            
        insert area2;            
        insert area3;            
        insert area4;            
        
        List<SCAreaItem__c> areaitems = new List<SCAreaItem__c>();
        // positive test
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12345', description__c = 'desc 1.1'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12346', description__c = 'desc 1.2'));
        // negative test
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'DE', name = 'T12345', description__c = 'desc 1.4'));
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'DE', name = 'T12346', description__c = 'desc 1.5'));

        // test concerning engineer areas
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'NL', name = 'T12345', prio__c = '2', description__c = 'desc 2.1'));
        areaitems.add(new SCAreaItem__c(area__c = area3.id, country__c = 'NL', name = 'T12345', prio__c = '3', description__c = 'desc 3.1'));

        areaitems.add(new SCAreaItem__c(area__c = area4.id, country__c = 'GB', name = '23'));

        insert areaitems;

        SCHelperTestClass.createTestCalendar(true);

        SCBusinessUnit__c bus1 = new SCBusinessUnit__c(name='Test BUS1', area__c = area1.id, calendar__c = SCHelperTestClass.calendar.id, Operational__c=true, type__c='5901');
        SCBusinessUnit__c bus2 = new SCBusinessUnit__c(name='Test BUS2', area__c = area2.id, calendar__c = SCHelperTestClass.calendar.id, Operational__c=true, type__c='5901' );
        SCBusinessUnit__c bus3 = new SCBusinessUnit__c(name='Test BUS3', area__c = area4.id, calendar__c = SCHelperTestClass.calendar.id, Operational__c=true, type__c='5901' );

        insert bus1;
        insert bus2;
        insert bus3;
        
        Test.startTest();        

        List<SCBusinessUnit__c> units = SCboArea.getBusinessUnits('NL', 'T12345');
        System.Assert(units.size() == 2);

        // unknown
        units = SCboArea.getBusinessUnits('xx', 'unknown');
        System.Assert(units.size() == 0);

        
        SCBusinessUnit__c first = SCboArea.getBusinessUnitFirst('NL', 'T12345');
        System.Assert(first.name == bus1.name);
        
        first = SCboArea.getBusinessUnitFirst('XX', 'unknown');
        System.Assert(first == null);
        
        first = SCboArea.getBusinessUnitFirst('GB', '23 456');
        System.Assert(first.name == bus3.name);

        Test.stopTest();
    }    
    
   /*
    * Test the data access
    */
    private static testmethod void testDistanceZoneAreas()
    {
        // create some postcode areas    
        SCArea__c area1 = new SCArea__c(name = 'My test area 1', context__c = 'Resource', DistanceZone__c = '901');
        insert area1;
        SCArea__c area2 = new SCArea__c(name = 'My test area 2', context__c = 'Resource', DistanceZone__c = '902');
        insert area2;
        
        List<SCAreaItem__c> areaitems = new List<SCAreaItem__c>();
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12345'));
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'GB', name = '23'));
        insert areaitems;

        Test.startTest();        

        String distZone = SCboArea.getDistanceZone('DE', 'T12345');
        System.AssertEquals('901', distZone);

        distZone = SCboArea.getDistanceZone('DE', 'T12346');
        System.AssertEquals(null, distZone);

        distZone = SCboArea.getDistanceZone('GB', '23 456');
        System.AssertEquals('902', distZone);

        Test.stopTest();
    }    
}