/*
 * @(#)CCWCMaterialInventoryCreateTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 @isTest
private class SCbtcMaterialPackageReceivedTest 
{
	private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
	
	/*
	* Empty test
	*/
	static testMethod void positiveTestCase1()
	{
		SCbtcMaterialPackageReceived.asyncProcessAll(10,'productive');
	}
	
	/*
	* create PDF
	* ### TODO
	*/
	/*static testMethod void positiveTestCase2()
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
        	appSettings = SCApplicationSettings__c.getInstance();
        }
		CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
        	ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        Boolean doUpsert = true;
        SCOrder__c order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        
        SCAssignment__c assignment = [select ID, Name, CreatedDate, ClearingPostProcessStatus__c, 
        				ClearingStartPostProcessing__c
        				from SCAssignment__c where Order__c =: order.Id];
		
		assignment.CreatedDate.addMinutes(-4);
		assignment.ClearingPostProcessStatus__c = 'pending';
		update assignment;

        Test.StartTest();
        
        SCbtcMaterialPackageReceived.asyncProcessAll(10,'productive');
        
        Test.StopTest();
        
        System.assertEquals('archive', assignment.ClearingPostProcessStatus__c ); 
        
    }*/
	
}