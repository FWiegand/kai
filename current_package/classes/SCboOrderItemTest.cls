/*
 * @(#)SCboOrderItemTest.cls
 *  
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCboOrderItemTest {

    static testMethod void getAssignedOrdersPositive1() {
        SCHelperTestClass.createOrderItem(true);
        
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        List<SCOrderItem__c> orderItems = boOrderItem.getAssignedOrders();
        
        //TODO System.assertEquals(0, orderItems.size());
       
        //List<SCboOrderLine> boOrderLines = boOrderItem.boOrderLines;
        //System.debug(SCboOrderItem.boOrderLines);
    }
    
    static testMethod void getSavePositive1()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createTestArticles(true);
        
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);

        SCOrderRepairCode__c repairCode = new SCOrderRepairCode__c();
        repairCode.ErrorLocation1__c         = '01';
        repairCode.ErrorLocation2__c         = '001';
        repairCode.ErrorType1__c             = '001';
        repairCode.ErrorType1New__c          = 'Text';
        repairCode.ErrorType2__c             = '999';
        repairCode.ErrorActivityCode1__c     = '01';
        repairCode.ErrorActivityCode1New__c  = 'Text';
        repairCode.ErrorActivityCode2__c     = '99';
        repairCode.ErrorArticle__c           = SCHelperTestClass.articles[0].Id;
        repairCode.ErrorArticleTrackingNo__c = '1234';
        repairCode.ErrorDescription__c       = 'Descr';
        repairCode.ErrorCausing__c           = true;
        repairCode.ErrorSymptom1__c          = '78';
        SCboOrderRepairCode boRepiarCode = new SCboOrderRepairCode(repairCode);
        boOrderItem.boRepairCodes.add(boRepiarCode);
        
        boOrderItem.save(); 
        // System.assertEquals(0, orderItems);
    }


    /**
     * Check saving of order item containing an order line
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    static testMethod void getSavePositive2() {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);

        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);

        // Id orderId, Id orderItemId, Id articleId, Id assignmentId
        SCboOrderLine boOrderLine = new SCboOrderLine(SCHelperTestClass.order.Id,
                                                      SCHelperTestClass.orderItem.Id,
                                                      SCHelperTestClass.articles[0].Id,
                                                      SCHelperTestClass.assignmentMap.get('assignmentDispo1').Id);
        
        SCboArticle boArticle = new SCboArticle(SCHelperTestClass.articles[0].Id);
        boOrderLine.orderLine.Article__r = boArticle.article;
        
        // set the require field manually since the 
        // pricing requires a complete order
        boOrderLine.orderLine.ListPrice__c = 1234.56;
        boOrderLine.orderLine.Order__c = SCHelperTestClass.order.Id;
        boOrderItem.boOrderLines.add(boOrderLine);

        Test.startTest();        
        boOrderItem.save(); 
        Test.stopTest();

    }


    static testMethod void getSaveNegativ1() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createInstalledBase(true);
        SCOrderItem__c orderItem = new SCOrderItem__c(Order__c = SCHelperTestClass.order.Id);
        
        SCboOrderItem boOrderItem = new SCboOrderItem(orderItem);
        
        try
        {
            //TODO Check why the save did happen without an exception
            //System.assert(false, 'Never shold be here');
        }
        catch (SCfwOrderException e)
        {
            System.assert(true);
        }
    }    

    static testMethod void getHistoryDataPositive1() {
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        
        String history = boOrderItem.getHistoryData();
        
        // System.assertEquals(0, orderItems);
    }


    /***
    * create and select qualifications assigned to order item
    * positive test   
    *
    */
    static testMethod void initQualificationPositive1() 
    {
        SCHelperTestClass.createOrderTestSet4(true);
        
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem.Id);
        boOrderItem.initQualifications('nl');        
        integer count1 = boOrderItem.boQualifications.size();    
        System.debug('positive qualification tests count1 ' + count1);

        boOrderItem = new SCboOrderItem();
        SCHelperTestClass2.createQualificationLanguage('nl');
        boOrderItem.initQualifications('nl');        
        integer count2 = boOrderItem.boQualifications.size();    
        System.debug('positive qualification tests count2 ' + count2);

        System.assert(count2 - count1 < 2);
    }

    /***
    * create and select qualifications assigned to order item
    * negative test   
    *
    */
    static testMethod void initQualificationNegative1() 
    {
        SCboOrderItem boOrderItem = new SCboOrderItem();
        boOrderItem.initQualifications('nl');        
        integer count1 = boOrderItem.boQualifications.size();    
        System.debug('negative qualification tests count1 ' + count1);
        boOrderItem = new SCboOrderItem();
        SCHelperTestClass2.createQualificationLanguage('nl');
        boOrderItem.initQualifications('xx');        
        integer count2 = boOrderItem.boQualifications.size();    
        System.debug('negative qualification tests count2 ' + count2);

        System.assert(count2 - count1 < 1);
    }


    /**
     * Check the hasLines() method
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    static testMethod void checkHasLinePositive1() {
        SCboOrderItem boOrderItem = new SCboOrderItem();
        
        // hs: was assertEquals ???
        System.assert(!boOrderItem.hasLines());
    }
    
    /**
     * Check getting the order lines
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    static testMethod void checkOrderLinesPositive1() {
        SCboOrderItem boOrderItem = new SCboOrderItem();
        
        // hs: was assertEquals ???
        System.assertEquals(0, boOrderItem.boOrderLines.size());
    }

    /**
     * Check getting the order lines
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    static testMethod void checkRepairCodesPositive1() {
        SCboOrderItem boOrderItem = new SCboOrderItem();
        
        // hs: was assertEquals ???
        System.assertEquals(0, boOrderItem.boRepairCodes.size());
    }

    /**
     * Check creating an order item via the id
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void checkCreateOrderItemPositive()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem.Id);
        boOrderItem.copyDataFromInstBase();
        
        System.assert(boOrderItem.orderItem.InstalledBase__c != null);
    }
    
    //@author GMSS
    static testMethod void CodeCoverageA()
    {
        SCOrder__c order = new SCOrder__c();
        Database.insert(order);
        
        SCInstalledBase__c instBase = new SCInstalledBase__c();
        Database.insert(instBase);
        
        SCOrderItem__c orderItem = new SCOrderItem__c(Order__c = order.Id, InstalledBase__c = instBase.Id);
        Database.insert(orderItem);
        
        
        Test.startTest();
        
        
        SCboOrderItem boOrderItem = new SCboOrderItem(orderItem);
        boOrderItem.copyDataFromInstBase();
        
        
        Test.stopTest();
    }
}