/*
 * @(#)SCboStockTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
 
@isTest
private class SCboStockTest 
{

    static testMethod void testMarkAsReloadMobileByStock() 
    {
        // first create all necessary test data
        
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles, SCHelperTestClass.stocks.get(0).id, SCHelperTestClass.stocks.get(1).id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, SCHelperTestClass.stocks,null, true);
        
        system.debug('#### stocks before update: ' + SCHelperTestClass.stocks);
        system.debug('#### stockitems before update: ' + SCHelperTestClass.stockItems);
        
        Test.startTest();
        
        SCStock__c[] stocks = SCHelperTestClass.stocks;
        SCStockItem__c[] stockItems = SCHelperTestClass.stockItems;

		//Reset activity flag setted by trigger
        stocks.get(0).Activity__c = null;
        stockItems.get(0).Activity__c = null;
        update stocks;
        update stockItems;
        
        
        SCboStock.reloadMobileStockById(stocks.get(0).id);
        
        SCStock__c updatedStock =
        [
        	SELECT 
        		Activity__c 
        	FROM
        		SCStock__c
        	WHERE 
        		Id = :stocks.get(0).id
        	LIMIT 1
        ];
        
        SCStockItem__c updatedStockItem =
        [
        	SELECT 
        		Activity__c 
        	FROM
        		SCStockItem__c
        	WHERE 
        		Stock__c = :stocks.get(0).id
        	LIMIT 1
        ];
        
        system.debug('#### stocks : ' + updatedStock);
        system.debug('#### stockitems : ' + updatedStockItem);
        
        
        
        //Check Stocks
        system.assertEquals('U', updatedStock.Activity__c);
                
        //Check StockItems
        system.assertEquals('Q', updatedStockItem.Activity__c );
        
        Test.stopTest();
    }
    
    static testMethod void testMarkAsReloadMobileByResource() 
    {
        // first create all necessary test data
        
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItems(SCHelperTestClass.articles, SCHelperTestClass.stocks.get(0).id, SCHelperTestClass.stocks.get(1).id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, SCHelperTestClass.stocks,null, true);
        
        system.debug('#### stocks before update: ' + SCHelperTestClass.stocks);
        system.debug('#### stockitems before update: ' + SCHelperTestClass.stockItems);
        
        
        Test.startTest();
        
        SCStock__c[] stocks = SCHelperTestClass.stocks;
        SCStockItem__c[] stockItems = SCHelperTestClass.stockItems;

		//Reset activity flag setted by trigger
        stocks.get(0).Activity__c = null;
        stockItems.get(0).Activity__c = null;
        update stocks;
        update stockItems;
        
        
        SCboStock.reloadMobileStockByResource(SCHelperTestClass.resources.get(0).id);
        
        SCStock__c updatedStock =
        [
        	SELECT 
        		Activity__c 
        	FROM
        		SCStock__c
        	WHERE 
        		Id = :stocks.get(0).id
        	LIMIT 1
        ];
        
        SCStockItem__c updatedStockItem =
        [
        	SELECT 
        		Activity__c 
        	FROM
        		SCStockItem__c
        	WHERE 
        		Stock__c = :stocks.get(0).id
        	LIMIT 1
        ];
        
        system.debug('#### stocks : ' + updatedStock);
        system.debug('#### stockitems : ' + updatedStockItem);
        
        
        
        //Check Stocks
        system.assertEquals('U', updatedStock.Activity__c);
                
        //Check StockItems
        system.assertEquals('Q', updatedStockItem.Activity__c );
        
        Test.stopTest();
    }
}