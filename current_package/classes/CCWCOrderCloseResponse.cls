/*
 * @(#)CCWCOrderCloseResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCOrderCloseResponse
{
    public static void processCloseOrderResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
	{
		SCInterfaceLog__c responseInterfaceLog = null;
    	processCloseOrderResponse(messageID, requestMessageID, headExternalID, referenceItem, 
                                                  MaximumLogItemSeverityCode, logItemArr, 
                                                  interfaceLogList,
                                                  GenericServiceResponseMessage,
                                                  responseInterfaceLog);
		
	}                                                  



    public static void processCloseOrderResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        String interfaceName = 'SAP_ORDER_CLOSE';
        String interfaceHandler = 'CCWCOrderCloseResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            

		// Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
		responseInterfaceLog.Direction__c = direction;            
		responseInterfaceLog.MessageID__c = messageID;            
		responseInterfaceLog.ReferenceID__c = referenceID;            
		responseInterfaceLog.ResultCode__c = resultCode;            
		responseInterfaceLog.ResultInfo__c = resultInfo;
		responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);

        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            // find the order
            step = 'find an order'; 
            SCOrder__c order = readOrder(referenceItem.ExternalID, responseInterfaceLog);
            debug('order: ' + order);
			responseInterfaceLog.Order__c = order.ID;
			responseInterfaceLog.ReferenceID__c = order.ID;            
            
            // find the interfacelog
            step = 'find a request interface';
            List<String> interfaceNameList = new List<String>();
            interfaceNameList.add('SAP_ORDER_CLOSE');
            interfaceNameList.add('SAP_ORDER_CANCEL');
            
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, order.ID, 
            										referenceItem.ExternalID, interfaceNameList);
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            // update order
            step = 'update the order';
            order.ERPStatusOrderClose__c = CCWSGenericResponse.getResultStatus(logItemArr);
            order.ERPResultDate__c = DateTime.now();
            order.IDocOrderClose__c = responseInterfaceLog.Idoc__c;
            
            // GMSGB: 	if the order is in status__c == '5506' (DOMVAL_ORDERSTATUS_COMPLETED)
            // 			AND the order close status is ok
            // 			the status__c will be set to '5508' DOMVAL_ORDERSTATUS_CLOSEDFINAL 
            if(order.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED 
            	&& order.ERPStatusOrderClose__c == 'ok')
            {
            	order.status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL; 
            	// PMS 36903/INC0129250: Zu fakturierender Auftrag bekommt nach Abschluß  den Status "gebucht" statt "zu fakturieren"
            	// whish by CCE, do not change the Invoicing status! 
            	// order.InvoicingStatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED;
            	// set owner to SAPAPI (prevent closed order to be changed by old user)
            	User sapiUser = selectSAPAPIUser();
            	order.OwnerId = sapiUser.id;
            }
            
            
            update order;
            debug('order after update: ' + order);
            step = 'write a response interface log';

            // write response interface log
			responseInterfaceLog.Count__c = 1;           

            debug('responseInterfaceLog: ' + responseInterfaceLog);
            // update request interface log
            step = 'update the request interface log';
            if(responseInterfaceLog != null)
            {
                //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.
                if(requestInterfaceLog != null)
                {
	                requestInterfaceLog.Response__c = responseInterfaceLog.Id;
	                interfaceLogList.add(requestInterfaceLog);
                }
            }
            else
            {
                throw new SCfwException('A response interface log object could not be crated for messageID: ' + messageID + 
                                        ', order id: ' + order.ID + ', order name: ' + headExternalID); 
            }    
            
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
        finally
        {
//			if(!Test.isRunningTest())
//			{
	        	if(responseInterfaceLog.Order__c != null)
	        	{
					CCWCOrderCloseEx.processNext(responseInterfaceLog.Order__c);
	        	}
//			}		        	
        }  
    }//processCreateOrderResponse

    public static SCOrder__c readOrder(String headExternalID, SCInterfaceLog__c responseInterfaceLog)
    {
        SCOrder__c retValue = null;
        List<SCOrder__c> ol = [select ID, name, OwnerId, InvoicingStatus__c, status__c, ERPStatusOrderClose__c 
        	from SCOrder__c where name = : headExternalID];
        
        // Test:
        //SCfwInterfaceRequestPendingException er = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderClose__c  \'none\'.');
        //er.order = ol[0];
        //throw er;
        
        //SCfwException er = new SCfwException('Test SCfwException');
        //throw er;
        
        if(ol.size() > 0)
        {
        	if(ol[0].ERPStatusOrderClose__c == 'error' || ol[0].ERPStatusOrderClose__c == 'pending' )
        	{
	            retValue = ol[0];
	            debug('read order: ' + ol[0]);
        	}
        	else if (ol[0].ERPStatusOrderClose__c == 'none' )
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderClose__c  \'none\'.');
        		e.order = ol[0];
        		throw e;
        	}
        	else if (ol[0].ERPStatusOrderClose__c == 'ok' )
        	{
        		//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderClose__c  \'ok\'.');
        		//e.order = ol[0];       	
        		//throw e;
        		retValue = ol[0];
	            debug('The order with name: ' + headExternalId + ' has ERPStatusOrderClose__c  \'ok\'.');
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusOrderClose__c  '
        		+ ol[0].ERPStatusOrderClose__c +'.');
        		e.order = ol[0];       	
        		throw e;
        	}
        }
        else
        {
        	throw new SCfwException('The order with name: ' + headExternalId + ' has been not found in Salesforce.');
        }
        
        return retValue;
    }

	/*
	* Select the User named 'sapapi'.
	*/
	private static User selectSAPAPIUser()
	{
		List<User> users = [SELECT Id,Name FROM User where Name =: 'sapapi'];
		if (users.size() > 0)
		{
			return users[0];
		}
		else
		{
			throw new SCfwException('The SAPAPI user does not exist, but is neccesary to change the owner for finalized orders');
		}
	}

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }
}