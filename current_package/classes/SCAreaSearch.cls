/*
 * @(#)SCAreaSearch.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Simple search function for engineers / business units by the postcode
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCAreaSearch
{
    // The search fields that the user can fill
    public SCAreaItem__c searchvalue {get; set;}

    // Result list with the business units that match the postcode areas
    public List<SCBusinessUnit__c> units {get; set;}
    // Result list with the engineers that match the postcode areas
    public List<SCAreaItem__c> areaitems {get; set;}

   /*
    * Constructor - basis initialization
    */
    public SCAreaSearch()
    {
        searchvalue = new SCAreaItem__c();
    }

   /*
    * Starts the search. First searches for the business unit then for the engineers.
    */
    public PageReference onSearch() 
    {
        String country =  SCBase.getUserCountry();
    
        units = SCboArea.getBusinessUnits(country, searchvalue.name);

        areaitems = SCboArea.readByPostcode(country, searchvalue.name, null);

        return null;
    }
}