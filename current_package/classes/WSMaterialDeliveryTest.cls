/*
 * @(#)WSMaterialDeliveryTest.cls
 *
 * Copyright (c) 2012 GMS Development GmbH - all rights reserved - www.gms-online.de
 */
 
/**
 * This class contains the test code for WSMaterialDelivery webservices
 * Please note that only the web service implementation is tested here.
 * No web service calls are executed. 
 * @author GMSNA <narmbruster@gms-online.de>
*/ 
@isTest
private class WSMaterialDeliveryTest 
{

    /*
     * Test 1:
     */
    static testMethod void DeliveryWithOrderReference_DisableDeliveryConf_StockItemQty_0() 
    {
        // SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        // Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
        Boolean enableDeliveryConfirmation = true;    
        
        // create a test plant and stock
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        
        // Create a test order and required order roles 
        SCHelperTestClass.createDomsForOrderCreation();      
        SCHelperTestClass.createOrderTestSet(true);

        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id,
                                           SCHelperTestClass.boorderLines[0].orderLine.Article__c,
                                           true);

        // Create a material movement of type "5202 Material Request (deliver to engineer stock)"
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks[0].id;  
        insert matmove1;
        debug('matmove1: ' + matmove1);
        
        // create material request
        
        // prepare delivery note input data
        List<WSMaterialDelivery.DeliveryData> data = new List<WSMaterialDelivery.DeliveryData>(); 
        
        
        // simple selection by material movement id
        WSMaterialDelivery.DeliveryData item1 = new WSMaterialDelivery.DeliveryData();
        item1.type           = '5207'; // delivery    
        item1.matmoveId      = matmove1.id;
        item1.articleId      = matmove1.Article__c;
        item1.qty            = matmove1.Qty__c;
        item1.deliverynoteno = 'DELIVNOTE100';
        item1.deliverydate   = Date.today() + 2;
        data.add(item1);
        
        Test.startTest();
        // execute the test
        List<WSMaterialDelivery.DeliveryData> result = WSMaterialDelivery.processinternal('testDelivered', data, enableDeliveryConfirmation);   
        Test.stopTest();

        System.assertEquals(item1.type,           result[0].type,          'article not equal');
        System.assertEquals(item1.matmoveId,      result[0].matmoveId,     'material movement id not equal');
        System.assertEquals(item1.articleId,      result[0].articleId,     'article id not equal');
        System.assertEquals(item1.deliverynoteno, result[0].deliverynoteno,'delivery note not equal');
        System.assertEquals(item1.deliverydate,   result[0].deliverydate,  'delivery date not equal');
        System.assertEquals('SUCC',               result[0].resultcode,    'result code issue');
        
        // now check the material movement updates
        // read and check order
        SCOrder__c changedOrder = [select Id, MaterialStatus__c from SCOrder__c where Id = : SCHelperTestClass.order.Id ];
        System.assertEquals(changedOrder.MaterialStatus__c, '5405', 'the order material status not set to delivered');
        // read and check order line
        debug('SCHelperTestClass.orderLine.Id: ' + SCHelperTestClass.boOrderLines[0].orderLine.id);
        SCOrderLine__c changedOrderLine = [select Id, MaterialStatus__c from SCOrderLine__c where Id = : SCHelperTestClass.boOrderLines[0].orderLine.id];
        System.assertEquals(changedOrderLine.MaterialStatus__c, '5405', 'the order line material status not set to delivered');

        // read and check created material movement
        SCMaterialMovement__c cmm = [Select Status__c, Type__c, Article__c, Stock__c 
                                     from SCMaterialMovement__c 
                                     where Stock__c = : SCHelperTestClass.stocks[0].id
                                     and Article__c = : SCHelperTestClass.boorderLines[0].orderLine.Article__c
                                     and Id <> :matmove1.Id];
        System.assertEquals(cmm.Type__c, '5207', 'the type set to delivering');
        System.assertEquals(cmm.Status__c, '5405', 'the status not set to delivered');
                                                         
        // read and check stock item
        debug('SCHelperTestClass.boOrderLines[0].orderLine.qty__c: ' + SCHelperTestClass.boOrderLines[0].orderLine.qty__c);
        SCStockItem__c si = [Select Stock__c, Article__c, Id, Qty__c from SCStockItem__c where id = : SCHelperTestClass.stockItem.Id];
        debug('stock item: ' + si);
        System.assertEquals(SCHelperTestClass.boOrderLines[0].orderLine.qty__c, si.Qty__c, 'Quantity error');

        
        SCMaterialMovement__c mm1 = [select id, status__c from SCMaterialMovement__c where id = :item1.matmoveId];
        // the material status must be updated from '5403' ordered to '5423' In transit  
        // System.assertEquals('5423' , mm1.status__c,  'material status update failed');
    }

    /*
     * Test 2:
     */
    static testMethod void DeliveryWithOrderReference_DisableDeliveryConf_new_StockItemQty() 
    {
        // SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        // Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
        Boolean enableDeliveryConfirmation = true;    
        
        // create a test plant and stock
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        
        // Create a test order and required order roles  
        SCHelperTestClass.createDomsForOrderCreation();           
        SCHelperTestClass.createOrderTestSet(true);


        // Create a material movement of type "5202 Material Request (deliver to engineer stock)"
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks[0].id;  
        insert matmove1;
        debug('matmove1: ' + matmove1);
        
        // create material request
        
        // prepare delivery note input data
        List<WSMaterialDelivery.DeliveryData> data = new List<WSMaterialDelivery.DeliveryData>(); 
        
        
        // simple selection by material movement id
        WSMaterialDelivery.DeliveryData item1 = new WSMaterialDelivery.DeliveryData();
        item1.type           = '5207'; // delivery    
        item1.matmoveId      = matmove1.id;
        item1.articleId      = matmove1.Article__c;
        item1.qty            = matmove1.Qty__c;
        item1.deliverynoteno = 'DELIVNOTE100';
        item1.deliverydate   = Date.today() + 2;
        data.add(item1);
        
        Test.startTest();
        // execute the test
        List<WSMaterialDelivery.DeliveryData> result = WSMaterialDelivery.processinternal('testDelivered', data, enableDeliveryConfirmation);   
        Test.stopTest();

        System.assertEquals(item1.type,           result[0].type,          'article not equal');
        System.assertEquals(item1.matmoveId,      result[0].matmoveId,     'material movement id not equal');
        System.assertEquals(item1.articleId,      result[0].articleId,     'article id not equal');
        System.assertEquals(item1.deliverynoteno, result[0].deliverynoteno,'delivery note not equal');
        System.assertEquals(item1.deliverydate,   result[0].deliverydate,  'delivery date not equal');
        System.assertEquals('SUCC',               result[0].resultcode,    'result code issue');
        
        // now check the material movement updates
        // read and check order
        SCOrder__c changedOrder = [select Id, MaterialStatus__c from SCOrder__c where Id = : SCHelperTestClass.order.Id ];
        System.assertEquals(changedOrder.MaterialStatus__c, '5405', 'the order material status not set to delivered');
        // read and check order line
        debug('SCHelperTestClass.orderLine.Id: ' + SCHelperTestClass.boOrderLines[0].orderLine.id);
        SCOrderLine__c changedOrderLine = [select Id, MaterialStatus__c from SCOrderLine__c where Id = : SCHelperTestClass.boOrderLines[0].orderLine.id];
        System.assertEquals(changedOrderLine.MaterialStatus__c, '5405', 'the order line material status not set to delivered');

        // read and check created material movement
        SCMaterialMovement__c cmm = [Select Status__c, Type__c, Article__c, Stock__c 
                                     from SCMaterialMovement__c 
                                     where Stock__c = : SCHelperTestClass.stocks[0].id
                                     and Article__c = : SCHelperTestClass.boorderLines[0].orderLine.Article__c
                                     and Id <> :matmove1.Id];
        System.assertEquals(cmm.Type__c, '5207', 'the type set to delivering');
        System.assertEquals(cmm.Status__c, '5405', 'the status not set to delivered');
                                                         
        // read and check stock item
        debug('SCHelperTestClass.boOrderLines[0].orderLine.qty__c: ' + SCHelperTestClass.boOrderLines[0].orderLine.qty__c);
        String stockArticleId = WSMaterialDelivery.getJoinedKey(SCHelperTestClass.stocks[0].id, SCHelperTestClass.boorderLines[0].orderLine.Article__c, null);  
        SCStockItem__c si = [Select Stock__c, Article__c, Id, Qty__c from SCStockItem__c 
                            where StockArticleId__c = : stockArticleId ];
        debug('stock item: ' + si);
        System.assertEquals(SCHelperTestClass.boOrderLines[0].orderLine.qty__c, si.Qty__c, 'Quantity error');

        
        SCMaterialMovement__c mm1 = [select id, status__c from SCMaterialMovement__c where id = :item1.matmoveId];
        // the material status must be updated from '5403' ordered to '5423' In transit  
        // System.assertEquals('5423' , mm1.status__c,  'material status update failed');
    }


    /*
     * Test 3:
     */
    static testMethod void DeliveryWithOrderReference_EnableDeliveryConf() 
    {
        // SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        // Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
        Boolean enableDeliveryConfirmation = true;    

        // create a test plant and stock
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        
        // Create a test order and required order roles   
        SCHelperTestClass.createDomsForOrderCreation();          
        SCHelperTestClass.createOrderTestSet(true);


        // Create a material movement of type "5202 Material Request (deliver to engineer stock)"
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks[0].id;  
        insert matmove1;
        debug('matmove1: ' + matmove1);
        
        // create material request
        
        // prepare delivery note input data
        List<WSMaterialDelivery.DeliveryData> data = new List<WSMaterialDelivery.DeliveryData>(); 
        
        
        // simple selection by material movement id
        WSMaterialDelivery.DeliveryData item1 = new WSMaterialDelivery.DeliveryData();
        item1.type           = '5207'; // delivery    
        item1.matmoveId      = matmove1.id;
        item1.articleId      = matmove1.Article__c;
        item1.qty            = matmove1.Qty__c;
        item1.deliverynoteno = 'DELIVNOTE100';
        item1.deliverydate   = Date.today() + 2;
        data.add(item1);
        
        Test.startTest();
        // execute the test
        List<WSMaterialDelivery.DeliveryData> result = WSMaterialDelivery.processinternal('testWithConfirmation', data, enableDeliveryConfirmation);   
        Test.stopTest();

        System.assertEquals(item1.type,           result[0].type,          'article not equal');
        System.assertEquals(item1.matmoveId,      result[0].matmoveId,     'material movement id not equal');
        System.assertEquals(item1.articleId,      result[0].articleId,     'article id not equal');
        System.assertEquals(item1.deliverynoteno, result[0].deliverynoteno,'delivery note not equal');
        System.assertEquals(item1.deliverydate,   result[0].deliverydate,  'delivery date not equal');
        System.assertEquals('SUCC',               result[0].resultcode,    'result code issue');
        
        // now check the material movement updates
        // read order
        SCOrder__c changedOrder = [select Id, MaterialStatus__c from SCOrder__c where Id = : SCHelperTestClass.order.Id ];
        System.assertEquals(changedOrder.MaterialStatus__c, '5423', 'the order material status not set to in transit');
        // read order line
        debug('SCHelperTestClass.orderLine.Id: ' + SCHelperTestClass.boOrderLines[0].orderLine.id);
        SCOrderLine__c changedOrderLine = [select Id, MaterialStatus__c from SCOrderLine__c where Id = : SCHelperTestClass.boOrderLines[0].orderLine.id];
        System.assertEquals(changedOrderLine.MaterialStatus__c, '5423', 'the order line material status not set to in transit');

        // read created material movement
        SCMaterialMovement__c cmm = [Select Status__c, Type__c, Article__c, Stock__c 
                                     from SCMaterialMovement__c 
                                     where Stock__c = : SCHelperTestClass.stocks[0].id
                                     and Article__c = : SCHelperTestClass.boorderLines[0].orderLine.Article__c
                                     and Id <> :matmove1.Id];
        System.assertEquals(cmm.Type__c, '5207', 'the type set to delivering');
        System.assertEquals(cmm.Status__c, '5423', 'the status not set to in transit');
                                                         


        
        SCMaterialMovement__c mm1 = [select id, status__c from SCMaterialMovement__c where id = :item1.matmoveId];
        // the material status must be updated from '5403' ordered to '5423' In transit  
//        System.assertEquals('5423' , mm1.status__c,  'material status update failed');

        
    }
    
    /*
     * Test 4:
     */
    static testMethod void DeliveryWithoutMaterialMovementId_EnableDeliveryConf() 
    {
        // SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        // Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
        Boolean enableDeliveryConfirmation = true;    

        // create a test plant and stock
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        
        // Create a test order and required order roles 
        SCHelperTestClass.createDomsForOrderCreation();            
        SCHelperTestClass.createOrderTestSet(true);


        // Create a material movement of type "5202 Material Request (deliver to engineer stock)"
        SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks[0].id;  
        //insert matmove1;
        debug('matmove1: ' + matmove1);
        
        // create material request
        
        // prepare delivery note input data
        List<WSMaterialDelivery.DeliveryData> data = new List<WSMaterialDelivery.DeliveryData>(); 
        
        
        // simple selection by material movement id
        WSMaterialDelivery.DeliveryData item1 = new WSMaterialDelivery.DeliveryData();
        item1.type           = '5207'; // delivery    
        item1.articleId      = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        item1.qty            = 5;
        item1.deliverynoteno = 'DELIVNOTE100';
        item1.deliverydate   = Date.today() + 2;
        item1.stockId        = SCHelperTestClass.stocks[0].id;  
        data.add(item1);
        
        Test.startTest();
        // execute the test
        List<WSMaterialDelivery.DeliveryData> result = WSMaterialDelivery.processinternal('testWithConfirmation', data, enableDeliveryConfirmation);   
        Test.stopTest();

        System.assertEquals(item1.type,           result[0].type,          'article not equal');
        System.assertEquals(item1.articleId,      result[0].articleId,     'article id not equal');
        System.assertEquals(item1.deliverynoteno, result[0].deliverynoteno,'delivery note not equal');
        System.assertEquals(item1.deliverydate,   result[0].deliverydate,  'delivery date not equal');
        System.assertEquals('SUCC',               result[0].resultcode,    'result code issue');
        
        // read created material movement
        SCMaterialMovement__c cmm = [Select Status__c, Type__c, Article__c, Stock__c 
                                     from SCMaterialMovement__c 
                                     where Stock__c = : SCHelperTestClass.stocks[0].id
                                     and Article__c = : SCHelperTestClass.boorderLines[0].orderLine.Article__c
                                    ];
        System.assertEquals(cmm.Type__c, '5207', 'the type set to delivering');
        System.assertEquals(cmm.Status__c, '5423', 'the status not set to in transit');
    }
    
    /*
     * Test 5:
     */
    static testMethod void DeliveryWithoutMaterialMovementId_DisableDeliveryConf() 
    {
        // SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        // Boolean enableDeliveryConfirmation = appSettings.MATERIAL_ENABLE_DELIVCONF__c;
        Boolean enableDeliveryConfirmation = false;    

        // create a test plant and stock
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        
        // Create a test order and required order roles   
        SCHelperTestClass.createDomsForOrderCreation();          
        SCHelperTestClass.createOrderTestSet(true);


        // Create a material movement of type "5202 Material Request (deliver to engineer stock)"
        /*SCMaterialMovement__c matmove1 = new SCMaterialMovement__c();
        matmove1.Order__c       = SCHelperTestClass.order.id;
        matmove1.OrderLine__c   = SCHelperTestClass.boOrderLines[0].orderLine.id;
        matmove1.Article__c     = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        matmove1.Qty__c         = SCHelperTestClass.boOrderLines[0].orderLine.qty__c;
        matmove1.RequisitionDate__c = Date.today() + 1;
        matmove1.Status__c      = '5403'; // ordered
        matmove1.Type__c        = '5202'; // material request (deliver to engineer van stock)
        matmove1.Stock__c       = SCHelperTestClass.stocks[0].id;  
        insert matmove1;
        debug('matmove1: ' + matmove1);
        */
        //create stockitem
        SCStockItem__c stockItem = new SCStockItem__c();
        stockItem.Stock__c = SCHelperTestClass.stocks[0].id;
        stockItem.Article__c = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        stockItem.Qty__c = 5;
        //insert stockItem;
        
        
        // create material request
        // prepare delivery note input data
        List<WSMaterialDelivery.DeliveryData> data = new List<WSMaterialDelivery.DeliveryData>(); 
        
        
        // simple selection by material movement id
        WSMaterialDelivery.DeliveryData item1 = new WSMaterialDelivery.DeliveryData();
        item1.type           = '5207'; // delivery    
        item1.articleId      = SCHelperTestClass.boorderLines[0].orderLine.Article__c;
        item1.qty            = 5;
        item1.deliverynoteno = 'DELIVNOTE100';
        item1.deliverydate   = Date.today() + 2;
        item1.stockId        = SCHelperTestClass.stocks[0].id;  
        data.add(item1);
        
        Test.startTest();
        // execute the test
        List<WSMaterialDelivery.DeliveryData> result = WSMaterialDelivery.processinternal('testWithConfirmation', data, enableDeliveryConfirmation);   
        Test.stopTest();

        System.assertEquals(item1.type,           result[0].type,          'article not equal');
        System.assertEquals(item1.articleId,      result[0].articleId,     'article id not equal');
        System.assertEquals(item1.deliverynoteno, result[0].deliverynoteno,'delivery note not equal');
        System.assertEquals(item1.deliverydate,   result[0].deliverydate,  'delivery date not equal');
        System.assertEquals('SUCC',               result[0].resultcode,    'result code issue');
        
        // read created material movement
        SCMaterialMovement__c cmm = [Select Status__c, Type__c, Article__c, Stock__c 
                                     from SCMaterialMovement__c 
                                     where Stock__c = : SCHelperTestClass.stocks[0].id
                                     and Article__c = : SCHelperTestClass.boorderLines[0].orderLine.Article__c
                                   
                                    ];
        System.assertEquals(cmm.Type__c, '5207', 'the type set to delivering');
        System.assertEquals(cmm.Status__c, '5405', 'the status not set to in transit');
    }
    
    private static void debug(String text)
    {
       System.debug('###Test...................' + text);
    }


}


/*        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        boOrder.createDefaultRoles(SCHelperTestClass.account);
        boOrder.save();  
*/        //System.assertEquals(true, result.isValid,  'is valid failed');