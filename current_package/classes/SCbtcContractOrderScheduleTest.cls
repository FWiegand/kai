/*
 * @(#)SCbtcContractVisitCreateTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractOrderScheduleTest
{
    static testMethod void testOrderCreatePositive()
    {
        // Hiere is the problem caused by asynchronity of batch jobs.
        // and that, the tested class must be tested only by one class not more.
        // If I tested the SCbtcContractVisitCreate with SCbtcContractVisitCreateTest
        // I have got 72% percent, but after the running the SCbtcContractOrderScheduleTest
        // the perecentage has been decremented to 52%.
        // So I must insert in this function another test lines for the SCbtcContractVisitCreate.
             
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();

        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);

        SCContract__c c = [Select Name, Id from SCContract__c where Id = :contractID];
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);

        // Order Creation
        System.debug('###Test contractId: ' + contractID);
        System.debug('###Test c.Id: ' + c.Id);
        List<SCContractVisit__c> visitList = [Select Id, Status__c from SCContractVisit__c where Contract__c = :contractID];
        System.debug('###Test visitList: ' + visitList);
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            break;
        } 

        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
       
        
        // Order Schedule
        List<SCOrder__c> orderList = [Select Id from SCOrder__c where Contract__c = :c.Id];
        System.debug('###Test orderList: ' + orderList);
        for(SCOrder__c order: orderList)
        {   
            System.debug('###Test order.Id: ' + order.Id);      
            SCbtcContractOrderSchedule.canSchedule(visitIdList);
            SCbtcContractOrderSchedule.syncSchedule(visitIdList);
            SCbtcContractOrderSchedule.asyncSchedule(null);
        }    
        List<String> visitIdListAll = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdListAll.add(visit.Id);
            break;
        } 


        Test.StartTest();
/*
//////////////////////////////////////////////////////////////////////////////////////////
Note: Comment proposed by Norbert A. at 11.02.2013
On the QACCAG the starting of a batch job causes that the function Test.stopTest() throws 
System.UnexpectedException: Start did not return a valid iterable object.
//////////////////////////////////////////////////////////////////////////////////////////
        SCbtcContractOrderSchedule.asyncSchedule(visitIdListAll);
        
        //SCbtcContractOrderSchedule.asyncSchedule(visitIdListAll);
        
        SCbtcContractOrderCreate.canCreateOrders(visitIdList);

        SCbtcContractOrderCreate.createContractOrderAll('NL', 1, 'test');
 
        // to get the percent higher for the create visits
        //SCbtcContractVisitCreate.createContractVisit(c.Id, 'test');

        System.debug('###Test createContractVisit');
        System.debug('###Test c.Name: ' + c.Name);
        //SCbtcContractVisitCreate.createContractVisit(c.Name, 'test');

        System.debug('###Test createContractVisitAll');
        SCbtcContractVisitCreate.createContractVisitAll('NL', 1, 'test');
*/
        System.debug('###Test canCreateVisits');
        SCbtcContractVisitCreate.canCreateVisits(contractID);
         
        Test.StopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCContract__c contract = new SCContract__c();
        Database.insert(contract);
        
        SCContractVisit__c cv = new SCContractVisit__c(Contract__c = contract.Id);
        Database.insert(cv);
        
        
        Test.startTest();
        
        
        SCbtcContractOrderSchedule obj = new SCbtcContractOrderSchedule(new List<String>(), 'test');
        
        try { obj.getBatchProcessId(); } catch(Exception e) {}
        try { obj.getCause(' errcode(s): 12,34567,890,123456 faultcode='); } catch(Exception e) {}
        try { obj.methodForCodeCoverage(obj, cv); } catch(Exception e) {}
        try { obj.finish(null); } catch(Exception e) {}
        
        Test.stopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageB()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        
        SCOrder__c order = new SCOrder__c();
        Database.insert(order);
        order = [SELECT Name, Id FROM SCOrder__c WHERE Id = :order.Id];
        
        Test.startTest();
        
        
        try { SCbtcContractOrderSchedule.scheduleAll('de',3,'test'); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.scheduleDepartmentAll('de',appSettings.DEFAULT_BUSINESSUNITNO__c, 3,'test'); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.canSchedule(new List<String>{'test'}); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.canSchedule(new List<String>()); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.syncSchedule(new List<String>{'test'}); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.syncSchedule(new List<String>()); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.asyncSchedule(new List<String>{'test'}); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.asyncSchedule(new List<String>()); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.makeOrderAppointments(order.Id,'de',3,'test'); } catch(Exception e) {}
        try { SCbtcContractOrderSchedule.makeOrderAppointments(order.Name,'de',3,'test'); } catch(Exception e) {}
        
        
        Test.stopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageC()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        List<SCApplicationSettings__c> listAppSetting = new List<SCApplicationSettings__c>();
        for (SCApplicationSettings__c appSetting : [SELECT Id, CONTRACT_SCHEDULING_DAYS__c FROM SCApplicationSettings__c])
        {
            appSetting.CONTRACT_SCHEDULING_DAYS__c = null;
            listAppSetting.add(appSetting);
        }
        Database.update(listAppSetting);
        
        
        Test.startTest();
        
        
        SCbtcContractOrderSchedule obj = new SCbtcContractOrderSchedule(new List<String>(), 'test');
        SCbtcContractOrderSchedule obj2 = new SCbtcContractOrderSchedule('de', 3, 'test');
        
        
        Test.stopTest();
    }
}