/* * @(#)SCUtilDatetimeTest.cls
*
* Copyright 2010 by GMS Development GmbH
* Karl-Schurz-Strasse 29,  33100 Paderborn
* All rights reserved. *
* This software is the confidential and proprietary information
* of GMS Development GmbH. ("Confidential Information").  You
* shall not disclose such Confidential Information and shall use
* it only in accordance with the terms of the license agreement
* you entered into with GMS.
*/
/**
*
* @version $Revision$, $Date$
*/

@isTest
private class SCUtilDatetimeTest
{
    static testMethod void testFormatPositiv()
    {
        System.assertequals('01:30', SCutilDatetime.convertTime('1:30'));
        System.assertequals('00:30', SCutilDatetime.convertTime(':30'));
        System.assertequals('00:05', SCutilDatetime.convertTime('5'));
        System.assertequals('01:10', SCutilDatetime.convertTime('70'));
        System.assertequals('02:00', SCutilDatetime.convertTime('120'));
        System.assertequals('03:30', SCutilDatetime.convertTime('3,5'));
        System.assertequals('10:30', SCutilDatetime.convertTime('10.5'));

        System.assertequals('01:30', SCutilDatetime.convertTimeHour('1:30'));
        System.assertequals('00:30', SCutilDatetime.convertTimeHour(':30'));
        System.assertequals('09:15', SCutilDatetime.convertTimeHour('915'));
        System.assertequals('15:30', SCutilDatetime.convertTimeHour('1530'));
        System.assertequals('09:15', SCutilDatetime.convertTimeHour('9 15'));
        System.assertequals('15:30', SCutilDatetime.convertTimeHour('15 30'));
        System.assertequals('12:00', SCutilDatetime.convertTimeHour('12'));
        System.assertequals('08:00', SCutilDatetime.convertTimeHour('8'));
    }

    static testMethod void testFormatNegativ()
    {
        System.assertEquals('0345:45', SCutilDatetime.convertTime('0345:45'));
        System.assertEquals('3.a', SCutilDatetime.convertTime('3.a'));
        System.assertEquals('0345:45', SCutilDatetime.convertTimeHour('0345:45'));
        System.assertEquals('8-45', SCutilDatetime.convertTimeHour('8-45'));
    }

    static testMethod void roundMinutesPositiv()
    {
        Test.startTest();
        
        Database.insert(new SCApplicationSettings__c(DEFAULT_ROUNDMINUTES_GRID__c = null));
        
        // time round method positiv test
        Datetime myDate = Datetime.newInstance(2010, 9, 1, 14, 35, 59);
        Datetime proofDate = Datetime.newInstance(2010, 9, 1, 14, 45, 00);
        Datetime proofDate2 = Datetime.newInstance(2010, 9, 1, 15, 00, 00);

        System.assertequals(proofDate,
                            SCutilDatetime.roundMinutes(myDate, 15));
        System.assertequals(proofDate2,
                            SCutilDatetime.roundMinutes(myDate, 30));
        System.assertequals(proofDate2,
                            SCutilDatetime.roundMinutes(myDate, 61));
        System.assertequals(proofDate,
                            SCutilDatetime.roundMinutes(myDate));
                            
        Test.stopTest();
    }
    
    /**
     * Positiv test for the method minutesBetween().
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void minutesBetweenPositiv()
    {

        // minutesBetween positiv test
        datetime myDate1 = datetime.newInstance (2000, 1, 1, 10, 00, 10);
        datetime myDate2 = datetime.newInstance (2000, 1, 1, 10, 10, 10);

        datetime myDate3 = datetime.newInstance (2000, 1, 1, 10, 00, 10);
        datetime myDate4 = datetime.newInstance (2000, 1, 2, 10, 00, 10);

        Test.startTest();
        System.assertequals(10,
                            SCutilDatetime.minutesBetween(myDate1, myDate2));
        System.assertequals(1440,
                            SCutilDatetime.minutesBetween(myDate3, myDate4));
        System.assertequals(1440,
                            SCutilDatetime.minutesBetween(myDate4, myDate3));
        Test.stopTest();
    }
}