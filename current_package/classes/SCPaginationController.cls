/*
 * @(#)SCPaginationController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The pagination controller is used in the pagination component that allows
 * us to to have the pagination used with every SetController w/o too 
 * much effort.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCPaginationController
{
    // StandardSetController
    public ApexPages.StandardSetController pager { get; set; }


    /**
     * Get the the starting result for the current page
     *
     * @return Starting result item for the current page.
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public Integer getResultFrom()
    {
        try
        {
            Integer resultFrom = (pager.getPageSize() * (pager.getPageNumber() - 1) + 1);
            return resultFrom;
        }
        catch (Exception e)
        {
            return 0;
        }

    }

    /**
     * Get the the ending result for the current page
     *
     * @return Ending result item for the current page.
     * @author Thorsten Klein <tklein@gms-online.de>
     */    
    public Integer getResultTo()
    {
        Integer resultTo = 0;
        try
        {
            resultTo = (pager.getPageSize() * pager.getPageNumber());
            
            if (resultTo > pager.getResultSize())
            {
                resultTo = pager.getResultSize();
            }
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.WARN, '####PG resultTo calculation got an error: ' + e);
        }
        return resultTo;
    }
    
    

    /**
     * Get the total number of results found for the query
     *
     * @return Total number of hits for the query
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public Integer getResultSize()
    {
        try
        {
            Integer resultSize = pager.getResultSize();
            return resultSize;
        }
        catch (Exception e)
        {
            return 0;
        }
    }
    
    /**
     * The total number of pages for the results set. It depends on the
     * total number of results and the page size
     *
     * @return Total number of pages for this result set
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public String pageCount
    {
        get
        {
            if (pager != null)
            {
                System.debug('####PG result size -> ' + pager.getResultSize());
                System.debug('####PG page size -> ' + pager.getPageSize());
                
                Decimal d =  (pager.getResultSize() / (Decimal)pager.getPageSize());
                
                try
                {
                    this.pageCount = String.valueOf(d.round(System.RoundingMode.CEILING));
                }
                catch (Exception e)
                {
                    ApexPages.addMessages(e);
                    pageCount = '1';
                }
            }
            return pageCount;
        }
    
        private set;
    }
    
    /**
     * Getter and setter for the currently selected page.
     *
     * REMARK:
     * This used to be an Integer which at some point of time
     * caused some trouble and was therefore changed to a String
     * This makes the handling a bit more complicated since the 
     * {g|s}etPageNumber are using Integer values.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public String currentPage
    {
        get
        {
            Integer curPage = 1;
            System.debug('####PG setController.getCurrentPage() called!' + currentPage);
            if (pager != null)
            {
                curPage = pager.getPageNumber();
            }
            return curPage.format();
        }
        
        set
        {
            System.debug('####PG setController.setCurrentPage() called!');
            
            if (pager != null)
            {
                Integer curPage = 1;
                try
                {
                    curPage = Integer.valueOf(value);
                }
                catch (Exception e)
                { 
                    // curPage is still 1 in this case.
                }
                
                pager.setPageNumber(curPage);
            }
            this.currentPage = value;
        }
    }

    /**
     * Setter for the current page
     *
     * @return Page reference for the follow-up page 
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference setPage()
    {
        return null;
    }

    /**
     * Directly select the first page
     *
     * @return Page reference for the follow-up page
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference firstPage()
    {
        System.debug('####PG setController.firstPage() called!');
        pager.first();
        return null;
    }

    /**
     * Directly select the last page
     *
     * @return Page reference for the follow-up page
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference lastPage()
    {
        System.debug('####PG setController.lastPage() called!');
        pager.last();
        return null;
    }

    /**
     * Directly select the next page
     *
     * @return Page reference for the follow-up page
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference nextPage()
    {
        System.debug('####PG setController.next() called!');
        pager.next();
        return null;
    }
    
    /**
     * Directly select the previous page
     *
     * @return Page reference for the follow-up page
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public PageReference previousPage()
    {
        System.debug('####PG setController.previous() called!');
        pager.previous();
        return null;
    }


}