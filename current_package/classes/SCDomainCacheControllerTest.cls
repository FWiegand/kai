/*
 * @(#)SCDomainCacheControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCDomainCacheControllerTest
{
    //REMARK:
    // This class assumes that all domains AND all dependencies are filled.
    // It's optimized for code coverage only!

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive1NL()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '0');
        
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assertEquals(3, controller.domvalRead);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive1NL

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive1GB()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '0');
        
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assertEquals(3, controller.domvalRead);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive1GB

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive2()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST5,DOM_TEST4');
        ApexPages.currentPage().getParameters().put('mode', '0');
        
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assertEquals(6, controller.domvalRead);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive2

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive3()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('mode', '0');
        
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive3

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive4()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive4

    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode0Positive5()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '0');
        ApexPages.currentPage().getParameters().put('debug', '1');
        
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('text/plain', controller.getContentType());
        
        String content = controller.getContent();
        
        System.assert(content.length() > 0);
        System.assert(controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
    } // testMode0Positive5
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive1NL1()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('m3d', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('m4d', 'DOM_TEST4');
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(2, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive1NL1
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive1NL2()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(3, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive1NL2
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive1GB1()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('m3d', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('m4d', 'DOM_TEST4');
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(3, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive1GB1
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive1GB2()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('lng', 'en');
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(3, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive1GB2
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive2()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('mode', '1');
    
        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(11, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive2
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive3()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m1v', SCHelperTestClass.DOM_TEST1_VAL1);
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(5, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive3
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive4()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m1v', SCHelperTestClass.DOM_TEST1_VAL3);
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(5, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive4
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive5()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomainTransData();
       
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m1v', SCHelperTestClass.DOM_TEST1_VAL1);
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(2, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
    } // testMode1Positive5
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive6NL()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m1v', SCHelperTestClass.DOM_TEST1_VAL1);
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('m2v', SCHelperTestClass.DOM_TEST2_VAL1);
        ApexPages.currentPage().getParameters().put('m3d', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('m3v', SCHelperTestClass.DOM_TEST3_VAL1);
        ApexPages.currentPage().getParameters().put('m4d', 'DOM_TEST4');
        ApexPages.currentPage().getParameters().put('m4v', SCHelperTestClass.DOM_TEST4_VAL1);
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(1, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
   } // testMode1Positive6NL
    
    /**
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    static testMethod void testMode1Positive6GB()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('m1d', 'DOM_TEST1');
        ApexPages.currentPage().getParameters().put('m1v', SCHelperTestClass.DOM_TEST1_VAL1);
        ApexPages.currentPage().getParameters().put('m2d', 'DOM_TEST2');
        ApexPages.currentPage().getParameters().put('m2v', SCHelperTestClass.DOM_TEST2_VAL1);
        ApexPages.currentPage().getParameters().put('m3d', 'DOM_TEST3');
        ApexPages.currentPage().getParameters().put('m3v', SCHelperTestClass.DOM_TEST3_VAL1);
        ApexPages.currentPage().getParameters().put('m4d', 'DOM_TEST4');
        ApexPages.currentPage().getParameters().put('m4v', SCHelperTestClass.DOM_TEST4_VAL1);
        ApexPages.currentPage().getParameters().put('sd', 'DOM_TEST5');
        ApexPages.currentPage().getParameters().put('mode', '1');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getDependencies();
        
        System.assert(content.length() > 0);
        System.assertEquals(2, controller.domdepRead);
        System.assert(!controller.getRenderDomain());
        System.assert(controller.getRenderDependencies());
   } // testMode1Positive6GB
    
    /**
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    /*static testMethod void testMode2PositiveNL()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass2.createOrderReason(true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('mode', '2');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getOrderReasons();
        
        System.assert(content.length() > 0);
        System.assert(controller.ordReasonRead >= 7);
        System.assert(!controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
        System.assert(controller.getRenderOrderReasons());
    } */ // testMode2PositiveNL
    
    /**
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    /*static testMethod void testMode2PositiveGB()
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass2.createOrderReason(true);
        SCHelperTestClass.createDomainTransData();
    
        ApexPages.currentPage().getParameters().put('mode', '2');

        SCDomainCacheController controller = new SCDomainCacheController();
        System.assertEquals('application/json', controller.getContentType());
        
        String content = controller.getOrderReasons();
        
        System.assert(content.length() > 0);
        System.assert(controller.ordReasonRead >= 5);
        System.assert(!controller.getRenderDomain());
        System.assert(!controller.getRenderDependencies());
        System.assert(controller.getRenderOrderReasons());
    } */ // testMode2PositiveGB
}