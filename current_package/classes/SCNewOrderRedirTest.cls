/*
 * @(#)SCNewOrderRedirTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCNewOrderRedirTest
{
    public static testMethod void newOrderRedirPositiv1() 
    {
        Account newAccount = new Account (name='Test Dummy Account');
        insert newAccount;
        
        ApexPages.StandardController sc = new ApexPages.standardController(newAccount);
        
        SCNewOrderRedir obj = new SCNewOrderRedir(sc);
        PageReference page = obj.orderWizard();
    }
}