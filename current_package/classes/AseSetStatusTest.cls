/*
 * @(#)AseSetStatusTest.cls SCCloud    hs 29.Oct.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class AseSetStatusTest 
{
    /**
     * Class under test
     */
    static testMethod void testSetStatus()
    {
        // create test order
        SCHelperTestClass.createOrder(true);
        // create test assignment
        SCHelperTestClass.createAssignmentMap(true, false, false, false, false, false, false);
        // inform ASE engine about new status planned
        SCAssignment__c assignment = SCHelperTestClass.assignmentMap.get('assignmentDispo1');
        AseSetStatus.callout(assignment.id, assignment.id2__c, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED);
    }
}