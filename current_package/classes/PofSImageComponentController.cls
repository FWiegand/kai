/**********************************************************************

Name: fsFA_PofS_ImageComponentController

======================================================

Purpose: 

This class serves as the data management controller for the PofS Editor view.

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/05/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

public class PofSImageComponentController{
 
	//Non-Static Variables-----------------------------------------------------------------------------
	
    //PofS
	public Id GV_ImageId;
 
    public PictureOfSuccess__c GO_Image;
	
	//PofS Settings
    public PofSSettings__c GO_PofSSettings;
	
    //ClickAreas
	public List< PofSClickarea__c > GL_ClickAreas;
    public Boolean GV_DrawNewArea;
    public Boolean GV_EditArea;
    public Boolean GV_MoveArea;
    public Map< String, PofSClickAreaTypes__c > GM_ClickAreaTypes;
	//Selected ClickArea
	public PofSClickarea__c GO_ImageArea;
    public Id GV_ImageAreaId;
    private String GV_LastClickAreaType;
    public Decimal GV_X;
    public Decimal GV_Y;
	public String GV_NewImageName;

	//ClickAreaQuestions
	public Map< Integer, SObject > GM_ClickAreaQuestions;
    public Integer GV_RemainingQuestions;

	public Integer GV_ClickAreaQuestionRowNumber;
	public Integer GV_OldClickAreaQuestionRowNumber;
	public Integer GV_NewClickAreaQuestionRowNumber;
	
	//Assortments
	public Map< Integer, PofSAssortment__c > GM_Assortments;
	public Integer GV_AssortmentsCount;
	private List< PofSAssortment__c > GL_AssortmentsDelete;
	public Integer GV_AssortmentRowNumber;
	public Integer GV_OldAssortmentRowNumber;
	public Integer GV_NewAssortmentRowNumber;
	
	//Assortment Products
	public Map< Id, ArticleGroup__c > GM_AssortmentProducts;
	
	//Selected AssortmentProduct
	public String GV_AssortmentProductId;
	
	//Assortment Product Filters
	public List< String > GL_FilterFields;
	public Map< String, List< SelectOption > > GM_AssortmentProductFilters;
	public Map< String, String > GM_AssortmentProductFilterValues;
	public Map< String, String > GM_AssortmentProductFilterLabels;
	
	//Document Handling
    public fsFA_PofS_DocumentController GO_DocumentController;
    
	//getter & setter methods------------------------------------------------------------------------
	public Id getGV_ImageId(){
        return GV_ImageId;
    }
    public void setGV_ImageId( Id PV_ImageId ){
        if( GV_ImageId == null ){
            System.debug( 'Set Image ID: ' + PV_ImageId );
            GV_ImageId = PV_ImageId;
            init();
        }
    }

	public PictureOfSuccess__c getGO_Image(){
		return GO_Image;
	}
	public void setGO_Image( PictureOfSuccess__c PL_Value ){
		GO_Image = PL_Value;
	}	
	
	public PofSSettings__c GetGO_PofSSettings(){
		if (GO_PofSSettings == null){
			GO_PofSSettings = PofSSettings__c.getInstance('Default');
		}
		return GO_PofSSettings;
	}
    public void SetGO_PofSSettings( PofSSettings__c PO_Value ){
		GO_PofSSettings = PO_Value;
	}
	
	public List< PofSClickarea__c > getGL_ClickAreas(){
		return GL_ClickAreas;
	}
	public void setGL_ClickAreas( List< PofSClickarea__c > PL_Value ){
		GL_ClickAreas = PL_Value;
	}
	
	public Boolean getGV_DrawNewArea(){
		return GV_DrawNewArea;
	}
	public void setGV_DrawNewArea( Boolean PL_Value ){
		GV_DrawNewArea = PL_Value;
	}
	
	public Boolean getGV_EditArea(){
		return GV_EditArea;
	}
	public void setGV_EditArea( Boolean PL_Value ){
		GV_EditArea = PL_Value;
	}	

	public Boolean getGV_MoveArea(){
		return GV_MoveArea;
	}
	public void setGV_MoveArea( Boolean PL_Value ){
		GV_MoveArea = PL_Value;
	}	
	
	public Map< String, PofSClickAreaTypes__c > getGM_ClickAreaTypes(){
		if( GM_ClickAreaTypes == null ){
                GM_ClickAreaTypes = new Map< String, PofSClickAreaTypes__c >();
                Map< String, PofSClickAreaTypes__c > LM_ClickAreaTypes = PofSClickAreaTypes__c.getAll();
                for( String LV_ClickAreaName: LM_ClickAreaTypes.KeySet() ){
                    PofSClickAreaTypes__c LO_ClickAreaType = LM_ClickAreaTypes.get( LV_ClickAreaName );
                    if ( LO_ClickAreaType.X1__c == null ){
                        LO_ClickAreaType.X1__c = 0;
                    }
                    if ( LO_ClickAreaType.Y1__c == null ){
                        LO_ClickAreaType.Y1__c = 0;
                    }
                    if ( LO_ClickAreaType.X2__c == null ){
                        LO_ClickAreaType.X2__c = 100;
                    }
                    if ( LO_ClickAreaType.Y2__c == null ){
                        LO_ClickAreaType.Y2__c = 100;
                    }
                    GM_ClickAreaTypes.put( LV_ClickAreaName.replace( ' ', '_' ), LO_ClickAreaType );
                }
        }
		return GM_ClickAreaTypes;
	}
	public void setGM_ClickAreaTypes( Map< String, PofSClickAreaTypes__c > PL_Value ){
		GM_ClickAreaTypes = PL_Value;
	}	
	
	public PofSClickarea__c getGO_ImageArea(){
		return GO_ImageArea;
	}
	public void setGO_ImageArea( PofSClickarea__c PL_Value ){
		GO_ImageArea = PL_Value;
	}	
	
	public Id getGV_ImageAreaId(){
		return GV_ImageAreaId;
	}
	public void setGV_ImageAreaId( Id PL_Value ){
		GV_ImageAreaId = PL_Value;
	}	

	public String getGV_LastClickAreaType(){
		return GV_LastClickAreaType;
	}
	public void setGV_LastClickAreaType( String PL_Value ){
		GV_LastClickAreaType = PL_Value;
	}	

	public Decimal getGV_X(){
		return GV_X;
	}
	public void setGV_X( Decimal PL_Value ){
		GV_X = PL_Value;
	}	
	
	public Decimal getGV_Y(){
		return GV_Y;
	}
	public void setGV_Y( Decimal PL_Value ){
		GV_Y = PL_Value;
	}	
	
	public String getGV_NewImageName(){
		return GV_NewImageName;
	}
	public void setGV_NewImageName( String PL_Value ){
		GV_NewImageName = PL_Value;
	}	
	
	public Map< Integer, SObject > getGM_ClickAreaQuestions(){
		return GM_ClickAreaQuestions;
	}
	public void setGM_ClickAreaQuestions( Map< Integer, SObject > PL_Value ){
		GM_ClickAreaQuestions = PL_Value;
	}	
	
	public Integer getGV_RemainingQuestions(){
		Integer LV_RemainingQuestions = 1;
                String LV_ClickAreaType = GO_ImageArea.Type__c;
                if( ( LV_ClickAreaType != null ) && ( LV_ClickAreaType != '' ) ){
                    PofSClickAreaTypes__c LO_ClickAreaSettings = GM_ClickAreaTypes.get( LV_ClickAreaType.replace( ' ', '_' ) );
                    if( LO_ClickAreaSettings != null ){
                        Integer LV_TotalQuestions = Integer.valueOf( LO_ClickAreaSettings.Max_Number_Of_Questions__c );
                        if( LV_TotalQuestions != null ){
                            Integer LV_ExistingQuestions = getNumberOfQuestions();
                            LV_RemainingQuestions = LV_TotalQuestions - LV_ExistingQuestions;
                        }
                    }
                }else{
                    LV_RemainingQuestions = 0;
                }
            GV_RemainingQuestions = LV_RemainingQuestions;
		return GV_RemainingQuestions;
	}
	public void setGV_RemainingQuestions( Integer PL_Value ){
		GV_RemainingQuestions = PL_Value;
	}	
	
	public Integer getGV_ClickAreaQuestionRowNumber(){
		return GV_ClickAreaQuestionRowNumber;
	}
	public void setGV_ClickAreaQuestionRowNumber( Integer PL_Value ){
		GV_ClickAreaQuestionRowNumber = PL_Value;
	}	
	
	public Integer getGV_OldClickAreaQuestionRowNumber(){
		return GV_OldClickAreaQuestionRowNumber;
	}
	public void setGV_OldClickAreaQuestionRowNumber( Integer PL_Value ){
		GV_OldClickAreaQuestionRowNumber = PL_Value;
	}	
	
	public Integer getGV_NewClickAreaQuestionRowNumber(){
		return GV_NewClickAreaQuestionRowNumber;
	}
	public void setGV_NewClickAreaQuestionRowNumber( Integer PL_Value ){
		GV_NewClickAreaQuestionRowNumber = PL_Value;
	}	

	public Map< Integer, PofSAssortment__c > getGM_Assortments(){
		if( GM_Assortments == null ){
				GM_Assortments = new Map< Integer, PofSAssortment__c >();
				//List< SObject > LL_Assortments = fsFA_Tools.getRecordsFlat( 'PofSAssortment__c', 'PictureOfSuccess__c', GV_ImageId ).Values();
				List< SObject > LL_Assortments = [ SELECT Produkt__c, ArticleGroupRelation__r.Picture__c, ArticleGroupRelation__r.Packaging_Size__c, ArticleGroupRelation__r.KindOfPackage__c, ManualSurvey__c, HasReplacementArticle__c, RowNumber__c FROM PofSAssortment__c WHERE ( PictureOfSuccess__c =: GV_ImageId ) ];
				for( SObject LO_Assortment: LL_Assortments ){
					GM_Assortments.put( Integer.valueOf( LO_Assortment.get( 'RowNumber__c' ) ), (PofSAssortment__c) LO_Assortment );
				}
			}
		return GM_Assortments;
	}
	public void setGM_Assortments( Map< Integer, PofSAssortment__c > PL_Value ){
		GM_Assortments = PL_Value;
	}	

	public Integer getGV_AssortmentsCount(){
		return GM_Assortments.size();
	}
	public void setGV_AssortmentsCount( Integer PL_Value ){
		GV_AssortmentsCount = PL_Value;
	}	

	public List< PofSAssortment__c > getGL_AssortmentsDelete(){
		if( GL_AssortmentsDelete == null ){
			GL_AssortmentsDelete = new List< PofSAssortment__c >();
		}
		return GL_AssortmentsDelete;
	}
	public void setGL_AssortmentsDelete( List< PofSAssortment__c > PL_Value ){
		GL_AssortmentsDelete = PL_Value;
	}	

	public Integer getGV_AssortmentRowNumber(){
		return GV_AssortmentRowNumber;
	}
	public void setGV_AssortmentRowNumber( Integer PL_Value ){
		GV_AssortmentRowNumber = PL_Value;
	}	

	public Integer getGV_OldAssortmentRowNumber(){
		return GV_OldAssortmentRowNumber;
	}
	public void setGV_OldAssortmentRowNumber( Integer PL_Value ){
		GV_OldAssortmentRowNumber = PL_Value;
	}	

	public Integer getGV_NewAssortmentRowNumber(){
		return GV_NewAssortmentRowNumber;
	}
	public void setGV_NewAssortmentRowNumber( Integer PL_Value ){
		GV_NewAssortmentRowNumber = PL_Value;
	}	

	public Map< Id, ArticleGroup__c > getGM_AssortmentProducts(){
				String LV_AssortmentProductsFilterString = '';
				if( getGM_AssortmentProductFilterValues().size() > 0 ){
					LV_AssortmentProductsFilterString = '';
					String LV_Seperator = ' WHERE ';
					for( String LV_FieldName: getGM_AssortmentProductFilterValues().KeySet() ){
						if( getGM_AssortmentProductFilterValues().get( LV_FieldName ) != '*' ){
							LV_AssortmentProductsFilterString += LV_Seperator + LV_FieldName + '=\'' + getGM_AssortmentProductFilterValues().get( LV_FieldName ) + '\'';
							LV_Seperator = ' AND ';
						}
					}
				}
				GM_AssortmentProducts = new Map< Id, ArticleGroup__c >();
				List< ArticleGroup__c > LL_AssortmentProducts = Database.query( 'SELECT Name, Picture__c, Preview__c, KindOfPackage__c, Packaging_Size__c FROM ArticleGroup__c' + LV_AssortmentProductsFilterString );
				for( ArticleGroup__c LO_AssortmentProduct: LL_AssortmentProducts ){
					if( !existsAssortment( LO_AssortmentProduct.Id ) ){
						GM_AssortmentProducts.put( LO_AssortmentProduct.Id, LO_AssortmentProduct );
					}
				}
				return GM_AssortmentProducts;
	}
	public void setGM_AssortmentProducts( Map< Id, ArticleGroup__c > PL_Value ){
		GM_AssortmentProducts = PL_Value;
	}

	public String getGV_AssortmentProductId(){
		return GV_AssortmentProductId;
	}
	public void setGV_AssortmentProductId( String PL_Value ){
		GV_AssortmentProductId = PL_Value;
	}	

	public List< String > getGL_FilterFields(){
		return GL_FilterFields;
	}
	public void setGL_FilterFields( List< String > PL_Value ){
		GL_FilterFields = PL_Value;
	}	

	public Map< String, List< SelectOption > > getGM_AssortmentProductFilters(){
			if( GM_AssortmentProductFilters == null ){
				GL_FilterFields = new List< String >{ 'Group__c', 'SubGroup__c', 'KindOfPackage__c', 'Packaging_size__c' };
				Map< String, Schema.SObjectField > LM_Fields = Schema.SObjectType.ArticleGroup__c.fields.getMap();
				GM_AssortmentProductFilters = new Map< String, List< SelectOption > >();
				GM_AssortmentProductFilterLabels = new Map< String, String >();
				//Schema.DescribeFieldResult LO_DescribeField = ArticleGroup__c.Group__c.getDescribe();
				Schema.DescribeFieldResult LO_DescribeField;
				List<Schema.PicklistEntry> LL_PicklistEntries;
				String LV_FieldLabel;
				List< SelectOption > LL_ProductFilter;
				for( String LV_FilterField: GL_FilterFields ){
					LO_DescribeField = LM_Fields.get( LV_FilterField ).getDescribe();
					LL_PicklistEntries = LO_DescribeField.getPicklistValues();
					LV_FieldLabel = LO_DescribeField.getLabel();
					LL_ProductFilter = new List< SelectOption >();
					LL_ProductFilter.add( new SelectOption( '*', 'All' ) );
					for( Schema.PicklistEntry LO_PicklistEntry: LL_PicklistEntries ){
						LL_ProductFilter.add( new SelectOption( LO_PicklistEntry.getValue(), LO_PicklistEntry.getLabel() ) );
					}
					GM_AssortmentProductFilters.put( LV_FilterField, LL_ProductFilter );
					GM_AssortmentProductFilterLabels.put( LV_FilterField, LV_FieldLabel );
				}
			}
		return GM_AssortmentProductFilters;
	}
	public void setGM_AssortmentProductFilters( Map< String, List< SelectOption > > PL_Value ){
		GM_AssortmentProductFilters = PL_Value;
	}	

	public Map< String, String > getGM_AssortmentProductFilterValues(){
			if( GM_AssortmentProductFilterValues == null ){
                getGM_AssortmentProductFilters();
				GM_AssortmentProductFilterValues = new Map< String, String >();
				for( String LV_FilterName: GM_AssortmentProductFilters.keySet() ){
					GM_AssortmentProductFilterValues.put( LV_FilterName, '*' );
				}
			}
		return GM_AssortmentProductFilterValues;
	}
	public void setGM_AssortmentProductFilterValues( Map< String, String > PL_Value ){
		GM_AssortmentProductFilterValues = PL_Value;
	}	

	public Map< String, String > getGM_AssortmentProductFilterLabels(){
		return GM_AssortmentProductFilterLabels;
	}
	public void setGM_AssortmentProductFilterLabels( Map< String, String > PL_Value ){
		GM_AssortmentProductFilterLabels = PL_Value;
	}	

	public fsFA_PofS_DocumentController getGO_DocumentController(){
		return GO_DocumentController;
	}
	public void setGO_DocumentController( fsFA_PofS_DocumentController PL_Value ){
		GO_DocumentController = PL_Value;
	}	

	//Constructors-----------------------------------------------------------------------------
    public PofSImageComponentController(){
    	
    }
    
	//Action Methods---------------------------------------------------------------------------
    
	/*******************************************************************
	 Purpose: 		Initialize the PofS when the page loads
	 ********************************************************************/
	public void init(){
		System.debug( 'Entering init: ' );
		
        loadImage();
        GV_DrawNewArea = false;
        GV_EditArea = false;
        GV_MoveArea = false;
        GO_DocumentController = new fsFA_PofS_DocumentController();
		
		System.debug( 'Exiting init: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Initialize the PofS when the page loads
	 ********************************************************************/	
	private void loadImage(){
		System.debug( 'Entering loadImage: ' );
		
        GO_Image = ( PictureOfSuccess__c ) fsFA_Tools.getRecord( GV_ImageId );
        List< SObject > LL_ClickAreas = fsFA_Tools.getRecordsFlat( 'PofSClickarea__c', 'PictureOfSuccess__c', GV_ImageId ).Values();
        GL_ClickAreas = new List< PofSClickarea__c >();
        for( SObject LO_ClickArea: LL_ClickAreas ){
            GL_ClickAreas.add( ( PofSClickarea__c ) LO_ClickArea );
        }
		
		System.debug( 'Exiting loadImage: ' );
    }
    
	/*******************************************************************
	 Purpose: 		initialize a new ClickArea after the user clicked on the New button
	 ********************************************************************/	
    public void drawNewArea(){
		System.debug( 'Entering drawNewArea: ' );
		
        GO_ImageArea = new PofSClickarea__c(
        	Picture__c = GO_DocumentController.getGL_ClickAreaQuestions().get( 0 ).getValue(),
            CenterX__c = 50,
            CenterY__c = 50
        );
        GM_ClickAreaQuestions = new Map< Integer, RedSurveyQuestion__c >();
        GV_DrawNewArea = true;
        GV_EditArea = true;
        GV_MoveArea = false;
		
		System.debug( 'Exiting drawNewArea: ' );
    }

	/*******************************************************************
	 Purpose: 		Save the current ClickArea, the questions and the assortments after the user clicked on the Save button
	 ********************************************************************/	
    public void saveArea(){
        getGM_ClickAreaTypes();
		System.debug( 'Entering saveArea: ' );
		if( GM_ClickAreaTypes.get( GO_ImageArea.Type__c.replace( ' ', '_' ) ).MinNumberOfQuestions__c <= GM_ClickAreaQuestions.size() ){
        if( GV_DrawNewArea ){
            GO_ImageArea.PictureOfSuccess__c = GV_ImageId;
            GO_ImageArea.CenterX__c = GV_X;
            GO_ImageArea.CenterY__c = GV_Y;
            
            insert( GO_ImageArea );
			for( SObject LO_ClickAreaQuestion: GM_ClickAreaQuestions.values() ){
				LO_ClickAreaQuestion.put( 'ClickArea__c', GO_ImageArea.Id );
			}
			insert( GM_ClickAreaQuestions.Values() );
			saveAssortments();
        }else{
            if( GV_X != 0 ){
                GO_ImageArea.CenterX__c = GV_X;
                GO_ImageArea.CenterY__c = GV_Y;
            }
            update( GO_ImageArea );
			saveQuestions();
			saveAssortments();
        }
        
        loadImage();
        GV_ImageAreaId = GO_ImageArea.Id;
        GV_DrawNewArea = false;
        GV_EditArea = true;
        GV_MoveArea = false;
		}else{
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'There are not enough questions added to this area (minimum ' + GM_ClickAreaTypes.get( GO_ImageArea.Type__c.replace( ' ', '_' ) ).MinNumberOfQuestions__c + ')' ) );
		}
		
		System.debug( 'Exiting saveArea: ' );
    }
	
	/*******************************************************************
	 Purpose: 		Save the questions of the current ClickArea
	 ********************************************************************/	
	private void saveQuestions(){
		System.debug( 'Entering saveQuestions: ' );
		
			List< SObject > LL_ExistingClickAreaQuestions = new List< SObject >();
			List< SObject > LL_NewClickAreaQuestions = new List< SObject >();
			for( SObject LO_ClickAreaQuestion: GM_ClickAreaQuestions.values() ){
				if( ( LO_CLickAreaQuestion.Id != null ) ){
					LL_ExistingClickAreaQuestions.add( LO_CLickAreaQuestion );
				}else{
					LL_NewClickAreaQuestions.add( LO_CLickAreaQuestion );
				}
			}
            update( LL_ExistingClickAreaQuestions );
			insert( LL_NewClickAreaQuestions );
			
		System.debug( 'Exiting saveQuestions: ' );
	}
	
	/*******************************************************************
	 Purpose: 		Save the assortments of the current ClickArea
	 ********************************************************************/	
	private void saveAssortments(){
		System.debug( 'Entering saveAssortments: ' );
		
			List< PofSAssortment__c > LL_ExistingAssortments = new List< PofSAssortment__c >();
			List< PofSAssortment__c > LL_NewAssortments = new List< PofSAssortment__c >();
			System.debug( 'AssortmentSize: ' + getGM_Assortments().size() );
			for( PofSAssortment__c LO_Assortment: getGM_Assortments().values() ){
				if( ( LO_Assortment.Id != null ) ){
					LL_ExistingAssortments.add( LO_Assortment );
				}else{
					LL_NewAssortments.add( LO_Assortment );
				}
			}
            update( LL_ExistingAssortments );
			insert( LL_NewAssortments );
			System.debug( getGL_AssortmentsDelete() );
			delete( getGL_AssortmentsDelete() );
			setGM_Assortments( null );
			setGL_AssortmentsDelete( null );
			
		System.debug( 'Exiting saveAssortments: ' );
	}
    
	/*******************************************************************
	 Purpose: 		Prepare the current ClickArea to be moved by the user after the Move button was clicked.
	 ********************************************************************/	
    public void moveArea(){
		System.debug( 'Entering moveArea: ' );
		
        GV_MoveArea = true;
		
		System.debug( 'Exiting moveArea: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Cancel the current ClickArea
	 ********************************************************************/	
    public void cancelArea(){
		System.debug( 'Entering cancelArea: ' );
		
        GV_DrawNewArea = false;
        GV_EditArea = false;
        GV_MoveArea = false;
		
		System.debug( 'Exiting cancelArea: ' );
    }
    
    public void updateCoordinates(){
    
    }
    
	/*******************************************************************
	 Purpose: 		Load the data of a specific ClickArea after the user clicked on it.
	 ********************************************************************/	
    public void chooseArea(){
		System.debug( 'Entering chooseArea: ' );
		
        GO_ImageArea = ( PofSClickArea__c ) fsFA_Tools.getRecord( GV_ImageAreaId );
		GV_LastClickAreaType = GO_ImageArea.Type__c;
        Map< Id, SObject > LM_ClickAreaQuestions = fsFA_Tools.getRecordsFlat( 'RedSurveyQuestion__c', 'ClickArea__c', GV_ImageAreaId );
		GM_ClickAreaQuestions = new Map< Integer, SObject >();
		for( SObject LO_CLickAreaQuestion: LM_ClickAreaQuestions.values() ){
			GM_ClickAreaQuestions.put( Integer.valueOf( LO_CLickAreaQuestion.get( 'RowNumber__c' ) ), LO_CLickAreaQuestion );
		}
        GV_EditArea = true;
		
		System.debug( 'Exiting chooseArea: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Delete the current ClickArea after the user clicked on the Delete button
	 ********************************************************************/	
    public void deleteArea(){
		System.debug( 'Entering deleteArea: ' );
		
        delete( GO_ImageArea );
        loadImage();
        GV_EditArea = false;
		
		System.debug( 'Exiting deleteArea: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Create a new ClickAreaQuestion after the user clicked on the New button
	 ********************************************************************/	
    public void newQuestion(){
		System.debug( 'Entering newQuestion: ' );
		
        RedSurveyQuestion__c LO_NewQuestion = new RedSurveyQuestion__c(
        	ClickArea__c = GO_ImageArea.Id,
			RowNumber__c = getNumberOfQuestions() + 1
        );
		GM_ClickAreaQuestions.put( Integer.valueOf( LO_NewQuestion.RowNumber__c ), LO_NewQuestion );
		
		System.debug( 'Exiting newQuestion: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Delete the current ClickAreaQuestion after the user clicked on the Delete button
	 ********************************************************************/	
    public void deleteQuestion(){
		System.debug( 'Entering deleteQuestion: ' );
		
		SObject LO_ClickAreaQuestionDelete = GM_ClickAreaQuestions.get( GV_ClickAreaQuestionRowNumber );
        if( ( LO_ClickAreaQuestionDelete.Id != null ) ){
			delete( LO_ClickAreaQuestionDelete );
		}
        GM_ClickAreaQuestions.remove( GV_ClickAreaQuestionRowNumber );
		Map< Integer, SObject > LM_ClickAreaQuestions = new Map< Integer, SObject >();
		for( SObject LO_ClickAreaQuestion: GM_ClickAreaQuestions.values() ){
			if( Integer.valueOf( LO_ClickAreaQuestion.get( 'RowNumber__c' ) ) > GV_ClickAreaQuestionRowNumber ){
				LO_ClickAreaQuestion.put( 'RowNumber__c', Integer.valueOf( LO_CLickAreaQuestion.get( 'RowNumber__c' ) ) - 1 );
			}
			LM_ClickAreaQuestions.put( Integer.valueOf( LO_ClickAreaQuestion.get( 'RowNumber__c' ) ), LO_ClickAreaQuestion );
		}
		GM_ClickAreaQuestions = LM_ClickAreaQuestions;
		
		System.debug( 'Exiting deleteQuestion: ' );
    }
    
 	/*******************************************************************
	 Purpose: 		Get the number of questions associated with the current ClickArea
	 ********************************************************************/	
   private Integer getNumberOfQuestions(){
	   	System.debug( 'Entering getNumberOfQuestions: ' );
	   
		Integer LV_NumberOfExistingQuestion = GM_ClickAreaQuestions.size();
	   
        System.debug( 'Exiting getNumberOfQuestions: ' + LV_NumberOfExistingQuestion );
		return LV_NumberOfExistingQuestion;
    }
    
 	/*******************************************************************
	 Purpose: 		Check if the last change of the ClickArea Type was valid. There may be restrictions regarding the maximum number of ClickAreas of a specific type 
	 				or with the maximum number of questions associated with a specific type. If there is a problem the last type will be restored and an error will be generated.
	 ********************************************************************/	
   public void changeClickAreaType(){
	   	System.debug( 'Entering changeClickAreaType: ' );
	   
        if( GV_RemainingQuestions < 0 ){
            GO_ImageArea.Type__c.addError( 'You cannot switch to the Type to "' + GO_ImageArea.Type__c + '" because there are already more questions related to the Click Area than supported by this Type.' );
            GO_ImageArea.Type__c = GV_LastClickAreaType;
        }else{
			if( isNewClickAreaValid( GO_ImageArea.Type__c ) ){
				GV_LastClickAreaType = GO_ImageArea.Type__c;
			}else{
				GO_ImageArea.Type__c.addError( 'You cannot switch to the Type to "' + GO_ImageArea.Type__c + '" because there are already more Click Areas present than supported by this Type.' );
				GO_ImageArea.Type__c = GV_LastClickAreaType;
			}
        }
		
		System.debug( 'Exiting changeClickAreaType: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Check if the newly created ClickArea is valid. There may be restrictions regarding the maximum number of ClickAreas
     Parameters:	String PV_ClickAreaType
     Returns:		Boolean
	 ********************************************************************/	
	public Boolean isNewClickAreaValid( String PV_ClickAreaType ){
		System.debug( 'Entering isNewClickAreaValid: ' + PV_ClickAreaType );
		
		PofSClickAreaTypes__c LO_ClickAreaSettings = GM_ClickAreaTypes.get( PV_ClickAreaType.replace( ' ', '_' ) );
		System.debug( 'Type: ' + PV_ClickAreaType );
		System.debug( 'Settings: ' + LO_ClickAreaSettings );
		Integer LV_CLickAreaCount = [ SELECT COUNT() FROM PofSClickarea__c WHERE ( ( PictureOfSuccess__c =: GO_Image.Id ) AND ( Type__c =: PV_ClickAreaType ) AND ( Id !=: GO_ImageArea.Id ) ) ];
		if( GO_ImageArea.Type__c == GV_LastClickAreaType ){
			LV_CLickAreaCount++;
		}
		System.debug( 'Count: ' + LV_CLickAreaCount );
		Boolean LV_Result;
		if( LO_ClickAreaSettings.MaxNumberOfAreas__c > LV_ClickAreaCount ){
			LV_Result = true;
		}else{
			LV_Result = false;
		}
		
		System.debug( 'Exiting isNewClickAreaValid: ' + LV_Result );
		return LV_Result;
	}
	
	/*******************************************************************
	 Purpose: 		Get a specific ClickAreaQuestion for a given row number
	 Parameters:	Integer PV_RowNumber
	 Returns:		SObject
	 ********************************************************************/	
	private SObject getQuestion( Integer PV_RowNumber ){
		System.debug( 'Entering getQuestion: ' + PV_RowNumber );
		
		if( GM_ClickAreaQuestions.containsKey( PV_RowNumber ) ){
			System.debug( 'Exiting getQuestion: ' + GM_ClickAreaQuestions.get( PV_RowNumber ) );
			return GM_ClickAreaQuestions.get( PV_RowNumber );
		}else{
			System.debug( 'Exiting getQuestion: ' + null );
			return null;
		}
	}
	
	/*******************************************************************
	 Purpose: 		Change the question order after a drag & drop
	 ********************************************************************/	
	public void changeQuestionOrder(){
		System.debug( 'Entering changeQuestionOrder: ' );
		
		SObject LO_MovedClickAreaQuestion = getQuestion( GV_OldClickAreaQuestionRowNumber );
		if( LO_MovedClickAreaQuestion != null ){
			Integer LV_OldClickAreaQuestionRowNumber = Integer.valueOf( LO_MovedClickAreaQuestion.get( 'RowNumber__c' ) );
			
			if( LV_OldClickAreaQuestionRowNumber < GV_NewClickAreaQuestionRowNumber ){
				for( Integer LV_Counter = LV_OldClickAreaQuestionRowNumber + 1; LV_Counter <= GV_NewClickAreaQuestionRowNumber; LV_Counter++ ){
					SObject LO_AffectedClickAreaQuestion = getQuestion( LV_Counter );
					if( LO_AffectedClickAreaQuestion != null ){
						Decimal LV_NewRowNumber = (Decimal)LO_AffectedClickAreaQuestion.get( 'RowNumber__c' ) - 1 ;
						LO_AffectedClickAreaQuestion.put( 'RowNumber__c', LV_NewRowNumber );
					}
				}
			}else if( LV_OldClickAreaQuestionRowNumber > GV_NewClickAreaQuestionRowNumber ){
				for( Integer LV_Counter = GV_NewClickAreaQuestionRowNumber; LV_Counter <= LV_OldClickAreaQuestionRowNumber - 1; LV_Counter++ ){
					SObject LO_AffectedClickAreaQuestion = getQuestion( LV_Counter );
					if( LO_AffectedClickAreaQuestion != null ){
						Decimal LV_NewRowNumber = (Decimal)LO_AffectedClickAreaQuestion.get( 'RowNumber__c' ) + 1 ;
						LO_AffectedClickAreaQuestion.put( 'RowNumber__c', LV_NewRowNumber );
					}
				}
			}
			LO_MovedClickAreaQuestion.put( 'RowNumber__c', GV_NewClickAreaQuestionRowNumber );
			
			Map< Integer, SObject > LM_ClickAreaQuestions = new Map< Integer, SObject >();
			for( SObject LO_ClickAreaQuestion: GM_ClickAreaQuestions.Values() ){
				LM_ClickAreaQuestions.put( Integer.valueOf( LO_ClickAreaQuestion.get( 'RowNumber__c' ) ), LO_ClickAreaQuestion );
			}
			GM_ClickAreaQuestions = LM_ClickAreaQuestions;
		}
		
		System.debug( 'Exiting changeQuestionOrder: ' );
	}
	
	/*******************************************************************
	 Purpose: 		Get an assortment article for a given id
	 Parameters:	String PV_AssortmentId
	 Returns:		ArticleGroup__c
	 ********************************************************************/	
	private ArticleGroup__c getAssortmentProduct( String PV_AssortmentId ){
		System.debug( 'Entering getAssortmentProduct: ' + PV_AssortmentId );
		getGM_AssortmentProducts();
        
		if( GM_AssortmentProducts.containsKey( PV_AssortmentId ) ){
			System.debug( 'Exiting getAssortmentProduct: ' + GM_AssortmentProducts.get( PV_AssortmentId ) );
			return GM_AssortmentProducts.get( PV_AssortmentId );
		}else{
			System.debug( 'Exiting getAssortmentProduct: ' + null );
			return null;
		}
	}

	/*******************************************************************
	 Purpose: 		Get an assortment for a given row number
	 Parameters:	Integer PV_AssortmentRow
	 Returns: 		 PofsAssortment__c
	 ********************************************************************/	
	public PofsAssortment__c getAssortment( Integer PV_AssortmentRow ){
		System.debug( 'Entering getAssortment: ' + PV_AssortmentRow );
		
		if( getGM_Assortments().containsKey( PV_AssortmentRow ) ){
			System.debug( 'Exiting getAssortment: ' + getGM_Assortments().get( PV_AssortmentRow ) );
			return getGM_Assortments().get( PV_AssortmentRow );
		}else{
			System.debug( 'Exiting getAssortment: ' + null );
			return null;
		}
	}
		
	/*******************************************************************
	 Purpose: 		Create a new assortment for the current ClickArea after a user clicked on the New Button
	 ********************************************************************/	
	public void newAssortment(){
		System.debug( 'Entering newAssortment: ' );
		
		ArticleGroup__c LO_AssortmentProduct = getAssortmentProduct( GV_AssortmentProductId );
		PofsAssortment__c LO_NewAssortment = new PofSAssortment__c(
			PictureOfSuccess__c = GV_ImageId,
			ArticleGroupRelation__c = GV_AssortmentProductId,
			ArticleGroupRelation__r = GM_AssortmentProducts.get( GV_AssortmentProductId ),
			RowNumber__c = GV_AssortmentRowNumber
		);
		Map< Integer, PofSAssortment__c > LM_Assortments = new Map< Integer, PofSAssortment__c >();
		for( PofSAssortment__c LO_Assortment: getGM_Assortments().values() ){
			if( LO_Assortment.RowNumber__c >= GV_AssortmentRowNumber ){
				LO_Assortment.RowNumber__c++;
			}
			LM_Assortments.put( Integer.valueOf( LO_Assortment.RowNumber__c ), LO_Assortment );
		}
		setGM_Assortments( LM_Assortments );
		getGM_Assortments().put( Integer.valueOf( LO_NewAssortment.RowNumber__c ), LO_NewAssortment );
		
		System.debug( 'Exiting newAssortment: ' );
	}
	
	/*******************************************************************
	 Purpose: 		Delete the current Assortment after a user clicked on the Delete Button
	 ********************************************************************/	
	public void deleteAssortment(){
		System.debug( 'Entering deleteAssortment: ' );
		
		getGL_AssortmentsDelete().add( getGM_Assortments().get( GV_AssortmentRowNumber ) );
		getGM_Assortments().remove( GV_AssortmentRowNumber );
		System.debug( 'delete Size 2: ' + getGM_Assortments().size() );
		Map< Integer, PofSAssortment__c > LM_Assortments = new Map< Integer, PofSAssortment__c >();
		for( PofSAssortment__c LO_Assortment: getGM_Assortments().values() ){
			System.debug( 'RowNumber: ' + LO_Assortment.RowNumber__c );
			if( LO_Assortment.RowNumber__c > GV_AssortmentRowNumber ){
				LO_Assortment.RowNumber__c--;
				System.debug( '--' );
				
			}
			LM_Assortments.put( Integer.valueOf( LO_Assortment.RowNumber__c ), LO_Assortment );
		}
		setGM_Assortments( LM_Assortments );
		
		System.debug( 'Exiting deleteAssortment: ' );
	}

	/*******************************************************************
	 Purpose: 		Change the order of the Assortments after Drag & Drop
	 ********************************************************************/	
	public void changeAssortmentOrder(){
		System.debug( 'Entering changeAssortmentOrder: ' );
		
		SObject LO_MovedAssortment = getAssortment( GV_OldAssortmentRowNumber );
		if( LO_MovedAssortment != null ){
			Integer LV_OldAssortmentRowNumber = Integer.valueOf( LO_MovedAssortment.get( 'RowNumber__c' ) );
			
			if( LV_OldAssortmentRowNumber < GV_NewAssortmentRowNumber ){
				for( Integer LV_Counter = LV_OldAssortmentRowNumber + 1; LV_Counter <= GV_NewAssortmentRowNumber; LV_Counter++ ){
					PofsAssortment__c LO_AffectedAssortment = getAssortment( LV_Counter );
					if( LO_AffectedAssortment != null ){
						Decimal LV_NewRowNumber = (Decimal)LO_AffectedAssortment.get( 'RowNumber__c' ) - 1 ;
						LO_AffectedAssortment.put( 'RowNumber__c', LV_NewRowNumber );
					}
				}
			}else if( LV_OldAssortmentRowNumber > GV_NewAssortmentRowNumber ){
				for( Integer LV_Counter = GV_NewAssortmentRowNumber; LV_Counter <= LV_OldAssortmentRowNumber - 1; LV_Counter++ ){
					PofsAssortment__c LO_AffectedAssortment = getAssortment( LV_Counter );
					if( LO_AffectedAssortment != null ){
						Decimal LV_NewRowNumber = (Decimal)LO_AffectedAssortment.get( 'RowNumber__c' ) + 1 ;
						LO_AffectedAssortment.put( 'RowNumber__c', LV_NewRowNumber );
					}
				}
			}
			LO_MovedAssortment.put( 'RowNumber__c', GV_NewAssortmentRowNumber );
			
			Map< Integer, PofsAssortment__c > LM_Assortments = new Map< Integer, PofsAssortment__c >();
			for( PofsAssortment__c LO_Assortment: getGM_Assortments().Values() ){
				LM_Assortments.put( Integer.valueOf( LO_Assortment.get( 'RowNumber__c' ) ), LO_Assortment );
			}
			setGM_Assortments( LM_Assortments );
		}
		
		System.debug( 'Exiting changeAssortmentOrder: ' );
	}
	
	/*******************************************************************
	 Purpose: 		Check if an Article is already chosen
	 Parameters: 	Id PV_AssortmentProductId
	 Returns: 		Boolean
	 ********************************************************************/	
	private Boolean existsAssortment( Id PV_AssortmentProductId ){
		System.debug( 'Entering existsAssortment: ' + PV_AssortmentProductId );
		
		Boolean LV_AssortmentExists = false;
		for( PofSAssortment__c LO_Assortment: getGM_Assortments().values() ){
			if( LO_Assortment.ArticleGroupRelation__c == PV_AssortmentProductId ){
				LV_AssortmentExists = true;
			}
		}
		System.debug( 'Exiting existsAssortment: ' + LV_AssortmentExists );
		
		return LV_AssortmentExists;
	}
	
	/*******************************************************************
	 Purpose: 		Refreshes the ClickArea Image Preview
	 ********************************************************************/	
    public void refreshClickAreaQuestionImages(){
		System.debug( 'Entering refreshClickAreaQuestionImages: ' );
		
        if( GO_ImageArea != null ){
			GO_DocumentController.refreshGL_ClickAreaQuestionImages();
			GO_ImageArea.Picture__c = GO_DocumentController.getClickAreaQuestionImageByName( GV_NewImageName );
		}
		
		System.debug( 'Exiting refreshClickAreaQuestionImages: ' );
    }
	
	/*******************************************************************
	 Purpose: 		Saves the original size of the PofS after the image was loaded in the client
	 ********************************************************************/	
	public void saveImageSize(){
		System.debug( 'Entering saveImageSize: ' );
		
		update GO_Image;
		
		System.debug( 'Exiting saveImageSize: ' );
	}
	
	/*******************************************************************
	 Purpose: 		Dummy Function
	 ********************************************************************/	
    public void Dummy(){
		System.debug( 'Entering Dummy: ' );
		System.debug( 'Exiting Dummy: ' );
    }

	/*******************************************************************
	 Purpose: 		Refresh the list of available ClickArea Images after an upload
	 ********************************************************************/	
    public void refreshBackgrounds(){
		System.debug( 'Entering refreshBackgrounds: ' );
		
        GO_DocumentController.refreshGL_Backgrounds();
		
		System.debug( 'Exiting refreshBackgrounds: ' );
    }    
}