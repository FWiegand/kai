/*
 * @(#)SCTimereportControllerTest.cls    
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @author Sebastian Schrage  <sschrage@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCTimereportControllerTest
{
    private static SCTimereportController timereportController;
    
    /**
     * init method under positiv test
     */      
    static testMethod void initSCTimereportControllerPositiv()
    {
        SCTool__c toolData = new SCTool__c();
        timereportController = new SCTimereportController ();
        timereportController.toolData.Start__c = date.newinstance(2010, 10, 09);
        //String todayString = datetime.now().format('yyyyMMdd');
        
        test.startTest();
        
        // positiv test for the method onSelect()
        timereportController.onSelect();
        
        // positiv test for the method onSelect(), dayStartListString is not null
        timereportController.dayStartListString = '20101009';
        timereportController.onSelect();
        
        System.assertEquals('20101009', timereportController.day);
        
        // positiv test for the method getDayOffset()
        timereportController.getDayOffset();
        
        // positiv test for the method getDayOffset() with start equals null
        timereportController.toolData.Start__c = null;

        timereportController.getDayOffset();
        test.stopTest();
    }

    
    /**
     * timereportController under positiv test 2
     */           
    static testMethod void timereportControllerPositiv2()
    {        
        // create test users with the helper test class without resource
        SCHelperTestClass.createTestUsers(true);
        
        User testUser = SCHelperTestClass.users.get(0);
        
        test.startTest();
        
        // run the test with one user 
        // The user had no resource in the system.
        System.runAs(testUser)
        {
            timereportController = new SCTimereportController ();
        }
        
        test.stopTest();
    }
    
        /**
     * timereportController under positiv test 3
     */           
    static testMethod void timereportControllerPositiv3()
    {
        timereportController = new SCTimereportController ();
        timereportController.changeTimeReportStatus();
        
        // set the application settings to null
        SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
        appset.TIMEREPORT_OPENDAYBUTTON__c = false;
        
        // new time report controller
        timereportController = new SCTimereportController ();
      
    }
    
   /**
     * @author: Sebastian Schrage <sschrage@gms-online.de>
     */
    static testMethod void timereportControllerPageCalls()
    {
        SCHelperTestClass.createOrderTestSet(true);
        
        // set test user from SCHelperTestClass --> nlburgt
        User testUser = SCHelperTestClass.users.get(6);
        SCResource__c resource = [select Id, Name from SCResource__c where Employee__c = :testUser.Id or Id = :testUser.Id];
        
        System.runAs (testUser)
        {       
            Test.startTest();
            
            timereportController = new SCTimereportController();
            
            System.assertEquals(timereportController.ChangeResource(),null);
            //System.assertEquals(timereportController.getAllowedToChooseTheResource(),false);
            System.assertEquals(timereportController.getDifferentRescource(),'false');
            System.assertEquals(timereportController.getRescourceId(), resource.Id);
            System.assertEquals(timereportController.getSelectedResource(), resource.Id);
            
            timereportController.getAllResourceList();
            
            Test.stopTest();
        }
    }   
}