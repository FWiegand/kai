/*
 * @(#)SCArticleImportController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 */

/**
 * Imports one ore multiple articles into the order lines 
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCArticleImportController
{
    public Boolean checked { get; private set; }
    public Boolean importOk { get; private set; }
    public String input { get; set;}
    //public String output { get; set;}
    public List<ArticleImport> output { get; private set; }
    public String articleList { get; private set; }
    private Set<Id> articleIdsOk = new Set<Id>();

    public final static String TAB_SEP = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';


    /**
     * Standard controller
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCArticleImportController()
    {
        System.debug('#### SCArticleImportController()');
        checked = false;
        importOk = false;

        input = '';

//        output = System.Label.SC_app_ArticleSection;
//        output += TAB_SEP + System.Label.SC_app_Status + TAB_SEP + System.Label.SC_app_Remark + '<br>';
//        output += '<br><br><br><br><br>';
    } // SCArticleImportController

    /**
     * Checks the input, if there are valid data.
     * @return    the page reference
     */
    public PageReference checkInput()
    {
        System.debug('#### checkInput()');

        Boolean noOk;
        String remark;
        List<String> articleNos = new List<String>();
        output = new List<ArticleImport>();
        articleIdsOk.clear();

        input = input.replaceAll('\r', '');
        input = input.replaceAll('\n', ',');
        List<String> items = input.split(',');

//        output = System.Label.SC_app_ArticleSection;
//        output += TAB_SEP + System.Label.SC_app_Status + TAB_SEP + System.Label.SC_app_Remark + '<br>';

        // iterate over all items and fill a list with the article numbers
        for (String item :items)
        {
            item = item.replace(',', '');
            System.debug('#### checkInput(): item -> ' + item);

            articleNos.add(item);
        } // for (String item :items)
        System.debug('#### checkInput(): Articles  -> [' + articleNos + ']');

        // This map combines article numbers with the article id
        Map<String, Id> articleMap = new Map<String, Id>();

        // fill the map, use as key hte article number and the EAN code if available
        for (SCArticle__c article: [Select Id, Name, EANCode__c, ArticleNameCalc__c
                                      from SCArticle__c
                                     where Name in :articleNos
                                        or EANCode__c in :articleNos
                                        or ArticleNameCalc__c in :articleNos])
        {
            articleMap.put(article.Name, article.Id);
            if (null != article.EANCode__c)
            {
                articleMap.put(article.EANCode__c, article.Id);
            } // if (null != article.EANCode__c)
            if (null != article.ArticleNameCalc__c)
            {
                articleMap.put(article.ArticleNameCalc__c, article.Id);
            } // if (null != article.ArticleNameCalc__c)
        } // for (SCArticle__c article: [Select Id, Name, EANCode__c, ArticleNameCalc__c ...

        // now check the items
        for (String artNo :articleNos)
        {
            noOk = true;
            remark = '';

            // look for the article number in the map
            if (articleMap.containsKey(artNo))
            {
                System.debug('#### checkProfileItems(): Article [' + artNo + '] valid');
                if (!articleIdsOk.contains(articleMap.get(artNo)))
                {
                    articleIdsOk.add(articleMap.get(artNo));
                } // if (!articleIdsOk.contains(articleMap.get(artNo)))
            } // if (articleMap.containsKey(artNo))
            else
            {
                // no valid article number
                System.debug('#### checkProfileItems(): Article [' + artNo + '] not valid');
                remark = System.Label.SC_msg_ArticleNotFound;
                noOk = false;
            } // else [if (articleMap.containsKey(artNo))]

            ArticleImport currentArticle = new ArticleImport();

            if (remark.length() > 0)
            {
                //output += '<span class="red">';
                currentArticle.error = true;
            }
            else
            {
                currentArticle.error = false;
            } // if (remark.length() > 0)

            currentArticle.article = artNo;
            currentArticle.status = (remark.length() == 0) ? System.Label.SC_btn_Ok : System.Label.SC_app_Error;
            currentArticle.remark = remark;
            /*
            output += artNo;
            output += TAB_SEP;
            output += (remark.length() == 0) ? System.Label.SC_btn_Ok : System.Label.SC_app_Error;
            output += TAB_SEP + remark + '<br>';

            if (remark.length() > 0)
            {
                output += '</span>';
            } // if (remark.length() > 0)
            */
            if (currentArticle.error == false)
            {
                checked = true;
            }

            output.add(currentArticle);
        } // for (String artNo :articleNos)

//        checked = (output.indexOf(System.Label.SC_btn_Ok) > 0);

        return null;
    } // checkInput

    /**
     * Generates a comma separated string with all article ids.
     * This will used for calling a javascript methode of the parent page.
     *
     * @return    the page reference
     */
    public PageReference importArticles()
    {
        System.debug('#### importArticles()');

        articleList = '';
        for (Id artId :articleIdsOk)
        {
            articleList += artId + ',';
        } // for (Id artId :articleIdsOk)
        articleList = articleList.substring(0, articleList.length() - 1);

        importOk = true;
        return null;
    } // importArticles


    /**
     * Wrapper class which contains infos for articles to be imported
     */
    public class ArticleImport
    {
        public String article { get; set; }
        public String status { get; set; }
        public String remark { get; set; }
        public Boolean error { get; set; }
    }
} // class SCArticleImportController