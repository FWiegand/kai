public class SCSearchClass {

    public String search {set;get;}
    public SCResource__c resource {set;get;}
    public Integer days {set;get;}
    public String radioSel {set;get;}
    public List<ApexClass> results {set;get;}
    public List<ApexTrigger> resultsT {set;get;}
    public List<ApexPage> resultsP {set;get;}
    public List<ApexComponent> resultsComp {set;get;}
    public List<User> resultsU {set;get;}
    public List<ObjectInfo> resultsObj {set;get;}
    
    
    public SCSearchClass()
    {
        system.debug('Hallo Welt');
        system.debug('Search gleich ' + search);
        system.debug('Parameter gleich ' + System.currentPagereference().getParameters().get('search'));
        radioSel = '0';
        days = 1;
        resource = new SCResource__c(Employee__c = UserInfo.getUserId());
        if (search==null) search = System.currentPagereference().getParameters().get('search');
        if (search!=null) doSearch();
    }
    
    public void doSearch()
    {
        if (radioSel.equals('0'))
        {
            if (search == null || search=='')
            {
                results=null;
                resultsT=null;
            }
            else
            {
                search = search.trim();
                string rsearch = '%' + search + '%';
                results = [Select Id,Name,LastModifiedBy.Name,LastModifiedDate from ApexClass where Name like :rsearch limit 20];
                resultsT = [Select Id,Name,LastModifiedBy.Name,LastModifiedDate,status from ApexTrigger where Name like :rsearch limit 20];
                resultsP = [Select Id,Name,LastModifiedBy.Name,LastModifiedDate from ApexPage where Name like :rsearch limit 20];
                resultsComp = [Select Id,Name,LastModifiedBy.Name,LastModifiedDate from ApexComponent where Name like :rsearch limit 20];
                resultsU = [Select Id,Name,LastModifiedBy.Name,LastModifiedDate,Profile.Name,UserRole.Name from User where Name like :rsearch limit 20];
                resultsObj = selectObject(search);
            }
        }
        else
        {
            if (null == resource.Employee__c)
            {
                resource.Employee__c = UserInfo.getUserId();
            }
            if (null == days)
            {
                days = 1;
            }

            Date toDate = Date.today().addDays(1);
            Date fromDate = Date.today().addDays(-(days-1));
            
            results = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                         from ApexClass 
                        where LastModifiedBy.Id = :resource.Employee__c 
                          and LastModifiedDate <= :toDate 
                          and LastModifiedDate >= :fromDate 
                        limit 20];
            resultsT = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate,status 
                          from ApexTrigger 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         limit 20];
            resultsP = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate
                          from ApexPage 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         limit 20];
            resultsComp = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                             from ApexComponent 
                            where LastModifiedBy.Id = :resource.Employee__c 
                              and LastModifiedDate <= :toDate 
                              and LastModifiedDate >= :fromDate 
                            limit 20];
            resultsU = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate, Profile.Name, UserRole.Name 
                          from User 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         limit 20];
            resultsObj = new List<ObjectInfo>(); // GMSAW: ??? selectObject(search);
        }
    }
    
    
    public string getorgid()
    {
        return UserInfo.getOrganizationId();
    }
    

 
    public List<ObjectInfo> selectObject(String name)
    {
        List<ObjectInfo> res = new List<ObjectInfo>(); 
        if(name != null)
        {
            // loop through the custom objects
            List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
            for(Schema.SObjectType f : gd)
            {
                // describe the objects
                Schema.DescribeSObjectResult r = f.getDescribe();
                String objName = r.getName();
                // search for the name (exclude tags and history objects)
                if(objName.toUpperCase().contains(name.toUpperCase()) &&
                   !objName.endswith('Tag') && 
                   !objName.endswith('History'))
                {
                    ObjectInfo o = new ObjectInfo();
                    o.name = objName; // + '##' + r.getKeyPrefix();
                    if(r.isCustom())
                    {
                        String prefix = r.getKeyPrefix();
                        // opens the tab with the object content
                        o.url = '' + prefix + '/o';        
                        
                        //TODO: determine the "01I20000000Ap5O" id to jump directly to the custom object definition                        
                        //https://cs4.salesforce.com/01I20000000Ap5O?setupid=CustomObjects
                    
                    }
                    else
                    {    
                        // simple access to the object
                        o.url = 'ui/setup/Setup?setupid=' + o.name;        
                    }

                    res.add(o);
                }
            }
        }   
        return res;
    }
    
    public class ObjectInfo
    {
        public String name {get; set;}
        public String url {get; set;}
    }
}