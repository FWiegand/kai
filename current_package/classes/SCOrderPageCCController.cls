/*
 * @(#)SCOrderPageCCController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * Class Hierarchy
 * ===============
 *
 *                                      this class
 *                                          |
 * SCOrderPageCCController        <---------+
 *          |
 * SCOrderPageController
 *          |
 * SCfwOrderControllerBase
 *          |
 * SCfwComponentControllerBase
 *
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCOrderPageCCController extends SCOrderPageController
{
    public Boolean accountHasInstalledBase { get; set; }
    
    /**
     * controlOrderProperties
     * returns a value unequal to null, if all required-fields are non-zero.
     */
    public Boolean getControlOrderProperties()
    {
        //Integer i = 1/0;
        //return Page.SCOrderCreation;//curOrderItem
        if (curOrderItem != null)
        {
            System.debug(LoggingLevel.Error, '###+++###curOrderItem: ' + curOrderItem.orderItem);
            
            try
            {
                if ((curOrderItem.orderItem.ErrorSymptom1__c != null) && (curOrderItem.orderItem.ErrorSymptom2__c != null))
                {
                    return true;
                }
            }
            catch (Exception e) { }
        }
        else
        {
            System.debug(LoggingLevel.Error, '###+++###curOrderItem: ' + curOrderItem);
        }
        return false;
    }
    
    public Integer counter { get{if(counter == null){counter=0;}return counter;} private set; }
    
    public PageReference refreshGMSData()
    {
        counter++;
        System.debug('+++###+++Test');
        try { System.debug('+++###+++ currentUser.ERPWorkCenterPlant__c: "' + currentUser.ERPWorkCenterPlant__c + '"'); } catch (Exception e) { }
        try { System.debug('+++###+++ order.order.DepartmentCurrent__r.Name: "' + order.order.DepartmentCurrent__r.Name + '"'); } catch (Exception e) { }
        
        return null;
    }
    
    
    
    String AccountPlant = null;
    
    public Boolean mustSelectSalesOrg { get; set;}
    public List<AccInfoObj> accInfos { get; private set;}
    
    /**
     * getAllowedToPlan - TRUE: The current user is allowed to dispatch this order
     */
    public User currentUser
    {
        get
        {
            if (currentUser == null)
            {
                currentUser = [SELECT Id, ERPWorkCenterPlant__c FROM User WHERE Id = :UserInfo.getUserId()].get(0);
            }
            return currentUser;
        }
        private set;
    }
    
    /*public Boolean getAllowedToPlan()
    {
        return true;
    
        String departmentUser = currentUser.DefaultBusinessUnit__c;
        String departmentOrder = order.order.DepartmentCurrent__c;
        
        if(departmentUser != null && departmentOrder != null &&
           departmentUser != departmentOrder)
        {
            return false;
        }

        //if (currentUser.DefaultBusinessUnit__c == AccountPlant)
        //    || (currentUser.DefaultBusinessUnit__c == null && appSettings.DEFAULT_PLANT__c == AccountPlant)  )
        return true;
    }*/
    
    
    /**
     * Wrapper class for displaying an account info in the selection list.
     */
    public class AccInfoObj
    {
        public Boolean isChecked { get; set;}
        public SCAccountInfo__c accInfo { get; private set;}

        /**
         * Default constructor
         */
        public AccInfoObj()
        {
            isChecked = false;
            accInfo = null;
        }
    } // class AccInfoObj


    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCOrderPageCCController()
    {
        initEx();
    } // SCOrderPageCCController
    
    /**
     * Constructor
     *
     * @param    accountId    id of an account for the LE
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    public SCOrderPageCCController(Id accountId)
    {
        initEx();
    }

    /**
     * Constructor
     *
     * @param    accountId    id of an account for the LE
     * @param    orderId      SFDC ID of an already existing order
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCOrderPageCCController(Id accountId, Id orderId)
    {
        initEx();
    }

    /**
     * Initialize all necessary data for order creation.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    private void initEx()
    {
        if (ApexPages.currentPage().getParameters().containsKey('ibid'))
        {
            newInstalledBase = ApexPages.currentPage().getParameters().get('ibid');
            addNewOrderItem();
        }
    
        accountHasInstalledBase = getHasInstalledBase();
        handleSalesOrganisation();
    }
    
    private void handleSalesOrganisation()
    {
        mustSelectSalesOrg = false;
        
        SCOrderRole__c roleSR = order.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
        
        accInfos = new List<AccInfoObj>();
        for (SCAccountInfo__c accInfo :[ Select Id, Name, CompanyCode__c, SalesArea__c, Division__c, DistributionChannel__c, SalesOffice__c, SalesGroup__c, ServiceArea__c 
                                         From SCAccountInfo__c 
                                         Where Account__c = :roleSR.Account__c ])
        {
            AccInfoObj accInfoObj = new AccInfoObj();
            accInfoObj.accInfo = accInfo;
            accInfos.add(accInfoObj);
        }
        
        if (accInfos.size() > 0)
        {
            if (accInfos.size() == 1)
            {
                order.order.CompanyCode__c = accInfos[0].accInfo.CompanyCode__c;
                order.order.SalesArea__c = accInfos[0].accInfo.SalesArea__c;
                order.order.Division__c = accInfos[0].accInfo.Division__c;
                order.order.DistributionChannel__c = accInfos[0].accInfo.DistributionChannel__c;
                // CCE required to show the distribution channel on mobile system
                order.order.Info__c = order.order.DistributionChannel__c;    
                order.order.SalesOffice__c = accInfos[0].accInfo.SalesOffice__c;
                order.order.SalesGroup__c = accInfos[0].accInfo.SalesGroup__c;
                order.order.ServiceArea__c = accInfos[0].accInfo.ServiceArea__c;
                AccountPlant = accInfos[0].accInfo.SalesOffice__c;
            }
            else
            {
                mustSelectSalesOrg = true;
            }
        }
    } // handleSalesOrganisation

    /**
     * Sets the selected sales organisation in the order.
     */
    public PageReference setSalesOrganisation()
    {
        if ((null != accInfos) && (accInfos.size() > 0))
        {
            for (AccInfoObj curAccInfo :accInfos)
            {
                if (curAccInfo.isChecked)
                {
                    order.order.CompanyCode__c = curAccInfo.accInfo.CompanyCode__c;
                    order.order.SalesArea__c = curAccInfo.accInfo.SalesArea__c;
                    order.order.Division__c = curAccInfo.accInfo.Division__c;
                    order.order.DistributionChannel__c = curAccInfo.accInfo.DistributionChannel__c;
                    // CCE required to show the distribution channel on mobile system
                    order.order.Info__c = order.order.DistributionChannel__c;    

                    order.order.SalesOffice__c = curAccInfo.accInfo.SalesOffice__c;
                    order.order.SalesGroup__c = curAccInfo.accInfo.SalesGroup__c;
                    order.order.ServiceArea__c = curAccInfo.accInfo.ServiceArea__c;
                    AccountPlant = curAccInfo.accInfo.SalesOffice__c;
                    break;
                } // if (curAccInfo.isChecked)
            } // for (AccInfoObj curAccInfo :accInfos)
        } // if ((null != accInfos) && (accInfos.size() > 0))
        
        mustSelectSalesOrg = false;
        return null;
    } // setSalesOrganisation

    /**
     * Adds a new role to the order.
     *
     * @return    the page reference
     */
    public override PageReference processCustomer()
    {
        PageReference page = processCustomer(order, processCustomerParamAid, processCustomerParamType);
        
        if (SCfwConstants.DOMVAL_ORDERROLE_LE == processCustomerParamType)
        {
            handleSalesOrganisation();
        }    
        return page;
    } // processCustomer

    /**
     * Deletes the selected role and fills it with the role from 
     * the account roles, if available. If not it will be filled
     * with the LE.
     *
     * @return    the page reference
     */
    public override PageReference delRole()
    {
        PageReference page = delRole(order);
        
        if (SCfwConstants.DOMVAL_ORDERROLE_LE == processCustomerParamType)
        {
            handleSalesOrganisation();
        }    
        return page;
    } // delRole

    /**
     * Reinit order roles with an account.
     *
     * @return    the page reference
     */
    public override PageReference changeRole()
    {
        PageReference page = changeRole(order, processCustomerParamAid);
        
        if (SCfwConstants.DOMVAL_ORDERROLE_LE == processCustomerParamType)
        {
            handleSalesOrganisation();
        }    
        return page;
    } // changeRole

    /**
     * Saves the new created order.
     *
     * @return    true if successful
     */
    public override Boolean save()
    {
        order.resetRole(SCfwConstants.DOMVAL_ORDERROLE_AG);
        
        SCOrderRole__c orderRole = order.getRole(SCfwConstants.DOMVAL_ORDERROLE_AG);
        if (null != orderRole)
        {
            if (SCBase.isSet(order.order.cce_callername__c))
            {
                orderRole.Name1__c = order.order.cce_callername__c;
            }
            if (SCBase.isSet(order.order.cce_callerphone__c))
            {
                orderRole.Phone__c = order.order.cce_callerphone__c;
            }
            if (SCBase.isSet(order.order.cce_calleremail__c))
            {
                orderRole.Email__c = order.order.cce_calleremail__c;
            }
        } // if (null != orderRole)
        
        return super.save();
    } // save
    
    /**
     * Set calculated InvoicingType to order
     *
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    public override void doAfterOrderItemChange() 
    {
    	calculateInvoicingType(order);
    }
    
    /**
     * Force to recalculate the invoicing type for the order
     *
     *  @author Eugen Tiessen <etiessen@gms-online.de>
     */
    public void onRecalculateInvoicingType()
    {
        calculateInvoicingType(order);
        
        removeEQthatWasAddedInZC04mode();
    }
   
    /**
     * Calculate order.InvoicingType__c depending on order.type and order.pricelist
     *
     * @author Eugen Tiessen <etiessen@gms-online.de>
      
     protected void calculateInvoicingType()
     {
        List<CCBillingIndicator__c> billingIndicator = 
        [
            SELECT 
                InvoicingType__c
            FROM
                CCBillingIndicator__c
            WHERE
                OrderType__c =: order.order.Type__c
                AND
                PriceList__c =: order.order.PriceList__c
                
        ];
        
        if(billingIndicator.size() > 0)
        {
            system.debug('#### found billing indicator: ' + billingIndicator);
            order.order.InvoicingType__c = billingIndicator.get(0).InvoicingType__c;
        }
        else
        {
            system.debug('#### billing indicator not found. Using default invoicing type. Order Type=' + order.order.Type__c + '; Pricelist=' + order.order.PriceList__c);
            order.order.InvoicingType__c = appSettings.DEFAULT_INVOICINGTYPE__c;
        }
        
         
     }*/
     
    // This function removes IBL reference from the last selected equipment with all sub equipments if
    // this equipment wasn't assigned to the order
    public String lastEqId { get; set; }
    public PageReference removeLastEQ()
    {
        if(String.isNotBlank(lastEqId))
        {
            SCInstalledBaseSearchExtension ext = new SCInstalledBaseSearchExtension();
            ext.removeLastAddedEquipment(lastEqId);
        }
        return null;
    }
    
    // This method fires only if user changed order type from ZC04 to other type
    public void removeEQthatWasAddedInZC04mode()
    {
        
        if(!order.boOrderItems.isEmpty() && eqWasAddedInZC04Mode)
        {
            for (Integer i=order.boOrderItems.size(); i>0; i--)
            {
                SCboOrderItem item = order.boOrderItems[i - 1];
                
                // Installed base must be updated (removing IBL reference) if
                // this items was added in ZC04 mode. We need only HQ because it schould exist and only once
                if(item.orderItem.installedbase__r.cce_EquipmentTypeFilter__c == 'EQ')
                {
                    SCInstalledBaseSearchExtension ext = new SCInstalledBaseSearchExtension();
                    ext.removeLastAddedEquipment(String.valueOf(item.orderItem.installedbase__r.id));
                }
            }
            
            deleteAllItems();
            mustOpenInstalledBase = true;
        }
        
        // If user changed order type to ZC04 - we must remove all order items 
        // because the user must select a new appliance
        if(order.order.type__c == '5705' || order.order.type__c == SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT)
        {
            deleteAllItems();
            mustOpenInstalledBase = true;
        }
        
    }
    
    // Checks whether selected account has any installed bases
    public Boolean getHasInstalledBase() 
    {
        if (ApexPages.currentPage().getParameters().containsKey('aid'))
        {
            // Reading all inst. base roles for this account
            List<aggregateResult> roles = [ Select InstalledBaseLocation__c, InstalledBaseLocation__r.Id locId, InstalledBaseLocation__r.LocName__c locName
                                            From SCInstalledBaseRole__c 
                                            Where Account__c = : ApexPages.currentPage().getParameters().get('aid') 
                                            Group By InstalledBaseLocation__c, InstalledBaseLocation__r.Id, InstalledBaseLocation__r.LocName__c, Account__c 
                                            Limit 50 ];
            
            if(!roles.isEmpty())
            {
                // Preparing the list
                List<SCInstalledBaseLocation__c> locations = new List<SCInstalledBaseLocation__c>();
                       
                for (AggregateResult ar : roles)
                {      
                    String lId   = String.valueOf(ar.get('locId'));
                    String lName = String.valueOf(ar.get('locName'));
                    String lNum  = [ Select Name From SCINstalledBaseLocation__c Where Id = :lId ].Name;
                
                    SCInstalledBaseLocation__c tmpLoc = new SCInstalledBaseLocation__c( Id = lId, Description__c = lNum, LocName__c = lName );
                    locations.add(tmpLoc);
                }
                
                // Reading all bases                                           
                List<SCInstalledBase__c> instBases = [ Select   Id, Name
                                                       From SCInstalledBase__c 
                                                       Where InstalledBaseLocation__c IN : locations
                                                       AND Parent__c = null ];
                                                       
                if(instBases.size() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}