/*
 * @(#)SCfwCollectionUtilsTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This test class does all the tests that are needed for the
 * collection utils class.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwCollectionUtilsTest
{
    /**
     * Test the join of a set delimited by a comma.
     * The resulting string has to be the proper length and must contain
     * all elements and the delimiter.
     */
    private static testMethod void testJoinSetComma()
    {
        // REMARK: A set has no order at all and we therefore 
        // are not able to predict the order of the elements in the
        // result of the join() operation.
        
        Set<String> testSet = new Set<String>{'a', 'b', 'c'};
        
        String result1 = SCfwCollectionUtils.join(testSet, ',');
        
        System.assertEquals(5, result1.length());
        System.assert(result1.contains(','));
        System.assert(result1.contains('a'));
        System.assert(result1.contains('b'));
        System.assert(result1.contains('c'));
    }

    /**
     * Test the join of a set delimited by a comma.
     * The resulting string has to be the proper length and must contain
     * all elements and the delimiter.
     */
    private static testMethod void testJoinSetCommaEnclosed()
    {
        // REMARK: A set has no order at all and we therefore 
        // are not able to predict the order of the elements in the
        // result of the join() operation.
        
        Set<String> testSet = new Set<String>{'a', 'b', 'c'};
        
        String result1 = SCfwCollectionUtils.join(testSet, ',', '\'');
        
        System.assertEquals(11, result1.length());
        System.assert(result1.contains(','));
        System.assert(result1.contains('a'));
        System.assert(result1.contains('b'));
        System.assert(result1.contains('c'));
    }
   
    /**
     * Test the join of a set delimited by a space.
     * The resulting string has to be the proper length and must contain
     * all elements and the delimiter
     */    
    private static testMethod void testJoinSetSpace()
    {
        // REMARK: A set has no order at all and we therefore 
        // are not able to predict the order of the elements in the
        // result of the join() operation.
        
        Set<String> testSet = new Set<String>{'a', 'b', 'c'};
        
        String result1 = SCfwCollectionUtils.join(testSet, ' ');
        
        System.assertEquals(5, result1.length());
        System.assert(result1.contains(' '));
        System.assert(result1.contains('a'));
        System.assert(result1.contains('b'));
        System.assert(result1.contains('c'));
    }
    
    /**
     * Test the join of an (ordered) list of elements with a given delimter.
     * The resulting string has to contain an ordered, delimited version of 
     * the list.
     */   
    private static testMethod void testJoinListComma()
    {
        List<String> testSet = new List<String>{'a', 'b', 'c'};
        
        String result1 = SCfwCollectionUtils.join(testSet, ',');
        
        System.assertEquals('a,b,c', result1);
    }
    
    /**
     * Test the join of an empty list of elements with a given delimter.
     * The resulting string should be empty as well while no errors or
     * exceptions are thrown.
     */   
    private static testMethod void testJoinListEmpty()
    {
        List<String> testSet = new List<String>{};
        
        String result1 = SCfwCollectionUtils.join(testSet, ',');
        
        System.assertEquals('', result1);
    }
    
    /**
     * Test the join of an empty set of elements with a given delimter.
     * The resulting string should be empty as well while no errors or
     * exceptions are thrown.
     */   
    private static testMethod void testJoinSetEmpty()
    {
        List<String> testSet = new List<String>{};
        
        String result1 = SCfwCollectionUtils.join(testSet, ',');
        
        System.assertEquals('', result1);
    }
    
    /**
     * There is a list with two elements. First test checks whether the first element is there.
     * Second test tests whether the item different from every item in the list is in the list.
     */
    public static testMethod void testIsContained()
    {
        List<String> l = new List<String>();
        l.add(' 1 ');
        l.add(' 2    ');
        Boolean contains1 = SCfwCollectionUtils.isContained(' 1 ', l);
        System.assertEquals(true, contains1);
        
        Boolean contains3 = SCfwCollectionUtils.isContained(' 3 ', l);
        System.assertEquals(false, contains3);
    }
    
    public static testMethod void testIsContainedList()
    {
        String destItems = '1,2,3,4,5,6';
        String src1Items = '7, 8,9';
        String src2Items = '7, 8, 2 , 9';
        
        List<String> destList = destItems.split(',');
        List<String> src1List = src1Items.split(',');
        List<String> src2List = src2Items.split(',');
        
        Boolean contains1 = SCfwCollectionUtils.isContained(src1List, destList);
        System.assertEquals(false, contains1);
        
        Boolean contains2 = SCfwCollectionUtils.isContained(src2List, destList);
        System.assertEquals(true, contains2);
    }
}