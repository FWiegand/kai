/*
 * @(#)SCInvalidDomDependsControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for the SCInvalidDomDependsController.
 * <p>
 * Actually all needed data for comparison should be created by the test itself,
 * because real life (TM) data could change.
 * <p>
 * Remember that code of test classes doesn't count to Apex code limit.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCInvalidDomDependsControllerTest
{
    /**
     * There are domains that don't have a tarnslation by design. One of them
     * is DOM_SYSPARAM, but their might follow others later on.
     */
    static testMethod void testNonTranslatablePositive()
    {
        SCHelperTestClass.createDomainTransData();
        
        SCInvalidDomDependsController controller = new SCInvalidDomDependsController();
        System.assertEquals(1, controller.invalidDeps.size());
    }
} // SCInvalidDomDependsControllerTest