/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class trgCase_Beschwerde_Test {

    static testMethod void myUnitTest() {
        ComplaintFields__c cfields = new ComplaintFields__c(
        	Name='Test',ComplaintFieldName__c = 'Beschwerdepunkte_Verkauf__c',Additional_Info__c = 'Verkauf'
        );
        insert cfields;
        
        ComplaintFields__c cfields2 = new ComplaintFields__c(
        	Name='Test2',ComplaintFieldName__c = 'Beschwerdepunkte_Marketing__c',Additional_Info__c = 'Marketing'
        );
        insert cfields2;
        
        Case testCase = new Case(
        	Stammnummer__c = '12345',
        	Beschwerdepunkte_Verkauf__c = 'Merchandising'
        );
        RecordType recordType = [SELECT Id FROM RecordType WHERE Name =: 'Beschwerde'];
        
        testCase.RecordTypeId = recordType.Id;
        
        insert testCase;
        
        List<Beschwerde__c> listBeschwerde = [SELECT Id,Name,Additional_Info__c FROM Beschwerde__c WHERE Kundenvorgang__c =: testCase.Id];
        
        //check if trigger set correct 
        System.assertEquals('Verkauf',listBeschwerde[0].Additional_Info__c);
        //check if correct number of "Beschwerde__c" childobjects were created
        System.assertEquals(1,listBeschwerde.size());
        
        testCase.Beschwerdepunkte_Verkauf__c = '';
        testCase.Beschwerdepunkte_Marketing__c = 'Erreichbarkeit';
        
        update testCase;
        
        listBeschwerde = [SELECT Id,Name,Additional_Info__c FROM Beschwerde__c WHERE Kundenvorgang__c =: testCase.Id];
        
        System.assertEquals('Marketing',listBeschwerde[0].Additional_Info__c);
        //check if old Beschwerde__c objects were deleted successfully before new one being inserted
        System.assertEquals(1,listBeschwerde.size());
        
        
    }
}