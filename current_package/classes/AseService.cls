//Generated by wsdl2apex
// @version $Revision$, $Date$
public class AseService {
	    public class getAllResponse {
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class setExFault_element {
        public String type_x;
        public String errorCode;
        public String errorString;
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] errorString_type_info = new String[]{'errorString','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','errorCode','errorString'};
    }
    public class removeAllResponse_element {
        public String response;
        private String[] response_type_info = new String[]{'response','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class aseFaultType {
        public String type_x;
        public String errorCode;
        public String errorString;
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] errorString_type_info = new String[]{'errorString','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','errorCode','errorString'};
    }
    public class setResponse_element {
        public String response;
        private String[] response_type_info = new String[]{'response','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class setEx_element {
        public String tenant;
        public AseService.aseDataType[] aseData;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] aseData_type_info = new String[]{'aseData','http://www.gms.com/ase/','aseDataType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','aseData'};
    }
    public class aseKeyValueType {
        public String key;
        public String value;
        private String[] key_type_info = new String[]{'key','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] value_type_info = new String[]{'value','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'key','value'};
    }
    public class aseParaSelectType {
        public String type_x;
        public String[] keys;
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] keys_type_info = new String[]{'keys','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','keys'};
    }
    public class setExResponse_element {
        public AseService.aseDataType[] response;
        private String[] response_type_info = new String[]{'response','http://www.gms.com/ase/','aseDataType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class aseDataType {
        public String type_x;
        public AseService.aseDataEntry[] dataEntries;
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] dataEntries_type_info = new String[]{'dataEntries','http://www.gms.com/ase/','aseDataEntry','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','dataEntries'};
    }
    public class aseParaType {
        public String tenant;
        public AseService.aseParaSelectType[] paraSelect;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] paraSelect_type_info = new String[]{'paraSelect','http://www.gms.com/ase/','aseParaSelectType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','paraSelect'};
    }
    public class aseDataEntry {
        public AseService.aseKeyValueType[] keyValues;
        private String[] keyValues_type_info = new String[]{'keyValues','http://www.gms.com/ase/','aseKeyValueType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'keyValues'};
    }
    public class getAllResponseType {
        public AseService.aseDataType[] response;
        private String[] response_type_info = new String[]{'response','http://www.gms.com/ase/','aseDataType','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class aseSOAP {
        public String endpoint_x = 'http://www.gms.com';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.gms.com/ase/', 'AseService'};
        public AseService.aseDataType[] setEx(String tenant,AseService.aseDataType[] aseData) {
            AseService.setEx_element request_x = new AseService.setEx_element();
            AseService.setExResponse_element response_x;
            request_x.tenant = tenant;
            request_x.aseData = aseData;
            Map<String, AseService.setExResponse_element> response_map_x = new Map<String, AseService.setExResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/setEx',
              'http://www.gms.com/ase/',
              'setEx',
              'http://www.gms.com/ase/',
              'setExResponse',
              'AseService.setExResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
        public String removeAll(String tenant,String[] type_x) {
            AseService.aseAllType request_x = new AseService.aseAllType();
            AseService.removeAllResponse_element response_x;
            request_x.tenant = tenant;
            request_x.type_x = type_x;
            Map<String, AseService.removeAllResponse_element> response_map_x = new Map<String, AseService.removeAllResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/removeAll',
              'http://www.gms.com/ase/',
              'removeAll',
              'http://www.gms.com/ase/',
              'removeAllResponse',
              'AseService.removeAllResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
        public AseService.aseDataType[] getAll(String tenant,String[] type_x) {
            AseService.aseAllType request_x = new AseService.aseAllType();
            AseService.getAllResponseType response_x;
            request_x.tenant = tenant;
            request_x.type_x = type_x;
            Map<String, AseService.getAllResponseType> response_map_x = new Map<String, AseService.getAllResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/getAll',
              'http://www.gms.com/ase/',
              'getAll',
              'http://www.gms.com/ase/',
              'getAllResponse',
              'AseService.getAllResponseType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
        public String set(String tenant,AseService.aseDataType[] aseData) {
            AseService.aseDataComplexType request_x = new AseService.aseDataComplexType();
            AseService.setResponse_element response_x;
            request_x.tenant = tenant;
            request_x.aseData = aseData;
            Map<String, AseService.setResponse_element> response_map_x = new Map<String, AseService.setResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/set',
              'http://www.gms.com/ase/',
              'set',
              'http://www.gms.com/ase/',
              'setResponse',
              'AseService.setResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
        public String remove(String tenant,AseService.aseParaSelectType[] paraSelect) {
            AseService.aseParaType request_x = new AseService.aseParaType();
            AseService.removeResponse_element response_x;
            request_x.tenant = tenant;
            request_x.paraSelect = paraSelect;
            Map<String, AseService.removeResponse_element> response_map_x = new Map<String, AseService.removeResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/remove',
              'http://www.gms.com/ase/',
              'remove',
              'http://www.gms.com/ase/',
              'removeResponse',
              'AseService.removeResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
        public AseService.aseDataType[] get(String tenant,AseService.aseParaSelectType[] paraSelect) {
            AseService.aseParaType request_x = new AseService.aseParaType();
            AseService.getAllResponseType response_x;
            request_x.tenant = tenant;
            request_x.paraSelect = paraSelect;
            Map<String, AseService.getAllResponseType> response_map_x = new Map<String, AseService.getAllResponseType>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.gms.com/ase/get',
              'http://www.gms.com/ase/',
              'get',
              'http://www.gms.com/ase/',
              'getResponse',
              'AseService.getAllResponseType'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.response;
        }
    }
    public class removeResponse_element {
        public String response;
        private String[] response_type_info = new String[]{'response','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'response'};
    }
    public class aseAllType {
        public String tenant;
        public String[] type_x;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','type_x'};
    }
    public class aseDataComplexType {
        public String tenant;
        public AseService.aseDataType[] aseData;
        private String[] tenant_type_info = new String[]{'tenant','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] aseData_type_info = new String[]{'aseData','http://www.gms.com/ase/','aseDataType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.gms.com/ase/','false','false'};
        private String[] field_order_type_info = new String[]{'tenant','aseData'};
    }
}