/*
 * @(#)SCOrderComponentController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * Class Hierarchy
 * ===============
 *
 *                                      this class
 *                                          |
 * SCItemsTabComponentController            |
 * SCOrderTabComponentController            |
 * SCPartsServicesTabComponentController    |
 *          |                               |
 * SCOrderComponentController     <---------+
 *          |
 * SCfwOrderControllerBase
 *          |
 * SCfwComponentControllerBase
 *
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCOrderComponentController extends SCfwOrderControllerBase
{
    /**
     * Keep a copy of the BO for orders here for easier reference
     */
    public SCboOrder boOrder 
    { 
        get
        {
            if (this.boOrder == null)
            {
                this.boOrder = ((SCProcessOrderController)pageController).boOrder;
            }
            
            return boOrder;
        } 
        private set; 
    } // boOrder

    /**
     * Keep a copy of the id of the appointment here for easier reference
     */
    public String aid 
    { 
        get
        {
            if (aid == null)
            {
                aid = ((SCProcessOrderController)pageController).appointmentId;
            }
            
            return aid;
        }
        private set; 
    } // aid
    
    /**
     * Get the currently selected resource Id
     * The resource ID of the currently selected engineer, which
     * is needed for special article searches.
     *
     * @return resource Id
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public String getCurrentResourceId()
    {
        SCProcessOrderController.AssignmentObj obj = getCurAssignment();
        if(obj != null)
        {
            return obj.resId;
        }
        return null;
    }
    
    /**
     * Get the currently selected resource Id
     * The resource ID of the currently selected engineer, which
     * is needed for special article searches.
     *
     * @return resource Id
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCProcessOrderController.AssignmentObj getCurAssignment()
    {
        return ((SCProcessOrderController)pageController).curAssignment;
    }

    /**
     * Keep a list of ids for all in the page deleted order items. We need this for deleting them
     * in the db when the order will be saved.
     */
    public List<Id> deletedOrderItemIds
    { 
        get
        {
            if (deletedOrderItemIds == null)
            {
                deletedOrderItemIds = ((SCProcessOrderController)pageController).deletedOrderItemIds;
            }
            
            return deletedOrderItemIds;
        }
        private set; 
    } // deletedOrderItemIds

    /**
     * Keep a copy of the BO for the order item we currently work on.
     */
    public SCboOrderItem currentOrderItem 
    { 
        get
        {
            curOrderItem = ((SCProcessOrderController)pageController).currentOrderItem;
            System.debug('#### SCOrderComponentController.getCurrentOrderItem(): ID2 -> ' + curOrderItem.orderItem.ID2__c);
            return curOrderItem;
        }
        set
        {
            System.debug('#### SCOrderComponentController.setCurrentOrderItem(): ID2 -> ' + value.orderItem.ID2__c);
            ((SCProcessOrderController)pageController).currentOrderItem = value;
            selectedItem = value.orderItem.ID2__c;
            selectItem(boOrder);
            // curOrderItem = value;
        } 
    } // currentOrderItem
} // class SCOrderComponentController