/*
 * @(#)SCbtcContractVisitCreateTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractPostalcodeTest
{
    static testMethod void testContractPostalcodePositive()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        
        SCbtcContractPostalcode.asyncPostalcodeAll('NL', 1, 'trace');
        Id id = (Id) contractID;
        Id apexJobId = SCbtcContractPostalcode.asyncPostalcodeContract(Id, 'trace');
        List<SCContract__c> cList = [Select Name from SCContract__c where Id = :id];
        if(cList.size() > 0)
        {
            String contractName = cList[0].Name;
            apexJobId = SCbtcContractPostalcode.asyncPostalcodeContract(contractName, 'trace');
        }
        List<String> idList = new List<String>();
        for(SCContract__c c: cList)
        {
            idList.add(c.Id);
        }
        apexJobId = SCbtcContractPostalcode.asyncPostalcodeContracts(idList);

        SCbtcContractPostalcode.syncPostalcodeContracts(idList);

        SCbtcContractPostalcode.syncPostalcodeContract(contractID);

 

        Test.StopTest();
    }

    private static void debug(String msg)
    {
        System.debug('###Test..........' + msg);
    }
}