/*
 * @(#)SCbtcStockReplenishmentTestHelper.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Please do not change the functions, because they are used in SCbtcStockOptimizationTest asserts
 */
 
 public class SCbtcStockReplenishmentTestHelper
 {
    
    public static void createReplenishmentRuleSetItems_A(Id ruleSetId)
    {
//        Set by the function prepareTestData_A()
//        Decimal maxPartValue = 1000;
//        Decimal maxPartVolume = 50;
//        Decimal maxPartWeight = 30;
//        Decimal maxStockValue = 30000;
//        Decimal maxStockVolume = 500;
//        Decimal maxStockWeight = 560;
//        String optimizationInterval = '365';
        // R0 on the black list
        // R1 BLP > 1000 (maxPartValue) 
        // R2 Weight > 30 (maxPartWeight)
        // R3 Volume > 50 (maxPartVolume)
        // R7 on the white list
        // The rules R4 - R5 are set by the rules from S4 -S9
    }

    public static void createAllItems_A(ID plantId, ID stockId, ID priceListId)
    {
        // Created by the caller of the function prepareTestData_A()
        // the stock's: max weight 560 kg , max value = 30000

        // MATERIAL_REORDER_LOCKTYPE    50201, 50204, 50206
        
        // article                                                       Stock
  // pseudorule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,       lockType,minQty, qty,movementQty
        cI('R0', 'SB001','201','KGM',     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          2,        plantId, stockId, priceListId); // replenishment 2
        cI('S4', 'S0001','201','KGM',     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          1,        plantId, stockId, priceListId); // replenishment 3
        cI('S5', 'S0002','201','KGM',     1,  1,   null,      10,        1,     true,       '50203',     5,   1,          3,        plantId, stockId, priceListId); // replenishment 0

        // supersession wiht replenishment = 6
        cI('E5', 'S0008','201', null,     1,  1,   null,      10,        1,     true,       '50201',     5,   1,          1,        plantId, stockId, priceListId); // replenishment 3 + 2 + 1 = 6
        cI('E6', 'S0009','201', null,     1,  1,'S0008',      10,        1,     true,       '50201',     5,   1,          2,        plantId, stockId, priceListId); // replenishment 0
        cI('E7', 'S0010','201', null,     1,  1,'S0009',      10,        1,     true,       '50201',     5,   1,          3,        plantId, stockId, priceListId); // replenishment 0

        // supersession with replenishment = 0
        cI('F5', 'S0004','201', null,     1,  1,   null,      10,        1,     true,       '50203',     5,   1,          1,        plantId, stockId, priceListId); // replenishment 3 + 2 + 1 = 6 aber weil locked replenishment = 0
        cI('F6', 'S0005','201', null,     1,  1,'S0004',      10,        1,     true,       '50205',     5,   1,          2,        plantId, stockId, priceListId); // replenishment 0
        cI('F7', 'S0006','201', null,     1,  1,'S0005',      10,        1,     true,       '50205',     5,   1,          3,        plantId, stockId, priceListId); // replenishment 0


        }
    /**
     * returns the stock id
     */
    public static ID prepareTestData_A()
    {
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks.get(2).Id, 
                                               SCHelperTestClass.Calendar.Id, true);
        List<SCStock__c> assignStocks = new List<SCStock__c>();
        assignStocks.add(SCHelperTestClass.stocks.get(0));
        assignStocks.add(SCHelperTestClass.stocks.get(1));
        SCHelperTestClass.createOrderTestSet3(true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, assignStocks, 
                                              SCHelperTestClass.businessUnit.Id, true);

        Id brandId = null;
        // create a price list
        SCPriceList__c priceList = createPriceList('GB', brandId);
        debug('priceList: ' + priceList);        
        
        // create stock        
        Date start = Date.today();
        start = start.addDays(-480);

        createAllItems_A(SCHelperTestClass.plant.Id, SCHelperTestClass.stocks.get(0).Id, priceList.Id);

        return SCHelperTestClass.stocks.get(0).Id;
    }
    
    public static SCPlant__c createPlant()
    {
        SCPlant__c plant = new SCPlant__c();
        plant.Name = 'SO Test GB Plant';
        plant.Info__c = 'SO Test GB Plant';
        plant.ID2__c = 'SO Test GB Plant';
        plant.Description__c = 'SO Test GB Plant';
        
        upsert plant;
        return plant; 
    }    

    public static SCPriceList__c createPriceList(String country, Id brand)
    {
        SCPriceList__c priceList = new SCPriceList__c();
        priceList.Name = 'SO Test GB price list name';
        priceList.Country__c = country;
        priceList.Brand__c = brand;
        
        priceList.ID2__c = 'SO Test GB price list';
        upsert priceList;
        return priceList;
    }


    public static SCStock__c createStock(Id plantId)
    {
        SCStock__c stock = new SCStock__c();
        stock.Description__c = 'SO Test GB Stock';
        stock.ID2__c = 'SO Test GB Stock';
        stock.MaxValue__c = 30000; // change 
        stock.MaxWeight__c = 560;
        stock.Name = 'SO Test GB Stock';
        stock.Plant__c = plantId;
        
        upsert stock;
        return stock;
    }


    // create package of items
    public static void cI( String rule,
                    String articleName,
                    String articleUnit,
                    String articleWeightUnit,
                    Decimal articleWeight,
                    Decimal articleVolume,
                    String articleReplacedBy,
                    Decimal articlePrice,
                    Decimal articlePriceUnit,
                    Boolean stockItemPermanent,
                    String lockType,
                    Decimal stockItemMinQty,
                    Decimal stockItemQty,
                    Decimal movementQty,
                    Id plantId,
                    Id stockId,
                    Id priceListId)
    {
        debug('Test rule: ' + rule);
        // create an article
        SCArticle__c article = createArticle(articleName, 
                                             articleUnit,
                                             articleWeightUnit, 
                                             articleWeight,
                                             articleVolume,
                                             articleReplacedBy,
                                             lockType);
        debug('article: ' + articleName + ' ' + article);
        // create a price list item
        SCPriceListItem__c priceListItem = createPriceListItem(priceListId, 
                                                  article.Id,
                                                  articlePrice,
                                                  articlePriceUnit);
        debug('priceListItem: ' + articleName + ' ' + priceListItem);
        
        // create stock item
        createStockItem(stockId,
                        article.Id,
                        stockItemMinQty,
                        stockItemPermanent,
                        stockItemQty);


        // create material movements
        createMaterialMovements(stockId, article.Id, movementQty);
    }// createItems
     
 
        
    public static SCArticle__c createArticle(String name, 
                                     String unit,
                                     String weightUnit, 
                                     Decimal weight,
                                     Decimal volume,
                                     String articleReplacedBy,
                                     String lockType)
    {
        Id articleReplacedById = null;
        if(articleReplacedBy != null)
        {
            List<SCArticle__c> articleList = [Select id from SCArticle__c where name =  :articleReplacedBy];
            if(articleList.size() > 0)
            {
                articleReplacedById = articleList[0].id;
            }
        }
        SCArticle__c a = new SCArticle__c();
        a.Name = name;
        a.Unit__c = unit;
        a.WeightUnit__c = weightUnit;
        a.Weight__c = weight;
        a.Volume__c = volume;
        a.LockType__c = lockType;
        if(articleReplacedById != null)
        {
            a.ReplacedBy__c = articleReplacedById;
        }
        upsert a;
        
        return a;
    }

    public static SCPriceListItem__c createPriceListItem(Id priceListId, 
                                                  Id articleId,
                                                  Decimal price,
                                                  Decimal priceUnit)
    {
        SCPriceListItem__c priceListItem = new SCPriceListItem__c();
        priceListItem.PriceList__c = priceListId;
        priceListItem.Article__c = articleId;
        priceListItem.Price__c = price;
        priceListItem.PriceUnit__c = priceUnit;
        
        upsert priceListItem;
        return priceListItem;
    }


    public static void createStockItem(Id stockId,
                                Id articleId,
                                Decimal stockItemMinQty,
                                Boolean stockItemPermanent,
                                Decimal stockItemQty)
    {
        SCStockItem__c stockItem = new SCStockItem__c();
        stockItem.Article__c = articleId;
        stockItem.MinQty__c = stockItemMinQty;
        stockItem.Permanent__c = stockItemPermanent;
        stockItem.Qty__c = stockItemQty;
        stockItem.Stock__c = stockId;
        upsert stockItem;
        debug('stock item: ' + stockItem);
    }



    public static void  createMaterialMovements(ID stockId, ID articleId, Decimal qty)
    {
        SCMaterialMovement__c mm = new SCMaterialMovement__c();
        mm.Stock__c = stockId;
        mm.Article__c = articleId;
        mm.Type__c = '5222';
        mm.Qty__c = qty;
        mm.Status__c = '5402';
        upsert mm;
        debug('materialMovement: ' + mm);
    }

    public static SCMaterialReplenishment__c readReplenishment(Id stockId)
    {
        SCMaterialReplenishment__c ret = [SELECT CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, LastModifiedById, LastModifiedDate, Name, OwnerId, Plant__c, Id, RequisitionDate__c, Stock__c, SystemModstamp FROM SCMaterialReplenishment__c where Stock__c = : stockId];
        debug('replenishment object: ' + ret);
        return ret;
    }
    
    public static Map<String, SCMaterialMovement__c> getMapArticleToMaterialMovement(Id stockId, Id replenishmentId)
    {
        Map<String, SCMaterialMovement__c> ret = new Map<String, SCMaterialMovement__c>();
        List<SCMaterialMovement__c> materialMovementList = [SELECT Article__c, Article__r.Name, Assignment__c, CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, DeliveryDate__c, DeliveryNote__c, DeliveryQty__c, DeliveryStock__c, ID2__c, LastModifiedById, LastModifiedDate, ListPrice__c, Name, Replenishment__c, Order__c, OrderLine__c, OrderRole__c, OwnerId, Plant__c, PrimeCost__c, ProcessDate__c, Qty__c, Id, ReplenishmentType__c, RequisitionDate__c, Status__c, Stock__c, SystemModstamp, Type__c, DelivAddrCity__c, DelivAddrCountry__c, DelivAddrCounty__c, DelivAddrExtension__c, DelivAddrHouseNo__c, DelivAddrName__c, DelivAddrPostalCode__c, DelivAddrCountryState__c, DelivAddrStreet__c, ReturnInfo__c, ReturnNo__c, SerialNo__c
                                                             FROM SCMaterialMovement__c
                                                             where Stock__c = :stockId and Replenishment__c = : replenishmentId];
        debug('materialMovementsList: ' + materialMovementList);                                                             
        for(SCMaterialMovement__c mm: materialMovementList)
        {
            debug('read material movement: ' + mm.Article__r.Name + ' -> ' + mm);
            ret.put(mm.Article__r.Name, mm);
        }
        return ret;
    }    

    public static List<SCMaterialMovement__c> readProposalItem(Id stockId)
    {
        List<SCMaterialMovement__c> itemList = [SELECT Article__c, Assignment__c, CreatedById, CreatedDate, CurrencyIsoCode, IsDeleted, DeliveryDate__c, DeliveryNote__c, DeliveryQty__c, DeliveryStock__c, ID2__c, LastModifiedById, LastModifiedDate, ListPrice__c, Name, Replenishment__c, Order__c, OrderLine__c, OrderRole__c, OwnerId, Plant__c, PrimeCost__c, ProcessDate__c, Qty__c, Id, ReplenishmentType__c, RequisitionDate__c, Status__c, Stock__c, SystemModstamp, Type__c, DelivAddrCity__c, DelivAddrCountry__c, DelivAddrCounty__c, DelivAddrExtension__c, DelivAddrHouseNo__c, DelivAddrName__c, DelivAddrPostalCode__c, DelivAddrCountryState__c, DelivAddrStreet__c, ReturnInfo__c, ReturnNo__c, SerialNo__c
                                                             FROM SCMaterialMovement__c
                                                             where Stock__c = :stockId];
        return itemList;
    }   
    
    public static List<SCStockItem__c> readStockItems(Id stockId)
    {
        List<SCStockItem__c> itemList = [Select Id, Name, MinQty__c 
                                         From SCStockItem__c 
                                         Where Stock__c = :stockId];
        return itemList;
    }   

    public static void createResourceAssignment(SCStock__c stock)
    {
    }

    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }
 
 }