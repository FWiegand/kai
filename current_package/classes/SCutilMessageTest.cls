/*
 * @(#)SCutilMessageTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !! temporary helper to send messages - will be replaced by a more powerful implementation
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

@isTest
private class SCutilMessageTest
{   
   /*
    * Simple test of the today evaluation
    */
    static testMethod void test2IsOnToday()
    {
        Date startdat = DateTime.NOW().dateGMT();
        Date enddat   = DateTime.NOW().dateGMT();            

        SCAppointment__c app = new SCAppointment__c();
        System.assertEquals(false, SCutilMessage.isOnToday(app));
        
        app.Start__c = startdat;
        app.End__c   = enddat;
        System.assertEquals(true, SCutilMessage.isOnToday(app));

        app.Start__c = startdat + 1;
        app.End__c   = enddat;
        System.assertEquals(false, SCutilMessage.isOnToday(app));

        app.Start__c = startdat;
        app.End__c   = enddat - 1;
        System.assertEquals(false, SCutilMessage.isOnToday(app));

        app.Start__c = startdat - 1;
        app.End__c   = enddat + 1;
        System.assertEquals(true, SCutilMessage.isOnToday(app));

        app.Start__c = startdat - 10;
        app.End__c   = enddat   - 9;
        System.assertEquals(false, SCutilMessage.isOnToday(app));

        app.Start__c = startdat + 9;
        app.End__c   = enddat   + 10;
        System.assertEquals(false, SCutilMessage.isOnToday(app));
    }
    
   /*
    * Test the order creation message event creation
    */
    static testMethod void test1OrderCreate()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        
        Date startdat = DateTime.NOW().dateGMT();
        Date enddat   = DateTime.NOW().dateGMT();            
        
        Test.startTest();

            SCutilMessage  msg = new SCutilMessage();
            msg.onOrderCreate(SCHelperTestClass.order);
            String msgId1 = msg.send();                        
    
            msg.onOrderCancel(SCHelperTestClass.order);
            String msgId2 = msg.send();                        
                                
        Test.stopTest();

        SCMessage__c msg1 = [select id, Subject__c, Event__c, Order__c, appointment__c, resource__c  from SCMessage__c where id = :msgId1];
        SCMessage__c msg2 = [select id, Subject__c, Event__c, Order__c, appointment__c, resource__c  from SCMessage__c where id = :msgId2];

        System.debug('###na1###' + msg1);
        System.debug('###na1###' + msg2);
        
        System.assertEquals('Order.Created', msg1.Event__c);
        System.assertEquals(SCHelperTestClass.order.id, msg1.order__c);

        System.assertEquals('Order.Cancelled', msg2.Event__c);
        System.assertEquals(SCHelperTestClass.order.id, msg2.order__c);
    }
    
    
   /*
    * Test the order appointment message event creation
    */
    static testMethod void test2Appointments()
    {
        SCHelperTestClass.createOrderTestSet4(true);
        String oid = SCHelperTestClass.order.id;        
        
        Date startdat = DateTime.NOW().dateGMT();
        Date enddat   = DateTime.NOW().dateGMT();            
        
        Test.startTest();

        SCAppointment__c app = new SCAppointment__c(); 
        app.order__c    = oid;
        app.resource__c = SCHelperTestClass.resources[0].id;
        app.Start__c    = startdat;
        app.End__c      = enddat;
        app.PlanningType__c = SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_MANUAL;
        app.assignmentstatus__c   = SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED;
        
        // the insert trigger creates the message record 
        insert app;
        // the delete trigger creates the message record 
        delete app;

        Test.stopTest();

        List<SCMessage__c> items = [select id, Subject__c, Event__c, Order__c, appointment__c, resource__c  from SCMessage__c where order__c = :oid order by name];
        for(SCMessage__c i : items)
        {
            if(i.Event__c == 'Appointment.InsertOnToday' && i.id == app.id)
            {
                System.assertEquals(oid, i.order__c);        
                System.assertEquals(app.resource__c, i.resource__c);        
                System.assertEquals(app.id, i.appointment__c);        
            }            
            else if(i.Event__c == 'Appointment.DeletedOnToday')
            {
                System.assertEquals(SCHelperTestClass.order.id, i.order__c);        
                System.assertEquals(app.resource__c, i.resource__c);        
            }            
        } 
    }    
}