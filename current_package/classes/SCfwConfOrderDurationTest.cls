/*
 * @(#)SCfwConfOrderDurationTest.cls
 *
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwConfOrderDurationTest
{
    static testMethod void testOrderDuration1()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createInstalledBaseLocation(true);

        SCInstalledBase__c instBase1 = new SCInstalledBase__c(
                                            SerialNo__c = '100100100',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductGroup__c = 'XY_100');
        insert instBase1;
        SCInstalledBase__c instBase2 = new SCInstalledBase__c(
                                            SerialNo__c = '100100101',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductGroup__c = 'XY_110');
        insert instBase2;
        SCInstalledBase__c instBase3 = new SCInstalledBase__c(
                                            SerialNo__c = '100100102',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductGroup__c = 'XY_120');
        insert instBase3;
        SCInstalledBase__c instBase4 = new SCInstalledBase__c(
                                            SerialNo__c = '100100103',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductUnitClass__c = '09');
        insert instBase4;
        SCHelperTestClass2.createOrderDurations(true);

        Test.startTest();

        ApexPages.currentPage().getParameters().put('aid', SCHelperTestClass.account.Id);
        SCOrderPageController controller = new SCOrderPageController();
        controller.newInstalledBase = instBase1.Id;
        controller.addNewOrderItem();
        System.assertEquals(15, controller.order.order.Duration__c);

        controller.newInstalledBase = instBase2.Id;
        controller.addNewOrderItem();
        //TODO System.assertEquals(35, controller.order.order.Duration__c);

        controller.deletedItem = '2';
        controller.delItem();
        //TODO System.assertEquals(15, controller.order.order.Duration__c);

        controller.newInstalledBase = instBase3.Id;
        controller.chgOrderItem();
        System.assertEquals(25, controller.order.order.Duration__c);

        controller.newInstalledBase = instBase4.Id;
        controller.chgOrderItem();
        System.assertEquals(90, controller.order.order.Duration__c);

        Test.stopTest();
    } // testOrderDuration1

    static testMethod void testOrderDuration2()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createInstalledBaseLocation(true);
        
        // GMSSU 28.06.2012: added because of 'article list is empty' error
        // and createOrderTestSet creates articles
        SCHelperTestClass.createOrderTestSet(true);

        System.debug('#### SCHelperTestClass.articles[0].Id: ' + SCHelperTestClass.articles[0].Id);
        System.debug('#### SCHelperTestClass.location.Id: ' + SCHelperTestClass.location.Id);

        SCInstalledBase__c instBase1 = new SCInstalledBase__c(
                                            SerialNo__c = '100100100',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            Article__c = SCHelperTestClass.articles[0].Id,
                                            ProductGroup__c = 'XY_150', 
                                            ProductPower__c = '100', 
                                            ProductUnitClass__c = '01', 
                                            ProductUnitType__c = '001');
        insert instBase1;

        // create order durations
        SCHelperTestClass2.createOrderDurations(true);

        Test.startTest();

        SCboOrder boOrder = new SCboOrder();
        boOrder.order.Brand__c = SCHelperTestClass.brand.Id;
        boOrder.createDefaultRoles(SCHelperTestClass.account.Id);

        SCboInstalledBase boInstBase = new SCboInstalledBase();
        SCOrderItem__c orderItem1 = new SCOrderItem__c();
        orderItem1.Order__r = boOrder.order;
        orderItem1.InstalledBase__c = instBase1.Id;
        orderItem1.InstalledBase__r = instBase1;
        SCboOrderItem item = new SCboOrderItem(orderItem1);
        boOrder.boOrderItems.add(item);
        // ensure that the duration is calculated
        // boOrder.order.Duration__c = 0;

        // create resource for the test user
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);

        boOrder.save();
        //System.assertEquals(85, boOrder.order.Duration__c);

        test.stopTest();
    } // testOrderDuration2 

    static testMethod void testOrderDuration3()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass2.createOrderDurations(true);

        Test.startTest();

        Integer duration = -1;

        duration = SCfwConfOrderDuration.getDurationEx(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, 
                                                       null, null, 'XY_100', null, null);
        System.assertEquals(15, duration);

        duration = SCfwConfOrderDuration.getDurationEx(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, 
                                                       null, null, 'XY_110', null, null);
        System.assertEquals(20, duration);

        duration = SCfwConfOrderDuration.getDurationEx(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, 
                                                       null, null, 'XY_120', null, null);
        System.assertEquals(25, duration);

        duration = SCfwConfOrderDuration.getDurationEx(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, 
                                                       '09', null, null, null, null);
        System.assertEquals(90, duration);

        Test.stopTest();
    } // testOrderDuration3

    static testMethod void testOrderDuration4()
    {
        Test.startTest();

        SCfwConfOrderDuration confDur = new SCfwConfOrderDuration('NL');
        
        Integer duration = confDur.getDuration(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, 
                                               '10', '111', 'XY_100', null, null);
        System.assertEquals(-1, duration);
        Test.stopTest();
    } // testOrderDuration4
} // SCfwConfOrderDurationTest