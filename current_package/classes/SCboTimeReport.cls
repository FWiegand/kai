/*
 * @(#)SCboTimeReport.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Business object to read and process time report data of the engineers.
 * The time report consists of a day start, time report records and a day end.
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboTimeReport
{
    public SCTimeReport__c timeReport;

    /**
     * Standardconstructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCboTimeReport()
    {
    } // SCboTimeReport

    /**
     * Constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCboTimeReport(SCTimeReport__c timeReport)
    {
        this.timeReport = timeReport;
    } // SCboTimeReport

    /**
     * Reads all day openings without corrosponding closing.
     * @param resourceId the id of the engineer SCResource
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCTimeReport__c readOpenDays(Id resourceId)
    {
        try
        {
            // search for the last opened day
            SCTimeReport__c daystart = [select Id, ID2__c, Start__c, Resource__c
                                     from SCTimeReport__c
                                     where Resource__c = :resourceId
                                       and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                                       and Start__c < :Date.today()
                                     order by Start__c, End__c, Name  desc limit 1];

            // day found...

            try
            {
                Datetime endTimeDayStart = Datetime.newInstance(daystart.Start__c.year(),
                                                                daystart.Start__c.month(),
                                                                daystart.Start__c.day(),
                                                                23, 59, 0);
                // look if the is a closed day entry for this day
                // DH: The day end must be at the same day not later
                SCTimeReport__c dayend = [select Id, Start__c, Resource__c
                                         from SCTimeReport__c
                                         where Resource__c = :resourceId
                                           and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND
                                           and Start__c >= :daystart.Start__c
                                           and End__c <= :endTimeDayStart
                                           limit 1];
                // closed day entry found, so return nothing
                return null;

            }
            catch (Exception e)
            {
                // opened day without closed day entry found, return the open day
                return daystart;
            }

        }
        catch (Exception e)
        {
            // no opened day found, so return nothing
            return null;
        }

    } // readOpenDays

    /**
     * Reads all day openings without corrosponding closing.
     * @param resourceId the id of the engineer SCResource
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCTimeReport__c readOpenDays2(Id resourceId)
    {
        try
        {
            // search for the last opened day
            SCTimeReport__c daystart = [select Id, Start__c, Resource__c,
                                        Status__c
                                     from SCTimeReport__c
                                     where Resource__c = :resourceId
                                       and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                                       and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                                       and Start__c < :Date.today()
                                     order by  Start__c, End__c, Name  desc limit 1];

            System.debug('#### SCTimeReport__c daystart ' + daystart);
            return daystart;
        }
        catch (Exception e)
        {
            // no opened day found, so return nothing
            return null;
        }
        //System.debug('#### SCTimeReport__c daystart 2 ' + daystart);
    } // readOpenDays

    /**
     * Reads an open day start entry for the given
     * resource and timespan.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCTimereport__c readOpenDayStart(String resourceId,
                                            DateTime startDate, DateTime endDate)
    {
        return [select Id, Name, ID2__c, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                  and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                  and Start__c >= :startDate
                  and End__c <= :endDate
                order by Start__c, End__c, Name limit 1];
    } // readOpenDayStart

    /**
     * Reads an open day start entry for the given
     * resource and timespan.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> readAllDayStart(String resourceId,
                                            DateTime startDate, DateTime endDate)
    {
        return [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                  //and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                  and Start__c >= :startDate
                  and End__c <= :endDate
                order by Start__c, End__c, Name];
    }

    /**
     * Reads an open day start entry for the given
     * resource and timespan.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<String> readAllDayStartsWithLink(String resourceId,
                                            DateTime startDate, DateTime endDate)
    {
        List <SCTimeReport__c> startList = [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                  //and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                  and Start__c >= :startDate
                  and End__c <= :endDate
                order by Start__c, End__c, Name];

        List <String> startListIds = new List <String> ();
        for (SCTimeReport__c startTimeReport :startList)
        {
            startListIds.add(startTimeReport.Id);
        }

        SCTimeReport__c linkTimeReport = new SCTimeReport__c ();

        try
        {
            linkTimeReport = [Select ReportLink__c
                                      from SCTimeReport__c
                                    where Resource__c = :resourceId and
                                      Type__c <> :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART and
                                      End__c >= :startDate and
                                      End__c <= :endDate and
                                      ReportLink__c NOT in :startListIds limit 1];
        }
        catch (Exception e)
        {
            linkTimeReport = null;
        }
        System.debug('### readAllDayStartsWithLink linkTimeReport is : '+ linkTimeReport);
        /*
        boolean searchId = false;

        for (SCTimeReport__c startTimeReport :startList)
        {

            //startListIds.add(startTimeReport.Id);

            if (linkTimeReport.ReportLink__c == startTimeReport.Id)
            {
                searchId = true;
                //return startList;
            }
        }
        */
        //if ((!searchId) && (linkTimeReport != null))
        if (linkTimeReport != null)
        {
            List <String> stringListWithLink = new List <String> ();
            stringListWithLink.add(linkTimeReport.ReportLink__c);
            stringListWithLink.addAll(startListIds);
            // add Id the start list
            startListIds = stringListWithLink;
            return startListIds;
        }

        System.debug('### readAllDayStartsWithLink startListIds are : '+ startListIds);
        return startListIds;
    }

    /**
     * Reads all time report recors of the class EMPLOYEE for the given
     * resource and timespan.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> readAllEmplTimereports(String resourceId,
                                                        DateTime startDate, DateTime endDate)
    {
        return [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and Start__c >= :startDate
                  and End__c <= :endDate
                order by  Start__c, End__c, Name];
    } // readAllEmplTimereports

    /**
     * Reads all time report recors of the class EMPLOYEE for the given
     * resource and timespan.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> readAllEmplTimereports3(String resourceId,
                                                         DateTime startDate, DateTime endDate, String status)
    {
        return [select Id, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and Start__c >= :startDate
                  and End__c <= :endDate
                  and Status__c = :status
                order by  Start__c, End__c, Name];
    } // readAllEmplTimereports

    /**
     * Reads all time report recors of the class EMPLOYEE for the given
     * resource and timespan.
     *
     * @resourceId Resource Id
     * @reportLink reportLink as String
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> readByLink(Id resourceId, String reportLink)
    {
        return [select Id, Name, ID2__c, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                from SCTimereport__c
                where Resource__c = :resourceId
                  and ReportLink__c = :reportLink
                  //and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                order by  Start__c, End__c, Name  ];
    } // readByLink

    /**
     * Rounds a date to a time grid
     * Example: round(Calendar.MINUTE, 15) fastens the date to a 15 minutes grid
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Datetime roundMinutes(Datetime orgDate, Integer gridValue)
    {
        Datetime roundDate;
        Integer cur = orgDate.minute();
        Integer grid = 0;

        if (gridValue != 0)
        {
            while (grid  < cur)
            {
                grid += gridValue;
            } // while (grid  < cur)
        } // if (gridValue != 0)

        if (grid > 60)
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(),
                                              orgDate.hour(), 0, 0).addHours(1);
        } // if (grid > 60)
        else
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(),
                                              orgDate.hour(), grid, 0);
        } // else [if (grid > 60)]

        return roundDate;
    } // roundMinutes

    /**
     * Get true if exists time reports earler as current entry.
     *
     * @param timeReport current time report entry (SCTimeReport__c)
     * @return overlapTimeReport String time that was found
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String overlapTimeReport(SCTimeReport__c timeReport)
    {
        /*
         * Try to found time reports with starts earler as current
         * entry with other time report link.
         */
        //Map <boolean, String> overlapTimeReport = new Map <boolean, String> ();
        String overlapTimeReport = null;
        SCTimeReport__c timeReportsEarler = new SCTimeReport__c ();

        System.debug('#### The transferred timereport is: '+timeReport);

        try
        {
            timeReportsEarler = [Select Id,
                                        ID2__c,
                                        Start__c,
                                        End__c,
                                        Resource__c,
                                        ReportLink__c
                                from SCTimeReport__c
                            where Id <> :timeReport.Id and
                                  Resource__c = :timeReport.Resource__c and
                                  Start__c < :timeReport.End__c and
                                  End__c > :timeReport.Start__c limit 1];

           System.debug('#### overlapTimeReport :'+timeReportsEarler);
        }
        catch (Exception e)
        {
            //nothing found, all ok
            return overlapTimeReport;
        }

        if (timeReportsEarler != null)
        {
            // time reports found
            overlapTimeReport = timeReportsEarler.End__c.format('HH:mm');
            System.debug('#### overlapTimeReport TRUE' + overlapTimeReport);
            return overlapTimeReport;
        }
        else
        {
            // no time reports found
            System.debug('#### overlapTimeReport FALSE');
            return overlapTimeReport;
        }
    }

    /**
     * Get true if exists time reports earler as current entry.
     * Ignores time report entries with the same order relation as the given time report.
     *
     * @param timeReport current time report entry (SCTimeReport__c)
     * @return overlapTimeReport String time that was found
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String overlapTimeReportEx(SCTimeReport__c timeReport)
    {
        /*
         * Try to found time reports with starts earler as current
         * entry with other time report link.
         */
        String overlapTimeReport = null;
        SCTimeReport__c timeReportsEarler = new SCTimeReport__c ();
        System.debug('#### The transferred timereport is: ' + timeReport);

        try
        {
            timeReportsEarler = [Select Id, ID2__c, Start__c, End__c,
                                        Resource__c, ReportLink__c
                                   from SCTimeReport__c
                                  where Id <> :timeReport.Id and
                                        Resource__c = :timeReport.Resource__c and
                                        // DH: search for the time report overlapping, order is not relevant 
                                        //Order__c <> :timeReport.Order__c and 
                                        Start__c < :timeReport.End__c and
                                        End__c > :timeReport.Start__c limit 1];
           System.debug('#### overlapTimeReportEx: ' + timeReportsEarler);
        }
        catch (Exception e)
        {
            //nothing found, all ok
            return overlapTimeReport;
        }

        if (timeReportsEarler != null)
        {
            // time reports found
            overlapTimeReport = timeReportsEarler.End__c.format('HH:mm');
            System.debug('#### overlapTimeReportEx TRUE ' + overlapTimeReport);
            return overlapTimeReport;
        }
        else
        {
            // no time reports found
            System.debug('#### overlapTimeReportEx FALSE');
            return overlapTimeReport;
        }
    } // overlapTimeReportEx

    /**
     * Get the last mileage entry for resource Id
     *
     * @param resourceId Resource Id as String
     * @return lastMileageEntry Last mileage entry as Integer
     */
    public static Integer getLastMileageEntry (String resourceId)
    {
        Integer lastMileageEntry = 0;
        try
        {
            lastMileageEntry = [select Id, ID2__c, Start__c, Mileage__c, Resource__c
                               from SCTimereport__c
                  where Resource__c = :resourceId and
                        Mileage__c > 0
                  //order by LastModifiedDate DESC limit 1].Mileage__c.intValue(); //by Mileage__c
                    order by End__c DESC limit 1].Mileage__c.intValue();
            return lastMileageEntry;
        }
        catch (Exception e)
        {
            // not mileage found
            return lastMileageEntry;
        }
    }

    /**
     * Get the mileage entry before time report
     *
     * @param timeReport SCTimeReport__c
     * @return lastMileageEntry Last mileage entry as Integer
     */
    public static Integer getMileageEntryBefore (SCTimeReport__c timeReport)
    {
        Integer mileageEntryBefore = 0;
        
        System.debug('#### getMileageEntryBefore for timeReport: '+timeReport);
        try
        {
            if ((timeReport.Mileage__c == null) || (timeReport.Mileage__c == 0))
            {
                mileageEntryBefore = [select Id, ID2__c, Mileage__c, Start__c, ReportLink__c, Resource__c
                                   from SCTimereport__c
                      //where (ReportLink__c = :timeReport.ReportLink__c or
                      //       Id = :timeReport.ReportLink__c) and
                      where Resource__c = :timeReport.Resource__c and
                            //Start__c <= :timeReport.Start__c and
                            End__c < :timeReport.End__c and
                            Mileage__c > 0
                      //order by Start__c, End__c, Name DESC limit 1].Mileage__c.intValue();
                      order by End__c DESC limit 1].Mileage__c.intValue();
                System.debug('#### getMileageEntryBefore mileageEntryBefore found: '+mileageEntryBefore);      
            }
            else
            {
                mileageEntryBefore = timeReport.Mileage__c.intValue();
            }
            return mileageEntryBefore;
        }
        catch (Exception e)
        {
            // not mileage found
            return mileageEntryBefore;
        }
    }

    /**
     * Get true if exists time reports earler as current entry.
     *
     * @param timeReport current time report entry (SCTimeReport__c)
     * @return overlapTimeReport String time that was found
     */
    public static String beforeTimeReportBegin (SCTimeReport__c timeReport)
    {
        /*
         * Try to found time reports with starts earler as current
         * entry with other time report link.
         */
        //Map <boolean, String> overlapTimeReport = new Map <boolean, String> ();
        String beforeTimeReportBegin = null;
        SCTimeReport__c timeReportsBegin = new SCTimeReport__c ();

        System.debug('#### The transferred timereport is: '+timeReport);

        try
        {
            timeReportsBegin = [Select Id,
                                       ID2__c,
                                       Start__c,
                                       End__c,
                                       Resource__c,
                                       ReportLink__c,
                                       Type__c
                                from SCTimeReport__c
                            where Id = :timeReport.ReportLink__c and
                                  //Resource__c = :timeReport.Resource__c and
                                  Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART and
                                  (Start__c > :timeReport.End__c or
                                   Start__c > :timeReport.Start__c)];

           System.debug('#### overlapTimeReport :'+timeReportsBegin);
        }
        catch (Exception e)
        {
            //nothing found, all ok
            return beforeTimeReportBegin;
        }

        if (timeReportsBegin != null)
        {
            // time reports start found
            beforeTimeReportBegin = timeReportsBegin.Start__c.format('HH:mm');
            //System.debug('#### overlapTimeReport TRUE' + beforeTimeReportBegin);
            return beforeTimeReportBegin;
        }
        else
        {
            // no time reports found
            System.debug('#### overlapTimeReport FALSE');
            return beforeTimeReportBegin;
        }
    }

    /**
     * Get the begin of time report after current entry.
     *
     * @param timeReport current time report entry (SCTimeReport__c)
     * @return overlapTimeReport String time that was found
     */
    public static String giveTimeReportAfter (SCTimeReport__c timeReport)
    {
        /*
         * Try to found time reports start after current time report.
         */
        String nexTimeReportBegin = null;
        SCTimeReport__c timeReportAfter = new SCTimeReport__c ();
        SCTimeReport__c dayBegin = new SCTimeReport__c ();

        System.debug('#### The transferred timereport is: '+timeReport);

        if (timeReport.ReportLink__c != null)
        {
            dayBegin = [Select Id,
                               ID2__c,
                               Start__c,
                               End__c,
                               ReportLink__c
                         from SCTimeReport__c
                         where Id = :timeReport.ReportLink__c];

        }
        else
        {
            dayBegin = timeReport;
        }

        Datetime startInto24Hours = dayBegin.Start__c.addHours(24);

        try
        {
            timeReportAfter = [Select Id,
                                      ID2__c,
                                      Start__c,
                                      End__c,
                                      Resource__c,
                                      ReportLink__c,
                                      Type__c
                            from SCTimeReport__c
                            where Resource__c = :timeReport.Resource__c and
                                  Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART and
                                  (Start__c < :timeReport.End__c or
                                   Start__c < :timeReport.Start__c) and
                                   Start__c > :dayBegin.Start__c and
                                   Id != :dayBegin.Id and
                                   Start__c < :startInto24Hours limit 1];

           System.debug('#### overlapTimeReport :'+timeReportAfter);
        }
        catch (Exception e)
        {
            //nothing found, all ok
            System.debug('#### overlapTimeReport is null');
            return nexTimeReportBegin;
        }

        if (timeReportAfter != null)
        {
            // time reports start found
            nexTimeReportBegin = timeReportAfter.Start__c.format('HH:mm');
            System.debug('#### overlapTimeReport TRUE' + nexTimeReportBegin);
            return nexTimeReportBegin;
        }
        else
        {
            // no time reports found
            System.debug('#### overlapTimeReport FALSE');
            return nexTimeReportBegin;
        }
        return null;
    }

    /**
     * Optimize the mileage of time report day start Id.
     * All start times of time report items will be set to
     * the end of item before.
     *
     * @dayStartId Id from the time report start
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void optimizeMileage(String dayStartId)
    {
        SCboTimeReport boTimeReport = new SCboTimeReport();

        List <SCTimeReport__c> timeReportList =
              boTimeReport.giveTimeReportsByStart(dayStartId);

        // set the current mileage from the value of day begin
        Integer currentMileage = 0;

        if (timeReportList.get(0).Mileage__c != null)
        {
            currentMileage = timeReportList.get(0).Mileage__c.IntValue();
        }

        for (Integer i = 0; i <= timeReportList.size()-1; i++)
        {
            if (i > 0)
            {
                /**
                 * Update the mileage of current time report entry,
                 * if it is not the first item.
                 */
                try
                {
                  //if ((items.Id != items.get(0).Id)&&(!items.Distance__c.isEmpty()))
                        currentMileage = currentMileage +
                                         timeReportList.get(i).Distance__c.intValue();
                        timeReportList.get(i).Mileage__c = currentMileage;
                }
                catch (Exception e)
                {
                    // Distance__c is empty ...
                    timeReportList.get(i).Mileage__c = currentMileage;
                    timeReportList.get(i).Distance__c = 0;
                }
            }
        }
        try
        {
            upsert timeReportList;
        }
        catch (Exception e)
        {
            // no upsert
        }
    }

    /**
     * Give all time reports for day start (day start included).
     *
     * @dayStartId Id from the time report start
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> giveTimeReportsByStart (String dayStartId)
    {
        List <SCTimeReport__c> timeReportList = new List <SCTimeReport__c> ();

        SCboTimeReport boTimeReport = new SCboTimeReport();
        SCTimeReport__c dayStartTR = new SCTimeReport__c();

        try
        {
            dayStartTR = [select Id, Name, ID2__c, Employee__c, Order__c, Type__c, Resource__c,
                           Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                           Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                           Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                           Status__c, ReportLink__c, Mileage__c, CompensationType__c
                        from SCTimeReport__c
                        where Id = :dayStartId];
        }
        catch (Exception e)
        {
            return timeReportList;
        }
        // add the start to the list
        timeReportList.add(dayStartTR);
        try
        {
             // add all time report with link to start into list
             timeReportList.addAll(boTimeReport.readByLink(dayStartTR.Resource__c,
                                                      dayStartId));
        }
        catch (Exception e)
        {
            return timeReportList;
        }
        return timeReportList;
    }

    /**
     * Reads all days with an open day start for one resource.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCTimereport__c> readAllOpenDays(String resourceId)
    {
        return [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                  from SCTimereport__c
                 where Resource__c = :resourceId
                   and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART
                   and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_OPENED
                 order by Start__c, End__c, Name];
    } // readAllOpenDays

    /**
     * Reads last closed time report.
     *
     * @param resourceId Resource Id as String
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCTimereport__c readLastClosedDay(String resourceId)
    {
        return [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                  from SCTimereport__c
                 where Resource__c = :resourceId
                   and Type__c = :SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND
                   //and Status__c = :SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED
                 order by Start__c DESC limit 1];
    }

    /**
     * Reads last time report for resource id.
     *
     * @param resourceId Resource Id as String
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCTimereport__c readLastTimeReport (String resourceId)
    {
        return [select Id, ID2__c, Name, Employee__c, Order__c, Type__c, Resource__c,
                       Resource__r.Name, Resource__r.DefaultDepartment__r.Name,
                       Start__c, End__c, Distance__c, Duration__c, Assignment__c,
                       Description__c, Calendar__c, Calendar__r.Name, LastModifiedDate,
                       Status__c, ReportLink__c, Mileage__c, CompensationType__c
                  from SCTimereport__c
                 where Resource__c = :resourceId
                 order by Start__c DESC limit 1];
    }

    /**
     * Compares whether the entry already exists in the database.
     * Return true if time report exists.
     *
     * @param timeReport time report for comparing
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public boolean timeReportCompare (SCTimeReport__c timeReport)
    {
        try
        {
            SCTimereport__c dayOpenEntryExists = [select Start__c,
                                                         End__c,
                                                         Resource__c,
                                                         Type__c,
                                                         Status__c
                                                    from SCTimereport__c
                                                    where Start__c = :timeReport.Start__c and
                                                          Resource__c = :timeReport.Resource__c and
                                                          Type__c = :timeReport.Type__c and
                                                          Status__c = :timeReport.Status__c
                                                          limit 1];
            // no errors, time report found
            return true;
        }
        catch (Exception e)
        {
            // no time reports with the same data found
            return false;
        }
    }

    /**
     * Get the compensation type of time report entry.
     *
     * @dateTimeEntry Entered date time
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String getCompensationType(Datetime datetimeEntry)
    {
        String compensationType = SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT;
        /*
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_DEFAULT = '7701';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_STANDBY = '7702';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY = '7703';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY = '7704';
            public final static String DOMVAL_TIMEREPORTCOMPENSATION_PUBLIC_HOLIDAY= '7705';        
        */
        
        // day of week
        String dayOfWeek = datetimeEntry.format('E');
        System.debug('#### getCompensationType(): dayOfWeek -> ' + dayOfWeek);
        
        if (dayOfWeek == 'Sat')
        {
           return SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SATURDAY;
        }
        else if (dayOfWeek == 'Sun')
        {
           return SCfwConstants.DOMVAL_TIMEREPORTCOMPENSATION_SUNDAY;        
        }
        return compensationType;
    } // getCompensationType
    
    /**
     * Create TimeReport(s) from an assignment if time report data available.
     * If the Timereport(s) already available in the database the function returns a merged timereport. 
     * Don't use the results in an insert statement! Only upsert is possible. 
     * PMS 34649: CP New Fields for Mobile Client - Time Reports Light and  PDF printoputs
     *
     * timereportLabourDuration__c     
	 * timereportTravelDistance__c 
	 * timereportTravelDuration__c 
	 * timereportEnd__c 
	 * followupDescription__c                                     
	 * 
	 * @param assignment 
	 * @return the generated time reports (traveltime, workingtime)
	 *
	 * @author	Eugen Tiessen <etiessen@gms-online.de>
     */
    public static List<SCTimeReport__c> generateTimeReportsLight(SCAssignment__c assignment)
    {
    	List<SCTimeReport__c> timeReports = new List<SCTimeReport__c>();
    	
    	//Create time reports only if timereportend__c is set
    	if(assignment.TimereportEnd__c != null)
    	{
    		//Create time report worktime only if 
    		//assignment.TimereportLabourDuration__c is set and > 0
    		DateTime driveEnd = assignment.TimereportEnd__c;
    		if(assignment.TimereportLabourDuration__c != null && assignment.TimereportLabourDuration__c > 0)
    		{
	    		SCTimeReport__c workReport	= new SCTimeReport__c(ID2__c = assignment.Id + '-' +  SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME);
	    		workReport.Type__c 			= SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME;
	    		workReport.End__c 			= assignment.TimereportEnd__c;
	    		workReport.Start__c 		= workReport.End__c.addMinutes(- (Integer)assignment.TimereportLabourDuration__c);
	    		
	    		workReport.Assignment__c	= assignment.Id;
	    		workReport.Order__c			= assignment.Order__c;
	    		workReport.Status__c		= SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED;
	    		workReport.Resource__c		= assignment.Resource__c;
	    		
	    		driveEnd					= workReport.Start__c;
	    		
	    		timeReports.add(getMergedTimeReport(workReport));
    		}
    		
    		//Create time report traveltime
    		if(assignment.TimereportTravelDuration__c != null && assignment.TimereportTravelDuration__c > 0)
    		{
	    		SCTimeReport__c travelReport	= new SCTimeReport__c(ID2__c = assignment.Id + '-' +  SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME);
	    		travelReport.Type__c 			= SCfwConstants.DOMVAL_TIMEREPORTTYPE_DRIVETIME;
	    		travelReport.End__c 			= driveEnd;
	    		travelReport.Start__c 			= travelReport.End__c.addMinutes(- (Integer)assignment.TimereportTravelDuration__c);
	    		travelReport.Distance__c		= assignment.timereportTravelDistance__c;
    			
    			travelReport.Assignment__c		= assignment.Id;
	    		travelReport.Order__c			= assignment.Order__c;
    		    travelReport.Status__c			= SCfwConstants.DOMVAL_TIMEREPORTSTATUS_CLOSED;
    		    travelReport.Resource__c		= assignment.Resource__c;
    		    
    		    timeReports.add(getMergedTimeReport(travelReport));
    		}

    	}
    	return timeReports;
    	
    }
    
    /**
     * Merge TimeReport depending on ID2
     *
     * @param timeReport the time report to merge
     * @return the merged time report 
     *
     * @author	Eugen Tiessen <etiessen@gms-online.de>
     */
    private static SCTimeReport__c getMergedTimeReport(SCTimeReport__c timeReport)
    {
        try
        {
            SCTimeReport__c tmpTimeReport = [SELECT Id FROM SCTimeReport__c WHERE Id2__c =: timeReport.ID2__c LIMIT 1];
            return (SCTimeReport__c) SCBase.mergeSObject(timeReport, tmpTimeReport);
        }
        catch(Exception e)
        {
            return timeReport;
        }
        
                        
    }
}