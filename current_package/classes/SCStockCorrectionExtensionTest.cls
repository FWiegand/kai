/*
 * @(#)SCStockCorrectionExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for manual stock correction.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCStockCorrectionExtensionTest
{
    /**
     * testStockCorrectionExtension
     * ============================
     *
     * Test the manual stock correction with the extension class.
     *
     * - Test 1: increase quantity for an article, which is not in the stock
     * - Test 2: decrease quantity for an article, which is in the stock, 
     *           but use a quantity which is greater than the one in the stock
     * - Test 3: decrease quantity for an article, which is in the stock
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testStockCorrectionExtension()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testStockCorrectionExtension()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        
        Test.startTest();
        
        // Test 1: increase quantity for an article, which is not in the stock
        SCStockCorrectionExtension stockCorrExt = 
                     new SCStockCorrectionExtension(new ApexPages.StandardController(stocks.get(0)));
        
        List<SelectOption> typeList = stockCorrExt.getTypeList();
        
        // set an article manual
        stockCorrExt.stockItem.Article__c = articleList.get(0).Id;
        // get the current quantity
        stockCorrExt.initQuantity();
        // validate the quantity
        System.assert(stockCorrExt.curQty == 0);
        
        // set a new quantity and type for correction
        stockCorrExt.qty = 2;
        stockCorrExt.matMoveType = SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN;
        // correct the stock
        stockCorrExt.correctStock();
        // validate the result
        System.assert(stockCorrExt.bookingOk);
        
        
        // Test 2: decrease quantity for an article, which is in the stock, 
        //         but use a quantity which is greater than the one in the stock
        // create a new instance, so that bookingOk is reset
        stockCorrExt = new SCStockCorrectionExtension(new ApexPages.StandardController(stocks.get(0)));
        
        // set an article manual
        stockCorrExt.stockItem.Article__c = articleList.get(0).Id;
        // get the current quantity
        stockCorrExt.initQuantity();
        // validate the quantity
        System.assert(stockCorrExt.curQty == 2);
        
        // set a new quantity and type for correction
        stockCorrExt.qty = 3;
        stockCorrExt.matMoveType = SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT;
        // correct the stock
        stockCorrExt.correctStock();
        // validate the result
        System.assert(!stockCorrExt.bookingOk);
        
        
        // Test 3: decrease quantity for an article, which is in the stock 
        // create a new instance, so that bookingOk is reset
        stockCorrExt = new SCStockCorrectionExtension(new ApexPages.StandardController(stocks.get(0)));
        
        // set an article manual
        stockCorrExt.stockItem.Article__c = articleList.get(0).Id;
        // get the current quantity
        stockCorrExt.initQuantity();
        // validate the quantity
        System.assert(stockCorrExt.curQty == 2);
        
        // set a new quantity and type for correction
        stockCorrExt.qty = 1;
        stockCorrExt.matMoveType = SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_OUT;
        // correct the stock
        stockCorrExt.correctStock();
        // validate the result
        System.assert(stockCorrExt.bookingOk);

        Test.stopTest();
    } // testStockCorrectionExtension
} // SCStockCorrectionExtensionTest