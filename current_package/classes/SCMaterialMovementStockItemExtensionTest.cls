/*
 * @(#)SCMaterialMovementStockItemExtensionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for displaying material movements for a stock item.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCMaterialMovementStockItemExtensionTest
{
    /**
     * testMaterialMovementStockItemExtension
     * ======================================
     *
     * Test the extension class for displaying the material movements for a stock item.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testMaterialMovementStockItemExtension()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testMaterialMovementStockItemExtension()');
        System.debug('########################################################');

        // create all neccessary datas
        Id plantId = SCHelperTestClass4.createTestPlant();
        List<SCStock__c> stocks = SCHelperTestClass4.createTestStocks(plantId);
        List<SCArticle__c> articleList = SCHelperTestClass4.createTestArticles();
        SCStockItem__c stockItem = SCHelperTestClass4.createTestStockItem(stocks.get(0).Id, 
                                                                        articleList.get(0).Id);
        
        SCMatMoveBaseExtension matMoveExt = new SCMatMoveBaseExtension();
        
        Test.startTest();
        
        matMoveExt.createMatMove(stocks.get(0), articleList.get(0).Id, 
                                 2 , SCfwConstants.DOMVAL_MATMOVETYP_CORRECTION_IN);
        
        SCMaterialMovementStockItemExtension matMoveStockItemExt = 
                     new SCMaterialMovementStockItemExtension(new ApexPages.StandardController(stockItem));
        
        List<SCMaterialMovement__c> matMoveList = matMoveStockItemExt.getMatMoves();
        System.assert(matMoveList.size() == 1);

        Test.stopTest();
    } // testMaterialMovementStockItemExtension
} // SCMaterialMovementStockItemExtensionTest