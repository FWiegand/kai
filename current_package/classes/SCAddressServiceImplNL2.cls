/*
 * @(#)SCAddressServiceImplNL2.cls 
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the address validation service that accesses 
 * the webservices.nl. This service is activated
 * depending on the country by SCAddressValidation.
 *
 *    SCAddressValidation v = new SCAddressValidation();
 *    List<AvsAddress> foundaddr;
 *    foundaddr = v.check('nl', 'Amsterdam Grasweg 2');
 *    System.debug('result:' + foundaddr);
*/
public with sharing class SCAddressServiceImplNL2 extends SCAddressServiceImplNL
{


    /**
     * Validate the address and return the result set
     * 
     * @param country country to search in
     * @param query search for the address string
     */
    public override AvsResult check(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            // prepare the authentication details (we are using header authentication)          
            WSWebserviceNL.Webservices_nlPort wsnl = prepare();
            
            WSWebserviceNL.PCReeks wsnlPostcodeResult;
            WSWebserviceNL.PCReeksSearchPartsPagedResult wsnlSearchParamResult;             
            if(addr.matchInfo != null && addr.matchInfo == 'test')
            {
                // only for automated testing of the service
                
                wsnlPostcodeResult = emulateCheck2(addr);
                // transfer the result into the return object
                AvsAddress item = new AvsAddress();
                item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                item.county         = wsnlPostcodeResult.provincienaam;         
                item.postalcode     = addr.postalcode; 
                item.city           = wsnlPostcodeResult.plaatsnaam;
                item.district       = wsnlPostcodeResult.gemeentenaam;
                item.street         = wsnlPostcodeResult.straatnaam;
                item.nofrom         = '' + wsnlPostcodeResult.huisnr_van; 
                item.noto           = '' + wsnlPostcodeResult.huisnr_tm; 
                item.housenumber    = addr.housenumber;
                item.matchInfo = 'emulation';
                // add the found address to the result list
                result.items.add(item);
            }
            else
            {
                // remove blanks that are not relevant here
                String inputPlCode = '';
                inputPlCode = addr.postalcode != null ? addr.postalcode.toUpperCase() :addr.postalcode ;   
                if(inputPlCode != null)
                {
                    inputPlCode.trim();
                    inputPlCode = inputPlCode.replace(' ', '');
                }
            
                boolean bSearchAgain = true;
                if( (inputPlCode != null && inputPlCode.length() > 5 && addr.housenumber != null && addr.housenumber.length() > 0)
                    || (inputPlCode != null && inputPlCode.length() > 6))
                {
                    // call the web service with unique post code 
                    
                    //-<Parameters>-------------------------------------------------------------------
                    // postcode         Postalcode search phrase 
                    //-<Return>-----------------------------------------------------------------------
                    // validation_status 
                    // result  - PCReaks structure 

                    try
                    {
                        if(inputPlCode.length() > 6)
                        {
                            wsnlPostcodeResult = wsnl.addressReeksPostcodeSearch(inputPlCode);
                        }
                        else
                        {
                            wsnlPostcodeResult = wsnl.addressReeksPostcodeSearch(inputPlCode + addr.housenumber);
                        }

                        // transfer the result into the return object
                        AvsAddress item = new AvsAddress();
                        item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                        item.countryState   = ''; // not supported by webservices.nl
                        item.county         = wsnlPostcodeResult.provincienaam;         
                        item.postalcode     = addr.postalcode != null ? addr.postalcode.toUpperCase() :addr.postalcode ;  
                        item.city           = wsnlPostcodeResult.plaatsnaam;
                        item.district       = wsnlPostcodeResult.gemeentenaam;
                        item.street         = wsnlPostcodeResult.straatnaam;
                        item.nofrom         = '' + wsnlPostcodeResult.huisnr_van; 
                        item.noto           = '' + wsnlPostcodeResult.huisnr_tm; 
                        item.geoX           = 0;  // not supported by webservices.nl 
                        item.geoY           = 0;  // not supported by webservices.nl 
                        if(addr.housenumber != null && addr.housenumber.length() > 0)
                        {
                            item.housenumber    = addr.housenumber;
                        }
                        else
                        {
                            item.housenumber    = ''; //'' + wsnlPostcodeResult.huisnr_van;
                        }
        
                        // Convert the status codes (for diagnosis only - can be removed in product environment)  
                        item.matchInfo = 'addressReeksPostcodeSearch: ' + wsnlPostcodeResult;
                        // add the found address to the result list
                        result.items.add(item);
                        bSearchAgain = false;
                    }//try
                    catch (Exception e)
                    {
                        bSearchAgain = true;        
                    }
                }//if(addr.postalcode != null && addr.postalcode.length() > 6)

                if(bSearchAgain)                                
                {
                    // call the web service with parameter search 
                    //-<Parameters>-------------------------------------------------------------------
                    // province         Province search phrase 
                    // district         Phrase used to select the municipality of the address, see PCReeks.gemeentenaam 
                    // city             Phrase used to select the city of the address, see PCReeks.plaatsnaam 
                    // street           Street search phrase 
                    // houseNo Number   used to select the house number of the address, see PCReeks.huisnr_van 
                    // houseNoAddition  Phrase used to select the house number addition of the address 
                    // page Page to retrieve, pages start counting at 1 
                    //-<Return>-----------------------------------------------------------------------
                    // validation_status 
                    // result  - PCReeksSearchPartsPagedResult 
                    Integer houseno = calcHouseno(addr.housenumber);     
                    
                    wsnlSearchParamResult = wsnl.addressReeksParameterSearch(addr.county, '', addr.city, addr.street, houseno, '', 1);

                    // Are there any results?
                    if (wsnlSearchParamResult.results.item == null)
                    {
                        result.status = AvsResult.STATUS_EMPTY;
                        result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                        System.Label.SC_app_AddressValidationEmpty);
                        ApexPages.addMessage(myMsg);
                        return result;
                    }

                    for(WSWebserviceNL.PCReeks res : wsnlSearchParamResult.results.item)
                    {
                        // transfer the result into the return object
                        AvsAddress item = new AvsAddress();
                        item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                        item.countryState   = ''; // not supported by webservices.nl
                        item.county         = res.provincienaam;         
                        item.postalcode     = res.wijkcode + res.lettercombinatie; 
                        item.city           = res.plaatsnaam;
                        item.district       = res.gemeentenaam;
                        item.street         = res.straatnaam;
                        item.nofrom         = '' + res.huisnr_van; 
                        item.noto           = '' + res.huisnr_tm; 
                        item.geoX           = 0;  // not supported by webservices.nl 
                        item.geoY           = 0;  // not supported by webservices.nl 
                        if(addr.housenumber != null && addr.housenumber.length() > 0)
                        {
                            item.housenumber    = addr.housenumber;
                        }
                        else
                        {
                            item.housenumber    = ''; //'' + wsnlPostcodeResult.huisnr_van;
                        }
        
                        // Convert the status codes (for diagnosis only - can be removed in product environment)  
                        item.matchInfo = 'addressReeksParameterSearch: ' + res;
                        // add the found address to the result list
                        result.items.add(item);
                    } // for..
                }// if(bSearchAgain)
            }//else
            // now set the status values
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e;
        }
        return result;
    } // check
    

    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public override AvsResult geocode(AvsAddress addr)
    {
        addr.geox = 0;
        addr.geoy = 0;
        Integer houseno = 0;

        AvsResult result = new AvsResult();
        try
        {
            // prepare the authentication details (we are using header authentication)          
            WSWebserviceNL.Webservices_nlPort wsnl = prepare();
            
            // call the web service
            //-<Parameters>-------------------------------------------------------------------
            // postcode Address postcode 
            // city             the town/city
            // street           Street search phrase 
            // houseno          House number search phrase 
            //-<Return>-----------------------------------------------------------------------
            // coordinates A LatLonCoordinatesMatch entry. 
            
            // this service supports only numerical house numbers
            houseno = calcHouseno(addr.housenumber);     

			String inputPLCode ='';
			inputPLCode = addr.postalcode != null ? addr.postalcode.toUpperCase() :addr.postalcode ;   
			
            if(inputPLCode != null)
            {
                inputPLCode.trim();
                inputPLCode = inputPLCode.replace(' ', '');
            }

            //when geocooding the postalcode must be 6 digits  
            if(inputPLCode.length() > 6)
            {
                //example : 3607GV274
                //extract to  postcode 3607GV and houseno 274
                addr.housenumber = inputPLCode.substring(6, inputPLCode.length());
                if(addr.housenumber.length() > 0)
                {
                    // this service supports only numerical house numbers
                    houseno = calcHouseno(addr.housenumber);     
                }
                inputPLCode = inputPLCode.substring(0,6);
            }            

            WSWebserviceNL.LatLonCoordinatesMatch wsnlResult;
            
            if(addr.matchInfo != null && addr.matchInfo == 'test')
            {
                wsnlResult = emulateGeocode2();
            }
            else
            {
                wsnlResult = wsnl.geoLocationAddressCoordinatesLatLon(inputPLCode, addr.city, addr.street, houseno);
            }                

            // Evaluate the results                 
            if(wsnlResult != null)
            {
                // transfer the result into the return object
                AvsAddress item = new AvsAddress();
                item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                item.countryState   = ''; // not supported by webservices.nl
                item.county         = ''; // not supported by webservices.nl         
                item.postalcode     = addr.postalcode != null ? addr.postalcode.toUpperCase() :addr.postalcode ;    
                item.city           = addr.city;
                item.district       = ''; // not supported by webservices.nl
                item.street         = addr.street;
                item.housenumber    = ''; // not supported by webservices.nl 
                item.nofrom         = ''; // not supported by webservices.nl
                item.noto           = ''; // not supported by webservices.nl
                item.geoX           = wsnlResult.longitude;
                item.geoY           = wsnlResult.latitude;
                if(item.geoX != null && item.geoX != 0)
                {
                    item.matchInfo = 'Geocoded';
                }
                // add the found address to the result list
                result.items.add(item);
            } // for..

            // now set the status values
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
            // set the originial status info of the service
            result.statusInfo = 'webservices.nl: -'; 
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e + ' (used houseno:' + houseno + ')';
        }
        return result;        
    } // geocode




 
    // Emulation of the web service call (test only)
    private WSWebserviceNL.LatLonCoordinatesMatch emulateGeocode2()
    {
        WSWebserviceNL.LatLonCoordinatesMatch item = new WSWebserviceNL.LatLonCoordinatesMatch();
        item.latitude = 50;
        item.longitude = 9; 
        return item;
    }
    
    // Emulation of the web service call (test only)
    private WSWebserviceNL.PCReeks emulateCheck2(AvsAddress addr)
    {
        WSWebserviceNL.PCReeks wsnlPostcodeResult = new WSWebserviceNL.PCReeks();
        // transfer the result into the return object
        wsnlPostcodeResult.plaatsnaam = addr.city;
        wsnlPostcodeResult.gemeentenaam = addr.district;
        wsnlPostcodeResult.straatnaam = addr.street;
        return wsnlPostcodeResult;
    }
 
}