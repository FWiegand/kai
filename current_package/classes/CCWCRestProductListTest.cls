/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	REST Webservice Test for ProductList
*
* @date			20.12.2013
*
* /services/apexrest/ProductList 
*/

@isTest(SeeAllData=true)
private class CCWCRestProductListTest {

	static {
		
		//create Pricebook
		Pricebook2 standardPB = [Select Id, Name, IsActive From Pricebook2 where IsStandard = true LIMIT 1];
		if (!standardPB.isActive) {
            standardPB.isActive = true;
            update standardPB;
        }
		Pricebook2 pricebook = new Pricebook2();
		pricebook.Name = 'TestPricebook';
		pricebook.DeliveryType__c = 'TestDeliveryType';
		pricebook.Type__c = 'TestType';
		insert pricebook;
		
		//create Account
		Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'DE-005';
        account.BillingPostalCode = '33106 PB';
        account.BillingCountry__c = 'DE';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        account.GeoApprox__c = false;
        account.ID2__c = '0000000test';
        account.Pricebook__c = pricebook.Id;
        insert account;
        
        //create AccountRole
        CCAccountRole__c accountrole = new CCAccountRole__c();
        accountrole.MasterAccount__c = account.Id;
        accountrole.SlaveAccount__c = account.Id;
        accountrole.Status__c = 'Active';
        accountrole.AccountRole__c = 'ZV';
        insert accountrole;
        
        //create Product
        Product2 product = new Product2();
        product.Name = 'Cola';
        product.ProductCode = 'Cola100';
        insert product;
        
        //create PricebookEntry
		PricebookEntry standardPrice = new PricebookEntry(
			Pricebook2Id = standardPB.Id, 
			Product2Id = product.Id, 
			UnitPrice = 0.70, 
			IsActive = true, 
			UseStandardPrice = false);
        insert standardPrice;        
        PricebookEntry bookentry = new PricebookEntry();
        bookentry.Pricebook2Id =  pricebook.Id;
        bookentry.Product2Id = product.Id;
        bookentry.UnitPrice = 0.75;
        bookentry.IsActive = true;
        insert bookentry;
        
		//create PricebookEntryDetail__c
		PricebookEntryDetail__c bookdetail = new PricebookEntryDetail__c();
		bookdetail.ValidUntil__c = date.today();
		bookdetail.ValidFrom__c = date.today();
		bookdetail.Product2__c = product.Id;
		bookdetail.Pricebook2__c = pricebook.Id;
		bookdetail.ListingIndicator__c = '1000';
		bookdetail.ID2__c = '0000000test';
		bookdetail.DeliveryType__c = '3213';
		insert bookdetail;
	}
	static testMethod void testDoGet() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/ProductList/0000000test';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
	 
		List<CCWCRestProductList.Products> results = CCWCRestProductList.doGet();
		System.debug('###...' +  results); 
		for (CCWCRestProductList.Products result : results){
			System.assertEquals('Cola',result.ProductEntry.ProductName);
		}
		
	}
}