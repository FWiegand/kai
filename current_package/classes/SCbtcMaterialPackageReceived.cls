/*
 * @(#)SCbtcMaterialPackageReceived.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Used to initiate the following post MaterialPackageReceived processing steps:
 * - select all pending material package reveived - (a settling time is used to 
 *   ensure that all receipt confirmations are available)
 * - call SAP 1. CCWCMaterialMovementCreate.callout(movementids);
 * - call SAP 2. CCWCMaterialPackageReceived.callout(pack.id)
 */
 
 /**
 * Run from Jenkins:
 * SCbtcMaterialPackageReceived.asyncProcessAll(10,'productive');
 */
global with sharing class SCbtcMaterialPackageReceived  extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts  
{
    // Activates the e-mail notification (see CCSettings) to forward the service report 
    private Boolean emailNotificationEnable;
    private String  emailNotificationTemplate;
    
    // we process one pdf export per execute (to avoid governor limits)
    private static final Integer batchSize = 1;
    private static final Integer batchClearingDelta = 90;  // the data must be at least this number of seconds old (settling time)

    private ID batchprocessid = null;

    String mode = 'productive';
    Integer max_cycles = 0;
    public String extraCondition = null;

    // Query for batch job 
    private String query; 

    // error text length limit
    private Integer materialPackageReceivedProcessInfoLength = 32760;

    private ID materialmovementid;

    //---<Helper functions to start the batch job>---------------------------------------

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode)
    {
        SCbtcMaterialPackageReceived btc = new SCbtcMaterialPackageReceived(max_cycles, mode);
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * You can specify an optional additional condition that is added to the statement
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode, String extraCondition)
    {
        SCbtcMaterialPackageReceived btc = new SCbtcMaterialPackageReceived(max_cycles, mode, extraCondition);
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job to process exactly the specified assigment
    * @param see below
    */
    public static ID asyncProcess(ID materialmovementid, String mode)
    {
        SCbtcMaterialPackageReceived btc = new SCbtcMaterialPackageReceived(materialmovementid, 0, mode);
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  


   /*
    * Starts the synchronosu bach job to process exactly the specified assigment
    * @param see below
    */

    public static List<SCMaterialMovement__c> syncProcess(ID materialmovementid, String mode)
    {
        SCbtcMaterialPackageReceived btc = new SCbtcMaterialPackageReceived(materialmovementid, 0, mode);
        btc.startCore(false, false); // prepares the query 
        
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCMaterialMovement__c> retValue = btc.executeCore(mList);
        return retValue;
    }  

    //---<constructurs>------------------------------------------------------------------

    /**
     * Constructor
     * @param see below
     */
    public SCbtcMaterialPackageReceived(Integer max_cycles, String mode)
    {
        this(null, max_cycles, mode, null);
    }

    /**
     * Constructor
     * @param see below
     */
    public SCbtcMaterialPackageReceived(Id materialmovementid, Integer max_cycles, String mode)
    {
        this(materialmovementid, max_cycles, mode, null);
    }


    /**
     * Constructor
     * @param see below
     */
    public SCbtcMaterialPackageReceived(Integer max_cycles, String mode, String extraCondition)
    {
        this(null, max_cycles, mode, extraCondition);
    }


    /**
     * Constructor
     * @param materialmovementid   the id of the assignment to process
     * @param max_cycles    maximum number of execution processes (0: not limited, 1: process only 1, ....)
     * @mode               'test' or 'trace'    trace additional information during test execution
     *                     'productive'         no additional tracing
     * @extraCondition     additional conditions (see soql statement below)
     */
    public SCbtcMaterialPackageReceived(Id materialmovementid, Integer max_cycles, String mode, String extraCondition)
    {
        this.materialmovementid = materialmovementid;
        this.max_cycles = max_cycles;
        this.mode = mode;
        this.extraCondition = extraCondition;
        
        // evaluate the CCEAG settings of the current (API) user for this process and store it in the batch job for later access
        CCSettings__c ccSettings = CCSettings__c.getInstance();
        
        // check if the automati e-mail notification is enabled        
        this.emailNotificationEnable = ccSettings.IFEnableEmailNotification__c == 1;
    }


   //---<batch preparation and processing>------------------------------------------------------------------

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        Boolean aborted = abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaterialPackageReceived', 'start');
        return startCore(true, aborted);
    } // start

   /*
    * Prepares the SOQL statement that is used to select the records that have to be processed
    * @param returnsRetValue   true: executes the query and returns the query locator. false: null only the soql statement is created
    * @param aborted
    * @return the query locator that represents the records to be processed
    */
    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {
        /*
             select id, name, stock__c, DeliveryNote__c, Qty__c, DeliveryQty__c, erpstatus__c,source__c,status__c,type__c
              from SCMaterialMovement__c 
              where
                source__c = 'mobile' 
                and type__c = '5232'
                and status__c = '5408' 
                and erpstatus__c = 'none'
                and canStartPostProcessing__c = '1'
                order by stock__c, deliverynote__c
        */
                
        // select all package received records that have not yet been transferred to SAP
        // the batch job will then select the sparepart received records
              
        query = 'select id, name, stock__c, DeliveryNote__c, Qty__c, DeliveryQty__c, erpstatus__c' +
                ' from SCMaterialMovement__c where' +
                ' source__c = \'mobile\' and ' +
                ' type__c = \'5232\' and ' +   // DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED
                ' status__c = \'5408\' and ' + // DOMVAL_MATSTAT_BOOKED
                ' erpstatus__c in(\'none\', \'exportpackage\')';
  
        // if a specific id is supplied - we add the id directly  
        if(materialmovementid != null)
        {
            query += ' and ID = \'' + materialmovementid + '\'';
        }
        
        // if extra conditions are to be considerted, add them here
        if(extraCondition != null)
        {
            query += ' and (' + extraCondition + ') ';
        }                   
        
        // we gourp the processing by stock and delivery note
        query += ' order by stock__c, deliverynote__c';


        // prevent the parallel execution of the batch job to avoid exporting items multiple times
        Boolean isRunning = isJobRunningForCallerCountry('SCbtcMaterialPackageReceived') == '1';
        
        // do not read any data if aborted or already running 
        if(aborted || isRunning)
        {
            query += ' limit 0';
        }
        else
        {
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }               

        system.debug('query: ' + query);
        
        if(returnsRetValue)
        {
            return Database.getQueryLocator(query);
        }
        return null;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaterialPackageReceived', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

  /*
    * Called to execute each batch
    */
    public List<SCMaterialMovement__c> executeCore(List<sObject> scope)
    {
        List<SCMaterialMovement__c> packageStatus = new List<SCMaterialMovement__c>();
        
        List<SCMaterialMovement__c> packages = (List<SCMaterialMovement__c>)scope;
        for(SCMaterialMovement__c pack : packages)
        {
            system.debug('### executeCore' + pack.DeliveryNote__c);
        
/*
select id, article__c, name, Qty__c, DeliveryQty__c from SCMaterialMovement__c where
                stock__c = 'a0pM0000000b3gvIAA' and 
                DeliveryNote__c = 'GMSGB130906-1' and
                source__c = 'mobile' and 
                type__c = '5227' and
                status__c = '5408' and
                erpstatus__c = 'none' 
                order by name
*/        
        
            // read all receipt confirmation records for the received package (from the mobile system only)
            List<SCMaterialMovement__c> receipts = [select id, name, Qty__c, DeliveryQty__c from SCMaterialMovement__c where
                stock__c = :pack.stock__c and 
                DeliveryNote__c = :pack.DeliveryNote__c and
                source__c = 'mobile' and 
                type__c = :SCfwConstants.DOMVAL_MATMOVETYP_RECEIPT_CONFIRMATION and 
                status__c = :SCfwConstants.DOMVAL_MATSTAT_BOOKED and
                erpstatus__c IN ('none','exportpackage')
                order by name];    
                
     

            // count the number of items received
            Decimal totalQty = 0;
            Decimal totalDeliveryQty = 0;
            
            // now determine the ids to be used in the callout   
            List<String> movementids = new List<String>();
            system.debug('### SCMaterialMovement__c receipt : receipts' + receipts.size() );
            for(SCMaterialMovement__c receipt : receipts)
            {
                // sum the confirmed and delivered quanities (to compare them later with the data in the package) 
                totalQty         += receipt.Qty__c;
                totalDeliveryQty += receipt.DeliveryQty__c;

                movementids.add(receipt.id);
            }     
            
            // if there are confirmed material movements - initiate the callouts
            // and all records are available
            system.debug('### for confirmed material movements - initiate the callouts' + pack.name );
            if(movementids.size() > 0 && pack.Qty__c == totalQty && pack.DeliveryQty__c == totalDeliveryQty)
            {
                // pass 1: first try to export all receipt confirmations
                if(pack.erpstatus__c == 'none')
                {    
                    // now send the information to 
                    CCWCMaterialMovementCreate.callout(movementids, false, false);
                    
                    system.debug('### CCWCMaterialMovementCreate.callout' + pack.DeliveryNote__c );
                    
                    // initiate pass 2
                    pack.ERPStatus__c = 'exportpackage';
                    packageStatus.add(pack);
                }
     
            }
            // pass 2: then export the package received
            else if(pack.erpstatus__c == 'exportpackage' )
            {

                    // and close the package (sets the pack.erpstatus__c = 'ok' or 'error')
                    CCWCMaterialPackageReceived.callout(pack.id, false, false);
                    system.debug('### CCWCMaterialPackageReceived.callout' + pack.DeliveryNote__c );
            }   
                
            else
            {
                // no receipt confirmations found or not all records are available, yet
                pack.ERPResultInfo__c = 'warning: skipped: movement count=' + movementids.size() + 
                    ' Qty[' + totalQty + ' of package ' + pack.Qty__c + '] ' +
                    ' totalDeliveryQty[' + totalDeliveryQty + ' of package ' + pack.DeliveryQty__c + '] ';
                    
                // TODO: add a max retry count and set the status to error if the max retry count has been reached.   
                //packageStatus.ERPStatus__c = 'error'
				pack.erpstatus__c = 'error';
                packageStatus.add(pack);

            }
        } // for(SCMaterialMovement__c pack
        
        // now write the status update
        update packageStatus;
        return null;
    }
        



}