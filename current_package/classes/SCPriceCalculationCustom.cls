/*
 * @(#)
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Use this class to overwrite the pricing in AmsPriceCalculation.
 * <p>
 * This class is initialised during pricing and in case nothing is overwritten
 * the base class will be used.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision: 826 $, $Date: 2010-11-16 13:39:43 +0100 (Tue, 16 Nov 2010) $
 */
public with sharing virtual class SCPriceCalculationCustom extends SCPriceCalculation
{
    private String articleWorkTime;
    private String articleTravelTime;
    
    public SCPriceCalculationCustom(SCOrder__c order)
    {
        super(order);
        this.init();
        System.debug('####[custom] SCPriceCalculationCustom order -> ' + order);
    } 

    public SCPriceCalculationCustom(SCboOrder boOrder)
    {
        super(boOrder);
        this.init();
        System.debug('####[custom] SCPriceCalculationCustom order -> ' + boOrder);
    } 
        
    public void init()
    {
        this.articleWorkTime = SCPriceCalculation.appSettings.ORDERFEEDBACK_ARTNO_WORKTIME__c;
        this.articleTravelTime = SCPriceCalculation.appSettings.ORDERFEEDBACK_ARTNO_TRAVELTIME__c;
    }   
    /**
     * Determince the price list to use for this customer.
     * 
     * The price list could be either the Account's default price list
     * or the global default price list.
     * The global price list depends on country and brand and in case the
     * brand/country specific one isn't found the country specific one is 
     * used. If neither is found the default price list from the 
     * application settings is used.
     *
     * However it could be overwritten later by other selections.
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param order Order to determine the price list for
     * @return Price List for the given order
     */
    public override SCPriceList__c determinePriceList(SCOrder__c order)
    {
        SCPriceList__c pl = null;

        System.debug('#### selectPriceList order -> ' + order);
        try
        {
            // Setting the pricelist of the head-equipment to the order
            if(orderItems != null && orderItems.size() > 0)
            {
                try
                {
                    for (SCboOrderItem item : orderItems)
                    {
                        if(item.orderItem.InstalledBase__r.Parent__c == null && item.orderItem.InstalledBase__r.PriceList__c != null)
                        {   
                            pl = [ select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c
                                   from SCPriceList__c 
                                   where id = :item.orderItem.InstalledBase__r.PriceList__c];
                                   
                                   System.debug('#### priceList 1');
                                   
                                   break;
                        }
                        if(item.orderItem.InstalledBase__r.Parent__c == null && item.orderItem.InstalledBase__r.PriceList__c == null)
                        {   
                            pl = null;
                            
                            System.debug('#### priceList 2');
                        }
                    }
                }
                catch (Exception e)
                {
                
                }
            }
            
            // a price list is assigned directly to the order
            if (order.PriceList__c <> null)
            {
                try
                {
                    pl = [select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c
                            from SCPriceList__c 
                           where id = :order.PriceList__c];
                           
                           System.debug('#### priceList 3');
                }
                catch (Exception e)
                {
                    // do nothing, pl will be still null
                }
            } // if (order.PriceList__c <> null)
            if ((null == pl) && (invoiceRecipient.Account__r.DefaultPriceList__c <> null))
            {
                try
                {
                    pl = [select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c
                            from SCPriceList__c 
                           where id = :invoiceRecipient.Account__r.DefaultPriceList__c];
                           
                           System.debug('#### priceList 4');
                }
                catch (Exception e)
                {
                    // do nothing, pl will be still null
                }
            } // if ((null == pl) && (invoiceRecipient.Account__r.DefaultPriceList__c <> null))
            if ((null == pl) && (null != invoiceRecipient.Account__r.BillingCountry__c) &&
                (null != order.Brand__c))
            {
                try
                {
                    pl = [select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c
                            from SCPriceList__c 
                           where Country__c = :invoiceRecipient.Account__r.BillingCountry__c 
                             and Brand__c   = :order.Brand__c];
                             
                             System.debug('#### priceList 5');
                }
                catch (Exception e)
                {
                    // do nothing, pl will be still null
                }
            } // if ((null == pl) && (null != invoiceRecipient.Account__r.BillingCountry__c) && ...
            if ((null == pl) && (null != invoiceRecipient.Account__r.BillingCountry__c))
            {
                try
                {
                    pl = [select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c
                            from SCPriceList__c 
                           where Country__c = :invoiceRecipient.Account__r.BillingCountry__c 
                             and Brand__c   = null];
                             
                             System.debug('#### priceList 6');
                }
                catch (Exception e)
                {
                    // do nothing, pl will be still null
                }
            } // if ((null == pl) && (null != invoiceRecipient.Account__r.BillingCountry__c))
            if (null == pl)
            {
                try
                {
                    pl = [select Id, Name, CurrencyIsoCode, Country__c, Brand__c, PriceList__c from SCPriceList__c 
                            where Name = :SCPriceCalculation.appSettings.DEFAULT_PRICELIST__c];
                            
                            System.debug('#### priceList 7');
                }
                catch (Exception e)
                {
                    // do nothing, pl will be still null
                }
            } // if (null == pl) 
        }
        catch (QueryException e)
        {
            System.debug(LoggingLevel.WARN, 'No valid price list found');
            throw new SCfwException(1002);
        }
        
        if (null == pl)
        {
            throw new SCfwException(1002);
        } // if (null == pl) 
        priceListCalculation = pl;

        System.debug('#### Found price list ->' + pl);
        return pl;
    }

    /**
     * The whole custom price calculation is done here.
     *
     * All steps to calculate the different prices and tax rates are
     * done here one after each other taking the manual settings into 
     * account.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param orderLine order line/service item to do the calculation for
     * @return order line/service item with all calculations done
     */
    public override SCOrderLine__c calculate(SCOrderLine__c orderLineOrig)
    {
        System.debug('[custom] initial orderLine -> ' + orderLineOrig);
        
        SCOrderLine__c orderLine = orderLineOrig;
                
        // check if a pricing date is already set or do it ourself otherwise
        if (orderLine.PricingDate__c == null)
        {
            orderLine.PricingDate__c = Date.today();
        }
        
        orderLine.ListPrice__c     = calculateListPrice(orderLine);
        
        if (orderLine.ManualUnitNetPrice__c == false)
        {
            orderLine.UnitPrice__c     = calculateUnitPrice(orderLine);
            orderLine.UnitNetPrice__c  = orderLine.UnitPrice__c;   
        }
        
        orderLine.PositionPrice__c = calculatePositionPrice(orderLine);

        // don't calculate the price in case it's set manually
        if (orderLine.ManualPricing__c == false)
        {
            orderLine.Price__c = calculatePrice(orderLine.PositionPrice__c,
                                                orderLine.InvoicingType__c);
        }

        orderLine = calculateDiscount(orderLine);

        orderLine = calculateVAT(orderLine);

        if (null == orderLine.PositionPrice__c)
        {
            orderLine.PositionPrice__c = 0;
        }
        if (null == orderLine.Price__c)
        {
            orderLine.Price__c = 0;
        }
        if (null == orderLine.UnitPrice__c)
        {
            orderLine.UnitPrice__c = 0;
        }
        if (null == orderLine.UnitNetPrice__c)
        {
            orderLine.UnitNetPrice__c = 0;
        }
        if (null == orderLine.ListPrice__c)
        {
            orderLine.ListPrice__c = 0;
        }
        
        return orderLine;
    }

    //FIXME This function used static article numbers for the  creation
    //      and needs to be refactored as soon as the concept is done.    
    /**
     * Automatically create and update lump sums for e.g. car allowance or
     * labour value.
     * 
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param assignmentId SFDC Id of the assignment to create the lump sums for
     * @param totalWorkingTime Total working time for this order
     * @param totalDistance Total driving distance for this order
     * @param boOrderLines Order lines that are currently assigned to the order
     * @return List of order line BOs containing the automatically created
     *         order lines for lump sums.
     */
    public List<SCboOrderLine> createLumpSums(Id assignmentId, 
                                              Decimal totalWorkingTime, 
                                              Decimal totalDistance,
                                              List<SCboOrderLine> boOrderLines)
    {
        // AutoposType__c
        // 56703 car allowance (899101)
        // 56701 labour value  (899102)
        List<SCboOrderLine> autoOrderLines;
        
        if (boOrderLines == null)
        {
            autoOrderLines = new List<SCboOrderLine>();
        }
        else
        {
            autoOrderLines = boOrderLines;
        }

        SCboOrderLine carAllowance;
        SCboOrderLine labourValue;
        
        // Denomination of working time
        Decimal workUnit = 15.0;

        if (autoOrderLines != null && autoOrderLines.size() > 0)
        {
            for (SCboOrderLine boOrderLine : autoOrderLines)
            {
                if (boOrderLine.orderLine.AutoPos__c == SCfwConstants.DOMVAL_AUTOPOSTYPE_DISTANCE)
                {
                    carAllowance = boOrderLine;
                }
                else if (boOrderLine.orderLine.AutoPos__c == SCfwConstants.DOMVAL_AUTOPOSTYPE_WORKING)
                {
                    labourValue = boOrderLine;
                }
                
                // No further looping needed since we're done with the lump sums
                if (carAllowance != null && labourValue != null)
                {
                    break;
                }
            }
            
        }

        // REMARK:
        // It might be the case that we create some objects in vain, because
        // they won't be needed later on
        // When refactoring this function during the full concept implementation
        // this might be a point for improvements.
        
        //ET 14.06.2012
        //add travel time orderitem only if custom setting ORDERFEEDBACK_ARTNO_TRAVELTIME is set and not disabled
        if (carAllowance == null && totalDistance > 0 && 
            SCBase.isSet(this.articleTravelTime) && this.articleTravelTime != 'disabled' )
        {
            carAllowance = new SCboOrderLine(order, null,
                                             null, assignmentId);
            SCboArticle carBoArticle = new SCboArticle();   
            carBoArticle.createByName(this.articleTravelTime, order.Brand__c);
            if(carBoArticle.article != null)
            {
                carAllowance.orderLine.Article__c = carBoArticle.article.Id;
                carAllowance.orderLine.Article__r = carBoArticle.article;
                carAllowance.orderLine.AutoPos__c = SCfwConstants.DOMVAL_AUTOPOSTYPE_DISTANCE; //'56703';
                
                autoOrderLines.add(carAllowance);
            }
        }
        //add work time orderitem only if custom setting ORDERFEEDBACK_ARTNO_WORKTIME is set and not disabled
        if (labourValue == null && totalWorkingTime > 0 && 
            SCBase.isSet(this.articleWorkTime) && this.articleWorkTime != 'disabled')
        {
            labourValue  = new SCboOrderLine(order, null,
                                             null, assignmentId);
            SCboArticle labourBoArticle = new SCboArticle();

            labourBoArticle.createByName(this.articleWorkTime, order.Brand__c);
            if(labourBoArticle.article != null)
            {
                labourValue.orderLine.Article__c = labourBoArticle.article.Id;
                labourValue.orderLine.Article__r = labourBoArticle.article;
                labourValue.orderLine.AutoPos__c = SCfwConstants.DOMVAL_AUTOPOSTYPE_WORKING; //'56701';
                
                autoOrderLines.add(labourValue);
            } 
        }


        // Very simple approach:
        // In case there is a driving distance we have to add a car allowance
        if (totalDistance > 0 && carAllowance != null)
        {
            carAllowance.orderLine = calculate(carAllowance.orderLine);
        }
        
        if (totalWorkingTime > 0 && labourValue != null)
        {
            // If totalTime < 15 and totalTime > 0 the Qty = 1
            Integer qty = 1;
            
            if (totalWorkingTime > workUnit)
            {
                Decimal labourUnits = totalWorkingTime / workUnit;
                qty = labourUnits.round(System.RoundingMode.HALF_UP).intValue();
            }
            


            labourValue.orderLine.Qty__c = qty;
            labourValue.orderLine  = calculate(labourValue.orderLine);           
        }
        
/*
        if (boOrderLines != null && boOrderLines.size() > 0)
        {
            return boOrderLines;
        }
        else
        {
*/
            return autoOrderLines;
/*
        }
*/
    }

    /**
     * Calculate the position price which is basically unit price times quantity
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param orderLine current order line
     * @return unit price
     */
    protected override Double calculatePositionPrice(SCOrderLine__c orderLine)
    {
        System.debug(LoggingLevel.FINE, '#####calculatePositionPrice() START');
        
        Double posPrice = null;
        
        if (orderLine.UnitNetPrice__c == null)
        {
            System.debug(LoggingLevel.Info, 'UnitNetPrice is null for ' + orderLine.Article__c);
            return 0;
        }
        
        if (orderLine.Qty__c == null)
        {
            System.debug(LoggingLevel.Info, 'Quantity is null for ' + orderLine.Article__c);
            return 0;
        }
        
        posPrice = orderLine.UnitNetPrice__c * orderLine.Qty__c;
        
        System.debug('posPrice -> ' + posPrice);
        System.debug(LoggingLevel.FINE, '#####calculatePositionPrice() END');
        
        return posPrice;
    }
}