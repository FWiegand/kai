/*
 * @(#)CCWCMaterialInventoryCreateTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
public class CCWCMaterialMovementCreateTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    static String MODE_ORDER = 'MODE_ORDER';
    static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
    static String MODE_PLANT = 'MODE_PLANT';
    static String MODE_STOCK = 'MODE_STOCK';

/*
    static testMethod void positiveTestCaseOrder1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_ORDER, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCaseOrder2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_ORDER, matStatus, matMoveType, valuationType);
    }       
*/
    static testMethod void negativeTestCaseOrder1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_ORDER, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseOrder2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_ORDER, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCasePlantAndStock1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCasePlantAndStock2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlantAndStock1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlantAndStock2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCasePlant1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCasePlant2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlant1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlant2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_PLANT, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCaseStock1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCaseStock2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseStock1()
    {
        String matStatus = '5408';
        String matMoveType = '5204'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseStock2()
    {
        String matStatus = '5407';
        String matMoveType = '5227'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static void positiveTestCaseOrder(String mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);
        SCOrder__c order = null;
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();
        if(mode == MODE_ORDER)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
            /*
            ValuationType = ZNEU - new spare part
                            ZGEBROW - used spare part without value
                            ZGEBRMW - used spare part with value
            */
            CCWCTestBase.createMaterialMovements(mmList, order.Id, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }
        else
        {
            CCWCTestBase.createMaterialMovements(mmList, null, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }   

        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        System.assertEquals(true, plantList.size() > 0);
        
        List<SCStock__c> stockListForName = [select name, id from SCStock__c where id = :stockID];
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            static String MODE_ORDER = 'MODE_ORDER';
            static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
            static String MODE_PLANT = 'MODE_PLANT';
            static String MODE_STOCK = 'MODE_STOCK';
        */
		Test.startTest();
        debug('before CCWCMaterialMovementCreate.callout');
        if(mode == MODE_ORDER)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(order.id, async, testMode);       
        }
        else if(mode == MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(plantName, stockName, modeOfReservation, async, testMode);        
        }
        else if(mode == MODE_PLANT)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(plantName, null, modeOfReservation, async, testMode);     
        }   
        else if(mode == MODE_STOCK)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(null, stockID, modeOfReservation, async, testMode);     
        }   
        
		Test.stopTest();		
		        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        System.assertEquals(mmList2.size(), 1);
        System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'MaterialMovement';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'MaterialDocument';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Material movement created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        debug('before CCWSGenericResponse.transmit(gr)');
        CCWSGenericResponse.transmit(gr);
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        System.assertEquals(mmList3.size(), 1);
        System.assertEquals('ok', mmList3[0].ERPStatus__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }

    static void negativeTestCaseOrder(String mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);
        SCOrder__c order = null;
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();
        if(mode == MODE_ORDER)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
            /*
            ValuationType = ZNEU - new spare part
                            ZGEBROW - used spare part without value
                            ZGEBRMW - used spare part with value
            */
			Test.startTest();
            CCWCTestBase.createMaterialMovements(mmList, order.Id, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }
        else
        {
        	Test.startTest();
            CCWCTestBase.createMaterialMovements(mmList, null, stockId, articleList,
                                                  valuationType, qty, matStatus, matMoveType);
        }   

        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        System.assertEquals(true, plantList.size() > 0);
        
        List<SCStock__c> stockListForName = [select name, id from SCStock__c where id = :stockID];
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            static String MODE_ORDER = 'MODE_ORDER';
            static String MODE_PLANT_STOCK = 'MODE_PLANT_STOCK';
            static String MODE_PLANT = 'MODE_PLANT';
            static String MODE_STOCK = 'MODE_STOCK';
        */
        if(mode == MODE_ORDER)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(order.id, async, testMode);       
        }
        else if(mode == MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(plantName, stockName, modeOfReservation, async, testMode);        
        }
        else if(mode == MODE_PLANT)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(plantName, null, modeOfReservation, async, testMode);     
        }   
        else if(mode == MODE_STOCK)
        {
            requestMessageID = CCWCMaterialMovementCreate.callout(null, stockID, modeOfReservation, async, testMode);     
        }   
        
        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        System.assertEquals(mmList2.size(), 1);
        System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'MaterialMovement';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'MaterialDocument';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Material movement not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);
        Test.stopTest();
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        System.assertEquals(mmList3.size(), 1);
        System.assertEquals('error', mmList3[0].ERPStatus__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }


/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}