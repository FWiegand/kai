/*
 * @(#)SCbtcMaterialMovement.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Wrapper class to book material movements (async, sync) that can be initiated
 * by the an external web service (e.g. from a button or external tool).
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcMaterialMovement implements Database.Batchable<sObject>
{
   /*
    * Synchronous booking of material movements. The method returns after
    * all records have been booked. Use this method if the number of records
    * is small (government limits). Use the asyncBookPending method instead.
    * @param mode ALL (id is ignored), 
    *             PLANT book all movements of stocks of a plant, 
    *             STOCK book only the movements of the stock
    * @param id   of the plant, stock or null
    */
    Webservice static void bookPending(String mode, String id)
    {
        if(mode == 'ALL')
        {
            // implement
        }
        else if(mode == 'PLANT')
        {
            // implement
        }
        else if(mode == 'STOCK')
        {
            // initialize the domain Material Movement type
            SCfwDomain domMatMove = new SCfwDomain('DOM_MATERIALMOVEMENT_TYPE');    
            SCboMaterialMovement boMatMove = new SCboMaterialMovement(domMatMove);
            boMatMove.bookMaterial(Id, null);
        }        
    } // bookPending
    
   /*
    * Asynchronous book material movements. The method returns immediately.
    * Use this method if you expect a large number of records and to avoid
    * governer limits.
    * @param mode ALL (id is ignored), 
    *             PLANT book all movements of stocks of a plant, 
    *             STOCK book only the movements of the stock
    * @param id   of the plant, stock or null
    * @return id of Apex Batch Job
    */
    Webservice static Id asyncBookPending(String mode, String id, String email)
    {
        SCbtcMaterialMovement btc = new SCbtcMaterialMovement (mode, id, email);
        return Database.executeBatch(btc);
    } // asyncBookPending
    
    //--<implementation>--------------------------------------------------------------
    global final String mode;
    global final String id;
    global final String email;
    global final String jobinfo;

    // internal variables    
    global final SCfwDomain domMatMove;
    
   /*
    * Use this constructor to initiate the batch apex 
    * @param mode  ALL (id is ignored), 
    *              PLANT book all movements of stocks of a plant, 
    *              STOCK book only the movements of the stock
    * @param id    of the plant, stock or null
    * @param email the notification e-mail that receives the completion notification
    *              if null, the e-mail of the user initiating the job is used.
    */
    global SCbtcMaterialMovement(String mode, String id, String email)
    {
        this.mode = mode;
        this.id = id;
        this.email = email;
        domMatMove = new SCfwDomain('DOM_MATERIALMOVEMENT_TYPE');    

        // Prepare job details that are used in the notification message
        String tmp = 'EPS Batch Job - Book Material Movements for ';
        if(mode == 'ALL')
        {
            tmp += 'all stocks';                
        }
        else if(mode == 'PLANT')
        {
            SCPlant__c plant = [select name from SCPlant__c where Id =:id];        
            tmp += 'all stocks of plant [' + plant.name + ']';                
        }
        else if(mode == 'STOCK')
        {
            SCStock__c stock = [select name, plant__r.name from SCStock__c where Id = :id];
            tmp += 'stock [' + stock.plant__r.name +'/' + stock.name + ']';
        }
        else 
        {
            tmp = 'warning: unknnown mode [' + mode + '] expected ALL, PLANT or STOCK - booking all records';
        }
        jobinfo = tmp;
    } // SCbtcMaterialMovement
    

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the material movements and the query locator.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        // select all material movements that are to be booked
        String query = 'select Id, Name, Stock__c, Stock__r.Name, Stock__r.ValuationType__c, Article__c, Article__r.Name, Type__c, Status__c, Qty__c from SCMaterialMovement__c where Status__c = \'' + SCfwConstants.DOMVAL_MATSTAT_BOOK + '\'';
        // if we only want to book the movements of a specific plant or stock, add the condition
        if(mode == 'PLANT')
        {
            query += ' and stock__r.plant__c = \'' + id + '\'';
        }
        else if(mode == 'STOCK')
        {
            query += ' and stock__c = \'' + id + '\'';
        }
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of material movements to process and book.
    * @param BC the batch context
    * @param scope the list of material movement records to be processed
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        SCboMaterialMovement boMatMove = new SCboMaterialMovement(domMatMove);
        boMatMove.bookMaterialMovements(scope);
    } // execute

        
   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global void finish(Database.BatchableContext BC)
    {
        // Get the ID of the AsyncApexJob representing this batch job and  
        // query the AsyncApexJob object to retrieve the current job's information.  
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  TotalJobItems, CreatedBy.Email  from AsyncApexJob where Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses;
        if(email == null)
        {
            toAddresses = new String[] {a.CreatedBy.Email};
        }
        else
        {
            toAddresses = new String[] {email};
        }
        
        mail.setToAddresses(toAddresses);
        mail.setSubject(jobinfo + ' with status[' + a.Status + ']');
        
        String text = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. \n';
        mail.setPlainTextBody(text);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    } // finish
}