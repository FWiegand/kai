/*
 * @(#)SCboArea.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Helper for evaluating postcode areas and assigned resources
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboArea
{
    public static String preferedEmployees { get; private set; }

   
   /*
    * Determines the engineers and the area assignment priority for a postcode.
    * @param country   the country to select SCAreaItem.country__c id (e.g. DE, GB, NL, ...)
    * @param postcode  the postcode to query in the SCAreaItem.name field 
    * @param dat       the date to use for reading the SCResourceAssignment (validfrom - validto)
    * @return a list of postcode area items and their area priority. The description field contains the engineer
    */
    public static List<SCAreaItem__c> readByPostcode(String country, String postcode, Date dat)
    {
        if(dat == null)
        {
            dat = Date.TODAY();
        }
    
        List<String> postcodes = new List<String>();
        postcodes.add(postcode);
        return readByPostcode(country, postcodes, dat);
    }

   /*
    * Determines the engineers and the area assignment priority for a postcode.
    * @param country   the country to select SCAreaItem.country__c id (e.g. DE, GB, NL, ...)
    * @param postcodes a list of postcode SCAreaItems 
    * @param dat       the date to use for reading the SCResourceAssignment (validfrom - validto)
    * @return a list of postcode area items and their area priority. The description field contains the engineer
    */
    public static List<SCAreaItem__c> readByPostcode(String country, List<String> postcodes, Date dat)
    {
        List<String> codes = new List<String>();
        for (String pcode :postcodes)
        {
            if (country.equals('GB'))
            {
                for (Integer i=pcode.length(); i>2; i--)
                {
                    codes.add(pcode.substring(0, i-1));
                }
            }
            else
            {
                codes.add(pcode);
            }
        }
        List<SCAreaItem__c> result = new List<SCAreaItem__c>();
        List<SCAreaItem__c> items = [select name, area__c, prio__c, description__c 
                                       from SCAreaItem__c 
                                      where country__c = :country 
                                        and name in :codes 
                                        and area__r.context__c = 'Resource' 
                                      order by prio__c];
        if (items != null)
        {
            // unfortunately SOQL has only a restricted support of joins - so we have to determine the resources manually
            
            // select the areas and read the resources from the resource assignment
            Set<String> areas = new Set<String>();
            for(SCAreaItem__c i : items)
            {
                areas.add(i.area__c);
            }
    
            Map<String, SCResourceAssignment__c> assignmentmap = new Map<String, SCResourceAssignment__c>(); 
            List<SCResourceAssignment__c> assignments = [select area__c, resource__c, resource__r.name, employee__c from SCResourceAssignment__c where area__c in :areas and validfrom__c <= :dat and validto__c >= :dat];
            for(SCResourceAssignment__c r : assignments)
            {
                assignmentmap.put(r.area__c, r);
            }
    
            System.debug('###area->' + areas);
            System.debug('###ass->' + assignments);
    
            System.debug('###map->' + assignmentmap);
    
            preferedEmployees = '';
            // now filter the items that have a fitting resourceassignment
            // fill the description field with the engineer name
            //fill string with prefered employee id's
            for(SCAreaItem__c i : items)
            {
                SCResourceAssignment__c r = assignmentmap.get(i.area__c);
                if(r != null)
                {
                    i.description__c = r.employee__c + ' (' + r.resource__r.name + ')';
                    preferedEmployees += r.resource__r.Id + ',';
                    result.add(i);
                }
            }
            if(preferedEmployees.length() > 0)
            {
                preferedEmployees = preferedEmployees.substring(0, preferedEmployees.length()-1);
            }

        }
        return result;    
     }

   /*
    * Determines the distance zone by country and postal code.
    * @param country   the country to select SCAreaItem.country__c id (e.g. DE, GB, NL, ...)
    * @param postcode  a postal code
    * @return the distance zone or null
    */
    public static String getDistanceZone(String country, String postcode)
    {
        String result = null;
        List<String> codes = new List<String>();
        
        if (country.equals('GB'))
        {
            for (Integer i=postcode.length(); i>2; i--)
            {
                codes.add(postcode.substring(0, i-1));
            }
        }
        else
        {
            codes.add(postcode);
        }

        List<SCAreaItem__c> items = [select Name, Area__c, Area__r.DistanceZone__c, Prio__c from SCAreaItem__c 
                                      where Country__c = :country 
                                        and Name in :codes 
                                        and Area__r.context__c = 'Resource' 
                                      order by Prio__c];
        if ((null != items) && (items.size() > 0))
        {
            result = items[0].Area__r.DistanceZone__c;
        }
        return result;    
     }
     
    /*
    * Returns a string of the engineers and the area assignment priority for a postcode.
    * @param items a list of items
    * @param maxitems  the maximum number of items (-1 for all)
    * @return a list of postcode area items and their area priority. The description field contains the engineer
    */
    public static String convert(List<SCAreaItem__c>items, Integer maxitems)
    {
        String result = '';
        integer i = 0;
        for(SCAreaItem__c item : items)
        {
            if(i > 0)
            {
                result += ' | ';
            }
            result += item.prio__c + ':' + item.description__c;

            i++;
            
            if(i >= maxitems)
            {
                break;
            }
        }
        return result;
    }
     
     
    /**
     * Determines the business unit based on this postcode / country
     * Only operational business unites are evaluated.
     * @param country  2-digit iso code (see SCAreaItem__c.Country__c)
     * @param pcode the postcode
     * @return a list of business units that match the postcode areas
     */
    public static List<SCBusinessUnit__c> getBusinessUnits(String country, String pcode)
    {
        List<String> codes = new List<String>();
        if (SCBase.isSet(country) && country.equals('GB'))
        {
            for (Integer i=pcode.length(); i>2; i--)
            {
                codes.add(pcode.substring(0, i-1));
            }
        }
        else
        {
            codes.add(pcode);
        }

        // read the business units which are conected to the areas 
        // which have an operational flag, the context 'business unit' 
        // and which belongs to the correct area items
        List<SCBusinessUnit__c> units = [select Id, name, type__c from SCBusinessUnit__c 
                                           where Operational__c = true 
                                             and Area__c in 
                                                 (select Area__c 
                                                    from SCAreaItem__c 
                                                   where Name in :codes 
                                                     and Country__c = :country 
                                                     and Area__r.Context__c = 'Business Unit') order by name];
        System.debug('###business areas->' + units);
        return units;                
    } // setBusinessUnits     


    /**
     * Determines the business unit based on this postcode / country
     * Only operational business unites are evaluated.
     * @param country  2-digit iso code (see SCAreaItem__c.Country__c)
     * @param pcode the postcode
     * @return the first business unit found or null if nothing is found
     */
    public static SCBusinessUnit__c getBusinessUnitFirst(String country, String pcode)
    {
        List<SCBusinessUnit__c> units = SCboArea.getBusinessUnits(country, pcode);
        if(units.size() > 0)
        {
            return units[0];
        }
        return null;     
    } // setBusinessUnits     
}