@isTest(SeeAllData = true)
private class CCWS_AddChatterFeedItemTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        //++++++++++++++ basic case +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        User testUser = [SELECT Id, Name, isActive, ID2__c FROM User WHERE isActive = true AND ID2__c !=:'' LIMIT 1];
 
 
	    Account testAccount = new Account();
	 
	    RecordType accountRecType= [SELECT Id, Name, SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Customer' LIMIT 1];
	    testAccount.RecordTypeId = accountRecType.Id;
	    
	    testAccount.Name = 'Max Mustermann';
	    testAccount.BillingCountry__c = 'DE';
	    testAccount.ID2__c = '12345678';
	    
	    insert testAccount;
	    testAccount = [SELECT Name, Id, ID2__c FROM Account WHERE Name = 'Max Mustermann' AND BillingCountry__c = 'DE' AND ID2__c = '12345678' LIMIT 1];
	    String teststring ='12345';
	    Blob testblob;
	  	CCWS_AddChatterFeeditem.addFeed('Account', '12345678', testUser.ID2__c, 'Test Body', 'testName', testblob);
	    
	   
	    
	    //+++++++++++++ special case for exceptions: null accounts ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	     Account testAccount2 = new Account();
	     
	     testAccount2.RecordTypeId = accountRecType.Id;
	    
	    testAccount2.Name = 'Max Müller';
	    testAccount2.BillingCountry__c = 'DE';
	    //testAccount2.ID2__c = null;
	    
	    insert testAccount2;
	    testAccount2 = [SELECT Name, Id, ID2__c FROM Account WHERE Name = 'Max Müller' AND BillingCountry__c = 'DE' LIMIT 1];
	    
	    
	    
	    //Task ourTask2 = [Select Subject, OwnerId From Task WHERE WhatId =: testAccount2.Id AND OwnerId =: testUser.Id LIMIT 1];
	    
	    //+++++++++++++ special case for exceptions: null user ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	    
	   User testUser2 = [SELECT Id, Name, isActive, ID2__c FROM User WHERE isActive = true AND ID2__c =: null LIMIT 1];
    }
}