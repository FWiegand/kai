/**
 * AseServiceManager.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseServiceManager is the main class which coordinates the 
 * callouts to the ASEWebservice 
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
public class AseServiceManager 
{
    
    //Examples:
    /*
    //First send the UserLogin for the SalesForce WebService. (The Login is hard 
    //coded in AseCalloutSCUserLogin at the moment.)
    AseServiceManager.setAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_USERLOGIN);
    
    //For ScheduleParameter
    AseServiceManager.removeAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_SCHEDULEPARA);
    AseServiceManager.setAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_SCHEDULEPARA);
    AseServiceManager.set(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_SCHEDULEPARA,'ams.dispo.showroutinginfo.user');
    AseServiceManager.printDebugGetAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_SCHEDULEPARA);

    //For Calendar
    AseServiceManager.removeAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_CALENDAR);
    AseServiceManager.setAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_CALENDAR);
    AseServiceManager.printDebugGetAll(AseCalloutConstants.TEST_TENANT,AseCalloutConstants.ASE_TYPE_CALENDAR);
    
    // Sample for appointments
    AseServiceManager.printDebugGetAll(UserInfo.getOrganizationID().substring(0, 15), AseCalloutConstants.ASE_TYPE_APPOINTMENT);
    System.debug(UserInfo.getOrganizationID().substring(0, 15));
    */    
    
    /**
     * removes all data for the tenant and dataType in the ASEWeb
     *
     * @param tenant the tenant fo which the data should be deleted
     * @param dataType the dataType which should be deleted
     */    
    public static void removeAll(String tenant, integer dataType)
    {
        integer[] dataTypes = new integer[0];
        dataTypes.add(dataType);
        removeAll(tenant,dataTypes);                
    }
    
    
    /**
     * removes all data for the tenant and the dataTypes in the ASEWeb
     *
     * @param tenant the tenant fo which the data should be deleted
     * @param dataTypes the dataTypes which should be deleted
     */    
    public static void removeAll(String tenant,integer[] dataTypes)
    {
        String[] strDataTypes = new String[0];
        for (integer i : dataTypes) 
        {
            AseCalloutUtils.getClassObject(i).addRemoveAllTypes(strDataTypes);
        }
        
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
        aseService.endpoint_x = AseCalloutConstants.ENDPOINT;
        aseService.removeAll(tenant, strDataTypes);
    }


    
    /**
     * makes a set()-callout to the ASEWeb for the given types with the
     * corresponding parameter.
     * For the most cases the data isn't send to the ASEWeb directly, but
     * the ASEWeb collects the data by performing an select on the
     * SalesForce Object over the PartnerAPI. 
     *
     * @param tenant the tenant for which the data should be set
     * @param dataTypesAndPara a Map with the dataType as Integer and a 
     *                         String-Array with the corresponding parameters
     */    
    public static void set(String tenant, Map<Integer,String[]> dataTypesAndPara)
    {
        AseService.aseDataType[] aseDataTypes = new AseService.aseDataType[0];

        for (integer i : dataTypesAndPara.keySet()) 
        {           
            AseCalloutUtils.getClassObject(i).addSetAseDataTypes(aseDataTypes,dataTypesAndPara.get(i) );
        }
        
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
        aseService.endpoint_x = AseCalloutConstants.ENDPOINT;
        aseService.set(tenant, aseDataTypes);
    }
    
    
    /**
     * makes a set()-callout to the ASEWeb for the given type with the
     * corresponding parameter.
     * For the most cases the data isn't send to the ASEWeb directly, but
     * the ASEWeb collects the data by performing an select on the
     * SalesForce Object over the PartnerAPI. 
     *
     * @param tenant the tenant for which the data should be set
     * @param dataType the dataType which should be send
     * @param parameter the key which should be set
     */    
    public static void set(String tenant, integer dataType, String parameter)
    {
        Map<Integer,String[]> dataTypesAndPara = new Map<Integer,String[]>();
        String[] tmpStr = new String[0];
        tmpStr.add(parameter);
        dataTypesAndPara.put(dataType, tmpStr);
        set(tenant, dataTypesAndPara);
    }


    /**
     * makes a set()-callout to the ASEWeb for the given type for 
     * the whole data.
     * For the most cases the data isn't send to the ASEWeb directly, but
     * the ASEWeb collects the data by performing an select on the
     * SalesForce Object over the PartnerAPI. 
     *
     * @param tenant the tenant for which the data should be set
     * @param dataType the dataType which should be send
     */    
    public static void setAll(String tenant, integer dataType)
    {
        integer[] tmpDataTypes = new integer[0];
        tmpDataTypes.add(dataType);
        setAll(tenant, tmpDataTypes);
    }
    
    /**
     * makes a set()-callout to the ASEWeb for the given types for 
     * the whole data.
     * For the most cases the data isn't send to the ASEWeb directly, but
     * the ASEWeb collects the data by performing an select on the
     * SalesForce Object over the PartnerAPI. 
     *
     * @param tenant the tenant for which the data should be set
     * @param dataTypes the dataTypes which should be send
     */    
    public static void setAll(String tenant, integer[] dataTypes)
    {
        Map<Integer,String[]> dataTypesAndPara = new Map<Integer,String[]>();
        for (integer i : dataTypes) 
        {
            dataTypesAndPara.put(i,new String[0]);          
        }
        set(tenant, dataTypesAndPara);
    }       


    /**
     * makes a getAll()-callout to the ASEWeb for the given type for 
     * the whole data. The result is printed on the DebugConsole afterwards.
     *
     * @param tenant the tenant for which the data should be set
     * @param dataType the dataType which should be send
     */    
    public static void printDebugGetAll(String tenant, integer dataType)
    {
        integer[] tmpDataTypes = new integer[0];
        tmpDataTypes.add(dataType);
        printDebugGetAll(tenant, tmpDataTypes);    
    }

    /**
     * makes a getAll()-callout to the ASEWeb for the given types for 
     * the whole data. The result is printed on the DebugConsole afterwards.
     *
     * @param tenant the tenant for which the data should be set
     * @param dataTypes the dataTypes which should be send
     */    
    public static void printDebugGetAll(String tenant, integer[] dataTypes)
    {
        integer[] tmpDataTypes = new integer[0];
        for (integer i : dataTypes) 
        {
            System.debug('### AseCalloutUtils.getClassObject(i) = ' + AseCalloutUtils.getClassObject(i));
            AseCalloutUtils.getClassObject(i).addPrintDebugGetAllTypes(tmpDataTypes);
        }

        //System.debug('++++'+tmpDataTypes);
        //AseCalloutTestService.testGetAll(tenant, tmpDataTypes);    
        //####gmsna - not supported any more as AseCalloutTestService has not the @isTest tag and is a private class
    }
        
}