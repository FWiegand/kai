/*
 * @(#)SCbtcContractRepairTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcContractVisitRepairTest
{
    static testMethod void testRepairContractVisit()
    {
        // To repair a visit we need a visit and its order to be created.
        // So we must first create a contract, its visits, then create an order
        // The visit must have a status = 'scheduled' and the order must have the 
        // status completed (5506), cancelled(5507) or closed final(5508) 
        // Only synchronized methods must be called because the asynchrone methods
        // could run in the random order.
        
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        Id id = (Id) contractID;
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);

        SCContract__c c = [Select Name, Id, Country__c from SCContract__c where Id = :contractID];
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);

        // Order Creation
        System.debug('###Test contractId: ' + contractID);
        System.debug('###Test c.Id: ' + c.Id);
        System.debug('###Test country: ' + c.Country__c);
        List<SCContractVisit__c> visitList = [Select Id, Status__c from SCContractVisit__c where Contract__c = :contractID];
        System.debug('###Test visitList: ' + visitList);
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            //break;
        } 
        update visitList;


        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
        List<SCOrder__c> orderList = [Select Id, Status__c from SCOrder__c where Contract__c = :c.Id];
        System.debug('###Test orderList: ' + orderList);
        Integer cnt = 0;
        for(SCOrder__c order: orderList)
        {
            cnt++;
            if(cnt == 1)
            {
                System.debug('###Test order status set from: ' + order.Status__c + ' to : 5506'); 
                order.Status__c = '5506';   
            }
            else
            {
                System.debug('###Test order status set from: ' + order.Status__c + ' to : 5507'); 
                order.Status__c = '5507';   
            }
            
        }
        update orderList;
        Test.StartTest();

        List<SCOrder__c> orderList2 = [Select Id, Status__c from SCOrder__c where Contract__c = :c.Id];
        System.debug('###Test oderList2 after update: ' + orderList2);

        List<SCContractVisit__c> visitList2 = [Select Id, Status__c from SCContractVisit__c where Contract__c = :contractID];
        for(SCContractVisit__c visit: visitList2)
        {
            visit.Status__c = 'scheduled';
        } 
        update visitList2;
        
        List<SCContractVisit__c> visitList3 = [ select ID, Contract__r.Country__c, Contract__r.ID, Status__c, Order__c, Order__r.Status__c  
                                                from SCContractVisit__c 
                                                where Contract__r.ID =  :contractID];
        System.debug('###Test visitList3 after update: ' + visitList3);
        
        // The visits 

        SCbtcContractVisitRepair.syncRepairContractVisits(id);
        String contractName = null;
        List<SCContract__c> cList = [Select Name from SCContract__c where Id = :id];
        if(cList.size() > 0)
        {
            contractName = cList[0].Name;
            SCbtcContractVisitRepair.repairContract(contractName, 'DE', 1, 'trace');
        }
        SCbtcContractVisitRepair.repairAll('DE', 1, 'trace');       
        Test.StopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCContract__c contract = new SCContract__c();
        Database.insert(contract);
        
        SCContractVisit__c cv = new SCContractVisit__c(Contract__c = contract.Id);
        Database.insert(cv);
        
        String strNull = null;
        
        
        Test.startTest();
        
        
        try { SCbtcContractVisitRepair.repairContract(contract.Id, 'de', 3, 'test'); } catch(Exception e) {}
        try { SCbtcContractVisitRepair.repairContract(strNull, 'de', 3, 'test'); } catch(Exception e) {}
        try { SCbtcContractVisitRepair.logBatchInternal(new List<SCInterfaceLog__c>(), 'test interfaceName', 'test interfaceHandler', null, null, 'test resultCode', 'test result', 'test date', Datetime.now(), 3); } catch(Exception e) {}
        
        
        Test.stopTest();
    }
    
    //@author GMSS
    static testMethod void testCodeCoverageB()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCContract__c contract = new SCContract__c();
        Database.insert(contract);
        
        SCContractVisit__c cv = new SCContractVisit__c(Contract__c = contract.Id);
        Database.insert(cv);
        
        String strNull = null;
        
        
        Test.startTest();
        
        
        SCbtcContractVisitRepair obj = new SCbtcContractVisitRepair(contract.Id, strNull, 3, 'test');
        
        try { obj.execute(null, new List<sObject>{contract}); } catch(Exception e) {}
        try { obj.repairContractVisit(cv, new List<SCInterfaceLog__c>()); } catch(Exception e) {}
        
        
        Test.stopTest();
    }



}