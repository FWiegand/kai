/**********************************************************************

Name: fsFA_PofS_ImageComponentController

======================================================

Purpose: 

This is the test class for fsFA_PofS_ImageComponentController

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/08/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

@isTest
private class fsFA_PofS_Test {
    
    static List< sObject > GL_FolderSettings;
    static List< sObject > GL_ClickAreaTypes;
    static List< sObject > GL_Assortments;
    
	static testMethod void myUnitTest() {
		//Create Custom Settings
        createCustomSettings();
        
        Test.startTest();
        
        //Create PofS
		
		PictureOfSuccess__c LO_PofS = new PictureOfSuccess__c(
			Bezeichnung__c					=	'Name',
			ValidFrom__c					=	Date.today(),
			ValidUntil__c					=	Date.today(),
			RedWeightingActivation__c		=	25,
			RedWeightingArea__c				=	25,
			RedWeightingAssortment__c		=	25,
			RedWeightingEquipment__c		=	25,
			Subtradechannel__c				=	'SubtradeChannel'
		);
		
		insert( LO_PofS );
                
        //Check PofS
        List< PictureOfSuccess__c > LL_TestPofSs = [ SELECT Id FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 1, LL_TestPofSs.size() );
		
        //Create PofS Controller
        ApexPages.StandardController LO_StandardController = new ApexPages.StandardController( LO_PofS );
        fsFA_PofS_Controller LO_PofSController = new fsFA_PofS_Controller( LO_StandardController );
        LO_PofSController.Dummy();
        LO_PofSController.refreshBackgrounds();
        
        //Getter/Setter
        LO_PofSController.setGO_DocumentController( LO_PofSController.getGO_DocumentController() );
        LO_PofSController.setGV_ImageId( LO_PofSController.getGV_ImageId() );
        LO_PofSController.setGV_NewImageName( LO_PofSController.getGV_NewImageName() );
        LO_PofSController.setGO_Image( LO_PofSController.getGO_Image() );
        LO_PofSController.setGO_PofSSettings( LO_PofSController.getGO_PofSSettings() );

        //Init PofS
		fsFA_PofS_ImageComponentController LO_ImageComponentController = new fsFA_PofS_ImageComponentController();
		LO_ImageComponentController.GV_ImageId = LO_PofS.Id;
		LO_ImageComponentController.init();
		
        //Create new ClickArea
		LO_ImageComponentController.drawNewArea();
		LO_ImageComponentController.GO_ImageArea.Type__c = 'Area';
		LO_ImageComponentController.GO_ImageArea.Name = 'Name';
		
        createNewQuestions(LO_ImageComponentController);

        //Change Question Order
        LO_ImageComponentController.GV_OldClickAreaQuestionRowNumber = 1;
        LO_ImageComponentController.GV_NewClickAreaQuestionRowNumber = 2;
        LO_ImageComponentController.changeQuestionOrder();
        
        //Save Area
		LO_ImageComponentController.saveArea();

        //Check ClickArea
        LL_TestPofSs = [ SELECT Id, ( SELECT Id FROM ClickAreas__r ) FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 1, LL_TestPofSs.get( 0 ).ClickAreas__r.size() );

        //Check Questions
        List< PofSClickarea__c > LL_TestClickAreas = [ SELECT Id, ( SELECT Id FROM RED_Survey_Questions__r ) FROM PofSClickarea__c WHERE Id =: LL_TestPofSs.get( 0 ).ClickAreas__r.get( 0 ).Id ];
        System.assertEquals( 3, LL_TestClickAreas.get( 0 ).RED_Survey_Questions__r.size() );

        //Choose Area
        LO_ImageComponentController.chooseArea();
        
        //Move Area
        LO_ImageComponentController.moveArea();
        
        //Cancel Area
        LO_ImageComponentController.cancelArea();
        
        //Choose Area
        LO_ImageComponentController.chooseArea();
        
        //Delete Question
        LO_ImageComponentController.GV_ClickAreaQuestionRowNumber = 2;
        LO_ImageComponentController.deleteQuestion();

        //Check Questions
        LL_TestClickAreas = [ SELECT Id, ( SELECT Id FROM RED_Survey_Questions__r ) FROM PofSClickarea__c WHERE Id =: LL_TestPofSs.get( 0 ).ClickAreas__r.get( 0 ).Id ];
        System.assertEquals( 2, LL_TestClickAreas.get( 0 ).RED_Survey_Questions__r.size() );
                
        //Change ClickArea Type
        LO_ImageComponentController.GO_ImageArea.Type__c = 'Assortment';
        LO_ImageComponentController.changeClickAreaType();
        
        //Create Assortments
        LO_ImageComponentController.GV_AssortmentProductId = GL_Assortments.get( 0 ).Id;
        LO_ImageComponentController.GV_AssortmentRowNumber = 1;
        LO_ImageComponentController.newAssortment();
        LO_ImageComponentController.newAssortment();
        LO_ImageComponentController.newAssortment();
		
        //Change Assortment Order
        LO_ImageComponentController.GV_OldAssortmentRowNumber = 1;
        LO_ImageComponentController.GV_NewAssortmentRowNumber = 2;
        LO_ImageComponentController.changeAssortmentOrder();

        //Save Area
		LO_ImageComponentController.saveArea();

        //Check Assortments
        LL_TestPofSs = [ SELECT Id, ( SELECT Id FROM Assortment__r ) FROM PictureOfSuccess__c WHERE Id =: LO_PofS.Id ];
        System.assertEquals( 3, LL_TestPofSs.get( 0 ).Assortment__r.size() );
        
        //Delete Assortment
        LO_ImageComponentController.GV_AssortmentRowNumber = 2;
        LO_ImageComponentController.deleteAssortment();
        
        //Getter/Setter
        LO_ImageComponentController.setGV_ImageId(LO_ImageComponentController.getGV_ImageId());
        LO_ImageComponentController.setGO_Image(LO_ImageComponentController.getGO_Image());
        LO_ImageComponentController.setGO_PofSSettings(LO_ImageComponentController.getGO_PofSSettings());
        LO_ImageComponentController.setGL_ClickAreas(LO_ImageComponentController.getGL_ClickAreas());
        LO_ImageComponentController.setGV_DrawNewArea(LO_ImageComponentController.getGV_DrawNewArea());
        LO_ImageComponentController.setGV_EditArea(LO_ImageComponentController.getGV_EditArea());
        LO_ImageComponentController.setGV_MoveArea(LO_ImageComponentController.getGV_MoveArea());
        LO_ImageComponentController.setGM_ClickAreaTypes(LO_ImageComponentController.getGM_ClickAreaTypes());
        LO_ImageComponentController.setGO_ImageArea(LO_ImageComponentController.getGO_ImageArea());
        LO_ImageComponentController.setGV_ImageAreaId(LO_ImageComponentController.getGV_ImageAreaId());
        LO_ImageComponentController.setGV_LastClickAreaType(LO_ImageComponentController.getGV_LastClickAreaType());
        LO_ImageComponentController.setGV_X(LO_ImageComponentController.getGV_X());
        LO_ImageComponentController.setGV_Y(LO_ImageComponentController.getGV_Y());
        LO_ImageComponentController.setGV_NewImageName(LO_ImageComponentController.getGV_NewImageName());
        LO_ImageComponentController.setGM_ClickAreaQuestions(LO_ImageComponentController.getGM_ClickAreaQuestions());
        LO_ImageComponentController.setGV_RemainingQuestions(LO_ImageComponentController.getGV_RemainingQuestions());
        LO_ImageComponentController.setGV_ClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_ClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_OldClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_OldClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_NewClickAreaQuestionRowNumber(LO_ImageComponentController.getGV_NewClickAreaQuestionRowNumber());
        LO_ImageComponentController.setGV_AssortmentsCount(LO_ImageComponentController.getGV_AssortmentsCount());
        LO_ImageComponentController.setGV_AssortmentRowNumber(LO_ImageComponentController.getGV_AssortmentRowNumber());
        LO_ImageComponentController.setGV_OldAssortmentRowNumber(LO_ImageComponentController.getGV_OldAssortmentRowNumber());
        LO_ImageComponentController.setGV_NewAssortmentRowNumber(LO_ImageComponentController.getGV_NewAssortmentRowNumber());
        LO_ImageComponentController.setGM_AssortmentProducts(LO_ImageComponentController.getGM_AssortmentProducts());
        LO_ImageComponentController.setGV_AssortmentProductId(LO_ImageComponentController.getGV_AssortmentProductId());
        LO_ImageComponentController.setGL_FilterFields(LO_ImageComponentController.getGL_FilterFields());
        LO_ImageComponentController.setGM_AssortmentProductFilters(LO_ImageComponentController.getGM_AssortmentProductFilters());
        LO_ImageComponentController.setGM_AssortmentProductFilterValues(LO_ImageComponentController.getGM_AssortmentProductFilterValues());
        LO_ImageComponentController.setGM_AssortmentProductFilterLabels(LO_ImageComponentController.getGM_AssortmentProductFilterLabels());
        LO_ImageComponentController.setGO_DocumentController(LO_ImageComponentController.getGO_DocumentController());
        
        LO_ImageComponentController.getGO_DocumentController().setGV_DocumentURL(LO_ImageComponentController.getGO_DocumentController().getGV_DocumentURL());
        LO_ImageComponentController.getGO_DocumentController().setGO_UploadFile(LO_ImageComponentController.getGO_DocumentController().getGO_UploadFile());
        LO_ImageComponentController.getGO_DocumentController().setGL_Backgrounds(LO_ImageComponentController.getGO_DocumentController().getGL_Backgrounds());
        LO_ImageComponentController.getGO_DocumentController().setGL_ClickAreaIcons(LO_ImageComponentController.getGO_DocumentController().getGL_ClickAreaIcons());
        LO_ImageComponentController.getGO_DocumentController().setGL_ClickAreaQuestions(LO_ImageComponentController.getGO_DocumentController().getGL_ClickAreaQuestions());
            
        Test.stopTest();
		
	}
    
    public static void createCustomSettings(){
        GL_FolderSettings = Test.loadData( PofSDocumentsFolder__c.sObjectType, 'PofS_TestDataDocumentFolders' );
        GL_ClickAreaTypes = Test.loadData( PofSClickAreaTypes__c.sObjectType, 'PofS_TestDataClickAreaTypes' );
        GL_Assortments = Test.loadData( ArticleGroup__c.sObjectType, 'PofS_TestDataArticleGroups' );
    }
    
    private static void createNewQuestions(fsFA_PofS_ImageComponentController LO_ImageComponentController) {
    	LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 1 ).put( 'Potential_Hint__c', 'Hint' );
        LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 2 ).put( 'Potential_Hint__c', 'Hint' );
        LO_ImageComponentController.newQuestion();
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Name', 'Name' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Type__c', 'Type' );
		LO_ImageComponentController.GM_ClickAreaQuestions.get( 3 ).put( 'Potential_Hint__c', 'Hint' );        
    }
}