/*
 * @(#)SCOrderCloseController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * This controller closes an order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderCloseController
{
    public SCOrder__c tmpOrder { get; set; }
    private String oid;
    public String message { get; set; }
    public SCOrder__c currentOrder { get; set; }
    public Boolean closeable { get; set; }

    /**
     * Constructor
     */
    public SCOrderCloseController()
    {
        tmpOrder = new SCOrder__c();
        message = '';
        
        init();
        
        closeable = validate();
        
        /*if(!closeable)
        {
        	//ApexPages.Message msg; 
            //msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Auftrag hat DummyEqipment. \n Aufträge ohne echtes Equipment dürfen nicht abgeschlossen werden.');
            //ApexPages.addMessage(msg);
            message = 'Der Auftrag hat kein echtes Equipment. \n Aufträge ohne echtes Equipment dürfen nicht abgeschlossen werden.';
        }*/
        
    }
    
    public void init()
    {
        if ( ApexPages.currentPage().getParameters().containsKey('oid') && ApexPages.currentPage().getParameters().get('oid') != '' )
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
            message = '';
            currentOrder = [ Select id, name from scorder__c where id = :oid ]; 
        }
        else
        {
            oid = null;
            message = 'Please select an order first!';
            currentOrder = new SCOrder__c();
        }
    }
    
    /*
    * Validate functions test if an order has a dummy installedBase.
    * CCEs SAP does not allow to close an order without correct installedBase.
    * Returns true of no dummy EQ is on the order and false if the order has an dummy EQ
    *
    * ET: a ZC16 Order should have one Equipment and one EquipmentNew
    */
    private Boolean validate()
    {
        if ( oid != null )
        {
            /*List<SCOrderItem__c> items = [SELECT Id,InstalledBase__c,InstalledBase__r.article__c, Order__c 
            	FROM SCOrderItem__c where Order__c =: oid];
            */
            
            SCOrder__c ord = 
            [
            	SELECT 
            		Id,
            		Type__c,
            		(
            			SELECT 
            				InstalledBase__c, 
            				InstalledBase__r.Article__c,
            				RecordType.DeveloperName 
            			FROM 
            				OrderItem__r
            		) 
            	FROM 
            		SCOrder__c 
            	WHERE 
            		Id =: oid
            ];
            
            List<SCOrderItem__c> items = ord.OrderItem__r;
            
            //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
            if(ord.Type__c == SCfwConstants.DOMVAL_ORDERTYPE_REPLACEMENT)
            {
            	Boolean eq = false;
	            Boolean eqNew = false;
	            for(SCOrderItem__c item : items)
	            {
	            	if(item.RecordType.DeveloperName == 'Equipment')
	            	{
	            		eq = true;
	            	}
	            	else if(item.RecordType.DeveloperName == 'EquipmentNew')
	            	{
	            		eqNew = true;
	            	}
	            }
	            
	            if(!eq)
	            {
	            	message = 'Der Auftrag enthält kein Altgerät.';
	            }
	            else if(!eqNew)
	            {
	            	message = 'Der Auftrag enthält kein neues Gerät.';
	            }
	             
	            
	            return eq && eqNew;
            }	
            	
            if(items.size() == 1)
            {
            	// Test if an dummy EQ is on the order
            	// Dummy EQs do not have an article on the installedBase
            	if(items[0].InstalledBase__r.article__c == null)
            	{
            		message = 'Der Auftrag hat kein echtes Equipment. \n Aufträge ohne echtes Equipment dürfen nicht abgeschlossen werden.';
            		return false; 
            	}
            }
        }

        return true;
        

    }
    
    
    
    public PageReference close()
    {
        if(closeable)
        {
        	// cancel the order
	        SCboOrder boOrder = new SCboOrder();
	        PageReference p = new PageReference('/' + oid);
	        String closeOrder = boOrder.closeOrder(oid, tmpOrder.description__c);
	        
	        if(closeOrder == 'OK')
	        {
	            return p;
	        }
	        else
	        {
	            message = closeOrder;
	            return null;
	        }
        }
        else
        {
        	return null;
        }
    }
    
    public PageReference back()
    {
    	return new PageReference('/' + oid);
    }
}