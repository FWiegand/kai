/*
 * @(#)SCboOrderQualificationTest.cls 
 *  
 * Copyright 2012 by GMS Development GmbH 
 * Karl-Schurz-Strasse 29,  33100 Paderborn 
 * All rights reserved. 
 * 
 * This software is the confidential and proprietary information 
 * of GMS Development GmbH. ("Confidential Information").  You 
 * shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement 
 * you entered into with GMS. 
 */ 
 
/** 
 * 
 * @author Alexander Wagner <awagner@gms-online.de> 
 * @version $Revision$, $Date$ */ 
@isTest 
private class SCboOrderQualificationTest
{
    private static testMethod void testBasic()
    {
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        SCboOrderQualification quali1 = new SCboOrderQualification();
        quali1.init(SCHelperTestClass2.orderQualification.Id, SCHelperTestClass.orderItem.Id, SCHelperTestClass.order.Id);
        
        SCboOrderQualification quali2 = new SCboOrderQualification(SCHelperTestClass2.orderQualification);
    }
} // SCboOrderQualificationTest