/*
 * @(#)SCOrderRebookExtensionTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderRebookExtensionTest
{   
    static testMethod void testOrderRebook()
    {   
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        SCHelperTestClass.createOrderTestSet2(true);
        SCHelperTestClass.createOrderRoles(true);
        
        Test.startTest();
        
        // try to create a rebook for an open order
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCOrderRebookExtension rebookExt = new SCOrderRebookExtension(controller);
        System.assertEquals(true, rebookExt.canRebook);
        System.assertEquals(false, rebookExt.mustCopy);
        
        PageReference page = rebookExt.onContinue();
        System.assertNotEquals(null, page);
        System.assertEquals(true, page.getUrl().contains(appSettings.ORDERCREATION_PAGE__c));
        
        // return page should contains all three parameter
        Map<String, String> paramMap = page.getParameters();
        System.assertNotEquals(null, paramMap.get('aid'));
        System.assertNotEquals(null, paramMap.get('oid'));
        System.assertNotEquals(null, paramMap.get('rebook'));
        
        // the rebook order is the same as the original order
        SCOrder__c orgOrder = [Select Id, Name from SCOrder__c where Id = :rebookExt.boOrder.order.Id];
        SCOrder__c rebOrder = [Select Id, Name from SCOrder__c where Id = :paramMap.get('oid')];
        System.assertEquals(orgOrder.Name, rebOrder.Name);
        
        
        // delete the order item and test again (test for no errors on sales orders)
        SCOrderItem__c ordItem = new SCOrderItem__c(Id = SCHelperTestClass.orderItem.Id);
        delete ordItem;
       
        rebookExt = new SCOrderRebookExtension(controller);

        Test.stopTest();
    }

    static testMethod void testOrderRebookWithCopy()
    {   
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        SCHelperTestClass.createOrderTestSet2(true);
        SCHelperTestClass.createOrderRoles(true);
        
        
        
        
        SCOrder__c order = new SCOrder__c(Id = SCHelperTestClass.order.Id, 
                                          Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED);
        update order;

        Test.startTest();
        
        // try to create a rebook for a completed order
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.order);
        SCOrderRebookExtension rebookExt = new SCOrderRebookExtension(controller);
        System.assertEquals(true, rebookExt.canRebook);
        System.assertEquals(true, rebookExt.mustCopy);
        
        PageReference page = rebookExt.onContinue();
        System.assertNotEquals(null, page);
        System.assertEquals(true, page.getUrl().contains(appSettings.ORDERCREATION_PAGE__c));
        
        // return page should contains all three parameter
        Map<String, String> paramMap = page.getParameters();
        System.assertNotEquals(null, paramMap.get('aid'));
        System.assertNotEquals(null, paramMap.get('oid'));
        System.assertNotEquals(null, paramMap.get('rebook'));
        
        // the rebook order is not the same as the original order
        SCOrder__c orgOrder = [Select Id, Name from SCOrder__c where Id = :rebookExt.boOrder.order.Id];
        SCOrder__c rebOrder = [Select Id, Name, OrderOrigin__c from SCOrder__c where Id = :paramMap.get('oid')];
        System.assertNotEquals(orgOrder.Name, rebOrder.Name);
        
        
        System.assertEquals(SCHelperTestClass.order.Id, rebOrder.OrderOrigin__c);
        
        Test.stopTest();
    }
}