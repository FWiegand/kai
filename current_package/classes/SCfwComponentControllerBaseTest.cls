/*
 * @(#)SCfwComponentControllerBaseTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Simple test implementation
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwComponentControllerBaseTest 
{
    static testMethod void test() 
    {
        SCfwPageControllerBase     pcon = new SCfwPageControllerBase();
        SCfwComponentControllerBase con = new SCfwComponentControllerBase ();
        con.pageController = pcon;
        con.key = 'test';
        con.OnUpdateData('ctx', null);
    }
}