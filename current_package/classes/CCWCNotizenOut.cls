/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	The class exports an Salesforce FeedItem item to SAP
*
* @date			03.12.2013
*
* Test: 
* CCWCNotizenOut send = new CCWCNotizenOut();
* List<FeedItem> feeds = [Select Title, ParentId, Id, CreatedById, Body From FeedItem where id = '0D5c0000007WBXZCA4'] ;
* send.sendCallout(feeds, false, true);
*/

public without sharing class CCWCNotizenOut extends SCInterfaceExportBase{
	
	private static String OutletNr;
	private static String OwnerId2;
	private static List<FeedItem> FeedItems;

    public CCWCNotizenOut()
    {
    }
    
    public CCWCNotizenOut(String accId, String usrId)
    {
    	
    }    
    
    public CCWCNotizenOut(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    public CCWCNotizenOut(String interfaceName, String interfaceHandler, String refType, String accId, String usrId, List<FeedItem> feeds)
    {
    	OutletNr = accId;
    	OwnerId2 = usrId;
    	FeedItems = feeds;
    }
   
    /**
    * Validation for trigger
    *
    * @param newFeedItems		Trigger content
    * @param async				if asynchron = true
    * @param test				if true: send request
    							if false: Test
	*
	*/    							
    public void sendCallout(FeedItem [] newFeedItems, boolean async, boolean test){   	
    	String accId;
		String usrId;
		Set<Id> accIds = new Set<Id>();
		Set<Id> usrIds = new Set<Id>();	
		Set<Id> feedIds = new Set<Id>();
		for(FeedItem feet : newFeedItems){
			usrIds.add(feet.CreatedById);
			accIds.add(feet.ParentId);
			feedIds.add(feet.Id);
		}			
		List<Account> accounts = [SELECT Id, ID2__c FROM Account WHERE Id =: accIds];				
  		List<User> users = [SELECT Id, ID2__c FROM User WHERE Id =: usrIds];  	
  		List<FeedItem> items = [Select Title, Id, ParentId, CreatedBy.FirstName, CreatedBy.LastName, CreatedById, Body From FeedItem where Id =:feedIds];
  		debug('** DATA: ' + items + accounts + users);	
		for(FeedItem feedI : items){
			if (feedI != null){
				for(Account acc: accounts){
					if(acc != null){
						for(User usr: users){
							if(usr != null){
								if(acc.Id == feedI.ParentId && usr.Id == feedI.CreatedById){
									accId = acc.ID2__c;
									usrId = usr.ID2__c;
									debug('INPUT: feedId:' + feedI.Id + ', OutletNr: ' + accId +  ', OwnerId2: ' + usrId+ ', Async: ' + async + ', Test: ' + test);
									CCWCNotizenOut.callout(feedI.Id, async, test, accId, usrId, items);
								}
							}else{
								if(acc.Id == feedI.ParentId){
									accId = acc.ID2__c;
									usrId = null;
									debug('INPUT: feedId:' + feedI.Id + ', OutletNr: ' + accId +  ', OwnerId2: ' + usrId+ ', Async: ' + async + ', Test: ' + test);
									CCWCNotizenOut.callout(feedI.Id, async, test, accId, usrId, items);									
								}else{
									debug('Account ' + acc.Id + ' do not match!');
								}
							}
						}
					}
				}
			}
		}
    }
       
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      FeedItem id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @param accId Outletnummer
     * @param usrId User ID2
     * @param items ItemFeed
     */
    public static String callout(String ibid, boolean async, boolean testMode, String accId, String usrId, List<FeedItem> items)
    {
        String interfaceName = 'CustomerNote';
        String interfaceHandler = 'CCWCNotizenOut';
        String refType = 'FeedItem';
        CCWCNotizenOut note = new CCWCNotizenOut(interfaceName, interfaceHandler, refType, accId, usrId, items);
        return note.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCNotizenOut');
        
    }  

     /**
     * Reads an FeedItem to send
     *
     * @param noteID
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String noteID, Map<String, Object> retValue, Boolean testMode)
    {
    	//debug('Check Data**' + OutletNr + '**' + OwnerId2 + '**' + FeedItems);
    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        List<FeedItem> items = new List<FeedItem>();
        for (FeedItem item : FeedItems){
        	if (item.Id == noteID) items.add(item);
        }
        if(items.size() > 0)
        {
            retValue.put('ROOT', items[0]);
            debug('FeedItem: ' + retValue);
        }
        else
        {
            String msg = 'The FeedItem with id: ' + noteID + ' could not be found!';
            debug(msg);
            throw new SCfwException(msg);
        }
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the FeedItem under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        FeedItem note = (FeedItem)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdc_NotizenOut.HTTPS_Port ws = new piCceagDeSfdc_NotizenOut.HTTPS_Port();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('NotizenOut');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        try
        {
            // instantiate the body of the call
            piCceagDeSfdc_NotizenOut.item_element sfdcItems = new piCceagDeSfdc_NotizenOut.item_element ();   
            List<piCceagDeSfdc_NotizenOut.item_element> noteItmes = new List<piCceagDeSfdc_NotizenOut.item_element>(); 
            piCceagDeSfdc_NotizenOut.parameters_element sfdcParams = new piCceagDeSfdc_NotizenOut.parameters_element ();
            List<piCceagDeSfdc_NotizenOut.parameters_element> noteParams = new List<piCceagDeSfdc_NotizenOut.parameters_element> ();            
            piCceagDeSfdc_NotizenOut.parameter_element sfdcParam = new piCceagDeSfdc_NotizenOut.parameter_element ();
            List<piCceagDeSfdc_NotizenOut.parameter_element> noteParam = new List<piCceagDeSfdc_NotizenOut.parameter_element> ();

            noteItmes.add(sfdcItems); 
            noteParams.add(sfdcParams);      
            noteParam.add(sfdcParam);   
            
            // set the data to the body
            setNoteData(sfdcItems, sfdcParams, sfdcParam, note, u, testMode);
            
            String jsonsfdcItems = JSON.serialize(sfdcItems);
            String fromJSONMapNote = getDataFromJSON(jsonsfdcItems);
            debug('from json Note Item: ' + fromJSONMapNote);
            
            String jsonsfdcParams = JSON.serialize(sfdcParams);
            String fromJSONMapParams = getDataFromJSON(jsonsfdcParams);
            debug('from json FeedItem Params: ' + fromJSONMapParams);
            
            String jsonsfdcParam = JSON.serialize(sfdcParam);
            String fromJSONMapParam = getDataFromJSON(jsonsfdcParams);
            debug('from json FeedItem Param: ' + fromJSONMapParam);
            
            
                        
            rs.message = '\n messageData: ' + sfdcItems + sfdcParams;
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.Notizen_Out: \n\n';
                    rs.setCounter(1);
                    ws.Notizen_Out(noteItmes, noteParams);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
        return retValue;
    }
   
     /**
     * sets the call out Note structure with data form an Note
     *
     * @param cso callout Note structure
     * @param par callout parameter structure     
     * @param noteEntry with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
    public String setNoteData(piCceagDeSfdc_NotizenOut.item_element cso, piCceagDeSfdc_NotizenOut.parameters_element par, piCceagDeSfdc_NotizenOut.parameter_element pa, FeedItem noteEntry, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
        String step = '';
        
        //debug('**' + OutletNr + '**' + OwnerId2);
                
        try 
        {
            step = 'set Subject = noteEntry.Title';
            cso.Subject = noteEntry.Title;
                        
            step = 'set Description = noteEntry.Body';
            cso.Description = noteEntry.Body;
            
            step = 'set OutletNumber = OutletNr';
            cso.OutletNumber = OutletNr;
            
            step = 'set OwnerId = OwnerId2';
            cso.OwnerId = OwnerId2;            
            
            step = 'set UserName = User.FirstName User.LastName';
            
            if(noteEntry.CreatedBy.FirstName != null){
            	cso.UserName = noteEntry.CreatedBy.FirstName + ' ' + noteEntry.CreatedBy.LastName;
            }else{
            	cso.UserName = noteEntry.CreatedBy.LastName;
            }
            
            step = 'set parameter = URL.getSalesforceBaseUrl().toExternalForm() / noteEntry.Id';               
            pa.parameter = URL.getSalesforceBaseUrl().toExternalForm() + '/' + noteEntry.Id;
            
            //piCceagDeSfdc_NotizenOut.parameter_element sfdcParam = new piCceagDeSfdc_NotizenOut.parameter_element ();            
            step = 'set parameter attribute = SF_CustomerNoteUR';
            pa.qualifier = 'SF_CustomerNoteURL';
            par.parameter = pa;
            
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setFeedItem: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }

   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}