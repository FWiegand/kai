/*
 * @(#)SCAddressServiceImplNL.cls 
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the address validation service that accesses 
 * the webservices.nl. This service is activated
 * depending on the country by SCAddressValidation.
 *
 *    SCAddressValidation v = new SCAddressValidation();
 *    List<AvsAddress> foundaddr;
 *    foundaddr = v.check('nl', 'Amsterdam Grasweg 2');
 *    System.debug('result:' + foundaddr);
*/
public virtual class SCAddressServiceImplNL implements SCAddressService
{
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    /*
     * Prepare the web servcie call by filling the login header and
     * @return instance of the web service wrapper
     */
    public WSWebserviceNL.Webservices_nlPort prepare()
    {
        // instantiate the web service          
        WSWebserviceNL.Webservices_nlPort service = new WSWebserviceNL.Webservices_nlPort();
        
        // prepare the authentication details (we are using header authentication)          
        WSWebserviceNL.HeaderLoginType login = new WSWebserviceNL.HeaderLoginType();
        //Test system   
//        login.username = 'GMSDE_User';
//        login.password = '9367d6ff0deb5e8e40d1607a6545a336';
        //Praxis System
//         <soap:username>GMSepsnl_User</soap:username>
//         <soap:password>aef8de2ebcca828cc06d7a3bd14efa7e</soap:password>
        
        login.username = appSettings.ADDRESS_SERVICE_WSNL_U__c;
        login.password = appSettings.ADDRESS_SERVICE_WSNL_PW__c;

        service.HeaderLogin = login;
        
        return service;
    } // prepare


    /**
     * Validate the address and return the result set
     * 
     * @param country country to search in
     * @param query search for the address string
     */
    public virtual AvsResult check(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            // prepare the authentication details (we are using header authentication)          
            WSWebserviceNL.Webservices_nlPort wsnl = prepare();
            
            // call the web service
            
            //-<Parameters>-------------------------------------------------------------------
            // organization res Name of the company or organisation at the address 
            // building     res Building or subbuilding name 
            // street           Street search phrase 
            // housenr          House number search phrase 
            // pobox        res PO box search phrase 
            // locality         District or municipality search phrase 
            // postcode         Postalcode search phrase 
            // province         Province search phrase 
            // country          Country of the address, required.  Accepts ISO2 / ISO3 country codes. 
            // language         Language in which the results are returned ('latin') 
            // country_format   The format in which the country is returned ('iso_2')
            //-<Return>-----------------------------------------------------------------------
            // validation_status 
            // result  - an array of InternationalV2 entries. 
            WSWebserviceNL.InternationalAddressSearchV2Result wsnlResult;
            if(addr.matchInfo != null && addr.matchInfo == 'test')
            {
                // only for automated testing of the service
                wsnlResult = emulateCheck(addr);
            }
            else
            {
                // call the web service 
                wsnlResult = wsnl.internationalAddressSearchV2('', '', addr.street, addr.housenumber, '', addr.city, addr.postalcode, addr.county, addr.country, 'latin', 'iso_2');
            }
            // Are there any results?
            if (wsnlResult.result.item == null)
            {
                result.status = AvsResult.STATUS_EMPTY;
                result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                System.Label.SC_app_AddressValidationEmpty);
                ApexPages.addMessage(myMsg);

                return result;
            }
            // evaluate the results                 
            for(WSWebserviceNL.InternationalV2 i : wsnlResult.result.item)
            {
                WSWebserviceNL.InternationalFormattedAddress wsnlAddress = i.address;

                // transfer the result into the return object
                AvsAddress item = new AvsAddress();
                item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                item.countryState   = ''; // not supported by webservices.nl
                item.county         = wsnlAddress.province;         
                item.postalcode     = wsnlAddress.postcode; 
                item.city           = wsnlAddress.locality;
                item.district       = ''; // not supported by webservices.nl
                item.street         = wsnlAddress.street;
                item.housenumber    = wsnlAddress.housenr;
                item.nofrom         = ''; // not supported by webservices.nl
                item.noto           = ''; // not supported by webservices.nl
                item.geoX           = 0;  // not supported by webservices.nl 
                item.geoY           = 0;  // not supported by webservices.nl 

                // Convert the status codes (for diagnosis only - can be removed in product environment)  
                item.matchInfo = convertStatus(i.element_match_status, i.element_result_status);
                
                // add the found address to the result list
                result.items.add(item);
            } // for..

            // now set the status values
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
            // set the originial status info of the service
            result.statusInfo = convertValidationStatus(wsnlResult.validation_status); 
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e;
        }
        return result;
    } // check
    
    

    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public virtual  AvsResult geocode(AvsAddress addr)
    {
        addr.geox = 0;
        addr.geoy = 0;
        Integer houseno = 0;

        AvsResult result = new AvsResult();
        try
        {
            // prepare the authentication details (we are using header authentication)          
            WSWebserviceNL.Webservices_nlPort wsnl = prepare();
            
            // call the web service
            
            
            // this service supports only numerical house numbers
            houseno = calcHouseno(addr.housenumber);     
            
            // remove blanks that are not relevant here
            String inputPlCode = '';
            inputPlCode = addr.postalcode;
            if(inputPlCode != null)
            {
                inputPlCode.trim();
                inputPlCode = inputPlCode.replace(' ', '');
            }

            boolean bSearchAgain = true;
            if(inputPlCode != null && inputPlCode.length() > 1 && addr.matchInfo != 'test')
            {
                try
                {
                    //  -<Parameters>--------------------------------------------
                    //  postcode Postcode to find the location of (postcode format varies depending on the country specified) 
                    //  country Country of the address.  Country can be specified by using ISO3 country codes (recommended).  Complete country names may not always work. 

                    //  -<Returns>-----------------------------------------------
                    //  coordinates A LatLonCoordinates entry. 

                    //Postcode search
                    WSWebserviceNL.LatLonCoordinates wsnlResultPostcode;
                    wsnlResultPostcode = wsnl.geoLocationInternationalPostcodeCoordinatesLatLon(inputPlCode, addr.Country);

                    // transfer the result into the return object
                    AvsAddress item = new AvsAddress();
                    item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                    item.postalcode     = addr.postalcode; 
                    item.geoX           = wsnlResultPostcode.longitude;
                    item.geoY           = wsnlResultPostcode.latitude;
                    if(item.geoX != null && item.geoX != 0)
                    {
                        item.matchInfo = 'geoLocationInternationalPostcodeCoordinatesLatLon : Geocoded';
                    }
                    // add the found address to the result list
                    result.items.add(item);
                    bSearchAgain = false;        
                }
                catch (Exception e)
                {
                    bSearchAgain = true;        
                }
            }//if( (addr.postalcode != null && addr.postalcode.length() > 1 )
                        
            if(bSearchAgain)                                
            {
                //-<Parameters>-------------------------------------------------------------------
                // street           Street search phrase 
                // houseno          House number search phrase 
                // city             the town/city
                // province         Province search phrase 
                // country          Country of the address, required.  Accepts ISO2 / ISO3 country codes. 
                // language         Language in which the results are returned ('latin') 
                //-<Return>-----------------------------------------------------------------------
                // result  - an array of InternationalV2 entries. 

                WSWebserviceNL.LatLonCoordinatesInternationalAddressArray wsnlResult;
                if(addr.matchInfo != null && addr.matchInfo == 'test')
                {
                    wsnlResult = emulateGeocode(addr);
                }
                else
                {
                    wsnlResult = wsnl.geoLocationInternationalAddressCoordinatesLatLon(
                                        addr.street, houseno, addr.city, addr.county, addr.country, 'latin');
                }                
                
                // Are there any results?
                if (wsnlResult.item == null)
                {
                    result.status = AvsResult.STATUS_EMPTY;
                    result.statusInfo = System.Label.SC_app_AddressValidationEmpty;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 
                                                                    System.Label.SC_app_AddressValidationEmpty);
                    ApexPages.addMessage(myMsg);
                    
                    return result;
                }
    
                // Evaluate the results                 
                for(WSWebserviceNL.LatLonCoordinatesInternationalAddress i : wsnlResult.item)
                {
                    // transfer the result into the return object
                    AvsAddress item = new AvsAddress();
                    item.country        = addr.country; // wsnlAddress.country (we can't use the converted country - we need our ISO2 code);                 
                    item.countryState   = ''; // not supported by webservices.nl
                    item.county         = i.province;         
                    item.postalcode     = i.postcode; 
                    item.city           = i.city;
                    item.district       = ''; // not supported by webservices.nl
                    item.street         = i.street;
                    item.housenumber    = ''; // not supported by webservices.nl 
                    item.nofrom         = ''; // not supported by webservices.nl
                    item.noto           = ''; // not supported by webservices.nl
                    item.geoX           = i.longitude;
                    item.geoY           = i.latitude;
                    if(item.geoX != null && item.geoX != 0)
                    {
                        item.matchInfo = 'geoLocationInternationalAddressCoordinatesLatLon : Geocoded';
                    }
                    // add the found address to the result list
                    result.items.add(item);
                    
                    //FIX: only use the first value - ignore all others
                    break;
                } // for..
            }//if(bSearchAgain)

            // now set the status values
            if(result.items.size() == 1)
            {
                result.status = AvsResult.STATUS_EXACT;
            } 
            else if(result.items.size() > 1)
            {
                result.status = AvsResult.STATUS_MULTI;
            }
            else
            {
                result.status = AvsResult.STATUS_BLANK;
            }
            // set the originial status info of the service
            result.statusInfo = 'webservices.nl: -'; 
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e + ' (used houseno:' + houseno + ')';
        }
        return result;        
    } // geocode


   /*
    * Tries to determine a house number. Unfortunately the webservicenl implementation
    * does not support alphanumerical house numbers. Extend this method to optimize the 
    * geocoding
    * @param value the string housenumber
    * @return the extracted housenumber  
    */
    public virtual Integer calcHouseno(String value)
    {
        // use a dummy house number as default
        Integer houseno = 1;
        try
        {   
            value = value.trim();
            
            String[] a = value.split('\\D');
            if(a.size() > 0)
            {
                // if there is a housenumber range "12-24" or 12a use the fist part 
                value = a[0];
            }
            houseno = Integer.valueof(value);
        }
        catch(Exception e)
        {
            // do nothing
        }
        return houseno; 
    }     


    // Helper functions
    
   /*
    * Converts the status in readable code (for diagnosis only)
    *
    * element_match_status>-
    * A string of length 8 describing how each individual address element matched the available reference data.  If two elements match this does not guarantee that they are valid in combination.  Each character position corresponds to an address element.
    * 1 Postal code 
    * 2 Locality 
    * 3 Province 
    * 4 Street 
    * 5 Building number 
    * 6 PO Box 
    * 7 Building 
    * 8 Organization 
    *
    * The possible values for each position are as follows: 
    * 0 empty 
    * 1 not found 
    * 2 not checked (no reference data or no chance of success) 
    * 3 matched with errors 
    * 4 matched without errors 
    * 
    * element_result_status 
    * A string of length 8 describing how each address element has been modified.  Each character position corresponds to an address element, in the same way as for the ‘element_match_status’ field described above.  The possible values for each position are as follows.
    * 0 empty 
    * 1 not checked 
    * 2 not checked but standardized 
    * 3 checked and corrected (changed or inserted) 
    * 4 validated, but changed (synonyms, old names) 
    * 5 validated, but standardized 
    * 6 validated and unchanged 
    * 7 no value returned, due to multiple possible matches
    */ 
    private String convertStatus(String status1, string status2)
    {
        String result = 'Match/Result'; 
        if(status1 != null && status1.length() == 8 &&
           status2 != null && status1.length() == 8)
        {
            result += '|1 Postal code: ' + convertMatch(status1, 0) + '/' + convertResult(status2, 0);
            result += '|2 Locality: '    + convertMatch(status1, 1) + '/' + convertResult(status2, 1);
            result += '|3 Province: '    + convertMatch(status1, 2) + '/' + convertResult(status2, 2);
            result += '|4 Street: '      + convertMatch(status1, 3) + '/' + convertResult(status2, 3);
            result += '|5 Housenumber: ' + convertMatch(status1, 4) + '/' + convertResult(status2, 4);
            result += '|6 PO Box: '      + convertMatch(status1, 5) + '/' + convertResult(status2, 5);
            result += '|7 Building: '    + convertMatch(status1, 6) + '/' + convertResult(status2, 6);
            result += '|8 Organization: '+ convertMatch(status1, 7) + '/' + convertResult(status2, 7);
        }
        return result;
    } // convertStatus
    
    /*
     * Converts the match status - see above
     */
    private String convertMatch(String status, Integer i)
    {
        status = status.substring(i, i + 1);
        
        if(status == '0')
            return 'empty';
        if(status == '1')
            return 'not found';
        if(status == '2')
            return 'not checked';
        if(status == '3')
            return 'matched with errors';
        if(status == '4')
            return 'matched';
        return '';  
    }

    /*
     * Converts the result status - see above
     */
    private String convertResult(String status, Integer i)
    {
        status = status.substring(i, i + 1);
        
        if(status == '0')
            return 'empty';
        if(status == '1')
            return 'not checked';
        if(status == '2')
            return 'not checked but standardized';
        if(status == '3')
            return 'checked and corrected (changed or inserted)';
        if(status == '4')
            return 'validated, but changed (synonyms, old names)';
        if(status == '5')
            return 'validated, but standardized';
        if(status == '6')
            return 'validated and unchanged';
        if(status == '7')
            return 'no value returned, due to multiple possible matches';
        return '';  
    } // convertResult

    /*
     * Converts the status code into a readyblel string.
     * Indicates the status of the input address.
     * N1 No validation performed because country not recognized 
     * N2 No validation performed because reference data for this country is not available 
     * N3 No validation performed because country is not available 
     * N4 Only parsing was performed for this address 
     * N6 No validation performed because country is not available 
     * N5 internationalAddressSearchV2 only: Insufficient information to generate suggestions 
     * Q1 internationalAddressSearchV2 only: No suggestions are available 
     * Q2 internationalAddressSearchV2 only: Suggested address is not complete (enter more information) 
     * Q3 internationalAddressSearchV2 only: Suggestions are available 
     * V internationalAddressSearchInteractive only: The input address is correct.  
     * C internationalAddressSearchInteractive only: The input address has been corrected. 
     * P1 internationalAddressSearchInteractive only: Data cannot be corrected and unlikely to be deliverable 
     * P2 internationalAddressSearchInteractive only: Data cannot be corrected, but fair chance that the address is deliverable 
     * P3 internationalAddressSearchInteractive only: Data cannot be corrected, but is very likely to be deliverable.  The supplied address may be correct.  The P1, P2 and P3 codes indicate an increasing chance of the address being correct.  However, the function could not correct the address because the address was either incomplete or ambigious.  Instead of correcting the address, the function determines the chance that a piece of post would arrive, given the supplied address. 
     * @param status    the validation_status
     * @return string containing the status code plus english description.
     */
    private String convertValidationStatus(String status)
    {
        String result = 'webservices.nl';
        if(status != null)
        {
            if(status == 'N1')
            {
                result += '[N1] No validation performed because country not recognized';
            } 
            else if(status == 'N2')
            {
                result += '[N2] No validation performed because reference data for this country is not available';
            } 
            else if(status == 'N3')
            {
                result += '[N3] No validation performed because country is not available ';
            }
            else if(status == 'N4')
            {
                result += '[N4] Only parsing was performed for this address ';
            }
            else if(status == 'N6')
            {
                result += '[N6] No validation performed because country is not available ';
            }
            else if(status == 'N5')
            {
                result += '[N5] Insufficient information to generate suggestions ';
            }
            else if(status == 'Q1')
            {
                result += '[Q1] No suggestions are available ';
            }
            else if(status == 'Q2')
            {
                result += '[Q2] Suggested address is not complete (enter more information) ';
            }
            else if(status == 'Q3')
            {
                result += '[Q3] Suggestions are available ';
            }
            else if(status == 'V')
            {
                result += '[V] The input address is correct. ';
            }
            else if(status == 'C')
            {
                result += '[C] The input address has been corrected. ';
            }
            else if(status == 'P1')
            {
                result += '[P1] Data cannot be corrected and unlikely to be deliverable ';
            }
            else if(status == 'P2')
            {
                result += '[P2] Data cannot be corrected, but fair chance that the address is deliverable ';
            }
            else if(status == 'P3')
            {
                result += '[P3] Data cannot be corrected, but is very likely to be deliverable.  The supplied address may be correct.  The P1, P2 and P3 codes indicate an increasing chance of the address being correct.  However, the function could not correct the address because the address was either incomplete or ambigious.  Instead of correcting the address, the function determines the chance that a piece of post would arrive, given the supplied address. ';
            }
            else
            {
                result += '[' + status + ']';
            }  
        }
        return result;
    } // convertValidationStatus 




    //-------------------------------------------------------------------------
    //  Test methods - for checking the private methods   
    //-------------------------------------------------------------------------

    // no reasonable test possible here - just call the method to get the coverage
    static testMethod void testPrivateMethods() 
    {
        SCAddressServiceImplNL v = new SCAddressServiceImplNL();
        
        String s;
        s = v.convertValidationStatus('N1');
        s = v.convertValidationStatus('N2');
        s = v.convertValidationStatus('N3');
        s = v.convertValidationStatus('N4');
        s = v.convertValidationStatus('N5');
        s = v.convertValidationStatus('N6');
        s = v.convertValidationStatus('N7');
        s = v.convertValidationStatus('N8');
        s = v.convertValidationStatus('N9');
        s = v.convertValidationStatus('Q1');
        s = v.convertValidationStatus('Q2');
        s = v.convertValidationStatus('Q3');
        s = v.convertValidationStatus('V');
        s = v.convertValidationStatus('C');
        s = v.convertValidationStatus('P1');
        s = v.convertValidationStatus('P2');
        s = v.convertValidationStatus('P3');
        s = v.convertValidationStatus('xx');
        
        s = v.convertMatch('0', 0);
        s = v.convertMatch('1', 0);
        s = v.convertMatch('2', 0);
        s = v.convertMatch('3', 0);
        s = v.convertMatch('4', 0);
        s = v.convertMatch('x', 0);

        s = v.convertResult('0', 0);
        s = v.convertResult('1', 0);
        s = v.convertResult('2', 0);
        s = v.convertResult('3', 0);
        s = v.convertResult('4', 0);
        s = v.convertResult('5', 0);
        s = v.convertResult('6', 0);
        s = v.convertResult('7', 0);
        s = v.convertResult('x', 0);
        
        s = v.convertStatus('11111111', '11111111');

        // test the house number extraction
        System.assertEquals(12, v.calcHouseno('12'));
        System.assertEquals(12, v.calcHouseno('12-14'));
        System.assertEquals(12, v.calcHouseno(' 12 - 14 '));
        System.assertEquals(12, v.calcHouseno('12a'));
        System.assertEquals(12, v.calcHouseno('12a-12b'));
        System.assertEquals(1,  v.calcHouseno('invalid 12'));

        v.prepare();        
    } // testPrivateMethods


    // Emulation of the web service call (test only)
    private WSWebserviceNL.InternationalAddressSearchV2Result emulateCheck(AvsAddress addr)
    {
        WSWebserviceNL.InternationalAddressSearchV2Result wsnlResult = new WSWebserviceNL.InternationalAddressSearchV2Result();
        wsnlResult.validation_status = '12345678';
        // continue test case 2         
        wsnlResult.result = new WSWebserviceNL.InternationalV2Array();
        if(addr.city == 'unknown')
        {   
            // test case 1
            return wsnlResult;
        }
        // just fill the return objects
        List<WSWebserviceNL.InternationalV2> itemlist = new List<WSWebserviceNL.InternationalV2>();

        WSWebserviceNL.InternationalV2 item = new WSWebserviceNL.InternationalV2();
        item.element_match_status = '11111111'; 
        item.element_result_status = '11111111'; 
        item.address = new WSWebserviceNL.InternationalFormattedAddress();
        item.address.province   = addr.county;         
        item.address.postcode   = addr.postalcode; 
        item.address.locality   = addr.city;
        item.address.street     = addr.street;
        item.address.housenr    = addr.housenumber;

        itemlist.add(item); 
        wsnlResult.result.item = itemlist;

        return wsnlResult;
    }
 
    // Emulation of the web service call (test only)
    private WSWebserviceNL.LatLonCoordinatesInternationalAddressArray emulateGeocode(AvsAddress addr)
    {
        WSWebserviceNL.LatLonCoordinatesInternationalAddressArray wsnlResult = new WSWebserviceNL.LatLonCoordinatesInternationalAddressArray();
        if(addr.city == 'unknown')
        {   
            // test case without result
            return wsnlResult;
        }
        
        List<WSWebserviceNL.LatLonCoordinatesInternationalAddress> itemlist = new List<WSWebserviceNL.LatLonCoordinatesInternationalAddress>();
        WSWebserviceNL.LatLonCoordinatesInternationalAddress item = new WSWebserviceNL.LatLonCoordinatesInternationalAddress();
        item.city = addr.city;
        item.street = addr.street;
        item.postcode = addr.postalcode;
        item.latitude = 50;
        item.longitude = 9; 
        itemlist.add(item);  
        wsnlResult.item = itemlist;
        return wsnlResult;
    }
    
    // Test the address validation  
    static testMethod void checkPositive2() 
    {
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'Aduard';
        addr.houseNumber = '6';
        addr.street = 'Moorweg';
        addr.matchInfo = 'test'; // emulation mode
        
        SCAddressServiceImplNL service = new SCAddressServiceImplNL();
        AvsResult result = service.check(addr);
        result = service.geocode(addr);
    }

    static testMethod void checkNegative() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'unknown';
        addr.houseNumber = '6';
        addr.street = 'Moorweg';
        addr.matchInfo = 'test'; // emulation mode
        
        SCAddressServiceImplNL service = new SCAddressServiceImplNL();
            
        AvsResult result = service.check(addr);
        result = service.geocode(addr);
    }
}