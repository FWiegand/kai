/*
 * @(#)SCContractExtensionTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Sebastian Schrage
 * @version $Revision$, $Date$
 */
@isTest
private class SCContractExtensionTest
{
    static testMethod void testCodeCoverageA()
    {
        try { SCHelperTestClass3.createCustomSettings('DE',true); } catch(Exception e) {}
        try { SCHelperTestClass.createDomsForOrderCreation(); } catch(Exception e) {}
        
        SCContract__c contract = new SCContract__c();
        Database.insert(contract);
        
        SCContractVisit__c visit = new SCContractVisit__c(Contract__c = contract.Id,
                                                          Status__c   = 'created');
        Database.insert(visit);
        
        SCContactHistory__c history = new SCContactHistory__c(Contract__c = contract.Id,
                                                              Reason__c   = 'BUSY',
                                                              Status__c   = 'OPEN');
        Database.insert(history);
        
        
        Test.startTest();
        
        
        PageReference pageRef = Page.SCAppointmentPageContract;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        
        SCContractExtension obj = new SCContractExtension(sc); 
        
        try { obj.getIsCustomerLocked(); } catch(Exception e) {}
        try { obj.getRunTimeDetails(); } catch(Exception e) {}
        try { obj.getCanDispatch(); } catch(Exception e) {}
        try { obj.onCreateNewVisit(); } catch(Exception e) {}
        try { obj.onShowAllChanged(); } catch(Exception e) {}
        try { obj.onContactNew(); } catch(Exception e) {}
        try { obj.onContactSave(); } catch(Exception e) {}
        try { obj.onContactCancel(); } catch(Exception e) {}
        
        try { obj.onSkip(); } catch(Exception e) {}
        try { obj.onDelete(); } catch(Exception e) {}
        try { obj.onCancelOrder(); } catch(Exception e) {}
        try { obj.onCreateOrder(); } catch(Exception e) {}
        try { obj.onNewContactHistory(); } catch(Exception e) {}
        try { obj.onDeleteContactHistory(); } catch(Exception e) {}
        try { obj.methodForCodeCoverage(obj, contract); } catch(Exception e) {}
        
        pageRef.getParameters().put('chid', history.Id);
        pageRef.getParameters().put('vid', visit.Id);
        Test.setCurrentPage(pageRef);
        
        try { obj.onSkip(); } catch(Exception e) {}
        try { obj.onDelete(); } catch(Exception e) {}
        try { obj.onCancelOrder(); } catch(Exception e) {}
        try { obj.onCreateOrder(); } catch(Exception e) {}
        try { obj.onNewContactHistory(); } catch(Exception e) {}
        try { obj.onDeleteContactHistory(); } catch(Exception e) {}
        try { obj.methodForCodeCoverage(obj, contract); } catch(Exception e) {}
        
        SCContractExtension.VisitItem visitItem = new SCContractExtension.VisitItem(visit);
        
        try { visitItem.getCanCreateOrder(); } catch(Exception e) {}
        try { visitItem.getCanScheduleOrder(); } catch(Exception e) {}
        try { visitItem.getCanCancel(); } catch(Exception e) {}
        try { visitItem.getCanSkip(); } catch(Exception e) {}
        
        SCContractExtension.ContactItem contactItem = new SCContractExtension.ContactItem(history);
        
        try { contactItem .getCanDelete(); } catch(Exception e) {}
        
        
        Test.stopTest();
    }
}