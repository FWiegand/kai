/**********************************************************************
Name:  fsfaAlertBatch
======================================================
Purpose:                                                            
Runs each night and sends out an Alert to the Owner of an Content record in case it expiers in two weeks                                                     
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
11/26/2013 Bernd Werner	<b.werner@yoursl.de>           
***********************************************************************/

global class fsfaContentAlertScheduler  implements Schedulable {
	global void execute(SchedulableContext sc) 
	{
		fsfaContentAlertBatch batch = new fsfaContentAlertBatch();
		Id batchId = Database.executeBatch(batch);
	}
}