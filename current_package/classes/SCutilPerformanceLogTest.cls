/*
 * @(#)SCutilPerformanceLogTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class - saves the search parameter if the search takes
 * longer as X ms.
 */
@isTest
private class SCutilPerformanceLogTest
{
    static testMethod void getSaveSearchPositive1() 
    {
        String testhandler = 'TheTestHandler';
        String testinterface = 'TheTestInterface';
        String testtype = 'SOQL';
        String testdirection = 'internal';
        
        // run the test
        SCutilPerformanceLog pl = new SCutilPerformanceLog(testhandler,  testinterface, 'SOQL'); 
        pl.start();
        // modify the start so that we get a delta (for test only)
        pl.start = pl.start -1;
        
        // stop the counter
        String logid = pl.stop();
        
        // check the result
        SCInterfaceLog__c log = [select Interface__c, InterfaceHandler__c, Data__c, Count__c, start__c, stop__c, duration__c, Type__c, Direction__c from SCInterfaceLog__c where id = :logid];
        
        System.assertEquals(log.Interface__c,  testinterface);
        System.assertEquals(log.InterfaceHandler__c,  testhandler);
        System.assertEquals(log.Type__c,  testType);
        System.assertEquals(log.Direction__c, testdirection);
        System.assertEquals(log.Data__c,  null);
        System.assertEquals(log.Count__c ,  null);
        System.assertEquals(log.start__c,  pl.start);
        System.assertEquals(log.stop__c,    pl.stop);
        
/*        
        pl.stop(soql, count);
        pl.saveLogAlways(soql, count);
        
        //System.assertEquals(pl.e,  null);
*/        
    }
    
    static testMethod void getSaveSearchPositive2() 
    {
        String testhandler = 'TheTestHandler';
        String testinterface = 'TheTestInterface';
        String testtype = 'SOQL';
        String testquery = 'The test query string';
        String testdirection = 'internal';
        Integer testcount = 123;
        
        // run the test
        SCutilPerformanceLog pl = new SCutilPerformanceLog(testhandler,  testinterface, 'SOQL'); 
        pl.start();
        // modify the start so that we get a delta (for test only)
        pl.start = pl.start -1;
        
        // stop the counter
        String logid = pl.stop(testquery, testcount);
        
        // check the result
        SCInterfaceLog__c log = [select Interface__c, InterfaceHandler__c, Data__c, Count__c, start__c, stop__c, duration__c, Type__c, Direction__c from SCInterfaceLog__c where id = :logid];
        
        System.assertEquals(log.Interface__c,  testinterface);
        System.assertEquals(log.InterfaceHandler__c,  testhandler);
        System.assertEquals(log.Type__c,  testType);
        System.assertEquals(log.Direction__c, testdirection);
        System.assertEquals(log.Data__c,  testquery);
        System.assertEquals(log.Count__c ,  testcount);
        System.assertEquals(log.start__c,  pl.start);
        System.assertEquals(log.stop__c,    pl.stop);
    }    
    
    static testMethod void getSaveSearchPositive3() 
    {
        String testhandler = 'TheTestHandler';
        String testinterface = 'TheTestInterface';
        String testtype = 'SOQL';
        String testquery = 'The test query string';
        String testdirection = 'internal';
        Integer testcount = 123;
        
        // run the test
        SCutilPerformanceLog pl = new SCutilPerformanceLog(testhandler,  testinterface, 'SOQL'); 
        pl.start();
        // modify the start so that we get a delta (for test only)
        pl.start = pl.start -10;
        
        // stop the counter
        String logid = pl.stopEx(testquery, testcount);
        
        // check the result
        SCInterfaceLog__c log = [select Interface__c, InterfaceHandler__c, Data__c, Count__c, start__c, stop__c, duration__c, Type__c, Direction__c from SCInterfaceLog__c where id = :logid];
        
        System.assertEquals(log.Interface__c,  testinterface);
        System.assertEquals(log.InterfaceHandler__c,  testhandler);
        System.assertEquals(log.Type__c,  testType);
        System.assertEquals(log.Direction__c, testdirection);
        System.assertEquals(log.Data__c,  testquery);
        System.assertEquals(log.Count__c ,  testcount);
        System.assertEquals(log.start__c,  pl.start);
        System.assertEquals(log.stop__c,    pl.stop);
    }    
    
    
}