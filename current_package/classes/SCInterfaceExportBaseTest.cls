/*
 * @(#)SCInterfaceExportBaseTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author Sebastian Schrage
 */
@isTest
private class SCInterfaceExportBaseTest
{
    static testMethod void CodeCoverageA()
    {
        Test.startTest();
        
        SCInterfaceExportBase obj = new SCInterfaceExportBase();
        
        obj.setInterfaceName('interfaceName');
        obj.setInterfaceHandler('interfaceHandler');
        obj.setSubclassName('subclassName');
        obj.setReferenceType('refType');
        obj.setReferenceID(UserInfo.getUserId());
        obj.setReferenceType2('refType');
        obj.setReferenceID2(UserInfo.getUserId());
        
        SCInterfaceExportBase obj2 = new SCInterfaceExportBase('interfaceName', 'interfaceHandler', 'refType', 'subclassNamePar');
        
        Test.stopTest();
    }
}