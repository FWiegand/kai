/*
 * @(#)SCDomainCacheController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
/**
 * The domain cache controller is an attempt to cache the domain
 * data that is used in picklists on VF pages.
 * The main problem is that dependencies between domains and 
 * domain values on VF pages could not be modelled on DB side 
 * since the sheer number of dependencies is far more that SF 
 * could handle.
 * The basic idea is to use this domain cache to retrieve JSON
 * data for the needed domains and dependencies and handle 
 * the select boxes in JavaScript.
 *
 * Possible GET parameter:
 * [debug]: {0|1} In debug mode the content type is text/plain instead of 
 *                application/json
 * [mode]: 0      Fetch the actual domain, domain values and descriptions
 *                (This is the default mode)
 *         1      Fetch the dependencies
 *         2      Fetch the order reasons
 * [lng]:         The language to use for the descriptions, if not set the 
 *                user's language will be used
 * m1d:           List of domains to fetch separated by a comma (used as master1)
 *                If mode = 1 and m2d, m3d or m4d is specified, no list, only one domain
 * [m1v]:         List of Master1 values to use for the query.
 *                Only used on the dependecy mode (mode=1) to limit the number
 *                of records to retrieve.
 * [m2d]:         Master2 domain to consider, only used on the dependecy mode (mode=1)
 * [m2v]:         List of Master2 values to consider, separated by a comma
 *                Only used on the dependecy mode (mode=1) to limit the number
 *                of records to retrieve.
 * [m3d]:         Master3 domain to consider, only used on the dependecy mode (mode=1)
 * [m3v]:         List of Master3 values to consider, separated by a comma
 *                Only used on the dependecy mode (mode=1) to limit the number
 *                of records to retrieve.
 * [m4d]:         Master4 domain to consider, only used on the dependecy mode (mode=1)
 * [m4v]:         List of Master4 values to consider, separated by a comma
 *                Only used on the dependecy mode (mode=1) to limit the number
 *                of records to retrieve.
 * [sd]:          List of Slave domains to consider, separated by a comma.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCDomainCacheController
{
    public static String domainCacheName = 'domainCache';
    
    private static String userCountry = SCBase.getUserCountry(true);
    private static Date xToday = Date.today();

    // this variables only for the test units
    public Integer domvalRead = 0;
    public Integer domdepRead = 0;
    public Integer ordReasonRead = 0;
    
    public SCDomainCacheController()
    {
    } // SCDomainCacheController

    private boolean isValid(Date validFrom, Date validTo)
    {
        if ((null != validFrom) && (validFrom <= xToday) && 
            ((null == validTo) || (validTo >= xToday)))
        {
            return true;
        }
        return false;
    } // isValid

    public String getContentType()
    {
        if (ApexPages.currentPage().getParameters().get('debug') == '1')
        {
            return 'text/plain';
        }
        
        return 'application/json';
    } // getContentType

    /**
     * Fetch domain values and their descriptions considering the get parameter
     * given in the URL.
     * If the language is not set in the URL we use the language of logged in
     * user.
     * <p>
     * The structure of the JSON object is the following
     *
     * { "DOMAIN" : 
     *   { "Domain Value Name" : 
     *       { "ID2" : "12345", 
     *         "locale" : "Translation", 
     *         "controlParameter" : [0,1,0,13]
     *       } 
     *   } 
     * }
     */    
    public String getContent()
    {
        String json = '';
        List<String> domainNames = new List<String>();
        
        String locale = ApexPages.currentPage().getParameters().get('lng');
        
        if (ApexPages.currentPage().getParameters().get('m1d') != null)
        {
            domainNames = ApexPages.currentPage().getParameters().get('m1d').split(',');
        }
    
        if (domainNames == null || domainNames.size() == 0)
        {
            return '{}';
        }
        
        if (locale == null || locale.length() == 0)
        {
            locale = UserInfo.getLanguage().substring(0,2);
        }
               
        Map<String, List<DomainValue>> domainStructure = new Map<String, List<DomainValue>>();
        
        List<SCDomainDescription__c> domains = [select DomainValue__r.DomainRef__r.Name, 
                                                       DomainValue__r.Name, 
                                                       DomainValue__r.Id2__c,
                                                       DomainValue__r.ValidFrom__c,
                                                       DomainValue__r.ValidTo__c, 
                                                       DomainValue__r.ControlParameter__c,
                                                       DomainValue__r.Country__c,
                                                       LocaleSidKey__c, Text__c 
                                                  from SCDomainDescription__c  
                                                 where DomainValue__r.DomainRef__r.Name in :domainNames 
                                                   and LocaleSidKey__c = :locale 
                                                   and DomainValue__r.Country__c in ('XX', :userCountry) 
                                              order by DomainValue__r.DomainRef__r.Name, 
                                                       Text__c, 
                                                       DomainValue__r.Sort__c,
                                                       DomainValue__r.Name, 
                                                       LocaleSidKey__c,
                                                       DomainValue__r.ValidFrom__c desc
                                                 limit 10000];
        System.debug('#### SCDomainCacheController.getContent(): locale -> ' + locale);
        System.debug('#### SCDomainCacheController.getContent(): domainNames -> ' + domainNames);
        System.debug('#### SCDomainCacheController.getContent(): domains -> ' + domains);

        Map<String, SCDomainDescription__c> controlList = new Map<String, SCDomainDescription__c>();
        for (SCDomainDescription__c dom :domains)
        {
            // if the domain value is not in the control list
            // or there is a country specific entry
            if (!controlList.containsKey(dom.DomainValue__r.Name) || 
                ('XX' != dom.DomainValue__r.Country__c))
            {
                // add the entry to the control list
                controlList.put(dom.DomainValue__r.Name, dom);
            } // if (!controlList.containsKey(dom.DomainValue__r.Name) || 
        } // for (SCDomainDescription__c dom :domains)

        domvalRead = controlList.size();
        
        SCutilSorter sorter = new SCutilSorter();
        sorter.originalList = controlList.values();
        List<SCDomainDescription__c> sortedList = (List<SCDomainDescription__c>)sorter.getSortedList('Text__c', true);

        // prepare the structure in apex
        for (SCDomainDescription__c dom :sortedList)
        {
            // The domain is still valid
            if (isValid(dom.DomainValue__r.ValidFrom__c, dom.DomainValue__r.ValidTo__c))
            {
                DomainValue domainValue = new DomainValue();
                domainValue.descriptions.put(dom.LocaleSidKey__c, dom);
                domainValue.domainValue = dom.DomainValue__r;
                
                if (domainStructure.containsKey(dom.DomainValue__r.DomainRef__r.Name))
                {
                    domainStructure.get(dom.DomainValue__r.DomainRef__r.Name).add(domainValue);
                }
                else
                {
                    domainStructure.put(dom.DomainValue__r.DomainRef__r.Name, new List<DomainValue>());
                    domainStructure.get(dom.DomainValue__r.DomainRef__r.Name).add(domainValue);
                }
            } // if (isValid(dom.DomainValue__r.ValidFrom__c, dom.DomainValue__r.ValidTo__c))
        } // for (SCDomainDescription__c dom :sortedList)

        /************************
         *
         * convert the apex structure to JSON
         *
         ************************/
        if (!domainStructure.isEmpty())
        {
            json = '{';
            
            Integer domainCount = 0;
            for (String domain :domainStructure.keySet())
            {
                json += '"' + domain + '":';
                json += '{';
                
                // domain value part
                Integer valueCount = 0;
                for (DomainValue domVal :domainStructure.get(domain))
                {
                    json += '"' + domVal.domainValue.Name + '":{';
                    json += '"ID2":"' + domVal.domainValue.Id2__c + '",';
                    
                    // translations
                    for (String lng :domVal.descriptions.keySet())
                    {
                        json += '"' + domVal.descriptions.get(lng).LocaleSidKey__c + '": ';
                        json += '"' + escape(domVal.descriptions.get(lng).Text__c) + '",';
                    } // for (String lng :domVal.descriptions.keySet())
                    
                    json += '"controlParameter":[';
                    
                    // convert control parameter to an JSON Array
                    if (domVal.domainValue.ControlParameter__c != null)
                    {
                        for (String cp :domVal.domainValue.ControlParameter__c.split('\\|'))
                        {
                            if (cp.contains('='))
                            {
                                Integer equalPosition = cp.indexOf('=') + 1;
                                if (cp.length() > equalPosition)
                                {                            
                                    json += '"' + cp.substring(equalPosition, cp.length()) + '",';
                                }
                                else
                                {
                                    json += '"",';
                                }
                            } // if (cp.contains('='))
                        } // for (String cp :domVal.domainValue.ControlParameter__c.split('\\|'))
                        if (json.endsWith(','))
                        {
                            json = json.substring(0, json.length() - 1);
                        }
                    } // if (domVal.domainValue.ControlParameter__c != null)
                    json += ']';
                    json += '}';
                    
                    valueCount++;
                    if (domainStructure.get(domain).size() > valueCount)
                    {
                        json += ',';
                    }
                } // for (DomainValue domVal : omainStructure.get(domain))
                
                json += '}';
               
                domainCount++;
                if (domainStructure.size() > domainCount)
                {
                    json += ',';
                }
            } // for (String domain :domainStructure.keySet())

            json += '}';       
        } // if (!domainStructure.isEmpty())
        return json;
    } // getContent

    /**
     * Get the dependencies for the given domains and 
     * pack it into a JSON structure
     * <p>
     * Example structure:
     * {"rows":[
     *    {
     *      "key": ["DOM_PRODUCTGROUP","312","","",""],
     *      "value": {"slave":"03","slaveDomain":"DOM_ERRLOC1"}
     *    },
     *    {
     *      "key": ["DOM_PRODUCTGROUP","312","50","",""],
     *      "value":{"slave":"49","slaveDomain":"DOM_ERRLOC1"}
     *    }
     * ]}
     */
    public String getDependencies()
    {
        String json = '';
        
        // Domain names used for the Mater1Ref
        List<String> domainNames = new List<String>();
        domainNames = ApexPages.currentPage().getParameters().get('m1d').split(',');
        System.debug('#### SCDomainCacheController.getDependencies(): domainName -> ' + domainNames);

        if (domainNames == null || domainNames.size() == 0)
        {
            return json;
        }

        // start json
        json = '{"rows":[';
        
        for (String domain :domainNames)
        {
            String soql = 'select Slave__r.Name, SlaveRef__c, ' + 
                                 'Master1__r.Name, Master2__r.Name, ' +
                                 'Master3__r.Name, Master4__r.Name, Master1Ref__c ' +
                            'from SCDomainDepend__c ' + 
                           'where Master1Ref__c = :domain ';

            String paramM1V = ApexPages.currentPage().getParameters().get('m1v');
            if ((null != paramM1V) && (paramM1V.length() > 0))
            {
                String m1v = '';
                for (String master :paramM1V.split('\\,'))
                {
                    m1v += '\'' + master + '\',';
                } // for (String master :paramM1V.split('\\,'))
                if (m1v.length() > 0)
                {
                    m1v = m1v.substring(0, m1v.length() - 1);
                    soql += ' and Master1__r.Name in (' + m1v + ') ';
                } // if (m1v.length() > 0)
            } // if ((null != paramM1V) && (paramM1V.length() > 0))

            String paramM2D = ApexPages.currentPage().getParameters().get('m2d');
            if ((null != paramM2D) && (paramM2D.length() > 0))
            {
                soql += ' and Master2Ref__c = \'' + paramM2D + '\' ';
            } // if ((null != paramM2D) && (paramM2D.length() > 0))

            String paramM2V = ApexPages.currentPage().getParameters().get('m2v');
            if ((null != paramM2V) && (paramM2V.length() > 0))
            {
                String m2v = '';
                for (String master :paramM2V.split('\\,'))
                {
                    m2v += '\'' + master + '\',';
                } // for (String master :paramM2V.split('\\,'))
                if (m2v.length() > 0)
                {
                    m2v = m2v.substring(0, m2v.length() - 1);
                    soql += ' and Master2__r.Name in (' + m2v + ') ';
                } // if (m2v.length() > 0)
            } // if ((null != paramM2V) && (paramM2V.length() > 0))

            String paramM3D = ApexPages.currentPage().getParameters().get('m3d');
            if ((null != paramM3D) && (paramM3D.length() > 0))
            {
                soql += ' and Master3Ref__c = \'' + paramM3D + '\' ';
            } // if ((null != paramM3D) && (paramM3D.length() > 0))

            String paramM3V = ApexPages.currentPage().getParameters().get('m3v');
            if ((null != paramM3V) && (paramM3V.length() > 0))
            {
                String m3v = '';
                for (String master :paramM3V.split('\\,'))
                {
                    m3v += '\'' + master + '\',';
                } // for (String master :paramM3V.split('\\,'))
                if (m3v.length() > 0)
                {
                    m3v = m3v.substring(0, m3v.length() - 1);
                    soql += ' and Master3__r.Name in (' + m3v + ') ';
                } // if (m3v.length() > 0)
            } // if ((null != paramM3V) && (paramM3V.length() > 0))

            String paramM4D = ApexPages.currentPage().getParameters().get('m4d');
            if ((null != paramM4D) && (paramM4D.length() > 0))
            {
                soql += ' and Master4Ref__c = \'' + paramM4D + '\' ';
            } // if ((null != paramM4D) && (paramM4D.length() > 0))

            String paramM4V = ApexPages.currentPage().getParameters().get('m4v');
            if ((null != paramM4V) && (paramM4V.length() > 0))
            {
                String m4v = '';
                for (String master :paramM4V.split('\\,'))
                {
                    m4v += '\'' + master + '\',';
                } // for (String master :paramM4V.split('\\,'))
                if (m4v.length() > 0)
                {
                    m4v = m4v.substring(0, m4v.length() - 1);
                    soql += ' and Master4__r.Name in (' + m4v + ') ';
                } // if (m4v.length() > 0)
            } // if ((null != paramM4V) && (paramM4V.length() > 0))

            String paramSd = ApexPages.currentPage().getParameters().get('sd');
            if ((null != paramSd) && (paramSd.length() > 0))
            {
                String sd = '';
                for (String slaveRef :paramSd.split('\\,'))
                {
                    sd += '\'' + slaveRef + '\',';
                } // for (String slaveRef :paramSd.split('\\,'))
                if (sd.length() > 0)
                {
                    sd = sd.substring(0, sd.length() - 1);
                    soql += ' and SlaveRef__c in (' + sd + ') ';
                } // if (sd.length() > 0)
            } // if ((null != paramSd) && (paramSd.length() > 0))

            soql += 'and (Country__c includes (\'XX\') ' + 
                    'or Country__c includes (\'' + userCountry + '\')) ';
            soql += 'order by Master1__r.Name, ' +
                             'Master2__r.Name, ' + 
                             'Master3__r.Name, ' + 
                             'Master4__r.Name ';
            soql += 'limit 10000';
            System.debug('#### SCDomainCacheController.getDependencies(): soql -> ' + soql);
            
            List<SCDomainDepend__c> domdepends = Database.query(soql);
            domdepRead = domdepends.size();
            for (SCDomainDepend__c depend :domdepends)
            {
                // in case we don't have a slave we could safely ignore this record.
                if (depend.Slave__r.Name == null || depend.Slave__r.Name.length() == 0)
                {
                    continue;
                }

                json += '{';
                json += '"key":[';
                json += '"' + depend.Master1Ref__c + '"';
                json += ',"' + depend.Master1__r.Name + '"';
                
                if (depend.Master2__r.Name != null)
                {
                    json += ',"' + depend.Master2__r.Name + '"';
                }
                else
                {
                    json += ',""';
                }

                if (depend.Master3__r.Name != null)
                {
                    json += ',"' + depend.Master3__r.Name + '"';
                }
                else
                {
                    json += ',""';
                }
                
                if (depend.Master4__r.Name != null)
                {
                    json += ',"' + depend.Master4__r.Name + '"';
                }
                else
                {
                    json += ',""';
                }
                
                json += '],';
                
                json += '"value":{"slave":"' + depend.Slave__r.Name + '",';
                json += '"slaveDomain":"' + depend.SlaveRef__c + '"}';
                json += '},';
            } // for (SCDomainDepend__c depend :domdepends)
            if (json.endsWith(','))
            {
                json = json.substring(0, json.length() - 1);
            }
            json += ',';
        } // for (String domain :domainNames)
        
        if (json.endsWith(','))
        {
            json = json.substring(0, json.length() - 1);
        }
        json += ']';

        // end json
        json += '}';
    
        return json;
    } // getDependencies

    /**
     * Get the order reasons and pack it into a JSON structure
     * <p>
     * Example structure:
     * {"rows":[
     *    {
     *      "key": ["Id","OrdType","InvType","InvSubType"],
     *      "value": {"descr":"name"}
     *    },
     *    {
     *      "key": ["Id","OrdType","InvType","InvSubType"],
     *      "value": {"descr":"name"}
     *    }
     * ]}
     */    
    /*public String getOrderReasons()
    {
        String json = '{"rows":[';
               
        List<SCorderReason__c> orderReasons = 
                        [Select Id, Name, ID2__c, 
                                cpAllowInactiveArticles__c, Brand__c, Country__c, 
                                cpDefaultArticleNo__c, ERPKey__c, 
                                cpOrderLineBehaviour__c, cpReferencePeriod__c, 
                                ERPTargetSystem__c, cpLimitAllowedArticleClass__c, 
                                OrderInvoicingType__c, OrderInvoicingSubType__c, 
                                OrderReasonCalc__c, OrderType__c, 
                                cpRequireDeliveryAddress__c, UsedBy__c, 
                                ValidFrom__c, ValidTo__c 
                           from SCOrderReason__c 
                          where Country__c = :userCountry 
                            and UsedBy__c = 'Service' 
                            and ValidFrom__c <= :xToday 
                            and (ValidTo__c = null or ValidTo__c >= :xToday) 
                       order by OrderReasonCalc__c];

        ordReasonRead = orderReasons.size();
        System.debug('#### getOrderReasons(): no reasons -> ' + ordReasonRead);
        
        // prepare the JSON string
        for (SCorderReason__c reason :orderReasons)
        {
            json += '{';
            json += '"key":[';
            json += '"' + reason.Id + '"';
            json += ',"' + reason.OrderType__c + '"';
            json += ',"' + reason.OrderInvoicingType__c + '"';
            json += ',"' + reason.OrderInvoicingSubType__c + '"';
            json += '],';
            
            json += '"value":{"descr":"' + reason.OrderReasonCalc__c + '"}';
            json += '},';
        } // for (SCorderReason__c reason :orderReasons)
        if (json.endsWith(','))
        {
            json = json.substring(0, json.length() - 1);
        }
        json += ']';

        // end json
        json += '}';
    
        return json;
    } */ // getOrderReasons
    
    /**
     * Check if the dependencies section shall be rendered or not depends
     * on the mode set as a GET parameter. Mode '1' is render the
     * dependencies.
     *
     * @return <code>true</code> if mode is '1'
     */
    public Boolean getRenderDependencies()
    {
        String mode = ApexPages.currentPage().getParameters().get('mode');

        return ((mode != null) && (mode == '1'));
    } // getRenderDependencies

    /**
     * Check if the domain value section shall be rendered or not depends
     * on the mode set as a GET parameter. Mode '0' or an unset mode 
     * renders the domain section.
     *
     * @return <code>true</code> if mode is '1'
     */
    public Boolean getRenderDomain()
    {
        String mode = ApexPages.currentPage().getParameters().get('mode');

        return ((mode == null) || (mode.length() == 0) || (mode == '0'));
    } // getRenderDomain
    
    /**
     * Check if the order reasons section shall be rendered or not depends
     * on the mode set as a GET parameter. Mode '2' is render the
     * order reasons.
     *
     * @return <code>true</code> if mode is '2'
     */
    /*public Boolean getRenderOrderReasons()
    {
        String mode = ApexPages.currentPage().getParameters().get('mode');

        return ((mode != null) && (mode == '2'));
    } */ // getRenderOrderReasons
    
    /**
     * Escape string that cause trouble in JS and has to be 
     * escaped therefore
     *
     */
    private String escape(String unescaped)
    {
        String escaped = '';
        
        if (unescaped == null)
        {
            return escaped;
        }
        escaped = unescaped;
        escaped = escaped.replace('\\','\\\\');
        escaped = escaped.replace('"','\\"');
        
        return escaped;
    } // escape
        
    /**
     * Wrapper class to hold a domain value object for 
     * translation to JSON
     */
    public class DomainValue
    {
        public SCDomainValue__c domainValue { get; set; }
        
        // <locale, description object>
        public Map<String, SCDomainDescription__c> descriptions 
            = new Map<String, SCDomainDescription__c>();
    } // DomainValue
} // SCDomainCacheController