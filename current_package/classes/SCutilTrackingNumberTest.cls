/**
 * @(#)SCutilTrackingNumberTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
@isTest
private class SCutilTrackingNumberTest 
{
    static testMethod void runTestsBasic() 
    {
        // some test data   
        String validNumber   = '901010053486<<<<458471270041T3';
        String invalidNumber = '471010053486<<<<458471270041T3';

        // default constructor      
        SCutilTrackingNumber t = new SCutilTrackingNumber();
        System.assertEquals(t.isValid(), false);
        // getter / setter      
        t.trackingNumber = validNumber;
        System.assertEquals(t.trackingNumber, validNumber);
        
        System.assertEquals(t.getComponent(), '053486<<<<');
        System.assertEquals(t.getSupplier(),  '458471');
        
        // Validation - positive
        System.assertEquals(t.isValid(), true);
        // Validation - negative
        t.trackingNumber = invalidNumber;
        System.assertEquals(t.isValid(), false);
        System.assertEquals(t.getComponent(), '');
        System.assertEquals(t.getSupplier(),  '');
        
    }
    
    static testMethod void runTestsAdvanced() 
    {
        SCConfOrderRepairCode__c data = new SCConfOrderRepairCode__c();
        data.Component__c = '654321<<<<'; 
        data.Supplier__c = '123456';
        data.ErrorLocation1__c = '50';
        data.ErrorLocation2__c = '741';
        data.ErrorActivityCode1__c = '05';
        insert data;
                
        // Construct a new item
        SCutilTrackingNumber t = new SCutilTrackingNumber('901010654321<<<<123456270041T3');

        // Test the extraction of data      
        SCConfOrderRepairCode__c result = t.getValues();
        if(result != null)
        {
            System.assertEquals(result.Component__c, data.Component__c);
            System.assertEquals(result.Supplier__c, data.Supplier__c);
            System.assertEquals(result.ErrorLocation1__c, data.ErrorLocation1__c);
            System.assertEquals(result.ErrorLocation2__c, data.ErrorLocation2__c);
            System.assertEquals(result.ErrorActivityCode1__c, data.ErrorActivityCode1__c);
        }      
        // Test the negative branches 
        t.trackingnumber = 'invalid';
        result = t.getValues();
        System.assertEquals(result == null, true);
        
    }
}