/*
 * @(#)CCWCMaterialInventoryCreateTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
public class CCWCMaterialReservationCreateTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
 
    public static String MODE_REPLENISHMENT = 'REPLENISHMENT';
    public static String MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
    public static String MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';

    static String TOPOLOGY_MODE_ORDER = 'TOPOLOGY_MODE_ORDER';
    static String TOPOLOGY_MODE_PLANT_STOCK = 'TOPOLOGY_MODE_PLANT_STOCK';
    static String TOPOLOGY_MODE_PLANT = 'TOPOLOGY_MODE_PLANT';
    static String TOPOLOGY_MODE_STOCK = 'TOPOLOGY_MODE_STOCK';

    static testMethod void positiveTestCaseOrder1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_ORDER, matStatus, matMoveType, valuationType);
    }       


    static testMethod void negativeTestCaseOrder1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_ORDER, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCasePlantAndStock_REPL_1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCasePlantAndStock_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       
    static testMethod void positiveTestCasePlantAndStock_RS_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlantAndStock_REPL_1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void negativeTestCasePlantAndStock_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       
    static testMethod void negativeTestCasePlantAndStock_RS_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_PLANT_STOCK, matStatus, matMoveType, valuationType);
    }       


    static testMethod void positiveTestCasePlant_REPL_1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCasePlant_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCasePlant_RS_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlant_REPL_1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlant_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCasePlant_RS_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_PLANT, matStatus, matMoveType, valuationType);
    }       





    static testMethod void positiveTestCaseStock_REPL1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCaseStock_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void positiveTestCaseStock_RS1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        positiveTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseStock_REPL1()
    {
        String matStatus = '5402';
        String matMoveType = '5222'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_REPLENISHMENT, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseStock_RO_1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_ORDER, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       

    static testMethod void negativeTestCaseStock_RS1()
    {
        String matStatus = '5402';
        String matMoveType = '5202'; 
        String valuationType = 'ZNEU';
        negativeTestCaseOrder(MODE_RESERVATION_FOR_STOCK, TOPOLOGY_MODE_STOCK, matStatus, matMoveType, valuationType);
    }       


/*
    public static String MODE_REPLENISHMENT = 'REPLENISHMENT';
    public static String MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
    public static String MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';
*/
    static void positiveTestCaseOrder(String reservation_mode, String topology_mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        
        
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);
        List<SCStock__c> stockListForName = [select name, id from SCStock__c];


        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);


        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);
        SCOrder__c order = null;
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();

        if(topology_mode == TOPOLOGY_MODE_ORDER)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        }
    Test.StartTest();

        if(reservation_mode == MODE_REPLENISHMENT)
        {
            // create a replenishment
            SCMaterialReplenishment__c replenishment = CCWCTestBase.createReplenishment(stockId);
            //stmt += ' and Replenishment__c <> null and Type__c = \'5222\'';
            if(topology_mode != TOPOLOGY_MODE_ORDER)
            {
                CCWCTestBase.createMaterialMovements(mmList, replenishment.Id, null, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }                                                     
        }
        else if(reservation_mode == MODE_RESERVATION_FOR_ORDER)
        {
            if(topology_mode == TOPOLOGY_MODE_ORDER)
            {
                CCWCTestBase.createMaterialMovements(mmList, order.Id, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }   
        }
        else if(reservation_mode == MODE_RESERVATION_FOR_STOCK)
        {
            if(topology_mode != TOPOLOGY_MODE_ORDER)
            {           
                CCWCTestBase.createMaterialMovements(mmList, null, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }                                                     
        }

//    Test.StopTest();


        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        System.assertEquals(true, plantList.size() > 0);
        
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            public static String MODE_REPLENISHMENT = 'REPLENISHMENT';
            public static String MODE_RESERVATION_FOR_ORDER = 'RESERVATION_FOR_ORDER';
            public static String MODE_RESERVATION_FOR_STOCK = 'RESERVATION_FOR_STOCK';

            static String TOPOLOGY_MODE_ORDER = 'TOPOLOGY_MODE_ORDER';
            static String TOPOLOGY_MODE_PLANT_STOCK = 'TOPOLOGY_MODE_PLANT_STOCK';
            static String TOPOLOGY_MODE_PLANT = 'TOPOLOGY_MODE_PLANT';
            static String TOPOLOGY_MODE_STOCK = 'TOPOLOGY_MODE_STOCK';
        */


//    Test.StartTest();

        if(topology_mode == TOPOLOGY_MODE_ORDER)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(order.id, async, testMode);        
        }
        else if(topology_mode == TOPOLOGY_MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(plantName, stockName, reservation_mode, async, testMode);      
        }
        else if(topology_mode == TOPOLOGY_MODE_PLANT)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(plantName, null, reservation_mode, async, testMode);       
        }   
        else if(topology_mode == TOPOLOGY_MODE_STOCK)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(null, stockName, reservation_mode, async, testMode);       
        }   

//    Test.StopTest();
        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        System.assertEquals(mmList2.size(), 1);
        System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'CreateMaterialReservation';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'MaterialReservation';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 1; 
        String msg = 'Material reservation created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       

//    Test.StartTest();

        // call the response service
        CCWSGenericResponse.transmit(gr);
        
    Test.StopTest();
        
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        System.assertEquals(mmList3.size(), 1);
        System.assertEquals('ok', mmList3[0].ERPStatus__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }

    static void negativeTestCaseOrder(String reservation_mode, String topology_mode, String matStatus, String matMoveType, String valuationType)
    {
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        // create stock
        Boolean doUpsert = true;
        SCPlant__c plant = CCWCTestBase.createTestPlant(doUpsert);
        List<SCStock__c> stockList = CCWCTestBase.createTestStocks(plant.Id, doUpsert);
        System.assertEquals(true, stockList.size() > 0);

        ID stockId = stockList[0].id;
        System.assertNotEquals(null, stockID);

        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);

        // create stock items
        List<SCStockItem__c> stockItemList = CCWCTestBase.createTestStockItems(articleList, stockList[0].id, stockList[1].id, doUpsert);
        System.assertEquals(true, stockItemList.size() > 0);

        // create material movements
        CCWCTestBase.createDomMatStat(seeAllData);

        SCOrder__c order = null;

        if(topology_mode == TOPOLOGY_MODE_ORDER)
        {
            CCWCTestBase.createDomsForOrderCreation(seeAllData);
            order = CCWCTestBase.createTestOrder(appSettings, doUpsert);
        }
    Test.StartTest();
        
        Decimal qty = 1.0; 
        List<SCMaterialMovement__c> mmList = new List<SCMaterialMovement__c>();
        if(reservation_mode == MODE_REPLENISHMENT)
        {
            SCMaterialReplenishment__c replenishment = CCWCTestBase.createReplenishment(stockId);
            //stmt += ' and Replenishment__c <> null and Type__c = \'5222\'';
            if(topology_mode != TOPOLOGY_MODE_ORDER)
            {
                CCWCTestBase.createMaterialMovements(mmList, replenishment.Id, null, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }                                                     
        }
        else if(reservation_mode == MODE_RESERVATION_FOR_ORDER)
        {
            if(topology_mode == TOPOLOGY_MODE_ORDER)
            {
                CCWCTestBase.createMaterialMovements(mmList, order.Id, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }   
        }
        else if(reservation_mode == MODE_RESERVATION_FOR_STOCK)
        {
            if(topology_mode == TOPOLOGY_MODE_ORDER)
            {           
                CCWCTestBase.createMaterialMovements(mmList, null, stockId, articleList,
                                                      valuationType, qty, matStatus, matMoveType);
            }
            else
            {
                return;
            }                                                     
        }

        List<SCMaterialMovement__c> mmList1 = [select id, name, ERPStatus__c from SCMaterialMovement__c where Stock__c = :stockId];
        System.assertEquals(true, mmList1.size() > 0);

        // call service
        Boolean async = false;
        Boolean testMode = true;
        
        // read plant and stock for their names
        List<SCPlant__c> plantList = [select name from SCPlant__c where id = : plant.id];
        System.assertEquals(true, plantList.size() > 0);
        
        List<SCStock__c> stockListForName = [select name, id from SCStock__c];
        System.assertEquals(true, stockListForName.size() > 0);
        String plantName = plantList[0].name;
        String stockName = stockListForName[0].name;
        String modeOfReservation = null; // not relevant
        String requestMessageID = null;
        /*
            static String TOPOLOGY_MODE_ORDER = 'TOPOLOGY_MODE_ORDER';
            static String TOPOLOGY_MODE_PLANT_STOCK = 'TOPOLOGY_MODE_PLANT_STOCK';
            static String TOPOLOGY_MODE_PLANT = 'TOPOLOGY_MODE_PLANT';
            static String TOPOLOGY_MODE_STOCK = 'TOPOLOGY_MODE_STOCK';
        */
        if(topology_mode == TOPOLOGY_MODE_ORDER)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(order.id, async, testMode);        
        }
        else if(topology_mode == TOPOLOGY_MODE_PLANT_STOCK)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(plantName, stockName, reservation_mode, async, testMode);      
        }
        else if(topology_mode == TOPOLOGY_MODE_PLANT)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(plantName, null, reservation_mode, async, testMode);       
        }   
        else if(topology_mode == TOPOLOGY_MODE_STOCK)
        {
            requestMessageID = CCWCMaterialReservationCreate.callout(null, stockName, reservation_mode, async, testMode);       
        }   
        
        
        // make assertion after web call
        List<SCMaterialMovement__c> mmList2 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from SCMaterialMovement__c 
                                        where id = : mmList1[0].Id];
        System.assertEquals(mmList2.size(), 1);
        System.assertEquals('pending', mmList2[0].ERPStatus__c);
        
        
        // fill response structure
        CCWSGenericResponse.GenericServiceResponseMessageClass gr = new CCWSGenericResponse.GenericServiceResponseMessageClass();
        String messageId = u.getMessageID();
        String messageUUID = u.getMessageID();
        String operation = 'CreateMaterialReservation';
        String externalID = mmList2[0].name;

        CCWCTestBase.prepareWCGenericResponse(gr, messageId, messageUUID, requestMessageID, operation, externalID);
        // add reference        
        String idType = 'MaterialReservation';
        String referenceID = '123';
        CCWCTestBase.addReferenceItem (gr, idType, externalID, referenceId);

        // add log
        String typeId = 'TypeID';
        Integer severityCode = 3; 
        String msg = 'Material reservation not created';
        
        CCWCTestBase.addLogEntry (gr, typeId, externalID, severityCode, msg);       
        // call the response service
        CCWSGenericResponse.transmit(gr);

    Test.StopTest();
        
        // make assertions 
        // read material movements
        List<SCMaterialMovement__c> mmList3 = [select id, name, ERPStatus__c, ERPResultInfo__c, ERPResultDate__c from 
                SCMaterialMovement__c where id = : mmList[0].Id];
        System.assertEquals(mmList3.size(), 1);
        System.assertEquals('error', mmList3[0].ERPStatus__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultInfo__c); 
        System.assertNotEquals(null, mmList3[0].ERPResultDate__c); 
        Date today = Date.today();
        System.assertEquals(today, mmList3[0].ERPResultDate__c.date()); 
    }


/*
    static testMethod void musterTest()
    {
        Test.StartTest();
        Test.StopTest();
    }   
*/
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}