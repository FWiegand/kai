public with sharing class T_RedSurveyResultAfterInsert {

	public void VisUpdate(List<RedSurveyResult__c> RedSRList)
	{	
		LIST<Visitation__c> VisUpdateList = new LIST<Visitation__c>();
		
		for(RedSurveyResult__c RedSRLoop: RedSRList){
			if(RedSRLoop.VisitationId__c != '')
			{
				Visitation__c newVis = new Visitation__c();
				newVis.Id = RedSRLoop.VisitationId__c;
				newVis.RedSurveyResult__c = RedSRLoop.Id;
				VisUpdateList.add(newVis);
			}
		}
		
		try{
			update VisUpdateList;
		} 
		catch(Exception e){
			System.debug('Trigger exception occurred: RedSurveyResultAfterInsert -- ' + e.getMessage());
		}
	}
	

}