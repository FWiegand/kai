/*
 * @(#)SCArticleImportControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

@isTest
private class SCArticleImportControllerTest {

    /**
     * SCArticleImportController under positiv test.
     */
    static testMethod void test() 
    {
        SCHelperTestClass.createArticle('test1212', true);
        SCHelperTestClass.createArticle('test2323', '3434', true);
        
        SCArticleImportController c = new SCArticleImportController();
        c.input = 'test1212,3434,test1234';
        c.checkInput();
        c.importArticles();
    }
}