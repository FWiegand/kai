/*
 * @(#)SCEditAccountControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCEditAccountControllerTest
{
    /**
     * testAccountUpdate
     */       
    static testMethod void testAccountUpdateDE()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createAccountobject('Customer', true);
        
        SCHelperTestClass.account.BillingCity       = 'Paderborn';
        SCHelperTestClass.account.BillingStreet     = 'Bahnhofstraße';
        SCHelperTestClass.account.BillingCountry__c    = 'DE';
        SCHelperTestClass.account.BillingPostalCode = '33098';
        SCHelperTestClass.account.BillingHouseNo__c    = '24';
        update SCHelperTestClass.account;
        
        SCEditAccountController controller = new SCEditAccountController(SCHelperTestClass.account.Id); 
        System.assert(controller.acc != null);      

        System.assertEquals(true, controller.getIsGeocoded());      

        AvsAddress addr = new AvsAddress();
        addr.City       = 'Paderborn';
        addr.Street     = 'Bahnhofstraße';
        addr.Country    = 'DE';
        addr.PostalCode = '33098';
        addr.HouseNumber   = '24';
        addr.title           = 'Dr.';
        addr.salutation      = 'Hr.';
        addr.firstName       = 'Hans';
        addr.surname         = 'Schmidt';
        addr.telephoneNumber = '01234/12345';
        addr.faxNumber       = '01234/12346';
        addr.birthdate       = Date.today();
        addr.extension       = 'a';
        addr.flatno          = '1';
        addr.floor           = 'B';
        addr.geox          = 11;
        addr.geoy          = 55;
        addr.geoApprox     = false;
        controller.OnUpdateData('validated', addr);
        System.assert(controller.acc != null);      

        controller.onSave();
        controller.OnCancelAddrValidation();
    } // testAccountUpdateDE

    static testMethod void testAccountUpdateNL()
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        SCHelperTestClass.createAccountobject('Customer', true);
        
        SCHelperTestClass.account.BillingCity       = 'Hilvarenbeek';
        SCHelperTestClass.account.BillingStreet     = 'Dorpsstraat';
        SCHelperTestClass.account.BillingCountry__c    = 'NL';
        SCHelperTestClass.account.BillingPostalCode = '5085EG';
        SCHelperTestClass.account.BillingHouseNo__c    = '24';
        update SCHelperTestClass.account;
        
        SCEditAccountController controller = new SCEditAccountController(SCHelperTestClass.account.Id); 
        System.assert(controller.acc != null);      

        System.assertEquals(true, controller.getIsGeocoded());      

        AvsAddress addr = new AvsAddress();
        addr.City       = 'Hilvarenbeek';
        addr.Street     = 'Dorpsstraat';
        addr.Country    = 'NL';
        addr.PostalCode = '5085EG';
        addr.HouseNumber   = '24';
        addr.title           = 'Dr.';
        addr.salutation      = 'Hr.';
        addr.firstName       = 'Hans';
        addr.surname         = 'Winford';
        addr.telephoneNumber = '01234/12345';
        addr.faxNumber       = '01234/12346';
        addr.birthdate       = Date.today();
        addr.extension       = 'a';
        addr.flatno          = '1';
        addr.floor           = 'B';
        addr.geox          = 11;
        addr.geoy          = 55;
        addr.geoApprox     = false;
        controller.saveLocations = '1';
        controller.OnUpdateData('validated', addr);
        System.assert(controller.acc != null);      

        controller.onSave();
        controller.OnCancelAddrValidation();
    } // testAccountUpdateNL

    /**
     * testAccountAndLocationUpdate
     */       
    static testMethod void testAccountAndLocationUpdate()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createAccountobject('Customer', true);
        SCHelperTestClass.createInstalledBase(true);
        
        ApexPages.currentPage().getParameters().put('aid', SCHelperTestClass.account.Id);
        SCEditAccountController controller = new SCEditAccountController(); 
        System.assert(controller.acc != null);      

        System.assertEquals(true, controller.getHasLocations());      
        System.assertEquals(true, controller.getIsGeocoded());
        controller.saveLocations = '1';
        controller.onSave();
    } // testAccountAndLocationUpdate

    /**
     * testAccountAndLocationUpdate
     */   
    static testMethod void testLocationWithOrder()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet4(true);
        
        ApexPages.currentPage().getParameters().put('aid', SCHelperTestClass.account.Id);
        SCEditAccountController controller = new SCEditAccountController(); 
        System.assert(controller.acc != null);      

        System.assertEquals(true, controller.getHasLocations());      
        controller.setIsGeocoded(false);
        System.assertEquals(false, controller.getIsGeocoded());      
        System.assertEquals(true, controller.locationInfo.length() > 0);      
    } // testLocationWithOrder
} // SCEditAccountControllerTest