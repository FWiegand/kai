/*
 * @(#)CCWCAssetInventoryDataRequestTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Georg Birkenheuer
 */
 
@isTest
public with sharing class CCWCAssetInventoryDataRequestTest {

	@isTest 
	static void test1()
	{
		SCInstalledBase__c equipment = prepareTestData();
		CCWCAssetInventoryDataRequest.callout(equipment.ID, true, true);
		
	}


	public static SCInstalledBase__c prepareTestData()
	{
		CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        appSettings = SCApplicationSettings__c.getInstance();
        
        CCSettings__c ccSettings = new CCSettings__c();
        ccSettings.SAPDispBasicAuthUsername__c = 'user';
        ccSettings.SAPDispBasicAuthPassword__c = 'passsword';
        ccSettings.SAPDispEndpointAssetInventory__c = 'AssettEndpoint';
        ccSettings.SAPDispServer__c = 'Server';
        insert ccSettings;
        
        Boolean doUpsert = true;
        
        // create a new article if we don't already have one
       SCArticle__c article = new SCArticle__c(
                                    Name = 'TEST-000252',
                                    //ID2__c = '00-' + articleName,
                                    EANCode__c = 'EAN-001', 
                                    Text_en__c = 'Unit Test Article',
                                    TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                                    Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A);

        upsert article;
       

        // Create Account
		Account account = createAccount();
        
		// Create location
		SCInstalledBaseLocation__c location = createLocationandRole(account);

		// create Brand
        Brand__c brand = createUsedBrand();
		// Create Product model
		SCProductModel__c prodModel  = createProdModel(brand);


		// create Equipment
        SCInstalledBase__c installedBase = new SCInstalledBase__c(
        									ID2__c = '1051273',
                                            IDExt__c = '1051273',
                                            Article__c = article.Id,
                                            Article__r = article,
                                            SerialNo__c = '00305005',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = location.Id,
                                            InstalledBaseLocation__r = location,
                                            Type__c = 'Appliance',
                                            ProductModel__c = prodModel.Id, 
                                            ProductUnitClass__c = prodModel.UnitClass__c, 
                                            ProductUnitType__c = prodModel.UnitType__c, 
                                            ProductGroup__c = prodModel.Group__c, 
                                            ProductPower__c = prodModel.Power__c, 
                                            ProductSkill__c = 'PS1', 
                                            Brand__c = prodModel.Brand__c
                                    );

        upsert installedBase;
        
        return  installedBase;
        

	}
	
	public static Account createAccount()
	{	
	    Account  account = new Account();
        account.LastName__c = 'testaccount 00';
        account.FirstName__c = 'Person';


        //account.RecordTypeId = [Select Id from RecordType where DeveloperName = :RecordTypeName  and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'DE-005';
        account.BillingPostalCode = '33106 PB';
        account.BillingCountry__c = 'DE';
        account.CurrencyIsoCode = 'EUR';

        // Fields SF42

        // Fields GMS
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Karl-Schurz Str.';
        account.BillingHouseNo__c = '29';
        account.BillingPostalCode = '33100 PB';
        account.BillingCity = 'Padebrorn';
        account.BillingCountry__c = 'PB';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        account.GeoApprox__c = false;
        /*if (priceList <> null && priceList.Id <> null)
        {
            account.DefaultPriceList__c = priceList.Id;
            account.DefaultPriceList__r = priceList;
        }*/

        // Fields Arlanis

        insert account;
        return account;
	}
	
	
	public static SCInstalledBaseLocation__c createLocationandRole(Account account)
	{		
        SCInstalledBaseLocation__c location = new SCInstalledBaseLocation__c(
                BuildingDate__c = (Date.today() - 1000),
                Street__c = 'Karl-Schurz Str.',
                HouseNo__c = '29',
                PostalCode__c = '33100 PB',
                City__c = 'Paderborn',
                Country__c = 'DE',
				GeoX__c	= 8.795968,
				GeoY__c	= 51.725393,
                LocName__c = 'Test Location',
                //ID2__c = 'TEST-LOCATION',
                Status__c = 'Active'
            );

	        SCInstalledBaseRole__c locationRole = new SCInstalledBaseRole__c(Account__c = account.Id,
	                                                  InstalledBaseLocation__r = location,
	                                                  Role__c = 'Owner',
	                                                  ValidFrom__c = Date.today()
                                    );


            upsert location;
            
            locationRole.InstalledBaseLocation__c = location.Id;
            upsert locationRole;
            return location;
	}
	
	public static Brand__c createUsedBrand()
	{		
		Brand__c usedBrand = new Brand__c();
		usedBrand.ID2__c = 'DECCRR0000';
		usedBrand.Name = 'DECCRR0000';
		insert usedBrand;
		return usedBrand;
	}
	
	public static SCProductModel__c createProdModel(Brand__c brand)
	{
	
        SCProductModel__c prodModel = new SCProductModel__c(Brand__c = brand.Id, 
                                          Country__c = 'DE', 
                                          UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                          UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 
                                          Group__c = '268', 
                                          Name = 'Coke Classic', 
                                          Power__c = '550');                                                    
        insert prodModel;
        return prodModel;
	}
}