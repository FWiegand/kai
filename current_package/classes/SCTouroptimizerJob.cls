/**
 * @(#)SCTouroptimizerJob.cls    ASE1.0 ld 02.11.2012
 * 
 * Copyright (c) 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 
/*
 * Helper to start the batch optimisation. 
 * 
 * Option 1: 
 * SCTouroptimizerJob x = new SCTouroptimizerJob();
 * x.execute(null);
 *
 * The application settings are used for configuration:
 * ASE_TOUROPTIMIZERJOB_TIMESPAN__c
 * ASE_TOUROPTIMIZERJOB_DURATION__c
 * ASE_TOUROPTIMIZERJOB_DEPARTMENT__c
 * ASE_TOUROPTIMIZERJOB_HINT__c
 *
 * Option 2: Optimize the 11.12.2012 based on tthe business units - simulation only
 * SCTouroptimizerJob x = new SCTouroptimizerJob();
 * x.execute('20121211', 1, 'C0001 (Phil Shaw);C0002 (Jim Dunlea);C0003 (Nigel Roberts)', ';SIMULATION=1');
 *
 * Option 3: Optimize the current day and use the SCRessourceAssignment.Grouping5 field and write the data
 * SCTouroptimizerJob x = new SCTouroptimizerJob();
 * x.execute('#TODAY#', 2, 'GROUPING5=Virtual Batch 1', ';SIMULATION=0');
 * Option 4: Optimize the current day and use the SCRessourceAssignment.Grouping5 field department and write the data
 * SCTouroptimizerJob x = new SCTouroptimizerJob();
 * x.execute('#TODAY#', 2, 'C0001 (Phil Shaw);C0002 (Jim Dunlea);GROUPING5=Virtual Batch 1', ';SIMULATION=0');
 */ 
global class SCTouroptimizerJob implements Schedulable
{
   /*
    * Function that can be called from the external job scheduler. 
    * @context plesae set to null
    */
    global void execute(SchedulableContext context)
    {
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        Map<String, String> params = new Map<String, String>();
        params.put('timespan',      appSettings.ASE_TOUROPTIMIZERJOB_TIMESPAN__c);
        params.put('duration', '' + appSettings.ASE_TOUROPTIMIZERJOB_DURATION__c);
        params.put('department',    appSettings.ASE_TOUROPTIMIZERJOB_DEPARTMENT__c);
        params.put('hint',       appSettings.ASE_TOUROPTIMIZERJOB_HINT__c);
        
        calloutAsync(params);
    }


   /*
    * Use this function to start the external batch job execution by passing the external parameters
    * @param timespan   the day to optmize: #TODAY# or a string in the form YYYYMMDD used for the optimisation
    *                   the date must by >= today (optimisation in the past is disabled)
    * @param duration   the number of minutes that can be used to execute the optimisation
    * @param department a semicolon separated list of SCBusinessUnit__c.Name values (please do not add spaces) 
    *                   e.g: 'C0001 (Phil Shaw);C0002 (Jim Dunlea);C0003 (Nigel Roberts)'
    * @param addhint    ';SIMULATION=1'    simulation only - not tour data is modified 
    *                   ';SIMULATION=0'    tour data is modified 
    */
    global void execute(String timespan, Integer duration, String department, String addhint)
    {
        System.debug('SCTouroptimizerJob timespan: ' + timespan + ' duration:' + duration + ' department:' + department + ' addhint' + addhint);
        
        Map<String, String> params = new Map<String, String>();
        params.put('timespan', timespan);
        params.put('duration', '' + duration);
        params.put('department',  department);
        params.put('hint', addhint);
        
        calloutAsync(params);
    }

    
    /**
     * Call the ASE asynchronously to start the processing
     */
    @Future(callout=true)
    public static void calloutAsync(Map<String, String> params)
    {
        // job session
        String sessionID = 'gwin-0';
    
        SCAse ase = new SCAse(AseCalloutConstants.ASE_TYPE_TOUROPTIMIZERJOB_STR);

        ase.addctx('id', sessionID);
        ase.addctx('userid', getUserID());
        
        ase.addparam('id', sessionID);
        ase.addparam('timespan',   params.get('timespan'));        
        ase.addparam('duration',   params.get('duration'));        
        ase.addparam('department', params.get('department'));        
        ase.addparam('hint',       params.get('hint'));        

        ase.callout(); 

    }
    
    // a small helper class to simplify the processing
    class SCAse
    {
        // Parameter that contain the current user context
        AseService.aseDataType  contextParam;
        AseService.aseDataEntry contextParamDataEntry;
         
        // Paremeter for the job to call
        AseService.aseDataType  jobParam;
        AseService.aseDataEntry jobParamDataEntry;
        String paramType; // see AseCalloutConstants....ASE_TYPE_TOUROPTIMIZERJOB_STR

        public SCAse(String paramType)
        {      
            this.paramType = paramType;
            
            // prepare the context parameter 
            contextParam = new AseService.aseDataType();
            contextParamDataEntry = new AseService.aseDataEntry();
            contextParamDataEntry.keyValues = new AseService.aseKeyValueType[0]; 
                
            // prepare job params
            jobParam = new AseService.aseDataType();
            jobParamDataEntry = new AseService.aseDataEntry();
            jobParamDataEntry.keyValues = new AseService.aseKeyValueType[0];
            
        }   
        public void addctx(String key, String value)
        {
            AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = value != null ? value : '';
            contextParamDataEntry.keyValues.add(keyValue);
            System.debug('Touroptimizer Job contextparam: '+ keyValue.value);
        }    
    
        public void addparam(String key, String value)
        {
            AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = value != null ? value : '';
            jobParamDataEntry.keyValues.add(keyValue);
            System.debug('Touroptimizer Job param: '+ keyValue.value);
       }    
    
        public void callout()
        {
            String tenant = UserInfo.getOrganizationID().toLowerCase();
         
            contextParam.dataEntries = new AseService.aseDataEntry[0];
            contextParam.dataEntries.add(contextParamDataEntry);
            contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
            System.debug('#### Touroptimizer Job contextparam: ' + contextParamDataEntry);
 
    
            jobParam.dataEntries = new AseService.aseDataEntry[0];
            jobParam.dataEntries.add(jobParamDataEntry);
            jobParam.type_x = paramType; // AseCalloutConstants.ASE_TYPE_TOUROPTIMIZERJOB_STR;
            System.debug('#### Touroptimizer Job param: ' + jobParamDataEntry);
            
            AseService.aseDataType[] params = new AseService.aseDataType[]
            { 
                contextParam, jobParam
            };
            
            AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
            aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
            aseSoap.set(tenant, params);        
        }
    }    
    
    /**
     * Initialize the demand with the current user
     */
    private static String getUserID()
    {
        String userID = UserInfo.getUserId();
        User user = [select id, alias from user where user.id = :userID limit 1 ];
        return user.alias;
    }
   
 }

/*
    {
        SCTouroptimizerJob job = new SCTouroptimizerJob();
        
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        
        // prepare context
        AseService.aseDataType contextParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaContext)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValue(key) != null ? job.getValue(key) : '';
            System.debug('Context: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        contextParam.dataEntries = new AseService.aseDataEntry[0];
        contextParam.dataEntries.add(dataEntry);
        contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
        
        // job params
        AseService.aseDataType jobParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaParam)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValue(key) != null ? job.getValue(key) : '';
            System.debug('Touroptimizer Job param: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        jobParam.dataEntries = new AseService.aseDataEntry[0];
        jobParam.dataEntries.add(dataEntry);
        jobParam.type_x = AseCalloutConstants.ASE_TYPE_TOUROPTIMIZERJOB_STR;
        
        AseService.aseDataType[] params = new AseService.aseDataType[]
        { 
            contextParam, jobParam
        };
        
        AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
        aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
        aseSoap.set(tenant, params);
    }
  */