/*
 * @(#)CCWCMaterialPackageReceived.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * The class exports an Salesforce order close information into SAP
 * 
 * The entry method is: 
 *      callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
 *
 * Methods to override: 
 *      fillAndSendERPData(...)
 *      fillAndSendERPData(...)
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public without sharing class CCWCMaterialPackageReceived extends SCInterfaceExportBase 
{
    public static String MODE_FOR_ORDER = 'FOR_ORDER';
    public static String MODE_FOR_STOCK = 'FOR_STOCK';
    public static String MODE_FOR_PLANT = 'FOR_PLANT';
    public static String MODE_FOR_PACKAGE = 'FOR_PACKAGE';

    public ID orderID = null;
    
    public CCWCMaterialPackageReceived()
    {
    }
    public CCWCMaterialPackageReceived(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    // If there is a four parameter constructor, Salesforce expects three parameter constructor
    public CCWCMaterialPackageReceived(String interfaceName, String interfaceHandler, String refType)
    {
        super(interfaceName, interfaceHandler, refType, 'Dummy');
    }

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     *
     * @param matmoveid the material movement id of the package received record
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String matmovid, boolean async, boolean testMode)
    {
        String interfaceName = 'SAP_MATERIAL_PACKAGE_RECEIVED';
        String interfaceHandler = 'CCWCMaterialPackageReceived';
        String refType = 'SCStock__c';
        CCWCMaterialPackageReceived oc = new CCWCMaterialPackageReceived(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(matmovid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialPackageReceived', null, null, MODE_FOR_PACKAGE);
    } // callout   
    

    /**
     * Prepare to make the webservice callout synchrounously or asynchronously (future)
     * @param plantName    if the plantname = null the stockname param contains the stockid!
     * @param stockName    
     * @param mode
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     */
    public static String callout(String plantName, String stockName, String mode, boolean async, boolean testMode)
    {
        ID oid = null;
        String interfaceName = 'SAP_MATERIAL_PACKAGE_RECEIVED';
        String interfaceHandler = 'CCWCMaterialPackageReceived';
        String refType = 'SCStock__c';
        CCWCMaterialPackageReceived oc = new CCWCMaterialPackageReceived(interfaceName, interfaceHandler, refType);
        return oc.calloutCore(oid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCMaterialPackageReceived',
                       plantName, stockName, mode);
    } // callout   



    /**
     * Reads an material package to send
     *
     * Selection                                        
     *  5202 Material Reservation for an engineer Replenishment is null and with or without an order                                        
     *  5222 Material Replenishment Nachversorgung (Order is null) und Replenishment is not null                Selection           Processing          
     *  
     *  Parameter           Type    MaterialMovement Status     ERPStatus       ERPStatus   By Response 
     *  replenishment   replenishment__c != null        5222    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     *  reservation for order   replenishment__c == null    order__c != null    5202    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     *  reservation for stock   replenishment__c == null    order__c == null    5202    5402 Order      none    pending ok, error   Ordered 5403 by ok  
     * 
     * @param objectId in this case without meaning
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     *
     */
    public override void readSalesforceData(String objectID, Map<String, Object> retValue, Boolean testMode)
    {
        debug('plantName: ' + plantName + ', stockName: ' + stockName);
        List<SCMaterialMovement__c> mml = null;

        if(objectID != null && objectID != '')
        {
            // Read the material movement "spare part package received" 
            mml = [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, DeliveryNote__c,
                                        DeliveryQty__c
                                        from SCMaterialMovement__c
                                        where id = :objectID    //matmovid
                                        and Type__c = '5232'     
                                        and ERPStatus__c in ('none', 'exportpackage')
                                        and Article__c = null
                                        order by Stock__c limit 1];
            if(mml != null && mml.size() > 0)
            {                            
                setReferenceID(mml[0].Stock__c);
                setReferenceType('SCStock__c');
            }    
        }  
        else if(plantName != null && plantName != ''
          && stockName != null && stockName != '')
        {
            mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, DeliveryNote__c,
                                        DeliveryQty__c
                                        from SCMaterialMovement__c
                                        where Plant__c = : plantName and Stock__r.Name = : stockName
                                        and Type__c = '5232' 
                                        and (ERPStatus__c = 'none')
                                        and Article__c = null
                                        order by Stock__c];
        }  
        else if(plantName != null && plantName != '')
        {
            mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, DeliveryNote__c,
                                        DeliveryQty__c
                                        from SCMaterialMovement__c
                                        where Plant__c = : plantName 
                                        and Type__c = '5232' 
                                        and (ERPStatus__c = 'none')
                                        and Article__c = null
                                        order by Stock__c];
            
        }
        else if (stockName != null && stockName != '')
        {
            mml =  [Select Id, Name, Type__c, Plant__c, Stock__c, Stock__r.Name, DeliveryPlant__c, DeliveryStock__r.Name,
                                        Article__r.Name, Article__r.Unit__c, Qty__c, Order__r.ERPOrderNo__c, ValuationType__c, DeliveryNote__c,
                                        DeliveryQty__c
                                        from SCMaterialMovement__c
                                        where Stock__r.Name = : stockName
                                        and Type__c = '5232' 
                                        and (ERPStatus__c = 'none')
                                        and Article__c = null
                                        order by Stock__c];
        }
        retValue.put(ROOT, mml);
        debug('material movement list: ' + mml);                                        
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the order under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
        if(mml != null && mml.size() > 0)
        {
            // instantiate the client of the web service
            piCceagDeSfdcCSMaterialPackageReceived.HTTPS_Port ws = new piCceagDeSfdcCSMaterialPackageReceived.HTTPS_Port();
    
            // sets the basic authorization for the web service
            ws.inputHttpHeaders_x = u.getBasicAuth();
    
            // sets the endpoint of the web service
            ws.endpoint_x  = u.getEndpoint('MaterialPackageReceived');
            debug('endpoint: ' + ws.endpoint_x);
            rs.message_v1 = ws.endpoint_x;
            ws.timeout_x = u.getTimeOut();
            
            try
            {
                // instantiate a message header
                piCceagDeSfdcCSMaterialPackageReceived.BusinessDocumentMessageHeader messageHeader = new piCceagDeSfdcCSMaterialPackageReceived.BusinessDocumentMessageHeader();
                
                // instantiate the body of the call
                piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element[] packageElementArr = setPackage(mml,u, testMode);
                if(packageElementArr.size() > 0)
                {
                    setMessageHeader(messageHeader, mml, messageID, u, testMode);
                    
        
                    String jsonInputMessageHeader = JSON.serialize(messageHeader);
                    String fromJSONMapMessageHeader = getDataFromJSON(jsonInputMessageHeader);
                    debug('from json Message Header: ' + fromJSONMapMessageHeader);
        
                    String jsonInputPackageElement = JSON.serialize(packageElementArr);
                    String fromJsonInputPackageElement = getDataFromJSON(jsonInputPackageElement);
                    debug('from json material array: ' + fromJsonInputPackageElement);
                    
                    rs.message = '\n\nMessageHeader: ' + fromJSONMapMessageHeader + ',\n\nmessageData: ' + fromJsonInputPackageElement;
                    // check if there are missing mandatory fields
                    if(rs.message_v3 == null || rs.message_v3 == '')
                    {
                        // All mandatory fields are filled
                        // callout
                        if(!testMode)
                        {
                            // It is not the test from the test class
                            // go
                            rs.setCounter(1);
                            ws.SparePartPackageStatusUpdateRequest_Out(messageHeader, packageElementArr);
                            rs.Message_v2 = 'void';
                        }
                    }
                }
                else
                {
                    retValue = false;
                    // the interface log must not be written
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }    
        else
        {
            retValue = false;
        }
        return retValue;
    }
    
    /**
     * sets data into the message header
     * messageID, referenceID = order.Name, CreationDateTime, SenderBusinessSystemID, RecipientBusinessSystemID
     *
     * @param mh message header 
     * @param order 
     * @param messageID MS: milliseconds R: random, e.g.    MS:1351254139745-R:0.7486318721270884
     * @param u CCWSUtil instance
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     */
    public void  setMessageHeader(piCceagDeSfdcCSMaterialPackageReceived.BusinessDocumentMessageHeader mh, 
                                         List<SCMaterialMovement__c> mml, String messageID, CCWSUtil u, Boolean testMode)
    {
        if(mml != null && mml.size() > 0)
        {
            mh.MessageID = messageID;
    //      mh.MessageUUID;
            mh.ReferenceID = mml[0].Name; //###? Ask Norbert
    //      mh.ReferenceUUID;
            mh.CreationDateTime = u.getFormatedCreationDateTime();
            if(testMode)
            {
                mh.TestDataIndicator = 'Test';
            }    
            mh.SenderBusinessSystemID = u.getSenderBusinessSystemID();
            mh.RecipientBusinessSystemID = u.getRecipientBusinessSystemID ();
            debug('Message Header: ' + mh);
        }    
    }

    /**
     * sets the call out order structure with data form an order
     *
     * @param mml list of material movements
     * @param u instance of the util class CCWSUtil
     * @param testMode used to prevent call out by calls from CCWCOrderCloseTest class
     *
     * @return SparePartPackage_element[] 
     *
     */
    public piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element[] setPackage(List<SCMaterialMovement__c> mml, CCWSUtil u, Boolean testMode)
    {
        List<piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element> retValue = new List<piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element>();
        if(mml != null)
        {
            String step = '';
            try
            {
                // default interface Log
                ID defInterfaceLogId = null;
                if(interfaceLogList.size() > 0 && interfaceLogList[0].id != null)
                {
                    defInterfaceLogId = interfaceLogList[0].id; 
                }
                for(SCMaterialMovement__c mm: mml)
                {
                    ID loopInterfaceLogId = getInterfaceLogId(interfaceLogList, mm.Stock__c); 
                    if(loopInterfaceLogId == null)
                    {
                        loopInterfaceLogId = defInterfaceLogId; 
                    }
                    if(loopInterfaceLogId != null)
                    {
                        mm.InterfaceLog__c = loopInterfaceLogId;
                    }
                    piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element mp = new piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element();
                    mp.PackageNumber = mm.DeliveryNote__c;
                    mp.Plant = mm.Plant__c;
                    mp.Status = '';
                    if(mm.DeliveryQty__c == mm.Qty__c)
                    {
                        mp.Status = 'S'; // Success
                    }
                    else
                    {
                        mp.Status = 'E'; // 'delivered: ' + mm.DeliveryQty__c + ' , expected: ' + mm.Qty__c;
                    }
                    retValue.add(mp);
                }
            }
            catch(Exception e)
            {
                String prevMsg = e.getMessage();
                String newMsg = 'setPackage: ' + step + ' ' + prevMsg;
                debug(newMsg);
                e.setMessage(newMsg);
                throw e;
            }
        }    
        return retValue;    
    }

    /**
     * Sets the ERPStatus in the root object list to 'pending'
     * 
     * @param dataMap of obects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has been occured
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
        // TODO: #### What about ERPStatus?
        if(dataMap != null)
        {
            List<SCMaterialMovement__c> mml = (List<SCMaterialMovement__c>)dataMap.get(ROOT);
            if(mml != null)
            {
                String status = null;
                for(SCMaterialMovement__c mr: mml)
                {
                    if(error)
                    {
                        mr.ERPStatus__c = 'error';
                    }
                    else
                    {   
                        mr.ERPStatus__c = 'pending';
                    }
                    // we do not override error status with pending
                    // if there is only one error in material movement then the flag in order for all is also set to error
                    if(status == null || status != 'error')
                    {
                        status = mr.ERPStatus__c;                       
                    }
                }       
                update mml;
                debug('materal movement after update: ' + mml);
//                if(orderId != null)
//                {
//                    updateOrder(orderId, status);
//                }               
            }
        }
    }
    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}