/*
 * @(#)SCfwOrderControllerBaseTest.cls SCCloud  
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwOrderControllerBaseTest 
{
    static testMethod void SCfwOrderControllerBaseConstructor()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.assertNotEquals(null, base.userLanguage);
        System.assertNotEquals(null, base.hasContract);
        System.assertNotEquals(null, base.getController());
        Boolean isMatAvail = base.isMatAvailCheck;
        Boolean bContr = base.isContractEnabled;
        base.appSettings.OrderRoleMode__c = 'SRCA';
        upsert base.appSettings;
        SCfwOrderControllerBase base2 = new SCfwOrderControllerBase();
        base.appSettings.OrderRoleMode__c = 'CASR';
        upsert base.appSettings;
    }

    static testMethod void testOrderRoleObj()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet3(true);       
        SCHelperTestClass.orderRoles[0].RiskClass__c = '51002';
        SCHelperTestClass.orderRoles[0].LockType__c = '50002';    
        SCHelperTestClass.orderRoles[0].Description__c = 'Descr';
        SCHelperTestClass.orderRoles[1].RiskClass__c = '51002';
        SCHelperTestClass.orderRoles[1].LockType__c = '50002';    
        SCHelperTestClass.orderRoles[1].Description__c = 'Descr';
        SCHelperTestClass.orderRoles[2].RiskClass__c = '51002';
        SCHelperTestClass.orderRoles[2].LockType__c = '50002';    
        SCHelperTestClass.orderRoles[2].Description__c = 'Descr';
        SCHelperTestClass.orderRoles[3].RiskClass__c = '51002';
        SCHelperTestClass.orderRoles[3].LockType__c = '50002';    
        SCHelperTestClass.orderRoles[3].Description__c = 'Descr';
        upsert SCHelperTestClass.orderRoles;

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);
        Map<String, SCfwOrderControllerBase.OrderRoleObj> mapRoles = base.getOrderRoleMap(boOrder);

        for (SCfwOrderControllerBase.OrderRoleObj ordObj :mapRoles.values())
        {
            ordObj.getIsEditable();
            Boolean bDel = ordObj.isDeletable;
            Boolean bAcc = ordObj.isPersonAccount;
            String image = ordObj.imageAlert;
            image = ordObj.imageVipContract;
            image = ordObj.imageCasesOrders;
            Boolean detail = ordObj.getHasDetailInfos();
            Boolean flag = ordObj.isLE;
            flag = ordObj.isAG;
            flag = ordObj.isRE;
            flag = ordObj.isRG;
            ordObj.getAddress();
            ordObj.getRoleTypes();
        }
        mapRoles = base.getOrderRoleMap(boOrder);
        System.assertNotEquals(null, mapRoles);

        SCfwOrderControllerBase.OrderRoleObj ordObj = new SCfwOrderControllerBase.OrderRoleObj();
        Test.stopTest();
    }

    static testMethod void processCustomer()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);   

        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        Map<String, SCfwOrderControllerBase.OrderRoleObj> mapRoles = base.getOrderRoleMap(boOrder);
        base.processCustomer(boOrder, SCHelperTestClass.account.Id, SCfwConstants.DOMVAL_ORDERROLE_AG);               
        base.processCustomer(boOrder, SCHelperTestClass.account.Id, SCfwConstants.DOMVAL_ORDERROLE_LE);               
        base.processCustomer(boOrder, SCHelperTestClass.account.Id, SCfwConstants.DOMVAL_ORDERROLE_RE);               
        mapRoles = base.getOrderRoleMap(boOrder);
        System.assertNotEquals(null, mapRoles);
        Test.stopTest();
    }

    static testMethod void changeRole()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);   

        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.changeRole(boOrder, SCHelperTestClass.account.Id);               
        Map<String, SCfwOrderControllerBase.OrderRoleObj> mapRoles = base.getOrderRoleMap(boOrder);
        System.assertNotEquals(null, mapRoles);
        Test.stopTest();
    }

    static testMethod void delRole()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);   

        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        ApexPages.currentPage().getParameters().put('type', SCfwConstants.DOMVAL_ORDERROLE_LE);
        base.delRole(boOrder);              
        ApexPages.currentPage().getParameters().put('type', SCfwConstants.DOMVAL_ORDERROLE_RE);
        base.delRole(boOrder);              
        Map<String, SCfwOrderControllerBase.OrderRoleObj> mapRoles = base.getOrderRoleMap(boOrder);
        System.assertNotEquals(null, mapRoles);
        Test.stopTest();
    }

    static testMethod void addNewOrderItem()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);   
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);
        
        SCInstalledBase__c ib = new SCInstalledBase__c(
                                            Article__c = SCHelperTestClass.article.Id,
                                            Article__r = SCHelperTestClass.article,
                                            SerialNo__c = 'AFDSC1235-678',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductUnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                            ProductUnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT
                                    );
        insert ib;

        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        ib.Id, 
                                        true);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.isContractEnabled = true;
        base.deletedItem = '1';
        base.delItem(boOrder);
        System.assertEquals(0, boOrder.boOrderItems.size());

        base.newInstalledBase = ib.Id; 
        base.isContractEnabled = true;
        base.addNewOrderItem(boOrder);
        System.assertEquals(1, boOrder.boOrderItems.size());
        base.initContracts(boOrder, ib.Id);
        base.selectContract(boOrder);
        Test.stopTest();
    }

    static testMethod void chgOrderItem()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        boOrder.createDefaultRoles(SCHelperTestClass.Account.Id);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrderItem.orderItem.ID2__c = '1';
        boOrder.boOrderItems.add(boOrderItem);
        
        SCInstalledBase__c ib = new SCInstalledBase__c(
                                            Article__c = SCHelperTestClass.article.Id,
                                            Article__r = SCHelperTestClass.article,
                                            SerialNo__c = 'AFDSC1235-678',
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = SCHelperTestClass.location.Id,
                                            Type__c = 'Appliance',
                                            ProductUnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                            ProductUnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT
                                    );
        insert ib;

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.curOrderItem = boOrderItem; 
        base.selectedItem = '1'; 
        base.newInstalledBase = ib.Id; 
        base.isContractEnabled = true;
        base.chgOrderItem(boOrder);
        System.assertEquals(1, boOrder.boOrderItems.size());
        base.updateOrderItem(boOrder);
        Boolean useProdSkill = base.useProductSkill;
        Test.stopTest();
    }

    static testMethod void deleteAllItems()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.deleteAllItems(boOrder);
        System.assertEquals(0, boOrder.boOrderItems.size());
        Test.stopTest();
    }

    static testMethod void deleteItem()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.isContractEnabled = true;
        boOrderItem.orderItem.ID2__c = '';
        base.deletedItem = '1';
        base.delItem(boOrder);
        System.assertEquals(1, boOrder.boOrderItems.size());
        boOrderItem.orderItem.ID2__c = '';
        base.deletedItem = '';
        base.delItem(boOrder);
        System.assertEquals(0, boOrder.boOrderItems.size());
        Test.stopTest();
    }

    static testMethod void getErrSym()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.curOrderItem = boOrderItem;
        System.assertNotEquals(null, base.getErrSym1());
        System.assertNotEquals(null, base.getErrSym2());
        base.curOrderItem.orderItem.InstalledBase__r.ProductGroup__c = null;
        System.assertNotEquals(null, base.getErrSym1());
        System.assertNotEquals(null, base.getErrSym2());
        Test.stopTest();
    }

    static testMethod void getAssignedInstBases()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.assertNotEquals(null, base.getAssignedInstBases(boOrder));
        Test.stopTest();
    }
    
    static testMethod void titlePage()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.curOrderItem = boOrderItem;
        String title = base.titleHistory;
        System.AssertNotEquals(null, title);
        title = base.titleError;
        System.AssertNotEquals(null, title);
        title = base.titleInstBase;
        System.AssertNotEquals(null, title);
        title = base.titleProdSafety;
        System.AssertNotEquals(null, title);
        Test.stopTest();
    }     

    static testMethod void getBoOrderLines()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrderItem.boOrderLines.add(SCHelperTestClass.boOrderLine);
        boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.AssertNotEquals(null, base.getBoOrderLines(boOrder));
        Test.stopTest();
    }     

    static testMethod void checkOrderLine()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.AssertEquals(null, base.checkOrderLine(null));
        System.AssertNotEquals(null, base.checkOrderLine(SCHelperTestClass.boOrderLine).orderLine.Article__r);
        Test.stopTest();
    }     

    static testMethod void getOrderItemFromList()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet2(true);   
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.curSelItem = '1';
        System.AssertNotEquals(null, base.getOrderItemFromList(boOrder));
        Test.stopTest();
    }     

    static testMethod void removeOrderLine()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);   
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem.Id);
        SCHelperTestClass.boOrderLine.orderLine.OrderItem__c = SCHelperTestClass.orderItem.Id;
        upsert SCHelperTestClass.boOrderLine.orderLine;
        // boOrderItem.boOrderLines.add(SCHelperTestClass.boOrderLine);
        
        SCOrderRepairCode__c repairCode = new SCOrderRepairCode__c();
        repairCode.Order__c                  = SCHelperTestClass.order.Id;
        repairCode.OrderItem__c              = SCHelperTestClass.orderitem.Id;
        repairCode.ErrorLocation1__c         = '01';
        repairCode.ErrorLocation2__c         = '001';
        repairCode.ErrorType1__c             = '001';
        repairCode.ErrorType1New__c          = 'Text';
        repairCode.ErrorType2__c             = '999';
        repairCode.ErrorActivityCode1__c     = '01';
        repairCode.ErrorActivityCode1New__c  = 'Text';
        repairCode.ErrorActivityCode2__c     = '99';
        repairCode.ErrorDescription__c       = 'Desc';
        repairCode.ErrorCausing__c           = true;
        insert repairCode;
        // boOrderItem.boRepairCodes.add(new SCboOrderRepairCode(repairCode));
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);
        // boOrder.boOrderItems.add(boOrderItem);

        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('currentLineRow', '0');
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.removeOrderLine(boOrder);
        System.assertEquals(0, boOrder.boOrderLines.size());
        Test.stopTest();
    }     

    static testMethod void addPayment()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass2.createArticleOrderlinePayment();
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);   

        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.AssertNotEquals(null, base.getOrderLineTypes());
        base.onAddPayment(boOrder);
        Boolean bContr = base.getIsContractCreateRequired(boOrder);
        Test.stopTest();
    }     

    static testMethod void testPricing1()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);   

        Test.startTest();
        
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        boOrderItem.boOrderLines.add(SCHelperTestClass.boOrderLine);
        boOrder.boOrderItems.add(boOrderItem);
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.getPriceLists(boOrder);
        // base.currentLineRow = 0.0;
        ApexPages.currentPage().getParameters().put('changedField', 'Discount');
        base.calculatePricing(boOrder);
        ApexPages.currentPage().getParameters().put('changedField', 'RelativeDiscount');
        base.calculatePricing(boOrder);
        ApexPages.currentPage().getParameters().put('changedField', 'ListPrice');
        base.calculatePricing(boOrder);
        
        System.assertNotEquals(null, base.sumPriceGross);
        Test.stopTest();
    }     

    static testMethod void testPricing2()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);   

        Test.startTest();
        
        SCboOrder boOrder = new SCboOrder(SCHelperTestClass.order);
        SCboOrderItem boOrderItem = new SCboOrderItem(SCHelperTestClass.orderItem);
        SCHelperTestClass.boOrderLine.orderLine.Price__c = 100.0;
        boOrderItem.boOrderLines.add(SCHelperTestClass.boOrderLine);
        boOrder.boOrderItems.add(boOrderItem);
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        base.pricing = new SCPriceCalculationCustom(boOrder);
        base.currentLineRow = 0.0;
        ApexPages.currentPage().getParameters().put('changedField', 'Price');
        base.calculatePricing(boOrder);
        ApexPages.currentPage().getParameters().put('changedField', 'UnitNetPrice');
        base.calculatePricing(boOrder);
        ApexPages.currentPage().getParameters().put('changedField', 'TaxRate');
        base.calculatePricing(boOrder);

        List<SCboOrderLine> lines = new List<SCboOrderLine>();
        lines.add(SCHelperTestClass.boOrderLine);
        base.recalculatePricing(boOrder, boOrderItem.boOrderLines);
        System.assertNotEquals(null, base.sumPriceNet);
        Test.stopTest();
    }

    static testMethod void testPaymentService()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet(true);   
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(SCHelperTestClass.order.Id);

        Test.startTest();
        
        SCfwOrderControllerBase base = new SCfwOrderControllerBase();
        System.AssertEquals(false, base.isPrePayment);
        System.AssertEquals(true, base.isPostPayment);
        System.AssertNotEquals(null, base.userCountry);
        base.getEnableDistanceZone();
        System.assertNotEquals(null, base.getArticleClassService());
        System.assertNotEquals(null, base.getCreditCardType());

        base.doPaymentService(boOrder, 'OK');
        System.assertNotEquals(null, base.errMsg);
        
        base.cardType = '376004';
        base.cardNumber = '123';
        base.expiryDate = '10/2012';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.cardHolder = 'John Smith';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.cardNumber = '4111111111111111';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.securityCode = '123';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.startDate = '10/2011';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.startDate = '9/20111';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.startDate = 'A9/2011';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.startDate = '10/201A';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.startDate = '10/11';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertNotEquals(null, base.errMsg);
        
        base.expiryDate = '10/12';
        base.doPaymentService(boOrder, 'OK');
        //CCE System.assertEquals(null, base.errMsg);
        
        base.doPaymentService(boOrder, 'ERROR');
        //CCE System.assertNotEquals(null, base.errMsg);

        Test.stopTest();
    }
}