/*
 * @(#)SCAddressServiceImplQASTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/** 
 * Additional tests for the webserivce.nl integration. 
 */
@isTest
private class SCAddressServiceImplQASTest {
    
    // basic tests - see SCAddressServiceImplQAS


    // ensure testcoverage of web service wrapper 
    static testMethod void checkServiceDummy(){

        // web services can't be tested directly - the following code is just for 
        // increasing coverage no real test possible here
        wwwQasComOndemand201001.QAQueryHeader qAQueryHeader = new wwwQasComOndemand201001.QAQueryHeader();
        wwwQasComOndemand201001.QAAuthentication qAAuthentication =  new wwwQasComOndemand201001.QAAuthentication();
        wwwQasComOndemand201001.QASOnDemand wsQAS = new wwwQasComOndemand201001.QASOnDemand();

        SCAddressServiceImplQAS  asQAS = new SCAddressServiceImplQAS();
        
        
        try
        {
            asQAS.authentication(wsQAS);   
        }
        catch(Exception e) {}

        try
        {
            wwwQasComOndemand201001.QALicenceInfo_element e1 =  wsQAS.DoGetLicenseInfo();
        }
        catch(Exception e) {}

        wwwQasComOndemand201001.EngineType engine;
        try
        {        
            engine = new wwwQasComOndemand201001.EngineType();
            engine.Flatten = true;
            engine.Intensity = 'Exact';
            engine.PromptSet = 'Default';
            engine.Threshold = 25;
            engine.Timeout = 10000;
        }
        catch(Exception e) {}
        
        wwwQasComOndemand201001.QASearchType bulkSearchTerm = null;
        try
        {
            bulkSearchTerm = new  wwwQasComOndemand201001.QASearchType();
        }
        catch(Exception e) {}

        try
        {
            wwwQasComOndemand201001.QABulkSearchResult_element res1 = wsQAS.DoBulkSearch
                ('GB',engine,'Salesforce', bulkSearchTerm);
        }
        catch(Exception e) {}
 
        try
        {
            wwwQasComOndemand201001.QADataSet[] res2 = wsQAS.DoGetData();
        }
        catch(Exception e) {}
 
        try
        {
            wwwQasComOndemand201001.QASearchResult_element wsResult = 
                                wsQAS.DoSearch('GB', engine, 'Salesforce', 'something');
        }
        catch(Exception e) {}
        
        try
        {
            wwwQasComOndemand201001.QALayout[] res3 = wsQAS.DoGetLayouts('GBR');
        }
        catch(Exception e) {}
        
        try
        {
            wwwQasComOndemand201001.QAExampleAddress[] res4 = wsQAS.DoGetExampleAddresses('GBR','Salesforce');
        }
        catch(Exception e) {}
        
        try
        {
            String[] res5 = wsQAS.DoGetSystemInfo();
        }
        catch(Exception e) {}
        
        try
        {
            wwwQasComOndemand201001.QAPicklistType res6 = wsQAS.DoRefine('Moniker','Refinement','Layout');
        }
        catch(Exception e) {}
        
        try
        {
            wwwQasComOndemand201001.QADataMapDetail_element res7 = wsQAS.DoGetDataMapDetail('DataMap');
        }
        catch(Exception e) {}

        try
        {
            wwwQasComOndemand201001.QAAddressType res8 =  wsQAS.DoGetAddress('Layout','Moniker');
        }
        catch(Exception e) {}

        try
        {
            wwwQasComOndemand201001.QASearchOk_element res9 = wsQAS.DoCanSearch('Country', engine, 'Layout');
        }
        catch(Exception e) {}
        
        try
        {
            wwwQasComOndemand201001.PromptLine[] res10 = wsQAS.DoGetPromptSet('Country',engine, 'PromptSet');
        }
        catch(Exception e) {}
        
        AvsAddress addr = new AvsAddress();
        addr.postalcode = 'B73 5HU';
        try
        {
             AvsResult res11 = asQAS.check(addr);
        }
        catch(Exception e) {}

        try
        {
             AvsResult res11 = asQAS.checkEmulation(addr);
        }
        catch(Exception e) {}
        
        try
        {
             AvsResult res11 = asQAS.checkReal(addr);
        }
        catch(Exception e) {}

        try
        {
             AvsResult res11 = asQAS.geocode(addr);
        }
        catch(Exception e) {}

        try
        {
             AvsResult res11 = asQAS.geocodeEmulation(addr);
        }
        catch(Exception e) {}
        
        try
        {
             AvsResult res11 = asQAS.geocodeReal(addr);
        }
        catch(Exception e) {}
        
    }
}