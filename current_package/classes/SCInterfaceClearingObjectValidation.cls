/**
 * @(#)SCInterfaceClearingObjectValidation
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * Validate any sObjects
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInterfaceClearingObjectValidation 
{

    
    /*public static final String ORDER = 'SCOrder__c';
    public static final String ORDERROLE = 'SCOrderRole__c';
    public static final String ORDERITEM = 'SCOrderItem__c';
    public static final String ORDERLINE = 'SCOrderLine__c';
    public static final String INSTALLEDBASE = 'SCInstalledBase__c';
    public static final String ORDERREPAIRCODE = 'SCOrderRepairCode__c';*/
    public static final String OBJECT_TIMEREPORT = 'SCTimeReport__c';
    public static final String OBJECT_MATERIALMOVEMENT = 'SCMaterialMovement__c';
    public static final String OBJECT_CORRUPT = 'SCTool__c';
    
    
    private sObject obj;
    private String objName;
    private String objLabel;
    private Integer validationMode = 0;
    
    public Boolean showMessageValid = true;
    public String errorMessage {get; private set;}
    
    public SCInterfaceClearingObjectValidation(sObject obj)
    {
        system.debug('#### validate object: ' + obj);
        if(obj == null)
        {
            return;
        }
        this.obj = obj;
        
        //read sobject info
        Schema.DescribeSObjectResult dR = obj.getSObjectType().getDescribe();
        
        //base names
        this.objName = dR.getName();
        this.objLabel = dR.getLabel();
        
    }
    
    public String getObjName()
    {
        return this.objName;
    }
    
    public Integer getValidationMode()
    {
        return this.validationMode;
    }
    
    public SCOrder__c getOrder()
    {
        if(!this.objName.equals(SCboOrder.OBJECT_ORDER))
        {
            return null;
        }
        return (SCOrder__c) obj;
    }
    
    public SCOrderRole__c getOrderRole()
    {
        if(!this.objName.equals(SCboOrder.OBJECT_ORDERROLE))
        {
            return null;
        }
        return (SCOrderRole__c) obj;
    }
    
    public SCOrderLine__c getOrderLine()
    {
        if(!this.objName.equals(SCboOrder.OBJECT_ORDERLINE))
        {
            return null;
        }
        return (SCOrderLine__c) obj;
    }
    
    public SCOrderRepairCode__c getOrderRepairCode()
    {
        if(!this.objName.equals(SCboOrder.OBJECT_ORDERREPAIRCODE))
        {
            return null;
        }
        return (SCOrderRepairCode__c) obj;
    }
    
    public SCInstalledBase__c getInstalledBase()
    {
        if(!this.objName.equals(SCboOrder.OBJECT_INSTALLEDBASE))
        {
            return null;
        }
        return (SCInstalledBase__c) obj;
    }
    
    public SCTimeReport__c getTimeReport()
    {
        if(!this.objName.equals(OBJECT_TIMEREPORT))
        {
            return null;
        }
        return (SCTimeReport__c) obj;
    }
    
    public SCMaterialMovement__c getMaterialMovement()
    {
        if(!this.objName.equals(OBJECT_MATERIALMOVEMENT))
        {
            return null;
        }
        return (SCMaterialMovement__c) obj;
    }
    
    
    
    
    public Boolean validate()
    {
        //writeMessageNotValid('error testing');
        //return false;
        //Mobile Order
        if(this.objName.equals(OBJECT_CORRUPT))
        {
            this.errorMessage = 'Object corrupt';
            return false;
        }
        
        Boolean result = true;
        if(this.objName.equals(SCboOrder.OBJECT_ORDER))
        {
            this.validationMode = SCboOrderValidation.ORDER_DATA;
            result = validateOrder();

        }
        else if(this.objName.equals(SCboOrder.OBJECT_ORDERROLE))
        {
            this.validationMode = SCboOrderValidation.ROLE_DATA;
            this.validationMode |= SCboOrderValidation.ROLE_ADDRESS_DATA;
            this.validationMode |= SCboOrderValidation.ROLE_GEO_DATA;
            
            result = validateOrderRole();

        }
        else if(this.objName.equals(SCboOrder.OBJECT_ORDERITEM))
        {
            validationMode = SCboOrderValidation.ORDERITEM_DATA;
            
            result = validateOrderItem();
        }
        else if(this.objName.equals(SCboOrder.OBJECT_ORDERLINE))
        {
            validationMode = SCboOrderValidation.ORDERLINE_DATA;
            
            result = validateOrderLine();
        }
        else if(this.objName.equals(SCboOrder.OBJECT_ORDERREPAIRCODE))
        {
            validationMode = SCboOrderValidation.REPAIRCODE_DATA;
            
            result = validateOrderRepairCode();
        }
        else if(this.objName.equals(SCboOrder.OBJECT_INSTALLEDBASE))
        {
            validationMode = SCboOrderValidation.INSTBASE_DATA;
            
            result = validateInstalledBase();
        }
        else if(this.objName.equals(OBJECT_TIMEREPORT))
        {
            result = validateTimeReport();
        }
        else if(this.objName.equals(OBJECT_MATERIALMOVEMENT))
        {
            result = validateMaterialMovement();
        }
        
        return result;      
        
    }
    
    public Boolean validateOrder()
    {
        SCOrder__c ord = getOrder();
        SCboOrder boOrder = new SCboOrder(ord);
        try
        {
            SCboOrderValidation.validate(boOrder, validationMode);
            writeMessageValid();
            
            return true; 
        }
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage());  
        }
        return false;
        
    }
    
    
    public Boolean validateOrderRole()
    {
        SCOrderRole__c role = getOrderRole() ;
        try
        {
            SCboOrderRoleValidation.validate(getOrderRole(), validationMode);
            writeMessageValid();
            
            return true;
        }
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage());  
        }
        return false;
    }
    
    public Boolean validateOrderItem()
    {
        SCOrderItem__c item = (SCOrderItem__c) obj;
        SCboOrderItem boOrderItem = new SCboOrderItem(item);
        try
        {
            SCboOrderItemValidation.validate(boOrderItem, validationMode);
            writeMessageValid();
            
            return true;
        }
        
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage()); 
        }
        return false;
    }
    
    public Boolean validateOrderLine()
    {
        SCOrderLine__c oLine = getOrderLine();
        SCboOrderLine boOrderLine = new SCboOrderLine(oLine);
        
        //GMSET 17.07.2013
        //Check material movement reference with external id
        /*if(oLine.ID2__c != null)
        {
            SCMaterialMovement__c materialMovement = SCboInterfaceClearing.getMaterialMovementsById2OrderLine(oLine.ID2__c);
            if(materialMovement == null)
            {
                writeMessageNotValid('Materialbewegung für Leistungsposition nicht gefunden. ID2 = ' + oLine.ID2__c);
                //writeMessageNotValid('MaterialMovement not found for the given orderline. ID2 = ' + oLine.ID2__c);
                return false;
            }
        
        }*/
            
        
        try
        {
            SCboOrderLineValidation.validate(boOrderLine, validationMode);
            writeMessageValid();
           
            return true;
        }
        
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage()); 
        }
        return false;
    }
    
    public Boolean validateInstalledBase()
    {
        SCInstalledBase__c iBase = getInstalledBase();
        SCboOrderItem boOrderItem = new SCboOrderItem();
        boOrderItem.orderItem.installedBase__r = iBase;
        
        try
        {
            SCboOrderItemValidation.validate(boOrderItem, validationMode);
            writeMessageValid();
            
            return true;
        }
        
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage()); 
        }
        return false;
    }
    
    public Boolean validateOrderRepairCode()
    {
        SCOrderRepairCode__c rCode = getOrderRepairCode();
        SCboOrderItem boOrderItem = new SCboOrderItem();
        boOrderItem.boRepairCodes.add(new SCboOrderRepairCode(rCode));
        
        try
        {
            SCboOrderItemValidation.validate(boOrderItem, validationMode);
            writeMessageValid();
            
            return true;
        }
        
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage()); 
        }
        return false;
    }
    
    
    public Boolean validateTimeReport()
    {
        SCTimeReport__c tR = getTimeReport();
        Boolean isValid = true;
         
        if(tR.Resource__c == null)
        {
            if(isPage())
            {
                tR.Resource__c.addError('required field');
            }
            else
            {
                writeMessageNotValid('Resource not set');   
            }
            
            isValid = false;
        }
        if(tR.Start__c == null)
        {
            if(isPage())
            {
                tR.Start__c.addError('required field');
            }
            else
            {
                writeMessageNotValid('Start not set');  
            }
            
            isValid = false;
        }
        if(tR.End__c == null)
        {
            if(isPage())
            {
                tR.End__c.addError('required field');
            }
            else
            {
                writeMessageNotValid('End not set');    
            }
            
            
            isValid = false;
        }
        
        
        if(tR.End__c < tR.Start__c)
        {
            isValid = false;
            writeMessageNotValid('End < Start');
             
        }
        
        if(tR.Status__c == null)
        {
            if(isPage())
            {
                tR.Status__c.addError('required field');
            }
            else
            {
                writeMessageNotValid('Status not set'); 
            }
            
            isValid = false;
        }
        
        if(tR.Type__c == null)
        {
            if(isPage())
            {
                tR.Type__c.addError('required field');
            }
            else
            {
                writeMessageNotValid('Status not set'); 
            }
            
            isValid = false;
        }
        
        if(isValid)
        { 
            
            writeMessageValid(tR.Start__c.format());
        }
        
        //writeMessageNotValid('test error');
        //return false;
        return isValid;
    }
    
    public Boolean validateMaterialMovement()
    {
        SCMaterialMovement__c matMove = getMaterialMovement();
        
        Boolean isValid = true;
        try
        {
            if(matMove.Stock__c == null)
            {
                if(isPage())
                {
                    matMove.Stock__c.addError('required field');
                }
                else
                {
                    //writeMessageNotValid('Stock not set');
                }
                isValid = false;
            }
            // the article number us required - beside the confirmation of the package (where we have no parts to book)
            if (matMove.Article__c == null && matMove.Type__c != SCfwConstants.DOMVAL_MATMOVETYP_SPAREPART_PACKAGE_RECEIVED)
            {
                if(isPage())
                {
                    matMove.Article__c.addError('required field');
                }
                else
                {
                    //writeMessageNotValid('Article not set');
                }
                isValid = false;
            }
            if(matMove.Qty__c == null)
            {
                if(isPage())
                {
                    matMove.Qty__c.addError('required field');  
                }
                else
                {
                    //writeMessageNotValid('Qty not set');
                }
                
                
                isValid = false;
            }
            if(matMove.Status__c == null)
            {
                if(isPage())
                {
                    matMove.Status__c.addError('required field');
                }
                else
                {
                    //writeMessageNotValid('Status not set');
                }
                isValid = false;
            }
            if(matMove.Type__c == null)
            {
                if(isPage())
                {
                    matMove.Type__c.addError('required field');
                }
                else
                {
                    //writeMessageNotValid('Type not set'); 
                }
                
                isValid = false;
            }
            
            
            if(isValid)
            {
                writeMessageValid();
            }
            
            return isValid;
        }
        
        catch(Exception e)
        {
            writeMessageNotValid(e.getMessage()); 
        }
        return isValid;
    }
    
   
    private void writeMessageValid()
    {
        String name = String.valueOf(obj.get('Name'));
        
        writeMessageValid(name);
    }
    
    private void writeMessageValid(String name)
    {
        if(!showMessageValid || !isPage())
        {
            return;
        }
        
        String message;
        if(name != null)
        {
            message = this.objLabel + ' (' + name + ') : Valid'; 
        }
        else
        {
            message = this.objLabel + ' Valid'; 
        }
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Confirm, message);
        ApexPages.addMessage(msg); 
    }
    
    public void writeMessageNotValid(String errMsg)
    {
       
        
        String name = String.valueOf(obj.get('Name'));
        String message;
        if(name != null)
        {
            message = this.objLabel + ' (' + name + ') : ' + errMsg; 
        }
        else
        {
            message = this.objLabel + ': ' + errMsg; 
        }
        
        this.errorMessage = message; 
        
        if(!isPage())
        {
            return;
        }
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, message);
        ApexPages.addMessage(msg);
        
    }
    
    private static Boolean isPage()
    {
        return Apexpages.currentPage() != null;
    }
    
}