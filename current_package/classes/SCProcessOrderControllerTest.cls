/*
 * @(#)SCProcessOrderControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCProcessOrderControllerTest 
{
    static testMethod void testConstructor()
    {
         SCHelperTestClass3.createCustomSettings('DE', true);
         SCHelperTestClass.createDomsForOrderCreation();
        
         //Add parameters to page URL
         SCHelperTestClass.createOrderTestSet(true);
         Id appointmentId = SCHelperTestClass.appointmentsSingle1[0].Id ;          
         ApexPages.currentPage().getParameters().put('aid', appointmentId);
         SCProcessOrderController controller = new  SCProcessOrderController ();
         System.assert(controller.currentOrderItem != null);
         
         // for code coverage
         SCOrderTabComponentController ordTabController = controller.orderComponentController;
         controller.getOrderComponentKey();
         System.assertEquals(UserInfo.getLanguage().substring(0,2), controller.getUserLanguage());
    }
    
    /**
     * Tests basic things.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testTimeReports()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[5].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id,
                                              true);
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, true);
        
        Test.startTest();
        System.runAs(SCHelperTestClass.users[0])
        {
            Id appointmentId = SCHelperTestClass.appointments[0].Id ;          
            ApexPages.currentPage().getParameters().put('aid', appointmentId);
            SCProcessOrderController controller = new  SCProcessOrderController();
            
            List<SelectOption> assList = controller.getAssignmentList();
            System.debug('#### testTimeReports: assList -> ' + assList);
            System.assertEquals(true, assList.size() > 0);
            List<SelectOption> dayList = controller.getDayList();
            System.assertEquals(true, dayList.size() > 0);
            System.assertEquals(true, controller.timeReportMap.size() > 0);

            System.assertEquals(true, controller.curAssignment.hasStock());
            String stockInfos = controller.getStockInfos();
            System.assertNotEquals(null, stockInfos);
            System.assertNotEquals('-/-', stockInfos);
            System.assertEquals(true, stockInfos.length() > 0);
            
            System.assertEquals(true, controller.getIsTimeReportEditable());
            
            // set the time report data, because they are mandatory
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 11, 30, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            
            String lastSelection = controller.selectedAssignment;
            controller.selectedAssignment = String.valueOf(0);
            controller.initAssignment();
            controller.initTimeReportData();
            System.assertEquals(false, controller.getIsTimeReportEditable());
            
            controller.selectedAssignment = lastSelection;
            controller.initAssignment();
            controller.initTimeReportData();
            System.assertEquals(true, controller.getIsTimeReportEditable());
        }
        Test.stopTest();
    } // testTimeReports
    
    /**
     * Tests editing time report data.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testEditTimeReports()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[5].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id,
                                              true);
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, true);
        SCHelperTestClass.installedBaseSingle.InstallationDate__c = 
                        System.today() - 10;
        upsert SCHelperTestClass.installedBaseSingle;
        SCOrderRepairCode__c repairCode = new SCOrderRepairCode__c(Order__c = SCHelperTestClass.order.Id, 
                                                                   OrderItem__c = SCHelperTestClass.orderitem.Id);
        insert repairCode;
        
        Test.startTest();
        System.runAs(SCHelperTestClass.users[0])
        {
            Id appointmentId = SCHelperTestClass.appointments[0].Id ;          
            ApexPages.currentPage().getParameters().put('aid', appointmentId);
            SCProcessOrderController controller = new  SCProcessOrderController();
            
            System.assertEquals(true, controller.getIsTimeReportEditable());
            System.assertEquals('11:30', controller.trTravelTimeStart.format('HH:mm'));
            System.assertEquals(12987, controller.trMileageOld);
            
            // set the time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 11, 30, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(true, controller.setTimeReportData());
            
            // change the entered time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 11, 30, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 13, 0, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(true, controller.setTimeReportData());
            controller.saveAssignment();
            
            // change the saved time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 13, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 13, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            controller.finishProcessing();
            System.assertEquals(true, controller.finishedOk);
        }
        Test.stopTest();
    } // testEditTimeReports
    
    /**
     * Tests updating time report data.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testUpdateTimeReports()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[5].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id,
                                              true);
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, true);
        SCHelperTestClass.installedBaseSingle.InstallationDate__c = 
                        System.today() - 10;
        upsert SCHelperTestClass.installedBaseSingle;
        SCOrderRepairCode__c repairCode = new SCOrderRepairCode__c(Order__c = SCHelperTestClass.order.Id, 
                                                                   OrderItem__c = SCHelperTestClass.orderitem.Id);
        insert repairCode;
        
        Test.startTest();
        System.runAs(SCHelperTestClass.users[0])
        {
            Id appointmentId = SCHelperTestClass.appointments[0].Id ;          
            ApexPages.currentPage().getParameters().put('aid', appointmentId);
            SCProcessOrderController controller = new SCProcessOrderController();
            
            System.assertEquals(true, controller.getIsTimeReportEditable());
            System.assertEquals('11:30', controller.trTravelTimeStart.format('HH:mm'));
            System.assertEquals(12987, controller.trMileageOld);
            
            // set and save the time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 13, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 13, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(true, controller.setTimeReportData());
            controller.saveAssignment();
            controller.refreshData();
            
            // change the saved time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 13, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 14, 0, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(true, controller.setTimeReportData());
            controller.saveTimeReports();
        }
        Test.stopTest();
    } // testUpdateTimeReports
    
    /**
     * Tests the validation of time report data.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testValidationTimeReports()
    {
        SCHelperTestClass3.createCustomSettings('DE', true);
        SCHelperTestClass.createDomsForOrderCreation();
        
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);
        SCHelperTestClass.createTestCalendar(true);
        SCHelperTestClass.createTestBusinessUnit(SCHelperTestClass.stocks[5].Id, 
                                               SCHelperTestClass.calendar.Id, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources,
                                              SCHelperTestClass.stocks, 
                                              SCHelperTestClass.businessUnit.Id,
                                              true);
        SCHelperTestClass2.createTimeReports(SCHelperTestClass.resources, true);
        SCHelperTestClass.installedBaseSingle.InstallationDate__c = 
                        System.today() - 10;
        upsert SCHelperTestClass.installedBaseSingle;
        SCOrderRepairCode__c repairCode = new SCOrderRepairCode__c(Order__c = SCHelperTestClass.order.Id, 
                                                                   OrderItem__c = SCHelperTestClass.orderitem.Id);
        insert repairCode;
        
        Test.startTest();
        System.runAs(SCHelperTestClass.users[0])
        {
            Id appointmentId = SCHelperTestClass.appointments[0].Id ;          
            ApexPages.currentPage().getParameters().put('aid', appointmentId);
            SCProcessOrderController controller = new SCProcessOrderController();
            
            System.assertEquals(true, controller.getIsTimeReportEditable());
            System.assertEquals('11:30', controller.trTravelTimeStart.format('HH:mm'));
            System.assertEquals(12987, controller.trMileageOld);
            
            // set the time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 4, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 4, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 5, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 5, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(false, controller.setTimeReportData());
            
            // set the time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 9, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 9, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 10, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 11, 0, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(false, controller.setTimeReportData());
            controller.saveAssignment();
            controller.finishProcessing();
            System.assertEquals(false, controller.finishedOk);
            
            // set and save time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 11, 30, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 12, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 13, 0, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(true, controller.setTimeReportData());
            controller.saveAssignment();
            
            // set the time report data
            controller.trMileageNew = 13000;
            controller.trDistance = 13;
            controller.trTravelTimeStart = Datetime.newInstance(2010, 9, 1, 4, 0, 0);
            controller.trTravelTimeEnd = Datetime.newInstance(2010, 9, 1, 4, 30, 0);
            controller.trTravelTimeDesc = 'Travel time 1';
            controller.trLabourTimeStart = Datetime.newInstance(2010, 9, 1, 5, 0, 0);
            controller.trLabourTimeEnd = Datetime.newInstance(2010, 9, 1, 5, 30, 0);
            controller.trLabourTimeDesc = 'Labour time 1';
            System.assertEquals(false, controller.setTimeReportData());
        }
        Test.stopTest();
    } // testValidationTimeReports
}