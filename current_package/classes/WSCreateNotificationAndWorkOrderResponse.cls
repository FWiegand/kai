/** *
 * WSCreateNotificationAndWorkOrderResponse 
 * This object implements a web service that is used on the PoC to 
 * process incomming CreateNotificationAndWorkOrder responses.
 * 
 * How to use this interface:
 *
 * Preparations 
 * 1) Get the WSDL of the Partner API [System|Develop|API|Generate Partner WSDL]
 * 2) Get the WSDL of the WSPartnerPortal [System|Develop|Classes|WSPartnerPortal|WSDL]
 * 3) Import both WSDL fields into your development environment (clients)
 * 
 * Call Sequence
 * 1) Use the Partner API to login and get the session id and the the endpoint url
 *    The password must contain the security token      
 *   <soapenv:Body>
 *     <urn:login>
 *        <urn:username>yourlogin@yoursalesforcelogin</urn:username>
 *        <urn:password>xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</urn:password>
 *     </urn:login>
 *  </soapenv:Body>
 * 
 * 2)The response contains the session id and the serverURL (load ballancing) that is the endpoint 
 *  <soapenv:Body>
 *     <loginResponse>
 *        <result>
 *           <serverUrl>https://xxxxxxx.salesforce.com/services/Soap/u/19.0/00DT0000000xxxx</serverUrl>
 *           <sessionId>00DT0000000xxxx!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxWgw7M2PuCnCHWiq6HDqn1HAPxIg5M7NpDsmjR</sessionId>
 * 
 * 3) Call the services at endpoint (for apex classes included in the wsdl like 'https://tapp0-api.salesforce.com/services/Soap/class/CreateNotificationAndWorkOrderResponse'
 *    and use the sessionId in the SessionHeader
 *  
 *   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cre="http://soap.sforce.com/schemas/class/CreateNotificationAndWorkOrderResponse">
 *      <soapenv:Header>
 *         <cre:SessionHeader>
 *            <cre:sessionId>sessionId>00DT0000000xxxx!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxWgw7M2PuCnCHWiq6HDqn1HAPxIg5M7NpDsmjR</cre:sessionId>
 *         </cre:SessionHeader>
 *      </soapenv:Header>
 *      <soapenv:Body>
 *         <cre:process>
 *            <cre:refno>1234567890123456</cre:refno>
 *            <cre:sapno>abcdefghi</cre:sapno>
 *            <cre:errorcode>OK</cre:errorcode>
 *            <cre:errortext>this is a test</cre:errortext>
 *         </cre:process>
 *      </soapenv:Body>
 *   </soapenv:Envelope>
 *    
 
 * 
 * 5) Logout when you have finished processing
 *   <soapenv:Header>
 *     <wsep:SessionHeader>
 *        <wsep:sessionId>00DT0000000xxxx!xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxWgw7M2PuCnCHWiq6HDqn1HAPxIg5M7NpDsmjR</wsep:sessionId>
 *     </wsep:SessionHeader>
 *  </soapenv:Header>
 *   <soapenv:Body>
 *     <wsep:logout/>
 *  </soapenv:Body>
 *
 * @author GMS Development <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
global without sharing class WSCreateNotificationAndWorkOrderResponse
{
    /** 
     * Used to set the response from the CreateNotificationAndWorkOrder allout to SAP.
     * The refno is the Clockport order id (global unique identifier within salesforce).
     * The process method searches for the Clockport order and sets the sapno into 
     * the SCOrder__c.ERPOrderNo__c field.
     *
     * @param refno        Clockport order id SCOrder__c.Id (as passed in the callout to SAP)
     * @param sapno        The SAP reference number that was created during the callout 
     * @param errorcode    OK   successfully processd in SAP, 
     *                     ERR  processing failed (details in errortext)
     * @param errortext    optional error text 
     * @return a result string (to be defined, yet)
     */
   WebService static String process(String refno, String sapno, String errorcode, String errortext)
   {
       String res = 'SUCC';
       if(refno == null)
       {
           res = 'ERR - refno is missing';
       }
       else if(sapno == null)
       {
           res ='ERR - sapno is missing';
       }
       else
       {
           List<SCOrder__c> ord = [select id from SCOrder__c where name = :refno  limit 1];
           if(ord.size() == 0)
           {
               return 'ERR - order with refno [' + refno + '] not found';
           }
           // now set the sap reference number and update the order
           ord[0].ERPOrderNo__c = sapno;
           if(errorcode != null)
           {
               //#### ord[0].ERPResult__c = errorcode;
           }
           if(errortext != null)
           {
               //#### ord[0].ERPResult__c += ' ' + errortext;
           }
           update ord;  
       }
    
   
       Map<String, String> datamap = new Map<String, String>(); 
       datamap.put('refno', refno);
       datamap.put('sapno', sapno);
       datamap.put('errorcode', errorcode);
       datamap.put('errortext', errortext);
       datamap.put('RESULT', res);
       //cce.sendMail('WSCreateNotificationAndWorkOrderResponse', datamap);
            
       return res;
   }
}