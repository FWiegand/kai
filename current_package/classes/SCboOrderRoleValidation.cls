/*
 * @(#)SCboOrderRoleValidation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * www.gms-online.de 
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of
 * GMS Development GmbH. ("Confidential Information").  You shall not disclose 
 * any confidential information and shall use it only in accordance with the 
 * terms of the license agreement you aggreed with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderRoleValidation
{
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Validates a sobject from the type order role.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean validate(SCOrderRole__c orderRole)
    {
        return validate(orderRole, SCboOrderValidation.ALL);
    }
    public static Boolean validate(SCOrderRole__c orderRole, Integer mode)
    {
        Boolean ret = true;
        
        // CCE - GMSNA 12.03.2013 - no order role valiation in this project
        return true;
        
        /*
        if ((mode & SCboOrderValidation.ROLE_DATA) == SCboOrderValidation.ROLE_DATA)
        {
            if (!SCBase.isSet(orderRole.OrderRole__c))
            {
                throw new SCfwException(2100);
            }
            // an account id or minimal order role data must be set
            if (!SCBase.isSet(orderRole.Account__c) && 
                !SCBase.isSet(orderRole.Name1__c) && 
                !SCBase.isSet(orderRole.Street__c) && 
                !SCBase.isSet(orderRole.City__c) && 
                !SCBase.isSet(orderRole.Country__c) && 
                !SCBase.isSet(orderRole.PostalCode__c))
            {
                throw new SCfwException(2101);
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(orderRole.AccountType__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.AccountType__c, orderRole.AccountType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.AccountType__c, 'SCOrderRole__c.AccountType__c'));
                }
                
                if (SCBase.isSet(orderRole.DistanceZone__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.DistanceZone__c, orderRole.DistanceZone__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.DistanceZone__c, 'SCOrderRole__c.DistanceZone__c'));
                }
                
                if (SCBase.isSet(orderRole.LocaleSidKey__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.LocaleSidKey__c, orderRole.LocaleSidKey__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.LocaleSidKey__c, 'SCOrderRole__c.LocaleSidKey__c'));
                }
                
                if (SCBase.isSet(orderRole.LockType__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.LockType__c, orderRole.LockType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.LockType__c, 'SCOrderRole__c.LockType__c'));
                }
                
                if (SCBase.isSet(orderRole.RiskClass__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.RiskClass__c, orderRole.RiskClass__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.RiskClass__c, 'SCOrderRole__c.RiskClass__c'));
                }
                
                if (SCBase.isSet(orderRole.OrderRole__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.OrderRole__c, orderRole.OrderRole__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.OrderRole__c, 'SCOrderRole__c.OrderRole__c'));
                }
                
                if (SCBase.isSet(orderRole.Salutation__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.Salutation__c, orderRole.Salutation__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.Salutation__c, 'SCOrderRole__c.Salutation__c'));
                }
                
                if (SCBase.isSet(orderRole.TaxType__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.TaxType__c, orderRole.TaxType__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.TaxType__c, 'SCOrderRole__c.TaxType__c'));
                }
                
                if (SCBase.isSet(orderRole.Type__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.Type__c, orderRole.Type__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.Type__c, 'SCOrderRole__c.Type__c'));
                }
                
                if (SCBase.isSet(orderRole.VipLevel__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.VipLevel__c, orderRole.VipLevel__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.VipLevel__c, 'SCOrderRole__c.VipLevel__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.ROLE_DATA) == SCboOrderValidation.ROLE_DATA)
        
        if ((mode & SCboOrderValidation.ROLE_ADDRESS_DATA) == SCboOrderValidation.ROLE_ADDRESS_DATA)
        {
            if (!SCBase.isSet(orderRole.Account__c) && !SCBase.isSet(orderRole.Street__c))
            {
                throw new SCfwException(2102);
            }
            
            //if (!SCBase.isSet(orderRole.Account__c) && !SCBase.isSet(orderRole.HouseNo__c))
            //{
            //    throw new SCfwException(2103);
            //}
            
            if (!SCBase.isSet(orderRole.Account__c) && !SCBase.isSet(orderRole.PostalCode__c))
            {
                throw new SCfwException(2104);
            }
            if (!SCBase.isSet(orderRole.Account__c) && !SCBase.isSet(orderRole.City__c))
            {
                throw new SCfwException(2105);
            }
            if (!SCBase.isSet(orderRole.Account__c) && !SCBase.isSet(orderRole.Country__c))
            {
                throw new SCfwException(2106);
            }

            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(orderRole.Country__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.Country__c, orderRole.Country__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.Country__c, 'SCOrderRole__c.Country__c'));
                }
                
                if (SCBase.isSet(orderRole.CountryState__c) && 
                    !SCBase.isPicklistValue(SCOrderRole__c.CountryState__c, orderRole.CountryState__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, orderRole.CountryState__c, 'SCOrderRole__c.CountryState__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.ROLE_ADDRESS_DATA) == SCboOrderValidation.ROLE_ADDRESS_DATA)
        
        if ((mode & SCboOrderValidation.ROLE_GEO_DATA) == SCboOrderValidation.ROLE_GEO_DATA)
        {
            if (!SCBase.isSet(orderRole.Account__c) && (null == orderRole.GeoX__c))
            {
                throw new SCfwException(2107);
            }
            if (!SCBase.isSet(orderRole.Account__c) && (null == orderRole.GeoY__c))
            {
                throw new SCfwException(2108);
            }
        } // if ((mode & SCboOrderValidation.ROLE_GEO_DATA) == SCboOrderValidation.ROLE_GEO_DATA)
        return ret;
        */
    } // validate
} // SCboOrderRoleValidation