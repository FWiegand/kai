/*
 * @(#)SCboAppointment.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements basic operations on appointments and the communication with the ASE
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboAppointment
{
    // prevent to call our own trigger
    static Boolean ignoreTrigger = false;
    
    /**
     * event after inserting a appointment item
     * @param list of appointment items
     */
    public void AfterInsert(List<SCAppointment__c> items)
    {
        System.debug('#### appointment : AfterInsert()');
        List<SCAssignment__c> assignmentsNew = new List<SCAssignment__c>(); 
        List<Id> newOrderIds = new List<Id>(); 
        List<Id> newAppIdChange = new List<Id>(); 
        for(SCAppointment__c appointment: items)
        {
           //check for order relation
           if(appointment.order__c != null)
           {
                //create new assignment with same status
                SCAssignment__c assignment = new SCAssignment__c();
                assignment.Order__c = appointment.Order__c;
                assignment.Resource__c = appointment.Resource__c;
                assignment.Status__c = appointment.AssignmentStatus__c;
                assignmentsNew.add(assignment);
                newOrderIds.add(appointment.order__c);
                newAppIdChange.add(appointment.Id);
           }
        }                 
        insert assignmentsNew;

        Map <Id, Id> newAppAssign = new Map<Id, Id>();
        for (Integer i = 0; i < assignmentsNew.size(); i++)
        {
            System.debug('Appointment Insert: App ID  --> new Assign ID' + newAppIdChange[i] + '-->' +  assignmentsNew[i].Id);
            newAppAssign.put(newAppIdChange[i], assignmentsNew[i].Id);
        }
        //set assigment to appointment              
        List<SCAppointment__c> apps = new List<SCAppointment__c>();
        Set<Id> processedIds = new Set<Id>();

        for (Id appId :newAppIdChange)
        {
            if (!processedIds.contains(appid))
            {
                SCAppointment__c appointment = new SCAppointment__c(Id = appId);
                ID assignId = newAppAssign.get(appId);
                if (assignId != null)
                {
                    System.debug('Appointment Insert: App ID --> new Assign ID adviced ' + appId + ' --> ' +  assignId);
                    appointment.Assignment__c = assignId;
                }
                apps.add(appointment);
                processedIds.add(appId);
            } // if (!processedIds.contains(appid))
        } // for (Id appId :newAppIdChange)
        // prevent own trigger
        ignoreTrigger = true;
        update apps;
        ignoreTrigger = false;

        updateOrderStatus(newOrderIds, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED, 'Appointment Insert Status Order: ID ');

        //### TMP GMSNA - for writing SCMessage records - to be replaced by a future implementation
        SCutilMessage.onAppointmentInsert(items);
    }
        
    /**
     * helper method to update the order status
     * @param list of order id's
     * @param newStat state to set
     * @param msg for trace info
     */
    private void updateOrderStatus(List<Id> newOrderIds, String newStat, String msg)
    {
        System.debug('#### updateOrderStatus(): Orders Ids to update status ' + newOrderIds);
        System.debug('#### updateOrderStatus(): new state ' + newStat);
        if (newOrderIds.size() > 0)
        {
            // set order states
            Map<Id, SCboOrder.CalcStateResult> stateResults = SCboOrder.calculateStates(newStat, newOrderIds);
            List<SCOrder__c> orders = new List<SCOrder__c>();
            Set<Id> processedIds = new Set<Id>();

            for (Id orderId :newOrderIds)
            {
                if (!processedIds.contains(orderid))
                {
                    SCOrder__c order = new SCOrder__c(Id = orderId);
                    String status = stateResults.get(orderId).nextState;
                    if (status != null)
                    {
                        order.Status__c = status;
                        //PMS 34541: by ET: Set order.closed and order.closedby
                        if(status.equals(SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED))
                        {
                            DateTime closed = ScboOrder.getClosed(stateResults.get(orderId));
                            Id closedBy = SCboOrder.getClosedBy(stateResults.get(orderId));
                            
                            System.debug('#### updateOrderStatus(): ' + order.Id + ' completed. Closed:' + closed + '; ClosedBy:' + closedBy);
                            order.Closed__c = closed;
                            order.ClosedBy__c = closedBy;
                            
                            //PMS 36413/INC0095770: ORD-0000083088 / "Bereit zum Fakturieren" gesetzt, obwohl noch nicht final abgeschlossen
                            //GMSET: Set Invoicing Status to 52402 if order completed
                            order.InvoicingReleased__c = true;
                            order.InvoicingStatus__c = SCfwConstants.DOMVAL_INVOICING_STATUS_READYFORINVOICING;
                        }
                    }
                    orders.add(order);
                    processedIds.add(orderId);
                    System.debug('#### updateOrderStatus(): ' + msg + ' ' + order.Id + ', status ' + order.Status__c);
                } // if (!processedIds.contains(orderid))
            } // for (Id orderId :newOrderIds)
            update orders;
        } // if (newOrderIds.size() > 0)
    } // updateOrderStatus


    /**
     * event after updating a appointment item
     * @param list of new appointment items 
     * @param list of old appointment items
     */
    public void AfterUpdate(List<SCAppointment__c> itemsNew, List<SCAppointment__c> itemsOld)
    {
        if (ignoreTrigger)
        {
            // prevent loop if the trigger updates data that will retrigger an update
            return;
        }
        
        // prevent recursive triggering
        ignoreTrigger = true;
        
        System.debug('#### appointment: AfterUpdate()');
        
        // Determine which assignments stay on the same resources or are moved to another resource
        SCAppointment__c newEntry = null;    
        SCAppointment__c oldEntry = null;    

        // List containing assignments without any changes of the resource  
        List <Id> newAssIds = new List<ID>();
        Map <Id, String> newAssStatus = new Map<Id, String>();
        
        // List containing assignments where the resource has changed
        List <Id> newAssIdsEmplChange = new List<ID>();
        List <SCAppointment__c> newAppEmplChanges = new List<SCAppointment__c>();

        // All cancelled assignments (where the appointment was moved to another resource)
        List<SCAssignment__c> assignmentsCancelled = new List<SCAssignment__c>();

        for (Integer i = 0; i < itemsNew.size(); i++)
        {
            newEntry = itemsNew.get(i);    
            oldEntry = itemsOld.get(i);
            String newEmplID;
            String oldEmplID;

            //ignore entries with trigger hint = 1--> see trigger before update
            if (newEntry.TriggerHint__c == '1')
            {
                System.debug('#### appointment: AfterUpdate() canceled');
                continue;
            }

            if(newEntry.order__c != null && oldEntry.order__c != null)
            {
                newEmplID = newEntry.Resource__c;   
                oldEmplID = oldEntry.Resource__c;   
        
                // dispatch on another resource?
                if(newEmplID != oldEmplID)
                { 
                    System.debug('NewEntryCh: ' + newEntry);
                    if (null != oldEntry.assignment__c)
                    {
                        newAssIdsEmplChange.add(oldEntry.assignment__c);                
                    }
                    else
                    {
                        System.debug('#### SCboAppointment.AfterUpdate(): found null assignment -> ' + oldEntry);
                    }
                    newAppEmplChanges.add(newEntry);
                }   
                else
                {
                    System.debug('NewEntry: ' + newEntry);
                    System.debug('NewEntry.assignment__c: ' + newEntry.assignment__c);
                    //remenber Id's
                    newAssIds.add(newEntry.assignment__c);              
                    newAssStatus.put(newEntry.assignment__c, newEntry.AssignmentStatus__c);             
                }  

            }//if(newEntry.order__c != null && oldEntry.order__c != null)
        }//for (Integer i = 0; i < itemsNew.size(); i++)


        // Evaluate all assignments where the resource has been changed
        // - cancel the old assignment
        // - insert new assignment (for the new resource)
        // - update appointment (attach new assignment to the existing appointment)
        // - update the order status
        if(newAssIdsEmplChange.size() > 0)
        {
            //*** bulk update when change of employee ***//
            Set<Id> processedIds = new Set<Id>();
            // Cancel old assignments
            for (Id assId :newAssIdsEmplChange)
            {
                if ((null != assId) && !processedIds.contains(assid))
                {
                    SCAssignment__c assignment = new SCAssignment__c(Id = assId);
                    System.debug('Appointment Update: delete Assignment ID --> ' + assId);
                    // cancel old assignment
                    assignment.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED;
                    // prevent trigger ping-pong
                    assignment.TriggerHint__c = '1';
                    
                    assignmentsCancelled.add(assignment);
                    processedIds.add(assId);
                } // if ((null != assId) && !processedIds.contains(assid))
            } // for (Id assId :newAssIdsEmplChange)
            
            //### TMP GMSNA - for writing SCMessage records - to be replaced by a future implementation
            if(assignmentsCancelled.size() > 0)
            {
                System.debug('###na1###' + assignmentsCancelled);
                List<SCAppointment__c> appCancelled = [select id, order__c, start__c, end__c, order__r.country__c, PlanningType__c, resource__c from SCAppointment__c where assignment__r.id in :assignmentsCancelled];
                System.debug('###na2###' + appCancelled);
                
                SCutilMessage.onAppointmentCancelled(appCancelled);
            }
            
            update assignmentsCancelled;                                
                
            // Create the new assignments    
            List<SCAssignment__c> assignmentsNew = new List<SCAssignment__c>(); 
            List<Id> newAppIdChange = new List<Id>(); 
            for (Integer i = 0; i < newAppEmplChanges.size(); i++)
            {
                SCAppointment__c appNew = newAppEmplChanges[i];
                SCAssignment__c assignmentNew = new SCAssignment__c();
                assignmentNew.order__c = appNew.order__c;
                assignmentNew.Resource__c = appNew.Resource__c;
                assignmentNew.status__c = appNew.AssignmentStatus__c;
                assignmentsNew.add(assignmentNew);
                newAppIdChange.add(appNew.Id);
                System.debug('Appointment Update: new Assignment  --> ' + assignmentsNew);
            }
            insert assignmentsNew;

            // Ensure that the new appointments are updated 
            Map <Id, Id> newAppAssign = new Map<Id, Id>();
            for (Integer i = 0; i < assignmentsNew.size(); i++)
            {
                System.debug('Appointment Update: App ID  --> new Assign ID' + newAppIdChange[i] + '-->' +  assignmentsNew[i].Id);
                newAppAssign.put(newAppIdChange[i], assignmentsNew[i].Id);
            }

            // Attache the assigment to the existing appointments
            List<Id> newOrderIds = new List<Id>(); 
            List<SCAppointment__c> apps = [ select id, order__c from SCAppointment__c where id in :newAppIdChange];
            for (Integer i = 0; i < apps.size(); i++)
            {
                ID assignId= newAppAssign.get(apps[i].Id);
                if(assignId != null)
                {
                    System.debug('Appointment Update: App ID  --> new Assign ID adviced' + apps[i].Id + '-->' +  assignId);
                    apps[i].assignment__c = assignId;
                    newOrderIds.add(apps[i].Order__c);
                }
            }            
            update apps;
            
            
            
            // update the status on order level
            updateOrderStatus(newOrderIds, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED, 'Appointment Update Status Order ');
            
            
        } // if(newAssIdsEmplChange.size..
        else
        {
            // Save a message if appointment was updated
           //  SCutilMessage.onAppointmentUpdate2(itemsNew, itemsOld); 
        }


        // If the assignment is moved on the same resource
        // - update the assignment status (to enable the processing(download) if scheduled on the current day
        // - update the order status
        if(newAssIds.size() > 0)
        {
            //*** bulk update when no change of employee ***//
            // same resource, new time
            System.debug('NewEntry: ' + newEntry);
            System.debug('IDs: ' + newAssIds);
            // update assignment status
            //get assignment
            List<SCAssignment__c> assignments = [ select Id, order__c, status__c from SCAssignment__c 
                where Id in :newAssIds];

            List<Id> newOrderIds = new List<Id>(); 
            for (Integer i = 0; i < assignments.size(); i++)
            {
                String status = newAssStatus.get(assignments[i].Id);
                if(status !=  null)
                {
                    System.debug('Appointment Update: Ass ID --> ' + assignments[i].Id);
                    System.debug('Appointment Update: status new --> ' + status);
                    System.debug('Appointment Update: status old --> ' + assignments[i].status__c);

                    //update assignment status
                    assignments[i].status__c = status;
                    // prevent trigger ping-pong
                    assignments[i].TriggerHint__c = '1';

                    //remember order id when status change from ex. accepted to planned 
                    System.debug('Appointment Update: status changed, index --> ' + assignments[i].status__c + ' ' + i);
                    newOrderIds.add(assignments[i].Order__c);
                }
            }
            update assignments;                                

            updateOrderStatus(newOrderIds, SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED, 'Appointment Update Status Order ');
            
        }//if(newAssIds.size() > 0)
        ignoreTrigger = false;
    }
    /**
     * event before updating a appointment item
     * @param list of new appointment items 
     * @param list of old appointment items
     *
     */
    public void BeforeUpdate(List<SCAppointment__c> itemsNew, List<SCAppointment__c> itemsOld)
    {
          // prevent to change status from closed final to accepted
        //see PMS 34175/0000004836: Closed orders - Status changed to "transferred"
        List<String> changeStatusIds = new List<String>(); 
        for (Integer i = 0; i < itemsNew.size(); i++)
        {
            System.debug('Status new:' +  itemsNew[i].AssignmentStatus__c);
            System.debug('Status old:' +  itemsOld[i].AssignmentStatus__c);

            if (itemsNew[i].AssignmentStatus__c != itemsOld[i].AssignmentStatus__c &&
            (itemsOld[i].AssignmentStatus__c == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED ||
             itemsOld[i].AssignmentStatus__c == SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL ||
             itemsOld[i].AssignmentStatus__c == SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED))
            {
                itemsNew[i].AssignmentStatus__c = itemsOld[i].AssignmentStatus__c;
                itemsNew[i].TriggerHint__c = '1';
                System.debug('Preventing Status change from closed to transfered for id:' +  itemsNew[i].Id);
                // set status in ASE back to correct value                
                changeStatusIds.add(itemsNew[i].ID2__c);
            }
        }
        if(changeStatusIds.size() > 0)
        {
            String ids = '';
         // set status in ASE back to correct value                
            for(String id: changeStatusIds)
            {
                ids += id+ ',';
            }   
            System.debug('Appointment before update: Processing Assignment Ids -->, New Status for Ids: ' + ids  + '--->, Status: ' + SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED);
            AseSetStatus.callout(ids , '', SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED);
        }
    }    



    /**
     * event before deleting a appointment item
     * @param list of appointment items
     *
     */
    public void BeforeDelete(List<SCAppointment__c> items)
    {
    }

    /**
     * event after deleting a appointment item
     * @param list of appointment items
     *
     */
    public void AfterDelete(List<SCAppointment__c> items)
    {
        List<Id> newAssIds = new List<Id>(); 
        List<Id> newOrderIds = new List<Id>(); 
        for(SCAppointment__c appointment: items)
        {
           System.debug('#### appointment: AfterDelete()...');

           if(appointment.order__c != null && appointment.assignment__c != null)
           {
                newAssIds.add(appointment.assignment__c);
                newOrderIds.add(appointment.order__c);
           } 
        }    
        if(newAssIds.size() > 0)
        {
            List<SCAssignment__c> assignments = new List<SCAssignment__c>();
            Set<Id> processedIds = new Set<Id>();

            for (Id assId :newAssIds) 
            {
                if (!processedIds.contains(assid))
                {
                    SCAssignment__c assignment = new SCAssignment__c(Id = assId);
                    System.debug('Appointment Delete: ID --> ' + assId);
                    // update assignment status
                    assignment.Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED;
                    // prevent trigger ping-pong
                    assignment.TriggerHint__c = '1';
                    
                    assignments.add(assignment);
                    processedIds.add(assId);
                } // if (!processedIds.contains(assid))
            } // for (Id assId :newAssIdsEmplChange)
            update assignments;                                
        }

        updateOrderStatus(newOrderIds, SCfwConstants.DOMVAL_ORDERSTATUS_OPEN, 'Appointment Delete Status Order: ');
        
       //### TMP GMSNA - for writing SCMessage records - to be replaced by a future implementation
        SCutilMessage.onAppointmentDelete(items);
        
    }

    /**
     * readAllAppointments
     * ===================
     *
     * Reads all appointments for the given resource and timespan.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCAppointment__c> readAllAppointments(Id resourceId, DateTime startDate, 
                                                      DateTime endDate)
    {
        return [select Id, Name, Class__c, Type__c, Resource__c, Start__c, End__c 
                from SCAppointment__c 
                where Resource__c = :resourceId 
                  and Start__c >= :startDate 
                  and End__c <= :endDate 
                order by Start__c];
    } // readAllAppointments

    /**
     * Reads all appointments of the class EMPLOYEE for the given 
     * resource and timespan.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<SCAppointment__c> readAllEmplAppointments(Id resourceId, DateTime startDate, 
                                                          DateTime endDate)
    {
        return [select Id, Name, Class__c, Type__c, Resource__c, Start__c, End__c 
                from SCAppointment__c 
                where Resource__c = :resourceId 
                  and Class__c = :SCfwConstants.DOMVAL_APPOINTMENTCLASS_EMPLOYEE 
                  and Start__c >= :startDate 
                  and End__c <= :endDate 
                order by Start__c];
    } // readAllEmplAppointments

    /**
     * Reads all appointments and assignment details of an order 
     * @param oid         the order id
     * @param onlypending true: only pending appointments are read. 
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCAppointment__c> readByOrder(String oid, boolean onlypending)
    {
        if(onlypending)
        {
            List<String> stat = SCfwConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED.split(',');
        
            return [select Id, Start__c, End__c, Resource__c, Employee__c, Mobile__c, Phone__c, AssignmentStatus__c,
                    CustomerTimewindow__c, CustomerPrefStart__c, CustomerPrefEnd__c, Fixed__c, FixedResource__c,
                    Assignment__c, OrderItem__c 
                    from SCAppointment__c 
                    where order__c = :oid and AssignmentStatus__c in :stat
                     order by Start__c desc];
            
        }
        return [select Id, Start__c, End__c, Resource__c, Employee__c, Mobile__c, Phone__c, AssignmentStatus__c,
                CustomerTimewindow__c, CustomerPrefStart__c, CustomerPrefEnd__c, Fixed__c, FixedResource__c,
                Assignment__c, OrderItem__c 
                from SCAppointment__c 
                where order__c = :oid and AssignmentStatus__c <> :SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED 
                 order by Start__c desc];
                 
    } // readByOrder



    /**
     * roundMinutes
     * ============
     *
     * Rounds a date to a time grid 
     * Example: round(Calendar.MINUTE, 15) fastens the date to a 15 minutes grid
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Datetime roundMinutes(Datetime orgDate, Integer gridValue)
    {
        Datetime roundDate;
        Integer cur = orgDate.minute();
        Integer grid = 0;
        
        if (gridValue != 0)
        {
            while (grid  < cur)
            {
                grid += gridValue;
            } // while (grid  < cur)
        } // if (gridValue != 0)

        if (grid > 60)
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(), 
                                              orgDate.hour(), 0, 0).addHours(1);   
        } // if (grid > 60)
        else
        {
             roundDate = Datetime.newInstance(orgDate.year(), orgDate.month(), orgDate.day(), 
                                              orgDate.hour(), grid, 0);   
        } // else [if (grid > 60)]

        return roundDate;            
    } // roundMinutes
}