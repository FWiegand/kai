/**
 * SCMaterialAvailability.cls    aw  25.05.2012
 * 
 * Copyright (c) 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */


/**
 * Class is used to do material availability checks
 */
public class SCMaterialAvailability
{
    public static SCMaterialAvailabilityData.SCResult call(SCMaterialAvailabilityData.SCServiceData serviceData)
    {
        SCMaterialAvailabilityData.SCResult result = new SCMaterialAvailabilityData.SCResult();
        result.success = false;
        String mode = serviceData.mode;
        
        if ((null == mode) || mode.equals(SCMaterialAvailabilityData.MODE_TEST))
        {
            result.success = true;
            result.results = new List<SCMaterialAvailabilityData.SCResultData>();
            
            SCMaterialAvailabilityData.SCResultData resultData = new SCMaterialAvailabilityData.SCResultData();
            resultData.articleNr = 'ART_NO_1';
            resultData.qtyStr = '1';
            resultData.qty = 1;
            resultData.deliveryDate = Date.newInstance(2012, 5, 29);
            resultData.deliveryDateStr = '29.05.2012';
            result.results.add(resultData);

            resultData = new SCMaterialAvailabilityData.SCResultData();
            resultData.articleNr = 'ART_NO_2';
            resultData.qtyStr = '1';
            resultData.qty = 1;
            resultData.deliveryDate = Date.newInstance(2012, 5, 29);
            resultData.deliveryDateStr = '29.05.2012';
            result.results.add(resultData);
            return result;
        }
        
        SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
        String type = applicationSettings.MATERIALAVAILABILITY_SERVICE_TYPE__c;
        if ((null != type) && type.equalsIgnoreCase('SAP'))
        {
            SCMaterialAvailabilityServiceImplSAP mas = new SCMaterialAvailabilityServiceImplSAP ();
            result = mas.call(serviceData);
        }
        return result;
    } // call
} // class SCMaterialAvailability