public with sharing class CCWSInterfaceLogTest 
{
	public static void testTooManyScriptStatements_200001()
	{
		// create first EQChange call.
		CCWSEquipmentChange.MessageHeaderClass messageHeader 		= new CCWSEquipmentChange.MessageHeaderClass();
		messageHeader.MessageID = '51187F2F6D7C0CD0E10080000A62025';
		messageHeader.MessageUUID = '51187F2F-6D7C-0CD0-E100-80000A62025A';
		
        CCWSEquipmentChange.UserStatusClass userStatus				= new CCWSEquipmentChange.UserStatusClass();
        userStatus.StatusCode = 'E0007';
        userStatus.ShortText = 'USED';
        
        CCWSEquipmentChange.PlainFieldsClass plainFields			= new CCWSEquipmentChange.PlainFieldsClass();
        plainFields.CompanyCode = '1095';
        plainFields.EquipmentNumber = '1051273';
        plainFields.ShortText = 'RETRO COOLER ROT';
        plainFields.StartupDate = date.newinstance(2010, 2, 17);
        plainFields.TechnicalObjectType = 'CDE BO';
        plainFields.ValidFromDate = date.newinstance(2010, 2, 17);
        plainFields.ManufacturerSerialNumber = 'RO165635892';
        plainFields.PriceList = 'DE800';
        plainFields.MaintenancePlant = '0356';
        plainFields.SuperordinateEquipment = null;
        plainFields.MaterialNumber = '40000469';
        plainFields.MaterialDescription = 'RETRO COOLER SILBER';
        plainFields.SerialNumber = '33271306';
        plainFields.PlannerGroup = 'PG';
        
        
        
        CCWSEquipmentChange.Partners_element partners				= new CCWSEquipmentChange.Partners_element();
    	List<CCWSEquipmentChange.PartnerClass> partner		=	new List<CCWSEquipmentChange.PartnerClass>();
        
        CCWSEquipmentChange.PartnerClass p1 = new CCWSEquipmentChange.PartnerClass();
        CCWSEquipmentChange.PartnerClass p2 = new CCWSEquipmentChange.PartnerClass();
        p1.PartnerFunction  = 'WE'; 
        p1.PartnerNumber 	= '503422977'; 
        p2.PartnerFunction 	= 'AG'; 
        p2.PartnerNumber	= '503422977'; 
        partner.add(p1);
        partner.add(p2);
        partners.Partner = partner;
        
        CCWSEquipmentChange.CustomerWarrantyClass customerWarranty	= new CCWSEquipmentChange.CustomerWarrantyClass();
        customerWarranty.StartDate = date.newinstance(2010, 1, 1);
        customerWarranty.EndDate = date.newinstance(2020, 2, 17);
        
        CCWSEquipmentChange.StockDataClass stockData				= new CCWSEquipmentChange.StockDataClass();
        stockData.Plant = '0356';
        stockData.StorageLocation = '6400';
        stockData.Batch = 'DECCRR0000';
        
        CCWSEquipmentChange.AdressDataClass adressData 				= new CCWSEquipmentChange.AdressDataClass();
        adressData.Country = 'DE';
        adressData.City = 'Berling';
        adressData.PostCode = '10117';
        adressData.Street = 'Friedrichstraße';
        adressData.HouseNumber = '68';
        adressData.Building = 'Q205';
        adressData.Floor = '4';
        adressData.RoomNumber = '400-439';
        adressData.Name = 'Coca-Cola Erfrischungsgetränke AG';
        adressData.Name2 = 'CCE';
        adressData.Telephone = '030 920401';
        adressData.MobilePhone = '0151 920401';
        adressData.Facsimile = '1234 12456';
        adressData.Email = 'mail@cce.ag';
        
		
 		CCWSEquipmentChange.CCWSEquipmentChangeClass eqChange 		= new CCWSEquipmentChange.CCWSEquipmentChangeClass();  
        eqChange.MessageHeader  = messageHeader;
        eqChange.UserStatus = userStatus;
        eqChange.PlainFields = plainFields;
        eqChange.Partners = partners;
        eqChange.customerWarranty = customerWarranty;
        eqChange.StockData = stockData;
        eqChange.AdressData = adressData;     
        
        List<CCWSEquipmentChange.CCWSEquipmentChangeClass> inputData = new List<CCWSEquipmentChange.CCWSEquipmentChangeClass>();
        
        inputData.add(eqChange);
        String msg = '';
		for(integer i = 0; i< 100000; i++)
		{
			msg = '' + eqChange;		
		}	
	}
	
}