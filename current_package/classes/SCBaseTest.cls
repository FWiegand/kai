/*
 * @(#)SCBaseTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Basic tests for the tool / helper class
 * @version $Revision$, $Date$
 */
@isTest
private class SCBaseTest 
{
    static testMethod void formatSOQLDate() 
    {
        Datetime dt = datetime.newInstance(2010, 10, 1, 8, 10, 15);
        Date d = date.newInstance(2010, 10, 1);

        // test 1 - yyyy-MM-dd
        String data = SCBase.formatSOQLDate(dt);
        System.AssertEquals(data, '2010-10-01');
        
        // test 2 - yyyyMMddHHmmss
        data = SCBase.formatDBaseDate(dt);
        System.AssertEquals(data, '20101001081015');
        
        // test 3 - yyyy-MM-dd 
        data = SCBase.formatSOQLDate(d);
        System.AssertEquals(data, '2010-10-01');
        
        // test 4 - yyyyMMdd
        data = SCBase.formatDBaseDate(d);
        System.AssertEquals(data, '20101001000000');
        
        // test 5 - yyyymmddhhmmss
        Datetime aa = datetime.newInstanceGMT(2010, 10, 01, 08, 10, 15);
        DateTime bb = SCBase.parseDateTimeGMT('20101001081015');
//        a = a.
//        data = SCBase.formatDBaseDate(a);
        System.AssertEquals(aa, bb);

        try
        {
            bb = SCBase.parseDateTimeGMT('20101001081015000');
        }
        catch (Exception e)
        {
        }
        
        // test 6 - yyyymmdd
        Date b = SCBase.parseDate('20101115');
        data = SCBase.formatDBaseDate(b);
        System.AssertEquals(data, '20101115000000');

        try
        {
            b = SCBase.parseDate('2010111');
        }
        catch (Exception e)
        {
        }
        
        // test 7 - yyyymmddhhmmss        
        Time c = SCBase.parseTime('20101115130750');
        System.Assert(c.hour() == 13 && c.minute() == 7 && c.second() == 50);

        try
        {
            c = SCBase.parseTime('20101115130750000');
        }
        catch (Exception e)
        {
        }

        // test 8 - yyyymmddhhmmss        
        dt = SCBase.parseDateTime('20101115130750');
        System.Assert(dt.hour() == 13 && dt.minute() == 7 && dt.second() == 50);
        System.Assert(dt.day() == 15 && dt.month() == 11 && dt.year() == 2010);
        
        try
        {
            dt = SCBase.parseDateTime('20101115130750000');
        }
        catch (Exception e)
        {
        }
        
        // test 9 - just a stub
        Double e = 123.45;
        Double f = SCBase.convertCur2Euro(e, 'EUR');
        f = SCBase.convertCur(e, 'EUR', 'GBP');
    }

    static testMethod void testIsSet() 
    {
        String text = null;
        Boolean ret = SCBase.isSet(text);
        System.Assert(!ret);
        
        text = '';
        ret = SCBase.isSet(text);
        System.Assert(!ret);
        
        text = 'Test';
        ret = SCBase.isSet(text);
        System.Assert(ret);
    }

    static testMethod void testGetUserCountryNL() 
    {
        SCHelperTestClass3.createCustomSettings('NL', true);
        
        String country = SCBase.getUserCountry();
        System.AssertEquals('NL', country);
    } // testGetUserCountryNL

    static testMethod void testGetUserCountryGB() 
    {
        SCHelperTestClass3.createCustomSettings('GB', true);
        SCHelperTestClass.createTestUsers(true);
        
        String country = SCBase.getUserCountry();
        System.AssertEquals('GB', country);
    } // testGetUserCountryGB
    
    static testMethod void testFormatIntervall() 
    {
        DateTime dtStart = DateTime.now() - 1.0/24.0;
        DateTime dtEnd   = DateTime.now(); 
        DateTime dtEnd2  = dtEnd + 2; 
    
        String res;
        String tmp;
        // 26.07.2011 14:30 - 15:30
        res = SCBase.FormatInterval(dtStart, dtEnd);
        tmp = dtStart.format('HH:mm');
        System.assert(res.contains(tmp));
        tmp = dtEnd.format('HH:mm');
        System.assert(res.contains(tmp));
        
        // 26.07.2011 14:30
        res = SCBase.FormatInterval(dtStart, null);
        tmp = dtStart.format('HH:mm');
        System.assert(res.contains(tmp));
        tmp = dtEnd.format('HH:mm');
        System.assert(!res.contains(tmp));

        // 26.07.2011 15:30
        res = SCBase.FormatInterval(null, dtEnd);
        tmp = dtStart.format('HH:mm');
        System.assert(!res.contains(tmp));
        tmp = dtEnd.format('HH:mm');
        System.assert(res.contains(tmp));
                
        // blank string
        res = SCBase.FormatInterval(null, null);
        System.assertEquals('', res);

        // 26.07.2011 14:30 - 28.07.2011 15:30
        res = SCBase.FormatInterval(dtStart, dtEnd2);
        tmp = dtEnd2.date().format();
        System.assert(res.contains(tmp));
    }
    
    static testMethod void testFormatIntervall2() 
    {
        Date dtStart = Date.today().addDays(-1);
        Date dtEnd   = Date.today();
    
        String res;
        String tmp;

        res = SCBase.formatDateInterval(dtStart, dtEnd);
        tmp = dtStart.format() + ' - ' + dtEnd.format();
        System.assertEquals(tmp, res);

        res = SCBase.formatDateInterval(dtStart, dtStart);
        tmp = dtStart.format();
        System.assertEquals(tmp, res);

        res = SCBase.formatDateInterval(dtStart, null);
        tmp = dtStart.format();
        System.assertEquals(tmp, res);

        res = SCBase.formatDateInterval(null, dtEnd);
        tmp = dtEnd.format();
        System.assertEquals(tmp, res);

        res = SCBase.formatDateInterval(null, null);
        tmp = '';
        System.assertEquals(tmp, res);
    }
    
    static testMethod void testStringToMap() 
    {
        Map<String, String> keyMap = SCBase.stringToMap('50001=000.png|50002=002.png|50003=','50001=000.png|50002=002.png|50003=');

        System.assertEquals(3, keyMap.size());
        System.assertEquals('000.png', keyMap.get('50001'));
        System.assertEquals('002.png', keyMap.get('50002'));
        System.assertEquals(null, keyMap.get('50003'));

        keyMap = SCBase.stringToMap('','50001=000.png|50002=002.png|50003=');

        System.assertEquals(3, keyMap.size());
        System.assertEquals('000.png', keyMap.get('50001'));
        System.assertEquals('002.png', keyMap.get('50002'));
        System.assertEquals(null, keyMap.get('50003'));
    }
    
    static testMethod void testIsPicklistValue() 
    {
        System.assertEquals(true, SCBase.isPicklistValue(SCOrder__c.Type__c, SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT));
        System.assertEquals(false, SCBase.isPicklistValue(SCOrder__c.Type__c, 'XY'));
    }
    
    static testMethod void testGetPicklistText() 
    {
//CCE        System.assertNotEquals(SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT, SCBase.getPicklistText(SCOrder__c.Type__c, SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT));
        System.assertEquals('12345', SCBase.getPicklistText(SCOrder__c.Type__c, '12345'));
        System.assertEquals(null, SCBase.getPicklistText(SCOrder__c.Type__c, null));
    }
    
    static testMethod void testGetMultiPicklistValuesForQuery() 
    {
        //with several values
        String queryStr = SCBase.getMultiPicklistValuesForQuery('val1;val2;val3');
        System.assertEquals('(\'val1\',\'val2\',\'val3\')',queryStr);
        
        //with one value
        queryStr = SCBase.getMultiPicklistValuesForQuery('val1');
        System.assertEquals('(\'val1\')',queryStr);
        
        //with empty string
        queryStr = SCBase.getMultiPicklistValuesForQuery('');
        System.assertEquals('()',queryStr);
        
        //with null
        queryStr = SCBase.getMultiPicklistValuesForQuery(null);
        System.assertEquals('()',queryStr);
        
    }
    
    
}