/*
 * @(#)SCboOrder.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * BO for article handling which also includes the article details
 * which are brand specific details to an article
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboArticle
{
    // The actual article
    public SCArticle__c article { get; set; }
    
    // Brand specific details for an article
    public List<SCArticleDetail__c> articleDetails { get; set; }

    /**
     * Default Constructor
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboArticle ()
    {

    }
    
    /**
     * Constructor
     *
     * @param articleId ID of an article to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboArticle (String articleId)
    {
        fetchArticle(articleId);
    }
    
    /**
     * Constructor
     *
     * @param articleId ID of an article to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboArticle (Id articleId)
    {
        fetchArticle(articleId);
    }

    /**
     * Constructor
     *
     * @param articleId ID of an article to use
     * @param brand Brand the article details belong to
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboArticle (String articleId, String brand)
    {
        fetchArticle(articleId, brand);
    }
    
    /**
     * Constructor
     *
     * @param articleId ID of an article to use
     * @param brand Brand ID of the article details belong to
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCboArticle (Id articleId, String brand)
    {
        fetchArticle(articleId, brand);
    }

    /**
     * Create BO by using the article name.
     *
     * @param articleName name of an article to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void createByName (String articleName)
    {
        fetchArticleByName(articleName);
    }

    /**
     * Create the BO details using the article name.
     *
     * @param articleName Article name of an article to use
     * @param brand Brand the article details belong to
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void createByName (String articleName, String brand)
    {
        fetchArticleByName(articleName, brand);
    }

    /**
     * Get the article details for the given brand ID
     *
     * @param brand Brand ID the article details belong to
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCArticleDetail__c getArticleDetails(String brand)
    {
        for (SCArticleDetail__c articleDetail : articleDetails)
        {
            if (articleDetail.Brand__c == brand)
            {
                return articleDetail;
            }
        }
        
        return null;
    }
    
    /**
     * Fetch an article and the brand specific details
     *
     * @param articleId ID of an article to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    private void fetchArticle(String articleId)
    {
        try
        {
            article = [select Id, Name, ArticleNameCalc__c, Class__c, toLabel(Type__c), 
                              ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c,
                              SalesUnit__c, SalesUnitQty__c, TaxType__c, PriceGroup__c, Orderable__c, 
                              Stockable__c, ValuationType__c, 
                              (select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, 
                                      toLabel(LockType__c), PriceGroup__c
                                 from Article_Details__r) 
                         from SCArticle__c
                        where Id = :articleId];
            articleDetails = article.Article_Details__r;
        }
        catch (QueryException e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to fetch article ' + e.getMessage());
        }
    }
    
    /**
     * Fetch an article and the brand specific details
     *
     * @param articleId ID of an article to use
     * @param brand ID of the brand to consider for article details
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    private void fetchArticle(String articleId, String brand)
    {
        try
        {
            article = [select Id, Name, ArticleNameCalc__c, Class__c, toLabel(Type__c), 
                              ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c,
                              SalesUnit__c, SalesUnitQty__c, TaxType__c, PriceGroup__c, Orderable__c, 
                              Stockable__c, ValuationType__c, 
                              (select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, 
                                      toLabel(LockType__c), PriceGroup__c
                                 from Article_Details__r where Brand__c = :brand) 
                         from SCArticle__c
                        where Id = :articleId];
            articleDetails = article.Article_Details__r;
        }
        catch (QueryException e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to fetch article ' + e.getMessage());
        }
    }

    /**
     * Fetch an article and the brand specific details
     *
     * @param article Name of an article to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    private void fetchArticleByName(String articleName)
    {
        try
        {
            article = [select Id, Name, ArticleNameCalc__c, Class__c, toLabel(Type__c), 
                              ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c,
                              SalesUnit__c, SalesUnitQty__c, TaxType__c, PriceGroup__c, Orderable__c, 
                              Stockable__c, ValuationType__c, 
                              (select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, 
                                      toLabel(LockType__c), PriceGroup__c
                                 from Article_Details__r) 
                         from SCArticle__c
                        where Name = :articleName];
            articleDetails = article.Article_Details__r;
        }
        catch (QueryException e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to fetch article ' + e.getMessage());
        }
    }
    
    /**
     * Fetch an article and the brand specific details
     *
     * @param articleName Name of an article to use
     * @param brand ID of the brand to consider for article details
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    private void fetchArticleByName(String articleName, String brand)
    {
        try
        {
            article = [select Id, Name, ArticleNameCalc__c, Class__c, toLabel(Type__c), 
                              ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c,
                              SalesUnit__c, SalesUnitQty__c, TaxType__c, PriceGroup__c, Orderable__c, 
                              Stockable__c, ValuationType__c, 
                              (select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, 
                                      toLabel(LockType__c), PriceGroup__c
                                 from Article_Details__r where Brand__c = :brand) 
                         from SCArticle__c
                        where Name = :articleName];
            articleDetails = article.Article_Details__r;
        }
        catch (QueryException e)
        {
            System.debug(LoggingLevel.WARN, 'Unable to fetch article ' + e.getMessage());
        }
    }
}