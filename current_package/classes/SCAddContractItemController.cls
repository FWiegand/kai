/*
 * @(#)SCAddContractItemController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller saves selected installed bases (contract items)
 * for the actual contract.
 *
 */
public with sharing class SCAddContractItemController
{
    public String contractId;
    public String newSelectedItems { get; set; }
    public List<String> items;
    SCContract__c contract;
    public List<SCInstalledBaseLocation__c> installedBases;
    
    public List<SCInstalledBaseLocation__c> locations;
    public List<SCInstalledBase__c> bases;
    public List<SCContractItem__c> contractItems;
    
    public List<installedBaseLocations> itemsLocations;
    public List<InstalledBase> installedBase;
    public List<BaseSystem> systems;

    public String id { get; set; }
    public String name { get; set; }
    public String address { get; set; }
    public String baseId { get; set; }
    public String baseSerial { get; set; }
    public String baseIdInt { get; set; }
    public String baseName { get; set; }
    public String productModel { get; set; }
    public Boolean baseAssigned { get; set; }
    public String systemName { get; set; }
    public String contractno { get; set; }
    
    // 29.06.2011
    public List<SCInstalledBaseLocation__c> iblNew { get; set; }


    ApexPages.StandardSetController con;

/*
    public SCAddContractItemController(ApexPages.StandardController controller) {
        // we keep a reference to the controller to be able to redirect to the order page
        con = controller;

    }
*/    
    public SCAddContractItemController(ApexPages.StandardSetController controller) {
        // we keep a reference to the controller to be able to redirect to the order page
        con = controller;
        
        contractId = ApexPages.currentPage().getParameters().get('id');
        contractno = [select name from SCContract__c where id = :contractId].name;
        
    }

    /**
     * Default constructor
     *
     * @author Sergey Utko <sutko@gms-online.de>
     * @version $Revision$, $Date$
     */ 
    public SCAddContractItemController()
    {

    }
    
    /**
     * Reads all of installed base locations, installes base and base systems
     * for the actual contract
     *
     * @return    list of installes base and locations, installed base and systems
     *
     * @author Sergey Utko <sutko@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<installedBaseLocations> getIBL()
    {
        String accountId = [SELECT Account__c FROM SCContract__c WHERE Id = :contractId].Account__c;
        
        Map<Id,String> contractItems = new Map<Id,String>();
        
        List<SCContractItem__c> ci = [ Select InstalledBase__r.Id, InstalledBase__r.Name From SCContractItem__c Where Contract__c = :contractId ];
        
        if(ci.size() != 0)
        {
            for(SCContractItem__c i : ci)
            {
                contractItems.put(i.InstalledBase__r.Id,'assigned');
            }
        }
        
        Boolean assigned;

        System.debug('#### contractItems: ' + contractItems.keySet());
        
        iblNew = [ Select Id, Name, Street__c, PostalCode__c, HouseNo__c, City__c, 
                         (Select Id, Name, IdInt__c, SerialNo__c, Department__c, Room__c, Brand__r.Name, ProductModel__r.Name, System__r.Name, System__r.Id
                          From InstalledBaseLocation__r Order By System__r.Name)
                   From SCInstalledBaseLocation__c
                   Where Id In (Select InstalledBaseLocation__c From SCInstalledBaseRole__c Where Account__c = :accountId ) ];

        itemsLocations = new List<installedBaseLocations>();
        
        Integer iblCounter = 0;

        for(SCInstalledBaseLocation__c ibl : iblNew)
        {
            System.debug('#### location: ' + ibl.Name);
            
            installedBase = new List<InstalledBase>();
            systems       = new List<BaseSystem>();
            
            String systemFirst  = '+'; // old
            String systemSecond = '+'; // new
            Integer counter     = 0;  // genereller counter, läuft durch
            String oldSystemName = '';
            String oldSystemId   = '';
            String sid           = '';
            
            // if ibl.InstalledBaseLocation__r.size() != 0
            for(SCInstalledBase__c ib : ibl.InstalledBaseLocation__r)
            {
                if(counter == 0){ systemSecond = ib.System__r.Name; }
                if(counter != 0){ systemFirst  = ib.System__r.Name; }
                                
                System.debug('#### system: ' + ib.System__r.Name); 
                
                if(systemSecond == systemFirst)
                {
                    if( (counter + 1) == ibl.InstalledBaseLocation__r.size()  && ibl.InstalledBaseLocation__r.size() > 0 ) // Last installed base
                    {
                        System.debug('#### diese gruppe speichern (last IB)');
                        // Installed Base already assigned to this contract?
                        String i = contractItems.get(ib.Id);
                        if(i == null){ assigned = false; }
                        else         { assigned = true; System.debug('#### assigned!'); }
                        if(ib.System__r.Id == null){ sid   =  'nonamedSystem-' + iblCounter; }else{ sid = String.valueOf(ib.System__r.Id); }
                        installedBase.add( new InstalledBase(String.valueOf(ib.Id), String.valueOf(ib.Name),String.valueOf(ib.ProductModel__r.Name),assigned,String.valueOf(ib.IdInt__c),String.valueOf(ib.SerialNo__c),String.valueOf(ib.Department__c),String.valueOf(ib.Room__c),String.valueOf(ib.Brand__r.Name) ) );
                        systems.add( new BaseSystem(ib.System__r.Name,sid,installedBase) );
                    }
                }
                if(systemSecond != systemFirst)
                {
                    if(counter != 0)
                    { 
                        System.debug('#### vorherige gruppe speichern');  
                        systems.add( new BaseSystem(oldSystemName,oldSystemId,installedBase) );
                        installedBase = new List<InstalledBase>();
                    }
                    if( (counter + 1) == ibl.InstalledBaseLocation__r.size() ) // Only one installed base 
                    {
                        System.debug('#### diese gruppe speichern (one IB)');  
                        // Installed Base already assigned to this contract?
                        String i = contractItems.get(ib.Id);
                        if(i == null){ assigned = false; }
                        else         { assigned = true; System.debug('#### assigned!'); }
                        if(ib.System__r.Id == null){ sid   =  'nonamedSystem-' + iblCounter; }else{ sid = String.valueOf(ib.System__r.Id); }
                        installedBase.add( new InstalledBase(String.valueOf(ib.Id), String.valueOf(ib.Name),String.valueOf(ib.ProductModel__r.Name),assigned,String.valueOf(ib.IdInt__c),String.valueOf(ib.SerialNo__c),String.valueOf(ib.Department__c),String.valueOf(ib.Room__c),String.valueOf(ib.Brand__r.Name) ) );
                        systems.add( new BaseSystem(ib.System__r.Name,sid,installedBase) );
                    }
                }

                if( (counter + 1) != ibl.InstalledBaseLocation__r.size()  && ibl.InstalledBaseLocation__r.size() > 0 ) // Not last installed base
                {
                    // Installed Base already assigned to this contract?
                    String i = contractItems.get(ib.Id);
                    if(i == null){ assigned = false; }
                    else         { assigned = true; System.debug('#### assigned!'); }
                    installedBase.add( new InstalledBase(String.valueOf(ib.Id), String.valueOf(ib.Name),String.valueOf(ib.ProductModel__r.Name),assigned,String.valueOf(ib.IdInt__c),String.valueOf(ib.SerialNo__c),String.valueOf(ib.Department__c),String.valueOf(ib.Room__c),String.valueOf(ib.Brand__r.Name) ) );
                }
                
                systemFirst  = ib.System__r.Name;

                if(ib.System__r.Id == null){ oldSystemId   =  'nonamedSystem-' + iblCounter; }else{ oldSystemId = String.valueOf(ib.System__r.Id); }
                oldSystemName = ib.System__r.Name;
                
                counter++;
            }
            
            String address = String.valueOf(ibl.HouseNo__c) + ' ' + 
                             String.valueOf(ibl.Street__c) + ', ' +
                             String.valueOf(ibl.City__c) + ' ' +
                             String.valueOf(ibl.PostalCode__c);
            
            itemsLocations.add(new installedBaseLocations(String.valueOf(ibl.Id),String.valueOf(ibl.Name),address,systems));
            
            iblCounter++;
        }
        
        return itemsLocations;
    }
    
    
    /**
     * Cancel the process and redirect user back to the contract page
     *
     * @return    Reference to contract page
     *
     * @author Sergey Utko <sutko@gms-online.de>
     * @version $Revision$, $Date$
     */ 
    public PageReference cancelButton()
    {
        PageReference pageRef = new PageReference('/'+contractId);
        return pageRef;
    }
    
    
    /**
     * Saves selected contract items for this contract
     *
     * @return    Reference to contract page
     *
     * @author Sergey Utko <sutko@gms-online.de>
     * @version $Revision$, $Date$
     */ 
    public PageReference saveItems()
    {
        List<SCContractItem__c> itemsToInsert = new List<SCContractItem__c>();
        
        System.debug('#### newSelectedItems : ' + newSelectedItems);
        if (SCBase.isSet(newSelectedItems))
        {
            items = newSelectedItems.split(',');
            
            for (Integer i=0; i < items.size(); i++)
            {
                SCContractItem__c ob = new SCContractItem__c(Contract__c = contractId, InstalledBase__c = items.get(i));
                itemsToInsert.add(ob);
            }
            
            if(itemsToInsert.size() > 0)
            {
                insert itemsToInsert;

                // add the contract reference to the installed base items (redundant but needed to increase the performance)
                List<SCInstalledBase__c> ibitems = [select id, contractref__c from SCInstalledBase__c where id in :items];            
                for(SCInstalledBase__c ib : ibitems)
                {
                    if(ib.contractref__c == null)
                    {
                        ib.contractref__c = contractno;
                    }
                    else
                    {
                        if(!ib.contractref__c.contains(contractno))
                        {
                            ib.contractref__c += ',' + ib.contractref__c;
                        }
                    }
                }
                update ibitems;
            }
            System.debug('#### itemsToInsert: ' + itemsToInsert);
                
            contractId = ApexPages.currentPage().getParameters().get('id');
            PageReference pageRef = new PageReference('/'+contractId);
            pageRef.setRedirect(true);
            return pageRef.setRedirect(true);
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.SC_msg_PleaseSelectItems);
        ApexPages.addMessage(myMsg);
        // nothing selected, show the page again
        return null;
    }
    
    
    
    // Container for installed base locations, installed base and system
    public class InstalledBaseLocations
    {
        public String id { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public List<BaseSystem> baseSystem { get; set; }
        
       
        installedBaseLocations(String id, String name, String address,List<BaseSystem> baseSystem)
        {
            this.id = id;
            this.name = name;
            this.address = address;
            this.baseSystem = baseSystem;
        }
    }
    
    
    // Container for installed base and systems
    public class BaseSystem
    {
        public String systemName { get; set; }
        public String systemId { get; set; }
        public List<InstalledBase> installedBase { get; set; }

        
        baseSystem(String systemName, String systemId, List<InstalledBase> installedBase)
        {
            this.systemName = systemName;
            this.systemId = systemId;
            this.installedBase = installedBase;
        }
    }
    

    // Container for installed base and base-system assignment
    public class InstalledBase
    {
        public String baseId { get; set; }
        public String baseSerial { get; set; }
        public String baseIdInt { get; set; }
        public String baseName { get; set; }
        public String productModel { get; set; }
        public Boolean baseAssigned { get; set; }
        public String baseDepartment { get; set; }
        public String baseRoom { get; set; }
        public String baseBrand { get; set; }
        
        installedBase(String baseId, String baseName, String productModel, Boolean baseAssigned, String baseSerial, String baseIdInt, String baseDepartment, String baseRoom, String baseBrand)
        {
            this.baseId = baseId;
            this.baseName = baseName;
            this.productModel = productModel;
            this.baseAssigned = baseAssigned;
            this.baseSerial = baseSerial;
            this.baseIdInt = baseIdInt;            
            this.baseDepartment = baseDepartment;    
            this.baseRoom = baseRoom;    
            this.baseBrand = baseBrand;    
        }
    }
        
}