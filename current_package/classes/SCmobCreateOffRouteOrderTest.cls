/*
 * @(#)SCmobCreateOffRouteOrder.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Unit Test Class for SCmobCreateOffRouteOrder 
 */
@isTest
private class SCmobCreateOffRouteOrderTest 
{
    public static final String IB_SERIAL_NO = '33149604';
    public static final String ENGINEER = 'DE004094';


    private static SCmobCreateOffRouteOrder.Input setUp()
    {
        
        // TODO: Create Engineers for testing purposes
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet(true);
        
        /* TODO: Use the following statement to randomly select a pair of correct Symptomcodes:
                 SELECT Slave__r.Name, Master1__r.Name
                   FROM SCDomainDepend__c
                  WHERE Master1Ref__c = 'DOM_ERRSYM1'
         */
        
        SCmobCreateOffRouteOrder.Input input;
        // TODO: Randomly selectable Engineer?
        // TODO: Randomly selectable InstalledBase?
        
        // TODO: create a correct Input Object
        input = new SCmobCreateOffRouteOrder.Input();
        input.resourceID = ENGINEER;
        input.installedBaseSerialNo = IB_SERIAL_NO; 
        input.orderType = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT; 
        input.errorSymptom1 = 'ZDEOBJ1'; 
        input.errorSymptom2 = 'A003';
        input.startTime = Datetime.now();
        input.duration = 60; // TODO: choose randomly
        
        return input;
    }
 
    /**
     * Check if a correct Input passes the validation.
     */
    static testMethod void testValidateSuccess() 
    {
        SCmobCreateOffRouteOrder.Input input = setUp();
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E000', result.status);
    }
    
    /**
     * Check if the validation can detect a wrong Engineer.
     */
    static testMethod void testValidateWrongEngineer() 
    {
        // TODO: Bonus -> Engineer is not a field Engineer.
        
        SCmobCreateOffRouteOrder.Input input = setUp();
        input.resourceID = '12340123faöfasd034'; // TODO: Somehow generate some garbage!
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E102', result.status);
            
    } 
   
    /** 
     * Check if the validation can detect that an InstalledBase with the given SerialNumber does not exist.
     */
    static testMethod void testValidateInstalledBaseDoesNotExist()
    {
        // TODO: failue because IB with given SerialNo does not exist
        SCmobCreateOffRouteOrder.Input input = setUp();
        input.installedBaseSerialNo = '156764864687478646'; 
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E102', result.status);     
    } 
    
    /**
     * Test whether validation only allows creation of orders with type 'ZC02-1 Reparatur'.
     */
    static testMethod void testValidateWrongOrderType()
    {
        SCmobCreateOffRouteOrder.Input input = setUp();
        input.orderType = '5731'; // TODO: test different order types except '5701'
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E102', result.status);
    }
    
    // TODO: test the symptom-codes including DomainDependency
    
    static testMethod void testValidateDurationTooSmall()
    {
        SCmobCreateOffRouteOrder.Input input = setUp();
        input.duration = 5; // < 15 min is not allowed!
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E102', result.status);
    }
    
    static testMethod void testValidateDurationTooLarge()
    {
        SCmobCreateOffRouteOrder.Input input = setUp();
        input.duration = 1000; // < 15 min is not allowed!
        SCmobResult result = null;
        
        Test.startTest();
           result = SCmobCreateOffRouteOrder.validate(input);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, result);
        
        // Check error code for failed validation
        System.assertEquals('E102', result.status);
    }
   
    static testMethod void testSelectResourceSuccess()
    {
        // TODO: think about testing with several Engineers from a List
        setUp();
        SCResource__c resource = null;
        
        Test.startTest();
           resource = SCmobCreateOffRouteOrder.selectResource(ENGINEER);
        Test.stopTest();
        
        // Output object must never be null
        System.assertNotEquals(null, resource);
        
        // Names should match
        System.assertEquals(ENGINEER, resource.Name);
    }
    
    static testMethod void testSelectResourceFuzzedEngineerName()
    {
        // TODO: think about testing with randomly generated Strings
        setUp();
        String testName = 'AGD29347290DFEad';
        SCResource__c resource = null;
        
        Test.startTest();
           resource = SCmobCreateOffRouteOrder.selectResource(testName);
        Test.stopTest();
        
        // Output object must never be null
        System.assertEquals(null, resource);
    }
    
    static testMethod void testCreateOrderSuccess()
    {
       SCmobCreateOffRouteOrder.Input input = setUp();
       SCmobResult result = SCmobCreateOffRouteOrder.createOrder(input, true);
       
       // Should have been successful:
       System.assertEquals('E000', result.status);
       
       // Should not be empty
       System.assertNotEquals(0, result.data.size());
    }
   
}