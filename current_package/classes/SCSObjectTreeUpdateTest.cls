/*
 * @(#)SCSObjectTreeUpdateTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCSObjectTreeUpdateTest
{
	/*
    static testMethod void testPositive1()
    {
        Test.StartTest();
        SCContractTestHelper cth = new SCContractTestHelper();
        String contractID = cth.prepareForBtcContractVisitCreateTest();
        System.debug('###Test contract: ' + cth.contract);
        System.debug('###ContractId: ' + contractID);
        SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
        
        // Order Creation
        SCContract__c c = [Select Name, Id, IdExt__c from SCContract__c where Id = :contractID];
        System.debug('### contract c: ' + c);
        List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
        List<String> visitIdList = new List<String>();
        for(SCContractVisit__c visit: visitList)
        {
            visitIdList.add(visit.Id);
            // create only one order
            break;
        } 

        SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
        List<SCContractVisit__c> vl = [Select Order__c, Contract__c from SCContractVisit__c where id in : visitIdList];
        Id orderId = null;
        if(vl.size() > 0)
        {
            orderId = vl[0].Order__c;
            System.assertEquals(contractID, vl[0].Contract__c);
        }

        if(orderId != null)
        {
            // update IDExt__c in the order and in the contract
            String idExt = 'Jerzy';
            c.IdExt__c = idExt;
            update c; 
            System.debug('### contract c after update: ' + c);

            SCOrder__c order = [Select id from SCOrder__c where id = :orderId];
            order.IdExt__c = idExt;
            order.Contract__c = null;
            update order;

            SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
            u.enableDebug();
            u.email = 'jpietrzyk@gms-online.de';
            u.addQuery('order','select id, Contract__c, IDOld__c, IDExt__c, status__c, Channel__c, Closed__c, CustomerPrefStart__c, Info__c, (Select Start__c from Appointments__r) from SCOrder__c where id = \'' + orderId  + '\' ');
            //CNTR-0000043699
            Boolean preventUpdateRootIfEmpty = true;
            u.addQuery(preventUpdateRootIfEmpty,
                       'order.contract', 
                       'select id, IdExt__c, OfferDate__c, template__r.OfferDate__c from SCContract__c where IDExt__c = {order#IDExt__c}');
            u.addQuery('order.contract.visit1', 'select id, Contract__c, Order__c, DueDate__c, Status__c from SCContractVisit__c '
                + ' where Contract__c = {order.contract#ID} and Status__c = \'schedule\'');
            u.addQuery('order.contract.visit2', 'select id, Contract__c, Order__c, DueDate__c, Status__c from SCContractVisit__c '
                + ' where Contract__c = {order.contract#ID} and Status__c = \'schedule\'');
            
            u.setFixedValue('order#Status__c', '5501');
            u.setVariableValue('order#Status__c', 'Status__c', true);
            u.setVariableValueToParent('order.contract', 'order#Contract__c', 'order.contract#Id', true);
            u.setFixedValueToParent('order.contract', 'order#Info__c', 'setFixedValueToParentTest');
            // getValueFromTree
            u.setVariableValue('order.contract#OfferDate__c', 'order.contract#template__r.OfferDate__c', true);
                         
            u.setFixedValue('order.contract.visit1#Status__c', 'schedule');
            u.setVariableValue('order.contract.visit1#DueDate__c', 'order#CustomerPrefStart__c', true);
            u.setVariableValue('order.contract.visit1#Order__c', 'order#Id', true);
            
            u.setVariableValue('order.contract.visit2#DueDate__c', 'order#Appointments__r[0].Start__c', false);
            u.setVariableValue('order.contract.visit2#Order__c', 'order#Id', true);
            
            // Mapping of the order status to a visit status
            Map<String, String> ma = new Map<String, String>();
            ma.put('5501', 'schedule');     // open
            ma.put('5502', 'scheduled');    // planned
            ma.put('5503', 'scheduled');    // transferred
            ma.put('5505', 'completed');     // completed offline        
            ma.put('5506', 'completed');    // completed
            ma.put('5507', 'skip');     // cancelled
            ma.put('5508', 'completed');    // closed final
            ma.put('5509', 'schedule'); // planned for precheck
            ma.put('5510', 'schedule'); // released for processing
            ma.put('5520', 'schedule'); // Waiting for approval
            ma.put('5521', 'scheduled');    // Partly delivered
            
            u.setVariableValue('order.contract.visit2#Status__c', 'order#Status__c', true, ma);
            u.email = 'jpietrzyk@gms-online.de';
            u.syncUpdate();
            SCOrder__c order2 = [Select id, Contract__c, IDExt__c, Info__c from SCOrder__c where id = :orderId];
            System.debug('###....order2: ' + order2);
            String orderContract = (String)order2.Contract__c;
            System.debug('###....contractID: ' + contractID + ', orderContract: ' + orderContract);
            System.assertEquals(contractID, orderContract);
            String orderInfo = order2.Info__c;
            System.assertEquals('setFixedValueToParentTest', orderInfo);
 
        }                    
        Test.StopTest();
    } // testPositive1
	*/

    static testMethod void testNegative1()
    {
        try
        {
            Test.StartTest();
            SCContractTestHelper cth = new SCContractTestHelper();
            String contractID = cth.prepareForBtcContractVisitCreateTest();
            System.debug('###Test contract: ' + cth.contract);
            System.debug('###ContractId: ' + contractID);
            SCbtcContractVisitCreate.syncCreateContractVisits(contractID);
            
            // Order Creation
            SCContract__c c = [Select Name, Id, IdExt__c from SCContract__c where Id = :contractID];
            System.debug('### contract c: ' + c);
            List<SCContractVisit__c> visitList = [Select Id from SCContractVisit__c where Contract__c = :contractID];
            List<String> visitIdList = new List<String>();
            for(SCContractVisit__c visit: visitList)
            {
                visitIdList.add(visit.Id);
                // create only one order
                break;
            } 
    
            SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
            List<SCContractVisit__c> vl = [Select Order__c, Contract__c from SCContractVisit__c where id in : visitIdList];
            Id orderId = null;
            if(vl.size() > 0)
            {
                orderId = vl[0].Order__c;
                System.assertEquals(contractID, vl[0].Contract__c);
            }
    
            if(orderId != null)
            {
                // update IDExt__c in the order and in the contract
                String idExt = 'Jerzy';
                c.IdExt__c = idExt;
                update c; 
                System.debug('### contract c after update: ' + c);
    
                SCOrder__c order = [Select id from SCOrder__c where id = :orderId];
                order.IdExt__c = idExt;
                order.Contract__c = null;
                update order;
    
                SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
                u.enableDebug();
                u.email = 'jpietrzyk@gms-online.de';
                u.addQuery('order','select falseName, id, Contract__c, IDOld__c, IDExt__c, status__c, Channel__c, Closed__c, CustomerPrefStart__c, Info__c, (Select Start__c from Appointments__r) from SCOrder__c where id = \'' + orderId  + '\' ');
                //CNTR-0000043699
                Boolean preventUpdateRootIfEmpty = true;
                u.addQuery(preventUpdateRootIfEmpty,
                           'order.contract', 
                           'select id, IdExt__c, OfferDate__c, template__r.OfferDate__c from SCContract__c where IDExt__c = {order#IDExt__cXXX}');
                u.addQuery('order.contract.visit1', 'select id, Contract__c, Order__c, DueDate__c, Status__c from SCContractVisit__c '
                    + ' where Contract__c = {order.contract#ID} and Status__c = \'schedule\'');
                u.addQuery('order.contract.visit2', 'select id, Contract__c, Order__c, DueDate__c, Status__c from SCContractVisit__c '
                    + ' where Contract__c = {order.contract#ID} and Status__c = \'schedule\'');
                
                // ### false field names
                u.setFixedValue('order#Status__cXXX', '5501');
                u.setVariableValue('order#Status__c', 'Status__cXXX', true);
                u.setVariableValueToParent('order.contractXXX', 'order#Contract__c', 'order.contract#Id', true);
                u.setFixedValueToParent('order.contract', 'order#Info__cXXX', 'setFixedValueToParentTest');
                // getValueFromTree
                u.setVariableValue('order.contract#OfferDate__c', 'order.contract#template__r.OfferDate__c', true);
                             
                u.setFixedValue('order.contract.visit1#Status__c', 'schedule');
                u.setVariableValue('order.contract.visit1#DueDate__c', 'order#CustomerPrefStart__cXXX', true);
                u.setVariableValue('order.contract.visit1#Order__c', 'order#Id', true);
                
                u.setVariableValue('order.contract.visit2#DueDate__c', 'order#Appointments__r[0].Start__c', false);
                u.setVariableValue('order.contract.visit2#Order__c', 'order#Id', true);
                
                // Mapping of the order status to a visit status
                Map<String, String> ma = new Map<String, String>();
                ma.put('5501', 'schedule');     // open
                ma.put('5502', 'scheduled');    // planned
                ma.put('5503', 'scheduled');    // transferred
                ma.put('5505', 'completed');     // completed offline        
                ma.put('5506', 'completed');    // completed
                ma.put('5507', 'skip');     // cancelled
                ma.put('5508', 'completed');    // closed final
                ma.put('5509', 'schedule'); // planned for precheck
                ma.put('5510', 'schedule'); // released for processing
                ma.put('5520', 'schedule'); // Waiting for approval
                ma.put('5521', 'scheduled');    // Partly delivered
                
                u.setVariableValue('order.contract.visit2#Status__c', 'order#Status__c', true, ma);
                u.email = 'jpietrzyk@gms-online.de';
                u.syncUpdate();
                SCOrder__c order2 = [Select id, Contract__c, IDExt__c, Info__c from SCOrder__c where id = :orderId];
                System.debug('###....order2: ' + order2);
                String orderContract = (String)order2.Contract__c;
                System.debug('###....contractID: ' + contractID + ', orderContract: ' + orderContract);
                System.assertEquals(contractID, orderContract);
                String orderInfo = order2.Info__c;
                System.assertEquals('setFixedValueToParentTest', orderInfo);
     
            }                    
            Test.StopTest();
        }
        catch(Exception e)
        {
        }            
    } // testNegative1

    static testMethod void testInsertAsBatchWithoutAssertBecauseItIsNotPossible()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 2);
        //------------------------------------------------
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('MaterialMovementInsert');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u.enableDebug();
        // the root query for orders
        u.addQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentID + '\'');
        
        // the child query for reading material movements
        u.addQuery('replenishment.movement', 
                        'select id, Qty__c, Replenishment__c, Article__c, Assignment__c, Order__c, OrderLine__c, OrderRole__c, Stock__c from SCMaterialMovement__c where Replenishment__c = {replenishment#ID}');
        
        u.addInsertQuery('replenishment.movement.newMovement', 'select id, Qty__c, Replenishment__c, Article__c, Assignment__c, Order__c, OrderLine__c, OrderRole__c, Stock__c from SCMaterialMovement__c'); 
        
        u.setVariableValue('replenishment.movement.newMovement#Article__c', 'replenishment.movement#Article__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Assignment__c', 'replenishment.movement#Assignment__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Replenishment__c', 'replenishment.movement#Replenishment__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Order__c', 'replenishment.movement#Order__c', false);
        u.setVariableValue('replenishment.movement.newMovement#OrderLine__c', 'replenishment.movement#OrderLine__c', false);
        u.setVariableValue('replenishment.movement.newMovement#OrderRole__c', 'replenishment.movement#OrderRole__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Stock__c', 'replenishment.movement#Stock__c', false);
        u.setFixedValue('replenishment.movement.newMovement#Qty__c', '2'); 
        
        u.email = 'jpietrzyk@gms-online.de';
        // DML Statement limit 150
        // 1 DML Statement SavePoint
        // 1 DML Statment Rollback
        // 150/4 ~ 35
        Integer batchSize = 25;
        Database.executeBatch(u, batchSize);
//        List<SCMaterialMovement
//        System.assertEquals(2, mml.size());        
        Test.StopTest();
    } // testInsert


    static testMethod void testInsertWithAssert()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 2);
        //------------------------------------------------
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('MaterialMovementInsert');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u.enableDebug();
        // the root query for orders
        u.addQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentID + '\'');
        
        // the child query for reading material movements
        u.addQuery('replenishment.movement', 
                        'select id, Qty__c, Replenishment__c, Article__c, Assignment__c, Order__c, OrderLine__c, OrderRole__c, Stock__c from SCMaterialMovement__c where Replenishment__c = {replenishment#ID}');
        
        u.addInsertQuery('replenishment.movement.newMovement', 'select id, Qty__c, Replenishment__c, Article__c, Assignment__c, Order__c, OrderLine__c, OrderRole__c, Stock__c from SCMaterialMovement__c'); 
        
        u.setVariableValue('replenishment.movement.newMovement#Article__c', 'replenishment.movement#Article__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Assignment__c', 'replenishment.movement#Assignment__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Replenishment__c', 'replenishment.movement#Replenishment__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Order__c', 'replenishment.movement#Order__c', false);
        u.setVariableValue('replenishment.movement.newMovement#OrderLine__c', 'replenishment.movement#OrderLine__c', false);
        u.setVariableValue('replenishment.movement.newMovement#OrderRole__c', 'replenishment.movement#OrderRole__c', false);
        u.setVariableValue('replenishment.movement.newMovement#Stock__c', 'replenishment.movement#Stock__c', false);
        u.setFixedValue('replenishment.movement.newMovement#Qty__c', '2'); 
        
        u.email = 'jpietrzyk@gms-online.de';
        // DML Statement limit 150
        // 1 DML Statement SavePoint
        // 1 DML Statment Rollback
        // 150/4 ~ 35
        Integer batchSize = 25;
        u.syncUpdate();
        List<SCMaterialMovement__c> mml = [select id from SCMaterialMovement__c where Replenishment__c = : replenishmentId];
        System.assertEquals(2, mml.size());        
        Test.StopTest();
    } // testInsert


    static testMethod void testDelete()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('Replenishment-Delete');
        u.disableSavePoint();
        u.enableInterfaceLog();
        u.enableDebug();
        u.disableSemanticChecking();
        // the root query for orders
        u.addDeleteQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        u.setFixedValue('replenishment#Ordered__c', '0'); 
        u.email = 'jpietrzyk@gms-online.de';
        // DML Statement limit 150
        // 1 DML Statement SavePoint
        // 1 DML Statment Rollback
        // 150/4 ~ 35
        Integer batchSize = 25;
        Database.executeBatch(u, batchSize);
        
        Test.StopTest();
    } // testDelete

    static testMethod void testDeleteWithAssert()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('Replenishment-Delete');
        u.disableSavePoint();
        u.enableInterfaceLog();
        u.enableDebug();
        u.disableSemanticChecking();
        // the root query for orders
        u.addDeleteQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        u.setFixedValue('replenishment#Ordered__c', '0'); 
        u.email = 'jpietrzyk@gms-online.de';
        // DML Statement limit 150
        // 1 DML Statement SavePoint
        // 1 DML Statment Rollback
        // 150/4 ~ 35
        Integer batchSize = 25;
        u.syncUpdate();
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = :replenishmentId];
        System.assertEquals(0, mrl.size());
        
        Test.StopTest();
    } // testDelete

    static testMethod void testSimpleDeleteWithAssert()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);

        List<SCMaterialReplenishment__c> mrlBefore = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = :replenishmentId];
        System.assertEquals(true, mrlBefore.size() > 0);
        
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        Boolean async = false;
        String objectName = 'SCMaterialReplenishment__c';
        String eMailAddress = 'jpietrzyk@gms-online.de';
        String condition = 'ID = ' + '\'' + replenishmentID + '\''; 
		Boolean enableDebug = true;
		
		u.deleteObject(async, objectName, condition, eMailAddress, enableDebug);
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = :replenishmentId];
        System.assertEquals(0, mrl.size());
        
        Test.StopTest();
    } // testSimpleDeleteWithAssert

    static testMethod void testSimpleDeleteWithAssert2()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);

        List<SCMaterialReplenishment__c> mrlBefore = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = :replenishmentId];
        System.assertEquals(true, mrlBefore.size() > 0);
        
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        Boolean async = false;
        String objectName = 'SCMaterialReplenishment__c';
        String condition = 'ID = ' + '\'' + replenishmentID + '\''; 
		String interfaceHandlerName = 'Delete-MaterialReplenishment';

        u.email = 'jpietrzyk@gms-online.de';
        u.setInterfaceHandler(interfaceHandlerName);
        u.disableSavePoint();
        u.enableInterfaceLog();
        u.enableDebug();
		
		u.deleteObject(async, objectName, condition);
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = :replenishmentId];
        System.assertEquals(0, mrl.size());
        
        Test.StopTest();
    } // testSimpleDeleteWithAssert2

    static testMethod void testSummation()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        ID mmID1 = createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 2);
        ID mmID2 = createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 3);
        
        // Calculating of summary fields Items__c, Ordered__c in SCMaterialReplenishment__c on the base of dependent SCMaterialMovement__c's
        // Items__c = number of dependent SCMaterialMovement__c objects
        // Ordered__c = sum of dependent SCMaterialMovement__c.Qty__c
        //
        //------------------------------------------------
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('Replenishment-SummaryFields');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u.enableDebug();
        // the root query for orders
        u.addQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        
        // the child query for reading material movements
        u.addQuery('replenishment.movement', 
                        'select id, Qty__c, Replenishment__c from SCMaterialMovement__c where Replenishment__c = {replenishment#ID}');
        
        u.setItemsCountToParent('replenishment.movement', 'replenishment#Items__c');              
        u.setSumToParent('replenishment.movement', 'replenishment#Ordered__c', 'replenishment.movement#Qty__c');
        
        u.email = 'jpietrzyk@gms-online.de';
        u.syncUpdate();
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id =: replenishmentId];
        System.assertEquals(1, mrl.size());
        System.assertEquals(2, mrl[0].Items__c);
        System.assertEquals(5, mrl[0].Ordered__c);
                        
        Test.StopTest();
    } // testSummation

    static testMethod void testIntegerIntoDecimal()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        ID mmID1 = createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 2);
        ID mmID2 = createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 3);
        
        // Calculating of summary fields Items__c, Ordered__c in SCMaterialReplenishment__c on the base of dependent SCMaterialMovement__c's
        // Items__c = number of dependent SCMaterialMovement__c objects
        // Ordered__c = sum of dependent SCMaterialMovement__c.Qty__c
        //
        //------------------------------------------------
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('Replenishment-SummaryFields');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u.enableDebug();
        // the root query for orders
        u.addQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        
        // the child query for reading material movements
        u.addQuery('replenishment.movement', 
                        'select id, Qty__c, Replenishment__c from SCMaterialMovement__c where Replenishment__c = {replenishment#ID}');
        
        u.setItemsCountToParent('replenishment.movement', 'replenishment#Items__c');              
        u.setSumToParent('replenishment.movement', 'replenishment#Ordered__c', 'replenishment.movement#Qty__c');
        
        u.email = 'jpietrzyk@gms-online.de';
        u.syncUpdate();
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c, Delivered__c from SCMaterialReplenishment__c where id =: replenishmentId];
        System.assertEquals(1, mrl.size());
        System.assertEquals(2, mrl[0].Items__c);
        System.assertEquals(5, mrl[0].Ordered__c);
        
        // Integer into Decimal
        SCSObjectTreeUpdate u2 = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u2.setInterfaceHandler('Replenishment-SummaryFields');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u2.enableDebug();
        // the root query for orders
        u2.addQuery('replenishment','select id, Items__c, Ordered__c, Delivered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        
        
        u2.setVariableValue('replenishment#Delivered__c','Items__c', true);              
        
        u2.email = 'jpietrzyk@gms-online.de';
        u2.syncUpdate();
        
        List<SCMaterialReplenishment__c> mrl2 = [select id, Items__c, Ordered__c, Delivered__c from SCMaterialReplenishment__c where id =: replenishmentId];
        System.assertEquals(1, mrl2.size());
        System.assertEquals(2, mrl2[0].Delivered__c);
                        
        Test.StopTest();
    } // testIntegerToDecimal

/*
    static testMethod void testStringToDecimal()
    {
        Test.StartTest();
        SCHelperTestClass.createTestPlant(true);
        SCHelperTestClass.createTestStocks(SCHelperTestClass.plant.Id, true);        
        ID replenishmentID = createReplenishment(SCHelperTestClass.stocks.get(2).Id);
        ID mmID1 = createMaterialMovement(SCHelperTestClass.stocks.get(2).Id, replenishmentID, 2);
        
        // Calculating of summary fields Items__c, Ordered__c in SCMaterialReplenishment__c on the base of dependent SCMaterialMovement__c's
        // Items__c = number of dependent SCMaterialMovement__c objects
        // Ordered__c = sum of dependent SCMaterialMovement__c.Qty__c
        //
        //------------------------------------------------
        SCSObjectTreeUpdate u = new SCSObjectTreeUpdate();
        // InterfaceLog.Interface = 'SCRIPT'
        u.setInterfaceHandler('Replenishment-SummaryFields');
        //u.disableSavePoint();
        //u.enableInterfaceLog();
        u.enableDebug();
        // the root query for orders
        u.addQuery('replenishment','select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id = \'' + replenishmentId + '\'');
        
        // the child query for reading material movements
        u.addQuery('replenishment.movement', 
                        'select id, Qty__c, Replenishment__c, SerialNo__c from SCMaterialMovement__c where Replenishment__c = {replenishment#ID}');
        
        u.setItemsCountToParent('replenishment.movement', 'replenishment#Items__c');              
        u.setVariableValue('replenishment.movement#Qty__c', 'replenishment.movement#SerialNo__c', true);
        u.setVariableValueToParent('replenishment.movement', 'replenishment#Ordered__c', 'replenishment.movement#SerialNo__c', true);
        
        u.email = 'jpietrzyk@gms-online.de';
        u.syncUpdate();
        
        List<SCMaterialReplenishment__c> mrl = [select id, Items__c, Ordered__c from SCMaterialReplenishment__c where id =: replenishmentId];
        System.assertEquals(1, mrl.size());
        System.assertEquals(10.314, mrl[0].Ordered__c);

        List<SCMaterialMovement__c> mml = [select id, Qty__c from SCMaterialMovement__c where id = : mmID1];
        System.assertEquals(1, mml.size());
        System.assertEquals(10.314, mml[0].Qty__c);                        
        Test.StopTest();
    } // testStringToDecimal
*/

    public static ID createReplenishment(ID stockId)
    {
        ID retValue = null;
        SCMaterialReplenishment__c mr = new SCMaterialReplenishment__c();
        mr.Stock__c = stockID;
        mr.RequisitionDate__c = Date.today();
        insert mr;
        retValue = mr.ID;
        return retValue;
    }

    public static ID createMaterialMovement(ID stockId, ID replenishmentId, Decimal Qty)
    {
        ID retValue = null;
        SCMaterialMovement__c mm = new SCMaterialMovement__c();

        SCArticle__c a = createArticle('TestArticle', '201','KGM', 1, 1, null, '50201');
        mm.Article__c = a.Id;
        mm.Assignment__c = null;
        mm.Replenishment__c = replenishmentId;
        mm.Stock__c = stockId;
        mm.Qty__c = Qty;
        mm.SerialNo__c = '10.314';
        insert mm;
        retValue = mm.Id;
        return retValue;
    }



    public static SCArticle__c createArticle(String name, 
                                     String unit,
                                     String weightUnit, 
                                     Decimal weight,
                                     Decimal volume,
                                     String articleReplacedBy,
                                     String lockType)
    {
        Id articleReplacedById = null;
        if(articleReplacedBy != null)
        {
            List<SCArticle__c> articleList = [Select id from SCArticle__c where name =  :articleReplacedBy];
            if(articleList.size() > 0)
            {
                articleReplacedById = articleList[0].id;
            }
        }
        SCArticle__c a = new SCArticle__c();
        a.Name = name;
        a.Unit__c = unit;
        a.WeightUnit__c = weightUnit;
        a.Weight__c = weight;
        a.Volume__c = volume;
        a.LockType__c = lockType;
        if(articleReplacedById != null)
        {
            a.ReplacedBy__c = articleReplacedById;
        }
        upsert a;
        
        return a;
    }

    static testMethod void testMuster()
    {
        Test.StartTest();
        Test.StopTest();
    } // testMuster

}