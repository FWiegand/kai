/*
 * @(#)SCOrderRebookExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This extension is used to rebook an order.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderRebookExtension 
{
    public SCboOrder boOrder {get; set;}
    public SCboOrderItem boOrderItem {get; set;}
    public String empty {get; set;}
    
    
    public SCOrderRebookExtension(ApexPages.StandardController controller) 
    {
        empty = '';
        boOrder = new SCboorder();
        boOrder.readById(controller.getId());
        boOrderItem = ((null != boOrder.boOrderItems) && (boOrder.boOrderItems.size() > 0)) ? boOrder.boOrderItems[0] : null;
    }

    public Boolean canRebook
    {
        get
        {
            System.debug('#### GetCanRebook()');
            if ((null != boOrder) && (null != boOrder.order.type__c) && (null != boOrder.order.status__c))
            {
                return !(SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED.equals(boOrder.order.status__c) ||
                         SCfwConstants.DOMVAL_ORDERTYPE_NOT_SCHEDULEABLE.contains(boOrder.order.type__c));
            }
            return false;                     
        }
        
        private set;
    } // canRebook

    public Boolean mustCopy
    {
        get
        {
            if ((null != boOrder) && (null != boOrder.order.type__c))
            {
                return (SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED.equals(boOrder.order.status__c) || 
                        SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED.equals(boOrder.order.status__c) || 
                        SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL.equals(boOrder.order.status__c));
            }
            return false;                     
        }
        
        private set;
    } // mustCopy
    
    /*
     * Everything is ok, we can jump to the order creation page, in modus rebook.
     */
    public PageReference onContinue()
    {
        SCboOrder rebookOrder = boOrder;
        
        if (mustCopy)
        {
            rebookOrder = boOrder.copyOrder();
            
            rebookOrder.order.OrderOrigin__c = boOrder.order.Id;
            rebookOrder.order.OrderOrigin__r = boOrder.order;
            rebookOrder.save();
            
            rebookOrder.order.Info__c = System.Label.SC_msg_RebookOfOrder + ' ' + boOrder.order.Name;
            rebookOrder.save();
        }
        
        SCOrderRole__c roleLE = rebookOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

        String url = '/apex/' + appSettings.ORDERCREATION_PAGE__c + '?aid=' + roleLE.Account__c + '&oid=' + rebookOrder.order.id + '&reBook=1';
        return new PageReference(url);
    } // onContinue
}