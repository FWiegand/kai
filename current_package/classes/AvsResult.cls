/*
 * @(#)AvsResult.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Data container for address validation results. 
 * Contains a list of matched addresses and status information
 * @author Norbert Armbruster <>narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
global class AvsResult 
{
    // Status codes 
    public final static String STATUS_BLANK = '';  // initial value - notthin validated
    public final static String STATUS_EXACT = 'EXACT';  // exactly one address found 
    public final static String STATUS_MULTI = 'MULTI';  // multiple addressed found 
    public final static String STATUS_APROX = 'APROX';  // approximated address / geocode found
    public final static String STATUS_ERROR = 'ERROR';  // exception 
    public final static String STATUS_EMPTY = 'EMPTY';  // Web service returned no results
    
    // an array of addresses - filled by the avs
    public List<AvsAddress> items {get; set;}
    // Overall status of the address validation / geocoding
    public String status {get; set;}
    // Status information from the web service called - can contain native error information
    public String statusInfo {get; set;}
    
    public AvsResult()
    {
        items = new List<AvsAddress>();
        status = STATUS_BLANK;
        statusInfo = '';
    }
}