/*
 * @(#)SCContractExtension.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This extension is used to support the scheduling of contract visits
 *
 * @author na <narmbruster@gms-online.de>
 *
 * TODO:  Finalize Code 
 * 
    Validation Rules for Contract Visits
                                                        Contract Visit
    Visit          Order                                Create Schedule Cancel Skip Del  
    ---------------------------------------------------------------------------------------------
    open           -                                    x      -        -      x    x    
    created        open                                 -      x        x      -    -    
                   planned                              -      -        x      -    -    
                   planned for precheck                 -      -        x      -    -     
                   planned released for download        -      -        x      -    -    
                   accepted                             -      -        -      -    -
                   completed                            -      -        -      -    -
                   closed final                         -      -        -      -    x
                   cancelled                            -      -        -      -    x
    schedule       open                                 -      x        x      -    -
    scheduled                                           -      -        -      -    -   
    skip                                                -      -        -      -    x
    cancelled                                           -      -        -      -    x
    
    open      - the contract visit was created ( after creation of record)
    created   - after its order has been manually created
    schedule  - after its order has been automatically created 
    scheduled - after its order has been automatically dispatched
    cancelled - after cancellation
    skip      - the customer does not want a maintenance - skip the contract visit
*/                    
public with sharing class SCContractExtension 
{
    public Account acc{get; set;}
    public SCContract__c contract {get; set;}
    public List<SCContractVisit__c> pendingVisits {get; set;}
    public List<SCContactHistory__c> contactHistory {get; set;}
    public List<VisitItem> visititems {get; set;}
    public Map<String, VisitItem> visititemsmap;
    public Map<String, ContactItem> contacthistorymap;
    
    public List<ContactItem> contactitems {get; set;}

    public SCContactHistory__c contHist {get; set;}

    
    public String paramVisit {get; set;}

    public Boolean showAllVisits {get; set;} 

    public Boolean canSchedule {get; set;}  
    
    public Boolean ShowAppointmentProposals {get; set;}
    
    public SCContractExtension(ApexPages.StandardController controller) 
    {
        String conid = controller.getId();
        showAllVisits = false;
        init(conid);
    }
    
    private void init(String conid)
    {
        ShowAppointmentProposals = false;
        contHist = new SCContactHistory__c();
        isNewEntry = false;
        visitForContactHistory = null;
    
        // read the conract details    
        contract = [select id, name, util_templatedetails__c, Status__c, EnableVisitCreation__c, EnableOrderCreation__c, EnableAutoScheduling__c,
                    Template__r.EnableVisitCreation__c, Template__r.EnableOrderCreation__c, Template__r.EnableAutoScheduling__c, Internal__c,
                    Runtime__c, RuntimeMax__c, AutoExtend__c, StartDate__c, EndDate__c, Contact__c, 
                    MaintenanceFirstDate__c, MaintenanceLastDate__c, MaintenanceCreationShiftFactor__c, util_MaintenanceDuration__c, util_MaintenancePrice__c,
                    Account__c, Account__r.locktype__c, Account__r.riskclass__c
                    from SCContract__c where id =:conid];    

        // read the visits that are currently visible
        if(showAllVisits)
        {
            pendingVisits = [select id, name, DueDate__c, StartDate__c, EndDate__c, Status__c, AutoCreated__c, 
                             OrderCreationDate__c, Contract__c, InstalledBaseLocation__c, util_IsValidDate__c,
                             Order__c, Order__r.name, Order__r.type__c, Order__r.status__c, Order__r.createdDate
                             from SCContractVisit__c
                             where Contract__c = :conid 
                             order by DueDate__c asc ];
        }
        else
        {
            pendingVisits = [select id, name, DueDate__c, StartDate__c, EndDate__c, Status__c, AutoCreated__c, 
                             OrderCreationDate__c, Contract__c, InstalledBaseLocation__c, util_IsValidDate__c,
                             Order__c, Order__r.name, Order__r.type__c, Order__r.status__c, Order__r.createdDate
                             from SCContractVisit__c
                             where Contract__c = :conid 
                                 and status__c in('open', 'created', 'schedule', 'scheduled', 'skip')
                             order by DueDate__c asc ];
        }  
                              
        // read the contact history 
        contactHistory = [select id, name, Status__c, Reason__c, NextCall__c, LastCall__c, Information__c, 
                          LastModifiedDate, LastModifiedById, Order__c, Account__c
                         from SCContactHistory__c where Contract__c = :conid order by name desc];

        // now transfer the records into processing lists to be able to dynamically activate actions
        // the visiits
        visititems = new List<VisitItem>();
        visititemsmap = new Map<String, VisitItem>();
        for(SCContractVisit__c v : pendingVisits)
        {
            VisitItem item = new VisitItem(v); 
            visititems.add(item);    
            visititemsmap.put(item.obj.id, item); // for quick access
        }
        
        // the contact history 
        contactitems = new List<ContactItem>();
        contacthistorymap = new Map<String, ContactItem>();
        for(SCContactHistory__c v : contactHistory)
        {
            ContactItem item = new ContactItem(v); 
            contactitems.add(item);    
            contacthistorymap.put(item.obj.id, item);
        }
        
        // workaround to show the translaget picklists values and still being able to process the picklist values directly
        acc = new Account();
        // acc.id = contract.account__r.id;
        acc.locktype__c  = contract.Account__r.locktype__c;
        acc.riskclass__c = contract.Account__r.riskclass__c;
        
        validate();
    }

   /*
    * Checks if we can schedule this contract
    */
    private void validate()
    {
        canSchedule = false;
   /*
   SC_msg_ContractCreateOrder
   SC_msg_ContractCreateVisit
   SC_btn_SkipNextVisit
   */
        
        // 1. Check the template details
        if((contract.EnableVisitCreation__c == false || contract.Template__r.EnableVisitCreation__c == false) ||
           (contract.Status__c == null || !SCfwConstants.DOMVAL_CONTRACT_STATUS_SCHEDULEABLE.contains(contract.Status__c)))
        {
            canSchedule = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.SC_msg_ContractCreateVisit));
//                                                       'Contract based visits must be enabled on contract and contract template level - Please check the [Enable visit creation] flags'));
                                                       
                                                       
                                                       
        }
/*
        // the contract must have the status {created, active} to be able to 
        // the contract and the template must allow to create visits
        if(contract.Status__c == null || !SCfwConstants.DOMVAL_CONTRACT_STATUS_SCHEDULEABLE.contains(contract.Status__c))
        {
            canSchedule = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
                                                       'The contract status must be [created or active] to be able to create contract visits - Please activate the contract if required'));
        }
*/
        if(contract.MaintenanceFirstDate__c == null)
        {
            canSchedule = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.SC_msg_ContractFirstMaintenanceRequired)); 
        }

/*
        if(MaintenanceCreationShiftFactor__c != '0' && MaintenanceLastDate__c == null)
        {
            canSchedule = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
                                                       'The next maintenance is calculated based on the last maintenance '));
        }
*/        
        
    } // validate



    public Boolean getIsCustomerLocked()
    {
        /*
        public final static String DOMVAL_LOCKTYP_CASH = '50002';
        public final static String DOMVAL_LOCKTYP_LOCKED = '50003';
        DOMVAL_RISKCLASS_NONE = '51001';
        DOMVAL_RISKCLASS_REMINDERS = '51002';
        */
    
        return(contract != null && (
            contract.Account__r.locktype__c != null &&
           contract.Account__r.locktype__c != SCfwConstants.DOMVAL_LOCKTYP_NONE || 
           contract.Account__r.riskclass__c != null && 
           contract.Account__r.riskclass__c != SCfwConstants.DOMVAL_RISKCLASS_NONE));
    }

    
    public String getRunTimeDetails()
    {
        return '' + contract.Runtime__c + ' / ' + contract.RuntimeMax__c +  ' / ' + contract.AutoExtend__c;
    }
    

    public Boolean getCanDispatch()
    {
        if(contract.Status__c != null)
        {
            // the contract must have the status {created, active} to be able to 
            // the contract and the template must allow to create visits
            return SCfwConstants.DOMVAL_CONTRACT_STATUS_SCHEDULEABLE.contains(contract.Status__c) && 
                   contract.EnableVisitCreation__c == true && contract.Template__r.EnableVisitCreation__c == true;
        }
        return false;                     
    }
    
    



   /* Internal helper class to manage contract visits and enable / disable the actions depending on the status
    *   open      - the contract visit was created ( after creation of record)
    *   schedule  - after its order has been automatically created 
    *   scheduled - after its order has been automatically dispatched
    *   created   - after its order has been manually created
    *   cancelled - after cancellation
    *   skip      - the customer does not want a maintenance - skip the contract visit
    */                    
    public class VisitItem 
    {
        public SCContractVisit__c obj {get; set;}     
        public Boolean selected {get; set;}
        
        public VisitItem(SCContractVisit__c o)
        {
            obj = o;
            selected = true;
        }

       /*
        * If only the visit exist: a new order can be created 
        */
        public Boolean getCanCreateOrder()
        {
            return obj.status__c == 'open';
        }
        

       /*
        * An existing order can be scheduled if it still has the status [open] 
        */        
        public Boolean getCanScheduleOrder()
        {
            return (obj.status__c == 'created' || obj.status__c == 'schedule') && 
                    obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_OPEN;
        }

       /*
        * An existing contract visit can be cancelled if it is created or only scheduled 
        * An existing order can be cancelled if
        */        
        public Boolean getCanCancel()
        {
            return (obj.status__c == 'created' || obj.status__c == 'schedule' || obj.status__c == 'scheduled') ||
                   (obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_OPEN 
                    || obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED 
                    || obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK 
                    || obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING
                   );                                         
        }          
        
       /*
        * A visit can be skipped if not yet processed by an engineer. 
        */
        public Boolean getCanSkip()
        {
            return obj.status__c == 'open' ;
        }
        
        public Boolean getCanDelete()
        {
            return obj.status__c == 'open' || obj.status__c == 'skip' || obj.status__c == 'cancelled';
        }
    }    

 
   /*
    * Used to pass the visit item id from the page block table to the controller
    * @return the visit item object
    */    
    VisitItem getVisitFromParam()
    {
        String vid = ApexPages.CurrentPage().getParameters().get('vid');
        if(vid != null)
        {
            return visititemsmap.get(vid);
        }
        return null;
    }
    
   /*
    * Sets the status to skip of the selected visit item.
    * Skip means that the customer does not want an anual service this time.
    * The next visit is calculated automatically. 
    */    
    public PageReference onSkip()
    {
        VisitItem item = getVisitFromParam();
        if(item != null)
        {
            item.obj.Status__c = 'skip';
            update item.obj;

            // calculate the next visit        
            SCbtcContractVisitCreate.syncCreateContractVisits(contract.id);
            
            init(contract.id);
        }
        return null;
    }

   /*
    * Deletes the selected visit item 
    */    
    public PageReference onDelete()
    {
        VisitItem item = getVisitFromParam();
        if(item != null)
        {
            delete item.obj;
            init(contract.id);
        }
        return null;
    }

 
   /*
    * Cancelles the order and the appointments and sets the contract visit to [cancelled]
    */    
    public PageReference onCancelOrder()
    {
        VisitItem item = getVisitFromParam();
        if(item != null)
        {
            if(item.obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED 
               || item.obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK 
               || item.obj.order__r.status__c == SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING)
            {
                SCboOrder.cancelAssignments(item.obj.Order__c);
                item.obj.Status__c = 'schedule';
                update item.obj;
            
            }
            else
            {
                // 286001 Cancelled by Customer
                SCboOrder.cancelOrder(item.obj.Order__c, '286001', 'Cancelled during contract appointment processing');
                item.obj.Status__c = 'cancelled';
                update item.obj;
            }   
            init(contract.id);
        }
        return null;
    }


   /*
    * Creates a new order for the visit item
    */    
    public PageReference onCreateOrder()
    {
        VisitItem item = getVisitFromParam();
        if(item != null)
        {
            List<String> visitIdList = new List<String>();
            visitIdList.add(item.obj.id);
            SCbtcContractOrderCreate.syncCreateOrders(visitIdList);
     
            init(contract.id);
        }           
        return null;
    }


   /*
    * Calculates the next visit
    */    
    public PageReference onCreateNewVisit()
    {
        SCbtcContractVisitCreate.syncCreateContractVisits(contract.id);
        init(contract.id);
        return null;
    }

 
   /*
    * Refresh visit list if the show all checkbox has changed
    */    
    public PageReference onShowAllChanged()
    {
        init(contract.id);
        return null;
    }








//######## Contact History Extension

   /*
    * Called if the user clicks the [Info] link in the visit item
    */    
    public PageReference onNewContactHistory()
    {
        VisitItem item = getVisitFromParam();
        if(item != null)
        {
            contHist = new SCContactHistory__c(); 
            contHist.Contract__c = contract.id;
            contHist.Account__c  = contract.account__c;
            contHist.Order__c    = item.obj.order__c;
            contHist.LastCall__c = DateTime.now();
            // now open the edit dialog        
            isNewEntry = true;
        }
        visitForContactHistory = item;
        return null;
    }
    


    public Boolean isNewEntry { get; set; }
    public VisitItem visitForContactHistory;

   /*
    * 
    */    
    public PageReference onContactNew()
    {
        contHist = new SCContactHistory__c(); 
        contHist.Contract__c = contract.id;
        contHist.Account__c  = contract.account__c;
        contHist.Order__c    = null;
        contHist.LastCall__c = DateTime.now();
    
        isNewEntry = true;
        visitForContactHistory = null;
        
        return null;
    }

   /*
    * 
    */    
    public PageReference onContactSave()
    {
        insert contHist;
        
        // If the customer has no need for maintenance or the maintenance is 
        // done by the installer or the customer has another product
        if (contHist.Status__c == 'NO_NEED' ||
            contHist.Status__c == 'MAINT_BY_INST' ||
            contHist.Status__c == 'NO_VAI_UNIT')
        {
            contract.EnableVisitCreation__c = false;
            contract.EnableOrderCreation__c = false;
            contract.EnableAutoScheduling__c = false;
        
            // update the contract
            SCContract__c con = new SCContract__c(Id = contHist.Contract__c);
            con.EnableVisitCreation__c = false;
            con.EnableOrderCreation__c = false;
            con.EnableAutoScheduling__c = false;
            
            update con;
            
            if(visitForContactHistory != null)
            {
                // cancel the pending order (if any exists)
                if(visitForContactHistory.obj.Order__c != null &&
                  (visitForContactHistory.obj.Order__r.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_OPEN ||
                   visitForContactHistory.obj.Order__r.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED ||
                   visitForContactHistory.obj.Order__r.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK ||
                   visitForContactHistory.obj.Order__r.Status__c == SCfwConstants.DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING))
                {                
                    SCboOrder.cancelOrder(visitForContactHistory.obj.Order__c, null, null);
                }
                // and cancel the contract visit
                visitForContactHistory.obj.Status__c = 'cancelled';
                update visitForContactHistory.obj;
            }
        }
        else if (contHist.Status__c == 'DONE' && 
                 visitForContactHistory != null && 
                 visitForContactHistory.obj.Order__c != null)
        {
            SCOrder__c order = new SCOrder__c(Id = visitForContactHistory.obj.Order__c, 
                                              FollowUp1__c = null, 
                                              FollowUp2__c = null);
            update order;
        }
        
        isNewEntry = false;
        visitForContactHistory = null;
        init(contract.id);
        return null;
    }

   /*
    * 
    */    
    public PageReference onContactCancel()
    {
        isNewEntry = false;
        init(contract.id);
        return null;
    }

   /*
    * Deletes the selected visit item 
    */    
    public PageReference onDeleteContactHistory()
    {
        ContactItem item = getContactFromParam();
        if(item != null)
        {
            delete item.obj;
            init(contract.id);
        }
        return null;
    }


   /* Internal helper class used to manage the contact history
    *    OPEN
    *    NO_NEED
    *    MAINT_BY_INST
    *    INV_NO
    *    DONE
    *    NO_VAI_UNIT 
    *    FOLLOW_UP
    */        
    public class ContactItem 
    {
        public SCContactHistory__c obj {get; set;}     
        public Boolean selected {get; set;}
        
        public ContactItem(SCContactHistory__c o)
        {
            obj = o;
            selected = true;
        }
        
        public Boolean getCanDelete()
        {
            return true; //obj.Status__c == 'OPEN' || obj.Status__c == 'FOLLOW_UP';
        }
    }

   /*
    * Used to pass the visit item id from the page block table to the controller
    * @return the visit item object
    */    
    ContactItem getContactFromParam()
    {
        String chid = ApexPages.CurrentPage().getParameters().get('chid');
        if(chid != null)
        {
            return contacthistorymap.get(chid);
        }
        return null;
    }
    
    
    
    
    //@author GMSS
    public void methodForCodeCoverage(SCContractExtension obj, SCContract__c contract)
    {
        if (Test.isRunningTest())
        {
            obj.showAllVisits = true;
            try { obj.init(contract.Id); } catch(Exception e) {}
            try { obj.validate(); } catch(Exception e) {}
            try { obj.getVisitFromParam(); } catch(Exception e) {}
            try { obj.getContactFromParam(); } catch(Exception e) {}
        }
    }
}