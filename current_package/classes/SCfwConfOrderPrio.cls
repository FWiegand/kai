/*
 * @(#)SCfwConfOrderPrio.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCfwConfOrderPrio
{
    protected List<SCConfOrderPrio__c> confOrderPrios;
    protected String userCountry;
    
    public Boolean isDebug { get; set;}
    public List<ConfOrderPrioObj> partnerObjList { get; set;}
    public List<ConfOrderPrioObj> situationObjList { get; set;}
    public List<ConfOrderPrioObj> failureTypeObjList { get; set;}
    private List<ConfigObj> confObjList;
    
    private Decimal koPartner;
    private Decimal koSituation;
    private Decimal koFailureType;
    private Decimal normalScPartner;
    private Decimal normalScSituation;
    private Decimal normalScFailureType;
    private Decimal normalFaPartner;
    private Decimal normalFaSituation;
    private Decimal normalFaFailureType;
    private Decimal totalPartner;
    private Decimal totalSituation;
    private Decimal totalFailureType;
    public Decimal prioValue;


    /**
     * Class which contains one object for displaying 
     * on the dialog page.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public class ConfOrderPrioObj
    {
        public String fieldType;
        public String selectType;
        public Integer score;
        public Double factor;
        public Boolean koCriteria;
        public String preSelObj;
        public String preSelField;
        public String preSelValue;

        // field text
        public String field { get; set;}
        // value for a single select entry (list of radio buttons), 
        // used in dialog page
        public String optValue { get; set;}
        // value for a multiple select entry (checkbox), 
        // used in dialog page
        public Boolean chkValue { get; set;}
        // list with selectable items for a single select entry, 
        // used in dialog page
        public List<SelectOption> optList { get; set;}
        // map for mapping dummy values to real values
        // inside a select option list
        public Map<String, SelectOptObj> valueMap;
    
        /**
         * Default constructor
         *
         * @author Alexander Wagner <awagner@gms-online.de>
         * @version $Revision$, $Date$
         */
        public ConfOrderPrioObj()
        {
            field = '';
            optValue = '';
            chkValue = false;
            optList = new List<SelectOption>();
            valueMap = new Map<String, SelectOptObj>();
        } // ConfOrderPrioObj
                
        public Boolean getIsTitle()
        {
            return ((null != fieldType) && ('Title' == fieldType));
        } // getIsTitle
        public Boolean getIsSubTitle()
        {
            return ((null != fieldType) && ('SubTitle' == fieldType));
        } // getIsSubTitle
        public Boolean getIsSeparator()
        {
            return ((null != fieldType) && ('Separator' == fieldType));
        } // getIsSeparator
        public Boolean getIsSingleSelect()
        {
            return ((null != fieldType) && ('Item' == fieldType) && 
                    (null != selectType) && ('Single select' == selectType));
        } // getIsSingleSelect
        public Boolean getIsMultipleSelect()
        {
            return ((null != fieldType) && ('Item' == fieldType) && 
                    (null != selectType) && ('Multiple select' == selectType));
        } // getIsMultipleSelect
                
        /**
         * Checks if the given value is available for this object, 
         * if yes, the selection for this object is set.
         *
         * @param    selOptObj    a mapping object
         * @param    value        the value to be checked
         *
         * @author Alexander Wagner <awagner@gms-online.de>
         * @version $Revision$, $Date$
         */
        public void checkValue(SelectOptObj selOptObj, String value)
        {
            // if we have a single selection (= list of radio buttons)
            if (null != selOptObj)
            {
                if ((null != selOptObj.preSelValue) && 
                    selOptObj.preSelValue.equals(value))
                {
                    optValue = selOptObj.dummyValue;
                } // if ((null != selOptObj.preSelValue) && ...
            } // if (null != selOptObj)
            // if we have a multiple selection (= checkbox), 
            // check the score/factor 
            else
            {
                chkValue = value.equals(preSelValue);
            } // else [if (optList.size() > 0)]
        } // checkValue
    } // class ConfOrderPrioObj

    /**
     * Class which contains data for mapping dummy values of
     * a select option list to the real values.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public class SelectOptObj
    {
        public String dummyValue;
        public String realValue;
        public Integer score;
        public Double factor;
        public Boolean koCriteria;
        public String preSelObj;
        public String preSelField;
        public String preSelValue;
    } // SelectOptObj

    /**
     * Class which contains data for the configuration.
     * Will be used, when the prio value has been calculated
     * and will be transformed to the order data (prio
     * and desired start date).
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public class ConfigObj
    {
        public String orderPrio;
        public Decimal prioValue;
        public Decimal offset;
    } // ConfigObj
    
    
    /**
     * Default constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCfwConfOrderPrio()
    {
        System.debug('#### SCfwConfOrderPrio()');
        userCountry = SCBase.getUserCountry(true);
        init();
    } // SCfwConfOrderPrio
    
    /**
     * Constructor for special country.
     *
     * @param    country        specific country
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCfwConfOrderPrio(String country)
    {
        System.debug('#### SCfwConfOrderPrio(): country -> ' + country);
        userCountry = country;
        init();
    } // SCfwConfOrderPrio
    
    /**
     * Initialize the intern list with all possible conf order prios.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void init()
    {
        System.debug('#### SCfwConfOrderPrio.init()');
        isDebug = SCApplicationSettings__c.getInstance().ORDERCREATION_PRIO_DETERMINATION_DEBUG__c;
        
        koPartner = 0;
        koSituation = 0;
        koFailureType = 0;
        normalScPartner = 0;
        normalScSituation = 0;
        normalSCFailureType = 0;
        normalFaPartner = 0;
        normalFaSituation = 0;
        normalFaFailureType = 0;
        totalPartner = 0;
        totalSituation = 0;
        totalFailureType = 0;
        prioValue = 0;

        confOrderPrios = [Select Id, Name, ID2__c, 
                                 Category__c, Type__c, SelectType__c, 
                                 Field__c, Score__c, Factor__c, KoCriteria__c, 
                                 PreSelObj__c, PreSelField__c, PreSelValue__c, 
                                 PrioValue__c, OrderPrio__c, Offset__c 
                            from SCConfOrderPrio__c 
                           where Country__c = :userCountry 
                        order by Category__c, SortOrder__c];
        System.debug('#### SCfwConfOrderPrio.init(): number conf order prios -> ' + confOrderPrios.size());
    } // init

    /**
     * Generates the three lists for each column in the dialog.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void generateLists()
    {
        System.debug('#### SCfwConfOrderPrio.generateLists()');
        partnerObjList = new List<ConfOrderPrioObj>();
        situationObjList = new List<ConfOrderPrioObj>();
        failureTypeObjList = new List<ConfOrderPrioObj>();
        confObjList = new List<ConfigObj>();
        
        Integer dummyValue = 1;
        String lastFieldType = '';
        String lastSelectType = '';
        ConfOrderPrioObj ordPrioObj;
        ConfigObj confObj;
        for (SCConfOrderPrio__c orderPrio :confOrderPrios)
        {
            // if we have a config object fill the corresponding list
            if ('Config' == orderPrio.Category__c)
            {
                confObj = new ConfigObj();
                confObj.prioValue = orderPrio.PrioValue__c;
                confObj.orderPrio = orderPrio.OrderPrio__c;
                confObj.offset = orderPrio.Offset__c;
                
                confObjList.add(confObj);
            } // if ('Config' == orderPrio.Category__c)
            // else create a new object and fill it
            else if (('Title' == orderPrio.Type__c) || 
                ('SubTitle' == orderPrio.Type__c) || 
                ('Separator' == orderPrio.Type__c) || 
                (('Item' == orderPrio.Type__c) && 
                 (!lastFieldType.equals('Item') || 
                  (lastFieldType.equals('Item') && 
                   (!lastSelectType.equals(orderPrio.SelectType__c) || 
                    ('Multiple select' == orderPrio.SelectType__c))))))
            {
                dummyValue = 1;
                
                ordPrioObj = new ConfOrderPrioObj();
                ordPrioObj.fieldType = orderPrio.Type__c;
                ordPrioObj.selectType = orderPrio.SelectType__c;
                ordPrioObj.score = SCBase.isSet(orderPrio.Score__c) ? Integer.valueOf(orderPrio.Score__c) : null;
                ordPrioObj.factor = orderPrio.Factor__c;
                ordPrioObj.koCriteria = orderPrio.KoCriteria__c;
                ordPrioObj.preSelObj = orderPrio.PreSelObj__c;
                ordPrioObj.preSelField = orderPrio.PreSelField__c;
                ordPrioObj.preSelValue = orderPrio.PreSelValue__c;
                ordPrioObj.field = orderPrio.Field__c;
                if (ordPrioObj.koCriteria && ('Multiple select' == orderPrio.SelectType__c))
                {
                    ordPrioObj.field += ' * ';
                } // if (ordPrioObj.koCriteria && ('Multiple select' == orderPrio.SelectType__c))
                if (isDebug && ('Multiple select' == orderPrio.SelectType__c))
                {
                    ordPrioObj.field += ' (' + 
                                        (SCBase.isSet(orderPrio.Score__c) ? 
                                                orderPrio.Score__c : String.valueOf(orderPrio.Factor__c)) + 
                                        ')';
                } // if (isDebug && ('Multiple select' == orderPrio.SelectType__c))
                
                if (('Item' == orderPrio.Type__c) && 
                    ('Single select' == orderPrio.SelectType__c))
                {
                    SelectOptObj selOptObj = new SelectOptObj();
                    selOptObj.realValue = SCBase.isSet(orderPrio.Score__c) ? orderPrio.Score__c : 
                                                        String.valueOf(orderPrio.Factor__c);
                    selOptObj.dummyValue = String.valueOf(dummyValue);
                    selOptObj.score = SCBase.isSet(orderPrio.Score__c) ? Integer.valueOf(orderPrio.Score__c) : null;
                    selOptObj.factor = orderPrio.Factor__c;
                    selOptObj.koCriteria = orderPrio.KoCriteria__c;
                    selOptObj.preSelObj = orderPrio.PreSelObj__c;
                    selOptObj.preSelField = orderPrio.PreSelField__c;
                    selOptObj.preSelValue = orderPrio.PreSelValue__c;
                    
                    // because the original values (scores or factors) of a
                    // single select list can be identically, we need a 
                    // mapping from self generate dummy values to the real one
                    ordPrioObj.valueMap.put(selOptObj.dummyValue, selOptObj);
                    String label = orderPrio.Field__c;
                    if (selOptObj.koCriteria)
                    {
                        label += ' * ';
                    } // if (selOptObj.koCriteria)
                    if (isDebug)
                    {
                        label += ' (' + selOptObj.realValue + ')';
                    } // if (isDebug)
                    ordPrioObj.optList.add(new SelectOption(selOptObj.dummyValue, label));
                } // if (('Item' == orderPrio.Type__c) && ...
                
                lastFieldType = orderPrio.Type__c;
                lastSelectType = orderPrio.SelectType__c;
                
                // add the object to the right list
                if ('Partner' == orderPrio.Category__c)
                {
                    partnerObjList.add(ordPrioObj);
                } // if ('Partner' == orderPrio.Category__c)
                else if ('Situation' == orderPrio.Category__c)
                {
                    situationObjList.add(ordPrioObj);
                } // else if ('Situation' == orderPrio.Category__c)
                else if ('FailureType' == orderPrio.Category__c)
                {
                    failureTypeObjList.add(ordPrioObj);
                } // else if ('FailureType' == orderPrio.Category__c)
            } // else if (('Title' == orderPrio.Type__c) || ...
            else
            {
                if ('Single select' == orderPrio.SelectType__c)
                {
                    dummyValue++;
                    
                    SelectOptObj selOptObj = new SelectOptObj();
                    selOptObj.realValue = SCBase.isSet(orderPrio.Score__c) ? orderPrio.Score__c : 
                                                        String.valueOf(orderPrio.Factor__c);
                    selOptObj.dummyValue = String.valueOf(dummyValue);
                    selOptObj.score = SCBase.isSet(orderPrio.Score__c) ? Integer.valueOf(orderPrio.Score__c) : null;
                    selOptObj.factor = orderPrio.Factor__c;
                    selOptObj.koCriteria = orderPrio.KoCriteria__c;
                    selOptObj.preSelObj = orderPrio.PreSelObj__c;
                    selOptObj.preSelField = orderPrio.PreSelField__c;
                    selOptObj.preSelValue = orderPrio.PreSelValue__c;
                    
                    ordPrioObj.valueMap.put(selOptObj.dummyValue, selOptObj);
                    String label = orderPrio.Field__c;
                    if (selOptObj.koCriteria)
                    {
                        label += ' * ';
                    } // if (selOptObj.koCriteria)
                    if (isDebug)
                    {
                        label += ' (' + selOptObj.realValue + ')';
                    } // if (isDebug)
                    ordPrioObj.optList.add(new SelectOption(selOptObj.dummyValue, label));
                } // if ('Single select' == orderPrio.SelectType__c)
            } // else [if (('Title' == orderPrio.Type__c) || ...]
        } // for (SCConfOrderPrio__c orderPrio :confOrderPrios)
    } // generateLists
    
    /**
     * Checks in all three lists if there are preselection infos and if yes
     * it will check these and set set value if necessary.
     *
     * @param    boOrder    business object used for looking for
     *                      preselection values
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void checkPreselection(SCboOrder boOrder)
    {
        System.debug('#### SCfwConfOrderPrio.checkPreselection()');
        
        List<List<ConfOrderPrioObj>> orderPrioObjLists = new List<List<ConfOrderPrioObj>>();
        orderPrioObjLists.add(partnerObjList);
        orderPrioObjLists.add(situationObjList);
        orderPrioObjLists.add(failureTypeObjList);
        
        for (List<ConfOrderPrioObj> ordPrioObjList :orderPrioObjLists)
        {
            for (ConfOrderPrioObj orderPrioObj :ordPrioObjList)
            {
                // if we have a single selection (= list of radio buttons), 
                // iterate over the map with the entries, which contains 
                // the preselection infos
                if (orderPrioObj.optList.size() > 0)
                {
                    for (SelectOptObj selOptObj :orderPrioObj.valueMap.values())
                    {
                        if (SCBase.isSet(selOptObj.preSelObj) && 
                            SCBase.isSet(selOptObj.preSelField) && 
                            SCBase.isSet(selOptObj.preSelValue))
                        {
                            setPreselection(orderPrioObj, selOptObj, boOrder);
                        } // if (SCBase.isSet(orderPrioObj.preSelObj) && 
                    } // for (SelectOptObj selOptObj :orderPrioObj.valueMap.values())
                } // if (orderPrioObj.optList.size() > 0)
                // if we have a multiple selection (= checkbox), 
                // the preselection infos are direct in the object
                else
                {
                    if (SCBase.isSet(orderPrioObj.preSelObj) && 
                        SCBase.isSet(orderPrioObj.preSelField) && 
                        SCBase.isSet(orderPrioObj.preSelValue))
                    {
                        setPreselection(orderPrioObj, null, boOrder);
                    } // if (SCBase.isSet(orderPrioObj.preSelObj) && 
                } // else [if (orderPrioObj.optList.size() > 0)]
            } // for (ConfOrderPrioObj orderPrioObj :ordPrioObjList)
        } // for (List<ConfOrderPrioObj> ordPrioObjList :orderPrioObjLists)
    } // checkPreselection
    
    /**
     * Checks the preselection value with the value in the order
     * and set set the value if necessary.
     *
     * @param    orderPrioObj    prio object with preselection infos
     * @param    selOptObj       a mapping object
     * @param    boOrder         business object used for looking for
     *                           preselection values
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private void setPreselection(ConfOrderPrioObj orderPrioObj, 
                                 SelectOptObj selOptObj, 
                                 SCboOrder boOrder)
    {
        System.debug('#### SCfwConfOrderPrio.setPreselection(): orderPrioObj -> ' + orderPrioObj);
        System.debug('#### SCfwConfOrderPrio.setPreselection(): selOptObj    -> ' + selOptObj);
        
        String value;
        String preSelObj = (null != selOptObj) ? selOptObj.preSelObj : orderPrioObj.preSelObj;
        String preSelField = (null != selOptObj) ? selOptObj.preSelField : orderPrioObj.preSelField;
        if ('RoleSR' == preSelObj)
        {
            SCOrderRole__c roleSR = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_LE);

            if ('PartnerType' == preSelField)
            {
                value = roleSR.Type__c;
                orderPrioObj.checkValue(selOptObj, value);
            } // if ('PartnerType' == preSelField)
            else
            {
                System.debug('#### SCfwConfOrderPrio.setPreselection(): preSelField ' + 
                                preSelObj + '.' + preSelField + ' unknown!!!');
            } // else [if ('PartnerType' == preSelField)]
        } // if ('RoleSR' == preSelObj)
        else if ('RoleIR' == preSelObj)
        {
            SCOrderRole__c roleIR = boOrder.getRole(SCfwConstants.DOMVAL_ORDERROLE_RE);

            if ('PartnerType' == preSelField)
            {
                value = roleIR.Type__c;
                orderPrioObj.checkValue(selOptObj, value);
            } // if ('PartnerType' == preSelField)
            else
            {
                System.debug('#### SCfwConfOrderPrio.setPreselection(): preSelField ' + 
                                preSelObj + '.' + preSelField + ' unknown!!!');
            } // else [if ('PartnerType' == preSelField)]
        } // else if ('RoleIR' == preSelObj)
        else if ('Order' == preSelObj)
        {
            if ('FailureType' == preSelField)
            {
                value = boOrder.order.FailureType__c;
                orderPrioObj.checkValue(selOptObj, value);
            } // if ('FailureType' == preSelField)
            else
            {
                System.debug('#### SCfwConfOrderPrio.setPreselection(): preSelField ' + 
                                preSelObj + '.' + preSelField + ' unknown!!!');
            } // else [if ('PartnerType' == preSelField)]
        } // else if ('Order' == preSelObj)
        else if ('OrderItem' == preSelObj)
        {
            if (boOrder.boOrderItems.size() > 0)
            {
                if ('ErrSym1' == preSelField)
                {
                    value = boOrder.boOrderItems.get(0).orderItem.ErrorSymptom1__c;
                    orderPrioObj.checkValue(selOptObj, value);
                } // if ('ErrSym1' == preSelField)
                else if ('ErrCond' == preSelField)
                {
                    value = boOrder.boOrderItems.get(0).orderItem.ErrorCondition1__c;
                    orderPrioObj.checkValue(selOptObj, value);
                } // if ('ErrCond' == preSelField)
                else
                {
                    System.debug('#### SCfwConfOrderPrio.setPreselection(): preSelField ' + 
                                    preSelObj + '.' + preSelField + ' unknown!!!');
                } // else [if ('PartnerType' == preSelField)]
            } // if (boOrder.boOrderItems.size() > 0)
        } // else if ('OrderItem' == preSelObj)
        else
        {
            System.debug('#### SCfwConfOrderPrio.setPreselection(): preSelObj ' + 
                            preSelObj + ' unknown!!!');
        }
    } // setPreselection
    
    /**
     * Calculates the total score depending on the values set in the three lists.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void calculateScore()
    {
        System.debug('#### SCfwConfOrderPrio.calculateScore()');
        
        Decimal koPart = getMax(partnerObjList, true, true);
        Decimal koSit = getMax(situationObjList, true, true);
        Decimal koFail = getMax(failureTypeObjList, true, true);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koPart -> ' + koPart);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koSit  -> ' + koSit);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koFail -> ' + koFail);
        
        normalScPartner = getMax(partnerObjList, true, false);
        normalScSituation = getMax(situationObjList, true, false);
        normalScFailureType = getMax(failureTypeObjList, true, false);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalScPartner     -> ' + normalScPartner);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalScSituation   -> ' + normalScSituation);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalScFailureType -> ' + normalScFailureType);
        
        normalFaPartner = getMax(partnerObjList, false, false);
        normalFaSituation = getMax(situationObjList, false, false);
        normalFaFailureType = getMax(failureTypeObjList, false, false);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalFaPartner     -> ' + normalFaPartner);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalFaSituation   -> ' + normalFaSituation);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): normalFaFailureType -> ' + normalFaFailureType);
        
        Decimal tmp = Math.max(koSit, koFail);
        koPartner = (tmp > 0) ? tmp : normalScPartner;
        tmp = Math.max(koPart, koFail);
        koSituation = (tmp > 0) ? tmp : normalScSituation;
        tmp = Math.max(koPart, koSit);
        koFailureType = (tmp > 0) ? tmp : normalScFailureType;
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koPartner     -> ' + koPartner);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koSituation   -> ' + koSituation);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): koFailureType -> ' + koFailureType);
        
        tmp = normalScPartner * normalFaPartner;
        totalPartner = (tmp > 10) ? 10 : Math.max(tmp, koPartner);
        tmp = normalScSituation * normalFaSituation;
        totalSituation = (tmp > 10) ? 10 : Math.max(tmp, koSituation);
        tmp = normalScFailureType * normalFaFailureType;
        totalFailureType = (tmp > 10) ? 10 : Math.max(tmp, koFailureType);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): totalPartner     -> ' + totalPartner);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): totalSituation   -> ' + totalSituation);
        System.debug('#### SCfwConfOrderPrio.calculateScore(): totalFailureType -> ' + totalFailureType);

        prioValue = totalPartner * totalSituation * totalFailureType;
        System.debug('#### SCfwConfOrderPrio.calculateScore(): prioValue -> ' + prioValue);
    } // calculateScore
    
    /**
     * Gets the maximum value of all a prio objetct list selected entries.
     *
     * @param    ordPrioObjList    the list, that sould be investigate
     * @param    score             if true investige the scores
     *                             if false investige the factors
     * @param    koCriteria        only investige the entries where 
     *                             the flag for ko criteria is set
     *
     * @return    the maximium value
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    private Decimal getMax(List<ConfOrderPrioObj> ordPrioObjList, Boolean score, Boolean koCriteria)
    {
        System.debug('#### SCfwConfOrderPrio.getMax(): ordPrioObjList -> ' + ordPrioObjList);
        System.debug('#### SCfwConfOrderPrio.getMax(): score -> ' + score);
        System.debug('#### SCfwConfOrderPrio.getMax(): koCriteria -> ' + koCriteria);
        
        Decimal maxValue = score ? 0 : 1;
        Decimal curValue = 0;
        for (ConfOrderPrioObj orderPrioObj :ordPrioObjList)
        {
            // only investigate entries with the type item
            if ('Item' == orderPrioObj.fieldType)
            {
                System.debug('#### SCfwConfOrderPrio.getMax(): orderPrioObj -> ' + orderPrioObj);
                // if we have a single selection (= list of radio buttons), 
                // iterate over the map with the entries
                if (orderPrioObj.optList.size() > 0)
                {
                    if (SCBase.isSet(orderPrioObj.optValue))
                    {
                        System.debug('#### SCfwConfOrderPrio.getMax(): optValue -> ' + orderPrioObj.optValue);
                        SelectOptObj selOptObj = orderPrioObj.valueMap.get(orderPrioObj.optValue);
                        if (null != selOptObj)
                        {
                            System.debug('#### SCfwConfOrderPrio.getMax(): selOptObj -> ' + selOptObj);
                            if (koCriteria && selOptObj.koCriteria)
                            {
                                maxValue = Math.max(maxValue, selOptObj.score);
                            } // if (koCriteria && selOptObj.koCriteria)
                            else if (!koCriteria)
                            {
                                curValue = score ? selOptObj.score : selOptObj.factor;
                                if (null != curValue)
                                {
                                    if (!score)
                                    {
                                        maxValue *= curValue;
                                    } // if (!score)
                                    else
                                    {
                                        maxValue = Math.max(maxValue, curValue);
                                    } // else [if (!score)]
                                } // if (null != curValue)
                            } // else if (!koCriteria)
                        } // if (null != selOptObj)
                    } // if (SCBase.isSet(orderPrioObj.optValue))
                } // if (orderPrioObj.optList.size() > 0)
                // if we have a multiple selection (= checkbox), 
                // look in the object itself
                else
                {
                    if (orderPrioObj.chkValue)
                    {
                        if (koCriteria && orderPrioObj.koCriteria)
                        {
                            maxValue = Math.max(maxValue, orderPrioObj.score);
                        } // if (koCriteria && orderPrioObj.koCriteria)
                        else if (!koCriteria)
                        {
                            curValue = score ? orderPrioObj.score : orderPrioObj.factor;
                            if (null != curValue)
                            {
                                if (!score)
                                {
                                    maxValue *= curValue;
                                } // if (!score)
                                else
                                {
                                    maxValue = Math.max(maxValue, curValue);
                                } // else [if (!score)]
                            } // if (null != curValue)
                        } // else if (!koCriteria)
                    } // if (orderPrioObj.chkValue)
                } // else [if (orderPrioObj.optList.size() > 0)]
            } // if ('Item' == orderPrioObj.fieldType)
        } // for (ConfOrderPrioObj orderPrioObj :ordPrioObjList)
        System.debug('#### SCfwConfOrderPrio.getMax(): maxValue -> ' + maxValue);
        return maxValue;
    } // getMax
    
    /**
     * Sets the order priority, the desired start date and the desired 
     * end date in the order.
     *
     * @param    boOrder    business object in which the data should be set
     * @param    interval   the interval between the desire start date and the
     *                      desired end date
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void setOrderData(SCboOrder boOrder, Integer interval)
    {
        System.debug('#### SCfwConfOrderPrio.setOrderData(): prioValue -> ' + prioValue);
        
        // Date today = Date.today();
        for (ConfigObj confObj :confObjList)
        {
            if (prioValue >= confObj.prioValue)
            {
                boOrder.order.CustomerPriority__c = confObj.orderPrio;
                // no longer wished
                // boOrder.order.CustomerPrefStart__c = today.addDays(confObj.offset.intValue());
                // boOrder.order.CustomerPrefEnd__c = boOrder.order.CustomerPrefStart__c.addDays(interval);
            } // if (prioValue >= confObj.prioValue)
        } // for (ConfigObj confObj :confObjList)
    } // setOrderData
    
    /**
     * Getter for displaying calculated values inside the dialog page on debug mode.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getKoPartner()
    {
        return String.valueOf(koPartner);
    } // getKoPartner
    public String getKoSituation()
    {
        return String.valueOf(koSituation);
    } // getKoSituation
    public String getKoFailureType()
    {
        return String.valueOf(koFailureType);
    } // getKoFailureType
    public String getNormalPartner()
    {
        return String.valueOf(normalScPartner * normalFaPartner);
    } // getNormalPartner
    public String getNormalSituation()
    {
        return String.valueOf(normalScSituation * normalFaSituation);
    } // getNormalSituation
    public String getNormalFailureType()
    {
        return String.valueOf(normalScFailureType * normalFaFailureType);
    } // getNormalFailureType
    public String getTotalPartner()
    {
        return String.valueOf(totalPartner);
    } // getTotalPartner
    public String getTotalSituation()
    {
        return String.valueOf(totalSituation);
    } // getTotalSituation
    public String getTotalFailureType()
    {
        return String.valueOf(totalFailureType);
    } // getTotalFailureType
    public String getPrioValue()
    {
        return String.valueOf(prioValue);
    } // getPrioValue
} // SCfwConfOrderPrio