/**
 * AseUnitTestAseService.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /*
  * Testclass
  */
@isTest
private class AseUnitTestAseService {

    @isTest
    public static void testCreateAllClasses()
    {
        //create all classes even if they were not used, but we need the 75%
        AseService.getAllResponse getAllResp = new AseService.getAllResponse(); 
        AseService.removeAllResponse_element removeAllResponse_el = new AseService.removeAllResponse_element(); 
        AseService.aseFaultType aseFaultT = new AseService.aseFaultType(); 
        AseService.setResponse_element setResponse_elem = new AseService.setResponse_element(); 
        AseService.aseKeyValueType aseKeyValueT = new AseService.aseKeyValueType();     
        AseService.aseParaSelectType aseParaSelectT = new AseService.aseParaSelectType(); 
        AseService.aseDataType aseDataT = new AseService.aseDataType(); 
        AseService.aseParaType aseParaT = new AseService.aseParaType(); 
        AseService.aseDataEntry aseDataEnt = new AseService.aseDataEntry(); 
        AseService.getAllResponseType getAllResponseT = new AseService.getAllResponseType(); 
        AseService.aseDataComplexType aseDataComplexT = new AseService.aseDataComplexType();
        AseService.aseAllType aseAllT = new AseService.aseAllType();
        AseService.removeResponse_element removeResponse_elem = new AseService.removeResponse_element(); 

        AseCalloutSCAppointment aseapp = new AseCalloutSCAppointment();
        aseapp.getASEType();
    }
}