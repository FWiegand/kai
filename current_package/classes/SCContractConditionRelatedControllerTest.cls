/*
 * @(#)SCContractConditionRelatedControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the controler functions for contract conditions related list.
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCContractConditionRelatedControllerTest
{

    private static SCContractConditionRelatedController ccr;
    
    
    static testMethod void contractConditionRelatedControllerPositiv1() 
    {
    
        PageReference pageRef = Page.SCContractConditionRelated;
        Test.setCurrentPageReference(pageRef);
        
        
        
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        SCHelperTestClass2.createContract('ContractFrame', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(SCHelperTestClass2.contracts.get(0));
        
        SCHelperTestClass2.contracts.get(0).Frame__c = SCHelperTestClass2.contracts.get(1).id;
        
        //update SCHelperTestClass2.contracts.get(0);
        
        ccr = new SCContractConditionRelatedController(sc);
        
        SCCondition__c condition = new SCCondition__c();

        condition.Contract__c = SCHelperTestClass2.contracts.get(0).id;
        condition.Account__c  = SCHelperTestClass.account.Id;
        condition.ConditionValue__c = 25;
        
        insert condition;

        System.debug('#### ccr.myContract' + ccr.myContract);
        System.debug('#### ccr.myFrameId' + ccr.myFrameId);
        System.debug('#### ccr.myTmplId' + ccr.myTmplId);


        ccr.getItemsCondition();
        
        ccr.SelectedConditionId = condition.id;
        
        ccr.deleteCondition();
        
    }

}