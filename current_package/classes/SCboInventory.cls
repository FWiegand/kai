/*
 * @(#)SCboInventory.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */

public class SCboInventory 
{
    public static List<ID> createInventoryForPlant(String plantName,
                                        String description,
                                        String fiscalYear,
                                        Boolean full,
                                        Date plannedCountDate)
    {
        List<ID> retValue = new List<ID>();
        System.debug('###plantName: ' + plantName);
        ID plantId = null;
        List<SCPlant__c> plantList = [select Id from SCPlant__c where Name =: plantName];
        if(plantList.size() > 0)
        {
            plantId = plantList[0].Id;
            List<SCStock__c> stockList = [Select id from SCStock__c where Plant__c = :plantId];
            for(SCStock__c s: stockList)
            {
                ID invId = createInventory(s.Id, description, fiscalYear, full, plannedCountDate);
                if(invId != null)
                {
                    retValue.add(invId);
                }   
            }
        }
        else
        {
            throw new SCfwException('There is no plant with name = ' + plantName);
        }
        return retValue;
    }

    public static List<ID> createInventory(List<String> stockIdList,
                                        String description,
                                        String fiscalYear,
                                        Boolean full,
                                        Date plannedCountDate)
    {
        List<ID> retValue = new List<ID>();
        List<SCStock__c> stockList = [Select id from SCStock__c where id = :stockIdList];
        for(SCStock__c s: stockList)
        {
            ID invId = createInventory(s.Id, description, fiscalYear, full, plannedCountDate);
            if(invId != null)
            {
                retValue.add(invId);
            }   
        }
        return retValue;
    }

    public static List<ID> createInventory(List<ID> stockIdList,
                                        String description,
                                        String fiscalYear,
                                        Boolean full,
                                        Date plannedCountDate)
    {
        List<ID> retValue = new List<ID>();
        List<SCStock__c> stockList = [Select id from SCStock__c where id = :stockIdList];
        for(SCStock__c s: stockList)
        {
            ID invId = createInventory(s.Id, description, fiscalYear, full, plannedCountDate);
            if(invId != null)
            {
                retValue.add(invId);
            }   
        }
        return retValue;
    }

    public static ID createInventoryForStockWithName(String stockName,
                                        String description,
                                        String fiscalYear,
                                        Boolean full,
                                        Date plannedCountDate)
    {
        ID retValue = null;
        List<SCStock__c> stockList = [Select id from SCStock__c where name = :stockName];
        if(stockList.size() > 0)
        {
                retValue = createInventory(stockList[0].Id, description, fiscalYear, full, plannedCountDate);
        }
        else
        {
            throw new SCfwException('There is no stock with name = ' + stockName);
        }
        return retValue;
    }

    public static ID createInventory(ID stockId,
                                        String description,
                                        String fiscalYear,
                                        Boolean full,
                                        Date plannedCountDate)
    {
        ID retValue = null;
        if(stockId != null)
        {
            List<SCStock__c> stockList = [Select id from SCStock__c where id = :stockId];
            if(stockList.size() > 0)
            {
                Savepoint sp = Database.setSavepoint();
                Boolean inventoryFound = false;

                try
                {
                    // determine or create an inventory
                    List<SCInventory__c> invList = [Select Id, Name from SCInventory__c where Stock__c = :stockID 
                                                    and FiscalYear__c = : fiscalYear and Status__c in ('created', 'planned')];
                    
                    SCInventory__c inv = null;
                    if(invList.size() == 1)
                    {
                        inv = invList[0];
                        retValue = inv.Id;
                        inventoryFound = true;                          
                    } 
                    else if (invList.size() > 1)
                    {
                        String msg = 'There is more inventories for the stock: ' + invList[0].Name + ' and fiscalYear: ' + fiscalYear;
                        debug(msg);
                        throw new SCfwException(msg);                   
                    }
                    if(inv == null)
                    {
                        // create inventory
                        inv = new SCInventory__c();
                        inv.Stock__c = stockID;
                        inv.Description__c = description;
                        inv.FiscalYear__c = fiscalYear;
                        inv.Full__c = full;
                        inv.PlannedCountDate__c = plannedCountDate;
                        insert inv;
                        retValue = inv.Id;
                    }
                    List<SCInventoryItem__c> invItemList = new List<SCInventoryItem__c>();
                    List<SCStockItem__c> stockItemList = [select Id, Article__c, Qty__c, ValuationType__c from SCStockItem__c where stock__c = : stockId];
                    if(inventoryFound)
                    {
                        // the inventory have been found

                        List<SCInventoryItem__c> invItemListRead = [Select id, StockItem__c from SCInventoryItem__c 
                                                                    where Inventory__c = : inv.id];
                        
                        // make map of stock item ids to its Inventory items of the given inventory
                        Map<ID, SCInventoryItem__c> mapStockItemIdToInventoryItem = new Map<ID, SCInventoryItem__c>();
                        for(SCInventoryItem__c ii: invItemListRead)
                        {
                            mapStockItemIdToInventoryItem.put(ii.StockItem__c, ii);
                        }
    
                        // create or update inventory items
                        for(SCStockItem__c si: stockItemList)
                        {
                            SCInventoryItem__c ii = mapStockItemIdToInventoryItem.get(si.Id);
                            if(ii == null)
                            {
                                // create an inventory item
                                ii = new SCInventoryItem__c();
                                ii.Article__c = si.Article__c;
                                ii.CurrentQty__c = si.Qty__c;
                                ii.CountedQty__c = 0;
                                ii.Inventory__c = inv.Id;
                                ii.StockItem__c = si.Id;
                                ii.ValuationType__c = si.ValuationType__c; 
                                invItemList.add(ii);
                            }
                            else
                            {
                                // update the inventory item 
                                ii.Article__c = si.Article__c;
                                ii.CurrentQty__c = si.Qty__c;
                                ii.CountedQty__c = 0;
                                ii.ValuationType__c = si.ValuationType__c; 
                            }           
                        }
                        insert invItemList;
                        upsert invItemListRead;
                    }
                    else
                    {
                        // the clean case we create also inventory items
                        // but only for the partial inventory
                        if(inv.Full__c)
                        {
                            for(SCStockItem__c si: stockItemList)
                            {
                                SCInventoryItem__c ii = new SCInventoryItem__c();
                                ii.Article__c = si.Article__c;
                                ii.CurrentQty__c = si.Qty__c;
                                ii.CountedQty__c = 0;
                                ii.Inventory__c = inv.Id;
                                ii.StockItem__c = si.Id;
                                ii.ValuationType__c = si.ValuationType__c; 
                                invItemList.add(ii);        
                            }
                            insert invItemList;
                        }
                    }
                }
                catch(Exception e)
                {
                    Database.rollback(sp);
                    debug('Exception: ' + SCfwException.getExceptionInfo(e));       
                }
            }
            else
            {
                throw new SCfwException('There is no stock with id: ' + stockId);
            }
        }
        return retValue;
    }
    
    /**
     * Get an inventory
     *
     * @param     invId    An Inventory ID
     * @return    inventory object
     * @author    Sergey Utko <sutko@gms-online.de>
     */
    public SCInventory__c readById(String invId)
    {
        SCInventory__c inventory = new SCInventory__c();
        
        try
        {
            inventory = [ Select Id, Name, Description__c, ERPResultDate__c, 
                                ERPStatus__c, FiscalYear__c, Full__c, InventoryDate__c, 
                                PlannedCountDate__c, Plant__c, Status__c, Stock__c, Stock__r.Name, Stock__r.ValuationType__c,
                               (Select Id, Name, CreatedDate, CreatedById, LastModifiedDate, 
                                       LastModifiedById, CurrentQty__c, CountedQty__c, Inventory__c, 
                                       Article__c, Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ArticleName__c, 
                                       ValuationType__c, StockItem__c, StockItem__r.Id, StockItem__r.Name, StockItem__r.StockArticleId__c,
                                       StockItem__r.InventoryDate__c, StockItem__r.Qty__c, StockItem__r.InventoryQty__c, StockItem__r.Article__c,
                                       StockItem__r.Article__r.Name, StockItem__r.Article__r.Id, StockArticleId__c,
                                       Inventory__r.Stock__c
                                From InventoryItem__r)
                          From SCInventory__c
                          Where Id = : invId ];
                          
            return inventory;
        }
        catch(Exception e)
        {
            System.debug('#### Cannot read inventory: ' + e.getMessage());
            return null;
        }
    }
    
    /**
     * Get a list of inventories
     *
     * @param     invIds    A list with Inventory IDs
     * @return    a list of inventory objects
     * @author    Sergey Utko <sutko@gms-online.de>
     */
    public List<SCInventory__c> readAllById(List<String> invIds)
    {
        List<SCInventory__c> inventories = new List<SCInventory__c>();
        
        try
        {
            inventories = [ Select Id, Name, Description__c, ERPResultDate__c, 
                                   ERPStatus__c, FiscalYear__c, Full__c, InventoryDate__c, 
                                   PlannedCountDate__c, Plant__c, Status__c, Stock__c, Stock__r.Name,
                                  (Select Id, Name, CreatedDate, CreatedById, LastModifiedDate, 
                                          LastModifiedById, CurrentQty__c, CountedQty__c, Inventory__c, 
                                           Article__c, Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ArticleName__c, 
                                           ValuationType__c, StockItem__c, StockItem__r.Id, StockItem__r.Name, StockItem__r.StockArticleId__c,
                                           StockItem__r.InventoryDate__c, StockItem__r.Qty__c, StockItem__r.InventoryQty__c, StockItem__r.Article__c,
                                           StockItem__r.Article__r.Name, StockItem__r.Article__r.Id, StockArticleId__c,
                                           Inventory__r.Stock__c
                                   From InventoryItem__r)
                            From SCInventory__c
                            Where Id IN : invIds ];
                          
            return inventories;
        }
        catch(Exception e)
        {
            System.debug('#### Cannot read inventories: ' + e.getMessage());
            return null;
        }
    }
    
   /*
    * Closes the inventory
    * @param   oid              id of the SCInventory object to close
    * @param   updateStockQty   this parameter controlls the update of the stock quantity
    * @param   inventoryDate    this parameter controlls the update of the inventory date
    * @return  'OK'             if the inventory was sucessfully cancelled or 'ERROR' text if not
    */
    public String close(String oid, Boolean updateStockQty, Date inventoryDate)
    {
        if(oid != null)
        {
            try
            {
                SCInventory__c inventory = [Select Id, Name, Description__c, ERPResultDate__c, 
                                                ERPStatus__c, FiscalYear__c, Full__c, InventoryDate__c, 
                                                PlannedCountDate__c, Plant__c, Status__c, Stock__c, Stock__r.Name, Stock__r.ValuationType__c
                                          From SCInventory__c
                                          Where Id = : oid ];
                                          
                List<SCMaterialMovement__c> matMoveToInsert = new List<SCMaterialMovement__c>();
                List<SCStockItem__c> stockItemsToUpdate = new List<SCStockItem__c>();
                
                List<SCInventoryItem__c> invItems = [Select Id, Name, CreatedDate, CreatedById, LastModifiedDate, 
                                                           LastModifiedById, CurrentQty__c, CountedQty__c, Inventory__c, 
                                                           Article__c, Article__r.Id, Article__r.Name, Article__r.ArticleNameCalc__c, ArticleName__c, 
                                                           ValuationType__c, StockItem__c, StockItem__r.Id, StockItem__r.Name, StockItem__r.StockArticleId__c,
                                                           StockItem__r.InventoryDate__c, StockItem__r.Qty__c, StockItem__r.InventoryQty__c, StockItem__r.Article__c,
                                                           StockItem__r.Article__r.Name, StockItem__r.Article__r.Id, StockArticleId__c,
                                                           Inventory__r.Stock__c
                                                    From SCInventoryItem__c
                                                    Where Inventory__c = :oid];
                
                if(!invItems.isEmpty())
                {
                    for(SCInventoryItem__c item : invItems)
                    {
                    
                    
                    
                        // Creating material movements   
                        SCMaterialMovement__c m = new SCMaterialMovement__c();
                        
                        m.Type__c           = '5225';
                        m.Status__c         = '5408';

                        m.Stock__c          = inventory.Stock__c;
                        m.Article__c        = item.Article__c;
                        m.ValuationType__c  = item.ValuationType__c;
                        
                        if(item.CountedQty__c != null)
                        {
                            m.Qty__c            = item.CountedQty__c;
                            m.QtyNew__c         = item.CountedQty__c;
                        }
                        if(item.StockItem__c != null)
                        {
                            m.QtyOld__c         = item.StockItem__r.Qty__c;
                        }
                        m.ProcessDate__c    = Date.today();
                        
                        matMoveToInsert.add(m);
                        
                        // Updating stock items
                        if(updateStockQty)
                        {
                            if(item.StockItem__c != null)
                            {
                                item.StockItem__r.Qty__c = item.CountedQty__c;
                                item.StockItem__r.InventoryQty__c = item.CountedQty__c;
                                stockItemsToUpdate.add(item.StockItem__r);
                            }
                        }
                        if(inventoryDate != null)
                        {
                            if(item.StockItem__c != null)
                            {
                                item.StockItem__r.InventoryDate__c = inventoryDate;
                            }
                        }
                    }
                    
                    upsert invItems;
                    upsert stockItemsToUpdate;
                    
                    insert matMoveToInsert;
                }
                
                inventory.Status__c = 'closed';
                update inventory;
                
                SCStock__c stock = [ Select id, name, locked__c From SCStock__c Where Id = :inventory.Stock__c ];
                stock.locked__c = false;
                update stock;
                
                //##################################################
                //CCEAG - transfer the inventory to SAP
                //##################################################
                CCWCMaterialInventoryCreate.callout(oid, true, false);
                
                return 'OK';    
            }
            catch(Exception e)
            {
                System.debug('#### Cannot close inventory: \n ' + e.getMessage() + '\nCause: ' + e.getCause() + '\nLineNumber: ' + e.getLineNumber() + '\nStack trace: ' + e.getStackTraceString());
                return 'ERROR-' + e.getMessage() + '\nStack trace: ' + e.getStackTraceString() + '\nType: ' + e.getTypeName();
            }
        }
        else
        {
            return 'ERROR';
        }
    }
    
   /*
    * Cancells the inventory
    * @param   oid   id of the SCInventory object to cancel
    * @return  'OK'  if the inventory was sucessfully cancelled or 'ERROR' text if not
    */
    public String cancel(String oid)
    {
        if(oid != null)
        {
            try
            {
                SCInventory__c inventory = readById(oid);
                inventory.Status__c = 'cancelled';
                update inventory;
                
                SCStock__c stock = [ Select id, name, locked__c From SCStock__c Where Id = :inventory.Stock__c ];
                stock.locked__c = false;
                update stock;
                
                return 'OK';    
            }
            catch(Exception e)
            {
                System.debug('#### Cannot cancel inventory: \n ' + e.getMessage() + '\nCause: ' + e.getCause() + '\nLineNumber: ' + e.getLineNumber() + '\nStack trace: ' + e.getStackTraceString());
                return 'ERROR-' + e.getMessage() + '\nStack trace: ' + e.getStackTraceString() + '\nType: ' + e.getTypeName();
            }
        }
        else
        {
            return 'ERROR';
        }
    }

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }
}