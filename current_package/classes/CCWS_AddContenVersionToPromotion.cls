/**********************************************************************
Name:  CCWS_AddContenVersionToPromotion()
======================================================
Purpose:                                                            
Represents a web service. Receives Postinformation, create a new chatterpost with the given info and inserts it to the database.                                                      
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
11/26/2013 Chris Sandra Schautt	<c.schautt@yoursl.de>           
***********************************************************************/


global with sharing class CCWS_AddContenVersionToPromotion {
	
	WebService static OperationResult addFeed(String sUserId, String sParentType, String sParentExtId, String sOwnerExtId, String sDocName, Blob sDocBody){
		
		//required local variables
		List<Promotion__c> listPromotions = new List<Promotion__c>(); // accounts with the given id
		List<User> listUsers = new List<User>(); //users with the given id
		OperationResult objOpResult = new OperationResult();
		// String to hold the original User-ID of Salesforce.com
		String sUserSFDCId = '';
		
		//Query User and validate result
			if(sUserId!=null && sUserId!=''){
				
			
			try{
				listUsers = [SELECT Name, IsActive, Id, ID2__c FROM User WHERE ID2__c = : sUserId]; 
			}catch (QueryException e){
				objOpResult.addError(e.getMessage());
				return objOpResult;
			}
			// check, if list is empty
			if(listUsers.size()==0){
				objOpResult.addError('No User found for ID '+sUserId+'.');
				return objOpResult;
			}
			// Check, if we found to much Users
			else if(listUsers.size()>1){
				objOpResult.addError('Found '+listUsers.size()+' Users for StrOwnerID '+sUserId+'.');
				return objOpResult;
			}
			else{
				sUserSFDCId = listUsers[0].Id;
				objOpResult.addUser(sUserSFDCId);
			}
		}// end if(sUserId!=null && sUserId!='')
		
		//+++++++++++++++++ STEP 2: Validate Promotion ++++++++++++++++++++++++++++++++++++
		
		// String to hold the original Account-ID of Salesforce.com
		String sPromotionSFDCId = '';
		
		if(sParentType =='Promotion'){
			//Query Promotion and validate result
			try{
				listPromotions = [SELECT Name, Id, PromotionID__c FROM Promotion__c WHERE PromotionID__c = : sParentExtId];
			}catch (QueryException e){
				objOpResult.addError(e.getMessage());
				return objOpResult;
			}
			// check, if list is empty
			if(listPromotions.size()==0){
				objOpResult.addError('No Promotion found for ID '+sParentExtId+'.');
				return objOpResult;
			}
			// Check, if there are to many Accounts
			else if(listPromotions.size()>1){
				objOpResult.addError('Found '+listPromotions.size()+' Promotion for StrPromotionID '+sParentExtId+'.');
				return objOpResult;
			}
			else{
				sPromotionSFDCId = listPromotions[0].Id;
				objOpResult.addSObject(sPromotionSFDCId);
			}	
		}else{
			objOpResult.addError('the parenttype is not a Promotion Objekt! ');
			return objOpResult;
		}// end if(sParentType =='Promotion')
		
	//+++++++++++++++++ STEP 3: Validate sDocName ++++++++++++++++++++++++++++++++++++
		if(sDocName==null||sDocName==''){
			objOpResult.addError('There is no title set! ');
			return objOpResult;
		}
	//+++++++++++++++++ STEP 4: Validate sDocBody ++++++++++++++++++++++++++++++++++++
	if(sDocBody== null){
		objOpResult.addError('There is no Data set! ');
		return objOpResult;
	}
	//+++++++++++++++++ STEP 5: Create the ContenVersion ++++++++++++++++++++++++++++++++++++
		ContentVersion  objContentVersion= new ContentVersion(Title=sDocName , OwnerId= sUserSFDCId);
		// convert the String into an base64
		//Blob bContenVersionData = EncodingUtil.base64Decode(sDocBody);
		
		objContentVersion.VersionData =sDocBody;
		objContentVersion.PathOnClient = sDocName;
		
		// the ContenURL or the PathOnClient must be set 
		//objContent.ContentUrl = 'https://c.na14.content.force.com/servlet/servlet.ImageServer?id=015d00000014L3G&oid=00Dd0000000d6Ea&lastMod=1385466328000';
		try{
				insert objContentVersion;
		}catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		/*
		try{
			objContentVersion=[SELECT Id, Title, OwnerId FROM ContentVersion WHERE Title =: sDocName LIMIT 1];
		}catch (QueryException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		*/
		
	//+++++++++++++++++ STEP 6: Create the Chatter Post ++++++++++++++++++++++++++++++++++++
				
		//set required fields
		FeedItem objFeedItem = new FeedItem();
		objFeedItem.ParentId = sPromotionSFDCId ;
		if(sUserSFDCId != ''){
			objFeedItem.CreatedById = sUserSFDCId;
		}else{
			objFeedItem.CreatedById = System.Userinfo.getUserId();
		}
		objFeedItem.RelatedRecordId = objContentVersion.Id;
		
		String sfullFileURL = 'link to Owner: '+ URL.getSalesforceBaseUrl().toExternalForm() +
   		'/' + objContentVersion.OwnerId +' Link to the ContenVersion: '+ URL.getSalesforceBaseUrl().toExternalForm() +
   		'/' + objContentVersion.Id +' .';
		objFeedItem.Body = sfullFileURL;
		
		// insert the feed
		try{
			insert objFeedItem;
		}
		catch (DmlException e){
			objOpResult.addError(e.getMessage());
			return objOpResult;
		}
		
		// return value if success
		objOpResult.bSuccess = true;
		return objOpResult;	
	}
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class OperationResult{
		
		// Class variables
		webservice String MessageUUID{get;set;} 
		webservice String sSObjectID{get;set;}
		webservice String sUserID{get;set;}
		webservice String sError{get;set;}
		webservice Boolean bSuccess{get;set;}
		
		// Constructor
		private OperationResult(){
			// MessageUUID='5257AA3FC97C0FE0E10080000A621094'; 
		}
		
		public boolean addUser(String sUserID){
			this.sUserID = sUserID;
			return true;
		}
		
		public boolean addSObject(String sSObjektID){
			this.sSObjectID = sSObjektID;
			return true;
		}
		
		public boolean addError(String sError){
			this.sError = sError;
			this.bSuccess = false;
			return true;
		}
	}

}