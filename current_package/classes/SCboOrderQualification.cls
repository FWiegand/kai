/* * @(#)SCboOrderQualification.cls *  
* Copyright 2010 by GMS Development GmbH 
* Karl-Schurz-Strasse 29,  33100 Paderborn 
* All rights reserved. *  
* This software is the confidential and proprietary information 
* of GMS Development GmbH. ("Confidential Information").  You 
* shall not disclose such Confidential Information and shall use 
* it only in accordance with the terms of the license agreement 
* you entered into with GMS. 
*/ 

public class SCboOrderQualification
{    
    // default qualification level
    public static final String DOM_QUALIFICATION_LEVEL_BASIC = '5001';
    
    public SCOrderQualification__c qualification { get; set; }    
    public SCboOrderQualification()    
    {   
        this.qualification = new SCOrderQualification__c();
    } 
    public SCboOrderQualification(SCOrderQualification__c qualification)    
    {        
        this.qualification = qualification;
    }

    /**     
     * initialize order qualifications
     *     
     */    
    public void init (String qualificationId, String orderItemId, String orderId)
    {
        this.qualification.qualification__c = qualificationId;
        this.qualification.orderItem__c = orderItemId;
        this.qualification.order__c = orderId;
        this.qualification.Level__c = DOM_QUALIFICATION_LEVEL_BASIC;
        System.debug('#### init Order Qualifications(): qualificationId-> ' + qualificationId);
    }

}