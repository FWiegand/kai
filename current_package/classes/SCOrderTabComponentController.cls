/*
 * @(#)SCOrderTabComponentController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * Class Hierarchy
 * ===============
 *
 *                                      this class
 *                                          |
 * SCItemsTabComponentController            |
 * SCOrderTabComponentController  <---------+
 * SCPartsServicesTabComponentController
 *          |
 * SCOrderComponentController
 *          |
 * SCfwOrderControllerBase
 *          |
 * SCfwComponentControllerBase
 *
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCOrderTabComponentController extends SCOrderComponentController
{
    /* 
     * Usage example:
     * ((SCProcessOrderController)pageController).varInController;
     * ((SCProcessOrderController)pageController).methodInController();
     */
    
    /**
     * Constructor
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCOrderTabComponentController()
    {
        // REMARK: Don't use the pageController in the constructor
        //         since it's not assigned yet for some weird reasons.
    }

    /**
     * Deletes the selected role and fills it with the role from 
     * the account roles, if available. If not it will be filled
     * with the LE.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference delRole()
    {
        roleObjList = null;
        return delRole(boOrder);
    } // delRole
    
    /**
     * Getter for order role objects.
     *
     * @return    list of order role objects
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public List<OrderRoleObj> roleObjList
    { 
        get
        {
            if (null == roleObjList)
            {
                roleObjList = new List<OrderRoleObj>();
                Map<String, OrderRoleObj> orderRoleMap = getOrderRoleMap(boOrder);
                // iterate over the sorted role types
                for (String roleType :sortOrderRole)
                {
                    if (orderRoleMap.containsKey(roleType))
                    {
                        roleObjList.add(orderRoleMap.get(roleType));
                    } // if (orderRoleMap.containsKey(roleType))
                } // for (String roleType :sortOrderRole)
            } // if (null == roleObjList)
            return roleObjList;
        }
        set;
    }

    /**
     * Adds a new role to the order.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference processCustomer()
    {
        roleObjList = null;
        return processCustomer(boOrder, processCustomerParamAid, processCustomerParamType);
    } // processCustomer

    /**
     * Reinit order roles with an account.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference changeRole()
    {
        roleObjList = null;
        return changeRole(boOrder, processCustomerParamAid);
    } // changeRole

    
    /**
     * @author Alexei Geiger <ageiger@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getRoleAccountIds()
    {
        String roleAccountIds = null;
        for( OrderRoleObj roleObj : roleObjList )
        {
            roleAccountIds = addIdString( roleAccountIds, roleObj.role.Account__c );
            
        }
        
        System.debug('###roleAccountIds: ' + roleAccountIds);
        return roleAccountIds;
    }
    
    private String addIdString( String str, String id )
    {
        if( str != null )
        {
            str += ',' + id;
        }
        else
        {
            str = id;
        }
        return str;
    }

} // SCOrderTabComponentController