/*
 * @(#)CCWCOrderEquipmentUpdateResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disEquipmentUpdate such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCOrderEquipmentUpdateResponse  
{

    public static void processOrderEquipmentUpdateResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
	{
		SCInterfaceLog__c responseInterfaceLog = null;
    	processOrderEquipmentUpdateResponse(messageID, requestMessageID, headExternalID, referenceItem, 
                                                  MaximumLogItemSeverityCode, logItemArr, 
                                                  interfaceLogList,
                                                  GenericServiceResponseMessage,
                                                  responseInterfaceLog);
	}                                                  
	
    public static void processOrderEquipmentUpdateResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog) 
    {
        String interfaceName = 'SAP_ORDER_EQUIPMENT_UPDATE';
        String interfaceHandler = 'CCWCOrderEquipmentUpdateResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            

		// Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
		responseInterfaceLog.Direction__c = direction;            
		responseInterfaceLog.MessageID__c = messageID;            
		responseInterfaceLog.ReferenceID__c = referenceID;            
		responseInterfaceLog.ResultCode__c = resultCode;            
		responseInterfaceLog.ResultInfo__c = resultInfo;
		responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);

        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            // find the order
            step = 'find an order'; 
            SCOrder__c order = readOrder(referenceItem.ExternalID, responseInterfaceLog);
            debug('order: ' + order);
			responseInterfaceLog.Order__c = order.ID;
			responseInterfaceLog.ReferenceID__c = order.ID;            
            
            // find the interfacelog
            step = 'find a request interface';
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, order.ID, referenceItem.ExternalID, 'SAP_ORDER_EQUIPMENT_UPDATE');
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            // update order
            step = 'update the order';
            order.ERPStatusEquipmentUpdate__c = CCWSGenericResponse.getResultStatus(logItemArr);
            order.ERPResultDate__c = DateTime.now();
            order.IDocOrderUpdate__c = responseInterfaceLog.Idoc__c;
            update order;
            debug('order after update: ' + order);
            step = 'write a response interface log';

            // write response interface log
			responseInterfaceLog.Count__c = 1;           
            debug('responseInterfaceLog: ' + responseInterfaceLog);

            // update request interface log
            step = 'update the request interface log';
            if(responseInterfaceLog != null)
            {
                //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.
                if(requestInterfaceLog != null)
                {                
	                requestInterfaceLog.Response__c = responseInterfaceLog.Id;
	                requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
	                interfaceLogList.add(requestInterfaceLog);
                }
            }
            else
            {
                throw new SCfwException('A response interface log object could not be crated for messageID: ' + messageID + 
                                        ', order id: ' + order.ID + ', order name: ' + headExternalID); 
            }    
            
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
		 
        finally
        {
        	if(responseInterfaceLog.Order__c != null)
        	{
				CCWCOrderCloseEx.processNext(responseInterfaceLog.Order__c);
        	}	        	
        }  
    }//processCreateOrderResponse

    public static SCOrder__c readOrder(String headExternalID, SCInterfaceLog__c responseInterfaceLog)
    {
        SCOrder__c retValue = null;
        List<SCOrder__c> ol = [select ID, name, ERPStatusEquipmentUpdate__c 
        	from SCOrder__c where name = : headExternalID];
 
 		//Test
 		//SCfwInterfaceRequestPendingException er = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusEquipmentUpdate__c  \'none\'.');
        //er.order = ol[0];
        //throw er;
 		//SCfwException er = new SCfwException('Test SCfwException');
        //throw er;
        
        
        if(ol.size() > 0)
        {
        	if(ol[0].ERPStatusEquipmentUpdate__c == 'error' || ol[0].ERPStatusEquipmentUpdate__c == 'pending' )
        	{
	            retValue = ol[0];
	            debug('read order: ' + ol[0]);
        	}
        	else if (ol[0].ERPStatusEquipmentUpdate__c == 'none' )
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusEquipmentUpdate__c  \'none\'.');
        		e.order = ol[0];
        		throw e;
        	}
        	else if (ol[0].ERPStatusEquipmentUpdate__c == 'ok' )
        	{
        		// GMSGB test to recompute sucessful equipment updates 
        		//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusEquipmentUpdate__c  \'ok\'.');
        		//e.order = ol[0];       	
        		//throw e;
        		retValue = ol[0];
	            debug('The order ('+ ol[0]+')with name: ' + headExternalId + ' has ERPStatusEquipmentUpdate__c  \'ok\'.' );
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The order with name: ' + headExternalId + ' has ERPStatusEquipmentUpdate__c ' 
        		+ ol[0].ERPStatusEquipmentUpdate__c +'.');
        		e.order = ol[0];       	
        		throw e;
        	}
        }
        else
        {
        	throw new SCfwException('The order with name: ' + headExternalId + ' has been not found in Salesforce.');
        }
        
        
        return retValue;
    }


    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }
}