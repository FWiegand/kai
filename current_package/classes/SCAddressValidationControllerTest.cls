/*
 * @(#)SCAddressValidationControllerTest.cls 
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Tests the address validation function by emulating the calls to 
 * the web services. 
 */
@isTest
private class SCAddressValidationControllerTest {

    static testMethod void testController() {
        
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        //TODO: implement
        SCAddressValidationController a = new SCAddressValidationController();
        a.showMap = true;
        a.showEdit = true;
        a.approxSearch = true;
        a.addrSelectedItem = 0;
        a.getAddr();
        
        a.setAddr(null);
        
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'Amsterdam';
        addr.houseNumber = '10';
        addr.street = 'Grot';
        addr.matchInfo = 'test';
        a.setAddr(addr);
        a.OnValidateAddress();
        a.getAddrSel();
        
        a.getAddr();
        a.getResultsAvailable();
        a.getIsSelectedAddrGeocoded();
        a.OnApproximate();
        a.OnGeocode();
        
        a.OnSelectItem();
        a.OnButtonShowMap();
        a.OnClickShowMap();
        a.getMapUrl();
    }
    
    //@author GMSS
    static testMethod void CodeCoverageA()
    {
        SCHelperTestClass3.createCustomSettings('DE',true);
        
        AvsAddress addr = new AvsAddress();
        
        SCAddressValidationController a = new SCAddressValidationController();
        a.accDataEdit = new Account();
        a.addrDataEdit = new SCInstalledBaseLocation__c();
        a.extraFields = new SCAddressValidationController.ExtraFields();
        a.getMapUrl();
        a.validateEmail();
        
        a.geoDataDE = addr;
        addr = a.geoDataDE;
        
        addr = new AvsAddress();
        addr.country = 'is';
        a.setAddr(addr);
        a.setAddr(addr);
        a.getAddrSel();
        
        a.searchQuery = 'test';
        a.OnValidateAddress();
        
        try { a.OnDoit(); } catch(Exception e) {}
        try { a.OnDoitGB(); } catch(Exception e) {}
        try { a.OnDoitDE(); } catch(Exception e) {}
        try { a.validateAll(); } catch(Exception e) {}
        try { a.validateEmail(); } catch(Exception e) {}
        try { a.validateNumbers(); } catch(Exception e) {}
        try { a.checkBeforeSave(); } catch(Exception e) {}
        try { a.gotoCustomerSearch(); } catch(Exception e) {}
        
        AvsAddress res = new AvsAddress();
        
        res.telephoneNumber = '019';
        res.mobileNumber = '019';
        res.faxNumber = '019';
        a.newData = res;
        a.accDataEdit = new Account();
        a.addrDataEdit = new SCInstalledBaseLocation__c();
        try { a.validateNumbers(); } catch(Exception e) {}
        
        res.telephoneNumber = '011111111111111111111119';
        res.mobileNumber = '011111111111111111111119';
        res.faxNumber = '011111111111111111111119';
        a.newData = res;
        a.accDataEdit = new Account();
        a.addrDataEdit = new SCInstalledBaseLocation__c();
        try { a.validateNumbers(); } catch(Exception e) {}
        
        
        SCAddressValidationController b = new SCAddressValidationController();
        b.mode = 'not location';
        b.defCountry = 'AT';
        
        addr = new AvsAddress();
        addr.surname = 'Test';
        addr.street = 'Musterstraße';
        addr.telephoneNumber = '111';
        addr.telephoneNumber2 = '111';
        addr.mobileNumber = '111';
        addr.faxnumber = '111';
        b.geoDataDE = addr;
        b.checkBeforeSave();
        
        addr.telephoneNumber = '012345';
        addr.telephoneNumber2 = '012345';
        addr.mobileNumber = '012345';
        addr.faxnumber = '012345';
        addr.email = 'test@test.de';
        b.geoDataDE = addr;
        b.checkBeforeSave();
        
    }
}