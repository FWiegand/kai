/*
 * @(#)SCbtcGeocodeInstalledBaseLocation.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Batch job to geocode installed base locations
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcGeocodeInstalledBaseLocation extends SCbtcGeocodeBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts 
{
    private ID batchprocessid = null;  
    String country = 'DE';
    Integer max_cycles = 0;
    String email = null;
    
    // the counter of the calls to the execute function
    // by the countries with double web call for geo coding 
    // such as NL, UK this counter is used to skip the even
    // record to allow the odd record the making of the second web call
    Integer callCnt = 0;
    // The limit of out calls. Its value is read from ApplicationSettings
    Integer limitOfOutCalls = 100;
    String emailData = '';

    // Subject of the email
    global String jobinfo;    
    global String summary;
    
        
    // The address validation results (temporary)
    public AvsResult addrResult {get; set;}
    public AvsResult geocodingResult {get; set;}
    private AvsAddress checkAddress = null;
    private AvsAddress checkAddress2 = null; // because geocode sets GeoX and GeoY to 0 in the input data

    List<String> locationIdList = null;    // locationIdList = null, normal batch call
    String msg = '';
    /**
     * if the contract id is null the batch job for all contract visit dates is started
     * if the contract id is not null the batch job for the given contract is started
     */
    private Id locationId = null;

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    private static Integer batchSize =  (Integer) appSettings.BTC_BATCH_SIZE_GEOCODE_INSTBAS_LOCATION__c;  
    static 
    {
	    if(batchSize == null)
	    {
	    	batchSize = 5;
	    }
    }

    // the intern location object
    public SCInstalledBaseLocation__c location { get; private set; }

   /**
    * API entry for starting the batch job for contracts
    * @param processingMode    for geocoding records with GeoStatus__c <> 'Invalid'
    *                          ALLNEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL_NEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL     geocdes all records (also the already geocoded)
    *                          SIM_ALLNEW  like ALLNEW but only simulation
    *                          SIM_ALL     like ALL but only simulation
    *                        
    *                          for geocoding records with GeoStatus__c = 'Invalid'
    *                          ALL_INVALID   
    *                          SIM_ALL_INVALID  like ALL_INVALID but only simulation
    *
    * @param country    the country code (e.g. DE, GB)
    * @param email      the email of the operator to get the message after finishing the job
    * @param max_cycles 
    * @param traceMode  if = 'trace' or 'test' the traces are written into a log file 
    */
    WebService static ID asyncGeocodeAll(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        if(!isAllowed(processingMode))
        {
            SCfwException e = new SCfwException(true, 'The processing mode \'' + processingMode + '\' is not allowed!');
            throw e;
        }
        System.debug('###traceMode: ' + traceMode);
        SCbtcGeocodeInstalledBaseLocation btc = new SCbtcGeocodeInstalledBaseLocation(processingMode, country, email, max_cycles, traceMode);
		if(SCbtcGeoCodeBase.isDoubleCall(country))
		{
			batchSize = 5;
		}
		else
		{
			batchSize = 10;
		}

        // only one location in one call to execute
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncGeocodeAll

    /**
     * checks whether the processing mode is allowed.
     * It returns true if it is allowed, else it returns false
     */
    private static Boolean isAllowed(String processingMode)
    {
       String modes = 'ALLNEW, ALL_NEW, ALL, SIM_ALLNEW, SIM_ALL, ALL_INVALID, SIM_ALL_INVALID';
       List<String> modeList = modes.split(',');
       Boolean ret = SCfwCollectionUtils.isContained(processingMode, modeList);
       return ret;
    }
    
    /**
     * Geocode asynchronically one location found on the base of location id.
     * If the location id is not null the batch job for the given location is started.
     * @param locationId
     * @param traceMode    'trace' or 'test' cause the trace to a log file
     */
    public static ID asyncGeocodeLocation(Id locationId, String traceMode)
    {
        String country = 'XX'; // dummy only - will be determined from the location
        if(locationId != null)
        {
            List<SCInstalledBaseLocation__c> cList = [Select Country__c from SCInstalledBaseLocation__c where Id = :locationId];
            if(cList.size() > 0)
            {
                country = cList[0].Country__c;
                Integer max_cycles = 1;
                String processingMode = null;
                String email = null;
                SCbtcGeocodeInstalledBaseLocation btc = new SCbtcGeocodeInstalledBaseLocation(locationId, processingMode, country, email, max_cycles, traceMode);
                // only one order in one call to execute
                batchSize = 5; // could be double call
                btc.batchprocessid = btc.executeBatch(batchSize);
                return btc.batchprocessid;
            }    
        }
        return null;
    } // asyncGeocodeLocation


    /**
     * geocodes asynchronically on the base of list of location
     * @param location list
     */
    WebService static ID asyncGeocodeLocations(List<String> locationIdList)
    {
        if(locationIdList == null || locationIdList.size() == 0)
        {
            return null;
        }   
        SCbtcGeocodeInstalledBaseLocation btc = new SCbtcGeocodeInstalledBaseLocation(locationIdList, 'trace');
        batchSize = 5; // could be double call
        // only one order in one call to execute
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncGeocodeLocations

 
    /**
     * Synchronically calls geocoding of the location list
     *
     * @param location list
     */ 
    Webservice static void syncGeocodeLocations(List<String> locationIdList)
    {
        if(locationIdList == null || locationIdList.size() == 0)
        {
            return;
        }   
        System.debug('###......locationIdList: ' + locationIdList);
        Integer max_cycles = 1;
        SCbtcGeocodeInstalledBaseLocation btc = new SCbtcGeocodeInstalledBaseLocation(locationIdList, 'test');        
            
        List<SCInstalledBaseLocation__c> locationList = [Select City__c, Country__c, County__c, District__c, 
                                                        Extension__c, FlatNo__c, Floor__c, GeoY__c, GeoX__c, GeoStatus__c, GeoApprox__c, 
                                                        HouseNo__c, PostalCode__c, Id, CountryState__c, Street__c 
                                                        from SCInstalledBaseLocation__c
                                            where 
//                                            GeoStatus__c = null
//                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) and
                                            Id in :locationIdList];
        
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(SCInstalledBaseLocation__c loc: locationList)
        {
            // validate a location
            btc.debug('location id: ' + loc.Id);
            Boolean sync = true;
            btc.country = loc.Country__c;
            if(btc.country != null)
            {
                btc.country = btc.country.toLowerCase();
            }

            btc.geocode(loc, sync, interfaceLogList);
        }
        update locationList;
        insert interfaceLogList;
    }
    
    /**
     * Synchronically calls geocoding of the location
     */ 
    Webservice static void syncGeocodeLocation(String locationId)
    {
        if(locationId == null || locationId == '')
        {
            return;
        }   
        System.debug('###......locationId: ' + locationId);
        Integer max_cycles = 1;
        String processingMode =  null;
        String email = null;
            
        List<SCInstalledBaseLocation__c> locationList = [select ID, City__c, Country__c, County__c, CountryState__c,
                                            District__c, Extension__c, FlatNo__c,
                                            HouseNo__c, PostalCode__c,  Street__c,
                                            GeoX__c, GeoY__c, GeoApprox__c
                                            from SCInstalledBaseLocation__c 
                                            where Id = :locationId];
        

        for(SCInstalledBaseLocation__c loc: locationList)
        {
            // geocode an location
            String countryLoc = loc.Country__c;
            
            SCbtcGeocodeInstalledBaseLocation btc = new SCbtcGeocodeInstalledBaseLocation(locationId, processingMode, countryLoc, email, max_cycles, 'test');        
            btc.debug('location id: ' + loc.Id);
            Boolean sync = true;
            btc.geocode(loc, sync, null);
        }
        update locationList;
    }

    /**
    * Constructors.
    * The parameter names are explicitly written.
    */
    public SCbtcGeocodeInstalledBaseLocation(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        this(null, processingMode, country, email, max_cycles, traceMode);
    }

    public SCbtcGeocodeInstalledBaseLocation(List<String> locationIdList, String traceMode)
    {
        // in case of this call the country is not regarded
        this(null, 'ALL', 'XX', null, 0, traceMode);
        this.locationIdList = locationIdList;
    }


    /**
     * Constructor 
     * @param locationId set for working out the specified location
     * @param processingMode
     * @param country
     * @param email
     * @param max_cycles
     * @param traceMode
     */
    public SCbtcGeocodeInstalledBaseLocation(Id locationId, String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {    
        this.locationId = locationId;
        if(country != null)
        {
            country = country.toLowerCase();
        }    
        this.country = country;
        this.processingMode = processingMode;
        if(this.processingMode != null)
        {
            this.processingMode = this.processingMode.toUpperCase();
        }
        this.email = email;
        this.max_cycles = max_cycles;
        this.traceMode = traceMode;
        System.debug('###traceMode: ' + traceMode);
        debug('traceMode: ' + traceMode);
        Decimal decLimitOfOutCalls = appSettings.BATCH_GEOCODE_WEB_CALLS_DAILY_MAXIMUM__c;
        if(decLimitOfOutCalls != null)
        {
            limitOfOutCalls = decLimitOfOutCalls.intValue();
        }
    }

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator 
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        /* To check the syntax of SOQL statement */
        List<SCInstalledBaseLocation__c> locationList = [select ID, City__c, Country__c, County__c, CountryState__c,
                                            District__c, Extension__c, FlatNo__c,
                                            HouseNo__c, PostalCode__c,  Street__c,
                                            GeoX__c, GeoY__c, GeoApprox__c
                                            from SCInstalledBaseLocation__c 
                                            where  
                                            Country__c = :country
                                            and GeoStatus__c = null
                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null)
                                            limit 1 ];
        debug('start: after checking statement');
        /**/
        String query = null;
        if(locationId == null)
        {
            query = ' select ID, City__c, Country__c, County__c, CountryState__c, '
                + ' District__c, Extension__c, FlatNo__c, '
                + ' HouseNo__c, PostalCode__c,  Street__c, '
                + ' GeoX__c, GeoY__c, GeoApprox__c '
                + ' from SCInstalledBaseLocation__c '
                + ' where  '
                + ' Country__c = \'' + country + '\'';

            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALLNEW')
                	|| processingMode.equalsIgnoreCase('ALL_NEW')
                    || processingMode.equalsIgnoreCase('SIM_ALLNEW')))
            {            
                query += ' and GeoStatus__c = null '
                      + ' and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) ';
            }    
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALL_INVALID')
                    || processingMode.equalsIgnoreCase('SIM_ALL_INVALID')))
            {
                query += ' and GeoStatus__c = \'Invalid\' ';
            }    
            
            if(max_cycles > 0 )
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            // specified location
            query = ' select ID, City__c, Country__c, County__c, CountryState__c, '
                + ' District__c, Extension__c, FlatNo__c, '
                + ' HouseNo__c, PostalCode__c,  Street__c, '
                + ' GeoX__c, GeoY__c, GeoApprox__c '
                + ' from SCInstalledBaseLocation__c '
                + ' where  '
                + ' Country__c = \'' + country + '\''
                + ' and id = \'' + locationId + '\'';
                if(max_cycles > 0 )
                {
                    query += ' limit ' + max_cycles;
                }
        }        
        if(locationIdList != null)
        {
            debug('locationIdList: ' + locationIdList);
            return Database.getQueryLocator([select ID, City__c, Country__c, County__c,
                                            CountryState__c,
                                            District__c, Extension__c, FlatNo__c,
                                            HouseNo__c, PostalCode__c,  Street__c,
                                            GeoX__c, GeoY__c, GeoApprox__c
                                            from SCInstalledBaseLocation__c 
                                            where  
//                                            GeoStatus__c = null
//                                            and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) and
                                            Id in :locationIdList]);
        }
        debug('query: ' + query);
        return Database.getQueryLocator(query);
    } // start
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        breakJobByExhaustedDailyCallouts(BC, limitOfOutCalls, callCnt);

        Boolean sync = false;
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCInstalledBaseLocation__c workLocation = (SCInstalledBaseLocation__c)res;
            country = workLocation.Country__c;
            if(country != null)
            {
                country = country.toLowerCase();
            }
            geocode(workLocation, sync, null);
        }// for
        if(processingMode == null  || !processingMode.startsWith('SIM'))
        {
	        if(scope.size() > 0)
	        {
	        	update scope;
	        }
        }    	
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
        // Get the ID of the AsyncApexJob representing this batch job and  
        // query the AsyncApexJob object to retrieve the current job's information.  
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  CreatedDate, TotalJobItems, CreatedBy.Email  from AsyncApexJob where Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses;
        if(email == null)
        {
            toAddresses = new String[] {a.CreatedBy.Email};
        }
        else
        {
            toAddresses = new String[] {email};
        }
        
        mail.setToAddresses(toAddresses);
        jobinfo = 'Job SCbtcGeocodeInstalledBaseLocation at ' + a.CreatedDate;
        mail.setSubject(jobinfo + ' with status[' + a.Status + ']');
        
        String text = 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. geocoded items\n\n';
//        text += 'id|name|Resource|Employee|Country|CountryState|County|PostalCode|City|District|Street|HouseNo|Extension|geoY|geoX| ---> |geoY|geoX|Country|CountryState|County|PostalCode|City|District|Street|HouseNo|Extension\n';
//        text += summary;        
        String fieldSeparator = '|';
        text += '\n\n' + getGridTitle(emailTitlePrefix, fieldSeparator, null);
        text += emailData;

        mail.setPlainTextBody(text);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    } // finish


    public static final String STEP_BEGIN = 'BEGIN';
    public static final String STEP_DBL_FIRST = 'DBL_FIRST';
    public static final String STEP_DBL_SECOND = 'DBL_SECOND';
    public static final String STEP_SINGLE = 'SINGLE';

    /**
    * Geocodes an location
    *
    * @param workLocation   the location to geocode
    * @param sync           true if the call is synchron
    * @param interfaceLogList the interface log list, used only by syncron calls, for batch job always null
    *
    */
    public void geocode(SCInstalledBaseLocation__c workLocation, Boolean sync, List<SCInterfaceLog__c> interfaceLogList)
    {
        // increase the counter of outgoing web calls
        debug('geocode');
        callCnt++;
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        Boolean secondCallMade = false;
        String resultInfo = '';
        Integer count = 1;
        String data = '';
        String step = STEP_BEGIN;
        try
        {
            if(isDoubleCall(country))
            {
                debug('double country: ' + country);
                // we make geo coding for an every odd record from batch
                // because we need two outgoing web calls.
                // The first web call we do by odd number of records.
                // The second web call we do by even number of records
                // for the result we got by the previous call by the odd number.
                // we can now call till 10 web services in one execute
                //if(isOdd(callCnt) || sync)
                //{
                    step = STEP_DBL_FIRST;

                    // checking
                    debug('odd or sync');
                    // get the list or moniker
                    location = (SCInstalledBaseLocation__c)workLocation;
                    SCAddressValidation validation = new SCAddressValidation();
                    checkAddress = getAddr(location);
                    debug('location: ' + location);
                    debug('check address: ' + checkAddress);
                    checkAddress2 = getAddr(location);

                    // prevent from taking addrResult of the first location of previous pair of accounts
                    addrResult = null;
                    geocodingResult = null;
                    addrResult = validation.check(checkAddress2);
                    if(addrResult.status == AvsResult.STATUS_EMPTY)
                    {
                        debug('the address has been not found: ' + checkAddress2);
                    }
                    debug('addrResult: ' + addrResult);
                    if(addrResult.items != null && addrResult.items.size() > 0)
                    {
                        debug('the first call: addrResult.items[0] : ' + addrResult.items[0]);
                    }
                //}
                callCnt++;        
                //if(isEven(callCnt) || sync)
                //{
                    step = STEP_DBL_SECOND;
                    
                    // geocoding
                    debug('even or sync');
                    // get the geocoding
                    validation = new SCAddressValidation();
                    if(addrResult != null && addrResult.items != null && addrResult.items.size() > 0
                        && addrResult.status != AvsResult.STATUS_EMPTY)
                    {
                        Integer indexOfAddr = getIndex(checkAddress2, addrResult);
                        AvsAddress a = addrResult.items[indexOfAddr];
                        debug('the second call: addrResult.items[' + indexOfAddr + '] : ' + addrResult.items[indexOfAddr]); 
                        
                        geocodingResult = validation.geocode(a);
                        debug('geocodingResult: ' + geocodingResult);
                        debug('geocodingResult.size(): ' + geocodingResult.items.size());
                        resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, geocodingResult, location);
                        secondCallMade = true;    
                    }
                    else
                    {
                    	workLocation.GeoStatus__c = 'Invalid';
//                    	update workLocation;
                        resultInfo = 'The address could not be found. ';
                    }    
                //}
            }
            else
            {
                step = STEP_SINGLE;
                
                // an only geocoding call
                location = (SCInstalledBaseLocation__c)workLocation;
                SCAddressValidation validation = new SCAddressValidation();
                checkAddress = getAddr(location);
                debug('location: ' + location);
                debug('check address: ' + checkAddress);
                
                checkAddress2 = getAddr(location);
                geocodingResult = validation.geocode(checkAddress2); // geocode sets GeoX and GeoY to 0
                debug('geocodingResult: ' + geocodingResult);
                addrResult = null;
                resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, 
                    geocodingResult, location);
            }
         }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo += 'Exception: ' + e.getMessage();
            String prevMode = traceMode;
            traceMode = 'test';
            debug('resultInfo:' + resultInfo);
            traceMode = prevMode;
            count = 0;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException
               || resultInfo.startsWith('Invalid'))
            {
                resultCode = 'E101';
            }
            // protocoll only the events that something happend
            if(!isDoubleCall(country) 
                || isDoubleCall(country) && isEven(callCnt)
                || thrownException)
            {
                debug('last check address: ' + checkAddress);
                String dataFieldSeparator = ',';
                Id locationId = null;
                if(location != null)
                {
                    locationId = location.Id;
                }
                data = getData(checkAddress, addrResult, geocodingResult, locationId, dataFieldSeparator, null);
                String emailFieldSeparator = '|';
                emailData += '\n' + getEmailData(checkAddress, addrResult, geocodingResult, 
                    locationId, emailFieldSeparator, null, 'step: ' + step + '; ' + resultInfo);
                if(location !=  null)
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_IB_LOCATION', 'SCbtcGeocodeInstalledBaseLocation',
                                location.Id, null, resultCode, 
                                resultInfo, data, start, count); 
                }
                else
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_IB_LOCATION', 'SCbtcGeocodeInstalledBaseLocation',
                                null, null, resultCode, 
                                resultInfo, data, start, count); 
                }                
            }                    
        }     
    } // geocode

    /**
    * Gets an address from an location
    */
    public AvsAddress getAddr(SCInstalledBaseLocation__c loc)
    {
        AvsAddress a = new AvsAddress();
        
        a.country = loc.Country__c;
        a.countrystate = loc.CountryState__c;
        a.county = loc.County__c;
        a.postalcode = loc.PostalCode__c;
        a.city = loc.City__c;
        a.district = loc.District__c;
        a.street = loc.Street__c;
        a.housenumber = loc.HouseNo__c;
        a.GeoX = loc.GeoX__c;
        a.GeoY = loc.GeoY__c;
        a.GeoApprox = loc.GeoApprox__c;
        
        return a;
    } // getAddr
    
    /**
    * Set coordinaten into an location.
    * @param sync true if the synchron call, else false
    * @param doubleCallOut true if on the reason of country we need to have two web calls
    * @param checkAddress the addresss to geocode
    * @param addrResult the result of check call
    * @param geocodingResult the result of geocode call
    * @param the location, we now working out
    * @return the text for the SCInterfaceLog.ResultInfo__c
    */
    public String setGeoCode(Boolean sync, Boolean doubleCallOut, AvsAddress checkAddress, AvsResult addrResult, 
        AvsResult geocodingResult, SCInstalledBaseLocation__c location)
    {
        SCbtcGeocodeBase.GeoCodeHolder gcHolder = new SCbtcGeocodeBase.GeoCodeHolder();
        String ret = prepareSetGeoCode(sync, doubleCallOut, checkAddress, addrResult, 
                                        geocodingResult, gcHolder);


        debug('processingMode: ' + processingMode);
        if(processingMode == null  || !processingMode.startsWith('SIM'))
        {
            location.GeoStatus__c = gcHolder.geoStatus;
            if(gcHolder.geoApprox != null)
            {
                location.GeoApprox__c = gcHolder.geoApprox;
            }    
            location.GeoX__c = gcHolder.geoX;
            location.GeoY__c = gcHolder.geoY;
//            update location; // Bulkify_Apex_Methods_Using_Collections_In_Methods
        }    
        return ret;
    }//setGeoCode

}