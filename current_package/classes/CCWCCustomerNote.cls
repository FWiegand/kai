/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	The class exports an Salesforce FeedItem item to SAP
*
* @date			17.12.2013
*
*/
public without sharing class CCWCCustomerNote extends SCInterfaceExportBase{
	
	private static String OutletNr;
	private static String OwnerId2;
	private static FeedItem FeedRecorcd;
	private static String UserName;

    public CCWCCustomerNote()
    {
    }
    
    public CCWCCustomerNote(String accId, String usrId)
    {
    	
    }    
    
    public CCWCCustomerNote(String interfaceName, String interfaceHandler, String refType, String subclassName)
    {
        super(interfaceName, interfaceHandler, refType, subclassName);
    }
    
    public CCWCCustomerNote(String interfaceName, String interfaceHandler, String refType, String accId, String usrId, FeedItem item, String usrName )
    {
    	OutletNr = accId;
    	OwnerId2 = usrId;
    	FeedRecorcd = item;
    	UserName = usrName;
    }
          
     /**
     * Prepare to make the web service callout synchrounously or asynchronously (future)
     *
     * ### Set the interface name, its interface handler and a refType for head id in an interface object!
     *
     * @param ibid      FeedItem id
     * @param async     true: use a future method to call 
     *                  false: do a synchrounous callout
     * @param testMode true: emulate call
     * @param accId Outletnummer
     * @param usrId User ID2
     * @param item String with FeedItem elements
     */
     
    @future(callout=true)
	public static void futurecallout(String ibid, boolean async, boolean testMode, String accId, String usrId, List<String> item, String usrName){									
        FeedItem feeditem = new FeedItem(
        	Title 		= item[0], 
			Body  		= item[1],
			CreatedById = item[2],
			ParentId 	= item[3]			
         );
		CCWCCustomerNote.callout(ibid, async, testMode, accId, usrId, feeditem, usrName);
	}
    
    public static String callout(String ibid, boolean async, boolean testMode, String accId, String usrId, FeedItem item, String usrName)
    {
        String interfaceName = 'CustomerNote';
        String interfaceHandler = 'CCWCCustomerNote';
        String refType = 'FeedItem';
        CCWCCustomerNote note = new CCWCCustomerNote(interfaceName, interfaceHandler, refType, accId, usrId, item, usrName);
        return note.calloutCore(ibid, async, testMode, interfaceName, interfaceHandler, refType, 'CCWCCustomerNote');
        
    }  

     /**
     * Reads an FeedItem to send
     *
     * @param noteID FeedItem ID
     * @param retValue Map Object
     * @param testMode if true no call is made, because in the test mode the salesforce framework prevent it.
     */
    public override void readSalesforceData(String noteID, Map<String, Object> retValue, Boolean testMode)
    {    	
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();                
        retValue.put('ROOT', FeedRecorcd);
        debug('FeedItem: ' + retValue);
    }

    /**
     * Fills the send structure and sends the call to SAP
     * 
     * @param dataMap with the FeedItem under the key ROOT
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure
     *           message = data sent to ERP
     *           message_v1 = endpoint 
     *           message_v2 = a return value of web service 
     *           message_v3 = a mandatory fields error
     *
     */
    public override Boolean fillAndSendERPData(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs)
    {
        Boolean retValue = true;
        FeedItem note = (FeedItem)dataMap.get('ROOT');
        // instantiate the client of the web service
        piCceagDeSfdc_NotizenOut.HTTPS_Port ws = new piCceagDeSfdc_NotizenOut.HTTPS_Port();
        
        // sets the basic authorization for the web service
        ws.inputHttpHeaders_x = u.getBasicAuth();

        // sets the endpoint of the web service
        ws.endpoint_x  = u.getEndpoint('NotizenOut');
        debug('endpoint: ' + ws.endpoint_x);
        rs.message_v1 = ws.endpoint_x;
		ws.timeout_x = u.getTimeOut();
        String errormessage = '';
        try
        {
            // instantiate the body of the call
            piCceagDeSfdc_NotizenOut.item_element sfdcItems = new piCceagDeSfdc_NotizenOut.item_element ();   
            List<piCceagDeSfdc_NotizenOut.item_element> noteItmes = new List<piCceagDeSfdc_NotizenOut.item_element>(); 
            piCceagDeSfdc_NotizenOut.parameters_element sfdcParams = new piCceagDeSfdc_NotizenOut.parameters_element ();
            List<piCceagDeSfdc_NotizenOut.parameters_element> noteParams = new List<piCceagDeSfdc_NotizenOut.parameters_element> ();       

            noteItmes.add(sfdcItems); 
            noteParams.add(sfdcParams);      
            //noteParam.add(sfdcParam);   
            
            // set the data to the body
            setNoteData(sfdcItems, note, u, testMode);
            
            String jsonsfdcItems = JSON.serialize(sfdcItems);
            String fromJSONMapNote = getDataFromJSON(jsonsfdcItems);
            debug('from json Note Item: ' + fromJSONMapNote);
            
            String jsonsfdcParams = JSON.serialize(sfdcParams);
            String fromJSONMapParams = getDataFromJSON(jsonsfdcParams);
            debug('from json FeedItem Params: ' + fromJSONMapParams);
                        
            rs.message = '\n messageData: ' + sfdcItems + sfdcParams;
            
            // check if there are missing mandatory fields
            if(rs.message_v3 == null || rs.message_v3 == '')
            {
                // All mandatory fields are filled
                // callout
                if(!testMode)
                {
                    // It is not the test from the test class
                    // go
                    errormessage = 'Call ws.Notizen_Out: \n\n';
                    rs.setCounter(1);
                    ws.Notizen_Out(noteItmes, noteParams);
                    rs.Message_v2 = 'void';
                }
            }
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'fillAndSendERPData: ' + errormessage + ' \n\n ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;

        }
        return retValue;
    }
   
     /**
     * sets the call out Note structure with data form an Note
     *
     * @param cso callout Note structure
     * @param par callout parameter structure     
     * @param noteEntry with input data
     * @param u instance of the util class CCWSUtil
     * @param testMode
     * @return ###
     */
    public String setNoteData(piCceagDeSfdc_NotizenOut.item_element cso, FeedItem noteEntry, CCWSUtil u, Boolean testMode)
    {
        CCSettings__c ccset = CCSettings__c.getInstance();
    
        String retValue = '';
        String step = '';
                
        try 
        {
            step = 'set Subject = noteEntry.Title';
            cso.Subject = noteEntry.Title;
                        
            step = 'set Description = noteEntry.Body';
            cso.Description = noteEntry.Body;
            
            step = 'set OutletNumber = OutletNr';
            cso.OutletNumber = OutletNr;
            
            step = 'set OwnerId = OwnerId2';
            cso.OwnerId = OwnerId2;            
            
            step = 'set UserName = UserName';
           	cso.UserName = UserName;
        }
        catch(Exception e)
        {
            String prevMsg = e.getMessage();
            String newMsg = 'setFeedItem: ' + step + ' ' + prevMsg;
            debug(newMsg);
            e.setMessage(newMsg);
            throw e;
        }
        return retValue;    
    }

   /**
     * Sets the ERPStatusEquipmentUpdate__c in the root object to 'pending'
     * 
     * @param dataMap of objects to dispatch
     * @param messageID 
     * @param u Util instance
     * @param testMode
     * @param rs return structure with error messages
     * @param error true if some error has occurred
     */
    public override void setERPStatus(Map<String, Object> dataMap, String messageID, CCWSUtil u, Boolean testMode, SCReturnStruct rs, Boolean error)
    {
    }
    

    public static void debug(String msg) 
    {
        System.debug('###...' +  msg);        
    }

}