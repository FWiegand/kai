/*
 * @(#)SCInterfaceImportBase.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * 
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCInterfaceImportBase extends SCInterfaceBase 
{ 

     public static void writeInterfaceLog(String interfaceName, 
                            String interfaceHandler,
                            String type,      // OUTBOUND
                            String direction, // outbound
                            String messageID,
                            ID referenceID, 
                            String refType,
                            ID referenceID2,
                            String refType2, 
                            String resultCode, 
                            String resultInfo, 
                            String data, 
                            Datetime start, 
                            Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = type;
        ic.MessageID__c = messageID;
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = direction;
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        if(refType != null && refType == 'SCContract__c')
        {
            ic.Contract__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCContract__c')
        {
            ic.Contract__c = referenceID2;
        }    
        
        if(refType != null && refType == 'SCOrder__c')
        {
            ic.Order__c = referenceID;
        }    
        if(refType2 != null && refType2 == 'SCOrder__c')
        {
            ic.Order__c = referenceID2;
        }    
        insert ic;
    }    
  
    
}

/**
    #### Preparing for a generic import
    
    public List<ImportItemDef> impItemDefList = new List<ImportItemDef>();
    public Map<String, ImportItemDef> srcImpItemDefMap = new Map<String, ImportItemDef>();    // key is srcPath + '#' + srcName
    public Map<String, ImportItemDef> destImpItemDefMap = new Map<String, ImportItemDef>();   // key is destNodePath

    public class ImportItemDef
    {
        public String srcName;               // The name of the field to be imported SerialNo
        public String srcPath;               // The path to the field in the web service input structure, e.g. ordItems.InstalledBase

        public String destSFObjectName;      // The Salesforce object name, e.g. SCInstalledBase__c
        public String destNodePath;          // The path to the Salesforce object in the output tree, e.g order.orderLine.InstalledBase#SerialNo

        public Map<String, String> valueMap = null; // mapping of input values to output values
        public String transformationFunctionName = null; // the name of a transformation function  

        public Boolean searchField = false;         // if true the field value is used to find the destination record by update
        public Map<String, String> modeMap = null;  // INSERT_MODE, UPDATE_MODE, DELETE_MODE
                                                    // The value of the field determines the mode for the object

        public String errorMsg;                     // error message for the field
        
        // for handling default values
        public Boolean isDefault = false;          // if true, the value of the field defValue is set as default value into the destination field
        public String defValue = null;             // the default values are set only into the destImplItemDefMap
        
        // constructor
        public ImportItemDef(String srcName, String srcPath, String destSFObjectName, String destNodePath, 
                             Map<String, String> valueMap, String transformationFunctionName,
                             Boolean searchField, Map<String, String> modeMap,
                             Boolean isDefault, String defValue)
        {
            this.srcName = srcName;
            this.srcPath = srcPath;
            this.destSFObjectName = destSFObjectName;
            this.destNodePath = destNodePath;
            this.valueMap = valueMap;
            this.transformationFunctionName = transformationFunctionName;
            this.searchField = searchField;
            this.modeMap = modeMap;
            this.errorMsg = errorMsg;
            this.isDefault = isDefault;
            this.defValue = defValue;
        }                                    
    }
 
*/