/*
 * @(#)SCMaterialRequestExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Used in the stock tab to order parts and book other material movements
 * @author Alexander Wagner <awagner@gms-online.de>
 */
public class SCMaterialRequestExtension extends SCMatMoveBaseExtension
{
    public static SCfwDomain domMatStat = new SCfwDomain('DOM_MATERIALMOVEMENT_STATUS');

    private SCStock__c stock;
    private List<SelectOption> receiverList;
    private String domMatRequest = SCfwConstants.DOMVAL_MATMOVETYP_MATREQUEST_EMPL;
    
    public SCStockItem__c stockItem { get; set; }
    public Integer curQty { get; set; }
    public Integer qty { get; set; }
    public String matMoveType { get; set;}
    public List<SelectOption> sourceList { get; set;}
    public Id sourceStock { get; set;}
    public SCResourceAssignment__c assignment { get; set;}
    
    public Boolean assignmentOk { get; private set; }
    public Boolean articleValType { get; private set; }
    public Boolean requestOk { get; private set; }
    
   /*
    * Returns a list of possible material movement types
    */
    public List<SelectOption> getTypeList()
    {
        String paramV5;
        String paramV15;
        Integer nV5;
        Integer nV15;
        String label;
        String language = UserInfo.getLanguage();
        List<SelectOption> domValList = new List<SelectOption>();
        
        for (SCfwDomainValue domainValue :domMatMove.getDomainValues())
        {
            paramV5 = domainValue.getControlParameter('V5');
            paramV15 = domainValue.getControlParameter('V15');
            nV5 = ((null != paramV5) && (paramV5.length() > 0)) ? 
                        Integer.valueOf(paramV5) : 0;
            nV15 = ((null != paramV15) && (paramV15.length() > 0)) ? 
                        Integer.valueOf(paramV15) : 0;
                        
            if (((1 == nV5) && (nV15 > 1)) || 
                domainValue.getId().equals(domMatRequest))
            {
                label = domainValue.getText(language);
                domValList.add(new SelectOption(domainValue.getId(), (null == label) ? domainValue.getId() : label));
            } 
        } // for (SCfwDomainValue domainValue...
        return domValList;
    } // getTypeList

    
    /**
     * Initialize the page objects for selecting an article and 
     * a material movement type.
     * Sets the quantity for the actual stock to 0.
     */
    public SCMaterialRequestExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        
        // read additional stock data required to display the information
        try
        {
            this.stock = [SELECT Id, Info__c, Name, Plant__r.Name, ValuationType__c FROM SCStock__c WHERE Id = :this.stock.Id LIMIT 1];
        }
        catch (QueryException e)
        {
            System.debug('#### SCMaterialRequestExtension(): Stock not found!!');
        }
        
        assignment = new SCResourceAssignment__c();
        assignment.ValidFrom__c = Date.today();
        assignment.ValidTo__c = Date.today();
        stockItem = new SCStockItem__c();
        requestOk = false;
        
        sourceList = new List<SelectOption>();
        receiverList = new List<SelectOption>();
        
        curQty = 0;
        assignmentOk = getMatMoveInfos(stock.Id, assignment.ValidFrom__c, 
                                        sourceList, receiverList);
        requestOk = !assignmentOk;
        articleValType = false;
    } // SCMaterialRequestExtension

    /**
     * This method is called when the require date is changed.
     * It fills the lists for source and receiver again.
     */
    public PageReference refillLists()
    {
        System.debug('#### refillLists(): new date -> ' + assignment.ValidFrom__c);

        if (assignment.ValidFrom__c < Date.today())
        {
            assignment.ValidFrom__c = Date.today();
            System.debug('#### refillLists(): correct date -> ' + assignment.ValidFrom__c);
        } 
        getMatMoveInfos(stock.Id, assignment.ValidFrom__c, sourceList, receiverList);
        return null;
    } // refillLists

    /**
     * Methode is called, when the user clicks [OK].
     * After the validion of the quantity the entry in SCMaterialMovement 
     * is created. If this is successful, entries in SCMateralMovement for
     * the current stock will be processed.
     */
    public PageReference requestMat()
    {
        System.debug('#### requestMat(): Stock         -> ' + stock.Name);
        System.debug('#### requestMat(): Article       -> ' + stockItem.Article__c);
        System.debug('#### requestMat(): ValuationType -> ' + stockItem.ValuationType__c);
        System.debug('#### requestMat(): Quantity      -> ' + qty);
        System.debug('#### requestMat(): DelivStock    -> ' + sourceStock);
        System.debug('#### requestMat(): RequestDate   -> ' + assignment.ValidFrom__c);
        
        // validate the quantity
        SCfwDomainValue domVal = domMatMove.getDomainValue(matMoveType, true);
        String param = domVal.getControlParameter('V4');
        if (param.equals('1') && (qty > curQty))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.SC_msg_QtyLessActualQty));
            return null;
        } 
        
        // create material movements and process them
        SCboMaterialMovement boMatMove = new SCboMaterialMovement(domMatStat);
        /* GMSNA 07.02.2012 disabled as the valuation type for the material request is not relevant
        try
        {
            if(!stock.ValuationType__c) 
            { 
                stockItem.ValuationType__c = null; 
            }
        }
        catch (Exception e) 
        { 
        }
        try
        {
            articleValType = [SELECT ValuationType__c FROM SCArticle__c WHERE Id = :stockItem.Article__c LIMIT 1].ValuationType__c;
        }
        catch (QueryException e)
        {
            System.debug('#### requestMat(): Article not found!!');
            articleValType = false;
        }
        
        if (!articleValType) 
        { 
            stockItem.ValuationType__c = null; 
        }
        */
        
        if (boMatMove.createMatMove(stock.Id, stockItem.Article__c, stockItem.ValuationType__c, qty, 
                                    SCfwConstants.DOMVAL_MATSTAT_BOOK, matMoveType, 
                                    Date.today(), sourceStock, null, null))
        {
           //###### CCE ###### -->
           
           if(matMoveType == '5202') //DOMVAL_MATMOVETYP_MATREQUEST_EMPL
           {
                
                CCWCMaterialReservationCreate.callout(stock.Plant__r.name, stock.name, CCWCMaterialReservationCreate.MODE_RESERVATION_FOR_STOCK, true, false);
           }
           //###### CCE ###### <--
          
            requestOk = true;
        } 
        
        return null;
    } // requestMat

    /**
     * initQuantity
     * ============
     *
     * After the selection of an article this methode is called.
     * It determines the actual quantity for the article in the current stock.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference initQuantity()
    {
        System.debug('#### initQuantity(): Article       -> ' + stockItem.Article__c);
        System.debug('#### initQuantity(): ValuationType -> ' + stockItem.ValuationType__c);
        
        try
        {
            articleValType = [SELECT ValuationType__c FROM SCArticle__c WHERE Id = :stockItem.Article__c LIMIT 1].ValuationType__c;
        }
        catch (QueryException e)
        {
            System.debug('#### initQuantity(): Article not found!!');
            articleValType = false;
        }
        
        if (!articleValType)
        {
            stockItem.ValuationType__c = null;
        }
        
        try
        {
            SCStockItem__c stockItem = [Select Name, Qty__c from SCStockItem__c where Stock__c = :stock.Id and Article__c = :stockItem.Article__c and ValuationType__c = :stockItem.ValuationType__c limit 1];
            if (null != stockItem.Qty__c)
            {
                curQty = stockItem.Qty__c.intValue();
            } 
            else
            {
                curQty = 0;
            }
        }
        catch (QueryException e)
        {
            System.debug('#### initQuantity(): nothing found!!');
            curQty = 0;
        }
        
        return null;
    } // initQuantity
} // SCMaterialRequestExtension