/**
 * SCPaymentData.cls    jp 17.02.2011
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * The class is used as a data holder for the payment data.
 *
 */
 public class SCPaymentData
 {
    public static String MODE_PRODUCTIVE = 'productive';
    public static String MODE_DEVELOPER_TEST = 'developer_test';
    public static String MODE_SALESFORCE_TEST = 'salesforce_test';
    public SCPaymentData()
    {
        // do the setting after instatiation
    }
    public SCPaymentData(String cardType,        
                            Double amountValue,
                            String currencyCode,           
                            String orderCode,   
                            Integer tryNum,   
                            String brand,        
                            String cardNumber,             
                            String expiryDateMonth,        
                            String expiryDateYear,         
                            String cardHolderName,         
                            String cvc,                    
                            String firstName,              
                            String lastName,               
                            String street,                 
                            String postalCode,             
                            String city,                   
                            String countryCode,            
                            String telephoneNumber,        
                            String startDateMonth,         
                            String startDateYear,
                            String shopperEmailAddress)
    {
        setCardType(cardType);                          
        setAmountValue(amountValue);        
        setCurrencyCode(currencyCode);
        setOrderCode(orderCode);
        setTryNum(tryNum);               
        setBrand(brand);                 
        setCardNumber(cardNumber);                              
        setExpiryDateMonth(expiryDateMonth);                    
        setExpiryDateYear(expiryDateYear);                      
        setCardHolderName(cardHolderName);                      
        setCvc(cvc);                                            
        setFirstName(firstName);                                
        setLastName(lastName);                                  
        setStreet(street);                                      
        setPostalCode(postalCode);                              
        setCity(city);                                          
        setCountryCode(countryCode);                            
        setTelephoneNumber(telephoneNumber);                    
        setStartDateMonth(startDateMonth);                      
        setStartDateYear(startDateYear);                        
        setShopperEmailAddress(shopperEmailAddress);
   }
    public String mode = MODE_PRODUCTIVE;
    public Datetime buttonStart = null;
    public Datetime afterWebCallStart = null;
    private String cardType;
    private Double amountValue;
    private String currencyCode;
    private String orderCode;
    private Integer tryNum; // 1 based it is incremented by caller
    private String brand;
    private String cardNumber;
    private String expiryDateMonth;
    private String expiryDateYear;
    private String cardHolderName;
    private String cvc;
    private String firstName;
    private String lastName;
    private String street;
    private String postalCode;
    private String city;
    private String countryCode;
    private String telephoneNumber;
    private String shopperEmailAddress;
    // only for SWITCH
    private String startDateMonth;
    private String startDateYear;
   
    // getter methods
    public String getCardType()
    {
        return cardType;
    }
    public String getCurrencyCode()
    {
        return currencyCode;
    }
    public String getDomCardType()
    {
        return cardType;
    }
 
    public Double getAmountValue()
    {
        return amountValue;
    }
 
    public String getOrderCode()
    {
        return orderCode;
    }
 
    public Integer getTryNum()
    {
        return tryNum;
    }
    public String getBrand()
    {
        return brand;
    }
    public String getCardNumber()
    {
        return cardNumber;
    }
 
    public String getExpiryDateMonth()
    {
        return expiryDateMonth;
    }
 
    public String getExpiryDateYear()
    {
        return expiryDateYear;
    }
 
    public String getCardHolderName()
    {
        return cardHolderName;
    }
 
    public String getCvc()
    {
        return cvc;
    }
 
    public String getFirstName()
    {
        return firstName;
    }
 
    public String getLastName()
    {
        return lastName;
    }
 
    public String getStreet()
    {
        return street;
    }
 
    public String getPostalCode()
    {
        return postalCode;
    }
 
    public String getCity()
    {
        return city;
    }
 
    public String getCountryCode()
    {
        return countryCode;
    }
 
    public String getTelephoneNumber()
    {
        return telephoneNumber;
    }
 
 
    public String getStartDateMonth()
    {
        return startDateMonth;
    }
 
    public String getStartDateYear()
    {
        return startDateYear;
    }
  
    public String getShopperEmailAddress()
    {
        return shopperEmailAddress;
    }
    public void setCardType(String val)
    {
        cardType = val;
    }
    
    public void setAmountValue(Double val)
    {
        amountValue = val;
    }
    public void setOrderCode(String val)
    {
         orderCode = val;
    }
 
    public void setBrand(String val)
    {
        brand = val;
    }
    public void setCardNumber(String val)
    {
         cardNumber = val;
    }
 
    public void setExpiryDateMonth(String val)
    {
         expiryDateMonth = val;
    }
 
    public void setExpiryDateYear(String val)
    {
         expiryDateYear = val;
    }
 
    public void setCardHolderName(String val)
    {
         cardHolderName = val;
    }
 
    public void setCvc(String val)
    {
         cvc = val;
    }
 
    public void setFirstName(String val)
    {
         firstName = val;
    }
 
    public void setLastName(String val)
    {
         lastName = val;
    }
 
    public void setStreet(String val)
    {
         street = val;
    }
 
    public void setPostalCode(String val)
    {
         postalCode = val;
    }
 
    public void setCity(String val)
    {
         city = val;
    }
 
    public void setCountryCode(String val)
    {
         countryCode = val;
    }
 
    public void setTelephoneNumber(String val)
    {
         telephoneNumber = val;
    }
  
    public void setStartDateMonth(String val)
    {
         startDateMonth = val;
    }
 
    public void setStartDateYear(String val)
    {
         startDateYear = val;
    }
    
    public void setCurrencyCode(String val)
    {
        currencyCode = val;
    }
    public void setTryNum(Integer val)
    {
        tryNum = val;
    }
    public void setShopperEmailAddress(String val)
    {
        shopperEmailAddress = val;
    }
    
    /**
    *    Gets the string with cnt asterisks
    */
    private String getAsterisks(Integer cnt)
    {
        String ret = '';
        for(Integer i= 0; i < cnt; i++)
        {
            ret += '*';
        }
        return ret;
    }//getAsterisks

    public String toString2()
    {
        String locCardNumber = 'null';
        if(cardNumber != null)
        {
            Integer len = cardNumber.length();
            if(len > 8)
            {
                Integer astLen = len - 8;
                String asterisks = getAsterisks(astLen);
                locCardNumber = cardNumber.substring(0,4) + asterisks + cardNumber.substring(len-4);
            }
            else
            {
                locCardNumber = '********';
            }
        }
        String ret = 'cardType: ' + cardType + 
        ', amountValue: ' + amountValue + 
        ', currencyCode: ' + currencyCode + 
        ', orderCode: ' + orderCode + 
        ', tryNum: ' + tryNum + 
        ', cardNumber: ' + locCardNumber  + 
        ', brand: ' + brand +
        ', expiryDateMonth: ' + expiryDateMonth + 
        ', expiryDateYear: ' + expiryDateYear + 
        ', cardHolderName: ' + cardHolderName + 
        ', cvc: ' + + cvc + 
        ', firstName: ' + firstName + 
        ', lastName: ' + lastName + 
        ', street: ' + street + 
        ', postalCode: ' + postalCode + 
        ', city: ' + city + 
        ', countryCode: ' + countryCode + 
        ', telephoneNumber: ' + telephoneNumber + 
        ', shopperEmailAddress: ' + shopperEmailAddress + 
        ', startDateMonth: ' + startDateMonth + 
        ', startDateYear: ' + startDateYear; 
        return ret;
    }
 }