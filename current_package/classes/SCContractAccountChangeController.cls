/*
 * @(#)SCContractAccountChangeController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller changes the contract account.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCContractAccountChangeController
{
    public Id cid { get; set; }
    public SCContract__c contract { get; set; }
    public String errorMessage { get; set; }
    public Id selAccountId { get; set; }
    public String selAccountName { get; set; }
    public Id selAccountOwnerId { get; set; }
    public String selAccountOwnerName { get; set; }

    public SCContractAccountChangeController(ApexPages.StandardController controller)
    {       
        if (ApexPages.currentPage().getParameters().containsKey('id'))
        {
            cid = ApexPages.currentPage().getParameters().get('id');
            contract = [ Select Name, Account__c, Account__r.FirstName__c, Account__r.LastName__c, Account__r.Name,
                                AccountOwner__c, AccountOwner__r.FirstName__c, AccountOwner__r.LastName__c, AccountOwner__r.Name,
                                Contact__c, Contact__r.Name,  Contact__r.FirstName, Contact__r.lastName
                         From SCContract__c 
                         Where id = :cid ];
            
            if(contract.Account__r.Name != null)
                selAccountName = contract.Account__r.Name;
            else
                selAccountName = '';
            
            if(contract.AccountOwner__r.Name != null)
                selAccountOwnerName = contract.AccountOwner__r.Name;
            else
                selAccountOwnerName = '';
        }
    }
    
    public PageReference onLoadNewAccount()
    {
        if(selAccountId != null && String.valueOf(selAccountId).trim() != '')
        {
            contract.Account__c = selAccountId;
        }
        return null;
    }
    
    public PageReference onLoadNewAccountOwner()
    {
        if(selAccountOwnerId != null && String.valueOf(selAccountOwnerId).trim() != '')
        {
            contract.AccountOwner__c = selAccountOwnerId;
        }
        return null;
    }    

    public PageReference onSaveNewAccounts()
    {
        try{
            upsert contract;
            
            // Redirecting back to the contract page
            PageReference pageRef;
            pageRef = new PageReference('/' + cid);
            return pageRef.setRedirect(true);
            
        }
        catch(Exception ex)
        {
            System.debug('#### upsert error: ' + ex);
            return null;
        }
    }

    public PageReference goBack()
    {
        if(cid != null)
        {
            PageReference pageRef;
            pageRef = new PageReference('/' + String.valueOf(cid));
            return pageRef.setRedirect(true);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Get Account Ids.
     *
     * @return    comma separated Account id string
     *
     * @author Alexei Geiger <ageiger@gms-online.de>
     * @version $Revision$, $Date$
     */
    public String getAccountIds()
    {
        String accountIds = null;

        if(contract.Account__c != null && contract.AccountOwner__c != null)
        {
          accountIds = addIdString( accountIds, (String)contract.Account__c);
          accountIds = addIdString( accountIds, (String)contract.AccountOwner__c);
        }

        System.debug('###accountIds: ' + accountIds);
        return accountIds;
    }
    private String addIdString( String str, String id )
    {
        if( str != null )
        {
            str += ',' + id;
        }
        else
        {
            str = id;
        }
        return str;
    }
    
}