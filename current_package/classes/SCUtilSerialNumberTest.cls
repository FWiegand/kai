/*
 * @(#)SCUtilSerialNumberTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for validating the behaviour of pricing for
 * service items (SCOrderLine).
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCUtilSerialNumberTest
{    
    static testMethod void TestUtilSerialNumber() 
    {     
        // SCutilSerialNumber s = new SCutilSerialNumber ();
        
        Test.startTest();
       
        // check if all methods are ok
        System.assertEquals(SCutilSerialNumber.isDouble('12441241'), true, 'Test failed on true-number test');
        System.assertEquals(SCutilSerialNumber.isDouble('12f41241'), false, 'Test failed on false-number test');
        
        System.assertEquals(SCutilSerialNumber.ASCII(' '), 32, 'Test failed on ASCII test, with character '+SCutilSerialNumber.ASCII(' '));
        System.assertEquals(SCutilSerialNumber.ASCII('u'.toLowerCase()), 117, 'Test failed on ASCII test, with character '+SCutilSerialNumber.ASCII(' '));
        
        //Test of serial numbers with lenght 8 and 9
        List <String> testNumbers = new String [] {'27417841',
        '28268942',
        '31292383',
        '00310300',
        '30039853',
        '00305005',
        '32282618',
        '30855887',
        '17670363',
        '29498473',
        '25390289',
        '23909767',
        '29731228',
        '31056974',
        '30627731',
        '25957293',
        '30605079',
        '25202843',
        '17285635',
        '30830065',
        '308300652',
        '308100612'};
              
        for (String testNumber: testNumbers) 
        {
            System.assertEquals(SCutilSerialNumber.isValidSerialNumber(testNumber), true, 'True-test failed on '+testNumber);
        }

        //Test of serial numbers with lenght 22
        List <String> testNumbers2 = new String [] {'2102007120050080320041',
        '2103007120050080171680',
        '2103007120050080129926',
        '2102003026210020059197',
        '2102007120050080164676',
        '2198003017220020050734',
        '2104003013350020051228',
        '2102003080090030071055',
        '2102003026000020068707',
        '2102003026220020050247',
        '2199000050730030381795',
        '2104003026020020050364',
        '2103003067780080052668',
        '2197004628289330654122',
        '2102003026200020085999',
        '2104003067780080064363',
        '2102003070210740050215',
        '2101003026200020090895',
        '2104003026200020083726'};
        
        //'2106013360060089000231'};
        
        for (String testNumber2: testNumbers2) 
        {
            System.assert(SCutilSerialNumber.isValidSerialNumber(testNumber2), 'True-test failed on '+testNumber2);
        }
        
        //Test of serial numbers with lenght 28
        List <String> testNumbers3 = new String [] {'210707310204<<<<0001005078N3',
        '210641309523<<<<3100005009N6',
        '210545306778<<<<0008007552N4',
        '210712305325<<<<0006005044N0',
        '21083900100027810006005424N7',
        '21071100100042880001005259N0',
        '21083900100054660001005523N2',
        '21064900100038763100005002N7',
        '210746310301<<<<0001005940N4',
        '21093200100061100001011674N9',
        '21090800100042850001005051N1',
        '21092200100042890001006225N9',
        '210640305945<<<<0002006469N0',
        '210707310012<<<<3100001361N2',
        '21072100100051101400057212N4',
        '21094300100093513100005072N6',
        '210519308017<<<<0003022893N0',
        '210825309515<<<<3100005631N2',
        '21071900200325320001005184N9',
        '210634302620<<<<3100010173N0'};
        
        for (String testNumber3: testNumbers3) 
        {
            System.assertEquals(SCutilSerialNumber.isValidSerialNumber(testNumber3), true, 'True-test failed on '+testNumber3);
        }
        
        //Test of serial numbers with lenght 20
        List <String> testNumbers4 = new String [] {'01003051880060051228',  
        '00003001600060258153',  
        '01003001600060079375',  
        '00003043540100052319',  
        '11111111100000000001',  
        '00003043540100052319',  
        '00003050890700053750',  
        '98003001280060050244',  
        '00003050890700050452',  
        '99003000190010050067',  
        '01003061150020050118',  
        '99003061150010050086',  
        '98003001290060058829',  
        '00003050890700051114',
        '02003101000010116170',
        '00003050000060090412',
        '99003058310020053712',
        '01003080510010068106',
        '01004626260020050488',
        '98003000440010169032',
        '97000068840020057604',
        '02003026000020053618',
        '97000068660020083560',
        '04003070210740000956',
        '98003002600010069529',
        '99003000750010065234',
        '04003360000890000780',
        '99003061510010057506',
        '03007114660080070409',
        '99003000090010050336',
        '03003080090030057307',
        '98003002100010070841',
        '00003050000060139319',
        '04003051030010063246'};
        
        for (String testNumber4: testNumbers4) 
        {
            System.assert(SCutilSerialNumber.isValidSerialNumber(testNumber4), 'True-test failed on '+testNumber4);
        }
        
        //Test of not valid serial numbers with lenght 8, 9, 20, 22, 28
        List <String> testNumbers5 = new String [] {'0100305188006005122n',  
        '0000300160006025w153',  
        '01003001600060079374',  
        '000030v3540100052319',  
        '11111111100000000000',
        '210641309523<<<<3100005009N4',    
        '2105e5306778<<<<0008007552N4',
        //'210712305225<<<<0006005044N0',
        '210605308501<<<<0001007165N3',    //'210605308501<<<<0001007165N2'
        '210523300160<<<<0006013358N8',    //'210523300160<<<<0006013358N9',
        '3083d065',
        '3083d0653',
        '2104003026020020050365',
        '210300306778g080052668',
        '0197004628289330654122'};
        
        for (String testNumber5: testNumbers5) 
        {
            System.assert(SCutilSerialNumber.isValidSerialNumber(testNumber5), 'True-test failed on '+testNumber5);
            //System.assertEquals(SCutilSerialNumber.isValidSerialNumber(testNumber5), false, 'False-test failed on '+testNumber5);
        }
        Test.stopTest();
        
    }
    
    static testMethod void TestCodeCoverage() 
    {
        Test.startTest();
        SCutilSerialNumber obj = new SCutilSerialNumber('123456789012345678901234567890');
        
        System.assertNotEquals(null,obj.isValid);
        System.assertNotEquals(null,obj.getSerialNumberDetails());
        
        System.assertNotEquals(null,obj.getSerialNumberDetails('12345678901234567890'));
        System.assertNotEquals(null,obj.getSerialNumberDetails('1234567890123456789012'));
        System.assertNotEquals(null,obj.getSerialNumberDetails('1234567890123456789012345678'));
        System.assertNotEquals(null,obj.getSerialNumberDetails('12345678901234567890123456<<'));
        System.assertNotEquals(null,obj.getSerialNumberDetails('123456789012345678901234<<<<'));
        
        Test.stopTest();
    }
}