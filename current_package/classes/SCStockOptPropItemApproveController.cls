/*
 * @(#)SCStockOptPropItemApproveController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCStockOptPropItemApproveController
{
    public String stockOptPropId { get; set; }
    public SCStockOptimizationProposal__c obj { get; set; }
    private Map<Id,Double> approvedQuantityCopy;

    public SCStockOptPropItemApproveController()
    {
    }

   /**
    * Default constructor
    */
    public SCStockOptPropItemApproveController(ApexPages.StandardController controller)
    {        
        List<String> itemFields = new List<String>{ 'Name',
                                                    'Status__c',
                                                    'StockOptimizationProposalItems__r',
                                                    'StockOptimizationProposalItems__r.Name',
                                                    'StockOptimizationProposalItems__r.Delta__c', 
                                                    'StockOptimizationProposalItems__r.CountedQuantity__c', 
                                                    'StockOptimizationProposalItems__r.Article__c', 
                                                    'StockOptimizationProposalItems__r.Article__r.Name',
                                                    'StockOptimizationProposalItems__r.Article__r.ArticleNameCalc__c',
                                                    'StockOptimizationProposalItems__r.ArticleName__c', 
                                                    'StockOptimizationProposalItems__r.ApprovedQuantity__c', 
                                                    'StockOptimizationProposalItems__r.AppliedRule__c',
                                                    'StockOptimizationProposalItems__r.StockItem__c',
                                                    'StockOptimizationProposalItems__r.StockItem__r.Name',
                                                    'StockOptimizationProposalItems__r.StockItemMinQuantity__c',
                                                    'StockOptimizationProposalItems__r.ProposalQuantity__c',
                                                    'StockOptimizationProposalItems__r.LastModifiedById',
                                                    'StockOptimizationProposalItems__r.LastModifiedDate' };
        
        // Must do this because of salesforce 'bug' 
        // (System.SObjectException: You cannot call addFields after you've already loaded the data.)
        if (!Test.isRunningTest())
        {
            controller.addFields(itemFields);
        }
        
        obj = (SCStockOptimizationProposal__c)controller.getRecord();
               
    }

   /**
    * Saves all stock optimisation proposal items
    *
    * @return    Page reference
    */
    public PageReference save()
    {   System.debug('#### obj.StockOptimizationProposalItems__r.size(): ' + obj.StockOptimizationProposalItems__r.size());
        if(obj.StockOptimizationProposalItems__r.size() > 0)
        {
            String proposalId = obj.id;
            try
            {   System.debug('#### trying to update obj');
                // Updating new approved quantity values
                upsert obj.StockOptimizationProposalItems__r;
                
                // Setting status to approved for the object
                obj.Status__c = 'Approved';
                upsert obj;
                
                // Collecting all stock item ids
                Set<Id> stockItemId = new Set<Id>();
                for(SCStockOptimizationProposalItem__c i :obj.StockOptimizationProposalItems__r)
                {
                    stockItemId.add(i.StockItem__c);
                }
                
                System.debug('#### stockItemId: ' + stockItemId);
                System.debug('#### stockItemId.size: ' + stockItemId.size());
                
                // Updating stock items (MinQty__c)
                if(stockItemId.size() > 0)
                {
                    List<SCStockItem__c> stockItems = new List<SCStockItem__c>([ Select Id, Name, MinQty__c, 
                                                                                (Select Id, ApprovedQuantity__c From StockOptimizationProposalItems__r
                                                                                 where proposal__c = :proposalId)
                                                                                 From SCStockItem__c
                                                                                 Where Id IN :stockItemId ]);
                    
                    System.debug('#### stockItems.size: ' + stockItems.size());
                    
                    for(SCStockItem__c item :stockItems)
                    {
                        if(item.StockOptimizationProposalItems__r[0] != null)
                        {
                            item.MinQty__c = item.StockOptimizationProposalItems__r[0].ApprovedQuantity__c;
                        }
                    }
                    
                    upsert stockItems;
                    
                } // if(stockItemId.size() > 0)
            }
            catch(DmlException e)
            {
                apexPages.message msg = new apexpages.message(apexpages.severity.INFO,'Save failed.');
                ApexPages.addMessage(msg);
                ApexPages.addMessages(e);
                
                System.debug('#### Save error: ' + e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage()); 
            }
        }
        
        PageReference pageRef;
        pageRef = new PageReference('/' + obj.id);
        return pageRef.setRedirect(true);
    }
    
   /**
    * Cancel method
    *
    * @return    Page reference to the Stock Optimization Proposal
    */
    public PageReference onCancel()
    {
        PageReference pageRef;
        pageRef = new PageReference('/' + obj.id);
        return pageRef.setRedirect(true);
    }

}