public with sharing class ContactMethods {
	
	public static void onInsertUpdate(List<Contact> newContacts) {
		
		List<Contact> filteredContacts= filterMultipleFlags(newContacts);
		
		//List<Contact> flaggedContacts = primaries(newContacts);
		if (filteredContacts.size() > 0) {
			
			System.debug('###################################### ENTER'); 
			setFlag(filteredContacts);
		} 
		
	} //END onInsertUpdate
	
	private static void setFlag(List<Contact> newContacts) {
		System.debug('####################################### setFLAG');
		//find all affected accounts
		Set<Id> affectedAccounts = getAccountSet(newContacts);
		
		List<Contact> oldAffectedContacts = new List<Contact>([SELECT Id, IsPrimarySalesContact__c FROM Contact WHERE AccountId IN: affectedAccounts]);
		
		List<Contact> updateContacts = new List<Contact>();
		for(Contact c : oldAffectedContacts) {
			if(c.IsPrimarySalesContact__c) {
				c.IsPrimarySalesContact__c = False;
				System.debug('*************************** SET FLAG TO FALSE');
				updateContacts.add(c);
			}
		}
		
		update updateContacts;
		
		
	} //END setFlag
	
	
	private static Set<Id> getAccountSet(List<Contact> newContacts) {
		Set<Id> affectedAccounts = new Set<Id>();
		for(Contact c : newContacts) {
			affectedAccounts.add(c.AccountId);
		}
		return affectedAccounts;
	}
	
	/*private static List<Contact> primaries(List<Contact> newContacts) {
		List<Contact> flaggedContacts = new List<Contact>();
		
		for(Contact c : newContacts) {
			if (c.isPrimarySalesContact__c) {
				flaggedContacts.add(c);
			}
		}
		
		return flaggedContacts;
		
	}//END primaries*/
	
	private static List<Contact> filterMultipleFlags(List<Contact> newContacts) {
		
		Set<Id> accounts = getAccountSet(newContacts);
		
		List<Contact> filteredContacts = new List<Contact>();
		//List<Contact> unflaggedContacts = new List<Contact>();
		for(Id currAcc : accounts) {
			Boolean flagged = false;
			for (Contact c : newContacts) {	
				if ((c.AccountId == currAcc) && (c.isPrimarySalesContact__c == True)) {
					if (!flagged) {
						filteredContacts.add(c);
						flagged = true;	
					} else{
						c.IsPrimarySalesContact__c = False;
						//unflaggedContacts.add(c);
					}		
				}
			}//END for contacts
				
		}//END for accounts
		
		return filteredContacts;
		
	} //END filterMultipleFlags
}