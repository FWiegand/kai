/*
 * @(#)SCutilTrackingNumber.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Used to validate the sparepart tracking number and determine
 * the error codes from SCConfOrderRepairCode
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCutilTrackingNumber
{
    /**
     * Store the tracking number
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public String trackingNumber { get; set; }
    
    /**
     * Empty default constructor
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCutilTrackingNumber()
    {
        
    }
    
    /**
     * Constructor
     * 
     * @param trackingNumber the tracking number to use
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCutilTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }

    /**
     * Check if the tracking number is valid or not
     *
     * @return <code>true</code> if the tracking number is valid
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public Boolean isValid()
    {
        Boolean valid = false;
        
        if (trackingNumber != null &&
            trackingNumber.length() == 30 && 
            trackingNumber.substring(28,29) == 'T' && 
            trackingNumber.substring(0,2) == '90')
        {
            valid = true;
        }
        
        return valid;
    }

    /**
     * Extracts the component number
     */
    public String getComponent()
    {
        if(isValid())
        {
            return trackingNumber.substring(6,16);
        }
        return '';
    }

    /**
     * Extracts the supplier number
     */
    public String getSupplier()
    {
        if(isValid())
        {
            return trackingNumber.substring(16,22);        
        }
        return '';
    }
    
    /**
     * Get the defaults that are set for this tracking number.
     *
     * @return defaults that are set for this tracking number
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public SCConfOrderRepairCode__c getValues()
    {
        try
        {
            String component;
            String supplier;
            
            if (isValid())
            {
                component = getComponent();
                supplier  = getSupplier();
            
                SCConfOrderRepairCode__c defaults = 
                    [select Component__c, Supplier__c, ErrorLocation1__c,
                            ErrorLocation2__c, ErrorActivityCode1__c
                       from SCConfOrderRepairCode__c 
                      where Component__c = :component and Supplier__c = :supplier limit 1];

                return defaults;
            }
        }
        catch (QueryException e)
        {
        }
        return null;
    }
}