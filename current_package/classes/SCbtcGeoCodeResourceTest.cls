/*
 * @(#)SCbtcGeoCodeResourceTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcGeoCodeResourceTest
{
    static testMethod void testDEPositive()
    {
        Test.StartTest();
        List<SCResourceAssignment__c> lr = [Select id from SCResourceAssignment__c where Country__c = 'DE' limit 1];
        if(lr.size() > 0)
        {
            Id resourceId = lr[0].Id;
            String strId = resourceId + '';
            SCbtcGeocodeResources.syncGeocodeResource(strId);
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeResources.syncGeocodeResources(idList);
        }
        try
        {
            SCbtcGeocodeResources.asyncGeocodeAll('TEST', 'DE', null,  1, 'trace');
        }
        catch (Exception e)
        {
        }
        SCbtcGeocodeResources.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }

    static testMethod void testDEPositive2()
    {
        Test.StartTest();
        List<SCResourceAssignment__c> lr = [Select id from SCResourceAssignment__c where Country__c = 'DE' limit 1];
        if(lr.size() > 0)
        {
            Id resourceId = lr[0].Id;
            String strId = resourceId + '';
            SCbtcGeocodeResources.asyncGeocodeResource(strId, 'trace');
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeResources.asyncGeocodeResources(null);
            SCbtcGeocodeResources.asyncGeocodeResources(idList);
        }
        SCbtcGeocodeResources.asyncGeocodeAll('ALL', 'DE', null,  1, 'trace');    
        Test.StopTest();
    }

    static testMethod void testNLPositive()
    {
        Test.StartTest();
        List<SCResourceAssignment__c> lr = [Select id from SCResourceAssignment__c where Country__c = 'NL' limit 1];
        if(lr.size() > 0)
        {
            Id resourceId = lr[0].Id;
            String strId = resourceId + '';
            SCbtcGeocodeResources.syncGeocodeResource(null);
            SCbtcGeocodeResources.syncGeocodeResource(strId);
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeResources.syncGeocodeResources(null); 
            SCbtcGeocodeResources.syncGeocodeResources(idList); 
        }
        if(lr.size() > 0)
        {
            Id resourceId = lr[0].Id;
            String strId = resourceId + '';
            SCbtcGeocodeResources.asyncGeocodeResource(strId, 'trace');
            List<String> idList = new List<String>();
            idList.add(strId);
            SCbtcGeocodeResources.asyncGeocodeResources(idList);
        }
        SCbtcGeocodeResources.asyncGeocodeAll('ALL', 'NL', null,  1, 'trace');    
        Test.StopTest();
    }

    static testMethod void testBasics()
    {
    	Boolean approximately = true;
        SCbtcGeocodeResources geoRes = new SCbtcGeocodeResources(null, 'ALLNEW', 'DE', '', 0, 'test', approximately);
        geoRes = new SCbtcGeocodeResources(null, 'SIM_ALL', 'DE', '', 0, 'test', approximately);
        geoRes = new SCbtcGeocodeResources(null, 'SIM_ALLNEW', 'DE', '', 0, 'test', approximately);
    }
}