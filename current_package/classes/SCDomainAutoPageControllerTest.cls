/*
 * @(#)SCDomainAutoController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author SS <sschrage@gms-online.de>
 */
@isTest
private class SCDomainAutoPageControllerTest
{
    static testMethod void testChangeLanguage()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assertEquals(null,con.LangDE());
        System.assertEquals('de',[SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey);
        
        System.assertEquals(null,con.LangENUS());
        System.assertEquals('en_US',[SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey);
        
        System.assertEquals(null,con.LangNLNL());
        System.assertEquals('nl_NL',[SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey);
        
        Test.stopTest();
    }
    
    
    static testMethod void testAdminAreaAsAdmin()
    {
        SCDomainAutoPageController.testIsAdmin = true;
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assertEquals(true,con.IsAdmin);
        System.assertEquals(false,con.ShowAdminArea);
        
        con.changeShowAdminArea();
        System.assertEquals(true,con.ShowAdminArea);
        
        con.changeShowAdminArea();
        System.assertEquals(false,con.ShowAdminArea);
        
        Test.stopTest();
    }
    
    
    static testMethod void testAdminAreaAsNonAdmin()
    {
        SCDomainAutoPageController.testIsAdmin = false;
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assertEquals(false,con.IsAdmin);
        System.assertEquals(false,con.ShowAdminArea);
        
        con.changeShowAdminArea();
        System.assertEquals(false,con.ShowAdminArea);
        
        con.changeShowAdminArea();
        System.assertEquals(false,con.ShowAdminArea);
        
        Test.stopTest();
    }
    
    
    static testMethod void testOverviewFutureJobs()
    {
        Database.insert(new List<SCJobControl__c>{
                    new SCJobControl__c(Activity__c = 'A', Name = 'A1', Type__c = 'SCDomainAutoController_A1'),
                    new SCJobControl__c(Activity__c = 'E', Name = 'E1', Type__c = 'SCDomainAutoController_E1'),
                    new SCJobControl__c(Activity__c = 'E', Name = 'E2', Type__c = 'SCDomainAutoController_E2'),
                    new SCJobControl__c(Activity__c = 'F', Name = 'F1', Type__c = 'SCDomainAutoController_F1'),
                    new SCJobControl__c(Activity__c = 'F', Name = 'F2', Type__c = 'SCDomainAutoController_F2'),
                    new SCJobControl__c(Activity__c = 'F', Name = 'F3', Type__c = 'SCDomainAutoController_F3'),
                    new SCJobControl__c(Activity__c = 'R', Name = 'R1', Type__c = 'SCDomainAutoController_R1'),
                    new SCJobControl__c(Activity__c = 'R', Name = 'R2', Type__c = 'SCDomainAutoController_R2'),
                    new SCJobControl__c(Activity__c = 'R', Name = 'R3', Type__c = 'SCDomainAutoController_R3'),
                    new SCJobControl__c(Activity__c = 'R', Name = 'R4', Type__c = 'SCDomainAutoController_R4'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'W1', Type__c = 'SCDomainAutoController_W1'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'W2', Type__c = 'SCDomainAutoController_W2'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'W3', Type__c = 'SCDomainAutoController_W3'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'W4', Type__c = 'SCDomainAutoController_W4'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'W5', Type__c = 'SCDomainAutoController_W5'),
                    new SCJobControl__c(Activity__c = 'A', Name = 'TA1', Type__c = 'TestA'),
                    new SCJobControl__c(Activity__c = 'E', Name = 'TE1', Type__c = 'TestE'),
                    new SCJobControl__c(Activity__c = 'F', Name = 'TF1', Type__c = 'TestF'),
                    new SCJobControl__c(Activity__c = 'R', Name = 'TR1', Type__c = 'TestR'),
                    new SCJobControl__c(Activity__c = 'W', Name = 'TW1', Type__c = 'TestW')
                });
        
        
        Test.startTest();
        
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assertEquals(true,con.FutureJobsRunning);
        System.assertEquals(1,con.getOverviewFutureJobsAbort());
        System.assertEquals(2,con.getOverviewFutureJobsError());
        System.assertEquals(3,con.getOverviewFutureJobsFinished());
        System.assertEquals(4,con.getOverviewFutureJobsRunning());
        System.assertEquals(5,con.getOverviewFutureJobsWaiting());
        
        con.UpdateOverviewFutureJobs();
        
        System.assertEquals(true,con.FutureJobsRunning);
        System.assertEquals(1,con.getOverviewFutureJobsAbort());
        System.assertEquals(2,con.getOverviewFutureJobsError());
        System.assertEquals(3,con.getOverviewFutureJobsFinished());
        System.assertEquals(4,con.getOverviewFutureJobsRunning());
        System.assertEquals(5,con.getOverviewFutureJobsWaiting());
        
        con.AbortFutureJobs();
        
        System.assertEquals(false,con.FutureJobsRunning);
        System.assertEquals(10,con.getOverviewFutureJobsAbort());
        System.assertEquals(2,con.getOverviewFutureJobsError());
        System.assertEquals(3,con.getOverviewFutureJobsFinished());
        System.assertEquals(0,con.getOverviewFutureJobsRunning());
        System.assertEquals(0,con.getOverviewFutureJobsWaiting());
        
        con.UpdateOverviewFutureJobs();
        
        System.assertEquals(false,con.FutureJobsRunning);
        System.assertEquals(10,con.getOverviewFutureJobsAbort());
        System.assertEquals(2,con.getOverviewFutureJobsError());
        System.assertEquals(3,con.getOverviewFutureJobsFinished());
        System.assertEquals(0,con.getOverviewFutureJobsRunning());
        System.assertEquals(0,con.getOverviewFutureJobsWaiting());
        
        
        Test.stopTest();
        
        
        System.assertEquals(5,[SELECT count() FROM SCJobControl__c WHERE (Activity__c = 'A' AND Type__c = 'TestA')
                                                                      OR (Activity__c = 'E' AND Type__c = 'TestE')
                                                                      OR (Activity__c = 'F' AND Type__c = 'TestF')
                                                                      OR (Activity__c = 'R' AND Type__c = 'TestR')
                                                                      OR (Activity__c = 'W' AND Type__c = 'TestW')]);
        
        System.assertEquals(1,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'A' AND Type__c LIKE 'SCDomainAutoController_A%']);
        System.assertEquals(2,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'E' AND Type__c LIKE 'SCDomainAutoController_E%']);
        System.assertEquals(3,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'F' AND Type__c LIKE 'SCDomainAutoController_F%']);
        System.assertEquals(4,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'A' AND Type__c LIKE 'SCDomainAutoController_R%']);
        System.assertEquals(5,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'A' AND Type__c LIKE 'SCDomainAutoController_W%']);
        
        System.assertEquals(10,[SELECT count() FROM SCJobControl__c WHERE Activity__c = 'A' AND Type__c LIKE 'SCDomainAutoController_%']);
        System.assertEquals(5,[SELECT count() FROM SCJobControl__c WHERE Activity__c != 'A' AND Type__c LIKE 'SCDomainAutoController_%']);
    }
    
    
    
    static testMethod void testObjectDomAutoRefs()
    {
        List<SCDomainRef__c> listDomRef = new List<SCDomainRef__c>();
        for (Integer i=0; i<100; i++)
        {
            listDomRef.add(new SCDomainRef__c(Name = 'Test_' + i.format(), ID__c = null));
        }
        Database.insert(listDomRef);
        
        SCDomainRef__c domRefTest = new SCDomainRef__c(Name = 'Test', ID__c = null);
        Database.insert(domRefTest);
        
        List<SCDomainAutoRef__c> listDomAutoRef = new List<SCDomainAutoRef__c>();
        for (SCDomainRef__c domRef : listDomRef)
        {
            listDomAutoRef.add(new SCDomainAutoRef__c(DomainRef__c            = domRef.Id,
                                                      ID2__c                  = domRef.Name,
                                                      isSelected__c           = false,
                                                      ObjectName__c           = domRef.Name,
                                                      SFObjectName__c         = domRef.Name,
                                                      Type__c                 = 'Object'));
        }
        listDomAutoRef.add(new SCDomainAutoRef__c(DomainRef__c            = domRefTest.Id,
                                                  ID2__c                  = null,
                                                  isSelected__c           = false,
                                                  ObjectName__c           = 'test',
                                                  SFObjectName__c         = null,
                                                  Type__c                 = 'Object'));
        Database.insert(listDomAutoRef);
        
        
        Test.startTest();
        
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assert(con.ListSelectObjOptionsColumn1.size() > 30, 'ListSelectObjOptionsColumn1.size() has to be over 30');
        System.assert(con.ListSelectObjOptionsColumn2.size() > 30, 'ListSelectObjOptionsColumn2.size() has to be over 30');
        System.assert(con.ListSelectObjOptionsColumn3.size() > 30, 'ListSelectObjOptionsColumn3.size() has to be over 30');
        
        con.ListSelectObjColumn1 = new List<String>();
        con.ListSelectObjColumn2 = new List<String>();
        con.ListSelectObjColumn3 = new List<String>();
        
        for (SelectOption opt : con.ListSelectObjOptionsColumn1) { con.ListSelectObjColumn1.add(opt.getValue()); }
        for (SelectOption opt : con.ListSelectObjOptionsColumn2) { con.ListSelectObjColumn2.add(opt.getValue()); }
        for (SelectOption opt : con.ListSelectObjOptionsColumn3) { con.ListSelectObjColumn3.add(opt.getValue()); }
        
        con.UpdateOverviewSelectObj();
        
        System.assertEquals(0,con.ListSelectObjColumn1.size());
        System.assertEquals(0,con.ListSelectObjColumn2.size());
        System.assertEquals(0,con.ListSelectObjColumn3.size());
        
        con.SaveSelectObj();
        
        System.assertEquals(0,con.ListSelectObjColumn1.size());
        System.assertEquals(0,con.ListSelectObjColumn2.size());
        System.assertEquals(0,con.ListSelectObjColumn3.size());
        
        for (SelectOption opt : con.ListSelectObjOptionsColumn1) { con.ListSelectObjColumn1.add(opt.getValue()); }
        for (SelectOption opt : con.ListSelectObjOptionsColumn2) { con.ListSelectObjColumn2.add(opt.getValue()); }
        for (SelectOption opt : con.ListSelectObjOptionsColumn3) { con.ListSelectObjColumn3.add(opt.getValue()); }
        
        con.SaveSelectObj();
        
        System.assertNotEquals(0,con.ListSelectObjColumn1.size());
        System.assertNotEquals(0,con.ListSelectObjColumn2.size());
        System.assertNotEquals(0,con.ListSelectObjColumn3.size());
        
        con.OverviewSelectObjUncheckAll();
        
        System.assertEquals(0,con.ListSelectObjColumn1.size());
        System.assertEquals(0,con.ListSelectObjColumn2.size());
        System.assertEquals(0,con.ListSelectObjColumn3.size());
        
        con.OverviewSelectObjCheckAll();
        
        System.assertNotEquals(0,con.ListSelectObjColumn1.size());
        System.assertNotEquals(0,con.ListSelectObjColumn2.size());
        System.assertNotEquals(0,con.ListSelectObjColumn3.size());
        
        
        Test.stopTest();
        
        
        System.assertEquals(100,[SELECT count() FROM SCDomainAutoRef__c WHERE isSelected__c = true AND SFObjectName__c != null]);
        System.assertEquals(1,[SELECT count() FROM SCDomainAutoRef__c WHERE isSelected__c = false]);
    }
    
    
    
    
    
    static testMethod void testStartControlDomainAutoRefsForObjects()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDomainAutoRefsForObjects();
        
        Test.stopTest();
        
        System.assertEquals('1',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlDomainValuesForObjectsZC()
    {
        SCDomainRef__c domRefObjZ = new SCDomainRef__c(Name = 'Objects__z', ID__c = 1);
        Database.insert(new List<SCDomainRef__c>{domRefObjZ});
        
        SCDomainAutoRef__c domAutoRefObjZ = new SCDomainAutoRef__c(DomainRef__c            = domRefObjZ.Id,
                                                                   Head__c                 = null,
                                                                   ID2__c                  = 'objects__z',
                                                                   isDependentPicklist__c  = false,
                                                                   isRestrictedPicklist__c = false,
                                                                   isSelected__c           = true,
                                                                   ObjectName__c           = 'objects__z',
                                                                   FieldName__c            = null,
                                                                   SFObjectName__c         = null,
                                                                   SFFieldName__c          = null,
                                                                   Type__c                 = 'Global');
        Database.insert(new List<SCDomainAutoRef__c>{domAutoRefObjZ});
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDomainValuesForObjectsZ();
        
        Test.stopTest();
        
        System.assertEquals('2',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlDomainValuesForObjectsZE()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDomainValuesForObjectsZ();
        
        Test.stopTest();
        
        System.assertEquals('2',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlFieldsOfTheObjects()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlFieldsOfTheObjects();
        
        Test.stopTest();
        
        System.assertEquals('3',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlPicklistFields()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlPicklistFields();
        
        Test.stopTest();
        
        System.assertEquals('4',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlDescriptionsObjectAndFields()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDescriptionsObjectAndFields();
        
        Test.stopTest();
        
        System.assertEquals('5',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlDescriptionsPicklistFields()
    {
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDescriptionsPicklistFields();
        
        Test.stopTest();
        
        System.assertEquals('6',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlDomainActivity()
    {
        SCDomainRef__c domRefObjZ = new SCDomainRef__c(Name = 'Objects__z', ID__c = 1);
        Database.insert(new List<SCDomainRef__c>{domRefObjZ});
        
        SCDomainAutoRef__c domAutoRefObjZ = new SCDomainAutoRef__c(DomainRef__c            = domRefObjZ.Id,
                                                                   Head__c                 = null,
                                                                   ID2__c                  = 'objects__z',
                                                                   isDependentPicklist__c  = false,
                                                                   isRestrictedPicklist__c = false,
                                                                   isSelected__c           = true,
                                                                   ObjectName__c           = 'objects__z',
                                                                   FieldName__c            = null,
                                                                   SFObjectName__c         = null,
                                                                   SFFieldName__c          = null,
                                                                   Type__c                 = 'Global');
        Database.insert(new List<SCDomainAutoRef__c>{domAutoRefObjZ});
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlDomainActivity();
        
        Test.stopTest();
        
        System.assertEquals('7',con.LastStep);
    }
    
    
    
    static testMethod void testStartControlAutoDomainSyntax()
    {
        SCDomainAutoPageController.testIsAdmin = true;
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startControlAutoDomainSyntax();
        
        Test.stopTest();
        
        System.assertEquals('',con.LastStep);
    }
    
    
    
    static testMethod void testStartDeleteDomValuesWithoutDesciptions()
    {
        SCDomainAutoPageController.testIsAdmin = true;
        
        SCDomainRef__c domRefObj = new SCDomainRef__c(Name = 'objects__z', ID__c = null);
        Database.insert(new List<SCDomainRef__c>{domRefObj});
        
        SCDomainAutoRef__c domAutoRefObj = new SCDomainAutoRef__c(DomainRef__c            = domRefObj.Id,
                                                                  Head__c                 = null,
                                                                  ID2__c                  = 'objects__z',
                                                                  isDependentPicklist__c  = false,
                                                                  isRestrictedPicklist__c = false,
                                                                  isSelected__c           = true,
                                                                  ObjectName__c           = 'objects__z',
                                                                  FieldName__c            = null,
                                                                  SFObjectName__c         = null,
                                                                  SFFieldName__c          = null,
                                                                  Type__c                 = 'Global');
        Database.insert(new List<SCDomainAutoRef__c>{domAutoRefObj});
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        con.startDeleteDomValuesWithoutDesciptions();
        
        Test.stopTest();
        
        System.assertEquals(1, [SELECT count() FROM SCDomainRef__c]);
    }
    
    
    
    static testMethod void testGetShowCheckedImgStep()
    {
        SCDomainRef__c domRefObj = new SCDomainRef__c(Name = 'objects__z', ID__c = null);
        Database.insert(new List<SCDomainRef__c>{domRefObj});
        
        SCDomainAutoRef__c domAutoRefObj = new SCDomainAutoRef__c(DomainRef__c            = domRefObj.Id,
                                                                  Head__c                 = null,
                                                                  ID2__c                  = 'objects__z',
                                                                  isDependentPicklist__c  = false,
                                                                  isRestrictedPicklist__c = false,
                                                                  isSelected__c           = true,
                                                                  ObjectName__c           = 'objects__z',
                                                                  FieldName__c            = null,
                                                                  SFObjectName__c         = null,
                                                                  SFFieldName__c          = null,
                                                                  Type__c                 = 'Global');
        Database.insert(new List<SCDomainAutoRef__c>{domAutoRefObj});
        
        Test.startTest();
        
        SCDomainAutoPageController con = new SCDomainAutoPageController();
        
        System.assertEquals(false,con.getShowCheckedImgStep1());
        System.assertEquals(false,con.getShowCheckedImgStep2());
        System.assertEquals(false,con.getShowCheckedImgStep3());
        System.assertEquals(false,con.getShowCheckedImgStep4());
        System.assertEquals(false,con.getShowCheckedImgStep5());
        System.assertEquals(false,con.getShowCheckedImgStep6());
        System.assertEquals(false,con.getShowCheckedImgStep7());
        
        Test.stopTest();
        
        System.assertEquals(1, [SELECT count() FROM SCDomainRef__c]);
    }
}