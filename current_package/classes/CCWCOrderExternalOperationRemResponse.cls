/*
 * @(#)CCWCOrderExternalOperationRemResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCOrderExternalOperationRemResponse
{
    public static void processOrderExternalOperationRemoveResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        String interfaceName = 'SAP_ORDER_EXTOP_REM';
        String interfaceHandler = 'CCWCOrderExternalOperationRemoveResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
        // Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
        responseInterfaceLog.Direction__c = direction;            
        responseInterfaceLog.MessageID__c = messageID;            
        responseInterfaceLog.ReferenceID__c = referenceID;            
        responseInterfaceLog.ResultCode__c = resultCode;            
        responseInterfaceLog.ResultInfo__c = resultInfo;
        responseInterfaceLog.Data__c = data;
        debug('interfaceLogResponse2: ' + responseInterfaceLog);
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);
        
        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            // find the order external assignment
            step = 'find an order external assignment'; 
            SCOrderExternalAssignment__c oea = readOrderExternalAssignment(referenceItem.ExternalID,responseInterfaceLog);
            debug('order external assignment: ' + oea);
            responseInterfaceLog.Order__c = oea.Order__r.ID;
            responseInterfaceLog.ReferenceID__c = oea.Order__r.ID;            

            // find the interfacelog
            step = 'find a request interface';
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.            
            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, oea.Order__r.ID, referenceItem.ExternalID, 'SAP_ORDER_EXTOP_REM');
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            // update order
            step = 'update the order external assignment';
            oea.ERPStatusRem__c = CCWSGenericResponse.getResultStatus(logItemArr);
            oea.ERPResultDate__c = DateTime.now();
			// PMS 36793/GMS: CCWCOrderExternalOperationRem - no OperationNumber          
			oea.Status__c = 'deleted';
			
            update oea;
            debug('order external assignment after update: ' + oea);

            step = 'update order status';
            updateOrders(oea.Order__r.ID, oea.ERPStatusRem__c,responseInterfaceLog.Idoc__c);          
            step = 'write a response interface log';
            // write response interface log
            responseInterfaceLog.Count__c = 1;           

            debug('responseInterfaceLog: ' + responseInterfaceLog);
            // update request interface log
            step = 'update the request interface log';
            if(responseInterfaceLog != null)
            {
                //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.
                if(requestInterfaceLog != null)
                {                
	                debug('change a request interface log');
	                requestInterfaceLog.Response__c = responseInterfaceLog.Id;
	                requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
	                
	                debug('requestInterfaceLog: ' + requestInterfaceLog);
	                
	                interfaceLogList.add(requestInterfaceLog);
                }
                
                debug('interfaceLogList.size: ' + interfaceLogList.size());
                debug('interfaceLogList: ' + interfaceLogList);
                for(SCInterfaceLog__c il: interfaceLogList)
                {
                    debug('*ID: ' + il.id + ', MessageID: ' + il.MessageID__c + ', Order__c: ' + il.Order__c
                    + ', Response: ' + il.Response__c);
                } 
            }
            else
            {
                throw new SCfwException('A response interface log object could not be created for messageID: ' + messageID + 
                                        ', order id: ' + oea.Order__r.ID + ', order order external assignment name: ' + headExternalID); 
            }    
            
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
    }//processOrderExternalOperationAddResponse

    public static SCOrderExternalAssignment__c readOrderExternalAssignment(String headExternalID, SCInterfaceLog__c responseInterfaceLog)
    {
        SCOrderExternalAssignment__c retValue = null;
        List<SCOrderExternalAssignment__c> ol = [select ID, name, Order__r.ID, Order__r.Name, 
        	Order__r.ERPOrderNo__c, ERPStatusRem__c, Status__c
        	 from SCOrderExternalAssignment__c where name = : headExternalID 
             and ERPStatusRem__c IN ('error','pending','none')];
                      
                
                
        if(ol.size() == 0)
        {
            throw new SCfwException('The Order: ' + headExternalId + ', has no SCOrderExternalAssignment__c.');
        }
                  
                  
        // Test
        //SCfwInterfaceRequestPendingException er = new SCfwInterfaceRequestPendingException('The Order: ' + headExternalId + ', has ERPStatusRem__c \'none\'.');
        //er.order = ol[0].Order__r;
        //throw er;   
        //Test
        //SCfwException er = new SCfwException('Test SCfwException');
        //throw er;                  
                            
        if(ol[0].ERPStatusRem__c == 'error' || ol[0].ERPStatusRem__c == 'pending' )
        {
            retValue = ol[0];
            debug('read order external assignment: ' + ol[0]);
        }
        else if(ol[0].ERPStatusRem__c == 'ok')
        {
            retValue = ol[0];
            debug('The Order: ' + headExternalId + ', has ERPStatusRem__c \'ok\'.');
        }
        else if(ol[0].ERPStatusRem__c == 'none')
        {
            SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The Order: ' + headExternalId + ', has ERPStatusRem__c \'none\'.');
            e.order = ol[0].Order__r;
            throw e;
        }
        else
        {
        	SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The Order: ' + headExternalId + ', has ERPStatusRem__c '
        		+ ol[0].ERPStatusRem__c + '.');
            e.order = ol[0].Order__r;
            throw e;
        }
        
        return retValue;
    }

    public static void updateOrders(ID orderID, String status, String iDoc)
    {
        List<SCOrder__c> orderList = [Select ID from SCOrder__c where id = :orderID];
        if(orderList != null && orderList.size() > 0)
        {
            for(SCOrder__c o: orderList)
            {
                if(status != null)
                {
                    o.ERPStatusExternalAssignmentRem__c = status;
                    o.ERPResultDate__c = DateTime.now();
                    o.IDocOrderExOpRem__c = iDoc;
                }       
            }
        }
        update orderList;
    }

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

   
}