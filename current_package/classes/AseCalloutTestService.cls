/**
 * AseCalloutTestService.cls    mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutTestService includes methods for testing the ASEWebserivce
 * and the communication. At the moment there is a function to get
 * all data from the ASEWeb for a specified dataType and print this
 * data on the DebugConsole. 
 * 
 * @author mt
 * @version $Revision$, $Date$
 */
@isTest
private class AseCalloutTestService 
{
       
    /**
     * get Data from the ASEWeb and print them on the DebugConsole
     * 
     * @param tenant the tenant
     * @param intType the dataType as Integer 
     * @param strKey the PrimaryKey for the record which should be selected 
     */
    public static void testGet(String tenant, Integer intType, String strKey) 
    {
        System.debug('#### Get - Start ####');
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();

        AseService.aseParaSelectType[] paraSelects = new AseService.aseParaSelectType[0];
        AseService.aseParaSelectType paraSelect =  new AseService.aseParaSelectType();
        
        paraSelect.type_x = AseCalloutUtils.getDataTypeAsString(intType);
        paraSelect.keys = new String[0];
        paraSelect.keys.add(strKey);
        
        paraSelects.add(paraSelect);
        
        System.debug('#### printDebug for tenant: '+ tenant +' ####');
        System.debug('#### printDebug for type: '+ paraSelect.type_x +' ####');
        System.debug('#### printDebug for key: '+ strKey +' ####');
        
        AseService.aseDataType[] dataTypes = testGetDataFromWebservice(tenant,paraSelects);
        testGetHandleResponseData(dataTypes);

        System.debug('#### Get - End ####');
    }
    
    /**
     * this method starts the callout to the ASEWeb
     * 
     * @param tenant the tenant
     * @param paraSelects the parameters for the data which should be selected 
     */
    private static AseService.aseDataType[] testGetDataFromWebservice(String tenant, AseService.aseParaSelectType[] paraSelects)
    {
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
        return aseService.get(tenant,paraSelects);
    } 
    
    
    /**
     * this method get all data from ASEWeb and prints them on the DebugConsole
     * 
     * @param tenant the tenant
     * @param intType the Type for which all Data should be selected 
     */
    public static void testGetAll(String tenant, Integer intType)
    {
        Integer[] intTypes = new Integer[1];
        intTypes[0] = intType;
        testGetAll(tenant,intTypes);
    } 

    /**
     * this method get all data from ASEWeb and prints them on the DebugConsole
     * 
     * @param tenant the tenant
     * @param intTypees the Types for which all Data should be selected 
     */
    public static void testGetAll(String tenant, Integer[] intTypes) 
    {
        System.debug('#### GetAll - Start ####');
        
        String[] types = new String[0];
        for (Integer intType : intTypes) 
        {
            types.add(AseCalloutUtils.getDataTypeAsString(intType));
        }

        System.debug('#### printDebug for tenant: '+ tenant +' ####');
        System.debug('#### printDebug for types: '+ types +' ####');
        
        AseService.aseDataType[] dataTypes = testGetAllDataFromWebservice(tenant,types);
        testGetHandleResponseData(dataTypes);
            
        System.debug('#### GetAll - End ####');    
    }
    
    /**
     * this method get all data from ASEWeb and prints them on the DebugConsole
     * 
     * @param tenant the tenant
     * @param types the Types for which all Data should be selected as String 
     */
    private static AseService.aseDataType[] testGetAllDataFromWebservice(String tenant, String[] types)
    {
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
        return aseService.getAll(tenant,types);
    } 
    
    /**
     * this method handles the Data which the ASEWeb returns. That means
     * that the whole data is printed on the DebugConsole
     * 
     * @param dataTypes the dataTypes which should be printed
     */
    private static void testGetHandleResponseData(AseService.aseDataType[] dataTypes)
    {
        if (dataTypes == null) 
        {
            System.debug('#### no data returned ####');
            return;
        }

        System.debug('#### dataType length: ' + dataTypes.size() + ' ####');

        for (integer a=0;a<dataTypes.size();a++) 
        {

                AseService.aseDataType dataType = dataTypes[a];
                System.debug('#### new Type: ' + dataType.type_x + ' ####');

                AseService.aseDataEntry[] dataEntries = dataType.dataEntries;
                System.debug('#### dataEntries length: ' + dataEntries.size() + ' ####');

                for (integer b=0;b<dataEntries.size();b++) 
                {
                        System.debug('#### new Dataset ####');
                        AseService.aseKeyValueType[] keyValues = dataEntries[b].keyValues;

                        System.debug('#### keyValues length: '+keyValues.size()+' ####');
                        
                        for (integer c=0;c<keyValues.size();c++) 
                        {
                                System.debug(keyValues[c].key+' : '+keyValues[c].value);
                        }
                }
        }
    }
             
    
    //tests for private methods
    public static void testGetDataFromWebservice()
    {
        AseService.aseParaSelectType[] paraSelects = new AseService.aseParaSelectType[0];
        AseService.aseParaSelectType paraSelect =  new AseService.aseParaSelectType();
        
        paraSelect.type_x = AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR;
        paraSelect.keys = new String[0];
        paraSelect.keys.add('key1');
        
        paraSelects.add(paraSelect);

        testGetDataFromWebservice(AseCalloutConstants.TEST_TENANT, paraSelects);
    } 
    
    public static void testGetAllDataFromWebservice()
    {
        String[] types = new String[1];
        types[0] = AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR;
        testGetAllDataFromWebservice(AseCalloutConstants.TEST_TENANT,types);
    } 
    
    public static void testGetHandleResponseData()
    {
    
    
        //first test with null
        AseCalloutTestService.testGetHandleResponseData(null);
        
        //second test with dummy data
        AseService.aseDataType[] dataTypes = new AseService.aseDataType[0];
        AseService.aseDataType dataType = new AseService.aseDataType();
        AseService.aseDataEntry[] dataEntries = new AseService.aseDataEntry[0];
        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType[] keyValues = new AseService.aseKeyValueType[0];
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        keyValue.key='key1';
        keyValue.value='val1';
        keyValues.add(keyValue);
        dataEntry.keyValues = keyValues;
        dataEntries.add(dataEntry);
        dataType.type_x=AseCalloutConstants.ASE_TYPE_UNKNOWN_STR;
        dataType.dataEntries = dataEntries;
        dataTypes.add(dataType);

        AseCalloutTestService.testGetHandleResponseData(dataTypes);
    }    
}