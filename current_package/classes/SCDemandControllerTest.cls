/*
 * @(#)SCDemandControllerTest.cls SCCloud    hs 07.04.2010
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @author DH <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCDemandControllerTest 
{
    /**
     * DemandController under test
     */
    private static SCDemandController demandController;
    
    /*
    static testMethod void testDemandController1()
    {
       /**
        * Test data from SCHelperTestClass
        *
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser
        demandController = new SCDemandController(SCHelperTestClass.orderItem.Id, sessionID);
        //demandController.init(SCHelperTestClass.orderItem.Id);
        //demandController.init(SCHelperTestClass.orderItems.get(0).Id);

        System.assert(demandController.getValue('orderid') == SCHelperTestClass.order.Id);
        System.assert(demandController.getValue('custprio') == SCHelperTestClass.order.CustomerPriority__c);
    }
    
    static testMethod void testDemandController2()
    {
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser
        demandController = new SCDemandController(SCHelperTestClass.orderItem.Id, sessionID);
        demandController.init(SCHelperTestClass.orderItem.Id);
        System.assert(demandController.getValue('orderid') == SCHelperTestClass.order.Id);
    }
    
    static testMethod void testDemandController3()
    {
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser
        demandController = new SCDemandController(SCHelperTestClass.orderItem.Id, sessionID);
        demandController.init(SCHelperTestClass.order, SCHelperTestClass.orderItem);
        System.assert(demandController.getValue('orderid') == SCHelperTestClass.order.Id);
    }
    */
    
    static testMethod void testDemandControllerStandard()
    {   
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.orderItem);
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser  
        demandController = new SCDemandController(controller, sessionID, true);
        System.assert(demandController.getValue('itemid') == SCHelperTestClass.orderItem.Id);
        System.assert(demandController.getValue('objids').length() > 0);
    }
    
    static testMethod void testDemandControllerPage()
    {   
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        // test is not completely here       

        SCOrderPageController pageController = new SCOrderPageController(SCHelperTestClass.account.Id);
        //pageController.assignedOrders.add(SCHelperTestClass.orderItem);
        //demandController = new SCDemandController(pageController, true);  
    }

    static testMethod void testDemandController2()
    {   
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        
        SCDemandController contr = new SCDemandController(String.valueof(SCHelperTestClass.order.id), '1234565432345654');
    }
    
    static testMethod void testDemandControllerReinit()
    {   
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.orderItem);
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser  
        demandController = new SCDemandController(controller, sessionID, true);
        System.assert(demandController.getValue('itemid') == SCHelperTestClass.orderItem.Id);
        
        AvsAddress locaddr = new AvsAddress();
        locaddr.country = 'DE';    
        
        demandController.reinit(locaddr, -1); 

        // some dummy tests
        demandController.initOrderOnly(null);
        demandController.initOrderOnly('invalid-id');
        
        demandController.init(SCHelperTestClass.order, SCHelperTestClass.orderItem);
    }
    
    static testMethod void testOrderPageController()
    {   
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createOrderItem(true);
        SCHelperTestClass.createOrderRoles(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true);  
        SCHelperTestClass2.createQualificationOrder(SCHelperTestClass.order.Id, SCHelperTestClass.orderItem.Id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(SCHelperTestClass.orderItem);
        
        String sessionID = '3242gfd43241212'; // free defined, comes from browser
        
        SCOrderPageController ordcon = new SCOrderPageController(SCHelperTestClass.account.id, SCHelperTestClass.order.id); 

        demandController = new SCDemandController(ordcon, sessionID, false, true);
        System.assert(demandController.getValue('itemid') == SCHelperTestClass.orderItem.Id);
    }
}