/*
 * @(#)CCWCMaterialMovementCreateResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCMaterialMovementCreateResponse  
{

    public static void processMaterialMovementCreateResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  List<CCWSGenericResponse.ReferenceItem> referenceList, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage)
    {
	        SCInterfaceLog__c responseInterfaceLog = null;
	        processMaterialMovementCreateResponse(messageID, requestMessageID, headExternalID, 
	                                                  referenceList, 
	                                                  MaximumLogItemSeverityCode, 
	                                                  logItemArr, 
	                                                  interfaceLogList,
	                                                  GenericServiceResponseMessage,
	                                                  responseInterfaceLog);
                                           
    }
    

    
    
    public static void processMaterialMovementCreateResponse(String messageID, String requestMessageID, String headExternalID, 
                                                  List<CCWSGenericResponse.ReferenceItem> referenceList, 
                                                  String MaximumLogItemSeverityCode, 
                                                  CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        String interfaceName = 'SAP_MATERIAL_MOVEMENT';
        String interfaceHandler = 'CCWCMaterialMovementCreateResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceList: ' + referenceList + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
        // Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
        responseInterfaceLog.Direction__c = direction;            
        responseInterfaceLog.MessageID__c = messageID;            
        responseInterfaceLog.ReferenceID__c = referenceID;            
        responseInterfaceLog.ResultCode__c = resultCode;            
        responseInterfaceLog.ResultInfo__c = resultInfo;
        responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);

        String step = '';
        Savepoint sp = Database.setSavepoint();
        ID orderIdHead = null;
        try
        {
            step = 'find the movements'; 
            List<SCMaterialMovement__c> mml = readMovements(referenceList,responseInterfaceLog);
            Map<String, List<CCWSGenericResponse.LogItemClass>> mmtoLogItemMap = createMovementMap(mml);
            mmtoLogItemMap = createMMLogItemMap(mmtoLogItemMap, logItemArr);
            
            
            debug('mml: ' + mml);
            
            // find the interfacelog
            step = 'find a request interface';
            List<SCInterfaceLog__c> requestInterfaceLogList = CCWSGenericResponse.readOutoingInterfaceLogs(requestMessageID, null, null, 'SAP_MATERIAL_MOVEMENT');
            //GMSGB 02.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            debug('requestInterfaceLogList: ' + requestInterfaceLogList); 
            if(requestInterfaceLogList != null)
            {
            	orderIdHead = requestInterfaceLogList[0].Order__c; 
            }
            else
            {
            	//###GMSGB 02.09.13 How do we find the order, if we do not have the ID?
            	// Is this correct?
            	orderIdHead = mml[0].Order__c;
            	
            }
            // update material movements
            List<String> listIDType = new List<String>();
            listIDType.add('MaterialDocument');
            listIDType.add('IDoc number');
            listIDType.add('CreatedFromLogItem');
            listIDType.add('Number of Material Document');

            List<ID> orderIDList = new List<ID>();
            Map<ID, String> mapOrderIdToStatus = new Map<ID, String>();
            


            for(SCMaterialMovement__c mm: mml)
            {
                
                CCWSGenericResponse.ReferenceItem referenceItem = CCWSGenericResponse.getReferenceItem(referenceList, listIDType, mm.Name);
                debug('referenceItemLoop: ' + referenceItem);
                if(referenceItem != null)
                {
                    //CCWSGenericResponse.LogItemClass[] loopLogItemArr = CCWSGenericResponse.getLogItems(mm.Name, logItemArr);
                    mm.ERPStatus__c = CCWSGenericResponse.getResultStatus(mmtoLogItemMap.get(mm.Name));
                    mm.ERPResultInfo__c = CCWSGenericResponse.getResultInfo(messageID, headExternalID, referenceItem, 
                                          MaximumLogItemSeverityCode, mmtoLogItemMap.get(mm.Name), 'HeadMaterialMovement');
                    mm.ERPResultDate__c = DateTime.now();
                    // write response interface log
                    responseInterfaceLog.Count__c++;
                    if(mm.Order__c != null)
                    {
                        orderIDList.add(mm.Order__c);
                        String prevStatus = mapOrderIdToStatus.get(mm.Order__c);
                        if(prevStatus == null || prevStatus != null && prevStatus != 'error')
                        {
                            mapOrderIdToStatus.put(mm.Order__c, mm.ERPStatus__c);
                        }   
                    }
                }    
            }    
            update mml;
            debug('mml after update: ' + mml);          

            step = 'update orders';
            updateOrders(orderIDList, mapOrderIdToStatus);

            step = 'write a response interface log';

            debug('responseInterfaceLog: ' + responseInterfaceLog);
            // update request interface log
            step = 'update the request interface log';
            
            if(responseInterfaceLog != null)
            {
                for(SCInterfaceLog__c requestInterfaceLog: requestInterfaceLogList)
                {
                    responseInterfaceLog.order__c = requestInterfaceLog.order__c;
                    requestInterfaceLog.Response__c = responseInterfaceLog.Id;
                    requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
                    interfaceLogList.add(requestInterfaceLog);
                }   
            }
            else
            {
            	throw new SCfwException('A response interface log object could not be crated for messageID: ' + messageID); 
            }
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
		
        finally
        {
            // Only for movements with an order
            if(orderIdHead != null)
            {
                CCWCOrderCloseEx.processNext(orderIdHead);
            }               
        }  
    }//processCreateOrderResponse


   /*
    * Create a map of material movements to the corresponding logItems 
    */        
	public static Map<String, List<CCWSGenericResponse.LogItemClass>> createMMLogItemMap(Map<String, List<CCWSGenericResponse.LogItemClass>> mmtoLogItemMap, CCWSGenericResponse.LogItemClass[] logItemArr)
	{
		
		if(logItemArr != null)
        {
            for(CCWSGenericResponse.LogItemClass lic: logItemArr)
            {
                if(lic != null && lic.ExternalID != null && lic.ExternalID != '' )
                {

                    lic.ExternalID = lic.ExternalID.trim();
                    if(lic.SeverityCode != null)
                    {
                        lic.SeverityCode = lic.SeverityCode.trim();
                    }   

                    mmtoLogItemMap.get(lic.ExternalID).add(lic);
                }            
            }
        }
        return mmtoLogItemMap;
	}


    public static List<SCMaterialMovement__c> readMovements(List<CCWSGenericResponse.ReferenceItem> referenceList,
    														SCInterfaceLog__c responseInterfaceLog)
    {
        List<String> mmNameList = new List<String>();
        for(CCWSGenericResponse.ReferenceItem ri: referenceList)
        {
            if(ri.externalID != null)
            {
                mmNameList.add(ri.externalID);
            }   
        }
        debug('mmNameList:' + mmNameList);
        
        // GMSGB 14.08.2013 PMS 35668
	    // Aim: Continue the computation, even when the status is 'none' or 'ok'
	    // It seems to be possible that the return of the SAP call can be faster 
	    // than the commit of the changes by the calling future call. 
	    
        List<SCMaterialMovement__c> items = [select ID, Name, Order__c, Order__r.ID, ERPStatus__c from SCMaterialMovement__c where name in : mmNameList]; 
        List<SCMaterialMovement__c> retValue = new List<SCMaterialMovement__c>();
        // old and (ERPStatus__c = 'error' or ERPStatus__c = 'pending')];
            
        
        if(items.size() == 0)
        {
            throw new SCfwException('There no material movements for names: ' + mmNameList);
        }
        
        // Test
        //SCfwException er = new SCfwException('Test SCfwException');
        //throw er;
       
       
        for (SCMaterialMovement__c mm : items)
        {
        	// Test
        	//SCfwInterfaceRequestPendingException er = new SCfwInterfaceRequestPendingException('The  request status is still \'none\'!');
        	//er.materialMovement = mm;
        	//er.order 			= mm.Order__r;
        	//throw er;
        
        	
        	if(mm.ERPStatus__c == 'error' || mm.ERPStatus__c == 'pending')
        	{
        		retValue.add(mm);
        	}
        	// test if any status is none
        	else if(mm.ERPStatus__c == 'none')
        	{
        		// there are uncommitted MaterialMovements for the SAP request
        		// abort the current computation and add it to the InterfraceQueue
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  request status is still \'none\'!');
        		e.materialMovement = mm;
        		e.order = mm.Order__r;
        		throw e;
        	}
        	
        	// test if any status is none
        	else if(mm.ERPStatus__c == 'ok')
        	{
       			//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  material movements for names: ' + mm.Name + 'is already confirmed by SAP!');
        		//e.materialMovement = mm;
        		//e.order = mm.Order__r;
        		//throw e;
        		debug('The  material movements for names: ' + mm.Name + 'is already confirmed by SAP!');
        		retValue.add(mm);
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The  material movements for names: ' + mm.Name + 'is has ERPStatus__c ' 
        			+ mm.ERPStatus__c);
        		e.materialMovement = mm;
        		e.order = mm.Order__r;
        		throw e;	
        	}
        }        
           
        return retValue;
    }
    
    public static Map<String, List<CCWSGenericResponse.LogItemClass>> createMovementMap(List<SCMaterialMovement__c> mml)
    {
    	Map<String, List<CCWSGenericResponse.LogItemClass>> result = new Map<String, List<CCWSGenericResponse.LogItemClass>> ();
    	for(SCMaterialMovement__c mm : mml)
    	{
    		result.put(mm.name,new List<CCWSGenericResponse.LogItemClass>());
    	}
    	return result;
    }

    public static void updateOrders(List<ID> orderIDList, Map<ID, String> mapIDToStatus)
    {
        List<SCOrder__c> orderList = [Select ID from SCOrder__c where id in :orderIDList];
        if(orderList != null && orderList.size() > 0)
        {
            for(SCOrder__c o: orderList)
            {
                String status = mapIdToStatus.get(o.ID);
                if(status != null)
                {
                    o.ERPStatusMaterialMovement__c = status;
                    o.ERPResultDate__c = DateTime.now();
                }       
            }
        }
        update orderList;
    }


    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

   
}