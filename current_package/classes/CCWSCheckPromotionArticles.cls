/**********************************************************************
Name:  CCWS_AddChatterFeeditem 
======================================================
Purpose:                                                            
1) Takes a PromotionID as parameter
2) Calls all related Promotion Article from the given Promotion
3) Puts the status of all called Promotion Article to "inactive"
4) Returns true if if all Promotion Article are set to inactive or false, if the call needs to be "redone"
======================================================
History                                                            
-------                                                            
Date  		AUTHOR								DETAIL 
01/14/2014 	Bernd Werner <b.werner@yoursl.de> 	creation of this class      
     
***********************************************************************/

global without sharing class CCWSCheckPromotionArticles {
	WebService static CheckResult checkPromotionArticles(String StrPromotionId){
		
		// Define List of all Objects to insert
		List<PromotionArticle__c> ListPromArtUpdate = new List<PromotionArticle__c>();
		// Define List to get Promotion
		List<Promotion__c> ListPromotion 			= new List<Promotion__c>();
		// String to hold the original Promotion ID of Salesforce.com
		Id IdPromotionSFDCId 						= null;
		// Prepare Result Object
		CheckResult ObjectResult	 				= new CheckResult();
		ObjectResult.error							= '';
		
		// Number of records calculated at one time
		Integer noRec								= 800;
		
		// +++++++++++++++++++++++++++ STEP 1: Validate and query Promotion with the given external Id +++++++++++++++++++++++++++++++++++++++++++++
		// Query promotion
		try{
			ListPromotion = [Select PromotionID__c, Id From Promotion__c WHERE PromotionID__c = : StrPromotionId];
		}catch (Exception e){
			ObjectResult.success = false;
			return ObjectResult;
		}
		// Check, if we found a promotion
		if(ListPromotion.size()==0){
			// ObjectResult.addError('No Promotion found for ID '+StrPromotionId+'.');
			ObjectResult.success = false;
			return ObjectResult;
		}
		// Check, if we found to much promotions
		else if(ListPromotion.size()>1){
			// ObjectResult.addError('Found '+ListPromotion.size()+' Promotions for PromotionID '+StrPromotionId+'.');
			ObjectResult.success = false;
			return ObjectResult;
		}
		else{
			IdPromotionSFDCId = ListPromotion[0].Id;
		}
		system.debug('####PromotionID: '+IdPromotionSFDCId);
		
		// +++++++++++++++++++++++++++ STEP 2: Call all active PromotionArticle__c of the give promotion and uptae the status to "inactive"  +++++++++++++++++++++++++++++++++++++++++++++
		for(PromotionArticle__c ObjPromArt : [SELECT Id, Status__c FROM PromotionArticle__c WHERE Status__c='Active' AND Promotion__c= : IdPromotionSFDCId LIMIT : noRec]){
			ObjPromArt.Status__c = 'Inactive';
			ListPromArtUpdate.add(ObjPromArt);
		}
		
		// +++++++++++++++++++++++++++ STEP 3: If the List is less than noRec records, we are done, otherwise not+++++++++++++++++++++++++++++++++++++++++++++
		ObjectResult.ready 			= false;
		if (ListPromArtUpdate.size()<noRec){
			ObjectResult.ready 		= true;
		}
		
		if (!ListPromArtUpdate.isEmpty()){
			// Perform update
			try{
				update ListPromArtUpdate;
			}
			catch (Exception e){
				ObjectResult.error 		= e.getMessage();
				ObjectResult.success	= false;
				return ObjectResult;
			}
		}
		
		return ObjectResult;
		
		
		
	}
	
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class CheckResult{
		webservice String MessageUUID{get;set;}
		webservice String error{get;set;}
		webservice Boolean success{get;set;}
		webservice Boolean ready{get;set;}
	}
}