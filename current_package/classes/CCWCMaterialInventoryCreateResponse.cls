/*
 * @(#)CCWCMaterialInventoryCreateResponse.cls 
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
global without sharing class CCWCMaterialInventoryCreateResponse
{
    public static void processMaterialInventoryCreateResponse(String messageID, String requestMessageID, String headExternalID, CCWSGenericResponse.ReferenceItem referenceItem, 
                                                  String MaximumLogItemSeverityCode, CCWSGenericResponse.LogItemClass[] logItemArr, 
                                                  List<SCInterfaceLog__c> interfaceLogList,
                                                  CCWSGenericResponse.GenericServiceResponseMessageClass GenericServiceResponseMessage,
                                                  SCInterfaceLog__c responseInterfaceLog)
    {
        String interfaceName = 'SAP_MATERIAL_INVENTORY';
        String interfaceHandler = 'CCWCMaterialInventoryCreateResponse';
        String type = 'INBOUND';
        String direction = 'inbound';
        ID referenceID = null;
        String refType = null;
        ID referenceID2 = null;
        String refType2 = '';
        ID responseID = null;
        String resultCode = 'E000';
        String resultInfo = 'Success'; 
        
        String jsonInput = JSON.serialize(GenericServiceResponseMessage);
        SCInterfaceBase ib = new SCInterfaceBase();
        String fromJSONMap = ib.getDataFromJSON(jsonInput);
        debug('from json: ' + fromJSONMap);
        
        
        String data = 'headExternalID: ' + headExternalID + ',\n\nreferenceItem: ' + referenceItem + ',\n\nMaximumLogItemSeverityCode: ' + MaximumLogItemSeverityCode
                    + '\n allResponse: ' + fromJSONMap;
        debug('data: ' + data);            
        // Fill interface log response created by a pivot web service
        responseInterfaceLog.Interface__c = interfaceName;
        responseInterfaceLog.InterfaceHandler__c = interfaceHandler;
        responseInterfaceLog.Direction__c = direction;            
        responseInterfaceLog.MessageID__c = messageID;            
        responseInterfaceLog.ReferenceID__c = referenceID;            
        responseInterfaceLog.ResultCode__c = resultCode;            
        responseInterfaceLog.ResultInfo__c = resultInfo;
        responseInterfaceLog.Data__c = data;
        responseInterfaceLog.Data__c = responseInterfaceLog.Data__c.left(32000);
        debug('interfaceLogResponse2: ' + responseInterfaceLog);
        
        String step = '';
        Savepoint sp = Database.setSavepoint();
        try
        {
            // find the order external assignment
            step = 'find an order external assignment'; 
            SCInventory__c inv = readMaterialInventory(referenceItem.ExternalID);
            debug('inventory: ' + inv);
            responseInterfaceLog.Order__c = null;
            responseInterfaceLog.ReferenceID__c = inv.ID;   
            responseInterfaceLog.Inventory__c = inv.ID;         

            // find the interfacelog
            step = 'find a request interface';
            //GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
            // Thus it is possible that the interface does not exist.
            SCInterfaceLog__c requestInterfaceLog = CCWSGenericResponse.readOutoingInterfaceLog(requestMessageID, null, referenceItem.ExternalID, 'SAP_MATERIAL_INVENTORY');
            debug('requestInterfaceLog: ' + requestInterfaceLog);
            
            // update order
            step = 'update the order external assignment';
            debug('logItemArr: ' + logItemArr);
			
            inv.ERPStatus__c = CCWSGenericResponse.getResultStatus(logItemArr);
            inv.ERPInventoryCountNumber__c = referenceItem.ReferenceID;
			debug('ERPStatus__c: ' + inv.ERPStatus__c);
            inv.ERPResultDate__c = DateTime.now();
            update inv;
            debug('order external assignment after update: ' + inv);

            step = 'write a response interface log';
            // write response interface log
            responseInterfaceLog.Count__c = 1;           

            debug('responseInterfaceLog: ' + responseInterfaceLog);
            // update request interface log
            step = 'update the request interface log';
            if(responseInterfaceLog != null)
            {
            	//GMSGB 03.09.13 It is possible that the SAP response is faster than the commit of the call
                // Thus it is possible that the interface does not exist.
                if(requestInterfaceLog != null)
                {
	                debug('change a request interface log');
	                debug('requestInterfaceLog: ' + requestInterfaceLog);
	                debug('responseInterfaceLog: ' + responseInterfaceLog);
	                requestInterfaceLog.Response__c = responseInterfaceLog.Id;
	                requestInterfaceLog.Idoc__c = responseInterfaceLog.Idoc__c;
	                responseInterfaceLog.Stock__c = requestInterfaceLog.Stock__c;
	                debug('requestInterfaceLog: ' + requestInterfaceLog);
	                
	                interfaceLogList.add(requestInterfaceLog);
                }
                debug('interfaceLogList.size: ' + interfaceLogList.size());
                debug('interfaceLogList: ' + interfaceLogList);
                for(SCInterfaceLog__c il: interfaceLogList)
                {
                    debug('*ID: ' + il.id + ', MessageID: ' + il.MessageID__c + ', Response: ' + il.Response__c);
                } 
            }
            else
            {
                throw new SCfwException('A response interface log object could not be created for messageID: ' + messageID + 
                                        ',  inventory name: ' + headExternalID); 
            }    
            
        }
        catch(SCfwInterfaceRequestPendingException errorRequestNotPending)
        {
        	Database.rollback(sp);
        	throw errorRequestNotPending;
        }
        catch(SCfwException e) 
        {
            Database.rollback(sp);
            throw e;
        }  
        catch(Exception e) 
        {
            Database.rollback(sp);
            throw e;
        } 
    }//processMaterialInventoryCreateResponse

    public static SCInventory__c readMaterialInventory(String headExternalID)
    {
        SCInventory__c retValue = null;
        List<SCInventory__c> ol = [select ID, name, ERPStatus__c from SCInventory__c where name = : headExternalID 
                            and (ERPStatus__c = 'error' or ERPStatus__c = 'pending')];
        if(ol.size() > 0)
        {
            if(ol[0].ERPStatus__c == 'error' || ol[0].ERPStatus__c  == 'pending')
            {
            	retValue = ol[0];
            	debug('read inventory: ' + ol[0]);
            }
            else if (ol[0].ERPStatus__c == 'none' )
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The SCInventory__c with name: ' + headExternalId + ' has ERPStatus__c  \'none\' (expected \'pending\' or \'error\').');
        		throw e;
        	}
        	else if (ol[0].ERPStatus__c == 'ok' )
        	{
        		//SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The SCInventory__c with name: ' + headExternalId + ' has ERPStatus__c  \'ok\' (expected \'pending\' or \'error\').');     	
        		//throw e;
        		retValue = ol[0];
            	debug('The SCInventory__c with name: ' + headExternalId + ' has ERPStatus__c  \'ok\' (expected \'pending\' or \'error\').');
        	}
        	else
        	{
        		SCfwInterfaceRequestPendingException e = new SCfwInterfaceRequestPendingException('The SCInventory__c with name: ' + headExternalId + ' has ERPStatus__c  '
        		+ ol[0].ERPStatus__c +'(expected \'pending\' or \'error\').');  	
        		throw e;
        	}       
        }
        else
        {
        	throw new SCfwException('The SCInventory__c with name: ' + headExternalId + ' has been not found in Salesforce.');
        }
        return retValue;
    }

    public static void debug(String msg)
    {
        System.debug('###...' +  msg);        
    }

   
}