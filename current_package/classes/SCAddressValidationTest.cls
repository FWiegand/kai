/*
 * @(#)SCAddressValidationTest.cls 
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Tests the address validation function by emulating the calls to 
 * the web services. 
 */
@isTest
private class SCAddressValidationTest {

    static testMethod void checkPositive1() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.city = 'Bahnhofstr 2 Paderborn';
        addr.matchInfo = 'test';

        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result = validation.check(addr);
    }
    
    static testMethod void checkPositive2() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.city = 'Paderborn';
        addr.houseNumber = '10';
        addr.street = 'Schulstraße';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result = validation.check(addr);
    }

    static testMethod void checkPositive3() {
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result = validation.check('de', '33100 Paderborn Bahnhofstr 2');
    }

    static testMethod void checkNegative1() {
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result = validation.check('unknown', '33100 Paderborn Bahnhofstr 2');
    }

    static testMethod void checkNegative2() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'unknown';
        addr.city = 'Paderborn';
        addr.houseNumber = '10';
        addr.street = 'Schulstraße';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result = validation.check(addr);
    }

    static testMethod void checkGeocode() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.city = 'Paderborn';
        addr.houseNumber = '10';
        addr.street = 'Schulstraße';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result  = validation.geocode(addr);
        //todo evaluate result
        
    }

    static testMethod void geocodeNegative() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'unknown';
        addr.city = 'Paderborn';
        addr.houseNumber = '10';
        addr.street = 'Schulstraße';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result  = validation.geocode(addr);
    }
    
    static testMethod void geocode2() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'Amsterdam';
        addr.houseNumber = '10';
        addr.street = 'Grot';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result  = validation.geocode(addr);
    }

    static testMethod void geocode3() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'xx';
        addr.city = 'Amsterdam';
        addr.houseNumber = '10';
        addr.street = 'Grot';
        addr.matchInfo = 'test';
        
        SCAddressValidation validation = new SCAddressValidation();
        AvsResult result  = validation.geocode(addr);
    }
}