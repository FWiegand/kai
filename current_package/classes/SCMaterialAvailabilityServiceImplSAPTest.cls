/**
 * SCMaterialAvailabilityServiceImplSAPTest.cls    aw        29.05.2012
 * 
 * Copyright (c) 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

@isTest
private class SCMaterialAvailabilityServiceImplSAPTest
{
    public static testMethod void checkMatAvailNegativ()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);

        SCMaterialAvailabilityServiceImplSAP mas = new SCMaterialAvailabilityServiceImplSAP();
        SCMaterialAvailabilityData.SCServiceData serviceData = new SCMaterialAvailabilityData.SCServiceData();
        serviceData.mode = 'TEST';
                
        //SCMaterialAvailabilityData.SCResult result = mas.call(serviceData);
        //System.assertEquals(false, result.success);
    } // checkMatAvailNegativ

    public static testMethod void checkMatAvailPositiv()
    {
        SCHelperTestClass3.createCustomSettings('XX', true);
        SCHelperTestClass.createDomsForOrderCreation();
        SCHelperTestClass.createOrderTestSet3(true);

        SCMaterialAvailabilityServiceImplSAP mas = new SCMaterialAvailabilityServiceImplSAP();
        SCMaterialAvailabilityData.SCServiceData serviceData = new SCMaterialAvailabilityData.SCServiceData();
        serviceData.mode = 'TEST_OK';
        serviceData.orderId = SCHelperTestClass.order.Id;
                
        //SCMaterialAvailabilityData.SCResult result = mas.call(serviceData);
        //System.assertEquals(true, result.success);
        //System.assertEquals(1, result.results.size());
    } // checkMatAvailPositiv
}