/**
 * SCMaterialAvailabilityService.cls    aw  24.05.2012
 * 
 * Copyright (c) 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */


/**
 * Interface is used to check material availability
 */
public interface SCMaterialAvailabilityService
{
    SCMaterialAvailabilityData.SCResult call(SCMaterialAvailabilityData.SCServiceData serviceData);
}