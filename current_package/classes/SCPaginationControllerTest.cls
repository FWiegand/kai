/*
 * @(#)SCPaginationControllerTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCPaginationControllerTest {

    /**
     * Create some records for the pagination test
     */
    static void createData()
    {
            SCHelperTestClass.createArticle('TEST-000251', true);
            SCHelperTestClass.createArticle('TEST-000252', true);
            SCHelperTestClass.createArticle('TEST-000253', true);
            SCHelperTestClass.createArticle('TEST-000254', true);
            SCHelperTestClass.createArticle('TEST-000255', true);
    }
    
    /**
     * Check initialisation of the controller
     */
    static testMethod void initPositive1() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        Test.startTest();

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;
        pageController.setPage();
        
        Test.stopTest();
        
        System.assertNotEquals(null, pageController, 
                               'Paging controller is NULL!');
        System.assertNotEquals(null, pageController.pager,
                               'Pager not set!');
    }
    
    /**
     * Check result set size and init state
     */
    static testMethod void testPaging1() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;

        System.assertEquals(1, pageController.getResultFrom(),
                            'From page invalid');
        System.assertEquals(1, pageController.getResultTo(),
                            'To page invalid');

        System.assertEquals(5, pageController.getResultSize(),
                            'Result set has wrong size');  

        System.assertEquals('5', pageController.pageCount,
                            'Wrong page count');  
                            
        System.assertEquals('1', pageController.currentPage,
                            'Wrong current page');
    }
    
   
    /**
     * Check page changing in the result set
     */
    static testMethod void testLastFirstPage() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;
        
        pageController.lastPage();
        System.assertEquals('5', pageController.currentPage,
                            'Setting to last page failed');
        
        pageController.firstPage();
        
        System.assertEquals('1', pageController.currentPage,
                            'Setting to first page faild');
    }
    
    
        
    /**
     * Check page changing in the result set
     */
    static testMethod void testNextPage() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;
        
        System.assertEquals('1', pageController.currentPage,
                            'First page wrong');

        // next page shall be page 2        
        pageController.nextPage();
        System.assertEquals('2', pageController.currentPage,
                            '2nd page wrong');

        // next page shall be page 3    
        pageController.nextPage();
        System.assertEquals('3', pageController.currentPage,
                            '3rd page wrong');
    
        // we should never get beyond the last page
        pageController.nextPage();
        pageController.nextPage();
        pageController.nextPage();
        pageController.nextPage();
        System.assertEquals('5', pageController.currentPage,
                            'Got beyong the max page');
                            

    }
    
    /**
     * Check page changing in the result set
     */
    static testMethod void testPrevPage() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;
        
        System.assertEquals('1', pageController.currentPage,
                            'First page wrong');

        // setting to page 3      
        pageController.nextPage();
        pageController.nextPage();
        System.assertEquals('3', pageController.currentPage,
                            'Setting 3rd page wrong');

        pageController.previousPage();
        System.assertEquals('2', pageController.currentPage,
                            'Setting 2nd page wrong');

        pageController.previousPage();
        System.assertEquals('1', pageController.currentPage,
                            'Setting 1st page wrong');
                            
        pageController.previousPage();
        pageController.previousPage();
        pageController.previousPage();
        System.assertEquals('1', pageController.currentPage,
                            'Setting before first page possible');
    }


    /**
     * Check page changing in the result set
     */
    static testMethod void testSetPage() 
    {
        createData();
        
        ApexPages.StandardSetController setController;
        
        // We shall get 5 records here
        String query = 'select Name, Id from SCArticle__c where Name like \'TEST-00025%\'';

        setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

        setController.setPageSize(1);

        SCPaginationController pageController = new SCPaginationController();
        pageController.pager = setController;
        
        pageController.lastPage();
        System.assertEquals('5', pageController.currentPage,
                            'Setting to last page failed');
        
        pageController.currentPage = '2';
        
        System.assertEquals('2', pageController.currentPage,
                            'Setting to 2nd page failed');
        System.assertEquals(2, pageController.getResultFrom(),
                            'Wrong from limit for 2nd page');
        System.assertEquals(2, pageController.getResultTo(),
                            'Wrong to limit for 2nd page');
    }
           
}