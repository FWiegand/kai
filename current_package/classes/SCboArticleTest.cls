/*
 * @(#)SCboArticleTest.cls 
 *  
 * Copyright 2010 by GMS Development GmbH 
 * Karl-Schurz-Strasse 29,  33100 Paderborn 
 * All rights reserved. 
 * 
 * This software is the confidential and proprietary information 
 * of GMS Development GmbH. ("Confidential Information").  You 
 * shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement 
 * you entered into with GMS. 
 */ 
 
/** 
 * 
 * @author Lars Diembeck <ldiembeck@gms-online.de> 
 * @author Thorsten Klein <tklein@gms-online.de> 
 * @version $Revision$, $Date$ */ 
@isTest 
private class SCboArticleTest
{



    /**
     * Create a new boArticle with the ID contructor
     */
    static testMethod void testNewArticlePositiv1()
    {
        SCHelperTestClass.createTestArticles(true);
        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            SCboArticle boArticle = new SCBoArticle(SCHelperTestClass.articles[0].Id);
            System.debug('ArticleId :' + boArticle.article.Id);
            System.assert(boArticle.article.Id != null);
        }
        
        
        
        
    }

    /**
     * Create a new boArticle with the String contructor
     */
    static testMethod void testNewArticlePositiv2()
    {
        SCHelperTestClass.createTestArticles(true);
        
        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            String articleId = SCHelperTestClass.articles[0].Id;
            
            SCboArticle boArticle = new SCBoArticle(articleId);
            
            System.debug('ArticleId :' + boArticle.article.Id);
            System.assert(boArticle.article.Id != null);
        }
    }

    /**
     * Create a new boArticle with the String contructor and brand
     */
    static testMethod void testNewArticlePositiv3()
    {
        SCHelperTestClass.createBrandObject(false, true);
        SCHelperTestClass.createTestArticles(true);
        
        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            String articleId = SCHelperTestClass.articles[0].Id;
            String brandId = SCHelperTestClass.brand.Id;
            SCHelperTestClass.createArticleDetail(articleId, true);       
            
            SCboArticle boArticle = new SCBoArticle(articleId, brandId);
            
            System.debug('ArticleId :' + boArticle.article.Id);
            
            // Test case 1
            SCArticleDetail__c articleDetails = boArticle.getArticleDetails(brandId);
            
            System.assert(boArticle.article.Id != null);
            System.assert(articleDetails != null);

            // Test case 2
            SCboArticle a = new SCboArticle(articleId);
            System.assertEquals(articleId, a.article.Id);
            
            // Test case 3
            SCboArticle b = new SCboArticle(articleId, brandId);
            System.assertEquals(articleId, a.article.Id);

            // Test case 4
            SCboArticle c = new SCboArticle();
            c.createByName(SCHelperTestClass.articles[0].Text_en__c);
            System.assertEquals(articleId, a.article.Id);

            // Test case 5
            SCboArticle d = new SCboArticle();
            c.createByName(SCHelperTestClass.articles[0].Text_en__c, brandId);
            System.assertEquals(articleId, a.article.Id);

            // Test case 5
            SCboArticle e = new SCboArticle();
            // e.getArticleDetails('ignore');  // just for the coverage            
        }
    }


    /**
     * Create a new boArticle with the ID contructor and brand
     */
    static testMethod void testNewArticlePositiv4()
    {
        SCHelperTestClass.createBrandObject(false, true);
        SCHelperTestClass.createTestArticles(true);
        
        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            String articleId = SCHelperTestClass.articles[0].Id;
            String brandId = SCHelperTestClass.brand.Id;
            SCHelperTestClass.createArticleDetail(articleId, true);       
            
            SCboArticle boArticle = new SCBoArticle(SCHelperTestClass.articles[0].Id, 
                                                    brandId);
            
            System.debug('ArticleId :' + boArticle.article.Id);
            
            SCArticleDetail__c articleDetails = boArticle.getArticleDetails(brandId);
            
            System.assert(boArticle.article.Id != null);
            System.assert(articleDetails != null);
            
        }
    }
    
    static testMethod void testNewArticleNagativ()
    {
        SCHelperTestClass.createTestArticles(true);
        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            SCBoArticle boArticle = new SCBoArticle('11111');
            System.assert(boArticle.article == null);
        }
    }


    static testMethod void testNewArticleNagativ2()
    {
        SCHelperTestClass.createBrandObject(false, true);
        SCHelperTestClass.createTestArticles(true);

        if (SCHelperTestClass.articles != null && SCHelperTestClass.articles.size() > 0)
        {
            SCHelperTestClass.createArticleDetail(SCHelperTestClass.articles[0].Id, true);

            SCBoArticle boArticle = new SCBoArticle('11111',
                                                    SCHelperTestClass.brand.Id);

            System.assert(boArticle.article == null);
        }
    }


}