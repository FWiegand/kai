/**
 * SCMaterialAvailabilityServiceImplSAP.cls    aw  24.05.2012
 * 
 * Copyright (c) 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */


/**
 * Class is used to check material avalability to SAP.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$ 
 */
public class SCMaterialAvailabilityServiceImplSAP implements SCMaterialAvailabilityService
{
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
    
    public SCMaterialAvailabilityData.SCResult call(SCMaterialAvailabilityData.SCServiceData serviceData)
    {
        SCMaterialAvailabilityData.SCResult result = new SCMaterialAvailabilityData.SCResult();
        result.success = true;
        result.results = new List<SCMaterialAvailabilityData.SCResultData>();
        
        // if we got an order id, get the data from the order and put them into the input data object
        if (SCBase.isSet(serviceData.orderId))
        {
            SCOrder__c order = [Select Name, Country__c, Brand__r.Name 
                                  from SCOrder__c where Id = :serviceData.orderId];
                                  
            serviceData.orderNo = order.Name;
            serviceData.country = order.Country__c;
            serviceData.brand = order.Brand__r.Name;
            
            serviceData.articles = new List<SCMaterialAvailabilityData.SCArticleData>();
            
            for (SCOrderLine__c line :[Select ArticleNumber__c, Qty__c from SCOrderLine__c where Order__c = :serviceData.orderId])
            {
                serviceData.articles.add(new SCMaterialAvailabilityData.SCArticleData(line.ArticleNumber__c, 
                                                                                      line.Qty__c.intValue()));
            }
            
            serviceData.accNumber = [Select AccountNumber__c from SCOrderRole__c 
                                      where Order__c = :serviceData.orderId 
                                        and OrderRole__c = :SCfwConstants.DOMVAL_ORDERROLE_AG].AccountNumber__c;
        }

        // check the input, fill emtpy fields
        if (!SCBase.isSet(serviceData.orderNo))
        {
            serviceData.orderNo = 'ORD-000000TEST';
        }
        if (!SCBase.isSet(serviceData.country))
        {
            serviceData.country = appSettings.DEFAULT_COUNTRY__c;
        }
        if (!SCBase.isSet(serviceData.brand))
        {
            if (SCBase.isSet(appSettings.DEFAULT_BRAND__c))
            {
                try
                {
                    Brand__c brand = [select Id, Name from Brand__c 
                                       where ID2__c = :appSettings.DEFAULT_BRAND__c];
                    
                    serviceData.brand = brand.Name;
                }
                catch (TypeException e)
                {
                }
            } // if (SCBase.isSet(appSettings.DEFAULT_BRAND__c))
        }
        if (!SCBase.isSet(serviceData.erpKey))
        {
            serviceData.erpKey = 'REPL';
        }
        if (!SCBase.isSet(serviceData.accNumber))
        {
            serviceData.accNumber = '';
            String accNumbers = appSettings.MATERIALAVAILABILITY_ACCNUMBER__c;
            if (SCBase.isSet(accNumbers))
            {
                for (String tuples :accNumbers.split('\\|'))
                {
                    List<String> items = tuples.split('=');
                    if (items[0] == serviceData.country)
                    {
                        serviceData.accNumber = items[1];
                        break;
                    }
                }
            }
        }
        while (serviceData.accNumber.length() < 10)
        {
            serviceData.accNumber = '0' + serviceData.accNumber;
        }
        if (null == serviceData.deliveryDate)
        {
            serviceData.deliveryDate = Date.today();
        }
        
        Datetime requestDate = Datetime.newInstance(serviceData.deliveryDate, Time.newInstance(0, 0, 0, 0));
        
        // now build up the request string
        String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ord="http://www.vaillant-group.com/LS/ZVG_EPS/EPS-SF-10/orderSimulate">';
        body += '<soapenv:Header/>';
        body += '   <soapenv:Body>';
        body += '      <ord:mtOrderSimulateReq>';
        body += '         <Order>';
        body += '            <OrderHeader>';
        body += '               <item>';
        body += '                  <Country>' + serviceData.country + '</Country>';
        body += '                  <Brand>' + serviceData.brand + '</Brand>';
        body += '                  <OrderNumber>' + serviceData.orderNo + '</OrderNumber>';
        body += '                  <BGM>' + serviceData.erpKey + '</BGM>';
        body += '                  <MaterialClass>SPARE PART</MaterialClass>';
        body += '                  <DeliveryDate>' + requestDate.format('yyyy-MM-dd') + '</DeliveryDate>';
        body += '               </item>';
        body += '               <OrderItem>';
        body += '                  <OrderLine>';
        
        Integer i = 0;
        for (SCMaterialAvailabilityData.SCArticleData article :serviceData.articles)
        {
            i++;
            body += '                     <Item>';
            body += '                        <PositionNo>' + i++ + '</PositionNo>';
            body += '                        <ArticleNo>' + article.articleNo + '</ArticleNo>';
            body += '                        <Qty>' + article.qty + '</Qty>';
            body += '                     </Item>';
        }
        
        body += '                  </OrderLine>';
        body += '               </OrderItem>';
        body += '               <OrderRole>';
        body += '                  <Item>';
        body += '                     <OrderRole>AG</OrderRole>';
        body += '                     <AccountNumber>' + serviceData.accNumber + '</AccountNumber>';
        body += '                  </Item>';
        body += '               </OrderRole>';
        body += '            </OrderHeader>';
        body += '         </Order>';
        body += '      </ord:mtOrderSimulateReq>';
        body += '   </soapenv:Body>';
        body += '</soapenv:Envelope>';
                
        Datetime start = Datetime.now();

        // send the request
        HttpRequest req = new HttpRequest();
        req.setEndpoint(appSettings.MATERIALAVAILABILITY_SERVICE_ENDPOINT__c);
        req.setMethod('POST');
        req.setHeader('Content-Type','text/xml; charset=utf-8');
        req.setHeader('Authorization', 'Basic ' + 
                        EncodingUtil.base64Encode(Blob.valueOf(appSettings.MATERIALAVAILABILITY_SERVICE_USER__c + ':' + 
                                                               appSettings.MATERIALAVAILABILITY_SERVICE_PWD__c)));
        
        req.setHeader('SOAPAction','http://sap.com/xi/WebService/soap1.1');
        req.setBody(body);
        req.setTimeout(60000);
        
        // get the response
        Http http = new Http();
        String httpResult;
        
        if (serviceData.mode.equalsIgnoreCase('PROD'))
        {
            HTTPResponse res = http.send(req);
            httpResult = res.getBody();
        }
        else
        {
            httpResult = createTestResponse(serviceData.mode.equalsIgnoreCase('TEST_OK'));
        }
    
        Datetime stop = Datetime.now();

        // check the result
        if (httpResult.contains('faultText'))
        {
            Pattern perror = Pattern.compile('<text>(.+?)</text>');
            Matcher merror = perror.matcher(httpResult);
            merror.find();
            result.success = false;
            result.errorMsg = merror.group(1);
        } // if (httpResult.contains('faultText'))

        writeProtocol(serviceData.orderId, result.success ? 'OK' : result.errorMsg, 
                      'Input:\n' + body + '\n\nResult:\n' + httpResult, start, stop);

        if (!result.success)
        {
            return result;
        }
        
        Pattern pitem = Pattern.compile('<Items>(.+?)</Items>');
        Pattern plines = Pattern.compile('<Lines>(.+?)</Lines>');
        Pattern pmaterial = Pattern.compile('<MaterialNumber>(.+?)</MaterialNumber>');
        Pattern pquant = Pattern.compile('<Quantity>(.+?)</Quantity>');
        Pattern pdate = Pattern.compile('<AvailabilityDate>(.+?)</AvailabilityDate>');
        
        Matcher  mitem = pitem.matcher(httpresult);

        while (mitem.find())
        {
            String item = mitem.group();
            
            Matcher mmaterial = pmaterial.matcher(item);
            mmaterial.find();
            String materialnumber = mmaterial.group(1);
            
            Matcher mlines = plines.matcher(item);
            while (mlines.find())
            {
                String line = mlines.group();
                SCMaterialAvailabilityData.SCResultData resultData = new SCMaterialAvailabilityData.SCResultData();
                resultData.articleNr = materialnumber;
                
                try
                {
                    Matcher mquant = pquant.matcher(line);
                    mquant.find();
                    resultData.qtyStr = mquant.group(1);
                    resultData.qty = SCBase.isSet(resultData.qtyStr) ? Decimal.valueOf(resultData.qtyStr) : 0.0;
                    
                    Matcher mdate = pdate.matcher(line);
                    mdate.find();
                    resultData.deliveryDateStr = mdate.group(1);
                    resultData.deliveryDate = Date.valueOf(resultData.deliveryDateStr);
                    resultData.deliveryDateStr = resultData.deliveryDate.format();
                }
                catch (Exception e)
                {
                }
                
                if (resultData.qty > 0)
                {
                    result.results.add(resultData);
                }
            } // while (mlines.find())
        } // while (mitem.find())

        System.debug('#### call(): result -> ' + result);
        return result;
    } // call
    
    /**
    * Records the BIBIT payment service
    */
    private void writeProtocol(Id orderId, String resultInfo, String data,
                               Datetime start, Datetime stop)
    {
        String resultCode = 'E000';
        if (!resultInfo.equalsIgnoreCase('OK'))
        {
            resultCode = 'E101';
        }

        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        ic.Interface__c = 'Material Availability Service';
        ic.InterfaceHandler__c = 'SCMaterialAvailabilityServiceImplSAP';
        ic.Type__c = 'OUTBOUND';
        ic.Count__c = 1;
        ic.ReferenceID__c = orderId;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Start__c = start;
        ic.Stop__c = stop;
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
        
        insert ic;
    } // writeProtocol
    
    /**
     * Creates a http response string for the test mode.
     */
    private String createTestResponse(Boolean testOk)
    {
        String response = '';

        response += '<SOAP:Envelope xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/">';
        response += '<SOAP:Body>';
        
        if (testOk)
        {
            response += '<detail><ns1:mtOrderSimulateRes xmlns:ns1="http://www.vaillant-group.com/LS/ZVG_EPS/EPS-SF-10/orderSimulate">';
            response += '<OrderItems><Items><ItemNumber>000010</ItemNumber><MaterialNumber>300712</MaterialNumber><TargetQuantity>1.0</TargetQuantity>';
            response += '<ScheduleLines><Lines><Quantity>0.0</Quantity><AvailabilityDate>2012-01-06</AvailabilityDate><Line>0001</Line></Lines>';
            response += '<Lines><Quantity>1.0</Quantity><AvailabilityDate>2012-01-10</AvailabilityDate><Line>0002</Line></Lines>';
            response += '<NetValue>45.08</NetValue><Currency>EUR</Currency></ScheduleLines></Items></OrderItems></ns1:mtOrderSimulateRes>';
        }
        else
        {
            response += '<SOAP:Fault><faultcode>SOAP:Server</faultcode><faultstring>Application Error</faultstring>';
            response += '<detail><ns1:fmtOrderSimulateError xmlns:ns1="http://www.vaillant-group.com/LS/ZVG_EPS/EPS-SF-10/orderSimulate">';
            response += '<standard><faultText>Proxy Class (generated)</faultText><faultDetail><severity>error</severity>';
            response += '<text>Die Verkaufsbelegart ???? ist nicht vorgesehen</text>';
            response += '<url>http://uxsgp04d01.bbn.vaillant.hp-net.de:8001/sap/xi/docu_apperror?ID=NA&OBJECT=V1312&LANGUAGE=D&MSGV1=%3f%3f%3f%3f</url><id>312(V1)</id></faultDetail>';
            response += '</standard></ns1:fmtOrderSimulateError></detail></SOAP:Fault>';
        }
        response += '</SOAP:Body>';
        response += '</SOAP:Envelope>';
        
        return response;
    } // createTestResponse
} // class SCMaterialAvailabilityServiceImplSAP