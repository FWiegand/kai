/**
 * @(#)SCInterfaceClearingSAPClose
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 * This class is a CCE sperxific implementation for an SAP order close operation in SAP.
 */
public class SCInterfaceClearingSAPCloseCCE implements SCInterfaceClearingSAPClose
{
    
    public Boolean orderSAPClose(String oid)
    {   
        System.debug('call CCWCOrderCloseEx.process(oid) in SCInterfaceClearingSAPCloseCCE '+oid);
        return CCWCOrderCloseEx.processForceClose(oid);
    }

}