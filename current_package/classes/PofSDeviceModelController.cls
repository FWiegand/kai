/**********************************************************************
Name:  fsFA_PofS_DeviceModelController()
======================================================
Purpose:                                                            
This class serves as the data management controller of PofS_DeviceModel for the PofS Editor view.                                              
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
01/14/2013	Jan Mensfeld
***********************************************************************/

public class PofSDeviceModelController {

    public PofSDeviceModelController() {
    	
    }
		
	public Id PofS_Id { get; set; }
	//public Id Device_Id { get; set; }
	public String DeviceModelName { get; set; }
	
	public List<PofSDeviceModel__c> listPofSDeviceModels;
	public List<PofSDeviceModel__c> getlistPofSDeviceModels() {
		if(PofS_Id != null)
			return [SELECT Id, Name, 
	        		DeviceModel__r.Name, 
	        		DeviceModel__r.AvailableBrands__c, 
	        		DeviceModel__r.Brand__c
	        	FROM PofSDeviceModel__c 
	        	WHERE PictureOfSuccess__c =: PofS_Id];
		return null;
	}
	
	public PofSDeviceModel__c PofSDeviceLookup { get; set; }

	public void savePofSDeviceModel () {
		if (DeviceModelName == null || DeviceModelName == '' || PofS_Id == null)
			return;
		//System.debug('Debug JM:' + Device_Id);
		PofSDeviceModel__c newPofSDeviceModel = new PofSDeviceModel__c();
		newPofSDeviceModel.PictureOfSuccess__c = PofS_Id;
	
		SCProductModel__c objProdModel = [SELECT Id, Name FROM SCProductModel__c WHERE Name =: DeviceModelName LIMIT 1];
		
		newPofSDeviceModel.DeviceModel__c = objProdModel.Id;
		insert newPofSDeviceModel;
	}
	
	public void cancelPofDeviceModel () {
		// 
	}
}