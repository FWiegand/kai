/*
 * @(#)SCutilSequence.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Allow the generation of country-specific continious sequences for 
 * invoices, orders and so on.
 * <p>
 * These sequences have to assure that all numbers are used only once and
 * that are continious for each country and each type. Furthermore the formats
 * can be different for types and/or countries.
 *
 * (see SCConfSequence__c for further details)
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision: 852 $, $Date: 2010-12-01 11:39:36 +0100 (Wed, 01 Dec 2010) $
 */
public class SCutilSequence
{
    /**
     * Generate the next x valid sequence strings for a given sequence and
     * country.
     * If no valid sequence for a given country is found the global one 
     * will be used.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param sequence Name of the sequence
     * @param country Country this sequence belongs to
     * @param count the number of sequence numbers to created
     * @return Next value in the sequence
     */
    public static String[] next(String sequence, String country, Integer count)
    {
        String sequenceName = sequence + '_' + country;
        SCConfSequence__c seq;
        List<String> nextSequences = new List<String>();
        
        try
        {
            // try to get the country specific sequence 
            seq = [select Id, Name, Min__c, Max__c, Current__c, 
                          Cycle__c, IncrementBy__c, 
                          Prefix__c, Postfix__c
                     from SCConfSequence__c
                    where Name = :sequenceName
                      for update];
                                           
        }
        catch (QueryException e)
        {
            System.debug('No country specific sequence found for ' + sequenceName);
            sequenceName = sequence;
        }
        
        if (seq == null)
        {
            try
            {
                // try to get the country specific sequence 
                seq = [select Id, Name, Min__c, Max__c, Current__c, 
                              Cycle__c, IncrementBy__c, 
                              Prefix__c, Postfix__c
                         from SCConfSequence__c
                        where Name = :sequence
                          for update];
                                               
            }
            catch (QueryException e)
            {
                String error = 'No sequence found for ' + sequence;
                System.debug(error);
                throw new SCutilSequenceException(error);
            }
        }
        
        for (Integer i = 0; i < count; i++)
        {
            String nextSequence = '';
            
            System.debug('### fetched -> ' + seq);
                
            Decimal nextNumber = seq.Current__c + seq.IncrementBy__c;
                
            if (nextNumber < seq.Min__c)
            {
                nextNumber = seq.Min__c;
                seq.Current__c = seq.Min__c;       
            }
            else if (nextNumber > seq.Max__c && seq.Cycle__c == true)
            {
                // If the incremented number is now larger than the maximum
                // number and cycling is allowed, we simply start at the beginning again.
                nextNumber = seq.Min__c;
                seq.Current__c = seq.Min__c;                
            }
            else if (nextNumber > seq.Max__c)
            {
                // The incremented number is larger than the maximum number and
                // cycling is not possible.
                // We throw an exception then
                throw new SCutilSequenceException('Maximum number for sequence ' + seq.Name + ' reached');
            }

            seq.Current__c = nextNumber;
System.debug('prefix -> ' + seq.Prefix__c);

            if (seq.Prefix__c <> null)
            {
System.debug('prefix not null -> ' + seq.Prefix__c);
                nextSequence = seq.Prefix__c;
            }
            
            nextSequence += nextNumber;

            if (seq.Postfix__c <> null)
            {
                nextSequence += seq.Postfix__c;
            }

            System.debug('### 2update -> ' + seq);
        
            nextSequences.add(nextSequence);
        }

        try
        {
            update seq;
        }
        catch (DmlException e)
        {
            throw new SCutilSequenceException('Unable to update ' + seq.Name + ': ' + e.getMessage());
        }    

        
        return nextSequences;
    }

    /**
     * Generate the next valid sequence string for a given sequence and
     * country.
     * If no valid sequence for a given country is found the global one 
     * will be used.
     *
     * @author Thorsten Klein <tklein@gms-online.de>
     * @param sequence Name of the sequence
     * @param country Country this sequence belongs to
     * @return Next value in the sequence
     */
    public static String next(String sequence, String country)
    {
        String nextSequence = next(sequence, country, 1)[0];

        return nextSequence;       
        
    }

}