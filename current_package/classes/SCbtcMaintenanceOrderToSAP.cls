/*
 * @(#)SCbtcMaintenanceOrderToSAP.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * As we can not call a web service when we create data (uncommitted work pendingexception)
 * we have to use a separate job to initiate the transfer to SAP. 
 * Execute this job after SCbtcMaintenanceOrderCreate
 *
 * Excample:
 * SCbtcMaintenanceOrderToSAP.asyncTransferAll(0, 'trace');
 *
 */

global with sharing class SCbtcMaintenanceOrderToSAP extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts  
{
    private static final Integer batchSize = 1;
    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    String mode = 'productive';
    Integer max_cycles = 0;
    public String extraCondition = null;

    public Id orderId = null;
    
    private Boolean autoCreated = true;
    private String query; // Query for batch job 

    // Object for application settings
    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();


    public static ID asyncTransferAll(Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderToSAP btc = new SCbtcMaintenanceOrderToSAP(max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncTransferAll(Integer max_cycles, String mode, String extraCondition)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderToSAP btc = new SCbtcMaintenanceOrderToSAP(max_cycles, mode, extraCondition);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static ID asyncTransfer(ID orderId, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderToSAP btc = new SCbtcMaintenanceOrderToSAP(orderId, max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

    public static List<SCOrder__c> syncTransfer(ID orderId, Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcMaintenanceOrderToSAP btc = new SCbtcMaintenanceOrderToSAP(orderId, max_cycles, mode);

        Boolean startCoreReturnsRetValue = false;
        Boolean aborted = false;        
        btc.startCore(startCoreReturnsRetValue, aborted); // calculates the query 
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCOrder__c> retValue = btc.executeCore(mList);
        return retValue;
    }  



   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        Boolean aborted = false;
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaintenanceOrderToSAP', 'start'))
        {
            aborted = true;
        }
        
        Boolean startCoreReturnsRetValue = true;
        return startCore(startCoreReturnsRetValue, aborted);
    } // start

    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {
        // one order will be created from one SContractVisit__c record 
        // one order will be created from one SContractVisit__c record 
        
        /* To check the syntax of SOQL statement 
        List<SCOrder__c> orderList = [select id from SCOrder__c 
                                                    where Maintenance__c <> null
                                                    and Channel__c = '5307' // FROM_CONTRACT_OR_MAINTENANCE
                                                    and ERPStatusOrderCreate__c = 'none'
                                                    and ERPOrderNo__c = null
                                                    and status__c <> '5507' // cancelled
                                                    limit 1];
        debug('start: after checking statement');
        */

        if(orderId == null)
        {
            query = 'select id from SCOrder__c where Maintenance__c <> null and Channel__c = \'5307\' and ERPStatusOrderCreate__c = \'none\' and ERPOrderNo__c = null and status__c <> \'5507\' ';

            if(extraCondition != null)
            {
                query += ' and (' + extraCondition + ')';
            }                   
             
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_cycles > 0)
                {
                    query += ' limit ' + max_cycles;
                }
            }               
        }
        else
        {
            query = 'select id from SCOrder__c where Maintenance__c <> null and Channel__c = \'5307\' and ERPStatusOrderCreate__c = \'none\' and ERPOrderNo__c = null and status__c <> \'5507\' '
		          + ' and ID = \'' + orderId + '\'';
            if(aborted)
            {
                query += ' limit 0';
            }
            else
            {
                if(max_cycles > 0)
                {
                    query += ' limit ' + max_cycles;
                }
            }
        }
        debug('query: ' + query);
        Database.QueryLocator retValue = null;
        if(returnsRetValue)
        {
            retValue = Database.getQueryLocator(query);
        }
        return retValue;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcMaintenanceOrderToSAP', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

    public List<SCOrder__c> executeCore(List<sObject> scope)
    {
    	List<SCOrder__c> retValue = new List<SCOrder__c>();
        List<SCMaintenance__c> maintenanceUpdateList = new List<SCMaintenance__c>();
        
        for(sObject rec: scope)
        {
            debug('sObject: ' + rec);
            SCOrder__c o = (SCOrder__c)rec;
            // CCEAG --> 
            // now call the SAP interface to create the SAP order (synchronously)
            // the Trigger SCOrder_AI_CallSapWebService is suppressing the async callout !
            Boolean async = false; // future calls in future calls are not supported so we need a synch callout
            CCWCOrderCreate.callout(o.Id, async, false);
			// we do not log the job because they are logged in the CCWCOrderCreate
            // CCEAG <-- 
        }
        debug('mainenanceUpdateList: ' + maintenanceUpdateList);
        update maintenanceUpdateList;
        return retValue;
    }
   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcMaintenanceOrderToSAP(Integer max_cycles, String mode)
    {
        this(null, max_cycles, mode, null);
    }

    public SCbtcMaintenanceOrderToSAP(Integer max_cycles, String mode, String extraCondition)
    {
        this(null, max_cycles, mode, extraCondition);
    }

    public SCbtcMaintenanceOrderToSAP(Id orderId, Integer max_cycles, String mode)
    {
        this(orderId, max_cycles, mode, null);
    }
    /**
     * Constructor
     * @param contractID
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcMaintenanceOrderToSAP(Id orderId, Integer max_cycles, String mode,
            String extraCondition)
    {
        this.orderId = orderId;
        this.max_cycles = max_cycles;
        this.mode = mode;
        this.extraCondition = extraCondition;
    }

    public String getQuery()
    {
        return query;
    }    

    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
}