/*
 * @(#)SCboOrderRepairCodeValidation.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboOrderRepairCodeValidation
{
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();

    /**
     * Validates a business object from the type order line.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean validate(SCboOrderRepairCode boRepairCode)
    {
        return validate(boRepairCode, SCboOrderValidation.ALL);
    }
    public static Boolean validate(SCboOrderRepairCode boRepairCode, Integer mode)
    {
        Boolean ret = true;
        
        System.debug('#### validate(): boRepairCode -> ' + boRepairCode);
        if ((mode & SCboOrderValidation.REPAIRCODE_DATA) == SCboOrderValidation.REPAIRCODE_DATA)
        {
            if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && 
                appSettings.VALIDATE_PICKLIST_VALUES__c.equals('1'))
            {
                if (SCBase.isSet(boRepairCode.repairCode.ErrorActivityCode1__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorActivityCode1__c, boRepairCode.repairCode.ErrorActivityCode1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorActivityCode1__c, 'SCOrderRepairCode__c.ErrorActivityCode1__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorActivityCode2__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorActivityCode2__c, boRepairCode.repairCode.ErrorActivityCode2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorActivityCode2__c, 'SCOrderRepairCode__c.ErrorActivityCode2__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorSymptom1__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorSymptom1__c, boRepairCode.repairCode.ErrorSymptom1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorSymptom1__c, 'SCOrderRepairCode__c.ErrorSymptom1__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorType1__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorType1__c, boRepairCode.repairCode.ErrorType1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorType1__c, 'SCOrderRepairCode__c.ErrorType1__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorType2__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorType2__c, boRepairCode.repairCode.ErrorType2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorType2__c, 'SCOrderRepairCode__c.ErrorType2__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorLocation1__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorLocation1__c, boRepairCode.repairCode.ErrorLocation1__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorLocation1__c, 'SCOrderRepairCode__c.ErrorLocation1__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ErrorLocation2__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ErrorLocation2__c, boRepairCode.repairCode.ErrorLocation2__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ErrorLocation2__c, 'SCOrderRepairCode__c.ErrorLocation2__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ReturnCause__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ReturnCause__c, boRepairCode.repairCode.ReturnCause__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ReturnCause__c, 'SCOrderRepairCode__c.ReturnCause__c'));
                }
                
                if (SCBase.isSet(boRepairCode.repairCode.ReturnUsage__c) && 
                    !SCBase.isPicklistValue(SCOrderRepairCode__c.ReturnUsage__c, boRepairCode.repairCode.ReturnUsage__c))
                {
                    throw new SCfwException(SCfwException.fwExceptionTexts.getExceptionText(1330, boRepairCode.repairCode.ReturnUsage__c, 'SCOrderRepairCode__c.ReturnUsage__c'));
                }
            } // if (SCBase.isSet(appSettings.VALIDATE_PICKLIST_VALUES__c) && ...
        } // if ((mode & SCboOrderValidation.REPAIRCODE_DATA) == SCboOrderValidation.REPAIRCODE_DATA)
        
        return ret;
    } // validate
} // SCboOrderRepairCodeValidation