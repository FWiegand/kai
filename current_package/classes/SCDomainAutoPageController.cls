/*
 * @(#)SCDomainAutoPageController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * @author SS <sschrage@gms-online.de>
 */
public with sharing class SCDomainAutoPageController extends SCDomainAutoController
{
    /**
     * LangDE; LangENUS; LangNLNL - Change the UserLanguage
     **/
    public PageReference LangDE()
    {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        user.LanguageLocaleKey = 'de';
        Database.update(user);
        CurrentUserLanguage = UserInfo.getLanguage();
        return null;
    }//LangDE
    public PageReference LangENUS()
    {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        user.LanguageLocaleKey = 'en_US';
        Database.update(user);
        CurrentUserLanguage = UserInfo.getLanguage();
        return null;
    }//LangENUS
    public PageReference LangNLNL()
    {
        User user = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        user.LanguageLocaleKey = 'nl_NL';
        Database.update(user);
        CurrentUserLanguage = UserInfo.getLanguage();
        return null;
    }//LangNLNL
    
    
    
    //Global Variables
    public List<SelectOption> ListSelectObjOptionsColumn1 {get; private set;}
    public List<SelectOption> ListSelectObjOptionsColumn2 {get; private set;}
    public List<SelectOption> ListSelectObjOptionsColumn3 {get; private set;}
    public List<String> ListSelectObjColumn1 {get; set;}
    public List<String> ListSelectObjColumn2 {get; set;}
    public List<String> ListSelectObjColumn3 {get; set;}
    
    public Boolean FutureJobsRunning {get; private set;}
    private Map<String,Integer> OverviewFutureJobs = new Map<String,Integer>();
    
    public Boolean IsAdmin {get; private set;}
    public Boolean ShowAdminArea {get; private set;}
    public String LastStep {get; private set;}
    
    private List<SCDomainAutoRef__c> ListDomAutoRef = new List<SCDomainAutoRef__c>();
    
    public String strObjFieldDescriptionLanguages {get; private set;}
    public String strPicklistDescriptionLanguages {get; private set;}
    public String currentUserLanguage {get; private set;}
    
    private Set<String> jobHistory = new Set<String>();
    
    public Boolean controlDomainActivityRestart {get; private set;}
    public Boolean deleteDomValuesWithoutDesciptionsRestart {get; private set;}
    
    
    //Getter and Setter Methods
    public Integer getOverviewFutureJobsAbort() { return ((OverviewFutureJobs.get('A') == null) ? 0 : OverviewFutureJobs.get('A')); }
    public Integer getOverviewFutureJobsError() { return ((OverviewFutureJobs.get('E') == null) ? 0 : OverviewFutureJobs.get('E')); }
    public Integer getOverviewFutureJobsFinished() { return ((OverviewFutureJobs.get('F') == null) ? 0 : OverviewFutureJobs.get('F')); }
    public Integer getOverviewFutureJobsRunning() { return ((OverviewFutureJobs.get('R') == null) ? 0 : OverviewFutureJobs.get('R')); }
    public Integer getOverviewFutureJobsWaiting() { return ((OverviewFutureJobs.get('W') == null) ? 0 : OverviewFutureJobs.get('W')); }
    
    public Boolean getShowCheckedImgStep1() { return jobHistory.contains('1'); }
    public Boolean getShowCheckedImgStep2() { return jobHistory.contains('2'); }
    public Boolean getShowCheckedImgStep3() { return jobHistory.contains('3'); }
    public Boolean getShowCheckedImgStep4() { return jobHistory.contains('4'); }
    public Boolean getShowCheckedImgStep5() { return jobHistory.contains('5'); }
    public Boolean getShowCheckedImgStep6() { return jobHistory.contains('6'); }
    public Boolean getShowCheckedImgStep7() { return jobHistory.contains('7'); }
    
    
    //Methods for controlling the overview-page
    public PageReference changeShowAdminArea()
    {
        if (IsAdmin)
        {
            ShowAdminArea = (ShowAdminArea ? false : true);
            return null;
        }
        
        ShowAdminArea = false;
        
        return null;
    }//changeShowAdminArea
    
    
    
    /**
     * SCDomainAutoPageController - Constructor
     **/
    public SCDomainAutoPageController()
    {
        FutureJobsRunning = false;
        ShowAdminArea = false;
        CurrentUserLanguage = UserInfo.getLanguage(); 
        LastStep = '';
        jobHistory = new Set<String>();
        strObjFieldDescriptionLanguages = '';
        strPicklistDescriptionLanguages = '';
        
        //Enable Admin-Area
        User currentUser = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
        
        Set<Id> SetSysAdminProfileIds = new Set<Id>();
        for (Profile pro : [SELECT Id FROM Profile WHERE Name = 'System Administrator' OR Name = 'Systemadministrator'])
        {
            SetSysAdminProfileIds.add(pro.Id);
        }
        
        if (   ((!Test.isRunningTest()) && SetSysAdminProfileIds.contains(currentUser.ProfileId))
            || ((Test.isRunningTest()) && testIsAdmin)  )
        {
            IsAdmin = true;
        }
        else
        {
            IsAdmin = false;
        }
        
        //Read Object-SCDomainAutoRefs
        ReadObjectDomAutoRefs();
        
        init();
    }
    
    
    /**
     * init
     **/
    private void init()
    {
        CurrentUserLanguage = UserInfo.getLanguage(); 
        UpdateOverviewFutureJobs();
    }//init
    
    
    
    /**
     * ReadObjectDomAutoRefs - Reads Object-SCDomainAutoRefs
     **/
    private void ReadObjectDomAutoRefs()
    {
        ListSelectObjOptionsColumn1 = new List<SelectOption>();
        ListSelectObjOptionsColumn2 = new List<SelectOption>();
        ListSelectObjOptionsColumn3 = new List<SelectOption>();
        ListSelectObjColumn1 = new List<String>();
        ListSelectObjColumn2 = new List<String>();
        ListSelectObjColumn3 = new List<String>();
        
        ListDomAutoRef = [SELECT Id, ID2__c, isSelected__c,
                                 ObjectName__c, SFObjectName__c,
                                 TimeStampDomVal__c, TimeStampDomDes__c
                            FROM SCDomainAutoRef__c
                           WHERE Type__c = 'Object'
                        ORDER BY SFObjectName__c];
        
        if ((ListDomAutoRef != null) && (ListDomAutoRef.size() > 0))
        {
            Integer listSize = ListDomAutoRef.size();
            Integer length = (listSize / 3) + 1;
            
            for (SCDomainAutoRef__c domAutoRef : ListDomAutoRef)
            {
                String sfObjName = domAutoRef.SFObjectName__c;
                
                if (sfObjName != null)
                {
                    sfObjName = sfObjName.toLowerCase();
                    
                    if (ListSelectObjOptionsColumn1.size() < length)
                    {
                        ListSelectObjOptionsColumn1.add(new SelectOption(sfObjName,sfObjName));
                        if (domAutoRef.isSelected__c) { ListSelectObjColumn1.add(sfObjName); }
                    }
                    else if (ListSelectObjOptionsColumn2.size() < length)
                    {
                        ListSelectObjOptionsColumn2.add(new SelectOption(sfObjName,sfObjName));
                        if (domAutoRef.isSelected__c) { ListSelectObjColumn2.add(sfObjName); }
                    }
                    else
                    {
                        ListSelectObjOptionsColumn3.add(new SelectOption(sfObjName,sfObjName));
                        if (domAutoRef.isSelected__c) { ListSelectObjColumn3.add(sfObjName); }
                    }
                }
                else
                {
                    domAutoRef.isSelected__c = false;
                }
            }
        }
    }//ReadObjectDomAutoRefs
    
    
    
    /**
     * OverviewSelectObjCheckAll - Checks all Objets
     **/
    public void OverviewSelectObjCheckAll()
    {
        ListSelectObjColumn1 = new List<String>();
        ListSelectObjColumn2 = new List<String>();
        ListSelectObjColumn3 = new List<String>();
        
        for (SelectOption sOpt : ListSelectObjOptionsColumn1)
        {
            if (sOpt != null)
            {
                ListSelectObjColumn1.add(sOpt.getValue());
            }
        }
        
        for (SelectOption sOpt : ListSelectObjOptionsColumn2)
        {
            if (sOpt != null)
            {
                ListSelectObjColumn2.add(sOpt.getValue());
            }
        }
        
        for (SelectOption sOpt : ListSelectObjOptionsColumn3)
        {
            if (sOpt != null)
            {
                ListSelectObjColumn3.add(sOpt.getValue());
            }
        }
    }//OverviewSelectObjCheckAll
    
    
    
    /**
     * OverviewSelectObjCheckAll - Checks all Objets
     **/
    public void OverviewSelectObjUncheckAll()
    {
        ListSelectObjColumn1 = new List<String>();
        ListSelectObjColumn2 = new List<String>();
        ListSelectObjColumn3 = new List<String>();
    }//OverviewSelectObjUncheckAll
    
    
    /**
     * UpdateOverviewSelectObj - Updates the Overview of the selected objectes
     **/
    public void UpdateOverviewSelectObj()
    {
        ReadObjectDomAutoRefs();
    }//UpdateOverviewSelectObj
    
    
    /**
     * SaveSelectObj - Save object-choice
     **/
    public void SaveSelectObj()
    {
        Set<String> setSelectObj = new Set<String>();
        
        for (String selObj : ListSelectObjColumn1)
        {
            if (selObj != null) { setSelectObj.add(selObj.toLowerCase()); }
        }
        for (String selObj : ListSelectObjColumn2)
        {
            if (selObj != null) { setSelectObj.add(selObj.toLowerCase()); }
        }
        for (String selObj : ListSelectObjColumn3)
        {
            if (selObj != null) { setSelectObj.add(selObj.toLowerCase()); }
        }
        System.debug('+++###+++ ListSelectObjColumn1: ' + ListSelectObjColumn1);
        System.debug('+++###+++ ListSelectObjColumn2: ' + ListSelectObjColumn2);
        System.debug('+++###+++ ListSelectObjColumn3: ' + ListSelectObjColumn3);
        System.debug('+++###+++ setSelectObj: ' + setSelectObj);
        
        for (SCDomainAutoRef__c domAutoRef : ListDomAutoRef)
        {
            String sfObjName = domAutoRef.SFObjectName__c;
            
            if (sfObjName != null)
            {
                sfObjName = sfObjName.toLowerCase();
                
                if (setSelectObj.contains(sfObjName))
                {
                    domAutoRef.isSelected__c = true;
                }
                else
                {
                    domAutoRef.isSelected__c = false;
                }
            }
            else
            {
                domAutoRef.isSelected__c = false;
            }
        }
        
        Database.update(ListDomAutoRef);
        
        ReadObjectDomAutoRefs();
    }//SaveSelectObj
    
    
    
    /**
     * UpdateOverviewFutureJobs - Controls if a future-Job is currently running.
     * If there is a future-job "running" or "waiting", the global-variable "FutureJobsRunning" is true and buttons are disabled.
     * If there is no future-job "running" or "waiting", the global-variable "FutureJobsRunning" is false and buttons are disabled.
     **/
    public PageReference UpdateOverviewFutureJobs()
    {
        FutureJobsRunning = true;
        
        for (String key : new String[]{'A','E','F','R','W'}) { OverviewFutureJobs.put(key,0); }
        
        for (SCJobControl__c job : [SELECT Activity__c, Id FROM SCJobControl__c WHERE Activity__c IN ('A','E','F','R','W') AND Type__c LIKE 'SCDomainAutoController_%'])
        {
            String key = job.Activity__c.toUpperCase();
            Integer count = OverviewFutureJobs.get(key);
            count++;
            OverviewFutureJobs.put(key,count);
        }
        
        if (   ((OverviewFutureJobs.get('R') == null) || (OverviewFutureJobs.get('R') == 0))
            && ((OverviewFutureJobs.get('W') == null) || (OverviewFutureJobs.get('W') == 0)) )
        {
            FutureJobsRunning = false;
        }
        
        return null;
    }//UpdateOverviewFutureJobs
    
    
    
    /**
     * AbortFutureJobs - Aborts the current future-Jobs (which are waiting).
     **/
    public PageReference AbortFutureJobs()
    {
        List<SCJobControl__c> listJobs = new List<SCJobControl__c>();
        
        for (SCJobControl__c job : [SELECT Activity__c, Id FROM SCJobControl__c WHERE Activity__c IN ('R','W') AND Type__c LIKE 'SCDomainAutoController_%'])
        {
            job.Activity__c = 'A';
            listJobs.add(job);
        }
        
        if ((listJobs != null) && (listJobs.size() > 0))
        {
            Database.update(listJobs);
        }
        
        UpdateOverviewFutureJobs();
        
        return null;
    }//AbortFutureJobs
    
    
    
    /**
     * startControlDomainAutoRefsForObjects - Control the Global-AutoDomainRef and all Object-AutoDomainRefs
     *
     * Had to be called, if a new object is created in Salesforce.
     **/
    public void startControlDomainAutoRefsForObjects()
    {
        LastStep = '1';
        addToJobHistory(LastStep);
        
        strObjFieldDescriptionLanguages = '';
        strPicklistDescriptionLanguages = '';
        
        controlDomainAutoRefsForObjects();
    }//startControlDomainAutoRefsForObjects
    
    
    
    /**
     * startControlDomainValuesForObjectsZ - Create for every selected object a DomainValue for the DomainAutoRef 'objects__z'
     *
     * Had to be called, if a new object is selected.
     **/
    public void startControlDomainValuesForObjectsZ()
    {
        LastStep = '2';
        addToJobHistory(LastStep);
        
        strObjFieldDescriptionLanguages = '';
        strPicklistDescriptionLanguages = '';
        
        SaveSelectObj();
        try
        {
            controlDomainValuesForObjectsZ();
        }
        catch(Exception e) { }
    }//startControlDomainValuesForObjectsZ
    
    
    
    /**
     * startControlFieldsOfTheObjects - Create for every selected object his fields as DomainValue of the current object-DomainAutoRef
     *                                  Also controls the picklist-DomainAutoRef of the current objects.
     *
     * Had to be called, if a new object is selected or a new field of a selected object is created.
     * 
     * This methode might create future-jobs (SCJobControl__c.Type__c = 'SCDomainAutoController_ControlFieldsOfTheObjects')
     **/
    public void startControlFieldsOfTheObjects()
    {
        LastStep = '3';
        addToJobHistory(LastStep);
        
        strObjFieldDescriptionLanguages = '';
        strPicklistDescriptionLanguages = '';
        
        controlFieldsOfTheObjects();
        UpdateOverviewFutureJobs();
    }//startControlFieldsOfTheObjects
    
    
    
    /**
     * startControlPicklistFields - Reads for every selected picklistfields the picklist values and the picklist-properties from Salesforce
     *                              (for example: stores which picklistfield is a dependend one).
     *
     * Had to be called, if a new object is selected or a new field (picklist-field) of a selected object is created.
     * 
     * This methode might create future-jobs (SCJobControl__c.Type__c = 'SCDomainAutoController_ControlPicklistFields')
     **/
    public void startControlPicklistFields()
    {
        LastStep = '4';
        addToJobHistory(LastStep);
        
        strObjFieldDescriptionLanguages = '';
        strPicklistDescriptionLanguages = '';
        
        controlPicklistFields();
        UpdateOverviewFutureJobs();
    }//startControlPicklistFields
    
    
    
    /**
     * startControlDescriptionsObjectAndFields - Controls for all selected-Objects and Fields the SCDomainDescription.
     *
     * Had to be called, if a new object is selected or a new field of a selected object is created
     *                   or a label of a selected object in the current userLanguage has changed.
     * 
     * This methode might create future-jobs (SCJobControl__c.Type__c = 'SCDomainAutoController_ControlDescriptionsObjectAndFields')
     **/
    public void startControlDescriptionsObjectAndFields()
    {
        LastStep = '5';
        addToJobHistory(LastStep);
        
        String currentLang = UserInfo.getLanguage();
        currentLang = ((currentLang != null) && (currentLang.length() > 2)) ? currentLang.substring(0,2) : currentLang;
        strObjFieldDescriptionLanguages = (strObjFieldDescriptionLanguages != null && strObjFieldDescriptionLanguages != '') ? (strObjFieldDescriptionLanguages + ', ') : '';
        strObjFieldDescriptionLanguages += currentLang;
        
        controlDescriptionsObjectAndFields();
        UpdateOverviewFutureJobs();
    }//startControlDescriptionsObjectAndFields
    
    
    
    /**
     * startControlDescriptionsPicklistFields - Controls for all selected-PicklistFields the SCDomainDescription.
     *
     * Had to be called, if a new object is selected or a new picklistfield of a selected object is created
     *                   or a label of a selected picklistfield in the current userLanguage has changed.
     * 
     * This methode might create future-jobs (SCJobControl__c.Type__c = 'SCDomainAutoController_ControlDescriptionsPicklistFields')
     **/
    public void startControlDescriptionsPicklistFields()
    {
        LastStep = '6';
        addToJobHistory(LastStep);
        
        String currentLang = UserInfo.getLanguage();
        currentLang = ((currentLang != null) && (currentLang.length() > 2)) ? currentLang.substring(0,2) : currentLang;
        strPicklistDescriptionLanguages = (strPicklistDescriptionLanguages != null && strPicklistDescriptionLanguages != '') ? (strPicklistDescriptionLanguages + ', ') : '';
        strPicklistDescriptionLanguages += currentLang;
        
        ControlDescriptionsPicklistFields();
        UpdateOverviewFutureJobs();
    }//startControlDescriptionsPicklistFields
    
    
    
    /**
     * startControlDomainActivity - This method controls the Activity-field of DomainValues and DomainDescriptions
     **/
    public void startControlDomainActivity()
    {
        LastStep = '7';
        addToJobHistory(LastStep);
        controlDomainActivityRestart = false;
        
        Set<Id> setDomRefIds = new Set<Id>();
        for (SCDomainAutoRef__c domAutoRef : [SELECT Id, DomainRef__c FROM SCDomainAutoRef__c])
        {
            setDomRefIds.add(domAutoRef.DomainRef__c);
        }
        
        controlDomainActivityRestart = ControlDomainActivity(setDomRefIds);
    }//startControlAutoDomainSyntax
    
    
    
    /**
     * startDeleteDomValuesWithoutDesciptions - This method deletes all DomainValues, which have no DomainDescriptions
     *                                          Admins only
     **/
    public void startDeleteDomValuesWithoutDesciptions()
    {
        deleteDomValuesWithoutDesciptionsRestart = false;
        
        if (IsAdmin)
        {
            Set<Id> setDomRefIds = new Set<Id>();
            for (SCDomainAutoRef__c domAutoRef : [SELECT Id, DomainRef__c FROM SCDomainAutoRef__c])
            {
                setDomRefIds.add(domAutoRef.DomainRef__c);
            }
            deleteDomValuesWithoutDesciptionsRestart = DeleteDomValuesWithoutDesciptions(setDomRefIds);
        }
    }//startControlAutoDomainSyntax
    
    
    
    /**
     * startControlAutoDomainSyntax - Controls the AutoDomRefs in the Database (including DomainRef, DomainValue, DomainDescription)
     *                                Admins only
     **/
    public void startControlAutoDomainSyntax()
    {
        LastStep = '';
        addToJobHistory(LastStep);
        
        if (IsAdmin)
        {
            ControlAutoDomainSyntax();
        }
    }//startControlAutoDomainSyntax
    
    
    
    
    /**
     * addToJobHistory - Adds a Method-Number to the Set "jobHistory" 
     *                   Also controls, which jobs as to remove from the jobHistory
     * 
     * Info:
     * 1 -> startControlDomainAutoRefsForObjects     -> After this method, the following jobs had to be started: 2,3,4,5,6,7
     * 2 -> startControlDomainValuesForObjectsZ      -> After this method, the following jobs had to be started: 3,4,5,6,7
     * 3 -> startControlFieldsOfTheObjects           -> After this method, the following jobs had to be started: 4,5,6,7
     * 4 -> startControlPicklistFields               -> After this method, the following jobs had to be started: 5,6,7
     * 5 -> startControlDescriptionsObjectAndFields  -> After this method, the following jobs had to be started: 7
     * 6 -> startControlDescriptionsPicklistFields   -> After this method, the following jobs had to be started: 7
     * 7 -> startControlDomainActivity
     **/
    public void addToJobHistory(String newJob)
    {
        Set<String> newJobHistory = new Set<String>();   
        
        newJobHistory.add(newJob);
        if ((newJob != '1') && (newJob != ''))
        {
            for (String job : jobHistory)
            {
                if (   ((newJob == '2') && (job == '1'))
                    || ((newJob == '3') && (job == '1' || job == '2'))
                    || ((newJob == '4') && (job == '1' || job == '2' || job == '3'))
                    || ((newJob == '5') && (job == '1' || job == '2' || job == '3' || job == '4' || job == '6'))
                    || ((newJob == '6') && (job == '1' || job == '2' || job == '3' || job == '4' || job == '5'))
                    || ((newJob == '7') && (job == '1' || job == '2' || job == '3' || job == '4' || job == '5' || job == '6'))
                   )
                {
                    newJobHistory.add(job);
                }
            }
        }
        
        jobHistory = newJobHistory;
    }
    
    /**
     * Tests_ChangeIsAdmin - method for the testclass
     **/
    public static Boolean testIsAdmin = false;
}