/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class T_RedSurveyResultAfterInsertTest {

    static testMethod void testVisUpdate() {
        // TO DO: implement unit test
        
        Account testAcc = new Account();
		testAcc.Name = 'Test Account';
		testAcc.BillingCountry__c = 'DE';
		insert testAcc;
		
		Visitation__c testVis = new Visitation__c();
		testVis.Account__c = testAcc.Id;
		testVis.Type__c = 'SRP';
		testVis.Time__c = System.today();
		insert testVis;
		
		// valid RedSurveyResult__c
		LIST<RedSurveyResult__c> testRedSR = new LIST<RedSurveyResult__c>();
		RedSurveyResult__c testRedSR_1 = new RedSurveyResult__c();
		testRedSR_1.Account__c = testAcc.Id;
		testRedSR_1.VisitationId__c = testVis.Id;
		testRedSR.add(testRedSR_1);

		// empty VisitationId__c
		RedSurveyResult__c testRedSR_2 = new RedSurveyResult__c();
		testRedSR_2.Account__c = testAcc.Id;
		testRedSR.add(testRedSR_2);
		
		insert testRedSR;
    }
}