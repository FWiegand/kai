public class SCProduct
{
    /**
     * The product class contains all relevant data that we need to show 
     * in the product search dialogues.
     *
     * We have to use this additional class since SCProduct__c refers to 
     * domain values and doesn't contain any translations.
     */
    public SCProduct (Id productId)
    {
        this.productId = productId;
    }


    public SCProduct ()
    {

    }

    // SF ID of a product record
    public Id productId { get; set; }

    // translated product string
    public String product { get; set; }

    // SF ID of found domain value of DOM_PRODUCTGROUP
    public Id productGroupId { get; set; }

    // SF ID of found domain value of DOM_PRODUCTUNITCLASS
    public Id unitclassId { get; set; }
        
    // translated unit class string
    public String unitClass { get; set; }
    
    // SF ID of found domain value of DOM_PRODUCTUNITTYPE
    public Id unitTypeId { get; set; }
        
    // translated unit type string
    public String unitType { get; set; }

    // brand object, no need for translation
    public String brand { get; set; }
        
    // SF ID of the found brand object
    public Id brandId { get; set; }
}