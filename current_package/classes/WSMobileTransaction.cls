/*
 * @(#)WSMobileTransaction.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implements a generic web service handler that can be called by the 
 * CMS gateway to forward transactions from the mobile gateway.
 *
 * @author narmbruster@gms-online.de
 * @author mautenrieth@gms-online.de
*/ 
global without sharing class WSMobileTransaction
{

    public static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
   /** 
    * Use this method to receive a transaction call from the Gateway and dispatch it to
    * some business-logic for performing the heavy-lifting. 
    *
    * NOTE: Read the Howto below for directions on how to _sensibly_ extend this WebService
    *
    * @param  trans the name of the transaction to be executed, needed for dispatching
    * @param  items a list of JSON objects encoded as Strings
    *
    * @return a list of JSON Objects encoded as Strings. As of now the list contains just one JSON-Object.
    */
    WebService static String[] call(String trans, List<String> items)
    {
        /* How to extend this WebService
         * ========================================
         * 1.) Simply add an 'else if' branch, testing against your transaction's name,
         *     which performs the necessary computations. 
         *
         * 2.) Keep this WebService compact and tidy and refactor large chunks of code
         *     into appropriate business-objects 'SCmobXXX' (create a new one, if you must)
         *
         * 3.) Use the 'SCmobResult' class to send data back. You can send your 
         *     custom data using the 'data' field of 'SCmobResult'
         *
         * 4.) Fill the 'status' and 'message' field of the 'SCmobResult' object with meaningful
         *     data so the client can check the outcome of the transaction. Status can be 
         *     either 'E000' indicating success or 'EXXX' with XXX > 101 for indicating 
         *     a custom Error, depending on the transaction. 'E101' is reserved for the case
         *     that a transaction is not implemented/supported. The 'message' field should contain
         *     some sensible text to present to the user.
         *
         * 5.) Use this WebService for good, not evil!
         * 
         */

        //--<helloWorld transaction>----------------------------------------------------
        // A simple transaction to serve as a demonstration and reference implementation
        if (trans == 'helloWorld') 
        {
            // unmarshal the Input
            List<SCmobHelloWorld.Input> input = new List<SCmobHelloWorld.Input>();
            for (String item : items)
            {
                SCmobHelloWorld.Input indata = (SCmobHelloWorld.input) JSON.deserialize(item, SCmobHelloWorld.Input.class);
                input.add(indata);
            } 
            
            // Call the Business Logic to obtain a Result Object
            SCmobResult res = SCmobHelloWorld.exec(input);
            
            // Marshal the result and output it. 
            return new String[] { JSON.serialize(res) };
        } 
        //--<echo transaction>-----------------------------------------------------------
        // You can use this to send data roundtrip
        else if (trans == 'echo') // Echoes back what you send!
        {
            if (items.size() > 0)
            {
                return items;
            }
            else
            {
                SCmobResult res = new SCmobResult('E102');
                res.message = 'You did not send any data to echo.';
                return new String[] { JSON.serialize(res) };
            }
        }
        //--<off-route order creation>-----------------------------------------------------------
        // Allows technicians to schedule appointsments directly 
        else if (trans == 'createOffRouteOrder')
        {
            SCmobResult result;
            
            // unmarshal items[0] into Input 
            if (items.size() > 0)
            {   
                SCmobCreateOffRouteOrder.Input input = (SCmobCreateOffRouteOrder.Input) JSON.deserialize(items[0], SCmobCreateOffRouteOrder.Input.class);
             
                result = SCmobCreateOffRouteOrder.exec(input);
            }
            else
            {
                result = new SCmobResult('E110'); // No Input Data given
                result.message = 'You must at least specify one Order to create.';
            }                
            return new String[] { JSON.serialize(result) };
        }
        //--<default / error case>-------------------------------------------------------
        // if we don't know the transaction or it isn't yet implemented
        else 
        {
            SCmobResult res = new SCmobResult('E101');
            res.message = 'This Service does not support the ' + trans + ' transaction.';
            return new String[] { JSON.serialize(res) };
        }
        
    } 
       
}