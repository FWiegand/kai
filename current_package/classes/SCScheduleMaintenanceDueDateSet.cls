/* 
 * @(#)SCScheduleMaintenanceDueDateSet .cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

global class SCScheduleMaintenanceDueDateSet implements Schedulable
{
    global void execute(SchedulableContext SC) 
    {
        // Alle
        SCbtcMaintenanceDueDateSet.asyncCalculateAll(0, 'no trace');
    }
}