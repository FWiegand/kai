/*
 * @(#)SCInstalledBaseSearchSimpleController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class implements the simple search for the installed base
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInstalledBaseSearchSimpleController
{
    public SCStock__c stock { get; set; }
    public List<SCInstalledBase__c> foundedInstalledBases { get; set; }
    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();   
    public String searchText { get; set; }
    public String selectedStock { get; set; }
    public String selectedStockDefault { get; set; }
    public String numberOfIbs { get; set; }
    private String selectedStockName;   // the name of selected stock (of a BU)
    private String selectedPlantName;   // the name of plant of the selected (stock.Plant__r.Name)
    public Boolean su;
    
    public Integer pageSize = 100;
    
    private Boolean isEquipmentNew;
    
    public User currentUser;
    public SCBusinessUnit__c defBusinessUnit;
    public ApexPages.StandardSetController resultsSetController { get; set; }
    public ID userId {get; set;}    // current   user to search for (for test class)
    private String stockPropBU;     // property name of the stock of the BU to read, added 7.2013
                                    // details see contructor 

    /**
     * existOldOrderForThisEquipment - Controls if there is an old Order for this installedBase
     * @param userIdTest       - user Id, can be null than the cuurent user is used 
     *                           but for testing here another user can be given
     *                           used for determining the buisiness unit 
     * @param stockPropFromBU  - the name of the stock field of the users buisiness unit 
     *                           with the stock from which the installed bases are read
     *        possible values for stockPropBU: 
     *          Stock__c     = normal stock of BU 
     *          StockWSRFR__c = "Workshop stock ready for refurbishment" - this is needed for workshop wizard
     *          StockWSRFS__c = "Workshop stock ready for scrapping
     *          StockWSRTM__c = "Workshop stock ready to market " - default (if null is given or stock field name is unknown)
     *          StockWS__c   = not yet supported       
     **/
    public SCInstalledBaseSearchSimpleController(ID userIdTest, String stockPropFromBU) 
    {
        System.debug('#### SCInstalledBaseSearchSimpleController constructor with (test) user:' + userIdTest);
        this.userId = userIdTest;   // can be null (than current user will be selected later). To set in test cases 
                
        initialize(stockPropFromBU);
    }

    public SCInstalledBaseSearchSimpleController()
    {
        initialize(null);
    }
    
    public SCInstalledBaseSearchSimpleController(Integer customPageSize)
    {
        if(customPageSize != null && customPageSize > 0)
        {
            pageSize = customPageSize;
        }
        initialize(null);
    }
    
   /*
    * Initializes the variables, reads data, performs an initial validation 
    */
    public void initialize(String stockPropFromBU)
    {
        // for possible values for stockPropBU see constructor param
        if( stockPropFromBU != null &&
            (stockPropFromBU.equalsIgnoreCase('StockWSRFR__c') ||   // note: == operator is case-insensitive only in Apex pages   
             stockPropFromBU.equalsIgnoreCase('StockWSRFS__c') ||
             stockPropFromBU.equalsIgnoreCase('StockWSRTM__c') ||
             stockPropFromBU.equalsIgnoreCase('Stock__c')))
        {   // field name is accepted 
            this.stockPropBU = stockPropFromBU;             
        }
        else
        {   // set default field name
            this.stockPropBU = 'StockWSRTM__c'; // default because first implementation used this stock
            System.Debug('##### Warning in SCInstalledBaseSearchSimpleController: Given stock property name of BusinessUnit [' + stockPropFromBU +  '] not set correctly. Using default [' + this.stockPropBU + ']: #####');       
        }
        
        foundedInstalledBases = new List<SCInstalledBase__c>();
        defBusinessUnit = new SCBusinessUnit__c();
        stock = new SCStock__c();
        searchText = '';
        numberOfIbs = '';
        String stockBU = '';    // stock ID of relevant BU-stock given in stockPropBU   
                
        if (userID == null)
        {
            userID = UserInfo.getUserId();
        }
        currentUser = [ Select Id, Name, DefaultBusinessUnit__c From User Where Id = : userID ];

        System.Debug('##### initialize() BusinessUnit of user [' + currentUser.Name + ']: ' + currentUser.DefaultBusinessUnit__c + ' #####');       
        
        if( ApexPages.currentPage().getParameters().containsKey('su') && ApexPages.currentPage().getParameters().get('su') == 'true' )
        {
            su = true;
        }
        else
        {
            su = false;
        }
        
        //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
        String eqNewParam = ApexPages.currentPage().getParameters().get('isEquipmentNew');
        this.isEquipmentNew = (eqNewParam != null && eqNewParam == 'true' ) ? true : false;
       
        
        if ( ApexPages.currentPage().getParameters().containsKey('sid') && ApexPages.currentPage().getParameters().get('sid') != '' )
        {
            stock = readStock(ApexPages.currentPage().getParameters().get('sid'));
                      
            selectedStock = stock.id;
            selectedStockDefault = stock.id;
            selectedStockName = stock.name;
            selectedPlantName = stock.Plant__r.Name;
            System.Debug('##### initialize() stock of sid [' + stock.id + ']: ' + stock.name + ' #####');       
            search();
        }
        else
        {
            // Reading default business unit
            if (currentUser.DefaultBusinessUnit__c != null && 
                String.isNotBlank(currentUser.DefaultBusinessUnit__c))
            {
                defBusinessUnit =  [ Select Id, StockWSRTM__c, StockWSRFR__c, StockWSRFS__c, Stock__c, Name 
                                     From SCBusinessUnit__c 
                                     Where Name = : currentUser.DefaultBusinessUnit__c ];
                if(stockPropBU.equalsIgnoreCase('Stock__c'))
                    stockBU = defBusinessUnit.Stock__c;
                else if(stockPropBU.equalsIgnoreCase('StockWSRFR__c'))
                    stockBU = defBusinessUnit.StockWSRFR__c;
                else if(stockPropBU.equalsIgnoreCase('StockWSRFS__c'))
                    stockBU = defBusinessUnit.StockWSRFS__c;
                else    // default
                    stockBU = defBusinessUnit.StockWSRTM__c;                
                System.Debug('##### initialize() stock [' + stockPropBU + '] of users def BusinessUnit [' + defBusinessUnit.Name + ']: ' + ' - ' + stockBU + ' #####');
            }
            
            if(defBusinessUnit != null && stockBU != null && String.isNotBlank(stockBU))
            {                          
                stock = readStock(stockBU);
                          
                selectedStock = stock.id;
                selectedStockDefault = stock.id;
                selectedStockName = stock.name;
                selectedPlantName = stock.Plant__r.Name;
                System.Debug('##### initialize() stock of BusinessUnit [' + defBusinessUnit.Name + '] taken: ' + stockBU + ' - ' + stock.name + ' #####');       
                search();
            }
            else if(String.isNotBlank(applicationSettings.DEFAULT_BUSINESSUNITNO__c))
            {
                defBusinessUnit =  [ Select Id, StockWSRTM__c, StockWSRFR__c, StockWSRFS__c, Stock__c, Name, StockWSRFR__r.Name 
                                     From SCBusinessUnit__c 
                                     Where Name = : applicationSettings.DEFAULT_BUSINESSUNITNO__c ];
                System.Debug('##### initialize() BusinessUnit of applicationSettings.DEFAULT_BUSINESSUNITNO__c taken: ' + applicationSettings.DEFAULT_BUSINESSUNITNO__c + ' #####');       
                
                if(stockPropBU.equalsIgnoreCase('Stock__c'))
                    stockBU = defBusinessUnit.Stock__c;
                else if(stockPropBU.equalsIgnoreCase('StockWSRFR__c'))
                {
                    stockBU = defBusinessUnit.StockWSRFR__c;
                    System.Debug('##### initialize() stockWSRFR of BusinessUnit [' + defBusinessUnit.Name + ' ' + defBusinessUnit.Id + '] added: ' + ' ' + defBusinessUnit.StockWSRFR__r.Name + ' ' + defBusinessUnit.StockWSRFR__r.Id + ' #####'); 
                }           
                else if(stockPropBU.equalsIgnoreCase('StockWSRFS__c'))
                    stockBU = defBusinessUnit.StockWSRFS__c;
                else    // default
                    stockBU = defBusinessUnit.StockWSRTM__c;                
                System.Debug('##### initialize() stock [' + stockPropBU + '] of appSettings default BusinessUnit [' + defBusinessUnit.Name + ']: ' + ' - ' + stockBU + ' #####');
                if(defBusinessUnit != null && stockBU != null)
                {                              
                    stock = readStock(stockBU);
                              
                    selectedStock = stock.id;
                    selectedStockDefault = stock.id;
                    selectedStockName = stock.name;
                    selectedPlantName = stock.Plant__r.Name;
                    search();
                }
                else
                {
                    System.Debug('##### initialize() stock [' + stockPropBU + '] of appSettings default BusinessUnit [' + defBusinessUnit.Name + '] is null - no search #####');                    
                }
            }
            System.Debug('##### initialize() stock of BusinessUnit [' + defBusinessUnit.ID + ']: ' + stockBU + ' - ' + stock.name + ' #####');       
        } // else
    }
    
    public SCStock__c readStock(String newId)
    {
        SCStock__c stock = new SCStock__c();
        
        String equipment = '';
        if(su)
        {
            // Only sub equipments
            equipment = ' Where cce_EquipmentTypeFilter__c = \'SU\' ';
        }
        else
        {
            // Only equipments
            equipment = ' Where Parent__c = null And cce_EquipmentTypeFilter__c = \'EQ\' And InstalledBaseLocation__c = null ';
        }
        
        String stockQuery;
        stockQuery = ' Select Id, Name, Plant__r.Name, (Select Id From InstalledBase__r ' +
                   + equipment + ' ) ' +
                   + ' From SCStock__c Where Id = : newId Limit 10000';
        
        try
        {
            stock = Database.query(stockQuery);
            numberOfIbs = String.valueOf(stock.InstalledBase__r.size());
            return stock;
        }
        catch(Exception e)
        {
            System.debug('#### IBSimple: Cannot read stock!');
            return null;
        }
    }
    
    // List of stocks to select for CB 
    public List<SelectOption> getStocks()
    {
        List<SelectOption> options = new List<SelectOption>();      
        
        // show the name of plant too 
        options.add(new SelectOption(selectedStock,SelectedPlantName + '/' + selectedStockName + ' [' + numberOfIbs + ']'));

        String buName = '';
        Set<Id> stId = new Set<Id>();
        System.Debug('##### IBSimple.getStocks() of def BusinessUnit [' + defBusinessUnit.Name + ' ' + defBusinessUnit.Id + ']  #####'); 
        for(SCBusinessUnit__c u : [ Select Id, Name, StockWSRTM__r.Id, StockWSRFR__r.Id, StockWSRFS__r.Id, Stock__r.Id, StockWSRFR__r.Name, StockWSRTM__r.Name, Plant__c
                                   From SCBusinessUnit__c
                                   Where Parent__c = : defBusinessUnit.id ])
        {
            //stId.add(u.StockWSRTM__r.Id);
            if(stockPropBU == 'Stock__c')
                    stId.add(u.Stock__r.Id);            
            else if(stockPropBU == 'StockWSRFR__c')
            {
                stId.add(u.StockWSRFR__r.Id);
                System.Debug('##### IBSimple.getStocks() stockWSRFR of BusinessUnit [' + u.Name + ' ' + u.Id + '] added: ' + ' ' + u.StockWSRFR__r.Name + ' ' + u.StockWSRFR__r.Id + ' #####'); 
            }
            else if(stockPropBU == 'StockWSRFS__c')
                stId.add(u.StockWSRFS__r.Id);
            else // default
            {
                stId.add(u.StockWSRTM__r.Id);
                System.Debug('##### IBSimple.getStocks() stockWSRTM of BusinessUnit [' + u.Name + ' ' + u.Id + '] added: ' + ' ' + u.StockWSRTM__r.Name + ' ' + u.StockWSRFR__r.Id + ' #####'); 
            }
            buName = u.Name;            
        }    
        
        String equipment = '';
        if(su)
        {
            // Only sub equipments
            equipment = ' Where cce_EquipmentTypeFilter__c = \'SU\' ';
        }
        else
        {
            // Only equipments
            equipment = ' Where Parent__c = null And cce_EquipmentTypeFilter__c = \'EQ\' And InstalledBaseLocation__c = null ';
        }
        
        String stockQuery;
        stockQuery = ' Select Id, Name, Plant__r.Name, (Select Id From InstalledBase__r ' +
                   + equipment + ' ) ' +
                   + ' From SCStock__c Where Id IN : stId Order By Name ';
        System.Debug('##### IBSimple.getStocks() equipments of BusinessUnit [' + buName + ']: ' + ' stockQuery:' + stockQuery + ' #####'); 
                   
        List<SCStock__c> st = Database.query(stockQuery);
        
        for(SCStock__c s : st)
        {           
            SelectOption opt = new SelectOption(s.Id,s.Plant__r.Name + '/' + s.Name + ' [' + s.InstalledBase__r.size() + '] ');
            if(s.InstalledBase__r.size() == 0)
            {
                opt.setDisabled(true);
            }            
            options.add(opt);
        }        
        
        return options;
    }

    // Main search function. search equipments (SCInstalledBase)  
    public PageReference search()
    {
        //if(String.isNotBlank(searchText) || String.isNotBlank(selectedStock))
        //{               
            searchText = '%' + searchText + '%';
            
            String stockSearch = '';
            
            if(String.isNotBlank(selectedStockDefault))
            {
                stockSearch = ' And Stock__c = \'' + selectedStockDefault + '\' ';
                System.debug('#### IBSimple selectedStockDefault: ' + selectedStockName + ' ' + selectedStockDefault);
            }
            
            // If only sub equipments must be selected
            String equipment = '';
            /*if(isEquipmentNew)
            {
                equipment = ' Where InstalledBaseLocation__c = null And Parent__c = null ';
            }
            else */
            if(su)
            {
                //GMSET 31.07.2013: Why do we allow sub equipment selection of already assigned items? 
                // Only sub equipments
                //equipment = ' Where cce_EquipmentTypeFilter__c = \'SU\' ';
                
                
                equipment = ' Where cce_EquipmentTypeFilter__c = \'SU\' And InstalledBaseLocation__c = null And Parent__c = null ';
            }
            else
            {
                // Only equipments
                equipment = ' Where cce_EquipmentTypeFilter__c = \'EQ\' And InstalledBaseLocation__c = null And Parent__c = null ';
            }
            
            String freeSearch = '';
            if(String.isNotBlank(searchText))
            {
                freeSearch += '    And (Name Like \'' + searchText + '\' OR ' +
                           + '         IdExt__c Like \'' + searchText + '\' OR ' +
                           + '         IdInt__c Like \'' + searchText + '\' OR ' +
                           + '         ArticleNo__c Like \'' + searchText + '\' OR ' +
                           + '         ArticleName__c Like \'' + searchText + '\' OR ' +
                           + '         SerialNo__c Like \'' + searchText + '\' OR ' +
                           + '         ManufacturerSerialNo__c Like \'' + searchText + '\')';
            }                        
            
            String searchQuery;    
            
            // GMSSU 10.09.2013 PMS 35892/INC0064786: Zuordnung von EQ bei Aufstellung nur "gebraucht" oder "neu"
            // For the order type ZC04 (5705) only two equipment status are allowed: E0006 & E0007
            String statusFilter = '';
            if(ApexPages.currentPage().getParameters().containsKey('otype'))
            {
                if(ApexPages.currentPage().getParameters().get('otype') == '5705' || ApexPages.currentPage().getParameters().get('otype') == '5711')
                {
                    statusFilter = ' And (Status__c = \'E0006\' OR Status__c = \'E0007\') ';
                }
            }
                   
            searchQuery = ' Select util_StockPlant__c, eps_ProductUnitTypeId__c, eps_ProductUnitClassId__c, eps_ProductGroupId__c,  ' +
                        + '         cce_isOwnedByCustomer__c, cce_ValuationTypeProposal__c, cce_ReadyForRefurbishment__c, Width__c,  ' +
                        + '           Weight__c, WGPlus__c, ValidationStatus__c, Type__c, TechnicalObjType__c, System__c, SystemModstamp,  ' +
                        + '           Stock__c, Stock__r.Name, Stock__r.util_StockSelector__c, StockInfo__c, Status__c, StatusText__c, Sort__c, SerialNo__c, SerialNoException__c,  ' +
                        + '           SafetyStatus__c, SafetyNote__c, SafetyAction__c, Safety2__c, Safety1__c, Room__c, cce_EquipmentTypeFilter__c, ' +
                        + '           MaintenancePlant__c, Quantity__c, PurchaseWholesaler__c, PurchasePrice__c, PurchaseOrderNo__c,  ' +
                        + '           PurchaseOrderDate__c, PurchaseInfo__c, PurchaseDate__c, ProductUnitType__c, ProductUnitClass__c,  ' +
                        + '           ProductSkill__c, ProductPower__c, ProductNameCalc__c, ProductModel__c, ProductModel__r.Name, ProductGroup__c, ProductEnergy__c,  ' +
                        + '           PriceList__c, Plant__c, PlantID__c, PlannerGroup__c, Phone__c, Parent__c, OwnerId, Name,   ' +
                        + '           ManufacturerSerialNo__c, LocationName__c, LastModifiedDate, LastModifiedById, ' +
                        + '           IsDeleted, InstalledBaseLocation__c, InstallationDate__c, IdInt__c, IdExt__c, Id, ID2__c, Height__c,  ' +
                        + '           HandoverDate__c, GuaranteeUntil__c, GuaranteeStandard__c, GuaranteeExtended__c, Floor__c, FieldTest__c,  ' +
                        + '           Description__c, Department__c, DeliveryDate__c, DeinstallationDate__c, CustomerWarrantyStart__c,  ' +
                        + '           CustomerWarrantyEnd__c, CurrencyIsoCode, CreatedDate, CreatedById, CostCenter__c, ControllingArea__c,  ' +
                        + '           ContractStartDate__c, ContractRef__c, ConstructionYear__c, ConstructionWeek__c, CompanyCode__c,  ' +
                        + '           CommissioningStatus__c, CommissioningDate__c, ComBoxVariant__c, ComBoxStatus__c, Building__c, Brand__c,  ' +
                        + '           BookValue__c, BookValueCurrency__c, AuditLast__c, AuditHint__c, Article__c, Article__r.Id, ArticleNo__c,  ' +
                        + '           ArticleName__c, ArticleEAN__c, AcquisitionValue__c, AcquisitionCurrency__c, Brand__r.Id, Article__r.Name, ' +
                        + '           Brand__r.name ' +
                        + '    From SCInstalledBase__c  ' +
                        +      equipment +
                        +      stockSearch +
                        +      statusFilter +
                        +      freeSearch +
                        + '    Order By ProductNameCalc__c  Limit 10000 ';            
            System.debug('#### IBSimple searchQuery executed: ' + searchQuery.normalizeSpace());
            
            //foundedInstalledBases = Database.query(searchQuery);            
            resultsSetController = new ApexPages.StandardSetController(Database.getQueryLocator(searchQuery));
            resultsSetController.setPageSize(pageSize);
            
            foundedInstalledBases = (List<SCInstalledBase__c>)resultsSetController.getRecords();
            
            searchText = searchText.remove('%');
        //}
                                    
        return null;
    } // search
    

    
    public List<SCInstalledBase__c> getAppliances()
    {
        return (List<SCInstalledBase__c>)resultsSetController.getRecords();
    }
}