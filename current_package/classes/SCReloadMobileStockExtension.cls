/*
 * @(#)SCStockCorrectionExtension.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
 
public class SCReloadMobileStockExtension 
{
	public SCStock__c stock {get; private set;}
	
	public SCReloadMobileStockExtension(ApexPages.StandardController controller)
	{
		this.stock = (SCStock__c)controller.getRecord();
		system.debug('#### stock: ' + stock);
	}
	
	public Integer getStockItemsCount()
	{
		AggregateResult[] aRes = 
		[
			SELECT
				COUNT(Id) cnt
			FROM
				SCStockItem__c
			WHERE
				Stock__c = :this.stock.Id
			
		];
		return Integer.valueOf(aRes[0].get('cnt'));
	}
	
	public void onReload()
	{
		SCboStock.reloadMobileStockById(stock.Id);
	}
	
	public void onCancel()
	{
		//nothing to do
	}
	
}