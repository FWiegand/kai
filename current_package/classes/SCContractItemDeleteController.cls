/*
 * @(#)SCContractItemDeleteController.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller delete selected installed bases (contract items)
 * for the actual contract and returns user back to contract page.
 *
 */
public with sharing class SCContractItemDeleteController
{  
    String contractId; 
    String contractno;

    List<SCContractItem__c> selectedItems;

    public SCContractItemDeleteController(ApexPages.StandardSetController controller) 
    {
        selectedItems = (List<SCContractItem__c>) controller.getSelected();
        
        contractId = ApexPages.currentPage().getParameters().get('id');
        contractno = [select name from SCContract__c where id = :contractId].name;
    }
    
    public SCContractItemDeleteController()
    {
    }
    
    
    /**
     * Delete selected contract items and returns to the contract page
     *
     * @return    Reference to contract page
     *
     * @author Sergey Utko <sutko@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference deleteItems()
    {
        try
        {
            // remove the contract reference to the installed base items (redundant but needed to increase the performance)
            List<SCInstalledBase__c> ibitems = [select id, contractref__c from SCInstalledBase__c where id in (select installedbase__c from SCContractItem__c where id in :selectedItems)];            
            for(SCInstalledBase__c ib : ibitems)
            {
                if(ib.contractref__c == null)
                {
                    // do nothing 
                }
                else
                {
                    if(!ib.contractref__c.contains(contractno))
                    {    
                        // ensure that no "," are left behind
                        ib.contractref__c = ib.contractref__c.replace(',' + contractno, '');
                        ib.contractref__c = ib.contractref__c.replace(contractno, '');
                    }
                }
            }
            update ibitems;
        
            delete selectedItems;
        }
        catch(Exception e)
        {
        }
        
        PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
        pageRef.setRedirect(true);
        
        return pageRef.setRedirect(true);
    }
}