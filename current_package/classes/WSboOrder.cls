/*
 * @(#)WSboOrder.cls 
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the web services to manipulate (service) orders
 * - canCancelOrder
 * - canCancelAppointments
 * - cancelOrder
 * - cancelAppointments
 * - archiveOrderDocuments
 * -
 */
global without sharing class WSboOrder
{
    //-------------------------------------------------------------------------
    // API Interface Method - canCancelOrder
    //-------------------------------------------------------------------------
    
   /*
    * Checks if the order can be cancelled (based on order and invoicing status) 
    * @param oid      id of the SCOrder that has to be cancelled
    * @return             'OK' if the order can be cancelled or an describing text if not
    */
    WebService static String canCancelOrder(String oid)
    {
        return SCboOrder.canCancelOrder(oid);
    }


    //-------------------------------------------------------------------------
    // API Interface Method - canCancelAssignments
    //-------------------------------------------------------------------------
    
   /*
    * Checks if the assignments can be cancelled (based on order and invoicing status) 
    * @param oid      id of the SCOrder whose assignemts are to be cancelled
    * @return             'OK' if the assignments can be cancelled or an describing text if not
    */
    WebService static String canCancelAssignments(String oid)
    {
        return SCboOrder.canCancelAssignments(oid);
    }
    
    

    //-------------------------------------------------------------------------
    // API Interface Method - canCancelOrder
    //-------------------------------------------------------------------------
    
   /*
     * Can be used to cancel an order and the appointments. Please note that this
     * function does not check the order status. Please ensure on caller level
     * that this order can be cancelled.
     *
     * @param oid      id of the SCOrder that has to be cancelled
     * @param cancelReason the reason for the cancellation SCOrder.CancellReason or null
     * @param info         an optional information text that is added to the field history or null
     * @return             always 'OK'
     */
    WebService static String cancelOrder(String oid, String cancelReason, String info)
    {
        SCboOrder.cancelOrder(oid, cancelReason, info);
        return 'OK';    
    }
    
    
    //-------------------------------------------------------------------------
    // API Interface Method - canCancelAssignments
    //-------------------------------------------------------------------------
    
    /*
     * Cancelles all pending assignments of an order.
     *
     * @param oid      id of the SCOrder that has to be cancelled
     * @param info         an optional information text that is added to the field history or null
     * @return             always 'OK'
     */
    WebService static String cancelAssignments(String oid, String info)
    {
        SCboOrder.cancelAssignments(oid, info);
        return 'OK';    
    }
    
    
    //-------------------------------------------------------------------------
    // API Interface Method - canCancelAssignments
    //-------------------------------------------------------------------------
    
    /*
     * Cancelles all pending assignments of an order.
     *
     * @param oid      id of the SCOrder that has to be cancelled
     * @return 'OK' if the the functions runs ok, otherwise 'NO_ATTACHMENTS' or 'WRONG_ID'
     * @author Sergey Utko <sutko@gms-online.de>
     */
    WebService static String archiveOrderDocuments(String oid)
    {
        SCboOrder boOrder = new SCboOrder();
        String returnValue = boOrder.archiveOrderDocuments(oid); 
        
        return returnValue; 
    }
}