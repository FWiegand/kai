/*
 * @(#)SCArticleSearchLookupController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implement a custom lookup to allow a highly sophisticated article search.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCArticleSearchLookupController {

    /**
     * Default constructor
     */
    public SCArticleSearchLookupController()
    {
        clear();
        userCountry = SCBase.getUserCountry();
        System.debug('##### SCArticleSearchLookupController(): userCountry -> ' + userCountry);
        helperArticle = new SCArticle__c();
    }
    
    public List<SelectOption> getLockTypesList()
    {
        List<SelectOption> options = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = SCArticle__c.LockType__c.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for(Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }       
        return options;
    }

    public static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();

    // The actual query string to search in the article
    private transient String articleQuery;

    private transient List<Article> foundArticles;

    private transient Date xToday = Date.today();

    private static SelectOption noneOption = new SelectOption('', System.Label.SC_flt_None);

    // The brand that is selected from the select options
    private transient String selectedBrand = '-1';
    
    public Boolean getShowBrandSelectList()
    {
        if (applicationSettings.DISABLE_BRAND_IN_ARTICLESEARCH__c == '1')
        {
            return false;
        }
        return true;
    }

    // a set of article lock types which should be used 
    private Set<String> articleLockTypes = null;

    // The country of the current user
    private String userCountry;

    // Helper object for output the picklist values at the VF-Page (gmssu 18.11.2011)
    public SCArticle__c helperArticle { get; set; }    

    // list of locktypes for selecting articles
    public String lockTypes { get; set; }    


    /**
     * Getter for the article query string
     *
     * @return Article query string
     */
    public String getSelectedBrand()
    {
        return selectedBrand;
    }

    /**
     * Setter for the article query string
     *
     * @param Article query string
     */
    public void setSelectedBrand(String value)
    {
        System.debug('#### setSelectedBrand(): brand -> ' + value);
        this.selectedBrand = value;
    }

    /**
     * Getter for the article query string
     *
     * @return Article query string
     */
    public String getArticleQuery()
    {
        return articleQuery;
    }

    /**
     * Setter for the article query string
     *
     * @param Article query string
     */
    public void setArticleQuery(String value)
    {
        articleQuery = value;
    }


    /**
     * List of unit classes of articles to consider in the search
     */
    public List<String> searchClasses { get; set; }

    /**
     * String of unit classes to consider
     * Since the component couldn't get a List object directly
     * we have to use this one and fill the list here.
     */
    public String articleClass
    {
        get;
        set
        {
            this.articleClass = value;

            if (value != null && value.length() > 0)
            {
                searchClasses = value.split(',');
            }
            else
            {
                searchClasses = null;
            }
        }
    }

    /**
     * List of unit types of articles to consider in the search
     */
    public List<String> searchTypes { get; set; }

    /**
     * String of unit types to consider
     */
    public String unitTypes
    {
        get;
        set
        {
            this.unitTypes = value;

            if (value != null)
            {
                searchTypes = value.split(',');
            }
        }
    }

    /**
     * The search mode changes the way that articles are searched
     * The default mode search on the articles taking unit class
     * and unit type into account (if set).
     * Other modes search on the van stock of the current user only
     * for example.
     *
     * search modes:
     * (null|0): default search
     * 1: search in the current user's stock only
     * 2: search in the current user's stock only; return stockitemid as articleid
     * 3: search in the current user's stock, in the stocks from the resources stored in "selectableResourceIdsAttr";
     */
    public Integer searchMode { get; set; }

    /**
     * If the debug mode is enabled some details are print out
     */
    public transient Boolean debugMode { get; set; }

    /**
     * SFDC ID of the resource to search for in case of
     * stock related searches.
     */
    public String resourceId { get; set; }

    /**
     * For SearchMode 3 the user can select the resource
     * selectableResourceIds -> the ids are seperated by ';'
     */
    public String usersResourceId { get; set; }
    public String selectableResourceIds { get; set; }


    public transient String message { get; private set; }

    /**
     * The actual SOQL query that is used for searching
     */
    public transient String query { get; private set; }

    /**
     * Clear results and search parameter
     */
    public PageReference clear()
    {
        debugMode = true;
        foundArticles = null;
        setController = null;
        articleQuery  = null;
        selectedBrand = '-1';
        lockTypes = null;

        return null;
    }

    /**
     * Initialize setController and return a list of records
     */
    public ApexPages.StandardSetController setController
    {
        
        get
        {
            debugMode = true;
 
            System.debug('#### setController(): articleQuery -> ' + articleQuery);
            System.debug('#### setController(): searchMode -> ' + searchMode);
            System.debug('#### setController(): selectedBrand -> ' + selectedBrand);
            System.debug('#### setController(): getShowBrandSelectList() -> ' + getShowBrandSelectList());

            if(setController == null)
            {
                List<String> conditions = new List<String>();

                String tablePrefix = '';

                List<String> artLockTypes = null;
                if (null != lockTypes)
                {
                    artLockTypes = lockTypes.split(',');
                }
                String artLockTypeList = '';
                if (null != artLockTypes)
                {
                    for (String locktype :artLockTypes)
                    {
                        if ('' != artLockTypeList)
                        {
                            artLockTypeList += ',';
                        }
                        artLockTypeList += '\''+ locktype + '\'';
                    }
                }
                
                // if the set is not created, but lock types are set in the component
                if ((null == articleLockTypes) && (null != lockTypes))
                {
                    // create a set and fill it with the descriptions of the lock types
                    articleLockTypes = new Set<String>();
                    for (String locktype :artLockTypes)
                    {
                        articleLockTypes.add(SCBase.getPicklistText(SCArticle__c.LockType__c, locktype));
                    }
                }

                // Stock search
                if ((searchMode != null) && ((searchMode == 1) || (searchMode == 2) || (searchMode == 3)))
                {

                    query = 'select Id, Qty__c, MinQty__c, MaxQty__c, ValuationType__c, ' +
                                'Article__r.Name, Article__r.EANCode__c, ' +
                                'Article__r.ArticleNameCalc__c,  ' +
                                'toLabel(Article__r.Class__c), ' +
                                'Article__r.ReplacedBy__c, Article__r.ReturnType__c, ' +
                                'Article__r.ReplacedBy__r.Name, ' +
                                'Article__r.ValuationType__c, ' +
                                'toLabel(Article__r.LockType__c), ' +
                                'toLabel(Article__r.Type__c), ' +
                                'stock__r.ValuationType__c ' +
                                'from SCStockItem__c ';
                    if (null != artLockTypes)
                    {
                        query += 'where Article__r.LockType__c in (' + artLockTypeList + ') and ';
                    }

                    tablePrefix = 'Article__r.';

                    // the stock of the current resource
                    
                    System.debug('#### my resourceId: ' + resourceId);
                    
                    
                    conditions.add('Stock__c in (select Stock__c ' +
                                    '    from SCResourceAssignment__c ' +
                                    '    where Resource__c = :resourceId ' +
                                    '      and ValidFrom__c <= :xToday ' +
                                    '      and ValidTo__c >= :xToday )');

                    if (selectedBrand <> null && selectedBrand.length() > 0)
                    {
                        if (null != artLockTypes)
                        {
                            conditions.add('Article__r.Id in (select Article__c from SCArticleDetail__c where'
                                           + ((getShowBrandSelectList() == true) ? (' Brand__r.Id = :selectedBrand and') : '')
                                           + ' Country__c = :userCountry' 
                                           + ' and LockType__c in (' + artLockTypeList + '))');
                        }
                        else
                        {
                            conditions.add('Article__r.Id in (select Article__c from SCArticleDetail__c where'
                                           + ((getShowBrandSelectList() == true) ? (' Brand__r.Id = :selectedBrand and') : '')
                                           + ' Country__c = :userCountry)');
                        }
                    }
                }
                else
                {
                    // we make no conditions for the lock type, this will be done when the result
                    // list is build up
                    query = 'select Id, Name, ArticleNameCalc__c, toLabel(Class__c), toLabel(Type__c), '
                            + ' ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c, '
                            + ' EANCode__c, ValuationType__c, '
                            + '(select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, '
                            + '        toLabel(LockType__c), PriceGroup__c '
                            + '   from Article_Details__r '
                            + '  where Country__c = \'' + userCountry + '\' ';
                    query += ') from SCArticle__c ';
                    
                    if (selectedBrand <> null && selectedBrand.length() > 0)
                    {
                        query = 'select Id, Name, ArticleNameCalc__c, toLabel(Class__c), toLabel(Type__c), '
                                + ' ReplacedBy__c, ReplacedBy__r.Name, toLabel(LockType__c), ReturnType__c, '
                                + ' EANCode__c, ValuationType__c, '
                                + '(select Id, Name, Brand__c, Brand__r.Name, DeliveringPlant__c, '
                                + '        toLabel(LockType__c), PriceGroup__c '
                                + '   from Article_Details__r ' 
                                + '  where Country__c = \'' + userCountry + '\') '
                                + ((getShowBrandSelectList() == true) ? ('and Brand__r.Id = \'' + selectedBrand + '\' ') : '')
                                + 'from SCArticle__c ';

                        conditions.add('Id in (select Article__c from SCArticleDetail__c where'
                                       + ((getShowBrandSelectList() == true) ? ('Brand__r.Id = :selectedBrand and') : '')
                                       + ' Country__c = :userCountry)');
                    }
                }
                // add the article number or the name or the ean code as search criteriy
                if (articleQuery <> null && articleQuery.length() > 0)
                {
                    // ensure that an '*' is replaced by a '%' (default sfdc )
                    articleQuery = articleQuery.replace('*', '%');

                    String condition = '(' + tablePrefix + 'Name like \''
                                + articleQuery + '%\' OR '
                                + tablePrefix + 'ArticleNameCalc__c like \''
                                + articleQuery + '%\' OR '
                                + tablePrefix + 'EANCode__c like \'' + articleQuery + '%\')';
                    conditions.add(condition);
                }

                if (searchClasses <> null && searchClasses.size() > 0)
                {
                    System.debug('##tk searchClasses -> ' + searchClasses.size());
                    conditions.add(tablePrefix + 'Class__c in :searchClasses');
                }

                if (searchTypes <> null && searchTypes.size() > 0)
                {
                    conditions.add(tablePrefix + 'Type__c in :searchTypes');
                }

                // if no condition is set we won't search at all
                // since this would give too much records anyway
                // except for stock search (mode = 1)
                if ((searchMode != 1) && (searchMode != 2) && (searchMode != 3) && (conditions.size() == 0))
                {
                    System.debug('ERROR: No search started since no conditions are set!');
                    return setController;
                }

                if ((null == artLockTypes) || (null == searchMode) || ((searchMode != 1) && (searchMode != 2) && (searchMode != 3)) )
                {
                    query += ' where ';
                }

                for (Integer i = 0; i < conditions.size(); i++)
                {
                    query += conditions.get(i);

                    if (i != conditions.size() - 1)
                    {
                        query += ' and ';
                    }
                }

                if (applicationSettings.DEFAULT_ARTICLESEARCH_RESULT_LIMT__c != null &&
                    applicationSettings.DEFAULT_ARTICLESEARCH_RESULT_LIMT__c > 0 &&
                    applicationSettings.DEFAULT_ARTICLESEARCH_RESULT_LIMT__c < 10000)
                {
                    query += ' LIMIT ';
                    try
                    {
                        query += applicationSettings.DEFAULT_ARTICLESEARCH_RESULT_LIMT__c.intValue();
                    }
                    catch (Exception e)
                    {
                        query += '10000';
                    }
                }
                else
                {
                    query += ' LIMIT 10000';
                }

                System.debug('#### query -> ' + query);
                try
                {
                    setController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
                    setController.setPageSize(10);
                    System.debug('#### try setController -> ' + setController);
                    System.debug('#### setController.getRecords().size: ' + setController.getRecords().size());
                }
                catch(Exception e)
                {
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'error:!' + e));
                    query += 'error: ' + e;
                }
            }
            return setController;
        }
        set;
    } // public ApexPages.StandardSetController setController

    /**
     * Getter for articles dependent on search mode.
     */
    public List<Article> getFoundArticles()
    {
        if (searchMode == 0)
        {
            assignArticleSearch();
        }
        else if ((searchMode == 1) || (searchMode == 2) || (searchMode == 3))
        {
            assignStockSearch();
        }
        
        return foundArticles;
    }

    /**
     * Get all Group brands
     *
     * @return list of all VG brands as select options
     */
    public List<SelectOption> getAllBrand()
    {
        List<SelectOption> options = new List<SelectOption>();

        // add a general option value to display if nothing is selected
        options.add(noneOption);

        for (Brand__c brand :
            [ select Name from Brand__c where Competitor__c = false
              order by Name] )
        {
            options.add(new SelectOption(brand.Id,brand.Name));
        }

        return options;
    }

    /**
     * 
     * 
     */
    public List<SelectOption> getAllResources()
    {
        String resString = selectableResourceIds != null ? selectableResourceIds : '';
        resString  += ';' + (usersResourceId != null ? usersResourceId : '');
        resString  += ';' + (resourceId != null ? resourceId : '');
        
        Set<Id> selRes = new Set<Id>();
        for (String resStr : resString.split(';',0))
        {
            try
            {
                Id curId = Id.valueOf(resStr);
                selRes.add(curId);
            }
            catch(Exception e) { }
        }
        
        
        List<SelectOption> options = new List<SelectOption>();
        
        SelectOption userOpt;
        
        //PMS 34692/Review: Review
        /*
        for (SCResource__c res : [SELECT Id, Alias_txt__c, Name
                                    FROM SCResource__c
                                   WHERE Id IN :selRes
                                ORDER BY Name])
        */
        List<SCResourceAssignment__c> resAssmnts = [
            SELECT 
                Employee__c, Plant__c, Resource__c, Stock__r.Name 
            FROM 
                SCResourceAssignment__c 
            WHERE 
                Resource__c IN :selRes
                AND
                ValidFrom__c <= :xToday 
                AND 
                ValidTo__c >= :xToday
            ORDER BY Resource__r.Name
        ];
        
               
        for (SCResourceAssignment__c res : resAssmnts)
        {
         /*CCEAG   if (res.Resource__c == usersResourceId)
            {
                userOpt = new SelectOption(res.Resource__c, System.Label.SC_flt_ResourceOfTheUser);
            }
            else
            */
            {
                options.add(new SelectOption(res.Resource__c, res.Employee__c + ' (' + res.Plant__c + '/' + res.Stock__r.Name + ')'));
            }
        }
        
        if (userOpt != null)
        {
            options.add(userOpt);
        }
        
        return options;
    }

    /********************************************************************
     *
     * Action methods
     *
     ********************************************************************/
     
    /**
     * Search article matching the given parameter set
     */
    public PageReference search() {
        
        debugMode = true;
        
        System.debug('### search() started');
        System.debug('### getParameters() -> ' + ApexPages.currentPage().getParameters());

        try
        {
            if (ApexPages.currentPage().getParameters().get('searchMode') != null)
            {
                searchMode = Integer.valueOf(ApexPages.currentPage().getParameters().get('searchMode'));
            }
        }
        catch (TypeException e)
        {
            searchMode = 0;
        }

        if (ApexPages.currentPage().getParameters().get('articleQuery') != null &&
            ApexPages.currentPage().getParameters().get('articleQuery').length() > 0)
        {
            articleQuery = ApexPages.currentPage().getParameters().get('articleQuery');
            System.debug('### search(): set articleQuery -> ' + articleQuery);
        }

        //ApexPages.currentPage().getParameters().get('articleClass').length() > 0
        if (ApexPages.currentPage().getParameters().get('articleClass') != null)
        {
            System.debug('### articleClass -> ' + articleClass);
            articleClass = ApexPages.currentPage().getParameters().get('articleClass');
        }

        System.debug('searchMode -> ' + searchMode);

        if (setController == null)
        {
            System.debug('### setController is NULL!');
            foundArticles = null;
            return null;
        }

        setController = null;
        foundArticles = null;

        return null;
    } // PageReference search()

    /********************************************************************
     *
     * Private methods
     *
     ********************************************************************/
     
    /**
     * Get records for a stock search and fill the list found article to show
     *
     */
    private void assignStockSearch()
    {
        if (setController == null)
        {
            return;
        }

        List<SCStockItem__c> stockItems = (List<SCStockItem__c>) setController.getRecords();

        if (stockItems <> null && stockItems.size() > 0)
        {
            foundArticles = new List<Article>();

            System.debug('stockItems -> ' + stockItems);

            for (SCStockItem__c item : stockItems)
            {
                Article art = new Article();
                art.articleId     = (searchMode == 3) ? item.Article__c : item.Id;
                art.articleNo     = item.Article__r.Name;
                art.description   = item.Article__r.ArticleNameCalc__c;
                art.articleType   = item.Article__r.Type__c;
                art.articleClass  = item.Article__r.Class__c;
                art.replacedBy    = item.Article__r.ReplacedBy__r.Name;
                art.returnType    = item.Article__r.ReturnType__c;
                art.eanCode       = item.Article__r.EANCode__c;
                art.lockType      = item.Article__r.LockType__c;
                art.qty           = item.Qty__c;
                art.minQty        = item.MinQty__c;
                art.maxQty        = item.MaxQty__c;
                art.article       = item.Article__r;
                art.valuationtype = item.ValuationType__c;
                art.stockItem     = item.id;
                
                foundArticles.add(art);
            }

            message = null;

        }
        else
        {
            // nothing found
            foundArticles = null;
            message = System.Label.SC_msg_NoProductFound;
        }
    }

    /**
     * Get records for a non-stock search and fill the list of records to show
     */
    private void assignArticleSearch()
    {
        if (setController == null)
        {
            return;
        }
        List<SCArticle__c> articles = (List<SCArticle__c>) setController.getRecords();
        List<SCArticle__c> displayedArticles = new List<SCArticle__c>();

        if (articles <> null && articles.size() > 0)
        {
            foundArticles = new List<Article>();

            System.debug('articles -> ' + articles);

            for (SCArticle__c article : articles)
            {
                Article art = new Article();
                art.articleId     = article.Id;
                art.articleNo     = article.Name;
                art.description   = article.ArticleNameCalc__c;
                art.articleType   = article.Type__c;
                art.articleClass  = article.Class__c;
                art.replacedBy    = article.ReplacedBy__r.Name;
                art.returnType    = article.ReturnType__c;
                art.eanCode       = article.EANCode__c;
                art.lockType      = article.LockType__c;
                art.article       = article;
                art.stockItem     = 'none';

                // FIXME lockType handling has to be rewritten taking the
                // domain into account.
                // The more serious lock type will be set to the actual
                // article record!!

                // Article Detail records found
                if (article.Article_Details__r.size() > 0)
                {
                    for (SCArticleDetail__c detail : article.Article_Details__r)
                    {
                        // if no set is created or the set contains the article detail lock type
                        // add it to the result list
                        if ((null == articleLockTypes) || articleLockTypes.contains(detail.LockType__c))
                        {
                            Article artDetail = art.clone();
                            artDetail.brand = detail.Brand__r.Name;
    
                            artDetail.lockType = detail.LockType__c;
    
                            foundArticles.add(artDetail);
                            displayedArticles.add(article);
                        }
                    }
                }
                else
                {
                    // if no set is created or the set contains the article lock type
                    // add it to the result list
                    if ((null == articleLockTypes) || articleLockTypes.contains(article.LockType__c))
                    {
                        foundArticles.add(art);
                        displayedArticles.add(article);
                    }
                }
            }

            message = null;
            setController = new ApexPages.StandardSetController(displayedArticles);
            System.debug('#### set new controller: ' + displayedArticles.size());
        }
        else
        {
            // nothing found
            foundArticles = null;
            message = System.Label.SC_msg_NoProductFound;
        }
    }
    
    /*
     *
     *
     */
    public Boolean getDisplayValTypeColumn()
    {
        if (((searchMode == 1) || (searchMode == 2) || (searchMode == 3)) && (setController != null))
        {
            try
            {
                List<SCStockItem__c> stockItems = (List<SCStockItem__c>) setController.getRecords();
                
                if (stockItems <> null && stockItems.size() > 0)
                {
                    for (SCStockItem__c item : stockItems)
                    {
                        if (item.stock__r.ValuationType__c == true)
                        {
                            return true;
                        }
                    }
                }
            }
            catch(Exception e) { }
        }
        
        return false;
    }
    


    /**
     * Internal wrapper class to show the merged contents of
     * Article and Article Details
     */
    public class Article {
        public String articleId { get; set; }
        public String articleNo { get; set; }
        public String description { get; set; }
        public String lockType { get; set; }
        public String articleType { get; set; }
        public String articleClass { get; set; }
        public String replacedBy { get; set; }
        public String returnType { get; set; }
        public String brand { get; set; }
        public String eanCode { get; set; }
        public Decimal qty { get; set; }
        public Decimal minQty { get; set; }
        public Decimal maxQty { get; set; }
        public SCArticle__c article { get; set; }
        public String valuationType { get; set; }
        public String stockItem { get; set; }
        
        public String getValuationTypeLabel()
        {
            if ((mapValuationTypeTranslation == null) || (mapValuationTypeTranslation.size() == 0))
            {
                for (Schema.PicklistEntry schemaPickEntry : SCOrderLine__c.ValuationType__c.getDescribe().getPicklistValues())
                {
                    mapValuationTypeTranslation.put(schemaPickEntry.getValue().toLowerCase(), schemaPickEntry.getLabel());
                }
            }
            
            if ((valuationType != null) && (mapValuationTypeTranslation.containsKey(valuationType.toLowerCase())))
            {
                return mapValuationTypeTranslation.get(valuationType.toLowerCase());
            }
            return valuationType;
        }
    }
    
    private static Map<String,String> mapValuationTypeTranslation = new Map<String,String>();
}