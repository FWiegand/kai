/*
 * @(#)SCTranslation.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCfwTranslation
{
    /**
     * Handling of translations, i.e. inserting, updating and fetching
     * will be done here.
     */

    /**
     * Domain that contains all language specific settings
     */
    private static SCfwDomain domain = new SCfwDomain('DOM_LANGUAGE');

    /**
     * Global construction w/o any further processing
     */
    public SCfwTranslation()
    {
        
    }
  
    /**
     * Set translation of a domain value for the locale
     *
     * @param reference id of the domain value
     * @param locale locale to use
     * @param textShort short text version
     * @param textLong long text version
     */

    public void setTranslation(String domainValue, String locale, 
                               String id2, String textShort,
                               String textLong)                                   
    {
        try 
        {

            SCDomainDescription__c descr = new SCDomainDescription__c(ID2__c = id2,
                                                         LocaleSidKey__c = locale,
                                                         //Reference__c = reference,
                                                         DomainValue__c = domainValue,
                                                         Text__c = textShort,
                                                         TextLong__c = textLong
                                                        );
                                                                                                           
            upsert descr;
        }
        catch (DmlException e)
        {
            throw new SCfwTranslationException(true, 'Unable to insert translation for ' + id2 );
        }
    }
    
    /**
     * Get translation for the given reference ID and locale
     *
     * @param reference Internal SFDC ID
     * @param locale to use
     * @return description object
     */
    public SCDomainDescription__c getTranslation(String domainValue, String locale)
    {
        SCDomainDescription__c descr = null;
        String validLocale = getValidLocale(locale);
        
        try
        {
            descr = [select Text__c, TextLong__c, DomainValue__c, LocaleSidKey__c, ID2__c
                       from SCDomainDescription__c 
                      where DomainValue__c = :domainValue 
                        and LocaleSidKey__c = :validLocale];                                    

        }
        catch (QueryException e)
        {
            throw new SCfwTranslationException(true, 'Unable to get translation for ' + domainValue +
                                        '(' + validLocale + ')' );
        }
        
        return descr;
        
    }
    
    /**
     * Get the translations for a list of references, since this will improve the performance
     *
     * @param reference array of reference IDs to fetch
     * @param locale locale to use
     * @return list of description objects
     */
    public List<SCDomainDescription__c> getTranslation(Set<String> domainValues, String locale)
    {
        List<SCDomainDescription__c> descr = null;
        String validLocale = getValidLocale(locale);

        descr = [select Text__c, TextLong__c, DomainValue__c, LocaleSidKey__c, ID2__c
                   from SCDomainDescription__c 
                  where DomainValue__c = :domainValues
                    and LocaleSidKey__c = :validLocale];                    

        if (descr.size() == 0)
        {                                         
            throw new SCfwTranslationException(true, 'No translation found for ' + domainValues +
                                        '(' + validLocale + ')' );                                        
        }
        
        return descr;
    }
    
    /**
     * Get the translations for a list of references, 
     * since this will improve the performance
     *
     * @param reference array of reference IDs to fetch
     * @param locale locale to use
     * @return map of description reference ID and description objects
     */
    public Map<String, SCDomainDescription__c> getTranslationMap(Set<String> domainValues, String locale)
    {
        Map<String, SCDomainDescription__c> descr = new Map<String, SCDomainDescription__c>();
        String validLocale = getValidLocale(locale);
   
        List<SCDomainDescription__c> found = [select Text__c, TextLong__c, DomainValue__c, 
                                              LocaleSidKey__c, ID2__c
                                       from SCDomainDescription__c 
                                      where DomainValue__c in :domainValues
                                        and LocaleSidKey__c = :validLocale];
        for (SCDomainDescription__c d : found)
        {
            descr.put(d.DomainValue__c, d);
        }            
        /* 
        catch (QueryException e)
        {   
            throw new SCfwTranslationException(true, 'Unable to get translation for ' + 
                                        domainValues +
                                        '(' + validLocale + ')' );                                        
        }
        */
        if (descr.size() == 0)
        {
            throw new SCfwTranslationException(true, 'No translation found for ' + 
                                        domainValues +
                                        '(' + validLocale + ')' );
        }    
        return descr; 
    }
        
    
    /**
     * Get all available translations for the given reference ID
     *
     * @param reference Internal SFDC ID
     * @return description object
     */
    public List<SCDomainDescription__c> getAllTranslation(String domainValue)
    {
        List<SCDomainDescription__c> descr = new List<SCDomainDescription__c>();
               
        descr = [select DomainValue__c, Text__c, TextLong__c, 
                        LocaleSidKey__c, ID2__c
                   from SCDomainDescription__c 
                  where DomainValue__c = :domainValue];

        if (descr.size() == 0)
        {               
            throw new SCfwTranslationException(true, 
               'No translation found for ' + domainValue);               
        }
        
        return descr;     
    }
   
    /**
     * Get all translations for a list of references
     *
     * @param reference array of reference IDs to fetch
     * @return list of description objects
     */
    public static List<SCDomainDescription__c> getAllTranslation(Set<String> domainValues)
    {
        List<SCDomainDescription__c> descr = null;

        descr = [select DomainValue__c, Text__c, TextLong__c,  
                        LocaleSidKey__c
                   from SCDomainDescription__c 
                  where DomainValue__c in :domainValues];          

        if (descr.size() == 0)
        {
            throw new SCfwTranslationException(true, 
                'Unable to get translation for ' + domainValues);
        }
        
        return descr;
    }
   
    /**
     * Find the description matching the text and the external ID
     *
     * @param text Text to search for
     * @param domain domain name
     * @param locale locale to search for
     * @return description record 
     */
     
    public static List<SCDomainDescription__c> findDescriptionByDom(String text,
                                                              String domainName,
                                                              String locale)
    {
        List<SCDomainDescription__c> descr = new List<SCDomainDescription__c>();
        
        if (text == null || text.length() == 0)
        {
            return descr;
        }
        
        text = text + '%';
        
        descr = [select DomainValue__r.DomainRef__r.Name, 
                        DomainValue__c, Id2__c, Text__c, TextLong__c, 
                        LocaleSidKey__c 
                   from SCDomainDescription__c
                  where Text__c like :text
                    and LocaleSidKey__c = :locale
                    and DomainValue__r.DomainRef__r.Name = :domainName];



        if (descr.size() == 0)
        {
            throw new SCfwTranslationException(true,
                'Unable to fetch translation for ' + text +
                ' domain ' + domainName +
                ' and locale ' + locale);
        }
        
        return descr;
    }
    
    /**
     * Get a valid locale from a given locale.
     *
     * Usually the locale consists of language and country (e.g. en_GB), 
     * but sometimes we won't be that specific and use the country
     * only for the locale.<p>
     * This function should then check DOM_LANGUAGE if the locale is valid 
     * in general or return the shortend version otherwise.
     *
     * @param locale The locale to check
     * @return valid locale from DOM_LANGUAGE
     */
    private String getValidLocale(String locale)
    {
        String validLocale = locale;
        
        if (isValidLocale(locale))
        {
            return validLocale;
        }
        else
        {
            if(locale.length() > 2)
            {
                validLocale = locale.substring(0,2);
            }
            return validLocale;
        }
    }
    
    /**
     * Check if the given locale is covered by the corresponding domain
     *
     * @param locale locale to check
     * @retrun true is domain is valid, false otherwise
     */
    private Boolean isValidLocale (String locale)
    {
        return domain.valueExistsById2(locale);     
        
    }
}