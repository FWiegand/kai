@isTest
private class SCInstalledBaseMethodsTest {

	static testMethod void testBeforeInsertUpdate() {
		
		SCProductModel__c objProductModel;
		List<SCProductModel__c> listProductModel = new List<SCProductModel__c>();
		for(Integer i = 0; i < 10; i++) {
			objProductModel = new SCProductModel__c();
			objProductModel.Name = 'Product ' + i;
			listProductModel.add(objProductModel);
		}
		insert listProductModel;
		
		SCInstalledBase__c objInstalledBase;
		List<SCInstalledBase__c> listInstalledBase = new List<SCInstalledBase__c>();
		for(Integer i = 0; i < 10; i++) {
			objInstalledBase = new SCInstalledBase__c();
			objInstalledBase.ProductModel__c = listProductModel.get(i).Id;
			objInstalledBase.SerialNo__c = '0000' + String.valueOf(i);
			listInstalledBase.add(objInstalledBase);
		}
		insert listInstalledBase;
		
		List<SCInstalledBase__c> listQueriedInstalledBase = [SELECT Id, Name FROM SCInstalledBase__c];
		System.assertEquals(listQueriedInstalledBase.size(), 10);
		
		List<SCInstalledBase__c> listSCInstallesBase = new List<SCInstalledBase__c>();
	}
	
	static testMethod void testAfterUpdate() {
		
	}
}