/*
 * @(#)SCfwWizard.cls
 *  
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Helper class to implement a wizard with [Previous] [Next] [Finish] buttons.
 
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCfwWizard
{
    // the current dialog step - starting with 1
    public Integer step {get; set;}
    // total number of steps - at least 1
    public Integer steps {get; set;}
    // enable a summary page after [Finish]
    public Boolean enableSummary {get; set;}

   /*
    * Constructor - initializes the wizard 
    * @param stepsTotal the total number of steps required 
    * @param enableConfirmationPage set this value to true to enable a summary 
    *        page after the user has pressed [Finish] 
    */            
    public SCfwWizard(Integer stepsTotal, Boolean enableSummaryPage)
    {
        step = 1;
        steps = stepsTotal;
        enableSummary = enableSummaryPage;
    }
    
   /*
    * Sets the wizart to the first step
    */            
    public void reset()
    {
        step = 1;
    }

   /*
    * Goes to the next page (increments the page counter)
    */            
    public Integer next()
    {
        if(step < steps)
        {
            step++;
        }
        return step;
    }

   /*
    * Goes to the previous page (decrements the page counter)
    */            
    public Integer prev()
    {
        if(step > 1)
        {
            step--;
        }
        return step;
    }

   /*
    * The user pressed the finish button. If a summary page is 
    * to be displayed the status is set now.
    */            
    public Integer finish()
    {
        if(enableSummary)
        {
            // the summary page is the last page + 1        
            step = steps + 1;
        }
        else
        {
            step = steps;
        }
        return step;
    }


   /*
    * Goes to the specified page - if possible
    * @param nextStep the page to select
    */            
    public Integer goto(Integer nextStep)
    {
        if(nextStep >= 1 && nextStep <= steps)
        {
            step = nextStep;
        }
        
        return step;
    }

   /*
    * Goes to the fihish page
    */            
    public Integer gotoFinish()
    {
        step = steps;
        return step;
    }


   /*
    * Can be used to activate the next button.
    * @return true if the last page is not yet reached
    */            
    public Boolean getEnableNext()
    {
        if(enableSummary && step == (steps + 1))
        {
            // the final summary page is already reached - no next available
            return false;
        }
        return (step < steps);
    }       

   /*
    * Can be used to activate the previous button.
    * @return true if the first page is not yet reached
    */            
    public Boolean getEnablePrev()
    {
        if(enableSummary && step == (steps + 1))
        {
            // the final summary page is already reached - no prev available
            return false;
        }
        return (step > 1);
    }       

   /*
    * Can be used to activate a finish button.
    * @return true if the last page is reached
    */            
    public Boolean getEnableFinish()
    {
        if(enableSummary && step == (steps + 1))
        {
            // the final summary page is already reached - no finish available
            return false;
        }
        return (step == steps);
    }       

   /*
    * Can be used to display a summary page after the user 
    * has pressed [finish]. Please note that the prev and 
    * next and finish buttons are disabled if the summary 
    * page is displayed.
    * @return true if the last page is reached
    */            
    public Boolean getShowSummary()
    {
        return (step == steps + 1);
    }       

    
   /*
    * Prepares a string that represents the current step
    * @return Returns a string in the form "1/4"
    */            
    public String getStatus()
    {
        return '' + step + '/' + steps;
    }
   
}