/*
 * @(#)SCTestNewOnePageOrderRedir.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for the new one page order redir extension.
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCNewOnePageOrderRedirTest
{
    /**
     * testStockProfileExtension
     * =========================
     *
     * Test the extension with the redirection.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    static testMethod void testNewOnePageOrderRedir()
    {
        System.debug('########################################################');
        System.debug('#### Starting Test:  testNewOnePageOrderRedir()');
        System.debug('########################################################');

        // create all neccessary datas
        SCHelperTestClass.createAccountObject('Customer', true);

        Test.startTest();
        
        SCNewOnePageOrderRedir newOnePageOrder = 
                     new SCNewOnePageOrderRedir(new ApexPages.StandardController(SCHelperTestClass.account));
        
        PageReference page = newOnePageOrder.openOnePageOrder();
        System.assert(null != page);

        Test.stopTest();
    } // testNewOnePageOrderRedir
} // SCTestNewOnePageOrderRedir