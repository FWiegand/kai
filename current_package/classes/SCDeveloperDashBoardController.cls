/*
 * @(#)SCDeveloperDashBoardController.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @author others
 * @version $Revision$, $Date$
 */
public with sharing class SCDeveloperDashBoardController
{
    private Id userId;
    
    public String search {set;get;}
    public SCResource__c resource {set;get;}
    public Integer days {set;get;}
    public String radioSel {set;get;}
    public List<ApexClass> results {set;get;}
    public List<ApexTrigger> resultsT {set;get;}
    public List<ApexPage> resultsP {set;get;}
    public List<ApexComponent> resultsComp {set;get;}
    public List<User> resultsU {set;get;}
    public List<ObjectInfo> resultsObj {set;get;}

    public Boolean editFavorites {set;get;}
    public Boolean hasFavorites {set;get;}
    public Boolean hasClasses {set;get;}
    public Boolean hasPages {set;get;}
    public Boolean hasComponents {set;get;}
    public Boolean hasTriggers {set;get;}
    public Boolean hasObjects {set;get;}
    public Boolean hasOthers {set;get;}
    public List<SCDeveloperFavorite__c> favorites {set;get;}
    public List<ClassObj> curClassObjs {set;get;}
    public List<SelectOption> curClasses {set;get;}
    public List<SelectOption> curPages {set;get;}
    public List<SelectOption> curTriggers {set;get;}
    public List<SelectOption> curComponents {set;get;}
    public List<SelectOption> curObjects {set;get;}
    public List<SelectOption> curOthers {set;get;}
    public List<SelectOption> allClasses {set;get;}
    public List<SelectOption> allPages {set;get;}
    public List<SelectOption> allTriggers {set;get;}
    public List<SelectOption> allComponents {set;get;}
    public List<SelectOption> allObjects {set;get;}
    public List<SelectOption> allOthers {set;get;}
    public String curClassIds {set;get;}
    public String curPageIds {set;get;}
    public String curTriggerIds {set;get;}
    public String curComponentIds {set;get;}
    public String curObjectIds {set;get;}
    public String curOtherIds {set;get;}
    
    
    /**
     * Constructor
     */
    public SCDeveloperDashBoardController()
    {
        system.debug('#### SCDeveloperDashBoardController(): search -> ' + search);
        system.debug('#### SCDeveloperDashBoardController(): param  -> ' + System.currentPagereference().getParameters().get('search'));
        radioSel = '0';
        days = 1;
        editFavorites = false;
        hasFavorites = false;
        
        userId = UserInfo.getUserId();
        resource = new SCResource__c(Employee__c = userId);
        
        initFavorites(userId, false);
                      
        if (null == search)
        {
            search = System.currentPagereference().getParameters().get('search');
        }
        if (null != search)
        {
            onSearch();
        }

        /*
        // getting all fields of an object (only for test, do not remove)
        Schema.SObjectType t = Schema.getGlobalDescribe().get('ApexTestResult');
        Schema.DescribeSObjectResult r = t.getDescribe();
        System.debug('#### SCDeveloperDashBoardController(): map -> ' + r.fields.getMap());
        for (String key :r.fields.getMap().keySet())
        {
            System.debug('#### SCDeveloperDashBoardController(): column -> ' + r.fields.getMap().get(key));
        }
        */
    } // SCDeveloperDashBoardController
    
    /**
     * Initialize all necessary things for displaying and editing favorites.
     */
    private void initFavorites(Id userId, Boolean all)
    {
        Map<String, ApexClass> mapName2Class = new Map<String, ApexClass>();
        allClasses = new List<SelectOption>();
        for (ApexClass apClass :[Select Id, Name from ApexClass order by Name])
        {
            allClasses.add(new SelectOption(apClass.Id, apClass.Name));
            mapName2Class.put(apClass.Name.toUpperCase(), apClass);
        }
        
        if (all)
        {
            allPages = new List<SelectOption>();
            allTriggers = new List<SelectOption>();
            allComponents = new List<SelectOption>();
            allObjects = new List<SelectOption>();
            allOthers = new List<SelectOption>();
    
            for (ApexPage apPage :[Select Id, Name from ApexPage order by Name])
            {
                allPages.add(new SelectOption(apPage.Id, apPage.Name));
            }
            for (ApexTrigger apTrigger :[Select Id, Name from ApexTrigger order by Name])
            {
                allTriggers.add(new SelectOption(apTrigger.Id, apTrigger.Name));
            }
            for (ApexComponent apComp :[Select Id, Name from ApexComponent order by Name])
            {
                allComponents.add(new SelectOption(apComp.Id, apComp.Name));
            }
            List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
            for (Schema.SObjectType f :gd)
            {
                // describe the objects
                Schema.DescribeSObjectResult r = f.getDescribe();
                String objName = r.getName();
                if (!objName.endswith('Tag') && !objName.endswith('History') && !objName.endswith('Share'))
                {
                    allObjects.add(new SelectOption(r.isCustom() ? r.getKeyPrefix() + '/o' : 'ui/setup/Setup?setupid=' + objName, objName));
                } // if (!objName.endswith('Tag') && !objName.endswith('History'))
            } // for (Schema.SObjectType f :gd)
            
            allOthers.add(new SelectOption('101', 'Custom Labels'));
            allOthers.add(new SelectOption('setup/ui/listCustomSettings.apexp', 'Custom Settings'));
            allOthers.add(new SelectOption('p/setup/custent/CustomObjectsPage', 'Objects'));
            allOthers.add(new SelectOption('i18n/TranslationWorkbench.apexp', 'Translate'));
            allOthers.add(new SelectOption('ui/setup/apex/ApexTestQueuePage', 'Apex Test Execution'));
            allOthers.add(new SelectOption('setup/ui/listApexTraces.apexp', 'Debug Logs'));
            allOthers.add(new SelectOption('005', 'Users'));
            allOthers.add(new SelectOption('00e', 'Profiles'));
            allOthers.add(new SelectOption('02u', 'Apps'));
            allOthers.add(new SelectOption('setup/ui/customtabs.jsp', 'Tabs'));
            allOthers.add(new SelectOption('apexpages/setup/listStaticResource.apexp', 'Static Resources'));
        } // if (all)
        else
        {
            List<ApexClass> curClassList = new List<ApexClass>();
            curClasses = new List<SelectOption>();
            curPages = new List<SelectOption>();
            curTriggers = new List<SelectOption>();
            curComponents = new List<SelectOption>();
            curObjects = new List<SelectOption>();
            curOthers = new List<SelectOption>();
    
            curClassIds = '';
            curPageIds = '';
            curTriggerIds = '';
            curComponentIds = '';
            curObjectIds = '';
            curOtherIds = '';

            favorites = [Select Id, Name, Type__c, Key__c, Name__c 
                           from SCDeveloperFavorite__c
                          where User__c = :userId 
                          order by Name__c];
            hasFavorites = favorites.size() > 0;
            
            for (SCDeveloperFavorite__c fav :favorites)
            {
                if ('Class' == fav.Type__c)
                {
                    curClasses.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curClassList.add(mapName2Class.get(fav.Name__c.toUpperCase()));
                    curClassIds += fav.Key__c + ',';
                }
                else if ('Page' == fav.Type__c)
                {
                    curPages.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curPageIds += fav.Key__c + ',';
                }
                else if ('Trigger' == fav.Type__c)
                {
                    curTriggers.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curTriggerIds += fav.Key__c + ',';
                }
                else if ('Component' == fav.Type__c)
                {
                    curComponents.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curComponentIds += fav.Key__c + ',';
                }
                else if ('Object' == fav.Type__c)
                {
                    curObjects.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curObjectIds += fav.Key__c + ',';
                }
                else if ('Other' == fav.Type__c)
                {
                    curOthers.add(new SelectOption(fav.Key__c, fav.Name__c));
                    curOtherIds += fav.Key__c + ',';
                }
            } // for (SCDeveloperFavorite__c fav :favorites)

            hasClasses = curClasses.size() > 0;
            hasPages = curPages.size() > 0;
            hasComponents = curComponents.size() > 0;
            hasTriggers = curTriggers.size() > 0;
            hasObjects = curObjects.size() > 0;
            hasOthers = curOthers.size() > 0;

            curClassObjs = new List<ClassObj>();
            ClassObj clObj;
            String className1;
            String className2;
            ApexClass apexClass;
            for (ApexClass apClass :curClassList)
            {
                clObj = new ClassObj();
                clObj.apexClass = apClass;

                if (!apClass.Name.toUpperCase().Contains('TEST'))
                {
                    className1 = apClass.Name + 'Test';
                    className2 = apClass.Name + '_Test';
                    apexClass = mapName2Class.get(className1.toUpperCase());
                    if (null != apexClass)
                    {
                        clObj.apexTestClass = apexClass;
                    }
                    else
                    {
                        apexClass = mapName2Class.get(className2.toUpperCase());
                        if (null != apexClass)
                        {
                            clObj.apexTestClass = apexClass;
                        }
                    }
                } // if (!apClass.Name.toUpperCase().Contains('TEST'))
                else
                {
                    clObj.apexTestClass = null;
                }
                
                curClassObjs.add(clObj);
            } // for (ApexClass apClass :curClassList)
        } // else [if (all)]
    } // initFavorites
    
    /**
     * Does the search
     */
    public PageReference onSearch()
    {
        if (radioSel.equals('0'))
        {
            if ((null == search) ||  ('' == search))
            {
                results = null;
                resultsT = null;
            }
            else
            {
                search = search.trim();
                string rsearch = '%' + search + '%';
                results = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                             from ApexClass 
                            where Name like :rsearch 
                            order by Name limit 20];
                resultsT = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                              from ApexTrigger 
                             where Name like :rsearch 
                             order by Name limit 20];
                resultsP = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                              from ApexPage 
                             where Name like :rsearch 
                             order by Name limit 20];
                resultsComp = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                                 from ApexComponent 
                                where Name like :rsearch 
                                order by Name limit 20];
                resultsU = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate, Profile.Name, UserRole.Name 
                              from User 
                             where Name like :rsearch 
                             order by Name limit 20];
                resultsObj = selectObject(search);
            }
        } // if (radioSel.equals('0'))
        else
        {
            if (null == resource.Employee__c)
            {
                resource.Employee__c = UserInfo.getUserId();
            }
            if (null == days)
            {
                days = 1;
            }

            Date toDate = Date.today().addDays(1);
            Date fromDate = Date.today().addDays(-(days-1));
            
            results = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                         from ApexClass 
                        where LastModifiedBy.Id = :resource.Employee__c 
                          and LastModifiedDate <= :toDate 
                          and LastModifiedDate >= :fromDate 
                        order by Name 
                        limit 20];
            resultsT = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                          from ApexTrigger 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         order by Name 
                         limit 20];
            resultsP = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate
                          from ApexPage 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         order by Name 
                         limit 20];
            resultsComp = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate 
                             from ApexComponent 
                            where LastModifiedBy.Id = :resource.Employee__c 
                              and LastModifiedDate <= :toDate 
                              and LastModifiedDate >= :fromDate 
                            order by Name 
                            limit 20];
            resultsU = [Select Id, Name, LastModifiedBy.Name, LastModifiedDate, Profile.Name, UserRole.Name 
                          from User 
                         where LastModifiedBy.Id = :resource.Employee__c 
                           and LastModifiedDate <= :toDate 
                           and LastModifiedDate >= :fromDate 
                         order by Name 
                         limit 20];
            resultsObj = new List<ObjectInfo>(); // GMSAW: ??? selectObject(search);
        } // else [if (radioSel.equals('0'))]
        return null;
    } // onSearch
    
    /**
     * Shows the part of the page for editing the favorites.
     * The part for search will be hidden.
     */
    public PageReference onEditFavorites()
    {
        editFavorites = true;
        initFavorites(userId, true);
        
        return null;
    } // onEditFavorites
    
    /**
     * Saves the favorites and hides the part of the page for editing the favorites.
     * Shows the search part.
     */
    public PageReference onSaveFavorites()
    {
        editFavorites = false;

        delete favorites;
        favorites.clear();
        
        System.debug('#### onSaveFavorites(): curClassIds -> ' + curClassIds);
        for (String id :curClassIds.split(','))
        {
            for (SelectOption option :allClasses)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Class', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        for (String id :curPageIds.split(','))
        {
            for (SelectOption option :allPages)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Page', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        for (String id :curTriggerIds.split(','))
        {
            for (SelectOption option :allTriggers)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Trigger', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        for (String id :curComponentIds.split(','))
        {
            for (SelectOption option :allComponents)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Component', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        for (String id :curObjectIds.split(','))
        {
            for (SelectOption option :allObjects)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Object', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        for (String id :curOtherIds.split(','))
        {
            for (SelectOption option :allOthers)
            {
                if (option.getValue() == id)
                {
                    favorites.add(new SCDeveloperFavorite__c(User__c = userId, Type__c = 'Other', 
                                                             Key__c = option.getValue(), Name__c = option.getLabel()));
                    break;
                }
            }
        }
        insert favorites;

        initFavorites(userId, false);
        
        return null;
    } // onSaveFavorites
    
    /**
     * Cancels the edition of the favorites.
     * Hides the part of the page for editing the favorites.
     * Shows the search part.
     */
    public PageReference onCancel()
    {
        editFavorites = false;
        // initFavorites(userId, false);
        return null;
    } // onCancel

    /**
     * Retrieves the id of the organization.
     */
    public string getOrgId()
    {
        return UserInfo.getOrganizationId();
    } // getOrgId
 
    /**
     * Gets a list objects matching the search conditions.
     */
    public List<ObjectInfo> selectObject(String name)
    {
        List<ObjectInfo> res = new List<ObjectInfo>(); 
        if (null != name)
        {
            // loop through the custom objects
            List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
            for (Schema.SObjectType f :gd)
            {
                // describe the objects
                Schema.DescribeSObjectResult r = f.getDescribe();
                String objName = r.getName();
                // search for the name (exclude tags and history objects)
                if (objName.toUpperCase().contains(name.toUpperCase()) &&
                    !objName.endswith('Tag') && 
                    !objName.endswith('History') && 
                    !objName.endswith('Share'))
                {
                    ObjectInfo o = new ObjectInfo();
                    o.name = objName; // + '##' + r.getKeyPrefix();
                    if (r.isCustom())
                    {
                        String prefix = r.getKeyPrefix();
                        // opens the tab with the object content
                        o.url = '' + prefix + '/o';        
                        
                        //TODO: determine the "01I20000000Ap5O" id to jump directly to the custom object definition                        
                        //https://cs4.salesforce.com/01I20000000Ap5O?setupid=CustomObjects
                    }
                    else
                    {    
                        // simple access to the object
                        o.url = 'ui/setup/Setup?setupid=' + o.name;        
                    }

                    res.add(o);
                } // if (objName.toUpperCase().contains(name.toUpperCase()) &&...
            } // for (Schema.SObjectType f :gd)
        } // if (null != name)   
        return res;
    } // selectObject
    
    /**
     * Small wrapper class, that contains data for an object.
     */
    public class ObjectInfo
    {
        public String name {get; set;}
        public String url {get; set;}
    } // ObjectInfo
    
    /**
     * Small wrapper class, that a ApexClass and their TestClass
     */
    public class ClassObj
    {
        public ApexClass apexClass {get; set;}
        public ApexClass apexTestClass {get; set;}
    } // ClassObj
} // class SCDeveloperDashBoardController