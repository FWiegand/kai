/*
 * @(#)SCPriceCalculationTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 * <p>
 * This class contains unit tests for validating the behaviour of pricing for
 * service items (SCOrderLine).
 * <p>
 * A complete testing environment is created first. Remember that all data
 * created in a Unit test is not commited to the database!!
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */

@isTest
private class SCPriceCalculationTest
{

    private static SCboOrder boOrderTest;
    
    private static SCOrder__c order;
    private static List<SCOrderRole__c> orderRole = new List<SCOrderRole__c>();
    private static List<SCOrderItem__c> orderItem = new List<SCOrderItem__c>();
    private static List<SCOrderLine__c> orderLine = new List<SCOrderLine__c>();

    private static SCPriceList__c pricelistPL;
    private static SCPriceList__c pricelistPG;

       
    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - no discount
     * - tax rate is 7% (taxable)
     */
    static testMethod void pricingInvoiceAll1()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        // dummy (for coverage)
        SCPriceCalculation pricing = new SCPriceCalculation();

        // now instanitiate the test
        pricing = new SCPriceCalculation(order);
               
        System.assertNotEquals(pricing, null);
        System.assertNotEquals(null, pricing.getPriceList(), 'Price list is null');
        
        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            ol = pricing.calculate(ol);
            
            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }


    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - no discount
     * - tax rate is 7% (taxable)
     * - empty pricing date
     */
    static testMethod void pricingInvoiceAll3()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);

        System.assertNotEquals(pricing, null);


        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            // Set pricing date to null and it's set in the pricing component.
            
            ol.PricingDate__c = null;
            
            ol = pricing.calculate(ol);
            
            System.assertEquals(System.today(), ol.PricingDate__c);
            
            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }




    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - no discount
     * - tax rate is 7% (taxable)
     * - manual list price
     */
    static testMethod void pricingInvoiceAll4()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal listPrice = 1122.33;
        
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);

        System.assertNotEquals(pricing, null);


        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            ol.ManualListPrice__c = true;
            ol.ListPrice__c = listPrice;
            
            ol = pricing.calculate(ol);
                    
            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(listPrice, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(listPrice, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * listPrice), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }

    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - negative position price
     * - tax rate is 7% (taxable)
     */
    static testMethod void pricingInvoiceDiscount1()
    {
        setup();
        
        Decimal price = 34.897;
        
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);

        System.assertNotEquals(pricing, null);


        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            // Check the resetting part of calculateDiscount()
            ol.PositionPrice__c = -5;
            
            ol = pricing.calculate(ol);

            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }


    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - absolute discount of 10 EUR
     * - tax rate is 7% (taxable)
     */
    static testMethod void pricingInvoiceDiscount2()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal discount = 10.0;
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);

        System.assertNotEquals(pricing, null);


        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            ol.Discount__c = discount;
            
            ol = pricing.calculate(ol);

            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals((ol.PositionPrice__c - discount), ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }
    
    
    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - relative discount of 10%
     * - tax rate is 7% (taxable)
     */
    static testMethod void pricingInvoiceDiscount3()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal discount = 10.0;
        Decimal qty = 1;
        

        
        createOrderLinePL(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        

        
        Test.startTest();
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);

        System.assertNotEquals(pricing, null);


        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            ol.RelativeDiscount__c = discount;
            
            ol = pricing.calculate(ol);

            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals((ol.PositionPrice__c - (ol.PositionPrice__c * discount / 100)), 
                                    ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }
    
    
    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - additional conditions set (V1=K0A0,V2=A3)
     * - list price is determince from price group
     * - no discount
     * - tax rate is 7% (taxable)
     */
     
/*     
    static testMethod void pricingInvoiceAll2()
    {
        setup();
        
        Decimal price = 98.765;
        Decimal specialPrice = 52.23;
        Decimal qty = 1;
        
        SCfwDomain domCondType = new SCfwDomain('DOM_CONDTYPE');
        SCfwDomain domCondSel = new SCfwDomain('DOM_CONDSEL');

        String condSelV1 = '51595';
        
        domCondSel.setDomainValue(condSelV1,
                                  'V1=K0A0|V2=|V3=',
                                  Date.today().addDays(-10),
                                  null, condSelV1, 
                                  null, null, null);


        String condSelV2 = '51591';
        
        domCondSel.setDomainValue(condSelV2,
                                  'V1=A3|V2=|V3=',
                                  Date.today().addDays(-10), null, 
                                  condSelV2, 
                                  null, null, null);
        
                
        String conditionType = '51603';
        String conditionCP = 'V1=' + condSelV1 + '|V2=' + condSelV2 + '|V3=|V4=|V5=|V6=|V7='; 
        
        domCondType.setDomainValue(conditionType,
                                   conditionCP,
                                   Date.today().addDays(-10), 
                                   null, conditionType,
                                   null, null, null);

        createConditionUsage(null, null, null, null, 
                             conditionType, false, 10.0);
        

        createOrderLinePG(qty, price,
                          SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT,
                          SCfwConstants.DOMVAL_TAXACCOUNT_TAX
                         );

        
        createCondition(specialPrice, 
                        SCfwConstants.DOMVAL_CONDVALUETYPE_FIXED,
                        condSelV1,
                        SCfwConstants.DOMVAL_CONDPRICETYPE_PRICE,
                        Date.today().addDays(-1), 
                        null,
                        SCfwConstants.DOMVAL_CURR_XEU, 
                        SCfwConstants.DOMVAL_CONDCLASS_PRICEDETDEFAULT,
                        null,
                        SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT, 
                        (String)orderLine.get(0).Article__c,
                        null,
                        (String)SCHelperTestClass4.account.Id,
                        null);
        
        
        
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        
        
        SCPriceCalculation pricing = new SCPriceCalculation(order);
        
        System.assertNotEquals(pricing, null);
            
        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            ol = pricing.calculate(ol);
            
            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mistmatch');
                System.assertEquals(specialPrice, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * specialPrice), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
            } 
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false);
        }

    }
    
    static void createCondition(Decimal conditionValue, String conditionValueType,
        String conditionSelection, String conditionPriceType, 
        Date validFrom, Date validTo, String currencyDomain, 
        String conditionClass, String clearingType, String clearingClass, 
        String articleRef, String contrRef, String accountRef,
        SCPriceList__c priceList)
    {
        if (validFrom == null)
        {
            validFrom = Date.today();
        }
        
        if (currencyDomain == null)
        {
            currencyDomain = String.valueOf(SCfwConstants.DOMVAL_CURR_XEU);
        }
        
        // make sure to use the 15-digit version of IDs
        if (articleRef <> null) 
        {
        articleRef = articleRef.substring(0,15);
        }
        
        if (contrRef <> null)
        {
            contrRef = contrRef.substring(0,15);
        }
        
        if (accountRef <> null)
        {
            accountRef = accountRef.substring(0,15);
        }
        
        SCCondition__c cond = new SCCondition__c(
                                        Country__c = 'NL',
                                        ValidTo__c = validTo,
                                        ValidFrom__c = validFrom,
                                        //CurrencyIsoCode = currencyDomain,
                                        ConditionValue__c = conditionValue,
                                        ConditionValueType__c = conditionValueType,
                                        ConditionSelection__c = conditionSelection,
                                        ConditionPriceType__c = conditionPriceType,
                                        ConditionClass__c = conditionClass,
                                        InvoicingSubType__c = clearingType,
                                        InvoicingType__c = clearingClass,
                                        ArticleRef__c = articleRef,
                                        ContrRef__c = contrRef,
                                        AccountRef__c = accountRef,
                                        PriceList__r = priceList
                                        );
        
        insert cond;
        System.debug('tempCond -> ' + cond);
    }
*/    
    
    /**
     * Create a special purpose condition usage record that is used 
     * for unit tests.
     *
     * Remember that you might create other records (domain values) that 
     * work well with this one!!
     *
     * @param accountType account type
     * @param orderType order type
     * @param orderLineType service item type
     * @param conditionClass
     * @param conditionType
     * @param contraceRelated
     * @param sortOrder
     */
    static void createConditionUsage(String accountType, String orderType, 
                                     String orderLineType, String conditionClass,
                                     String conditionType, Boolean contractRelated,
                                     Decimal sortOrder)
    {
        SCConditionUsage__c condUsage = 
            new SCConditionUsage__c(
                                     AccountType__c = accountType,
                                     OrderType__c = orderType,
                                     OrderLineType__c = orderLineType,
                                     ConditionClass__c = conditionClass,
                                     ConditionType__c = conditionType,
                                     ContractRelated__c = contractRelated,
                                     Sort__c = sortOrder 
                                    );
                                    
        insert condUsage;
    }

    
/*
free of charge clearing class!!
            SCOrderLine__c ol2 = new SCOrderLine__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = oi.Id,
                                            OrderItem__r = oi,
                                            InvoicingType__c = '170005',
                                            Qty__c = 1,
                                           // TaxType__c = '50501',
                                            TaxGroup__c = '50711',
                                            Invoiced__c = false,
                                            Article__c = article.get(1).Id,
                                            Article__r = article.get(1),
                                            PricingDate__c = Date.today()
                                                    );
*/
    static SCPriceGroup__c createPG(String pgName)
    {
        SCPriceGroup__c pg = new SCPriceGroup__c(Name = pgName);
        insert pg;
        
        return pg;
        
    }

    static SCPriceListItem__c createPricePG(String pgName, Decimal price)
    {
        SCPriceGroup__c pg = createPG(pgName);

        SCPriceListItem__c pi = new SCPriceListItem__c(PriceList__c = pricelistPG.id,
                                                         Price__c = price,
                                                         QtyFrom__c = 1,
                                                         ValidFrom__c = Date.today().addDays(-1),
                                                         PriceGroup__c = pg.Id,
                                                         PriceGroup__r = pg
                                                        );
        insert pi;
        return pi;
    }

    static SCPriceListItem__c createPricePL(SCArticle__c article, Decimal price)
    {
        SCPriceListItem__c pi = 
            new SCPriceListItem__c(PriceList__c = pricelistPL.id,
                                    PriceList__r = pricelistPL,
                                    Price__c = price,
                                    QtyFrom__c = 1,
                                    ValidFrom__c = Date.today().addDays(-1),
                                    Article__r = article,
                                    Article__c = article.Id
                                   );
        insert pi;
        return pi;
    }

    static SCArticle__c createArticlePL(Decimal price)
    {
        System.debug('### createArticlePL');
        
        SCArticle__c article = 
            new SCArticle__c(Name = '1234567890',
//TODO check refactoring                              ArticleNameCalc__c = 'UT Article (PL)',
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                               ReturnType__c = '13901'
                             );

        insert article;
        
        SCPriceListItem__c pi = createPricePL(article, price); 
        
        return article;
        
    }

    static SCArticle__c createArticlePG(Decimal price)
    {
        SCPriceListItem__c pi = createPricePG('Price group (Unit Test)', price); 


        SCArticle__c article = 
            new SCArticle__c(Name = '1234567890',
//TODO Check refactoring                              ArticleNameCalc__c = 'UT Article (PG)',
                              Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_SPAREPART,
                              Type__c = SCfwConstants.DOMVAL_ARTICLETYP_NO_TYP,
                              Unit__c = SCfwConstants.DOMVAL_UNIT_PIECE,
                              AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A,
                              Pricegroup__r = pi.PriceGroup__r,
                              Pricegroup__c = pi.PriceGroup__r.Id,
                              TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                              PriceChangesEnabled__c = true,
                              ReturnType__c = '13901' 
                             );

        insert article;
        return article;
    }
    
    
    static void createOrderLinePL(Decimal qty, 
                                  Decimal price, 
                                  String clearingClass)
    {
        System.debug('### createOrderLinePL');
        
        SCArticle__c article = createArticlePL(price);
        order.pricelist__c = pricelistPL.id;
        
        for (SCOrderItem__c oi : orderItem)
        {
            // invoice item
            SCOrderLine__c ol = new SCOrderLine__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = oi.Id,
                                            OrderItem__r = oi,
                                            InvoicingType__c = clearingClass,
                                            Qty__c = qty,
                                            Invoiced__c = false,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            PricingDate__c = Date.today(),
                                            CurrencyIsoCode = SCfwConstants.DOMVAL_CURR_XEU
                                           );
            orderLine.add(ol);
        }
    }
    
    
    static void createOrderLinePG(Decimal qty, 
                                  Decimal price, 
                                  String invoicingType,
                                  String taxType)
    {
        SCArticle__c article = createArticlePG(price);
        order.pricelist__c = pricelistPG.id;
        
        for (SCOrderItem__c oi : orderItem)
        {
            // invoice item
            SCOrderLine__c ol = new SCOrderLine__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = oi.Id,
                                            OrderItem__r = oi,
                                            InvoicingType__c = invoicingType,
                                            Qty__c = qty,
                                           // TaxType__c = taxType,
                                            Invoiced__c = false,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            PricingDate__c = Date.today(),
                                            CurrencyIsoCode = SCfwConstants.DOMVAL_CURR_XEU
                                                    );
            orderLine.add(ol);
            ol = new SCOrderLine__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = oi.Id,
                                            OrderItem__r = oi,
                                            InvoicingType__c = invoicingType,
                                            Qty__c = null,
                                            ListPrice__c = -10,
                                           // TaxType__c = taxType,
                                            Invoiced__c = false,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            PricingDate__c = Date.today(),
                                            CurrencyIsoCode = SCfwConstants.DOMVAL_CURR_XEU,
                                            ManualPricing__c = true,
                                            ManualTax__c = true,
                                            Discount__c = 1000,
                                            Price__c = 50,
                                            PositionPrice__c = 50

                                                    );
            orderLine.add(ol);
            ol = new SCOrderLine__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            OrderItem__c = oi.Id,
                                            OrderItem__r = oi,
                                            InvoicingType__c = invoicingType,
                                            Qty__c = 0,
                                            ListPrice__c = -10,
                                           // TaxType__c = taxType,
                                            Invoiced__c = false,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            PricingDate__c = Date.today().addDays(-10),
                                            CurrencyIsoCode = SCfwConstants.DOMVAL_CURR_XEU,
                                            ManualPricing__c = true,
                                            ManualTax__c = true,
                                            Discount__c = 1000,
                                            Price__c = 50,
                                            PositionPrice__c = 50

                                                    );
            orderLine.add(ol);

        }
    }
    
    static void createOrderItem()
    {
        
        for (SCInstalledBase__c ib : SCHelperTestClass4.installedBase)
        {
            SCOrderItem__c oi = new SCOrderItem__c(
                                            Order__c = order.Id,
                                            Order__r = order,
                                            InstalledBase__c = ib.Id,
                                            InstalledBase__r = ib,
                                            CompletionStatus__c = '58701',
                                            ErrorSymptom1__c = '110012',
                                            ErrorSymptom2__c = '120110',
                                            ErrorSymptom3__c = '',
                                            ErrorCondition1__c = '100001'
                                                    );

            orderItem.add(oi);
        }
        
        insert orderItem;
    }
    
    


    static void createOrder()
    {
        /*
        Account__c = SCHelperTestClass4.account.Id,
                                Account__r = SCHelperTestClass4.account,
        */
        Account account = SCHelperTestClass4.account;
        account.DefaultPriceList__c = pricelistPG.id;
        
        order = new SCOrder__c (Type__c = SCfwConstants.DOMVAL_ORDERTYPE_DEFAULT,
                                Status__c = SCfwConstants.DOMVAL_ORDERSTATUS_OPEN,
                                Country__c='DE',
                                CustomerPrefStart__c = Date.today(),
                                CustomerPrefEnd__c = Date.today().addDays(10),
                                CustomerPriority__c = SCfwConstants.DOMVAL_ORDERPRIO_DEFAULT,
                                CustomerTimewindow__c = SCfwConstants.DOMVAL_CUSTOMERTIMEWINDOW_DEFAULT,
                                PaymentType__c = SCfwConstants.DOMVAL_PAYMENTTYPE_CASH,
                                InvoicingStatus__c = SCfwConstants.DOMVAL_INVOICINGSTATUS_OPEN
                               );
                             
        
        insert order;
        
        boOrderTest = new SCboOrder(order);
        
        boOrderTest.createDefaultRoles(account);
        
        boOrderTest.save();

    }
 
    static void setup()
    {
        pricelistPL = new SCPriceList__c(Name = 'Standard Test PG', Country__c='DE', ID2__c ='Test1');
        pricelistPG = new SCPriceList__c(Name = 'Standard Test PL', Country__c='DE', ID2__c ='Test2');
        insert pricelistPL;
        insert pricelistPG;
            
        SCHelperTestClass4.createAccount();
        SCHelperTestClass4.createArticle();
        SCHelperTestClass4.createInstalledBaseLocation();
        SCHelperTestClass4.createInstalledBase();
        SCHelperTestClass4.createTaxRecord();
        
        createOrder();
        createOrderItem();
    }
    

    /**
     * First pricing test:
     * - all positions have to be invoiced
     * - no additional conditions set
     * - list price is determince from price list
     * - no discount
     * - tax rate is 7% (taxable)
     */
    static testMethod void pricingInvoiceAllEx()
    {
        setup();
        
        Decimal price = 34.897;
        Decimal qty = 1;
        
        createOrderLinePL(qty, price, SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT);

        // '50711' // VAT: 7%
        
        // check if all variables are created fine
        System.assertNotEquals(SCHelperTestClass4.account, null);
        System.assertNotEquals(order, null);
        System.assert(orderItem.size() > 0);
        System.assert(SCHelperTestClass4.installedBase.size() > 0);
        System.assert(orderLine.size() > 0);
        
        
        // boOrderTest.order.pricelist__c = null;
        
        Test.startTest();
        
        // dummy (for coverage)
        SCPriceCalculation pricing = new SCPriceCalculation(boOrderTest);

        // simple test for pricelist access
        SCPriceList__c pricelisttest = pricing.getPriceListById(pricelistPG.id);
        System.assert(pricelisttest != null);
        
        integer i = 0;
        // do the complete pricing and taxing
        for (SCOrderLine__c ol: orderLine)
        {
            if(i == 0)
            {
                ol.RelativeDiscount__c = -10;
                ol.PositionPrice__c = -1;
    
            } 
            
            ol = pricing.calculate(ol);
            
            if (SCfwConstants.DOMVAL_INVOICINGTYPE_DEFAULT == ol.InvoicingType__c)
            {
                // invoice item
                System.assertEquals(price, ol.ListPrice__c, 'List price mismatch');
                System.assertEquals(price, ol.UnitPrice__c, 'Unit price mismatch');
                System.assertEquals((qty * price), ol.PositionPrice__c, 'Position price mismatch');
                System.assertEquals(ol.PositionPrice__c, ol.Price__c, 'Price (net) mismatch');
                System.assertEquals((((((ol.Price__c + ol.Tax__c) / ol.Price__c) * 100) - 100) * 10000).round(),
                                    (ol.TaxRate__c * 10000).round(), 'VAT calculation mismatch');
                
            } 
            i++;
        }

        try {
            insert orderLine;
        }
        catch (DMLException e)
        {
            // Should never reach this
            System.assert(false, 'Unable to insert orderLine ' + e.getMessage());
        }
        
        Test.stopTest();
    }
    
    
}