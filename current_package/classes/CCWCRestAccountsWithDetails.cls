/**********************************************************************
Name: CCWCRestAccountsWithDetails()
======================================================
Purpose: 
Represents a web service. Gets list of accounts by Id or by list of Ids.  
======================================================
History 
------- 
Date AUTHOR DETAIL 
01/22/2014 Jan Mensfeld INITIAL DEVELOPMENT
***********************************************************************/

@RestResource(urlMapping='/AccountsWithDetails/*')
global with sharing class CCWCRestAccountsWithDetails {
	
	@TestVisible
	private static final String sSalesAppSettingNameForQueryLimit = 'AccountsWithDetailsQueryLimit';
	private static final String sFieldSetName = 'SalesAppQueryAccountWithDetailsFields';

	private static final String sUrlParameterAccountIds = 'accountIds';
	private static final String sUrlParameterOffsetId = 'offsetId';
	private static final String sUrlParameterTimeStamp = 'timestamp';
	
	private static final String sTaskStatusOpen = 'Open';
	private static final String sTaskStatusClosed = 'Completed';
	
	private static final String sSubSelectAttachment = ', ( SELECT Id, Description, Name FROM Attachments LIMIT 1)';	
	private static String sLimit = getLimitFromCustomSetting();
	private static String getLimitFromCustomSetting(){		
		if(Test.isRunningTest()) return '5';
		SalesAppSettings__c objSalesAppSetting = SalesAppSettings__c.getValues(sSalesAppSettingNameForQueryLimit);
		return objSalesAppSetting.Value__c;		
	}
	
	// to map object name and relationship name
	private static Map<String, String> mapAccountChildRelationShips = getAccountChildRelationShips();
	private static Map<String, String> getAccountChildRelationShips() {

		Schema.DescribeSObjectResult describeResult = Account.SObjectType.getDescribe();
		List<Schema.ChildRelationship> listChildRelationships = describeResult.getChildRelationships();		
		
		Map<String, String> mapChildRelationShipName = new Map<String, String>();		
		
		for(Schema.ChildRelationship childRel : listChildRelationships){
			String sChildObjectName = childRel.getChildSObject().getDescribe().getName();
			String sChildRelName = childRel.getRelationshipName();
			if (sChildRelName != null && sChildRelName != '')
		    	mapChildRelationShipName.put(sChildObjectName, sChildRelName);
		}	
		
		return mapChildRelationShipName;
	}
	
	private static List<Schema.FieldSetMember> getDetailFieldSetMembers(String sObjectName) {
		Map<String, Schema.SObjectType> mapGlobalDescribe = Schema.getGlobalDescribe(); 
	    Schema.SObjectType objSObjectType = mapGlobalDescribe.get(sObjectName);
	    
		Schema.DescribeSObjectResult objDescribeSObjectResult = objSObjectType.getDescribe();
	    Schema.FieldSet objFieldSet = objDescribeSObjectResult.FieldSets.getMap().get(sFieldSetName);

		if (objFieldSet != null)		
			return objFieldSet.getFields();
		return null;
	}
	
	private static String getCommaSeparatedQueryFieldsFromObject(String sObjectName) {
		if (Test.isRunningTest()){
			String testFields = 'Id';
			if (sObjectName == 'Task')
				testFields += ', Subject';
			else if (sObjectName == 'Account')
				testFields += ', IsDeleted';
			return testFields;
		}
			
		List<String> fieldList = new List<String>();		
		List<Schema.FieldSetMember> listFieldSetMember = getDetailFieldSetMembers(sObjectName);
		if (listFieldSetMember == null)
			return '';		
		for (Schema.FieldSetMember f : getDetailFieldSetMembers(sObjectName))
			fieldList.add(f.getFieldPath());
		fieldList.add('LastModifiedDate');
		return String.join(fieldList, ', ');
	}

	private static AccountWithDetails createAccountWithError(Exception e){
		AccountWithDetails a = new AccountWithDetails();
		a.Message = e.getTypeName() + ' ' + e.getMessage();
		return a;
	} 
		
	private static AccountWithDetails createAccountWithError(String sMessage){
		AccountWithDetails a = new AccountWithDetails();
		a.Message = sMessage;
		return a;
	} 
	
	private static String getParameterValueFromUrlRequest(String sUrlParameter) {
		try {
			RestRequest objRequest = RestContext.Request;
			return objRequest.params.get(sUrlParameter);			
		}
		catch(TypeException e) {
			System.debug(e.getTypeName() + ': ' + e.getMessage());
			return null;
		}
	}
		
	private static Set<Id> getAccountIdSetFromString(String sCommaSeparatedAccountIds) {
		Set<Id> accountIds = new Set<Id>();
		if(sCommaSeparatedAccountIds == null || sCommaSeparatedAccountIds == '')
			return accountIds;
		
		List<String> listAccountIds = sCommaSeparatedAccountIds.split(',');
		for (String accId : listAccountIds) {
			try {
				accountIds.add(Id.valueOf(accId));
			} catch(Exception e) {
				System.debug(e.getTypeName() + ' ' + e.getMessage());
			}
		}
		return accountIds;
	}
	
	private static Datetime getDateTimeFromTimeStamp(String sTimeStamp){
		try {
			return Datetime.valueOf(sTimeStamp.replace('T', ' '));
		} catch(TypeException e) {
			System.debug(e.getMessage());
			return null;
		}
	}
	
	private static List<Task> getAccountTasks(Set<Id> accountIds) {
		String sTaskQuery = 'SELECT ' + getCommaSeparatedQueryFieldsFromObject('Task') + sSubSelectAttachment + ' FROM Task' + 
			' WHERE AccountId IN (' + getCommaSeparatedIdListForQueries(accountIds) + ')' +
			' ORDER BY CreatedDate';
		System.debug('Debug JM: ' + sTaskQuery);
		return Database.query(sTaskQuery);
	}
	
	private static String getCommaSeparatedIdListForQueries(Set<Id> ids) {
		return '\'' + String.join(new List<Id>(ids), '\',\'') + '\'';
	}
	
	private static List<Task> getFilteredTasks(List<Task> listAccountTasks, Id accountId) {
		List<Task> listOpenTask = new List<Task>();
		List<Task> listClosedTask = new List<Task>();
		List<Task> listFilteredTask = new List<Task>();
		for(Task t : listAccountTasks) {
			if (t.Status == sTaskStatusOpen)
				listOpenTask.add(t);
			if (listClosedTask.size() < 10 && t.Status == sTaskStatusClosed)
				listClosedTask.add(t);
		}
		
		listFilteredTask.addAll(listOpenTask);
		listFilteredTask.addAll(listClosedTask);
		return listFilteredTask;
	}
	
	global class AccountWithDetails{
		public Account Account { get; set; }
		//public SCInstalledBaseLocation__c InstalledBaseLocation { get; set; }
		public List<SCInstalledBase__c> InstalledBases { get; set; }
		public List<Task> Tasks { get; set; }
		public String Message { get; set; }
	}
/*	
	global class AccountWithDetails2{
		public Account Account { get; set; }
		public BankAccount__c BankAccount { get; set; }
		public Contact Contact { get; set; }
		public String Message { get; set; }
		public RedSurveyResult__c RedSurveyResult { get; set; }
		public SalesDetails__c SalesDetails { get; set; }
		public SCAccountInfo__c SCAccountInfo { get; set; }
		public List<SCInstalledBase__c> SCInstalledBases { get; set; }
		public List<Task> Tasks { get; set; }
	}
*/
	@HttpGet
	global static List<AccountWithDetails> doGet(){
		String sCommaSeparatedAccountIds = getParameterValueFromUrlRequest(sUrlParameterAccountIds);
		Set<Id> accountIds = getAccountIdSetFromString(sCommaSeparatedAccountIds);
		String sOffsetId = getParameterValueFromUrlRequest(sUrlParameterOffsetId);
		String sTimeStamp = getParameterValueFromUrlRequest(sUrlParameterTimeStamp);
		Datetime dtLastModifiedDate;
		
		if(sTimeStamp != null && sTimeStamp != '')
			dtLastModifiedDate = getDateTimeFromTimeStamp(sTimeStamp);
		
		List<Account> listAccount = new List<Account>();
		AccountWithDetails objResult;
		List<AccountWithDetails> listResult = new List<AccountWithDetails>();
		
		String sQuery = 'SELECT ' + getCommaSeparatedQueryFieldsFromObject('Account') + 
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('BankAccount__c') + ' FROM ' + mapAccountChildRelationShips.get('BankAccount__c') + ') ' +	
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('SCAccountInfo__c') + ' FROM ' + mapAccountChildRelationShips.get('SCAccountInfo__c') + ') ' +	
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('SalesDetails__c') + ' FROM ' + mapAccountChildRelationShips.get('SalesDetails__c') + ' ORDER BY CreatedDate LIMIT 6) ' +
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('PromotionMember__c') + ' FROM ' + mapAccountChildRelationShips.get('PromotionMember__c') + ') ' +
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('RedSurveyResult__c') + ' FROM ' + mapAccountChildRelationShips.get('RedSurveyResult__c') + ') ' +
							', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('Contact') + ' FROM ' + mapAccountChildRelationShips.get('Contact') + ' WHERE IsPrimarySalesContact__c = true) ' +
							//', (SELECT ' + getCommaSeparatedQueryFieldsFromObject('Task') + ' FROM ' + mapAccountChildRelationShips.get('Task') + ' ORDER BY CreatedDate) ' +
						'FROM Account ';
		
		if ((accountIds != null && accountIds.size() > 0) ||
			(sOffsetId != null && sOffsetId != '') ||
			(dtLastModifiedDate != null))
			sQuery += 'WHERE ';
		
		if (accountIds != null && accountIds.size() > 0)
			sQuery += '(Id IN (' + getCommaSeparatedIdListForQueries(accountIds) + ')) ';		
		
		//2014-01-01T14:08:20.000Z
		if (dtLastModifiedDate != null) {			
			if (!sQuery.endsWith('WHERE '))
				sQuery += 'AND ';
			Set<Id> accountIdsOfChildElements = new Set<Id>();
			List<Contact> listModifiedContact = [SELECT AccountId FROM Contact WHERE LastModifiedDate > : dtLastModifiedDate];
			List<Task> listModifiedTask = [SELECT AccountId FROM Task WHERE LastModifiedDate > : dtLastModifiedDate];
			List<BankAccount__c> listModifiedBankAccount = [SELECT Account__c FROM BankAccount__c WHERE LastModifiedDate > : dtLastModifiedDate];
			
			for(Contact c : listModifiedContact)
				accountIdsOfChildElements.add(c.AccountId);
			
			sQuery += String.format('(LastModifiedDate > {0}) ', new String [] {sTimeStamp} );
			if (accountIdsOfChildElements.size() > 0) 
				sQuery += 'AND (Id IN (' + getCommaSeparatedIdListForQueries(accountIdsOfChildElements) + ')) ';
		}
		
		if (sOffsetId != null && sOffsetId != '') {
			if (!sQuery.endsWith('WHERE '))
				sQuery += ' AND ';
			sQuery += '(Id > \'' + sOffsetId + '\')';
		}		 
		
		sQuery += ' ORDER BY Id LIMIT ' + sLimit + 'ALL ROWS';

		System.debug('Debug JM: before querying "' + sQuery + '""');
		//try{
			listAccount = Database.query(sQuery);
			if(listAccount.size() == 0) {
				listResult.add(createAccountWithError('Found ' + listAccount.size() + ' Accounts where Id in (' + getCommaSeparatedIdListForQueries(accountIds) + ')'));
			}
			
			if(accountIds.size() == 0)
				for (Account a : listAccount)
					accountIds.add(a.Id);
			
			List<SCInstalledBaseRole__c> listInstalledBaseRole = [SELECT Id, InstalledBaseLocation__c, Account__c FROM SCInstalledBaseRole__c WHERE Account__c IN : accountIds];
			Map<Id, SCInstalledBaseRole__c> mapAccountId_InstalledBaseRole = new Map<Id, SCInstalledBaseRole__c>();
			Map<Id, Id> mapInstalledBaseRoleId_InstalledBaseLocationId = new Map<Id, Id>();
			Set<Id> installedBaseRoleIds = new Set<Id>();
			Set<Id> installedBaseLocationIds = new Set<Id>();		
			
			for (SCInstalledBaseRole__c r : listInstalledBaseRole) {
				mapAccountId_InstalledBaseRole.put(r.Account__c, r);
				mapInstalledBaseRoleId_InstalledBaseLocationId.put(r.Id, r.InstalledBaseLocation__c);
				installedBaseRoleIds.add(r.Id);
				installedBaseLocationIds.add(r.InstalledBaseLocation__c);
			}
			
			String sInstalledBaseLocationQuery = 'SELECT Id, (SELECT ' + getCommaSeparatedQueryFieldsFromObject('SCInstalledBase__c') + 
				' FROM InstalledBaseLocation__r) FROM SCInstalledBaseLocation__c WHERE Id IN (' + getCommaSeparatedIdListForQueries(installedBaseLocationIds) + ')';
			List<SCInstalledBaseLocation__c> listInstalledBaseLocation = Database.query(sInstalledBaseLocationQuery);
			Map<Id, SCInstalledBaseLocation__c> mapInstalledBaseLocationId_InstalledBaseLocation = new Map<Id, SCInstalledBaseLocation__c>();
			for(SCInstalledBaseLocation__c l : listInstalledBaseLocation) {
				mapInstalledBaseLocationId_InstalledBaseLocation.put(l.Id, l);
			}
					
			List<Task> listAccountTasks = getAccountTasks(accountIds);
			Account acc = new Account(Name='TestAcc');	
			for(Account a : listAccount) {
				objResult = new AccountWithDetails();
				if (a.IsDeleted) {
					objResult.Message = 'Account with ID ' + a.Id + ' is deleted';
					listResult.add(objResult);	
					continue;
				}
				objResult.Account = a;
				objResult.Tasks = getFilteredTasks(listAccountTasks, a.Id);
				SCInstalledBaseRole__c role = mapAccountId_InstalledBaseRole.get(a.Id);
				if(role != null){
					Id locId = mapInstalledBaseRoleId_InstalledBaseLocationId.get(role.Id);
					SCInstalledBaseLocation__c location = mapInstalledBaseLocationId_InstalledBaseLocation.get(locId);
					//objResult.InstalledBaseLocation = location;
					objResult.InstalledBases = location.InstalledBaseLocation__r;
				}
				listResult.add(objResult);
			}
		//}
		//catch(Exception e) {
		//	listResult.add(createAccountWithError(e));	
		//}		
		return listResult;		
	}
	
}