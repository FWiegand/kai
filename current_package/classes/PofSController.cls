/**********************************************************************

Name: fsFA_PofS_Controller

======================================================

Purpose: 

This class serves as the data management controller for the PofS edit view.

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/05/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT
01/07/2013		Jan Mensfeld		Added List of PofSDeviceModel

***********************************************************************/

public class PofSController{

	//Non-Static Variables-----------------------------------------------------------------------------
	ApexPages.StandardController GO_Controller;
    public fsFA_PofS_DocumentController GO_DocumentController;
    public String GV_ImageId;
	public String GV_NewImageName;
	
	public PictureOfSuccess__c GO_Image;
    public PofSSettings__c GO_PofSSettings;
    public List<PofSDeviceModel__c> GO_PofSDeviceModels;
        
	//getter & setter methods------------------------------------------------------------------------	
	public fsFA_PofS_DocumentController getGO_DocumentController(){
		return GO_DocumentController;
	}
	
	public void setGO_DocumentController( fsFA_PofS_DocumentController PO_Value){
		GO_DocumentController = PO_Value;
	}
	
	public String getGV_ImageId(){
		return GV_ImageId;
	}
		
	public void setGV_ImageId( String PO_Value){
		GV_ImageId = PO_Value;
	}
	
	public String getGV_NewImageName(){
		return GV_NewImageName;
	}
	
	public void setGV_NewImageName( String PO_Value){
		GV_NewImageName = PO_Value;
	}
	
	public PictureOfSuccess__c getGO_Image(){
		return GO_Image;
	}
	
	public void setGO_Image( PictureOfSuccess__c PO_Value){
		GO_Image = PO_Value;
	}

	public PofSSettings__c getGO_PofSSettings(){
			if( GO_PofSSettings == null ){
                GO_PofSSettings = PofSSettings__c.getInstance( 'Default' );
            }
		return GO_PofSSettings;
	}
	
	public void setGO_PofSSettings( PofSSettings__c PO_Value){
		GO_PofSSettings = PO_Value;
	}	

	public List<PofSDeviceModel__c> getGO_PofSDeviceModels() {
		return GO_PofSDeviceModels;
	}
	
	//Constructors-----------------------------------------------------------------------------
    public PofSController( ApexPages.StandardController PO_Controller ){
		System.debug( 'Entering fsFA_PofS_Controller: ' );
		
		GO_Controller = PO_Controller;
        GO_Image = ( PictureOfSuccess__c )PO_Controller.getRecord();    
        GO_PofSDeviceModels = 
        	[SELECT Id, Name, 
        		DeviceModel__r.Name, 
        		DeviceModel__r.AvailableBrands__c, 
        		DeviceModel__r.Brand__c
        	 FROM PofSDeviceModel__c 
        	 WHERE PictureOfSuccess__c =: GO_Image.Id];
        GO_DocumentController = new fsFA_PofS_DocumentController();
		System.debug( 'Exiting fsFA_PofS_Controller: ' );
    }
    
    //Action Methods---------------------------------------------------------------------------
	
	/*******************************************************************
	 Purpose: 		Dummy Function
	 ********************************************************************/
	public void Dummy(){
		System.debug( 'Entering Dummy: ' );
		
        if( GV_ImageId != '000000000000000' ){
			GO_Image.Picture__c = GV_ImageId;
		}
		
		System.debug( 'Exiting Dummy: ' );
    }
    
	/*******************************************************************
	 Purpose: 		Refresh the list of background images after an image has been uploaded
	 ********************************************************************/
    public void refreshBackgrounds(){
		System.debug( 'Entering refreshBackgrounds: ' );
		
		if( GV_NewImageName != '' ){
	        GO_DocumentController.refreshGL_Backgrounds();
			GV_ImageId = GO_DocumentController.getBackgroundByName( GV_NewImageName );
			System.debug( 'NewImageId: ' + GV_ImageId );
			if( GV_ImageId != '000000000000000' ){
	        	GO_Image.Picture__c = GV_ImageId;
			}
		}
		
		System.debug( 'Exiting refreshBackgrounds: ' );
    }	
}