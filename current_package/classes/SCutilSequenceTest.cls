/*
 * @(#)SCutilSequenceTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCutilSequence that generates
 * squence numbers (in fact strings).
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision: 852 $, $Date: 2010-12-01 11:39:36 +0100 (Wed, 01 Dec 2010) $
 */
@isTest
private class SCutilSequenceTest {


    /**
     * Test the sequence generation for a country specific sequence
     */
    static testMethod void testNextCountry()
    {
        createTestData1();
        
        String nextSequence = SCutilSequence.next('TESTCYCLE', 'DE');
    
        System.assertEquals('51', 
                            nextSequence,
                            '1st call failed');
        
        System.assertEquals('52', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            '2nd call failed');
    
        System.assertEquals('53', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            '3rd call failed');
    }
    
    /**
     * Test the sequence generation for an invalid country using the
     * global sequence instead
     */
    static testMethod void testNextGlobal()
    {
        createTestData1();
        
        System.assertEquals('1', 
                            SCutilSequence.next('TESTCYCLE', 'INVALID'),
                            '1st call failed');
                            
        System.assertEquals('2', 
                            SCutilSequence.next('TESTCYCLE', 'INVALID'),
                            '2nd call failed');
                            
        System.assertEquals('3', 
                            SCutilSequence.next('TESTCYCLE', 'INVALID'),
                            '3rd call failed');
    }

    /**
     * Cycling test on a cycling enabled sequence, after reaching the maximum
     * value the sequence shall restart.
     */
    static testMethod void testCyclePositive()
    {
        createTestData1();
        
        System.assertEquals('51', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            '1st call failed');
                            
        System.assertEquals('52', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            '2nd call failed');
                            
        System.assertEquals('53', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            '3rd call failed');
                            
        System.assertEquals('51', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            'Cycling failed');
    }

    /**
     * Cycling test on a cycling disable sequence, after reaching the maximum
     * value an exception has to be thrown.
     */
    static testMethod void testCycleNegative()
    {
        createTestData1();
        
        System.assertEquals('10', 
                            SCutilSequence.next('TESTNOCYCLE', ''),
                            '1st call failed');
                            
        System.assertEquals('11', 
                            SCutilSequence.next('TESTNOCYCLE', ''),
                            '2nd call failed');
                            
        System.assertEquals('12', 
                            SCutilSequence.next('TESTNOCYCLE', ''),
                            '3rd call failed');

        try
        {
            String nextSequence = SCutilSequence.next('TESTNOCYCLE', '');
            System.assert(false, 'No exception despite maximum limit reached!!');
        }
        catch (SCutilSequenceException e)
        {
            String err = 'Maximum number for sequence TESTNOCYCLE reached';
            System.assert(e.getMessage().contains(err), e.getMessage());
        }
                
        System.assertEquals('51', 
                            SCutilSequence.next('TESTCYCLE', 'DE'),
                            'Cycling failed');
    }

    /**
     * Test getting values for an invalid sequence. This should throw an
     * exception
     */
    static testMethod void testInvalidSequence()
    {
        createTestData1();
        
        try
        {
            String nextSequence = SCutilSequence.next('INVALID', 'INVALID');
            System.assert(false, 'No exception despite invalid country!!');
        }
        catch (SCutilSequenceException e)
        {
            String err = 'No sequence found for INVALID';
            System.assert(e.getMessage().contains(err), e.getMessage());
        }
    }
    
    /**
     * Test the sequence generation for a prefix enable sequence
     */
    static testMethod void testNextPrefix()
    {
        createTestData1();
        
        System.assertEquals('PRE-20', 
                            SCutilSequence.next('TESTPREFIX', ''),
                            '1st call failed');
        
        System.assertEquals('PRE-21', 
                            SCutilSequence.next('TESTPREFIX', ''),
                            '2nd call failed');
    
        System.assertEquals('PRE-22', 
                            SCutilSequence.next('TESTPREFIX', ''),
                            '3rd call failed');
    }


    /**
     * Test the sequence generation for a prefix enabled sequence
     */
    static testMethod void testNextPostfix()
    {
        createTestData1();
        
        System.assertEquals('30-POST', 
                            SCutilSequence.next('TESTPOSTFIX', ''),
                            '1st call failed');
        
        System.assertEquals('31-POST', 
                            SCutilSequence.next('TESTPOSTFIX', ''),
                            '2nd call failed');
    
        System.assertEquals('32-POST', 
                            SCutilSequence.next('TESTPOSTFIX', ''),
                            '3rd call failed');
    }
    

    /**
     * Test the sequence generation for a prefix and postfix 
     * enabled sequence
     */
    static testMethod void testNextPrePostfix()
    {
        createTestData1();
        
        System.assertEquals('PRE-40-POST', 
                            SCutilSequence.next('TESTPREPOST', ''),
                            '1st call failed');
        
        System.assertEquals('PRE-41-POST', 
                            SCutilSequence.next('TESTPREPOST', ''),
                            '2nd call failed');
    
        System.assertEquals('PRE-42-POST', 
                            SCutilSequence.next('TESTPREPOST', ''),
                            '3rd call failed');
    }


    /**
     * Test the sequence generation for a sequence with 
     * an increment larger than 1
     */
    static testMethod void testNextIncrement()
    {
        createTestData1();
        
        System.assertEquals('60', 
                            SCutilSequence.next('TESTINCREMENT_DE', ''),
                            '1st call failed');
        
        System.assertEquals('62', 
                            SCutilSequence.next('TESTINCREMENT_DE', ''),
                            '2nd call failed');
    
        System.assertEquals('64', 
                            SCutilSequence.next('TESTINCREMENT_DE', ''),
                            '3rd call failed');
    }
    
    /**
     * Test the sequence generation for a country specific sequence
     * in bulk mode
     */
    static testMethod void testNextBulk()
    {
        createTestData1();
        Integer bulkSize = 10;
        
        List<String> nextSequence = SCutilSequence.next('TESTBULK', 'DE', bulkSize);
    
        System.assertEquals(bulkSize, nextSequence.size());

        System.assertEquals('51', 
                            nextSequence[0],
                            '1st sequence number wrong');
        
        System.assertEquals('52', 
                            nextSequence[1],
                            '2nd sequence number wrong');
        
        System.assertEquals('60', 
                            nextSequence[9],
                            'Last sequence number wrong');

    }


    /**
     * Test the sequence generation for a country specific sequence
     * in bulk mode hitting the cycle limit
     */
    static testMethod void testNextBulkCycle()
    {
        createTestData1();
        Integer bulkSize = 21;
        
        List<String> nextSequence = SCutilSequence.next('TESTBULK', 'DE', bulkSize);

        System.assertEquals(bulkSize, nextSequence.size());

        System.assertEquals('51', 
                            nextSequence[0],
                            '1st sequence number wrong');
        
        System.assertEquals('52', 
                            nextSequence[1],
                            '2nd sequence number wrong');

        System.assertEquals('70', 
                            nextSequence[19],
                            '2nd last sequence number wrong');

        System.assertEquals('51', 
                            nextSequence[20],
                            'Last sequence number wrong');

    }


    /**
     * Test the sequence generation for a country specific sequence
     * in bulk mode hitting the cycle limit with a huge bulk
     * size trying to hit governor limits.
     */
    static testMethod void testNextBulkHuge()
    {
        createTestData1();
        Integer bulkSize = 150;
        
        List<String> nextSequence = SCutilSequence.next('TESTBULK', 'DE', bulkSize);

        System.assertEquals(bulkSize, nextSequence.size());

        System.assertEquals('51', 
                            nextSequence[0],
                            '1st sequence number wrong');
        
        System.assertEquals('52', 
                            nextSequence[1],
                            '2nd sequence number wrong');

        System.assertEquals('70', 
                            nextSequence[19],
                            '2nd last sequence number wrong');

        System.assertEquals('51', 
                            nextSequence[20],
                            'Last sequence number wrong');

    }


    private static void createTestData1()
    {
        List<SCConfSequence__c> allSeq = new List<SCConfSequence__c>();
    
        allSeq.add(new SCConfSequence__c(Name = 'TESTCYCLE',
                                         Min__c = 1,
                                         Max__c = 3,
                                         IncrementBy__c = 1,
                                         Cycle__c = true,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = ''
                                         )
                 );

 
       
        allSeq.add(new SCConfSequence__c(Name = 'TESTNOCYCLE',
                                         Min__c = 10,
                                         Max__c = 12,
                                         IncrementBy__c = 1,
                                         Cycle__c = false,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = ''
                                         )
                 );
                 
        allSeq.add(new SCConfSequence__c(Name = 'TESTPREFIX',
                                         Min__c = 20,
                                         Max__c = 23,
                                         IncrementBy__c = 1,
                                         Cycle__c = false,
                                         Current__c = 0,
                                         Prefix__c = 'PRE-',
                                         Postfix__c = ''
                                         )
                 );
 
         allSeq.add(new SCConfSequence__c(Name = 'TESTPOSTFIX',
                                         Min__c = 30,
                                         Max__c = 33,
                                         IncrementBy__c = 1,
                                         Cycle__c = false,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = '-POST'
                                         )
                 );
                 
        allSeq.add(new SCConfSequence__c(Name = 'TESTPREPOST',
                                         Min__c = 40,
                                         Max__c = 43,
                                         IncrementBy__c = 1,
                                         Cycle__c = false,
                                         Current__c = 0,
                                         Prefix__c = 'PRE-',
                                         Postfix__c = '-POST'
                                         )
                 );

        allSeq.add(new SCConfSequence__c(Name = 'TESTCYCLE_DE',
                                         Min__c = 51,
                                         Max__c = 53,
                                         IncrementBy__c = 1,
                                         Cycle__c = true,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = ''
                                         )
                 );

        allSeq.add(new SCConfSequence__c(Name = 'TESTBULK_DE',
                                         Min__c = 51,
                                         Max__c = 70,
                                         IncrementBy__c = 1,
                                         Cycle__c = true,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = ''
                                         )
                 );


        allSeq.add(new SCConfSequence__c(Name = 'TESTINCREMENT_DE',
                                         Min__c = 60,
                                         Max__c = 66,
                                         IncrementBy__c = 2,
                                         Cycle__c = true,
                                         Current__c = 0,
                                         Prefix__c = '',
                                         Postfix__c = ''
                                         )
                 );
  
        insert allSeq;
                                         
    }   

}