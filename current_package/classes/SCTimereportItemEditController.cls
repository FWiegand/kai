/*
 * @(#)SCTimereportItemEditController.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the controller for simple hovers that can be used to
 * display context sensitive information. The controller evalates
 * the mode and oid field and reads the corresponding data.
 *
 * @author Dietrich Herdt  <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCTimereportItemEditController extends SCfwComponentControllerBase
{
    public String tid {get; set; }
    public String mode {get; set; }
    public String isNewEntry {get; set; }
    public String selectedTimeReportType { get; set; }

    public SCTimereport__c timereportSingle { get; set; }
    public SCTimereport__c newEntryTimeReport { get; set; }

    public boolean isEditMode {get; set; }
    public boolean allOk { get; set; }
    public boolean isDayStart { get; set; }
    public boolean fillGaps { get; set; }

    public Datetime lastEndTime { get; set; }
    public Integer lastMileageEntry { get; set; }
    private SelectOption noneOption = new SelectOption('', System.Label.SC_flt_None);
    private Integer maximumDuration  { get; set; }
    private Integer defaultDuration  { get; set; }

    private SCTimereportViewController controller;

    /**
     * Default constructor
     */
    public SCTimereportItemEditController()
    {
        System.debug('#### SCTimereportItemEditController');

        // get the apex page parameteres
        tid = ApexPages.currentPage().getParameters().get('tid');
        mode = ApexPages.currentPage().getParameters().get('mode');
        isNewEntry = ApexPages.currentPage().getParameters().get('isNewEntry');

        System.debug('#### SCTimereportItemEditController tid: ' + tid);
        System.debug('#### SCTimereportItemEditController mode: ' + mode);
        System.debug('#### SCTimereportItemEditController isNewEntry: ' + isNewEntry);

        // if idtwo is not null,
        // is it addMode for empty time report entry

        if (isNewEntry == 'timeReportNew')
        {
            mode = 'addMode';
            System.debug('#### SCTimereportItemEditController mode change: '
                         + mode);
        }

        if (mode == 'editMode')
        {
            isEditMode = true;
        }
        else
        {
            isEditMode = false;
        }

        // application settings parameters
        SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();
        // Defines the time in minutes in which entries may be created in the future.
        if (appset.TIMEREPORT_MAXFUTURE__c != null)
        {
            maximumDuration = appset.TIMEREPORT_MAXFUTURE__c.intValue();
        }
        else
        {
            // if app settings is not defined, set period to 600 minutes (10 hours)
            maximumDuration = 600;
        }

        // Defines the duration of a time report entry in minutes.
        if (appset.TIMEREPORT_DEFAULTDURATION__c != null)
        {
            defaultDuration = appset.TIMEREPORT_DEFAULTDURATION__c.intValue();
        }
        else
        {
            // if app settings is not defined, set period to 60 minutes
            defaultDuration = 60;
        }

        System.debug('#### SCTimereportItemEditController maximumDuration: '
                     + maximumDuration);
        System.debug('#### SCTimereportItemEditController defaultDuration: '
                     + defaultDuration);

        init(tid);
    }

    /**
     * Constructor in case it's an extension
     */
    public SCTimereportItemEditController(SCTimereportViewController controller)
    {
        this.controller = controller;
    }

    public Boolean getRenderDialog()
    {
        tid = ApexPages.currentPage().getParameters().get('tid');
        mode = ApexPages.currentPage().getParameters().get('mode');

        if (tid != null && mode != null)
        {
            return true;
        }

        return false;
    }

    /**
     * Initialize all necessary datas.
     * @ tid   time report id
     */
    public void init(String tid)
    {
        String ReportLink = null;
        // time variables for new entry
        Datetime nextLastTime = null;
        Datetime nextTime = null;

        fillGaps = false;
        allOk = false;
        //toolData = new SCTool__c();
        System.debug('#### TID --> ' +tid);

        // try to find the selected entry into DB
        try
        {
            timereportSingle = [select Id, Start__c, End__c, ReportLink__c,
                                Resource__c, Type__c, Description__c, Distance__c,
                                CompensationType__c, Mileage__c, Status__c
                                    from SCTimereport__c
                                where Id = :tid];
        }
        catch (QueryException e)
        {
            //FIXME!!!
            // no need to to any further initialisation.
            return;
        }
        System.debug('#### timereportSingle ' +timereportSingle);
        /*
         * Round the time report time dependent on custom settings,
         * start time should be already rounded.
         */
        timereportSingle.Start__c = SCutilDatetime.roundMinutes(timereportSingle.Start__c);
        timereportSingle.End__c = SCutilDatetime.roundMinutes(timereportSingle.End__c);

        System.debug('#### timereportSingle.Start__c is: '+timereportSingle.Start__c);
        System.debug('#### timereportSingle.End__c  is: '+timereportSingle.End__c);

        // end time dependent on TIMEREPORT_MAXFUTURE
        nextTime = timereportSingle.End__c.addMinutes(defaultDuration);
        System.debug('#### nextTime is: '+nextTime);

        // time report must end after 24 hours, last time is start + 24 hours
        if (timereportSingle.ReportLink__c == null)
        {
            // day start was selected
            lastEndTime = timereportSingle.Start__c.addHours(24);
        }
        else
        {
            try
            {
                SCTimereport__c timeReportStart = [select Id, Start__c, End__c
                                                         from SCTimereport__c
                                          where Id = :timereportSingle.ReportLink__c];

                lastEndTime = timeReportStart.Start__c.addHours(24);

                // if next time greater than last time, set next time to last time
                if (nextTime > lastEndTime)
                {
                    nextTime = lastEndTime;
                    System.debug('#### nextTime set to lastEndTime: '+nextTime);
                }
            }
            catch (Exception e)
            {
                lastEndTime = null;
            }
        }
        System.debug('#### lastEndTime is: '+lastEndTime);

        // try to found next time report entry,
        // if found the current end is equal next entry start
        try
        {
          SCTimereport__c nextTimeReport = [select Id, Start__c, End__c, ReportLink__c,
                             Resource__c, Type__c, Description__c, Distance__c,
                             CompensationType__c, Mileage__c, Status__c
                          from SCTimereport__c
                          where Resource__c = :timereportSingle.Resource__c and
                          (ReportLink__c = :timereportSingle.ReportLink__c or
                          ReportLink__c = :timereportSingle.Id) and
                          Start__c > :timereportSingle.End__c
                          order by Start__c
                          limit 1];

          nextTime = nextTimeReport.Start__c;
          System.debug('#### lastEndTime try nextTime is: '+nextTime);
        }
        catch (Exception e)
        {
          // SCTimereport__c nextTimeReport = null;
          // nextTime = timereportSingle.End__c.addHours(1);
        }
        // is current report the day start?
        // depending on that set the report link of new time report item
        if (timereportSingle.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART)
        {
            isDayStart = true;
            reportLink = timereportSingle.Id;
        }
        else
        {
            isDayStart = false;
            reportLink = timereportSingle.ReportLink__c;
        }
        
        
        // controll the last mileage entry
        lastMileageEntry = SCboTimeReport.getMileageEntryBefore(timereportSingle);
        
        // new time report entry (add button)
        newEntryTimeReport = new SCTimereport__c(Start__c = timereportSingle.End__c,
                                                 End__c = nextTime,
                                                 //End__c = timereportSingle.End__c.addMinutes(maximumDuration),
                                                 ReportLink__c = reportLink,
                                                 Resource__c = timereportSingle.Resource__c,
                                                 CompensationType__c = timereportSingle.CompensationType__c,
                                                 //Description__c = timereportSingle.Description__c, <-- empty
                                                 Distance__c = 0,
                                                 //Status__c = timereportSingle.Status__c,
                                                 //Mileage__c = timereportSingle.Mileage__c);
                                                 Mileage__c = lastMileageEntry);

        // select time report type
        selectedTimeReportType = timereportSingle.Type__c;

        System.debug('#### SCTimereportItemEditController init() timereportSingle: '+
                      timereportSingle);

        System.debug('#### SCTimereportItemEditController init() newEntryTimeReport: '+
                      newEntryTimeReport);
    }

    /**
     * Calls the method for modification of the current
     * time report entry with mode.
     *
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference addTimeRepItemWithMode()
    {
        // time report object for further process
        SCTimereport__c currentTimeReportEntry = new SCTimereport__c ();

        System.debug('#### addTimeRepItem() mode: '+mode);

        if(mode == 'editMode')
        {
            currentTimeReportEntry = timereportSingle;
        }
        else if (mode == 'addMode')
        {
            currentTimeReportEntry = newEntryTimeReport;
        }

        // call the add function dependent on mode
        addTimeReportEntry (currentTimeReportEntry, mode);
        return null;
    }

    /**
     * Add or modify the current time report entry
     * dependent on mode.
     *
     * @param timeReport current time report entry
     * @param mode current mode: addMode or editMode
     * @author Dietrich Herdt <dherdt@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference addTimeReportEntry (SCTimereport__c timeReport, String mode)
    {
        System.debug('addTimeReportEntry timeReport ### '+timeReport);
        System.debug('addTimeReportEntry mode ### '+mode);

        try
        {
            // round the time report time dependent on custom settings
            timeReport.Start__c = SCutilDatetime.roundMinutes(timeReport.Start__c);
            timeReport.End__c = SCutilDatetime.roundMinutes(timeReport.End__c);

            // only for edit mode
            if (mode == 'editMode')
            {
                // if time report is day begin or day end, day end should be equals day start
                if (((timeReport.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                    (timeReport.End__c != timereportSingle.Start__c)) ||
                    ((timeReport.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND) &&
                    (timeReport.End__c != timereportSingle.Start__c)))
                {
                    timeReport.End__c = timeReport.Start__c;
                }
            }

            System.debug('####addTimeReportEntry selectedTimeReportType is: '+
                        selectedTimeReportType);
            // set the type of the current time report to selected time report type
            if ((selectedTimeReportType != null) &&
                (selectedTimeReportType.length() > 0))
            {
                timeReport.Type__c = selectedTimeReportType;
            }

            /*
            * Try to found time reports with starts earler as current entry with other
            * time report link.
            */
            String foundOverlappingTime =
                SCboTimeReport.overlapTimeReport(timeReport);
            String foundDayBeginLater =
                SCboTimeReport.beforeTimeReportBegin(timeReport);
            String foundNextDayStart =
                SCboTimeReport.giveTimeReportAfter(timeReport);
            // last mileage from another time report entry before
            lastMileageEntry = SCboTimeReport.getMileageEntryBefore(timeReport);
            // Duration of time report entry
            Integer timeReportDuration = SCutilDatetime.minutesBetween(timeReport.End__c,
                                                                       timeReport.Start__c);
                                                                       
            System.debug('### foundDayBeginLater  '+ foundDayBeginLater);
            System.debug('### foundOverlappingTime '+ foundOverlappingTime);
            System.debug('### foundNextDayStart '+foundNextDayStart);
            System.debug('### timeReport.End__c '+timeReport.End__c);
            System.debug('### lastEndTime '+lastEndTime);
            System.debug('### timeReport.Type__c '+timeReport.Type__c);
            System.debug('### START NEW '+ newEntryTimeReport.Start__c.format());
            System.debug('### END OLD '+ timereportSingle.End__c.format());
            System.debug('### timeReportDuration '+ timeReportDuration);
            
            // here comes the validations of the current time report entry
            // the entry have not valid type, error
            if ((timeReport.Type__c == null) ||
                (timeReport.Type__c == '') ||
                (timeReport.Type__c.length() == 0))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_NoValidType));
                return null;
            }
            // With the time entered the duration would be greater than
            // allowed duration of time report, error
            if (timeReport.End__c > lastEndTime)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_LastEndTime));
                return null;
            }
            // If the time report duration greater than
            // maximum duration of a time report entry in minutes, error
            if (timeReportDuration > maximumDuration)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_MaxDurationTime));
                return null;
            }           
            // if the end time less than the start time, error
            if (timeReport.End__c < timeReport.Start__c)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_EndLessStart));
                return null;
            }
            //  The time entered overlaps another time report, error
            if (foundNextDayStart != null)
            {
            /*###NA
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_OverlappingTimeBefore));
                                                        //+ ' ' + foundNextDayStart +
                                                        //'.'));
                return null;
             */
            }
            // the new entry is before time report starts, error
            if (foundDayBeginLater != null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_TRStartLater +
                                                        ' ' + foundDayBeginLater +
                                                        '.'));
                return null;
            }
            // the new mileage is negativ, error
            if (timeReport.Mileage__c < 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_MileageNegativ));
                return null;
            }
            // the new mileage is less than last mileage, error
            if ((timeReport.Mileage__c < lastMileageEntry)
                    && (timeReport.Type__c != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_MileageWrong));
                                                        //+' '+ lastMileageEntry));
                return null;
            }
             // the new distance is negativ, error
            if (timeReport.Distance__c < 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_DistanceNegativ));
                return null;
            }
            // For not day bedin entries the overlapping is allowed in the edit mode
            if (mode == 'editMode')
            {
                if ((timeReport.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                     foundOverlappingTime != null)
                {
                    // the new entry is inside of other time report, error
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                        System.Label.SC_msg_OverlappingTimeAfter));
                                                        // + ' ' +
                                                        // foundOverlappingTime+
                                                        // '.'));
                    return null;
                }
            }
            // The overlapping is not allowed in the add mode
            if (mode == 'addMode')
            {
                if (foundOverlappingTime != null)
                {
            /*###NA
                
                    // the new entry is inside of other time report, error
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                               System.Label.SC_msg_OverlappingTimeAfter));
                                                               //+ ' ' +
                                                               //foundOverlappingTime+
                                                               //'.'));
                    return null;
*/                    
                }
            }
            // all ok, no errors found, upsert new time report entry
            System.debug('### FILL GAPS is '+ fillGaps);
            if ((!timeReport.Start__c.format().equals(timereportSingle.End__c.format())) &&
                 fillGaps)
            {
                System.debug('#### START not equal END and fill Gaps is true');
                // new time report entry (add button)
                SCTimereport__c newEntryTimeOthers =
                        new SCTimereport__c(Start__c = timereportSingle.End__c,
                                            End__c = newEntryTimeReport.Start__c,
                                            ReportLink__c = timereportSingle.ReportLink__c,
                                            Resource__c = timereportSingle.Resource__c,
                                            Type__c = SCfwConstants.DOMVAL_TIMEREPORTTYPE_OTHERS,
                                            //Description__c = timereportSingle.Description__c,
                                            Distance__c = 0,
                                            Status__c = timereportSingle.Status__c,
                                            Mileage__c = timereportSingle.Mileage__c);

                System.debug('### newEntryTimeOthers with fill Gaps '+ newEntryTimeOthers);
                System.debug('### timeReport with fill Gaps '+ timeReport);

                insert newEntryTimeOthers;
                upsert timeReport;
                //allOk = true;
                //return null;
            }
            else
            {
                System.debug('### timeReport without Gaps '+ timeReport);
                upsert timeReport;
                //allOk = true;
                //return null;
            }

            // optimize and upsert mileage
            try
            {
                SCboTimeReport boTimeReport = new SCboTimeReport();
                if (timeReport.Type__c == SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART)
                {
                    System.debug('### MILEAGE upsert for timeReport.Id '+timeReport.Id);
                    boTimeReport.optimizeMileage(timeReport.Id);
                }
                else
                {
                    System.debug('### MILEAGE upsert for ReportLink '+timeReport.ReportLink__c);
                    boTimeReport.optimizeMileage(timeReport.ReportLink__c);
                }
            }
            catch (Exception e)
            {
                // Mileage upsert fail
                System.debug('MILEAGE upsert fail');
            }
            allOk = true;
            return null;
        }
        catch (DmlException e)
        {
            // FIXME !!!
            return null;
        }
        return null;
    }

    /**
     * Get available types for the current in add or edit dialog
     *
     * @return List of all time available report types as select options
     */
    public List<SelectOption> getTimeRepTypes()
    {
        List<SelectOption> types = new List<SelectOption>();
        List<SCfwDomainValue> allVal = new List <SCfwDomainValue>();

        // add a general option value to display if nothing is selected
        types.add(noneOption);

        SCfwDomain domain = new SCfwDomain('DOM_TIMEREPORTTYPE');
        try
        {
            allVal = domain.getDomainValues();
        }
        catch (Exception e)
        {
            // exception here
        }    
        System.debug('#### Values are ...' + allVal);

        String userLanguage = UserInfo.getLanguage();

        for (SCfwDomainValue currentValue: allVal)
        {
            //types.add(new SelectOption(brand.Name,brand.Name));

            if ((currentValue.getId2() != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYSTART) &&
                (currentValue.getId2() != SCfwConstants.DOMVAL_TIMEREPORTTYPE_DAYEND) &&
                (currentValue.getId2() != SCfwConstants.DOMVAL_TIMEREPORTTYPE_WORKTIME))
            {
                //System.debug('#### currentValue is ' +currentValue.domainValue);
                try
                {
                    System.debug('#### try currentValue.getId2() ...' + currentValue.getId2());
                    types.add(new SelectOption(currentValue.getId2(),
                              currentValue.getTranslation(userLanguage).Text__c));
                }
                catch (Exception e)
                {
                    // no translation for current value, add value to the list
                    types.add(new SelectOption(currentValue.getId2(),
                              currentValue.getId2()));
                }
                              
            }
        }

        // select time report type from selected time report entry
        if ((timereportSingle != null) && (timereportSingle.Type__c != null))
        {
            selectedTimeReportType = timereportSingle.Type__c;
        }
        return types;
    } // getTimeRepTypes()
}