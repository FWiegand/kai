/* 
 * @(#)SCOrderChangeRole.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.  
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use  
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
 */


/*
 * This class saves a new account in a order for a order role.
 */
public with sharing class SCOrderChangeRole
{
    public SCOrder__c order { get; set; }
    public List<SCOrderRole__c> roles { get; set; }
    public String oid { get; set; }
    
    //Values from the Accout search dialog
    public String value { get; set; }
    public String orderRole { get; set; }
    
    //Variables to validate whether we can modify the order roles
    public Boolean canModifyRoles { get; set; }
    private String ordStatus;
    private String ordType;
    private String ordInvoicingStatus;
    private SCfwOrderControllerBase contrBase;
    
    
   /*
    * Default constructor
    * Initializes the class variables
    */
    public SCOrderChangeRole(ApexPages.StandardSetController controller) 
    {
        oid = ApexPages.currentPage().getParameters().get('id');
        
        getOrderRoles(oid);
        getOrder(oid);
        
        ordStatus = this.order.Status__c;
        ordType = this.order.Type__c;
        ordInvoicingStatus = this.order.InvoicingStatus__c;
        validate();
        
        // we create an object of this controller, it is used in processCustomer
        contrBase = new SCfwOrderControllerBase();
    }

   /*
    * Checks if we can modify the order roles 
    * @param  ord.type__c the order type
    * @param  ord.status__c the order status
    * @param  ord.invoicingstatus__c the invoicing status
    *
    * @return the following variables are filled by this validation function:
    *         canModifyRoles
    */
    public void validate()
    {
        canModifyRoles = true;
        
        // 1. The order type should be ONE of the following
        // DOMVAL_ORDERTYPE_DEFAULT           = '5701';
        // DOMVAL_ORDERTYPE_INHOUSE           = '5702';
        // DOMVAL_ORDERTYPE_COMISSIONING      = '5705';
        // DOMVAL_ORDERTYPE_SPAREPARTSALES    = '5706';
        // DOMVAL_ORDERTYPE_MAINTENANCE       = '5707';
        // DOMVAL_ORDERTYPE_INVOICE_COPY      = '5709';
        // DOMVAL_ORDERTYPE_SERIAL_FAULT      = '5726';
        if (!SCfwConstants.DOMVAL_ORDERTYPE_FOR_CREDITNOTE.contains(ordType) ||
             ordType == SCfwConstants.DOMVAL_ORDERTYPE_INVOICE_COPY)
        { 
            canModifyRoles = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
                                                       System.Label.SC_msg_ChangeRoleInvalidType));
        }

        // 2. 
        if(ordStatus == SCfwConstants.DOMVAL_ORDERSTATUS_CLOSEDFINAL ||
            ordStatus == SCfwConstants.DOMVAL_ORDERSTATUS_ACCEPTED ||
            ordStatus == SCfwConstants.DOMVAL_ORDERSTATUS_COMPLETED ||
            ordStatus == SCfwConstants.DOMVAL_ORDERSTATUS_CANCELLED)
       {
            /*
            DOMVAL_ORDERSTATUS_OPEN                    enabled
            DOMVAL_ORDERSTATUS_PLANNED                 enabled
            DOMVAL_ORDERSTATUS_ACCEPTED                - 
            DOMVAL_ORDERSTATUS_COMPLETED               -
            DOMVAL_ORDERSTATUS_CANCELLED               -
            DOMVAL_ORDERSTATUS_CLOSEDFINAL             -
            DOMVAL_ORDERSTATUS_PLANNED_FOR_PRECHECK    enabled
            DOMVAL_ORDERSTATUS_RELEASED_FOR_PROCESSING enabled
            */
            canModifyRoles = false;
           
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
                                                       System.Label.SC_msg_ChangeRoleInvalidStatus));
        }

        // 3.
        if(ordInvoicingStatus == SCfwConstants.DOMVAL_INVOICING_STATUS_BOOKED ||
            ordInvoicingStatus == SCfwConstants.DOMVAL_INVOICING_STATUS_INVOICED ||
            ordInvoicingStatus == SCfwConstants.DOMVAL_INVOICING_STATUS_CANCELLED){
            /*
            DOMVAL_INVOICING_STATUS_OPEN                   enabled
            DOMVAL_INVOICING_STATUS_READYFORINVOICING      enabled
            DOMVAL_INVOICING_STATUS_INVOICED               -    
            DOMVAL_INVOICING_STATUS_BOOKED                 -
            DOMVAL_INVOICING_STATUS_CANCELLED              -
            */
            canModifyRoles = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
                                                       System.Label.SC_msg_ChangeRoleInvalidInvoicingStatus));
        }

    } // validate
        

   /*
    * This function saves the selected Account for the Role and updates the role list.
    * @param this.roles - List of the roles
    * @param value - selected Account
    *
    * @return the following variables are filled by this function:
    *         roles
    */
    public PageReference processCustomer()
    {
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(oid);
        // processCustomer of the controller base class does all the necessary things which should be done
        contrBase.processCustomer(boOrder, value, orderRole);
        if (boOrder.save())
        {
            getOrderRoles(oid);
        }
        return null;
    }

   /*
    * This function changes the account for the Role and updates the role list.
    * @param value - selected Account
    *
    * @return the following variables are filled by this function:
    *         roles
    */
    public PageReference changeRole()
    {
        SCboOrder boOrder = new SCboOrder();
        boOrder.readById(oid);
        // changeRole of the controller base class does all the necessary things which should be done
        contrBase.changeRole(boOrder, value);
        if (boOrder.save())
        {
            getOrderRoles(oid);
        }
        return null;
    }
    
   /*
    * Get order roles records 
    *
    * @param orderId the order Id
    *
    * @return the following variables are filled by this function:
    *         roles
    */
    private void getOrderRoles(String orderId)
    {
        // read all order roles that are not the service recipient
        // the service recipient can't be changed because the installed base is attaced 
        this.roles = [SELECT  Name, VipLevel__c,  Type__c,  TaxType__c,  TaxCode2__c,  TaxCode1__c,
            TaxCode0__c,  Street__c,  SpecialCustomer__c,  SearchCode__c,  Salutation__c,
            RiskClass__c,  PostalCode__c,  PoBox__c,  PoBoxPostCode__c,  PoBoxCity__c,
            Phone__c,  Phone2__c,  PersonTitle__c, Order__r.Status__c, Order__r.Type__c, Order__r.InvoicingStatus__c,
            OrderRole__c,  Name3__c, Name2__c,  Name1__c,  MobilePhone__c,  LockType__c,  LocaleSidKey__c,  ID2__c,
            HouseNo__c,  HomePhone__c,  GeoY__c,  GeoX__c,  GeoApprox__c,  Floor__c,  FlatNo__c,
            Fax__c,  Extension__c,  Email__c,  District__c,  DistanceZone__c,  DiscountGroup__c,
            Description__c,  County__c,  Country__c,  CountryState__c,  Contact__c,  City__c,
            Address__c,  Account__c,  AccountType__c,  AccountNumber__c, Order__c 
            FROM SCOrderRole__c where Order__c = :orderId order by OrderRole__c];
    }
    
   /*
    * Get order records 
    *
    * @param orderId the order Id
    *
    * @return the following variables are filled by this function:
    *         order
    */
    private void getOrder(String orderId)
    {
        this.order = [SELECT Status__c, Name, Type__c, InvoicingType__c, InvoicingStatus__c, MaterialStatus__c FROM SCOrder__c where id =:orderId];
    }
    
   /*
    * Button [Continue] returns to the original page 
    */
    public PageReference onContinue()
    {
        Pagereference pageRef = new PageReference('/' + oid);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /**
     * Get Account Ids.
     *
     * @return    comma separated Account id string
     */
    public String getRoleAccountIds()
    {
        String roleAccountIds = null;
        for( SCOrderRole__c role : this.roles )
        {
            roleAccountIds = addIdString( roleAccountIds, role.Account__c );
        }

        System.debug('###roleAccountIds: ' + roleAccountIds);
        return roleAccountIds;
    }
    
    private String addIdString( String str, String id )
    {
        if( str != null )
        {
            str += ',' + id;
        }
        else
        {
            str = id;
        }
        return str;
    }

}