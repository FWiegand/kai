/*
 * @(#)SCfwPageControllerBase.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * see http://wiki.developerforce.com/index.php/Controller_Component_Communication
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing virtual class SCfwPageControllerBase 
{
    // reference to the component controler controller (if only one component is used)
    private SCfwComponentControllerBase componentController;
    // map if multiple controllers are used
    private Map<String, SCfwComponentControllerBase>componentControllerMap;
    
    
    public virtual SCfwComponentControllerBase getComponentController() 
    {
        return componentController;
    }
    
    // new getter for the hashmap
    public virtual Map<String, SCfwComponentControllerBase> getComponentControllerMap()
    {
        return componentControllerMap;
    }
    
    //new method for putting value in the hashmap
    public virtual void setComponentControllerMap(String key, SCfwComponentControllerBase compController)
    {
        if (componentControllerMap == null)
        {
            componentControllerMap = new Map<String, SCfwComponentControllerBase>();
        }
            componentControllerMap.put(key,compController); 
    }
    
    public virtual void setComponentController(SCfwComponentControllerBase compController) 
    {
        componentController = compController;
    }
    
    public SCfwPageControllerBase getThis() 
    {
        return this;
    }
    
    
   /*
    * Called by the component to pass date to the page. 
    * Override this method in derived classed to implement component <-> page communication
    * @param ctx  context information optional context information
    * @param data  a data object 
    */    
    public virtual String OnUpdateData(String ctx, Object data)
    {
        // called by the component 
        return null;
    }
    
}