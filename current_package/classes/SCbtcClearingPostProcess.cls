/*
 * @(#)SCbtcClearingPostProcess.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Used to initiate the following post clearing processing steps:
 * - create the PDF document (service report) for the assignment
 * - send the PDF document to the caller's e-mail address (if entered)
 * - initiate the material reservation Process (grouped by the assignment) to SAP
 * - initiate the closure of the order (if all assignments have the status completed)
 *
 * This batch job has to be started externally by a job scheduler (e.g. every x minutes)
 *
 * Examples:
 * 1) process all pending order assignments
 * SCbtcClearingPostProcess.asyncProcessAll(0, 'no trace');
 *
 * 2) test execution
 * SCbtcClearingPostProcess.asyncProcess('a0sM0000005kkN5', 'trace');
 *
 * Technical note:
 * The job implements multiple passes to avoid exceptions caused by SF govenor limits
 * PHASE 1: creates the PDF and sends the e-mail
 * PHASE 2: exports the PDF document using the CC archive interface
 *
 */

global with sharing class SCbtcClearingPostProcess extends SCbtcBase 
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts  
{
    // Activates the e-mail notification (see CCSettings) to forward the service report 
    private Boolean emailNotificationEnable;
    private String  emailNotificationTemplate;
    
    // Required to create the pdf document in the batch job
    private String sessionid;

    // we process one pdf export per execute (to avoid governor limits)
    private static final Integer batchSize = 1;
    private static final Integer batchClearingDelta = 30;  // the data must be at least this number of seconds old (settling time)

    private ID batchprocessid = null;
    public ID getBatchProcessId()
    {
        return batchprocessid;
    }
    String mode = 'productive';
    Integer max_cycles = 0;
    public String extraCondition = null;

    // only if one assignment has to be used directly in the batch job
    public Id assignmentid = null;
    // Query for batch job 
    private String query; 

    // error text length limit
    private Integer clearingPostProcessInfoLength = 32760;


    //---<Helper functions to start the batch job>---------------------------------------

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcClearingPostProcess btc = new SCbtcClearingPostProcess(max_cycles, mode);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job. Use this method in the external job scheduler  
    * You can specify an optional additional condition that is added to the statement
    * @param see below
    */
    public static ID asyncProcessAll(Integer max_cycles, String mode, String extraCondition)
    {
        System.debug('###mode: ' + mode);
        SCbtcClearingPostProcess btc = new SCbtcClearingPostProcess(max_cycles, mode, extraCondition);

        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  

   /*
    * Starts the asynchronosu bach job to process exactly the specified assigment
    * @param see below
    */
    public static ID asyncProcess(ID assignmentid, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcClearingPostProcess btc = new SCbtcClearingPostProcess(assignmentid, 0, mode);
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    }  


   /*
    * Starts the synchronosu bach job to process exactly the specified assigment
    * @param see below
    */

    public static List<SCAssignment__c> syncProcess(ID assignmentid, String mode)
    {
        System.debug('###mode: ' + mode);
        SCbtcClearingPostProcess btc = new SCbtcClearingPostProcess(assignmentid, 0, mode);
        btc.startCore(false, false); // prepares the query 
        
        List<SObject> mList = Database.query(btc.query); // runs the query        
        List<SCAssignment__c> retValue = btc.executeCore(mList);
        return retValue;
    }  

    //---<constructurs>------------------------------------------------------------------

    /**
     * Constructor
     * @param see below
     */
    public SCbtcClearingPostProcess(Integer max_cycles, String mode)
    {
        this(null, max_cycles, mode, null);
    }

    /**
     * Constructor
     * @param see below
     */
    public SCbtcClearingPostProcess(Id assignmentid, Integer max_cycles, String mode)
    {
        this(assignmentid, max_cycles, mode, null);
    }


    /**
     * Constructor
     * @param see below
     */
    public SCbtcClearingPostProcess(Integer max_cycles, String mode, String extraCondition)
    {
        this(null, max_cycles, mode, extraCondition);
    }


    /**
     * Constructor
     * @param assignmentid   the id of the assignment to process
     * @param max_cycles    maximum number of execution processes (0: not limited, 1: process only 1, ....)
     * @mode               'test' or 'trace'    trace additional information during test execution
     *                     'productive'         no additional tracing
     * @extraCondition     additional conditions (see soql statement below)
     */
    public SCbtcClearingPostProcess(Id assignmentid, Integer max_cycles, String mode, String extraCondition)
    {
        this.assignmentid = assignmentid;
        this.max_cycles = max_cycles;
        this.mode = mode;
        this.extraCondition = extraCondition;
        
        // required to be able to call the pdf creation web service in a valid user context 
        this.sessionid = UserInfo.getSessionId();
            
        // evaluate the CCEAG settings of the current (API) user for this process and store it in the batch job for later access
        CCSettings__c ccSettings = CCSettings__c.getInstance();
        
        // check if the automati e-mail notification is enabled        
        this.emailNotificationEnable = ccSettings.IFEnableEmailNotification__c == 1;
        
        //TODO: emailNotificationTemplate = '';
        
        clearingPostProcessInfoLength = SCAssignment__c.ClearingPostProcessInfo__c.getDescribe().getLength();
    }


    //---<batch preparation and processing>------------------------------------------------------------------

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * @param BC the batch context
    * @return the query locator with the selected mainenances
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        Boolean aborted = abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcClearingPostProcess', 'start');
        return startCore(true, aborted);
    } // start

   /*
    * Prepares the SOQL statement that is used to select the records that have to be processed
    * @param returnsRetValue   true: executes the query and returns the query locator. false: null only the soql statement is created
    * @param aborted
    * @return the query locator that represents the records to be processed
    */
    public Database.QueryLocator startCore(Boolean returnsRetValue, Boolean aborted)
    {
        
        // To check the syntax of SOQL statement 
        //List<SCAssignment__c> items = [select id, order__r.roleCA__r.email__c, order__r.roleCA__r.Account__c  from SCAssignment__c ];
        debug('start: after checking statement');

        query = 'select id, name, ClearingPostProcessStatus__c, order__r.id, order__r.roleCA__r.email__c, order__r.roleCA__r.Account__c, ' +
                'order__r.ERPStatusOrderCreate__c, order__r.ERPStatusEquipmentUpdate__c, order__r.ERPStatusMaterialReservation__c,  ' +
                'order__r.ERPStatusMaterialMovement__c, order__r.ERPStatusOrderClose__c, order__r.ERPStatusArchiveDocumentInsert__c, ' +
                'order__r.ERPOrderNo__c, order__r.roleCA__r.Account__r.Name, order__r.roleCA__r.Name1__c, order__r.roleCA__r.Name2__c, order__r.roleCA__r.Name3__c ' +
                ' from SCAssignment__c ' +
                'where ClearingPostProcessStatus__c in (\'pending\', \'archive\',\'materialReservation\' ) and ClearingStartPostProcessing__c = \'1\' '; 

        if(assignmentid != null)
        {
            query += ' and ID = \'' + assignmentid + '\'';
        }

        if(extraCondition != null)
        {
            query += ' and (' + extraCondition + ') ';
        }                   
        
        // prevent the parallel execution of the batch job to avoid exporting items multiple times
        Boolean isRunning = isJobRunningForCallerCountry('SCbtcClearingPostProcess') == '1';
        
        // do not read any data if aborted or already running 
        if(aborted || isRunning)
        {
            query += ' limit 0';
        }
        else
        {
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }               

        debug('query: ' + query);
        
        if(returnsRetValue)
        {
            return Database.getQueryLocator(query);
        }
        return null;
    }

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        if(abortOneOfTwoSameJobsRunning(BC.getJobId(), 'SCbtcClearingPostProcess', 'execute'))
        {
            return;
        }
        executeCore(scope);
    } // execute

   /*
    * Called to execute each batch
    */
    public List<SCAssignment__c> executeCore(List<sObject> scope)
    {
        
        List<SCAssignment__c> items = new List<SCAssignment__c>();
        for(sObject rec: scope)
        {
            String errorText = '';
            SCAssignment__c ass = (SCAssignment__c)rec;
            items.add(ass);
            
            // PASS1: first created, export and email the PDF dokument)
            if(ass.ClearingPostProcessStatus__c == 'pending')
            {
                
                try
                {
                    //--<1. create the PDF service report for the assignment>--------------------------------------------------
                    // We use the web service trick to get a proper result (synchronous pdf creation in batch apex does not work!)
                    // The result is the attachment id that was created and attached to the order
                    String attachmentid = SCPrintOrderController.createPDFAttachmentByWebServiceByAssignment(sessionid, ass.order__r.id, ass.id, ass.name);
                    
                    //--<2. send the PDF service report to the caller if required>---------------------------------------------
                    // now notify the caller if enabled
                    System.debug('###+++###start sending Email');
                    
                    if(emailNotificationEnable && attachmentid != null && ass.order__r.roleCA__r.email__c != null)    
                    {
                        // ###TODO: temporary solution - use an e-mail template (see below)
                        {
                            SCPrintOrderController.sendDoc(attachmentid, ass.order__r.roleCA__r.email__c, 'Leistungsnachweis zu Einsatz ' + ass.name, 
                              'Sehr geehrter Kunde<BR/><BR/>Anbei erhalten Sie den Leistungsnachweis.<BR/><BR/>Mit freundlichen Grüßen<BR/><BR/>Ihr Coca-Cola Cold Drink Service Team');
                        }
                    
                        /*
                        //### tmp test only - to be finalised
                        ID GMSSS = '005D0000001X2lDIAS';
                        if (UserInfo.getUserId() == GMSSS)  //USER == GMSSS
                        {
                            try
                            {
                                //Find Contact
                                Contact con;
                                
                                List<Contact> listContact = [SELECT Id, AccountId, Email, LastName FROM Contact WHERE Email = :ass.order__r.roleCA__r.email__c];
                                
                                if (listContact != null && listContact.size() == 1) //There is one Contact
                                {
                                    con = listContact.get(0);
                                }//There is one Contact
                                else if (listContact != null && listContact.size() == 1) //There is more than one Contact
                                {
                                    Set<String> nameSet = new Set<String>{ass.order__r.roleCA__r.Name1__c, ass.order__r.roleCA__r.Name2__c, ass.order__r.roleCA__r.Name3__c};
                                    
                                    if (ass.order__r.roleCA__r.Account__c != null)
                                    {
                                        nameSet.add(ass.order__r.roleCA__r.Account__r.Name);
                                    }
                                    
                                    for (Contact row : listContact)
                                    {
                                        if (ass.order__r.roleCA__r.Account__c != null && row.AccountId == ass.order__r.roleCA__r.Account__c)
                                        {
                                            con = row;
                                            break;
                                        }
                                        else if (nameSet.contains(row.LastName))
                                        {
                                            con = row;
                                        }
                                    }
                                    
                                    if (con == null)
                                    {
                                        con = listContact.get(0);
                                    }
                                } //There is more than one Contact
                                else //There is no Contact
                                {
                                    String lastname = '';
                                    
                                    if (ass.order__r.roleCA__r.Name1__c != null && ass.order__r.roleCA__r.Name1__c != '')
                                    {
                                        lastname = ass.order__r.roleCA__r.Name1__c;
                                    }
                                    else if (ass.order__r.roleCA__r.Account__c != null && ass.order__r.roleCA__r.Account__r.Name != null && ass.order__r.roleCA__r.Account__r.Name != '')
                                    {
                                        lastname = ass.order__r.roleCA__r.Account__r.Name;
                                    }
                                    else
                                    {
                                        lastname = ass.order__r.roleCA__r.email__c;
                                    }
                                    
                                    con = new Contact(AccountId = ass.order__r.roleCA__r.Account__c,
                                                      Email     = ass.order__r.roleCA__r.email__c,
                                                      LastName  = lastname);
                                    
                                    Database.insert(con);
                                }//There is no Contact
                                
                                //SendEmail
                                SCPrintOrderController.sendEmailServiceReport(con.Id, attachmentid, ass.Id);
                            }
                            catch (Exception e)    //Fallback
                            {
                                SCPrintOrderController.sendDoc(attachmentid, ass.order__r.roleCA__r.email__c, 'Leistungsnachweis zu Einsatz ' + ass.name, 
                                  'Sehr geehrter Kunde<BR/><BR/>Anbei erhalten Sie den Leistungsnachweis.<BR/><BR/>Mit freundlichen Grüßen<BR/><BR/>Ihr Coca-Cola Cold Drink Service Team');
                            }
                        } //USER == GMSSS
                        else
                        */
                    }
                    // PASS 1: successfully completed, now initiate the next pass to export the pdf into the archive system
                    ass.ClearingPostProcessStatus__c = 'archive';
                }
                catch (Exception e)
                {
                    ass.ClearingPostProcessStatus__c = 'error';
                    errorText = '' + e;
                }
            }
            // PASS2: archive the document (we can't write data and call a web service and neitehr use a future call in the batch job)
            else if(ass.ClearingPostProcessStatus__c == 'archive')
            {
                try
                {
                    //--<4. activate the archive interface>--------------------------------------------------------------------
                    // select the attachment based on the order and the name servicereport-OA-0000087628
                    String oid = ass.order__r.id;
                    String attname = 'servicereport-' + ass.name;

                    // we can start the archive process if the order exists and no other errors are pending
                    if(canArchive(ass))
                    {
                        List<Attachment> attachments = [select id from Attachment where ParentId = :oid and name =:attname order by createddate desc limit 1];
                        if(attachments != null && attachments.size() > 0)
                        {
                            String attachmentid = attachments[0].id;
                            CCWCArchiveDocumentInsert.callout(attachmentid, false, false);
                        }
                        // PASS 2: successfully completed
                        ass.ClearingPostProcessStatus__c = 'materialReservation';
                    }
                    else
                    {
                        // mark the item as failed
                        ass.ClearingPostProcessStatus__c = 'error';
                        errorText = 'Archiving skipped for [' + attname + '] as there are ERP errors pending';   
                    }
                }
                catch (Exception e)
                {
                    ass.ClearingPostProcessStatus__c = 'error';
                    errorText = '' + e;
                }
            }
            else if(ass.ClearingPostProcessStatus__c == 'materialReservation')
            {
				// GMSGB 10.09.13  activate as soon as Version 1.0.60 or higher is active on the mobile system              
                try
                {
                    //--<4. create pending reservations>-----------------------------------------------------------------------
                    
                    // read all materal reservations for this assignment that have not yet been transferred to SAP 
                    // material requisitions - future callout to SAP
                    
                    // the processing is synchronous as we have pending work to be saved after exitting this method
                    CCWCMaterialReservationCreate.calloutbyMobileAssignment(ass.id, false, false);
                    
                    ass.ClearingPostProcessStatus__c = 'ok';
                }
                catch(Exception e)
                {
                    ass.ClearingPostProcessStatus__c = 'error';
                    errorText = (errorText != '' ? '; ' : '') + '' + e;
                }
//*/            
            } // else if .... pass2 archive               

            // Error handling
            if (errorText != null && errorText != '')
            {
                ass.ClearingPostProcessStatus__c = 'error';
                
                errorText = (errorText.length() > clearingPostProcessInfoLength) ? errorText.substring(0,ClearingPostProcessInfoLength) : errorText; // 32768
                ass.ClearingPostProcessInfo__c = errorText;
            }
        } // for
        
        // now update the status 
        update items;
        
        return items;
    }
    


   /*
    * Called by the framework when the batch job has been completed. 
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish


   /*
    * Checks if there are any ERP errors that would prevent us from archiving the document
    * @return true of the archiving is possible, else false
    */
    private Boolean canArchive(SCAssignment__c item)
    {
        // an sap reference number must exist and no other interface must have an error status
        return (item != null && item.order__r != null &&
                item.order__r.ERPOrderNo__c != null && 
                item.order__r.ERPStatusOrderCreate__c != 'error' && 
                item.order__r.ERPStatusEquipmentUpdate__c != 'error' && 
                item.order__r.ERPStatusMaterialReservation__c  != 'error' && 
                item.order__r.ERPStatusMaterialMovement__c != 'error' && 
                item.order__r.ERPStatusOrderClose__c != 'error' && 
                item.order__r.ERPStatusArchiveDocumentInsert__c != 'error'
                );
    }


    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test') || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }
}