/*
 * @(#)SCutilMessage.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !! temporary helper to send messages - will be replaced by a more powerful implementation
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
public class SCutilMessage
{
    static SCApplicationSettings__c appset = SCApplicationSettings__c.getInstance();

    SCMessage__c msg;

   /*
    * Contructur
    */
    public SCutilMessage()
    {
    }

   /*
    * Checks if the message creation is active. Reserved to be able to disable 
    * the creation on the production environment if required.
    *
    *    ORDCRE    message on order creation 
    *    ORDCAN    message on order cancellation
    *    ORDCOMP   message on order completed
    *    APPCRETODAY  message scheduled on today
    *    APPCANTODAY  message scheduled on today is cancelled
    */
    public static Boolean isActive(String ctx)
    {
        return appset.DISABLEMESSAGES__c == null || !appset.DISABLEMESSAGES__c.contains(ctx);
    }

   /*
    * Writes the message to the database
    * @return the message id or null 
    */
    public String send()
    {
        if(msg != null)
        {
           insert msg;
           return msg.id;
        }
        return null;
    }

   /*
    * Inserts a message for the event Order.Created 
    * @param data the order 
    */
    public void onOrderCreate(SCOrder__c data)
    {
        if(isActive('ORDCRE'))
        {
            msg = new SCMessage__c();
            msg.Status__c  = 'Open';
            msg.Type__c    = 'WORKFLOW';    
            msg.Format__c  = 'WF01';
            msg.Event__c   = 'Order.Created';
            msg.Subject__c = '';
            msg.Text__c    = '';
            msg.order__c   = data.id;
            msg.country__c = appset.DEFAULT_COUNTRY__c;
        }
    }

   /*
    * Inserts a message for the event Order.Cancelled
    * @param data the order 
    */
    public void onOrderCancel(SCOrder__c data)
    {
        if(isActive('ORDCAN'))
        {
            msg = new SCMessage__c();
            msg.Status__c  = 'Open';
            msg.Type__c    = 'WORKFLOW';    
            msg.Format__c  = 'WF01';
            msg.Event__c   = 'Order.Cancelled';            
            msg.Subject__c = '';
            msg.Text__c    = '';
            msg.order__c   = data.id;
            msg.country__c = appset.DEFAULT_COUNTRY__c;
        }
    }
    
   /*
    * Inserts a message for the event Order.Completed
    * @param data the order 
    */
    public void onOrderCompleted(SCOrder__c data)
    {
        if(isActive('ORDCOMP'))
        {
            msg = new SCMessage__c();
            msg.Status__c  = 'Open';
            msg.Type__c    = 'WORKFLOW';    
            msg.Format__c  = 'WF01';
            msg.Event__c   = 'Order.Completed';            
            msg.Subject__c = '';
            msg.Text__c    = '';
            msg.order__c   = data.id;
            msg.country__c = appset.DEFAULT_COUNTRY__c;
        }
    }
    
   /*
    * Checks if the appointment is on today or overlaps with today
    * @return true if the appointment is on the current day or overlapps with the current day. else false 
    */
    public static boolean isOnToday(SCAppointment__c app)
    {
        if(app.Start__c != null && app.End__c != null)
        {
            Date appstart = app.Start__c.dateGMT();
            Date append   = app.End__c.dateGMT();            
            Date todaydat = DateTime.NOW().dateGMT();
            return appstart <= todaydat && append >= todaydat;
        }
        return false; 
    }
    
   /*
    * Inserts a message for the event Message.Created 
    * @param data the order 
    */
    public void onMessageSend(SCOrder__c order, String msgText, SCResource__c resource)
    {
        //if(isActive('ORDCRE'))
        //{
            msg = new SCMessage__c();
            msg.Status__c   = 'Open';
            msg.Type__c     = 'WORKFLOW';    
            msg.Format__c   = 'WF07';
            msg.Event__c    = 'Message.Sent';
            msg.Subject__c  = 'Order: ' + order.name + ' (' + order.id + ')';
            msg.Text__c     = msgText;
            msg.order__c    = order.id;
            msg.Resource__c = resource.id;
            msg.country__c  = appset.DEFAULT_COUNTRY__c;
        //}
    }


   /*
    * Inserts a message for the event Appointment.Inserted
    * @param items list of appointments to process
    */
    public static void onAppointmentInsert(List<SCAppointment__c> items)
    {
        if(isActive('APPCRETODAY'))
        {
            try
            {
                List<SCMessage__c> messagelist = new List<SCMessage__c>(); 
                for(SCAppointment__c app: items)
                {
                    //if(isOnToday(app) || !appset.MESSAGEONTODAY__c)
                    //{
                        // automatically optimized appointments are excluded
                        if(app.PlanningType__c != null
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED)
                        {
                            SCMessage__c msg = new SCMessage__c();
                            msg.Status__c  = 'Open';
                            msg.Type__c    = 'WORKFLOW';    
                            msg.Format__c  = 'WF02';
                            msg.Event__c   = 'Appointment.Inserted';            
                            msg.Subject__c = '';
                            msg.Text__c    = '';
                            msg.order__c                 = app.order__c;
                            msg.resource__c              = app.resource__c;
                            msg.appointment__c           = app.id;
         //                   msg.appointmentStart__c      = app.Start__c;
         //                   msg.appointmentEnd__c        = app.End__c;

//                            msg.appointmentOld__c        = app.id;
//                            msg.appointmentOldStart__c   = app.Start__c;
//                            msg.appointmentOldEnd__c   = app.Start__c;

                            msg.country__c = appset.DEFAULT_COUNTRY__c;
                            
                            
                            
        
                            messagelist.add(msg);
                        }
                    //}        
                } 
                insert messagelist;                
            }
            catch(Exception e)
            {
                System.debug('exception: SCutilMessage.onAppointmentInsert' + e);
            }
        }
    }

   /*
    * 
    * @param items list of appointments to process
    */
    public static void onAppointmentCancelled(List<SCAppointment__c> items)
    {
        if(isActive('APPDELTODAY'))
        {
            try
            {
                List<SCMessage__c> messagelist = new List<SCMessage__c>(); 
                for(SCAppointment__c app: items)
                {
                    //if(isOnToday(app))
                    //{
                        // automatically optimized appointments are excluded
                        if(app.PlanningType__c != null
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED)
                        {

                        
                            SCMessage__c msg = new SCMessage__c();
                            msg.Status__c  = 'Open';
                            msg.Type__c    = 'WORKFLOW';    
                            msg.Format__c  = 'WF02';
                            msg.Event__c   = 'Appointment.Cancelled';            
                            msg.Subject__c = '';
                            msg.Text__c    = '';
                            msg.order__c         = app.order__c;
                            msg.appointment__c   = app.id;
                            msg.resource__c      = app.resource__c;
                            msg.country__c = app.order__r.country__c;
        
                            messagelist.add(msg);
                        }
                    //}        
                } 

                insert messagelist;                
            }
            catch(Exception e)
            {
                System.debug('exception: SCutilMessage.onAppointmentCancelled' + e);
            }
        }
    }

   /*
    * Inserts a message for the event Appointment.Deleted
    * @param items list of appointments to process
    */
    public static void onAppointmentDelete(List<SCAppointment__c> items)
    {
        if(isActive('APPDELTODAY'))
        {
            try
            {
                List<SCMessage__c> messagelist = new List<SCMessage__c>(); 
                for(SCAppointment__c app: items)
                {
                    //if(isOnToday(app))
                    //{
                        // automatically optimized appointments are excluded
                        if(app.PlanningType__c != null
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED)
                        {
                            SCMessage__c msg = new SCMessage__c();
                            msg.Status__c  = 'Open';
                            msg.Type__c    = 'WORKFLOW';    
                            msg.Format__c  = 'WF02';
                            msg.Event__c   = 'Appointment.Deleted';            
                            msg.Subject__c = 'app:' + app.id + ' ord:' + app.order__c;
                            msg.Text__c    = '';
                            msg.order__c         = app.order__c;
                            // msg.appointment__c   = app.id;    // can't reference a deleted appointment
                            msg.resource__c      = app.resource__c;
                            msg.country__c = appset.DEFAULT_COUNTRY__c;
        
                            messagelist.add(msg);
                        }
                    //}        
                } 
                insert messagelist;                
            }
            catch(Exception e)
            {
                System.debug('exception: SCutilMessage.onAppointmentDelete' + e);
            }
        }
    }
    
   /*
    * Inserts a message for the event Appointment.Updated | gmssu 10.11.2011
    * @param items list of appointments to process
    */
    public static void onAppointmentUpdate(List<SCAppointment__c> items)
    {
        if(isActive('APPUPDTODAY'))
        {
            try
            {
                List<SCMessage__c> messagelist = new List<SCMessage__c>(); 
                for(SCAppointment__c app: items)
                {
                    //if(isOnToday(app))
                    //{
                        // automatically optimized appointments are excluded
                        if(app.PlanningType__c != null
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_OPTIMIZED
                         && app.PlanningType__c != SCfwConstants.DOMVAL_APPOINTMENTPLANNINGTYPE_GENERATED)
                        {
                            SCMessage__c msg = new SCMessage__c();
                            msg.Status__c  = 'Open';
                            msg.Type__c    = 'WORKFLOW';    
                            msg.Format__c  = 'WF02';
                            msg.Event__c   = 'Appointment.Updated';            
                            msg.Subject__c = 'app:' + app.id + ' ord:' + app.order__c;
                            msg.Text__c    = '';
                            msg.order__c         = app.order__c;
                            msg.appointment__c   = app.id; 
                            msg.resource__c      = app.resource__c;
                            msg.country__c       = appset.DEFAULT_COUNTRY__c;
        
                            messagelist.add(msg);
                        }
                    //}        
                } 
                insert messagelist;                
            }
            catch(Exception e)
            {
                System.debug('exception: SCutilMessage.onAppointmentUpdate' + e);
            }
        }
    }

}

/*    
Subject__c
Text__c

To__c
From__c

Country__c

Type__c
- SMS
- EMAIL
- TODO
- WORKFLOW

Format__c
- TEXT
- HTML
- PDF
- LETTER
- WF01
- WF02
- WF03
- WF04
- WF05

Status__c
Open
Processed
Error

Order__c


Object          Event       Condition   Message         Recipient   Subject     Text
-----------------------------------------------------------------------------------------------------------
SCOrder         Created     always      Workflow.WF01   - none -    Order number    empty
SCAppointment   Created     today       SMS             Engineer mobile Order number    Order details
SCAppointment   Update      fixed       SMS             Engineer mobile Order number    Order details
SCContractVisit Scheduled   always      Letter      


*/