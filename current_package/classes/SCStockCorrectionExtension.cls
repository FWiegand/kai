/*
 * @(#)SCStockCorrectionExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCStockCorrectionExtension extends SCMatMoveBaseExtension
{
    private SCStock__c stock;
    
    public SCStockItem__c stockItem { get; set; }
    public Integer curQty { get; set; }
    public Integer qty { get; set; }
    public String matMoveType { get; set;}
    
    public Boolean articleValType { get; private set;}
    public Boolean bookingOk { get; private set; }

    public List<SelectOption> getTypeList()
    {
        return domMatMove.getDomainValuesByParameter(15, 16);
    }

    
    /**
     * Initialize the page objects for selecting an article and 
     * a material movement type.
     * Sets the quamtity for the actual stock to 0.
     *
     * @param    controller    the standard controller
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCStockCorrectionExtension(ApexPages.StandardController controller)
    {
        this.stock = (SCStock__c)controller.getRecord();
        
        try
        {
            this.stock = [SELECT Id, Info__c, Name, ValuationType__c FROM SCStock__c WHERE Id = :this.stock.Id LIMIT 1];
        }
        catch (QueryException e)
        {
            System.debug('#### SCStockCorrectionExtension(): Stock not found!!');
        }
        
        
        stockItem = new SCStockItem__c();
        
        articleValType = false;
        bookingOk = false;
        
        curQty = 0;
    } // SCStockCorrectionExtension

    /**
     * Methode is called, when the user clicks [OK].
     * After the validion of the quantity the entry in SCMaterialMovement 
     * is created. If this is successful, entries in SCMateralMovement for
     * the current stock will be processed.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference correctStock()
    {
        System.debug('#### correctStock(): Stock         -> ' + stock.Name);
        System.debug('#### correctStock(): Article       -> ' + stockItem.Article__c);
        System.debug('#### correctStock(): ValuationType -> ' + stockItem.ValuationType__c);
        System.debug('#### correctStock(): Quantity      -> ' + qty);
        
        // validate the quantity
        SCfwDomainValue domVal = domMatMove.getDomainValue(matMoveType, true);
        String param = domVal.getControlParameter('V4');
        if (param.equals('1') && (qty > curQty))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.SC_msg_QtyLessActualQty));
            return null;
        } // if (param.equals('1') && (qty > curQty))
        
        try
        {
            if (!stock.ValuationType__c) 
            { 
                stockItem.ValuationType__c = null; 
            }
        }
        catch (Exception e) { }
        
        try
        {
            articleValType = [SELECT ValuationType__c FROM SCArticle__c WHERE Id = :stockItem.Article__c LIMIT 1].ValuationType__c;
        }
        catch (QueryException e)
        {
            System.debug('#### correctStock(): Article not found!!');
            articleValType = false;
        }
        
        if (!articleValType) 
        { 
            stockItem.ValuationType__c = null; 
        }
        
        // create material movements and process this
        if (createMatMove(stock, stockItem.Article__c, stockItem.ValuationType__c, qty, matMoveType))
        {
            if (bookMaterial(stock))
            {
                bookingOk = true;
            } 
        } 
        
        return null;
    } // correctStock

    /**
     * After the selection of an article this methode is called.
     * It determines the actual quantity for the article in the current stock.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference initQuantity()
    {
        System.debug('#### initQuantity(): Article       -> ' + stockItem.Article__c);
        System.debug('#### initQuantity(): ValuationType -> ' + stockItem.ValuationType__c);
        
        try
        {
            articleValType = [SELECT ValuationType__c FROM SCArticle__c WHERE Id = :stockItem.Article__c LIMIT 1].ValuationType__c;
        }
        catch (QueryException e)
        {
            System.debug('#### initQuantity(): Article not found!!');
            articleValType = false;
        }
        
        if (!articleValType)
        {
            stockItem.ValuationType__c = null;
        }
        
        try
        {
            SCStockItem__c stockItem = [Select Name, Qty__c from SCStockItem__c where Stock__c = :stock.Id and Article__c = :stockItem.Article__c and ValuationType__c = :stockItem.ValuationType__c limit 1];
            if (null != stockItem.Qty__c)
            {
                curQty = stockItem.Qty__c.intValue();
            } 
            else
            {
                curQty = 0;
            }
        }
        catch (QueryException e)
        {
            System.debug('#### initQuantity(): StockItem not found!!');
            curQty = 0;
        }
        
        return null;
    } // initQuantity
} // SCStockCorrectionExtension