/*
 * @(#)SCbtcStockReplenishment.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * The class replenish stocks. Jobs can be call for plant determined by Id or its name,
 * or for the list of stock ids, a stock id or stock name.
 * 
 * AppSettings.MATERIAL_REORDER_LOCKTYPE ought to be set to list the lock types domain values
 * that are to be reorderd, e.g. 50201, 50204, 50206
 * 
 * 
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 *
 * Counting of SOQL operation in a replenishment of one stock
 * InterfaceLog                     1
 * Read stock items                 1
 * Read set of replaced articles    1
 * Creating of the new stock item   3
 * Creating material momvements     2
 * ------------------------------------
 *                                  8
 * 100 % 8 = 12 the size of one batch
 * Because the govornor limit for Total number of records retrieved by SOQL queries is 50000
 * and this was exceeded by prod in GB we take the batch size = 1.
 */
global class SCbtcStockReplenishment extends SCbtcBase
       implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private static Integer batchSize = 1;
    private ID batchprocessid = null;
    private String mode = 'productive';

    Integer max_cycles = 0;    
    List<String> stockIdList = null;    // stockIdList = null, normal batch call
                                        // stockIdList != null, 

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    private String unlockedList = null;
    private Set<String> unlockedSet =  null;
    
    private Id plantId = null;
    private Id stockId = null;
    private Id mainStockId = null;
    private Map<ID,ID> articleReplacementMap = null;
    public Integer sysparamStockV8 = -1;
    
    /**
     * Entry point for starting the apex batch replenishing all stocks of the given plant
     * where plant is defined by its id.
     *
     * @param plantId
     * @param max_cycles 0 if not limited, number of stocks that are to work out
     * @param mode could be 'trace', 'test' or 'no trace'
     *             This parameter controls only the traces in the log files.
     *             The traces are written only for the 'trace' and 'test' values.
     * @return the id of the batch job
     */
    public static ID asyncReplenishAll(Id plantId, Integer max_cycles, String mode)
    {
        List<SCPlant__c> plantList = [select Id from SCPlant__c where id =: plantId];
        if(plantList.size() > 0)
        {
            SCbtcStockReplenishment btc = new SCbtcStockReplenishment(plantId, max_cycles, mode);
            btc.batchprocessid = btc.executeBatch(batchSize);
            return btc.batchprocessid;
        }
        else
        {
            throw new SCfwException('There is no plant with id = ' + plantId);
            return null;
        }
    } // asyncReplenishAll

    /**
     * Entry point for starting the apex batch replenishing all stocks of the given plant
     * where plant is defined by its name e.g. SCPlant.Name = 'GB'
     *
     * @param plantName
     * @param max_cycles 0 if not limited, number of stocks that are to work out
     * @param mode could be 'trace', 'test' or 'no trace'
     *             This parameter controls only the traces in the log files.
     *             The traces are written only for the 'trace' and 'test' values.
     * @return the id of the batch job
     */
    public static ID asyncReplenishAllForPlantName(String plantName, Integer max_cycles, String mode)
    {
        System.debug('###plantName: ' + plantName);
        ID plantId = null;
        List<SCPlant__c> plantList = [select Id from SCPlant__c where Name =: plantName];
        if(plantList.size() > 0)
        {
            plantId = plantList[0].Id;
            SCbtcStockReplenishment btc = new SCbtcStockReplenishment(plantId, max_cycles, mode);
            btc.batchprocessid = btc.executeBatch(batchSize);
            return btc.batchprocessid;
        }
        else
        {
            throw new SCfwException('There is no plant with name = ' + plantName);
            return null;
        }
    } // asyncReplenishAllForPlantName

    /**
     * Entry point for starting the apex batch replenishing the given stock,
     * where the stock is defined by its id
     *
     * @param stockId
     * @param mode could be 'trace', 'test' or 'no trace'
     *             This parameter controls only the traces in the log files.
     *             The traces are written only for the 'trace' and 'test' values.
     * @return the id of the batch job
     */
    public static ID asyncReplenishStock(Id stockId, String mode)
    {
        List<SCStock__c> stockList = [select Plant__c from SCStock__c where id =: stockId];
        ID plantId = null;
        if(stockList.size() > 0)
        {
            plantId = stockList[0].Plant__c;
            Integer max_cycles = 0;
            SCbtcStockReplenishment btc = new SCbtcStockReplenishment(stockId, plantId, max_cycles, mode);
            btc.batchprocessid = btc.executeBatch(batchSize);
            return btc.batchprocessid;
        }
        else
        {
            throw new SCfwException('There is no stock with id = ' + stockId + ' or stock profile optimization is false!');
            return null;
        }
    } // asyncReplenishStock

    /**
     * Entry point for starting the apex batch replenishing the given stock,
     * where the stock is defined by its id
     *
     * @param stockId
     * @param mode could be 'trace', 'test' or 'no trace'
     *             This parameter controls only the traces in the log files.
     *             The traces are written only for the 'trace' and 'test' values.
     * @return the id of the batch job
     */
    public static ID asyncReplenishStockDeterminedByName(String stockName, String mode)
    {
        System.debug('###mode: ' + mode);
        System.debug('###stockName: ' + stockName);
        if(stockName != null)
        {
            List<SCStock__c> stockList = [Select Id, Plant__c from SCStock__c where Name = :stockName];
            if(stockList.size() > 0)
            {
                ID stockId = stockList[0].Id;
                ID plantId = stockList[0].Plant__c;
                System.debug('###stockId: ' + stockId);
                Integer max_cycles = 0;
                SCbtcStockReplenishment btc = new SCbtcStockReplenishment(stockId, plantId, max_cycles, mode);
                btc.batchprocessid = btc.executeBatch(batchSize);
                return btc.batchprocessid;
            }
            else
            {
                throw new SCfwException('There is no stock with name = ' + stockName);
                return null;
            }
        }
        return null;
    } // asyncReplenishStockFromName

    
    /**
     * Entry point for starting the apex batch replenishing the given stock id list,
     *
     * @param list of stock ids
     * @param mode could be 'trace', 'test' or 'no trace'
     *             This parameter controls only the traces in the log files.
     *             The traces are written only for the 'trace' and 'test' values.
     * @return the id of the batch job
     */
    public static ID asyncReplenishStockList(List<String> stockIdList)
    {
        if(stockIdList == null || stockIdList.size() == 0)
        {
            return null;
        }   
        SCbtcStockReplenishment btc = new SCbtcStockReplenishment(stockIdList, 'trace');
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncReplenishStockList 
    

    
    /**
     * Entry point for starting the synchronous replenishing of the given stock,
     * where the stock is defined by its id
     *
     * @param stockId
     */
    Webservice static void syncReplenishStock(ID stockId)
    {
        List<SCStock__c> stockList = [select  Id, Plant__c from SCStock__c where id =: stockId];
        ID plantId = null;
        if(stockList.size() > 0)
        {
            SCStock__c stock = stockList[0];
            plantId = stockList[0].Plant__c;
            Integer max_cycles = 0;
            String mode = 'trace';
            SCbtcStockReplenishment btc = new SCbtcStockReplenishment(stockId, plantId, max_cycles, mode);
            List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
            btc.replenishStock(stock, interfaceLogList);
            insert interfaceLogList;
        }
        else
        {
            throw new SCfwException('There is no stock with id = ' + stockId);
        }
    }//syncReplenishStoc

    /**
     * Entry point for starting the synchronous call replenishing the given stock id list,
     *
     * @param list of stock ids
     */
    Webservice static void syncReplenishStockList(List<String> stockIdList)
    {
        if(stockIdList == null || stockIdList.size() == 0)
        {
            return;
        }   
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        String mode = 'trace';
        SCbtcStockReplenishment btc = new SCbtcStockReplenishment(stockIdList, mode);
        List<SCStock__c> stockList = [select Id, Plant__c
                                    from SCStock__c where id in :stockIdList];
        for(SCStock__c stock: stockList)
        {
            btc.debug('stock id: ' + stock.Id);
            btc.replenishStock(stock, interfaceLogList);
        }
        insert interfaceLogList;
    }//syncReplenishStockList

    
    /**
     * Constructor
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcStockReplenishment(String plantId, Integer max_cycles, String mode)
    {
        this(null, plantId, max_cycles, mode);
    }


    /**
     * Constructor
     * @param stockId
     * @param mode could be 'test' or 'productive'
     */
    public SCbtcStockReplenishment(Id stockId, String plantId, Integer max_cycles, String mode)
    {
        this.stockId = stockId;
        this.plantId = plantId;
        this.max_cycles = max_cycles;
        this.mode = mode;
        // read replacement list for supersession
        articleReplacementMap = SCbtcStockOptimization.createReplacementMap();
        unlockedList = appSettings.MATERIAL_REORDER_LOCKTYPE__c;
        if(unlockedList != null && unlockedList != '')
        {
            unlockedSet = new Set<String>();
            SCMatMoveBaseExtension.fillUnlockedSet(unlockedSet, unlockedList);
        }    
        if(plantId != null && mainStockId == null)
        {
            List<SCBusinessUnit__c> businessUnitList = [Select Stock__r.id from SCBusinessUnit__c where Plant__c = : plantId];
            if(businessUnitList.size() > 0)
            {
                this.mainStockId = businessUnitList[0].Stock__r.id;
            }
        }
        SCfwDomain domSysParam = new SCfwDomain('DOM_SYSPARAM');
        SCfwDomainValue domVal = domSysParam.getDomainValue('1400159', true);
        String paramV8 = domVal.getControlParameter('V8');
        if ((null != paramV8) && (paramV8.length() > 0))
        {
            sysparamStockV8 = Integer.valueOf(paramV8);
        }
    }

    /**
     * Constructor for the list
     */
    public SCbtcStockReplenishment(List<String> stockIdList, String mode)
    {
        this(null, null, 0, mode);
        this.stockIdList = stockIdList;
        List<SCStock__c> stockList = [select Id, Plant__c from SCStock__c where id in :stockIdList];
        for(SCStock__c stock: stockList)
        {
            List<SCBusinessUnit__c> businessUnitList = [Select Stock__r.id from SCBusinessUnit__c where Plant__c = : stock.Plant__c];
            if(businessUnitList.size() > 0)
            {
                this.plantId = stock.Plant__c;
                this.mainStockId = businessUnitList[0].Stock__r.id;
            }
            break;
        }
    }
    
    
   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with up to 50 mio records possible
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        debug('start');
        /* To check the syntax of SOQL statement */
        List<SCStock__c> stockList = [select  Id, Plant__c from SCStock__c where Replenishment__c = true limit 1];

        debug('start: after checking statement');
        /**/
        String query = null;
        if(stockId == null)
        {
            query = 'select  Id, Plant__c '
                  + ' from SCStock__c where Plant__c = \'' + plantID  + '\' and Replenishment__c = true';
            if(max_cycles > 0)
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            query = 'Select id, Plant__c '
            + ' from SCStock__c where id = \'' + stockId + '\'' ;
        }

        if(stockIdList != null)
        {
            debug('stockIdList:' + stockIdList);
            return Database.getQueryLocator([select id, Plant__c from SCStock__c where Id in :stockIdList and Replenishment__c = true]);           
        }
        return Database.getQueryLocator(query);
    } // start
    
   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCStock__c c = (SCStock__c)res;
            replenishStock(c, interfaceLogList);
        }
        insert interfaceLogList;
    } // execute

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
    } // finish

    /**
     * Replenishes the stock
     *
     * @param stock
     * @param interfaceLogList
     */
    public void replenishStock(SCStock__c stock, List<SCInterfaceLog__c> interfaceLogList)
    {
        Datetime start = Datetime.now();
        Savepoint sp = Database.setSavepoint();
        Boolean thrownException = false;
        String resultInfo = '';
        Integer count = 0;        
        String step = '';
        try
        {
            step = 'Gets replenishments.';
            List<Id> stockList = new List<Id>();
            stockList.add(stock.Id);
            Map<Id, List<SCMatMoveBaseExtension.ReplenishmentMaterialInfo>> mapStockMatInfos = 
                    new Map<Id, List<SCMatMoveBaseExtension.ReplenishmentMaterialInfo>>();
            Date requestDate = Date.today(); 
            Integer sysparamStockV8 = 0;       
            SCMatMoveBaseExtension.getReplenishmentMaterialCore(stockList, requestDate, 
                                                                mapStockMatInfos,sysparamStockV8);
            
            step = 'Create material movements.';
            SCMatMoveBaseExtension.createMatMovesCore(mapStockMatInfos, mainStockId, stock.id,requestDate); 
            resultInfo = 'Stock has been replenished!';    
        }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo = 'step: ' + step + ', exception: '  + e.getMessage();
            String prevMode = mode;
            mode = 'test';
            debug('resultInfo:' + resultInfo);
            mode = prevMode;
            Database.rollback(sp); 
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException)
            {
                resultCode = 'E101';
            }
            logBatchInternal(interfaceLogList, 'STOCK_REPLENISHMENT', 'SCbtcStockReplenishment',
                                    stock.Id, null, resultCode, 
                                    resultInfo, 'Stock: ' + stock, start, 1);
        }        
    
    }// replenishStock


    /**
     * Creates the log record for one stock that has been optimized.
     *
     */
    public static void logBatchInternal(List<SCInterfaceLog__c> interfaceLogList, String interfaceName, 
                                String interfaceHandler,
                                ID referenceID, ID referenceID2, String resultCode, 
                                String resultInfo, String data, Datetime start, Integer count)
    {
        SCInterfaceLog__c ic = new SCInterfaceLog__c();
        interfaceLogList.add(ic);
        ic.Interface__c = interfaceName;
        ic.InterfaceHandler__c = interfaceHandler;
        ic.Type__c = 'BATCH';
        ic.Count__c = count;
        ic.ReferenceID__c = referenceID;
        ic.ReferenceID2__c = referenceID2;
        ic.ResultCode__c = '';
        ic.ResultCode__c = resultCode;
        ic.ResultInfo__c = resultInfo;
        ic.Data__c = data;
        ic.Direction__c = 'internal';
        ic.Start__c = start;
        ic.Stop__c = Datetime.now();
        Long duration = ic.Stop__c.getTime() - ic.Start__c.getTime();
        ic.Duration__c = duration; 
    }       


    private void debug(String text)
    {
        if(mode.equalsIgnoreCase('test')
           || mode.equalsIgnoreCase('trace'))
        {
            System.debug('###...................' + text);
        }
    }

}