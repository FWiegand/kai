/**********************************************************************

Name: fsFA_PofS_ImageComponentController

======================================================

Purpose: 

This is the test class for fsFA_PofS_ImageComponentController

======================================================

History 

------- 

Date 			AUTHOR 				DETAIL

11/08/2013 		Oliver Preuschl 	INITIAL DEVELOPMENT

***********************************************************************/

@isTest
private class fsFA_PofS_ImageComponentController_Test {
	static testMethod void myUnitTest() {
		createCustomSettings();
        System.debug( '#CATypes: ' + PofSClickAreaTypes__c.getInstance( 'Area' ) );
		PictureOfSuccess__c LO_PofS = new PictureOfSuccess__c(
			Bezeichnung__c					=	'Name',
			ValidFrom__c					=	Date.today(),
			ValidUntil__c					=	Date.today(),
			RedWeightingActivation__c		=	25,
			RedWeightingArea__c				=	25,
			RedWeightingAssortment__c		=	25,
			RedWeightingEquipment__c		=	25,
			Subtradechannel__c				=	'SubtradeChannel'
		);
		insert( LO_PofS );
		
		fsFA_PofS_ImageComponentController LO_ImageComponentConotroller = new fsFA_PofS_ImageComponentController();
		LO_ImageComponentConotroller.GV_ImageId = LO_PofS.Id;
		LO_ImageComponentConotroller.init();
		
		LO_ImageComponentConotroller.drawNewArea();
		LO_ImageComponentConotroller.GO_ImageArea.Type__c = 'Area';
		LO_ImageComponentConotroller.GO_ImageArea.Name = 'Name';
		
		LO_ImageComponentConotroller.newQuestion();
		LO_ImageComponentConotroller.GM_ClickAreaQuestions.get( 1 ).put( 'Name', 'Name' );
		LO_ImageComponentConotroller.GM_ClickAreaQuestions.get( 1 ).put( 'Type__c', 'Type' );
		LO_ImageComponentConotroller.GM_ClickAreaQuestions.get( 1 ).put( 'Potential_Hint__c', 'Hint' );

		LO_ImageComponentConotroller.saveArea();
		
	}
    
    public static void createCustomSettings(){
        /*List< PofSDocumentsFolder__c > LL_FolderSettings = new List< PofSDocumentsFolder__c >();
        PofSDocumentsFolder__c LO_FolderSetting = new PofSDocumentsFolder__c(
        		Name			= 'PofSBackgrounds',
            	FolderId__c	= '00lc0000000XTXcAAO'
        	);
        LL_FolderSettings.add( LO_FolderSetting );
        LO_FolderSetting = new PofSDocumentsFolder__c(
        		Name			= 'PofSClickareaIcons',
            	FolderId__c	= '00lc0000000XTXhAAO'
        	);
        LL_FolderSettings.add( LO_FolderSetting );
        LO_FolderSetting = new PofSDocumentsFolder__c(
        		Name			= 'PofSRedSurveyPictures',
            	FolderId__c	= '00lc0000000XTXmAAO'
        	);
        LL_FolderSettings.add( LO_FolderSetting );
        LO_FolderSetting = new PofSDocumentsFolder__c(
        		Name			= 'ArticleGroupIcons',
            	FolderId__c	= '00lc0000000XYz2AAG'
        	);
        LL_FolderSettings.add( LO_FolderSetting );*/
        List< sObject > LL_FolderSettings = Test.loadData( PofSDocumentsFolder__c.sObjectType, 'TestDataDocumentFolders' );
        List< sObject > LL_ClickAreaTypes = Test.loadData( PofSClickAreaTypes__c.sObjectType, 'TestDataClickAreaTypes' );
    }
}