/*
 * @(#)SCboEngineerStandby.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Helper for evaluating engineer standbys
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCboEngineerStandby
{
    /*
     * Determines the standby engineers for the given parameters.
     *
     * @param engineer     engineer for which standby engineers should be found
     * @param groups       groups for which standby engineers should be found
     * @param fromDate     start of the standby interval
     * @param toDate       end of the standby interval
     * @return a list of standby engineers
     */
    public static List<SCEngineerStandby__c> readStandbys(Id engineer, List<String> groups, Date fromDate, Date toDate)
    {
        if (null == fromDate)
        {
            fromDate = Date.TODAY();
        }
        if (null == toDate)
        {
            toDate = Date.TODAY();
        }

        System.debug('#### readStandbys(): engineer -> ' + engineer);
        System.debug('#### readStandbys(): groups -> ' + groups);
        System.debug('#### readStandbys(): fromDate -> ' + fromDate.format());
        System.debug('#### readStandbys(): toDate -> ' + toDate.format());
        
        List<SCEngineerStandby__c> standbys;
        if (groups.size() > 0)
        {
            standbys = [Select Id, Name, ValidFrom__c, ValidTo__c, Engineer__c, Group__c, 
                               StandbyEngineer__c, StandbyEngineer__r.Alias_txt__c, 
                               StandbyEngineer__r.FirstName_txt__c, 
                               StandbyEngineer__r.LastName_txt__c 
                          from SCEngineerStandby__c 
                         where ValidFrom__c <= :fromDate and ValidTo__c >= :toDate 
                           and (Engineer__c = :engineer 
                            or  Group__c in :groups)];
        }
        else
        {
            standbys = [Select Id, Name, ValidFrom__c, ValidTo__c, Engineer__c, Group__c, 
                               StandbyEngineer__c, StandbyEngineer__r.Alias_txt__c, 
                               StandbyEngineer__r.FirstName_txt__c, 
                               StandbyEngineer__r.LastName_txt__c 
                          from SCEngineerStandby__c 
                         where ValidFrom__c <= :fromDate and ValidTo__c >= :toDate 
                           and Engineer__c = :engineer];
        }
        return standbys;
    } // readStandbys
    
     
    /*
     * Returns a string of the standby engineers and the date interval.
     *
     * @param items     a list of items
     * @param maxitems  the maximum number of items (-1 for all)
     * @return a string for displying the standby engineers on a page.
     */
    public static String convert(List<SCEngineerStandby__c> items, Integer maxitems)
    {
        String result = '';
        integer i = 0;
        for(SCEngineerStandby__c item :items)
        {
            if (i > 0)
            {
                result += ' | ';
            }
            if (SCBase.isSet(item.StandbyEngineer__r.FirstName_txt__c))
            {
                result += item.StandbyEngineer__r.FirstName_txt__c + ' ';
            }
            result += item.StandbyEngineer__r.LastName_txt__c + ' (' + 
                      item.StandbyEngineer__r.Alias_txt__c + ') ' + 
                      SCBase.formatDateInterval(item.ValidFrom__c, item.ValidTo__c.isSameDay(item.ValidFrom__c) ? null : item.ValidTo__c);

            i++;
            
            if ((i >= maxitems) && (maxitems > 0))
            {
                break;
            }
        }
        return result;
    }
} // SCboEngineerStandby