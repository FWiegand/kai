/*
 * @(#)piCceagDeSfdcCSTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Dummy test class to ensure that we have > 0% code coverage
 * @author GMS
 */
@isTest
private class piCceagDeSfdcCSTest
{

    static testMethod void notificationPositiv1() 
    {
        Test.startTest();
        
        piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element p1_1 = new piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocument_element();
        piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocumentCreate p1_2 = new piCceagDeSfdcCSArchiveDocumentInsert.ArchiveDocumentCreate();
        piCceagDeSfdcCSArchiveDocumentInsert.BusinessDocumentMessageHeader p1_3 = new piCceagDeSfdcCSArchiveDocumentInsert.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSArchiveDocumentInsert.HTTPS_Port p1_4 = new piCceagDeSfdcCSArchiveDocumentInsert.HTTPS_Port();
        piCceagDeSfdcCSArchiveDocumentInsert.SFDCResponse_element p1_5 = new piCceagDeSfdcCSArchiveDocumentInsert.SFDCResponse_element();
        
        p1_4.endpoint_x = 'TEST';
        p1_4.ArchiveDocumentCreate_Out(p1_3, p1_1);
        
        p1_4.clientCertName_x = 'x';
        
        piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element p2_1 = new piCceagDeSfdcCSAssetInventoryDataChange.AssetHeader_element();
        piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest p2_2 = new piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest();
        piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort p2_3 = new piCceagDeSfdcCSAssetInventoryDataChange.AssetInventoryDataChangeRequest_OutPort();
        piCceagDeSfdcCSAssetInventoryDataChange.CompanyCode_element p2_4 = new piCceagDeSfdcCSAssetInventoryDataChange.CompanyCode_element();
        piCceagDeSfdcCSAssetInventoryDataChange.SFDCResponse_element p2_5 = new piCceagDeSfdcCSAssetInventoryDataChange.SFDCResponse_element();
        
        piCceagDeSfdcCSMaterialInventoryCreate.BusinessDocumentMessageHeader p3_1 = new piCceagDeSfdcCSMaterialInventoryCreate.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSMaterialInventoryCreate.HTTPS_Port p3_2 = new piCceagDeSfdcCSMaterialInventoryCreate.HTTPS_Port();
        piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocument_element p3_3 = new piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocument_element();
        piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentCreate p3_4 = new piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentCreate();
        piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentItem p3_5 = new piCceagDeSfdcCSMaterialInventoryCreate.InventoryDocumentItem();
        piCceagDeSfdcCSMaterialInventoryCreate.Items_element p3_6 = new piCceagDeSfdcCSMaterialInventoryCreate.Items_element();
        piCceagDeSfdcCSMaterialInventoryCreate.SFDCResponse_element p3_7 = new piCceagDeSfdcCSMaterialInventoryCreate.SFDCResponse_element();
        
        piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader p4_1 = new piCceagDeSfdcCSMaterialMovementCreate.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port p4_2 = new piCceagDeSfdcCSMaterialMovementCreate.HTTPS_Port();
        piCceagDeSfdcCSMaterialMovementCreate.MaterialMovementCreate p4_3 = new piCceagDeSfdcCSMaterialMovementCreate.MaterialMovementCreate();
        piCceagDeSfdcCSMaterialMovementCreate.MovementItem p4_4 = new piCceagDeSfdcCSMaterialMovementCreate.MovementItem();
        piCceagDeSfdcCSMaterialMovementCreate.SFDCResponse_element p4_5 = new piCceagDeSfdcCSMaterialMovementCreate.SFDCResponse_element();
        
        piCceagDeSfdcCSMaterialPackageReceived.BusinessDocumentMessageHeader p5_1 = new piCceagDeSfdcCSMaterialPackageReceived.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSMaterialPackageReceived.HTTPS_Port p5_2 = new piCceagDeSfdcCSMaterialPackageReceived.HTTPS_Port();
        piCceagDeSfdcCSMaterialPackageReceived.SFDCResponse_element p5_3 = new piCceagDeSfdcCSMaterialPackageReceived.SFDCResponse_element();
        piCceagDeSfdcCSMaterialPackageReceived.SparePartPackageStatusUpdate p5_4 = new piCceagDeSfdcCSMaterialPackageReceived.SparePartPackageStatusUpdate();
        piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element p5_5 = new piCceagDeSfdcCSMaterialPackageReceived.SparePartPackage_element();
        
        piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader p6_1 = new piCceagDeSfdcCSMaterialReservationCreate.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port p6_2 = new piCceagDeSfdcCSMaterialReservationCreate.HTTPS_Port();
        piCceagDeSfdcCSMaterialReservationCreate.MaterialReservationCreate p6_3 = new piCceagDeSfdcCSMaterialReservationCreate.MaterialReservationCreate();
        piCceagDeSfdcCSMaterialReservationCreate.ReservationItem p6_4 = new piCceagDeSfdcCSMaterialReservationCreate.ReservationItem();
        piCceagDeSfdcCSMaterialReservationCreate.SFDCResponse_element p6_5 = new piCceagDeSfdcCSMaterialReservationCreate.SFDCResponse_element();
        
        piCceagDeSfdcCSOrderClose.BusinessDocumentMessageHeader p7_1 = new piCceagDeSfdcCSOrderClose.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSOrderClose.CustomerServiceOrder_element p7_2 = new piCceagDeSfdcCSOrderClose.CustomerServiceOrder_element();
        piCceagDeSfdcCSOrderClose.CustomerServiceOrderClose p7_3 = new piCceagDeSfdcCSOrderClose.CustomerServiceOrderClose();
        piCceagDeSfdcCSOrderClose.HTTPS_Port p7_4 = new piCceagDeSfdcCSOrderClose.HTTPS_Port();
        piCceagDeSfdcCSOrderClose.NotificationData_element p7_5 = new piCceagDeSfdcCSOrderClose.NotificationData_element();
        piCceagDeSfdcCSOrderClose.Reason_element p7_6 = new piCceagDeSfdcCSOrderClose.Reason_element();
        piCceagDeSfdcCSOrderClose.Reasons_element p7_7 = new piCceagDeSfdcCSOrderClose.Reasons_element();
        piCceagDeSfdcCSOrderClose.SFDCResponse_element p7_8 = new piCceagDeSfdcCSOrderClose.SFDCResponse_element();
        piCceagDeSfdcCSOrderClose.TimeConfirmation_element p7_9 = new piCceagDeSfdcCSOrderClose.TimeConfirmation_element();
        piCceagDeSfdcCSOrderClose.TimeConfirmations_element p7_10 = new piCceagDeSfdcCSOrderClose.TimeConfirmations_element();
        
        piCceagDeSfdcCSOrderCreate.BusinessDocumentMessageHeader p8_1 = new piCceagDeSfdcCSOrderCreate.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSOrderCreate.CustomerServiceOrder_element p8_2 = new piCceagDeSfdcCSOrderCreate.CustomerServiceOrder_element();
        piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate p8_3 = new piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate();
        piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate_OutPort p8_4 = new piCceagDeSfdcCSOrderCreate.CustomerServiceOrderCreate_OutPort();
        piCceagDeSfdcCSOrderCreate.NotificationData_element p8_5 = new piCceagDeSfdcCSOrderCreate.NotificationData_element();
        piCceagDeSfdcCSOrderCreate.Partner p8_6 = new piCceagDeSfdcCSOrderCreate.Partner();
        piCceagDeSfdcCSOrderCreate.Partners_element p8_7 = new piCceagDeSfdcCSOrderCreate.Partners_element();
        piCceagDeSfdcCSOrderCreate.SalesData p8_8 = new piCceagDeSfdcCSOrderCreate.SalesData();
        piCceagDeSfdcCSOrderCreate.SFDCResponse_element p8_9 = new piCceagDeSfdcCSOrderCreate.SFDCResponse_element();
        
        piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader p9_1 = new piCceagDeSfdcCSOrderEquipmentUpdate.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrder_element p9_2 = new piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrder_element();
        piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate p9_3 = new piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate();
        piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate_OutPort p9_4 = new piCceagDeSfdcCSOrderEquipmentUpdate.CustomerServiceOrderEquipmentUpdate_OutPort();
        piCceagDeSfdcCSOrderEquipmentUpdate.Partner p9_5 = new piCceagDeSfdcCSOrderEquipmentUpdate.Partner();
        piCceagDeSfdcCSOrderEquipmentUpdate.Partners_element p9_6 = new piCceagDeSfdcCSOrderEquipmentUpdate.Partners_element();
        piCceagDeSfdcCSOrderEquipmentUpdate.SalesData p9_7 = new piCceagDeSfdcCSOrderEquipmentUpdate.SalesData();
        piCceagDeSfdcCSOrderEquipmentUpdate.SFDCResponse_element p9_8 = new piCceagDeSfdcCSOrderEquipmentUpdate.SFDCResponse_element();
        piCceagDeSfdcCSOrderEquipmentUpdate.SubEquipments_element p9_9 = new piCceagDeSfdcCSOrderEquipmentUpdate.SubEquipments_element();
        
        piCceagDeSfdcCSOrderExternalOperationAdd.BusinessDocumentMessageHeader p10_1 = new piCceagDeSfdcCSOrderExternalOperationAdd.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSOrderExternalOperationAdd.CustomerServiceOrderAddExternalOperation p10_2 = new piCceagDeSfdcCSOrderExternalOperationAdd.CustomerServiceOrderAddExternalOperation();
        piCceagDeSfdcCSOrderExternalOperationAdd.ExternalOperation_element p10_3 = new piCceagDeSfdcCSOrderExternalOperationAdd.ExternalOperation_element();
        piCceagDeSfdcCSOrderExternalOperationAdd.HTTPS_Port p10_4 = new piCceagDeSfdcCSOrderExternalOperationAdd.HTTPS_Port();
        piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItem_element p10_5 = new piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItem_element();
        piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItems_element p10_6 = new piCceagDeSfdcCSOrderExternalOperationAdd.ServiceItems_element();
        piCceagDeSfdcCSOrderExternalOperationAdd.SFDCResponse_element p10_7 = new piCceagDeSfdcCSOrderExternalOperationAdd.SFDCResponse_element();
        
        piCceagDeSfdcCSOrderExternalOperationRem.BusinessDocumentMessageHeader p11_1 = new piCceagDeSfdcCSOrderExternalOperationRem.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSOrderExternalOperationRem.CustomerServiceOrderRemoveExternalOperation p11_2 = new piCceagDeSfdcCSOrderExternalOperationRem.CustomerServiceOrderRemoveExternalOperation();
        piCceagDeSfdcCSOrderExternalOperationRem.ExternalOperation_element p11_3 = new piCceagDeSfdcCSOrderExternalOperationRem.ExternalOperation_element();
        piCceagDeSfdcCSOrderExternalOperationRem.HTTPS_Port p11_4 = new piCceagDeSfdcCSOrderExternalOperationRem.HTTPS_Port();
        piCceagDeSfdcCSOrderExternalOperationRem.SFDCResponse_element p11_5 = new piCceagDeSfdcCSOrderExternalOperationRem.SFDCResponse_element();
        
        piCceagDeSfdcCSReprocessingRequestResp.BusinessDocumentMessageHeader p12_1 = new piCceagDeSfdcCSReprocessingRequestResp.BusinessDocumentMessageHeader();
        piCceagDeSfdcCSReprocessingRequestResp.HTTPS_Port p12_2 = new piCceagDeSfdcCSReprocessingRequestResp.HTTPS_Port();
        piCceagDeSfdcCSReprocessingRequestResp.IDocReprocessingRequest p12_3 = new piCceagDeSfdcCSReprocessingRequestResp.IDocReprocessingRequest();
        piCceagDeSfdcCSReprocessingRequestResp.IDocReprocessingResponse p12_4 = new piCceagDeSfdcCSReprocessingRequestResp.IDocReprocessingResponse();
        piCceagDeSfdcCSReprocessingRequestResp.NOSC_Log p12_5 = new piCceagDeSfdcCSReprocessingRequestResp.NOSC_Log();
        piCceagDeSfdcCSReprocessingRequestResp.NOSC_LogItem p12_6 = new piCceagDeSfdcCSReprocessingRequestResp.NOSC_LogItem();
        
        System.assertEquals('x',p1_4.clientCertName_x);
        
        Test.stopTest();
    }

}