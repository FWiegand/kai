/*
 * @(#)SCNewOnePageOrderRedir.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCNewOnePageOrderRedir
{
    private Id id;
    
    public SCNewOnePageOrderRedir(ApexPages.StandardController controller) 
    {
        System.debug('#### SCNewOnePageOrderRedir()');
        this.id = controller.getId();
    } // SCNewOnePageOrderRedir

    public PageReference openOnePageOrder()
    {
        System.debug('#### SCNewOnePageOrderRedir.openOnePageOrder()');
        PageReference page = new PageReference('/apex/SCOrderCreation?aid=' + this.id);
        
        return page;
    } // openOnePageOrder
} // SCNewOnePageOrderRedir