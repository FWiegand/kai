/**
 * @(#)SCInterfaceClearing
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

 /**
 * Test class for the wrapper class  SCInterfaceClearingSAPCloseUtil
 * @author Georg Birkenheuer <gbirkenheuer@gms-online.de>
 */
public with sharing class SCInterfaceClearingSAPCloseUtilTest {

    static testMethod void createInstalledBase() 
    {
    	SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    	SCOrder__c testOrder = CCWCTestBase.createTestOrder(appSettings, true);
    	SCboOrder order = new SCBoOrder(testOrder);
    	
    	Boolean success = SCInterfaceClearingSAPCloseUtil.orderSAPClose(order);


 
    }


}