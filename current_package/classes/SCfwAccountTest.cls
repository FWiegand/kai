/*
 * @(#)SCfwAccountTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwAccountTest
{
    /**
     * Create SCfwAccount and add default roles
     */
    private static testMethod void createPositive1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createAccountRoles(SCHelperTestClass.account, 
                                           SCHelperTestClass.account, 
                                           true);
        SCfwAccount fwAccount = new SCfwAccount(SCHelperTestClass.account);                  
        fwAccount.addAllDefaultRoles(SCHelperTestClass.account.Id, 
                                     SCHelperTestClass.account.Id);

        System.assertNotEquals(null, fwAccount);
        System.assertNotEquals(0, fwAccount.getAllRole().size());
        
        // Test case 1
        SCfwAccount a = new SCfwAccount(SCHelperTestClass.account.id);
        System.assertEquals(SCHelperTestClass.account.id, fwAccount.account.id);
    }
    
    /**
     * Create SCfwAccount and add a specific default role
     */
    private static testMethod void createPositive2()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createAccountRoles(SCHelperTestClass.account, 
                                           SCHelperTestClass.account, 
                                           true);
        SCfwAccount fwAccount = new SCfwAccount(SCHelperTestClass.account);                  

        fwAccount.addAllDefaultRoles(SCHelperTestClass.account.Id, 
                                     SCHelperTestClass.account.Id,
                                     '50303'); // invoice recipient

        System.assertNotEquals(null, fwAccount);
        System.assertNotEquals(0, fwAccount.getAllRole().size());
        
        // test case 2
        Account res3 = fwAccount.getServiceRecipient();
        //TODO  System.assertEquals(SCHelperTestClass.allAccountRole[0].SlaveAccount__c, res3.id);
        
        // test case 3
        SCfwAccount fwAccount2 = new SCfwAccount(SCHelperTestClass.account); 
        SCAccountRole__c res2 = fwAccount2.getRole('50303');
    }

    private static testMethod void createNegative1()
    {
        try
        {
            SCfwAccount b = new SCfwAccount('invaldid');
        } catch(Exception e){}
    }
    
    /**
     * Check the isGeoCoded function for the Account
     */
    private static testMethod void accountGeoCodedPositive1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createAccountRoles(SCHelperTestClass.account, 
                                           SCHelperTestClass.account, 
                                           true);
        // geocodes account
        System.assert(SCfwAccount.isGeoCoded(SCHelperTestClass.account));
        // non geocoded account
        Account a = new Account();
        System.assert(!SCfwAccount.isGeoCoded(a));
    }

    /**
     * Check the isGeoCoded function for the Account role
     */
    private static testMethod void roleGeoCodedPositive1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createAccountRoles(SCHelperTestClass.account, 
                                           SCHelperTestClass.account, 
                                           true);

        System.assert(SCfwAccount.isGeoCoded(SCHelperTestClass.account));
        
        SCOrderRole__c orderRole = new SCOrderRole__c(); 
        System.assert(!SCfwAccount.isGeoCoded(orderRole));

        orderRole.GeoX__c = 1;
        orderRole.GeoY__c = 1;
        System.assert(SCfwAccount.isGeoCoded(orderRole));
    }

    static testmethod void findAccount1()
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        
        Test.startTest();

        // test 1: look for an existing address
        Account srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = SCHelperTestClass.account.BillingStreet;
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for an existing address with 'strasse'
        Account acc = new Account(Id = SCHelperTestClass.account.Id);
        acc.BillingStreet = 'Moormanstr.';
        upsert acc;
        
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstrasse';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 3: look for an existing address with 'str'
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstr';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 4: look for an existing address with 'straße' inside
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstraße';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 5: look for an existing address with 'strasse' inside
        acc = new Account(Id = SCHelperTestClass.account.Id);
        acc.BillingStreet = 'Moormanstraße';
        upsert acc;
        
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstrasse';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 6: look for an existing address with 'str'
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstr.';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 7: look for an existing address with 'straße' inside
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Moormanstraße';
        srcAccount.BillingHouseNo__c = SCHelperTestClass.account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = SCHelperTestClass.account.BillingPostalCode;
        srcAccount.BillingCity = SCHelperTestClass.account.BillingCity;
        srcAccount.BillingCountry__c = SCHelperTestClass.account.BillingCountry__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        
        // test 8: look for an address, that doesn't exist
        srcAccount = new Account();
        srcAccount.Firstname__c = SCHelperTestClass.account.FirstName__c;
        srcAccount.Lastname__c = SCHelperTestClass.account.LastName__c;
        srcAccount.Name = SCHelperTestClass.account.FirstName__c + ' ' + SCHelperTestClass.account.LastName__c;
        srcAccount.BillingStreet = 'Altdorfer Str.';
        srcAccount.BillingHouseNo__c = '12';
        srcAccount.BillingPostalCode = '91207';
        srcAccount.BillingCity = 'Lauf';
        srcAccount.BillingCountry__c = 'DE';
        
        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount2()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'Werner';
        account.FirstName__c = 'Hans';
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname
        Account srcAccount = new Account();
        // srcAccount.Firstname__c = account.FirstName__c;
        srcAccount.Lastname__c = account.LastName__c;
        srcAccount.Name = account.LastName__c;
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using first- and lastname
        srcAccount.Firstname__c = account.FirstName__c;
        srcAccount.Name = account.FirstName__c + ' ' + account.LastName__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount3()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'Apel, Hans';
        account.FirstName__c = null;
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname
        Account srcAccount = new Account();
        srcAccount.Lastname__c = 'Apel';
        srcAccount.Name = 'Apel';
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using first- and lastname
        srcAccount.Firstname__c = 'Hans';
        srcAccount.Name = 'Hans Apel';

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 3: look for the account by using first- and lastname
        srcAccount.Firstname__c = 'Hans';
        srcAccount.Lastname__c = 'Apel';

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount4()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'Werner';
        account.FirstName__c = 'Hans';
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname
        Account srcAccount = new Account();
        srcAccount.Lastname__c = 'Werner';
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using only the lastname and set the anme field
        srcAccount.Lastname__c = 'Werner';
        srcAccount.Name = 'Werner';

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount5()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'WernerSehrLangerNachname';
        account.FirstName__c = 'Hans';
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname, reduced to 20 characters
        Account srcAccount = new Account();
        srcAccount.Lastname__c = account.LastName__c.substring(0, 20);
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using the lastname, reduced to 20 characters, 
        //         and the firstname
        srcAccount.Lastname__c = account.LastName__c.substring(0, 20);
        srcAccount.Firstname__c = account.FirstName__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount6()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'WernerSehrLangerNachname';
        account.FirstName__c = 'RichardSehrLangerVorname';
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname, reduced to 20 characters
        Account srcAccount = new Account();
        srcAccount.Lastname__c = account.LastName__c.substring(0, 20);
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using the lastname, reduced to 20 characters, 
        //         and the firstname, reduced to 20 characters, 
        srcAccount.Lastname__c = account.LastName__c.substring(0, 20);
        srcAccount.Firstname__c = account.FirstName__c.substring(0, 20);

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }

    static testmethod void findAccount7()
    {
        // create an account
        Account account = new Account();

        account.LastName__c = 'Werner';
        account.FirstName__c = 'Richard';
        account.RecordTypeId = [Select Id from RecordType where DeveloperName = 'Customer' and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '9831 NK';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '16';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        upsert account;
        
        
        Test.startTest();

        // test 1: look for the account by using only the lastname
        Account srcAccount = new Account();
        srcAccount.Lastname__c = account.LastName__c;
        srcAccount.BillingStreet = account.BillingStreet;
        srcAccount.BillingHouseNo__c = account.BillingHouseNo__c;
        srcAccount.BillingPostalCode = account.BillingPostalCode;
        srcAccount.BillingCity = account.BillingCity;
        srcAccount.BillingCountry__c = account.BillingCountry__c;

        Account foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 2: look for the account by using the lastname and the firstname
        srcAccount.Lastname__c = account.LastName__c;
        srcAccount.Firstname__c = account.FirstName__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 3: look for the account by using the lastname and the firstname
        srcAccount.Lastname__c = null;
        srcAccount.Firstname__c = null;
        srcAccount.Name = account.FirstName__c + ' ' + account.LastName__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);

        // test 4: look for the account by using the lastname and the firstname in one field
        srcAccount.Lastname__c = account.LastName__c + ', ' + account.FirstName__c;
        srcAccount.Firstname__c = null;
        // srcAccount.Name = account.FirstName__c + ' ' + account.LastName__c;

        foundAccount = SCfwAccount.findAccount(srcAccount);
        System.AssertNotEquals(null, foundAccount);
        Test.stopTest();
    }
}