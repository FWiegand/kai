/*
 * @(#)SCNewOrderRedir.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Redirects to the order creation wizard
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCNewOrderRedir {
    private Id id;
    
    public SCNewOrderRedir(ApexPages.StandardController controller) 
    {
        this.id = controller.getId();
    }
 
    public PageReference orderWizard()
    {
        PageReference page = new PageReference('/apex/SCOrderWizardCustomer?aid=' + this.id);
        return page;
    }
    
}