/*
 * @(#)SCfwInterfaceRequestPendingExceptionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCfwInterfaceRequestPendingException.
 *
*/ 
@isTest
private class SCfwInterfaceRequestPendingExceptionTest
{
    /**
     * Test the basic exception this class depends on
     */
    static testMethod void SCfwInterfaceRequestPendingExceptionTest1()
    {
        try
        {
            throw new SCfwInterfaceRequestPendingException();
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing two arguments
     */
    static testMethod void SCfwInterfaceRequestPendingExceptionTest2()
    {
        try
        {
            throw new SCfwInterfaceRequestPendingException(true, 'Message');
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key
     */
    static testMethod void SCfwInterfaceRequestPendingExceptionTest3()
    {
        // known key
        try
        {
            throw new SCfwInterfaceRequestPendingException(1000);
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }

        // unknown key
        try
        {
            throw new SCfwInterfaceRequestPendingException(1);
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key and a parameter
     */
    static testMethod void SCfwInterfaceRequestPendingExceptionTest4()
    {
        try
        {
            throw new SCfwInterfaceRequestPendingException(1301, '1234567890');
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key and two parameter
     */
    static testMethod void SCfwInterfaceRequestPendingExceptionTest5()
    {
        try
        {
            throw new SCfwInterfaceRequestPendingException(1200, 'XXX', 'TEST');
        }
        catch (SCfwInterfaceRequestPendingException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }
}