public class fsFA_PofS_EditController{
    
    private Id GV_ImageId;
    public PictureOfSuccess__c GO_Image{Get; Set;}
    public List< PofSClickarea__c > GL_ClickAreas{Get; Set; }
    public Map< Id, SObject > GM_ClickAreaQuestions{Get; Set;}
    public String GV_ImageLink{ get; set; }
    public Integer GV_ImageWidth{ get; set; }
    public Integer GV_ImageHeight{ get; set; }
    
    public Boolean GV_DrawNewArea{get; Set;}
    public Boolean GV_EditArea{get; Set;}
    
    public PofSClickarea__c GO_ImageArea{Get; Set;}
    public Id GV_ImageAreaId{Get;Set;}
    public RedSurveyQuestion__c GO_ClickAreaQuestion{Get; Set;}
    
    public Decimal GV_X{Get; Set;}
    public Decimal GV_Y{Get; Set;}
    
    public String GV_DocumentURL{Get; Set;}
    
    private Map< String, Id > GM_DocumentFolders;
    public Map< String, PofSClickAreaTypes__c > GM_ClickAreaTypes{
        Get{
            if( GM_ClickAreaTypes == null ){
                GM_ClickAreaTypes = new Map< String, PofSClickAreaTypes__c >();
                Map< String, PofSClickAreaTypes__c > LM_ClickAreaTypes = PofSClickAreaTypes__c.getAll();
                for( String LV_ClickAreaName: LM_ClickAreaTypes.KeySet() ){
                    GM_ClickAreaTypes.put( LV_ClickAreaName.replace( ' ', '_' ), LM_ClickAreaTypes.get( LV_ClickAreaName ) );
                }
            }
            return GM_ClickAreaTypes;
        }
        Set;
    }
    
    public PofSSettings__c GO_PofSSettings{
        Get{
            if( GO_PofSSettings == null ){
                GO_PofSSettings = PofSSettings__c.getInstance( 'Default' );
            }
            return GO_PofSSettings;
        }
        Set;
    }

    public fsfA_PofS_EditController(ApexPages.StandardController PO_Controller){
        GV_ImageId = PO_Controller.getId();
        loadImage();
        GV_DrawNewArea = false;
        GV_EditArea = false;
        GV_DocumentURL = 'https://c.cs14.content.force.com/servlet/servlet.FileDownload?file=';
    }
    
    private void loadImage(){
        GO_Image = ( PictureOfSuccess__c ) fsFA_Tools.getRecord( GV_ImageId );
        List< SObject > LL_ClickAreas = fsFA_Tools.getRecordsFlat( 'PofSClickarea__c', 'PictureOfSuccess__c', GV_ImageId ).Values();
        GL_ClickAreas = new List< PofSClickarea__c >();
        for( SObject LO_ClickArea: LL_ClickAreas ){
            GL_ClickAreas.add( ( PofSClickarea__c ) LO_ClickArea );
        }
        //GO_Image = [ SELECT Id, (SELECT Name, Type__c, CenterX__c, CenterY__c FROM Clickareas__r) FROM PictureOfSuccess__c WHERE ( Id =: GV_ImageId ) ];
        Try{
            Attachment LO_Attachment = [ SELECT Id FROM Attachment WHERE ( ParentId =: GV_ImageId ) LIMIT 1 ];
            String LV_SFDCInstance = URL.getSalesforceBaseUrl().getHost().split( '\\.' ).get( 1 );
            System.debug( LV_SFDCInstance );
            GV_ImageLink = 'https://c.' + LV_SFDCInstance + '.content.force.com/servlet/servlet.FileDownload?file=' + LO_Attachment.Id;
        }catch( Exception LO_Exception ){
            GV_ImageLink = '';
        }
        GO_ImageArea = new PofSClickarea__c();
    }
    
    public void drawNewArea(){
        GO_ImageArea = new PofSClickarea__c();
        GM_ClickAreaQuestions = new Map< Id, RedSurveyQuestion__c >();
        GV_DrawNewArea = true;
        GV_EditArea = true;
    }

    public void saveArea(){
        if( GV_DrawNewArea ){
            GO_ImageArea.PictureOfSuccess__c = GV_ImageId;
            GO_ImageArea.CenterX__c = GV_X;
            GO_ImageArea.CenterY__c = GV_Y;
            
            insert( GO_ImageArea );
        }else{
            update( GO_ImageArea );
        }
        
        loadImage();
        
        GV_DrawNewArea = false;
        GV_EditArea = false;
    }
    
    public void cancelArea(){
        GV_DrawNewArea = false;
        GV_EditArea = false;
    }
    
    public void updateCoordinates(){
    
    }
    
    public void chooseArea(){
        GO_ImageArea = ( PofSClickArea__c ) fsFA_Tools.getRecord( GV_ImageAreaId );
        GM_ClickAreaQuestions = fsFA_Tools.getRecordsFlat( 'RedSurveyQuestion__c', 'ClickArea__c', GV_ImageAreaId );

        GV_EditArea = true;
    }
    
    public void deleteArea(){
        delete( GO_ImageArea );
        loadImage();
        GV_EditArea = false;
    }
    
    public void newQuestion(){
        RedSurveyQuestion__c LO_NewQuestion = new RedSurveyQuestion__c(
        	ClickArea__c = GO_ImageArea.Id
        );
        GM_ClickAreaQuestions.put( null, LO_NewQuestion );
    }
    
    public void deleteQuestion(){
        GM_ClickAreaQuestions.remove( GO_ClickAreaQuestion.Id );
    }
    
    private Id getDocumentFolder( String PV_FolderName ){
        if( GM_DocumentFolders == null ){
            GM_DocumentFolders = new Map< String, Id >();
        }
        if( !GM_DocumentFolders.containsKey( PV_FolderName ) ){
            Id LV_DocumentFolderId = Id.valueOf( PofSDocumentsFolder__c.getInstance( PV_FolderName ).FolderId__c );
            GM_DocumentFolders.put( PV_FolderName, LV_DocumentFolderId );
        }
        return GM_DocumentFolders.get( PV_FolderName );
    }
    
}