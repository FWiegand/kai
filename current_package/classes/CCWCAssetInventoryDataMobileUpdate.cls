/**
* @author		Development (AB)
* 				H&W Consult GmbH
* 				Bahnhofstr. 3
* 				21244 Buchholz i.d.N.
* 				http://www.hundw.com
*
* @description	The class calls CCWCAssetInventoryDataRequest.
*				Note:
*				The checkbox Mobile_Update_Indicator__c is true if the record by a mobile application has changed and is to be transferred to SAP. 
* 				After transmission, the checkbox is set to false.
*
* @date			17.01.2014
*
*/

public with sharing class CCWCAssetInventoryDataMobileUpdate {

	/**
	* @description	call webservice class CCWCAssetInventoryDataRequest 
	* @param		newItems SCInstalledBase__c record
	* @param 		async asyn future handler ist running
	* @param		test test modus without SAP call
	*/
	
    public void sendCallout(SCInstalledBase__c [] newItems, boolean async, boolean test){  
    	if (newItems[0].Mobile_Update_Indicator__c == true){
    		CCWCAssetInventoryDataRequest.callout(newItems[0].Id, async, test);
    		SCInstalledBase__c installedbase = [Select Mobile_Update_Indicator__c, Id From SCInstalledBase__c Where Id =:newItems[0].Id];
    		installedbase.Mobile_Update_Indicator__c = false;
    		update installedbase;
    	}
    } 	
}