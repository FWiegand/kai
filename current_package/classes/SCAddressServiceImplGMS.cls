/*
 * @(#)SCAddressServiceImplGMS.cls 
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Implements the address validation service that accesses 
 * the GMS internal web service. This service is activated
 * depending on the country by SCAddressValidation.
 *
 *    SCAddressValidation v = new SCAddressValidation();
 *    List<AvsAddress> foundaddr;
 *    foundaddr = v.check('de', 'Schulstr. 19 Paderborn');
 *    System.debug('result:' + foundaddr);
 */
public class SCAddressServiceImplGMS implements SCAddressService
{
    /*
     * Prepare the web servcie call by filling the login header and
     * @return instance of the web service wrapper
     */
    private AvsService.AddressValidationSOAP prepare()
    {
        // prepare the authentication details (we are using header authentication)          
        AvsService.AddressValidationSOAP service = new AvsService.AddressValidationSOAP();
        // reserved
        //TODO: AvsServcie.HeaderLoginType login = new AvsServcie.HeaderLoginType();
        //TODO: login.username = 'xxxxxx';
        //TODO: login.password = 'yyyyyy';
        //TODO: move to custom settings
        
        //TODO: service.HeaderLogin = login;
        
        return service;
    } // prepare
    
    /**
     * Validate the address and return the result set
     * @param country country to search in
     * @param query search for the address string
     * @return the validated address result
     */
    public AvsResult check(AvsAddress addr)
    {
        AvsResult result = new AvsResult();
        try
        {
            // The avs service uses a single linie query string to validate the address
            String query = prepareQuery(addr);
            // continue only if an address is available
            if(query != null && query.length() > 0 && addr != null && addr.country.length() > 0)
            {
                // prepare the authentication details (we are using header authentication)          
                AvsService.AddressValidationSOAP service = prepare();
                // call the web service
                AvsService.Address[] serviceresult = service.SearchAddress(UserInfo.getOrganizationId(), addr.country, addr.matchInfo, query);
                if(serviceresult != null)
                {
                    for(AvsService.Address i : serviceresult)
                    {
                        // transfer the result into the return object
                        AvsAddress item = new AvsAddress();
                        item.country        = addr.country; // i.country;                  
                        item.countryState   = ''; // not supported 
                        item.county         = ''; // not supported          
                        item.postalcode     = i.PC; 
                        item.city           = i.city;
                        item.district       = i.district;
                        item.street         = i.street;
                        item.housenumber    = i.houseNumber;
                        item.nofrom         = i.noFrom;
                        item.noto           = i.noTo;
                        item.geoX           = i.geoX; 
                        item.geoY           = i.geoY; 
        
                        // Convert the status codes (for diagnosis only - can be removed in product environment)  
                        item.matchInfo = ''; // reserved i.matchType;
                        
                        // add the found address to the result list
                        result.items.add(item);
                    }
                }
                // now set the status values
                if(result.items.size() == 1)
                {
                    result.status = AvsResult.STATUS_EXACT;
                } 
                else if(result.items.size() > 1)
                {
                    result.status = AvsResult.STATUS_MULTI;
                }
                else
                {
                    result.status = AvsResult.STATUS_BLANK;
                }
                // set the originial status info of the service
                result.statusInfo = ''; 
                
            } // if(query != null..
            else
            {
                result.status = AvsResult.STATUS_BLANK;
                result.statusInfo = 'no address data to validate for the country ' + addr.country;
            }
        }
        catch(Exception e)
        {
            result.status = AvsResult.STATUS_ERROR;
            result.statusInfo = '' + e;
        }
        return result;
    } // check

    /**
     * Determines the longitude and latitude for an address.
     * Only required if the external address validation service 
     * does not deliver the geocodes in the addres validateion.
     * 
     * @param addr address object with all needed data
     * @return the address with the longitude and latitude
     */
    public AvsResult geocode(AvsAddress addr)
    {
        return check(addr);
        
        /*as the address validation already the geocodes  
        //TODO: to be implemented if required 
        addr.geox = 0;
        addr.geoy = 0;
        AvsResult result = new AvsResult();
        result.items.add(addr);
        result.status = AvsResult.STATUS_EXACT;
        result.statusinfo = 'not supported';
        
        return result;
*/    
}

       
    /*Concatenates the address to a query string that is used by the 
     * address validation service. 
     * @param addr the address object
     * @return the query string 
     */
    private String prepareQuery(AvsAddress addr)
    {
        String query = '';
        if (addr.city != null)
        {
            query += addr.city  + ' ';
        }
        if (addr.postalCode != null)
        {
            query += addr.postalCode + ' ';
        }
        if (addr.street != null)
        {
            query += addr.street + ' ';
        }
        if (addr.houseNumber != null)
        {
            query += addr.houseNumber + ' '; 
        }
        return query;
    }   
}