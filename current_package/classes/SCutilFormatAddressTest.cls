/*
 * @(#)SCutilFormatAddressTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class - checks the address formatting
 */
@isTest
private class SCutilFormatAddressTest
{

    static testMethod void getAddressFormatPositive1() 
    {
       SCHelperTestClass.createAccountObject('Customer', true);
       SCHelperTestClass.account.BillingDistrict__c = 'Center';
       SCHelperTestClass.account.BillingCountry__c = 'NL';
       String input = 'Moormanweg 6 <br> 9831 NK Aduard Center';
       String output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim();
       System.assertEquals(input, output); 
       
       SCHelperTestClass.account.BillingCountry__c = 'GB';
       SCHelperTestClass.account.BillingFloor__c = '3';
       SCHelperTestClass.account.BillingFlatNo__c = 'B';
       input = '6 Moormanweg, 3/B <br> Aduard 9831 NK';
       output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim(); 
       //System.debug('### formatAddress output: ' +output);
       System.assertEquals(input, output); 
       
       SCHelperTestClass.account.BillingFloor__c = null;
       SCHelperTestClass.account.BillingFlatNo__c = null;
       input = '6 Moormanweg <br> Aduard 9831 NK';
       output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim(); 
       //System.debug('### formatAddress output: ' +output);
       System.assertEquals(input, output); 
       
       SCHelperTestClass.account.BillingCountry__c = 'DE';
       input = 'Moormanweg 6 <br> 9831 NK Aduard Center';
       output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim(); 
       System.assertEquals(input, output); 
       
       SCHelperTestClass.account.BillingCountry__c = 'DK';
       input = 'Moormanweg 6 <br> 9831 NK Aduard Center';
       output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim(); 
       System.assertEquals(input, output); 
       
       SCHelperTestClass.account.BillingCountry__c = 'AT';
       SCHelperTestClass.account.BillingExtension__c = 'a';
       SCHelperTestClass.account.BillingFloor__c = '3';
       SCHelperTestClass.account.BillingFlatNo__c = 'B';
       input = 'Moormanweg 6a 3/B <br> 9831 NK Aduard Center';
       output = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim(); 
       System.assertEquals(input, output); 
    }

    static testMethod void getAddressFormatNeagtiv1() 
    {
       SCHelperTestClass.createAccountObject('Customer', true);
       SCHelperTestClass.account.BillingCountry__c = 'NL';
       String input = 'Moormanweg 6 <br> 9831NK Aduard';
       String outputNL = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim();
       SCHelperTestClass.account.BillingCountry__c = 'GB';
       String outputGB = SCutilFormatAddress.formatAddress(SCHelperTestClass.account).trim();
       System.assertNotEquals(outputNL , outputGB );
       
       SCutilFormatAddress.add(null, null);
       SCutilFormatAddress.add(null, 'aa');
       SCutilFormatAddress.add('aa', null);
        
    }


    static testMethod void getAddressFormatPositive2() 
    {
        SCOrderRole__c orderRole = new SCOrderRole__c
             (
                 Country__c = 'DE',
                 PostalCode__c = '33818',
                 City__c = 'Leopoldshöhe',
                 District__c = 'Center',
                 HouseNo__c = '15',
                 Street__c = 'Südstrasse',
                 LocaleSidKey__c = 'de_DE',
                 GeoY__c = 52.036588,
                 GeoX__c = 8.693304,
                 Salutation__c = 'Herr',
                 Name1__c = 'Person Test User'
            );

       orderRole.Country__c = 'NL';
       String input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       String output = SCutilFormatAddress.formatAddress(orderRole).trim();
       System.assertEquals(input, output);
        
       orderRole.Country__c = 'GB';
       orderRole.Floor__c = '3';
       orderRole.FlatNo__c = 'B';
       input = '15 Südstrasse, 3/B <br> Leopoldshöhe 33818';
       output = SCutilFormatAddress.formatAddress(orderRole).trim(); 
       System.assertEquals(input, output); 
       
       System.debug(output);
       orderRole.Country__c = 'DE';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(orderRole).trim(); 
       System.assertEquals(input, output); 
       
       System.debug(output);
       orderRole.Country__c = 'DK';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(orderRole).trim(); 
       System.assertEquals(input, output); 
       
       System.debug(output);
       orderRole.Country__c = 'AT';
       orderRole.Extension__c = 'a';
       orderRole.Floor__c = '3';
       orderRole.FlatNo__c = 'B';
       input = 'Südstrasse 15a 3/B <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(orderRole).trim(); 
       System.assertEquals(input, output); 
    }
    
    static testMethod void getAddressFormatInstalledBase()
    {
        SCInstalledBaseLocation__c loc = new SCInstalledBaseLocation__c
             (
                 Country__c = 'DE',
                 County__c  = 'NRW',
                 PostalCode__c = '33818',
                 District__c = 'Center',
                 City__c = 'Leopoldshöhe',
                 Street__c = 'Südstrasse',
                 HouseNo__c = '15',
                 GeoY__c = 52.036588,
                 GeoX__c = 8.693304,
                 LocName__c = 'Bulding A'
            );
    
       loc.Country__c = 'NL';
       String input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       String output = SCutilFormatAddress.formatAddress(loc).trim();
       System.assertEquals(input, output); 
    
       loc.Country__c = 'GB';
       loc.Floor__c = '3';
       loc.FlatNo__c = 'B';
       input = '15 Südstrasse, 3/B <br> Leopoldshöhe 33818';
       output = SCutilFormatAddress.formatAddress(loc).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
    
       loc.Country__c = 'DE';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(loc).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
    
       loc.Country__c = 'DK';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(loc).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
    
       loc.Country__c = 'AT';
       loc.Extension__c = 'a';
       loc.Floor__c = '3';
       loc.FlatNo__c = 'B';
       input = 'Südstrasse 15a 3/B <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(loc).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
   } 
 
    static testMethod void getStreetAndHousenoFormatPositive() 
    {
        SCOrderRole__c orderRole = new SCOrderRole__c
             (
                 Country__c = 'DE',
                 PostalCode__c = '33818',
                 City__c = 'Leopoldshöhe',
                 District__c = 'Center',
                 HouseNo__c = '15',
                 Street__c = 'Südstrasse',
                 LocaleSidKey__c = 'de_DE',
                 GeoY__c = 52.036588,
                 GeoX__c = 8.693304,
                 Salutation__c = 'Herr',
                 Name1__c = 'Person Test User'
            );

       orderRole.Country__c = 'NL';
       String input = 'Südstrasse 15';
       String output = SCutilFormatAddress.formatStreetAndHouseno(orderRole).trim();
       System.assertEquals(input, output); 
       
       orderRole.Country__c = 'GB';
       orderRole.Floor__c = '3';
       orderRole.FlatNo__c = 'B';
       input = '15 Südstrasse, 3/B';
       output = SCutilFormatAddress.formatStreetAndHouseno(orderRole).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       orderRole.Country__c = 'DE';
       input = 'Südstrasse 15';
       output = SCutilFormatAddress.formatStreetAndHouseno(orderRole).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       orderRole.Country__c = 'DK';
       input = 'Südstrasse 15';
       output = SCutilFormatAddress.formatStreetAndHouseno(orderRole).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       orderRole.Country__c = 'AT';
       orderRole.Extension__c = 'a';
       orderRole.Floor__c = '3';
       orderRole.FlatNo__c = 'B';
       input = 'Südstrasse 15a 3/B';
       output = SCutilFormatAddress.formatStreetAndHouseno(orderRole).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
    }
        
    static testMethod void getAddressFormatPositive3() 
    {
        SCResourceAssignment__c res = new SCResourceAssignment__c 
        (
            Country__c = 'DE',
            PostalCode__c = '33818',
            City__c = 'Leopoldshöhe',
            District__c = 'Center',
            HouseNo__c = '15',
            Street__c = 'Südstrasse',
            GeoY__c = 52.036588,
            GeoX__c = 8.693304
        );
        
       res.Country__c = 'NL';
       String input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       String output = SCutilFormatAddress.formatAddress(res).trim();
       System.assertEquals(input, output); 
       
       res.Country__c = 'GB';
       res.Floor__c = '3';
       res.FlatNo__c = 'B';
       input = '15 Südstrasse, 3/B <br> Leopoldshöhe 33818';
       output = SCutilFormatAddress.formatAddress(res).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       res.Country__c = 'DE';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(res).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       res.Country__c = 'DK';
       input = 'Südstrasse 15 <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(res).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
       
       res.Country__c = 'AT';
       res.Extension__c = 'a';
       res.Floor__c = '3';
       res.FlatNo__c = 'B';
       input = 'Südstrasse 15a 3/B <br> 33818 Leopoldshöhe Center';
       output = SCutilFormatAddress.formatAddress(res).trim(); 
       System.assertEquals(input, output); 
       System.debug(output);
    }
}