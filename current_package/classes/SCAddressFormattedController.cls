/*
 * @(#)SCAddressFormattedController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller formats the address string.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCAddressFormattedController
{
    // Input parameter from the component
    public String objectId { get; set; }
    public String objectType { get; set; }   
    public Account accObj { get; set; }   
    public SCInstalledBaseLocation__c locObj { get; set; }   
    public Boolean lineBreak { get; set; }   
    
    // Output strings (county specific)
    public String line1 { get; set; }
    public String line2 { get; set; }
    
    // Application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance(); 
    public String defCountry = appSettings.DEFAULT_COUNTRY__c;
    
    public SCAddressFormattedController()
    {   
        lineBreak = true;
    }
       
    public String getAddress()
    {
        line1 = '';
        line2 = '';
            
        System.debug('#### getAddress(): objectId -> ' + objectId);
        System.debug('#### getAddress(): accObj -> ' + accObj);
        System.debug('#### getAddress(): locObj -> ' + locObj);
        System.debug('#### getAddress(): objectType -> ' + objectType);
        if (((null != objectId) || (null != accObj) || (null != locObj)) && (null != objectType))
        {   
            String street = '';
            String postal = '';
            String house = '';
            String city = '';
            String district = '';
            String extension = '';
            String floor = '';
            String flatNo = '';
        
            if(objectType == 'account')
            {
                List<Account> acc;
                if (null != accObj)
                {
                    acc = new List<Account>();
                    acc.add(accObj);
                }
                else
                {
                    acc = [ Select BillingStreet, 
                                   BillingPostalCode, 
                                   BillingHouseNo__c, 
                                   BillingCity,
                                   BillingDistrict__c,
                                   BillingExtension__c, 
                                   BillingFlatNo__c, 
                                   BillingFloor__c 
                              From Account 
                             Where Id = : objectId
                             Limit 1 ];
                }
                
                if(acc.size() > 0)
                {
                    if(acc[0].BillingStreet != null)
                        street     = acc[0].BillingStreet;
                    if(acc[0].BillingPostalCode != null)
                        postal     = acc[0].BillingPostalCode;
                    if(acc[0].BillingHouseNo__c != null)
                        house      = acc[0].BillingHouseNo__c;
                    if(acc[0].BillingCity != null)
                        city       = acc[0].BillingCity;
                    if(acc[0].BillingDistrict__c != null)
                        district   = acc[0].BillingDistrict__c;
                    if(acc[0].BillingExtension__c != null)
                        extension  = acc[0].BillingExtension__c;
                    if(acc[0].BillingFlatNo__c != null)
                        flatNo     = acc[0].BillingFlatNo__c;
                    if(acc[0].BillingFloor__c != null)
                        floor      = acc[0].BillingFloor__c;
                }
            }
            else if(objectType == 'location')
            {
                List<SCInstalledBaseLocation__c> loc;
                if (null != locObj)
                {
                    loc = new List<SCInstalledBaseLocation__c>();
                    loc.add(locObj);
                }
                else
                {
                    loc = [ Select Street__c, 
                                   PostalCode__c, 
                                   HouseNo__c, 
                                   City__c,
                                   District__c,
                                   Extension__c, 
                                   FlatNo__c, 
                                   Floor__c 
                              From SCInstalledBaseLocation__c
                             Where Id = : objectId
                             Limit 1 ];
                }
                
                if(loc.size() > 0)
                {
                    if(loc[0].Street__c != null)
                        street     = loc[0].Street__c;
                    if(loc[0].PostalCode__c != null)
                        postal     = loc[0].PostalCode__c;
                    if(loc[0].HouseNo__c != null)
                        house      = loc[0].HouseNo__c;
                    if(loc[0].City__c != null)
                        city       = loc[0].City__c;
                    if(loc[0].District__c != null)
                        district   = loc[0].District__c;
                    if(loc[0].Extension__c != null)
                        extension  = loc[0].Extension__c;
                    if(loc[0].FlatNo__c != null)
                        flatNo     = loc[0].FlatNo__c;
                    if(loc[0].Floor__c != null)
                        floor      = loc[0].Floor__c;
                }            
            }
            else if(objectType == 'orderrole')
            {
                List<SCOrderRole__c> role = [ Select Street__c, 
                                                     PostalCode__c, 
                                                     HouseNo__c, 
                                                     City__c,
                                                     District__c,
                                                     Extension__c, 
                                                     FlatNo__c, 
                                                     Floor__c 
                                              From SCOrderRole__c
                                              Where Id = : objectId
                                              Limit 1 ];
                
                if(role.size() > 0)
                {
                    if(role[0].Street__c != null)
                        street     = role[0].Street__c;
                    if(role[0].PostalCode__c != null)
                        postal     = role[0].PostalCode__c;
                    if(role[0].HouseNo__c != null)
                        house      = role[0].HouseNo__c;
                    if(role[0].City__c != null)
                        city       = role[0].City__c;
                    if(role[0].District__c != null)
                        district   = role[0].District__c;
                    if(role[0].Extension__c != null)
                        extension  = role[0].Extension__c;
                    if(role[0].FlatNo__c != null)
                        flatNo     = role[0].FlatNo__c;
                    if(role[0].Floor__c != null)
                        floor      = role[0].Floor__c;
                }  
            }
            else if(objectType == 'orderitem')
            {
                List<SCOrderItem__c> item = [ Select InstalledBase__r.InstalledBaseLocation__r.Street__c, 
                                                     InstalledBase__r.InstalledBaseLocation__r.PostalCode__c, 
                                                     InstalledBase__r.InstalledBaseLocation__r.HouseNo__c, 
                                                     InstalledBase__r.InstalledBaseLocation__r.City__c,
                                                     InstalledBase__r.InstalledBaseLocation__r.District__c,
                                                     InstalledBase__r.InstalledBaseLocation__r.Extension__c, 
                                                     InstalledBase__r.InstalledBaseLocation__r.FlatNo__c, 
                                                     InstalledBase__r.InstalledBaseLocation__r.Floor__c 
                                              From SCOrderItem__c
                                              Where Id = : objectId
                                              Limit 1 ];
                
                if(item.size() > 0)
                {
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.Street__c != null)
                        street     = item[0].InstalledBase__r.InstalledBaseLocation__r.Street__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.PostalCode__c != null)
                        postal     = item[0].InstalledBase__r.InstalledBaseLocation__r.PostalCode__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.HouseNo__c != null)
                        house      = item[0].InstalledBase__r.InstalledBaseLocation__r.HouseNo__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.City__c != null)
                        city       = item[0].InstalledBase__r.InstalledBaseLocation__r.City__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.District__c != null)    
                        district   = item[0].InstalledBase__r.InstalledBaseLocation__r.District__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.Extension__c != null) 
                        extension  = item[0].InstalledBase__r.InstalledBaseLocation__r.Extension__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.FlatNo__c != null)
                        flatNo     = item[0].InstalledBase__r.InstalledBaseLocation__r.FlatNo__c;
                    if(item[0].InstalledBase__r.InstalledBaseLocation__r.Floor__c != null)
                        floor      = item[0].InstalledBase__r.InstalledBaseLocation__r.Floor__c;
                }  
            }
        
            if(defCountry == 'DE')
            {
                line1 = street + ' ' + house;
                line2 = postal + ' ' + city + ' ' + district;
            }
            else if(defCountry == 'NL')
            {
                line1 = street + ' ' + house + ' ' + extension;
                line2 = postal + ' ' + city + ' ' + district;
            }
            else if(defCountry == 'GB')
            {
                line1 = house + ' ' + street;
                if (floor.length() > 0)
                {
                    line1 += ', ' + floor;
                }
                if (flatNo.length() > 0)
                {
                    line1 += '/' + flatNo;
                }
                line2 = city + ' ' + postal;
            }
            else if(defCountry == 'AT')
            {
                line1 = street + ' ' + house + extension;
                if (floor.length() > 0)
                {
                    line1 += ' ' + floor;
                }
                if (flatNo.length() > 0)
                {
                    line1 += '/' + flatNo;
                }
                line2 = postal + ' ' + city + ' ' + district;
            }
            else
            {
                line1 = street + ' ' + house;
                line2 = postal + ' ' + city + ' ' + district;
            }
        }
        
        return null;
        
    }
   
}