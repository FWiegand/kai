/*
 * @(#)CCWSSparePartPackageSentTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Tests for interface CCWSEquipmentChangeTest 
 */
@isTest
public with sharing class CCWSSparePartPackageSentTest {

   /*
	* Positive test
	*/
	@isTest 
	static void test1()
	{
		prepareTestData();
		
		Test.StartTest();
        
        
        CCWSSparePartPackageSent.CCWSSparePartPackageSentClass sparePartPackage = new CCWSSparePartPackageSent.CCWSSparePartPackageSentClass();
		
		CCWSSparePartPackageSent.MessageHeaderClass messageHeader = new CCWSSparePartPackageSent.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSSparePartPackageSent.PlainFieldsClass plainFields = new CCWSSparePartPackageSent.PlainFieldsClass();
        plainFields.PackageNumber = 'MH01';
        plainFields.Plant = '0359';
        plainFields.StorageLocation = 'SC01';
        plainFields.UnloadingPoint = '';
        plainFields.Status = 'CLOSED';
        
             
        CCWSSparePartPackageSent.PackageItem item1 = new CCWSSparePartPackageSent.PackageItem();
        item1.ItemID = '0001';
        item1.ReferenceNumber = 'MM-000000001';
        item1.MaterialDocumentNumber = '4926418623';
        item1.MaterialDocumentYear = '2012';
        item1.MaterialID = '151000001';
        item1.UnitOfMeasure = 'EA';
        item1.Quantity = '1.0';
        item1.ValuationType = 'ZNEU';
        item1.Batch = null;

        
        CCWSSparePartPackageSent.PackageItem item2 = new CCWSSparePartPackageSent.PackageItem();
        item2.ItemID = '0002';
        item2.ReferenceNumber = 'MM-000000002';
        item2.MaterialDocumentNumber = '4926418624';
        item2.MaterialDocumentYear = '2012';
        item2.MaterialID = '151000050';
        item2.UnitOfMeasure = 'EA';
        item2.Quantity = '4.0';
        item2.ValuationType = null;
        item2.Batch = null;

        
        sparePartPackage.MessageHeader = messageHeader;
        sparePartPackage.PlainFields =  plainFields;
        sparePartPackage.Item = new List<CCWSSparePartPackageSent.PackageItem>();    
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        
         
        sparePartPackage.Item.add(item1);
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        sparePartPackage.Item.add(item2);               
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        test.stopTest();
        
        
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c];
        System.debug('### Stock Items:' + stockItems.size());
        for(SCStockItem__c si : stockItems)
        {
        	System.debug('### Stock Item: Name: '+ si.Name + ' Stock: ' + si.Stock__r.Name + ' Plant: ' + si.Plant__c + ' Article ' 
        		+ si.Article__r.Name + ' Qty '+ si.Qty__c +' Selector '+ si.util_StockItemSelector__c );
        }
          
        // Item1: '0359-SC01-151000001-ZNEU'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =:  '0359-SC01-151000001-ZNEU' ];
        System.assertEquals(stockItems[0].Article__r.Name, '151000001');                                  
        
        // Item2: '0359-SC01-151000050-'
        stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c WHERE util_StockItemSelector__c =: '0359-SC01-151000050-' ];
        System.assertEquals(stockItems[0].Article__r.Name, '151000050'); 

        

                     
	}
	
	
   /*
	* Missing data test cases:
	* Case without article
	* Case with wrong quantity
	*/
	@isTest 
	static void test2()
	{
		prepareTestData();
		
		Test.StartTest();
        
        
        CCWSSparePartPackageSent.CCWSSparePartPackageSentClass sparePartPackage = new CCWSSparePartPackageSent.CCWSSparePartPackageSentClass();
		
		CCWSSparePartPackageSent.MessageHeaderClass messageHeader = new CCWSSparePartPackageSent.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSSparePartPackageSent.PlainFieldsClass plainFields = new CCWSSparePartPackageSent.PlainFieldsClass();
        plainFields.PackageNumber = 'MH01';
        plainFields.Plant = '0359';
        plainFields.StorageLocation = 'SC01';
        plainFields.UnloadingPoint = '';
        plainFields.Status = 'CLOSED';
        
             
        CCWSSparePartPackageSent.PackageItem item1 = new CCWSSparePartPackageSent.PackageItem();
        item1.ItemID = '0001';
        item1.MaterialDocumentNumber = '4926418623';
        item1.MaterialDocumentYear = '2012';
        item1.MaterialID = null; // wrong article
        item1.UnitOfMeasure = 'EA';
        item1.Quantity = '1.0';
        item1.ValuationType = 'ZNEU';
        item1.Batch = null;
        item1.ReferenceNumber = null;
        
        
        CCWSSparePartPackageSent.PackageItem item2 = new CCWSSparePartPackageSent.PackageItem();
        item2.ItemID = '0002';
        item2.MaterialDocumentNumber = '4926418624';
        item2.MaterialDocumentYear = '2012';
        item2.MaterialID = '151000050';
        item2.UnitOfMeasure = 'EA';
        item2.Quantity = 'four'; // wrong number
        item2.ValuationType = null;
        item2.Batch = null;
        item2.ReferenceNumber = null;
        
        
        sparePartPackage.MessageHeader = messageHeader;
        sparePartPackage.PlainFields =  plainFields;
        sparePartPackage.Item = new List<CCWSSparePartPackageSent.PackageItem>();    
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        
         
        sparePartPackage.Item.add(item1);
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        sparePartPackage.Item.add(item2);               
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        test.stopTest();
          
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c ];
        System.assertEquals(stockItems.size(), 0);                                                       
	}
	
	/*
	* Missing data test cases:
	* No Package Item 
	*/
	@isTest 
	static void test3()
	{
		prepareTestData();
		
		Test.StartTest();
        
        
        CCWSSparePartPackageSent.CCWSSparePartPackageSentClass sparePartPackage = new CCWSSparePartPackageSent.CCWSSparePartPackageSentClass();
		
		CCWSSparePartPackageSent.MessageHeaderClass messageHeader = new CCWSSparePartPackageSent.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSSparePartPackageSent.PlainFieldsClass plainFields = new CCWSSparePartPackageSent.PlainFieldsClass();
        plainFields.PackageNumber = 'MH01';
        plainFields.Plant = '0359';
        plainFields.StorageLocation = 'SC01';
        plainFields.UnloadingPoint = '';
        plainFields.Status = 'CLOSED';
        
             
        
        sparePartPackage.MessageHeader = messageHeader;
        sparePartPackage.PlainFields =  plainFields;
        sparePartPackage.Item = new List<CCWSSparePartPackageSent.PackageItem>();    
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        test.stopTest();
          
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c ];
        System.assertEquals(stockItems.size(), 0);                                  
        
                
	}
	
   /*
	* Failure test
	* stock not set
	*/
	@isTest 
	static void test4()
	{
		prepareTestData();
		
		Test.StartTest();
        
        
        CCWSSparePartPackageSent.CCWSSparePartPackageSentClass sparePartPackage = new CCWSSparePartPackageSent.CCWSSparePartPackageSentClass();
		
		CCWSSparePartPackageSent.MessageHeaderClass messageHeader = new CCWSSparePartPackageSent.MessageHeaderClass();
        messageHeader.MessageID = '50D01340996D0E10E10080000A62025A';
        messageHeader.MessageUUID = '50D01340-996D-0E10-E100-80000A62025A';
        
        CCWSSparePartPackageSent.PlainFieldsClass plainFields = new CCWSSparePartPackageSent.PlainFieldsClass();
        plainFields.PackageNumber = 'MH01';
        plainFields.Plant = '0359';
        plainFields.StorageLocation = null;
        plainFields.UnloadingPoint = '';
        plainFields.Status = 'CLOSED';
        
             
        CCWSSparePartPackageSent.PackageItem item1 = new CCWSSparePartPackageSent.PackageItem();
        item1.ItemID = '0001';
        item1.ReferenceNumber = 'MM-000000001';
        item1.MaterialDocumentNumber = '4926418623';
        item1.MaterialDocumentYear = '2012';
        item1.MaterialID = '151000001';
        item1.UnitOfMeasure = 'EA';
        item1.Quantity = '1.0';
        item1.ValuationType = 'ZNEU';
        item1.Batch = null;
        
        
        
        CCWSSparePartPackageSent.PackageItem item2 = new CCWSSparePartPackageSent.PackageItem();
        item2.ItemID = '0002';
        item2.ReferenceNumber = 'MM-000000002';
        item2.MaterialDocumentNumber = '4926418624';
        item2.MaterialDocumentYear = '2012';
        item2.MaterialID = '151000050';
        item2.UnitOfMeasure = 'EA';
        item2.Quantity = '4.0';
        item2.ValuationType = null;
        item2.Batch = null;

        
        sparePartPackage.MessageHeader = messageHeader;
        sparePartPackage.PlainFields =  plainFields;
        sparePartPackage.Item = new List<CCWSSparePartPackageSent.PackageItem>();    
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        
         
        sparePartPackage.Item.add(item1);
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        sparePartPackage.Item.add(item2);               
        CCWSSparePartPackageSent.transmit(sparePartPackage);
        test.stopTest();
          
          
        List<SCStockItem__c> stockItems = [SELECT ID, Name, Stock__r.Name, Plant__c, Article__r.Name, Qty__c, util_StockItemSelector__c, ValuationType__c FROM SCStockItem__c ];
        System.assertEquals(stockItems.size(), 0);                                  

                     
	}
	
	public static void prepareTestData()
	{
		CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();

        appSettings = SCApplicationSettings__c.getInstance();

	}

}