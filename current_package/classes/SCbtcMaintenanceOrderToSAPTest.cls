/*
 * @(#)SCbtcMaintenanceOrderToSAPTest.cls
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Note !!!
// ========
//@isTest(SeeAllData=true) not allowed by tests creating an order. Because a trigger SCOrder_AI_CallSapWebService calls the Web Service
//====================================
// CCWCOrderCreate if IFEnableTriggerOrderCreate__c of CCSetting is greater than 0. By production and qa data it is the case.
// Calling of Web services while testing causes an exception that aborts the test. So we will not get the test running over the 75 procent of 
// the code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@isTest
private class SCbtcMaintenanceOrderToSAPTest
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    
    public static ID createInstalledBaseAndCo()
    {
        SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
        System.assertNotEquals(null, appSettings);

        
        Boolean doUpsert = true;
        // initiate and create a brand
        Brand__c brand = new Brand__c(Name = 'GMS Test 1', Competitor__c = false);
        brand = CCWCTestBase.createBrandObject(brand, doUpsert);

        // initiate and create a price list
        SCPriceList__c priceList = new SCPriceList__c(Name = 'DE100', Country__c = 'DE');
        priceList = CCWCTestBase.createPricelist(appSettings, priceList, brand, doUpsert);

        // initiate and create a plant
        SCPlant__c plant = createTestPlant(doUpsert);
        
        // initiate and create stocks
        List<SCStock__c> stockList = createTestStocks(plant.Id, doUpsert);


        // initiate and create a calendar
        String currencyIsoCode = 'EUR';
        SCCalendar__c calendar = new SCCalendar__c(Name = 'GMS Test Cal', Country__c = 'DE', CurrencyIsoCode = currencyIsoCode);
        calendar = CCWCTestBase.createTestCalendar(calendar, doUpsert);

		SCArea__c area = new SCArea__c(Context__c  = 'Business Unit', Country__c = 'DE', CurrencyIsoCode = currencyIsoCode,Description__c = 'Test',
										DistanceZone__c = 'Standard');
		insert area;
		
		SCAreaItem__c areaItem = new SCAreaItem__c(Area__c = area.id,Country__c = 'DE', 
									 CurrencyIsoCode = currencyIsoCode,Description__c = 'Test', Prio__c = '1');
		insert areaItem;
											 
        // initiate and create a business unit
        SCBusinessUnit__c businessUnit = new SCBusinessUnit__c(Name = '0359', 
                                  Info__c = 'GMS test Dev',
                                  Stock__c = stockList[0].id,
                                  Calendar__c = calendar.id,
                                  Type__c = '5901',
                                  Operational__c = true,
                                  Area__c = area.id
                                 );
        businessUnit = CCWCTestBase.createTestBusinessUnit(businessUnit, doUpsert);


        // initiate and create an account
        Account account = new Account(LastName__c = 'Pietrzyk', FirstName__c = 'Jerzy', name = 'Jerzy Pietrzyk',
                                    CurrencyIsoCode = currencyIsoCode, Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER,
                                    TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX,
                                    TargetGroup__c = null, SubTargetGroup__c = null,
                                    BillingPostalCode = '33104',
                                    BillingCountry__c = 'DE',
                                    BillingStreet = 'Karl-Schurz-Str.',
                                    BillingHouseNo__c = '29',
                                    BillingCity = 'Paderborn',
                                    GeoX__c = -0.10345,
                                    GeoY__c = 51.49250,
                                    GeoApprox__c = false,
                                    ShippingPostalCode = '33104',
                                    ShippingCountry__c = 'DE',
                                    ShippingStreet__c = 'Karl-Schurz-Str.',
                                    ShippingHouseNo__c = '29',
                                    ShippingPostalCode__c = '33104',
                                    ShippingCity__c = 'Paderborn');
        
        String recordTypeName = 'Customer';
        account = CCWCTestBase.createAccountObject(appSettings, recordTypeName, account, priceList, doUpsert);

		SCAccountInfo__c ai = new SCAccountInfo__c(Account__c = account.id,
												   CompanyCode__c = 'CompanyCode',
												   DistributionChannel__c = 'DC',
												   Division__c = 'd',
												   SalesArea__c = 'sa',
												   SalesGroup__c = 'sg',
												   SalesOffice__c = 'so',
												   ServiceArea__c = 'sa',
												   Status__c = 's');
		insert ai;


        List<SCArticle__c> articleList = CCWCTestBase.createTestArticles(appSettings, doUpsert);
    
        // initiate and create a product model
        SCProductModel__c prodModel = new SCProductModel__c(Brand__c = brand.Id, 
                                          Country__c = 'DE', 
                                          UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                          UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 
                                          Group__c = '268', 
                                          Name = 'VAM climaVAIR [026]',  
                                          Power__c = '550'
                                          );
        prodModel = CCWCTestBase.createProductModel(prodModel, doUpsert);

        
        // initiate and create and installed base
        List<String> installedBaseRoleList = new List<String>();
        installedBaseRoleList.add('Owner');

        SCInstalledBase__c installedBase = new SCInstalledBase__c(
                                            SerialNo__c = '123456',
                                            Status__c = 'active',
                                            Type__c = 'Appliance',
                                            ProductSkill__c = 'PS1',
                                            ShipTo__c = account.id,
                                            Stock__c = stockList[0].id);

        installedBase = CCWCTestBase.createInstalledBase(installedBase, account, installedBaseRoleList, 
                                                        articleList[0], prodModel, doUpsert);
        
        installedBase.Stock__c = stockList[0].id;
        update installedBase;       
        
        List<SCInstalledBase__c> ibList = [select id, ShipTo__c, MaintenanceEnabled__c, Stock__c, Stock__r.Plant__r.Name from SCInstalledBase__c where id = : installedBase.id];
        debug('installed base created: ' + ibList);

        return installedBase.id;
    }

    public static SCPlant__c createTestPlant(Boolean doUpsert)
    {
        SCPlant__c plant = new SCPlant__c(Name = '0359', Info__c = 'Test plant');

        if (doUpsert == true)
        {
            upsert plant;
        }
        return plant;
    } // createTestPlant

    public static List<SCStock__c> createTestStocks(Id plantId, Boolean doUpsert)
    {
        List<SCStock__c> stocks = new List<SCStock__c>();
        stocks.add(new SCStock__c(Name = '6598', Info__c = 'Test stock 1', Plant__c = plantId));
        stocks.add(new SCStock__c(Name = '6599', Info__c = 'Test stock 2', Plant__c = plantId));

        if (doUpsert == true)
        {
            upsert stocks;
        }
        List<SCStock__c> sList = [select Plant__r.Name from SCStock__c where id = : stocks[0].id];
        debug('created stocks: ' + sList);
        debug('plant  name: ' + sList[0].Plant__r.Name);
        return stocks;
    } // createTestStocks


    public static ID createMaintenancePlan(List<SCPriceList__c> priceListList)
    {
        Integer interval = 15; // for the compatiblity with the previous test functions
        return createMaintenancePlan(priceListList, interval);  
    }

    public static ID createMaintenancePlan(List<SCPriceList__c> priceListList, Integer interval)
    {
        // Create price lists

        for(Integer i = 0; i < 5; i++)
        {
            priceListList.add(new SCPriceList__c(Name = 'DE10' + i, Info__c = 'Test DE10' + i));
        }
        insert priceListList; 

        SCMaintenancePlan__c mp = new SCMaintenancePlan__c(Description__c = 'Test', Interval__c = interval, MaintenanceActionDesc__c = 'Action', 
                                    MaintenanceAfterDue__c = 10, MaintenanceBeforeDue__c = 10, MaintenanceType__c = 'SANITATION',
                                    OrderCreationLeadtime__c = 30, OrderPrio__c = '2101',OrderType__c = '5707', 
                                    Pricelist1__c = priceListList[0].id, Pricelist2__c = priceListList[1].id, Pricelist3__c = priceListList[2].id, 
                                    Pricelist4__c = priceListList[3].id,
                                    PriceListDefault__c = priceListList[4].id);
        insert mp;
        return mp.id;
    }
    
    static testMethod void createOrderPositive1()
    {
        Test.StartTest();
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        
        ID installedBaseId = createInstalledBaseAndCo();
        System.assertNotEquals(null, installedBaseId);
        List<SCPriceList__c> priceListList = new List<SCPriceList__c>();
        ID maintenancePlanId = createMaintenancePlan(priceListList);        
        System.assertNotEquals(null, maintenancePlanId);
        
        SCMaintenance__c m = new SCMaintenance__c(MaintenanceFirstDate__c = Date.newInstance(2013, 4, 1), MaintenanceLastDate__c = null, 
                             InstalledBase__c = installedBaseId, MaintenancePlan__c = maintenancePlanId,
                             Status__c = 'active', SuspensionStartDay__c = null, SuspensionStartMonth__c = null, SuspensionEndDay__c = null, 
                             SuspensionEndMonth__c = null);
        insert m;
        debug('input maintenance: ' + m);
        upsert m;

        Integer max_cycles = 1; 
        String mode = 'trace';
        SCbtcMaintenanceDueDateSet.syncCalculateMaintenance(m.id, max_cycles, mode);
        
        
        List<SCInstalledBase__c> ibList = [select id, ShipTo__c, MaintenanceEnabled__c, Stock__c, Stock__r.Plant__r.Name from SCInstalledBase__c where id = : installedBaseId];
        debug('installed base after creating maintenance: ' + ibList);
        debug('plantName2: ' + ibList[0].Stock__r.Plant__r.Name);

        List<SCMaintenance__c> mList = [select MaintenancePlan__r.OrderCreationLeadtime__c, OrderCreationDate__c, InstalledBase__r.ShipTo__c, MaintenanceDateProposal__c, MaintenanceFirstDate__c, Order__c, InstalledBase__r.MaintenanceEnabled__c, Status__c, id from SCMaintenance__c where id = : m.id];
        debug('maintenances with condition fields:' + mList);

		
        SCbtcMaintenanceOrderCreate.syncCreateOrder(m.id);

        List<SCOrder__c> oList = [select id from SCOrder__c where Maintenance__c = : m.id];
        System.assertEquals(true, olist.size() > 0);        
        Test.StopTest();
    }

	static testMethod void canCreateOrder()
	{
        Test.StartTest();
        CCWSUtil u = new CCWSUtil();
        SCApplicationSettings__c appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);
        CCSettings__c ccSettings = new CCSettings__c();
        if(seeAllData)
        {
            ccSettings = CCSettings__c.getInstance();
        }
        CCWCTestBase.setCCSettings(ccSettings, seeAllData);
        
        CCWCTestBase.createDomsForOrderCreation(seeAllData);
        
        ID installedBaseId = createInstalledBaseAndCo();
        System.assertNotEquals(null, installedBaseId);
        List<SCPriceList__c> priceListList = new List<SCPriceList__c>();
        ID maintenancePlanId = createMaintenancePlan(priceListList);        
        System.assertNotEquals(null, maintenancePlanId);
        
        SCMaintenance__c m = new SCMaintenance__c(MaintenanceFirstDate__c = Date.newInstance(2013, 4, 1), MaintenanceLastDate__c = null, 
                             InstalledBase__c = installedBaseId, MaintenancePlan__c = maintenancePlanId,
                             Status__c = 'active', SuspensionStartDay__c = null, SuspensionStartMonth__c = null, SuspensionEndDay__c = null, 
                             SuspensionEndMonth__c = null);
        insert m;
        debug('input maintenance: ' + m);
        upsert m;

		Test.stopTest();
	} 

	static testMethod void getBatchProcessId()
	{
		createOrderPositive1();
		SCbtcMaintenanceOrderToSAP moc = new SCbtcMaintenanceOrderToSAP(0, 'no trace');
		moc.getBatchProcessId();
	} 

	static testMethod void asyncTransferAll()
	{
		createOrderPositive1();
		SCbtcMaintenanceOrderToSAP.asyncTransferAll(0, 'no trace');
	} 

	static testMethod void asyncTransferAllExtraCondition()
	{
		createOrderPositive1();
		SCbtcMaintenanceOrderToSAP.asyncTransferAll(0, 'no trace', 'Status__c = \'no active\'');
	}
	
	static testMethod void asyncTransfer()
	{
		createOrderPositive1();
		SCbtcMaintenanceOrderToSAP.asyncTransfer(null, 0, 'no trace');
	}

	static testMethod void syncTransfer()
	{
		SCbtcMaintenanceOrderToSAP.syncTransfer(null, 0, 'no trace');
	}


    private static void debug(String text) 
    {
        System.debug('###...................' + text);
    }

}