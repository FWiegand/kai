/*
 * @(#)SCBase.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCBase 
{
    public static Map<String, String> mapPickListTexts = new Map<String, String>();
    
    public class IllegalParameterException extends Exception
    {
    }
    
    /**
     * Format to a valid SOQL date string usable in dynamic queries.
     *
     * Unfortunately you can't compare two Date fields directly in dynamic
     * SOQL since the field containing the date will be expanded to a 
     * datetime.
     *
     * @param datetime value
     * @return date formatted as yyyy-MM-dd
     */
    public static String formatSOQLDate(Datetime dt)
    {
        String dtString = '';
        
        dtString = dt.format('yyyy-MM-dd');
        
        return dtString;
    }
    
    /**
     * Format to a valid SOQL date string usable in dynamic queries.
     *
     * @param datetime value
     * @return date formatted as HH:mm
     */
    public static String formatSOQLTime(Datetime dt)
    {
        String dtString = '';
        
        dtString = dt.format('HH:mm');
        
        return dtString;
    }
    
    /**
     * Format date to valid dbase date string.
     *
     * @param date value
     * @return date formatted as yyyyMMdd
     */
    public static String formatDBaseDate(DateTime dt)
    {
        String dtString = '';
        
        dtString = dt.format('yyyyMMddHHmmss');
        
        return dtString;
    }
    

    /**
     * Format start and end timestamp to a simplified string
     *
     * @param dtStart    the start date/time or null
     * @param dtEnd      the end date/ime or null
     * @return date formatted as yyyyMMdd HH:mm - HH:mm
     */
    public static String formatInterval(DateTime dtStart, DateTime dtEnd)
    {
        if(dtStart != null && dtEnd != null)
        {
            Date dStart = dtStart.date();
            Date dEnd   = dtEnd.date();
            if(dStart == dEnd)
            {
                // if both are on the same day - the date is only formatted once
                return dStart.Format() + ' ' + dtStart.format('HH:mm') +  '-' + dtEnd.format('HH:mm'); 
            }
            else
            {    // on multiple days both date values are to be formatted
                return dStart.Format() + ' ' + dtStart.format('HH:mm') +  '-' + dEnd.Format() + ' ' + dtEnd.format('HH:mm'); 
            }
        }
        if(dtStart != null)
        {
            Date dStart = dtStart.date();
            return dStart.Format() + ' ' + dtStart.format('HH:mm');
        }
        if(dtEnd != null)
        {
            Date dEnd = dtEnd.date();
            return dEnd.Format() + ' ' + dtEnd.format('HH:mm'); 
        }
        return '';
    }

    /**
     * Format start and end date to a simplified string
     *
     * @param dtStart    the start date or null
     * @param dtEnd      the end date or null
     * @return date formatted as dd.MM.yyyy or dd.MM.yyyy - dd.MM.yyyyy
     */
    public static String formatDateInterval(Date dtStart, Date dtEnd)
    {
        if (dtStart != null && dtEnd != null)
        {
            if (dtStart.isSameDay(dtEnd))
            {
                // if both are on the same day - the date is only formatted once
                return dtStart.format(); 
            }
            else
            {    // on multiple days both date values are to be formatted
                return dtStart.format() +  ' - ' + dtEnd.format(); 
            }
        }
        if (dtStart != null)
        {
            return dtStart.format();
        }
        if (dtEnd != null)
        {
            return dtEnd.format(); 
        }
        return '';
    }
    
    /**
     * Format date to valid SOQL date string usable in dynamic queries.
     *
     * @param date value
     * @return date formatted as yyyy-MM-dd
     */
    public static String formatSOQLDate(Date dt)
    {
        Datetime dt2 = Datetime.newInstance(dt.year(), dt.month(), dt.day());
        return formatSOQLDate(dt2);
    }
    
    /**
     * Format date to valid dbase date string.
     *
     * @param date value
     * @return date formatted as yyyyMMdd
     */
    public static String formatDBaseDate(Date dt)
    {
        Datetime dt2 = Datetime.newInstance(dt.year(), dt.month(), dt.day());
        return formatDBaseDate(dt2);
    }

    /**
     * Parse a date time string into a datetime object
     *
     * @param dt    date time as GMT string ('yyyymmddhhmmss')
     */
    public static DateTime parseDateTime(String dt)
    {
        if (dt.length() != 14)
        {
            throw new IllegalParameterException('Cannot parse ' + dt + ' into DateTime');
        }
        
        String year = dt.subString(0, 4);
        String month = dt.subString(4, 6);
        String day = dt.subString(6, 8);
        String hour = dt.subString(8, 10);
        String minute = dt.subString(10, 12);
        String second = dt.subString(12, 14);
        
        return DateTime.newInstance(Integer.valueOf(year), Integer.valueOf(month), 
            Integer.valueOf(day), Integer.valueOf(hour), 
            Integer.valueOf(minute), Integer.valueOf(second));
    }

    /**
     * Parse a date time string into a datetime object
     *
     * @param dt    date time as GMT string ('yyyymmddhhmmss')
     */
    public static DateTime parseDateTimeGMT(String dt)
    {
        if (dt.length() != 14)
        {
            throw new IllegalParameterException('Cannot parse ' + dt + ' into DateTime');
        }
        
        String year = dt.subString(0, 4);
        String month = dt.subString(4, 6);
        String day = dt.subString(6, 8);
        String hour = dt.subString(8, 10);
        String minute = dt.subString(10, 12);
        String second = dt.subString(12, 14);
        
        return DateTime.newInstanceGmt(Integer.valueOf(year), Integer.valueOf(month), 
            Integer.valueOf(day), Integer.valueOf(hour), 
            Integer.valueOf(minute), Integer.valueOf(second));
    }
       
    /**
     * Parse a date time string into a datetime object
     *
     * @param dt    date time as string ('yyyymmddhhmmss')
     */
    public static Date parseDate(String dt)
    {
        if (dt.length() < 8)
        {
            throw new IllegalParameterException('Cannot parse ' + dt + ' into Date');
        }
        
        String year = dt.subString(0, 4);
        String month = dt.subString(4, 6);
        String day = dt.subString(6, 8);
        
        return Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), 
            Integer.valueOf(day));
    }
       
    /**
     * Parse a date time string into a datetime object
     *
     * @param dt    date time as string ('yyyymmddhhmmss')
     */
    public static Time parseTime(String dt)
    {
        if (dt.length() != 14)
        {
            throw new IllegalParameterException('Cannot parse ' + dt + ' into Time');
        }
        
        String hour = dt.subString(8, 10);
        String minute = dt.subString(10, 12);
        String second = dt.subString(12, 14);
        
        return Time.newInstance(Integer.valueOf(hour), 
            Integer.valueOf(minute), Integer.valueOf(second), 0);
    }
       
    /**
     * Convert the given currency and value to Euro
     *
     * This is the most important rule at least in Europe.
     *
     * @param value amount of money in other currency
     * @param currency alternative currency
     * @return converted value to Euro
     */
     public static Double convertCur2Euro(Double value, String cur)
     {
        //FIXME this method is a stub
        return value;       
     }
     
     /**
      * Convert the given currency and value to Euro
      *
      * This is the most important rule at least in Europe.
      *
      * @param value amount of money in other currency
      * @param fromCur current currency
      * @param toCur conversion target currency
      * @return converted value in target currency
      */
     public static Double convertCur(Double value, String fromCur, String toCur)
     {
        //FIXME this method is a stub
        return value;
     }

    /**
     * Checks if the given value is not null and if it has a length.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean isSet(String value)
    {
        return ((null != value) && (value.length() > 0));
    } // isSet
    
    /**
     * Retrieves the country of the current user for multi country support.
     * If the country is not set, the intern variable will be set to 'XX'.
     * 
     * @param    substitute    if true, the method retrieves XX if no country is set
     * @return   the country of the current user
     * 
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static String getUserCountry()
    {
        return getUserCountry(false);
    } // getUserCountry
    public static String getUserCountry(Boolean substitute)
    {
        System.debug(LoggingLevel.FINER, '##### getUserCountry()');
        // don't use a global static variable for appsettings in this class, 
        // it can cause not wished situations
        SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();
        String userCountry = applicationSettings.DEFAULT_COUNTRY__c;
        if (substitute && 
            ((null == userCountry) || (userCountry.length() == 0)))
        {
            userCountry = 'XX';
        } // if (substitute && ...
        return userCountry;
    } // initUserCountry

    /**
     * Splits a string of the format <key1>=<value1>|<key2>=<value2> and puts
     * the items in a map
     */
//    public static Map<String, String> stringToMap(String tupleList)
//    {
    public static Map<String, String> stringToMap(String tupleList, String tupleListDefault)
    {
        if(!SCBase.IsSet(tupleList))
        {
            tupleList = tupleListDefault;
        }
       
        Map<String, String> keyMap = new Map<String, String>();
        if (SCBase.isSet(tupleList))
        {
            List<String> tuples = tupleList.split('\\|');
            List<String> items;
            
            for (String curTuple :tuples)
            {
                items = curTuple.split('=');
                keyMap.put(items[0], (items.size() > 1) ? items[1] : null);
            }
        }
        return keyMap;
    } // stringToMap
    
    /**
     * Checks if a given value is a valid picklist value.
     * 
     * @param    field    sobject filed which is a picklist
     * @param    value    value to be checked
     * @return   true if the value is a valid picklist value
     * 
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static Boolean isPicklistValue(Schema.SObjectField field, String value)
    {
        // check if the governor limit is reached
        if (Limits.getPicklistDescribes() >= Limits.getLimitPicklistDescribes())
        {
            System.debug('#### SCBase.isPicklistValue(): Governor limit reached, return always true!');
            return true;
        }
        
        for (Schema.PicklistEntry entry :field.getDescribe().getPicklistValues())
        {
            if (entry.isActive() && entry.getValue().equals(value))
            {
                return true;
            }
        }
        return false;
    } // isPicklistValue

    /**
     * Retrieves the text for a picklist entry.
     *
     * @param    field    sobject filed which is a picklist
     * @param    key      the key of the picklist value
     * @return   the text of the picklist entry
     */
    public static String getPicklistText(Schema.SObjectField field, String key)
    {
        if (null == key)
        {
            return null;
        }
        
        if ('GMS_LONG_TEXT' == key)
        {
            return '12345678901234567890123456789012345678901234567890' + 
                   '12345678901234567890123456789012345678901234567890' + 
                   '12345678901234567890123456789012345678901234567890' + 
                   '12345678901234567890123456789012345678901234567890' + 
                   '12345678901234567890123456789012345678901234567890' + 
                   '12345678901234567890123456789012345678901234567890';
        }
        
        String text = mapPickListTexts.get(key);
        if (null != text)
        {
            return text;
        }
        
        // check if the governor limit is reached
        if (Limits.getPicklistDescribes() >= Limits.getLimitPicklistDescribes())
        {
            System.debug('#### SCBase.isPicklistValue(): Governor limit reached, return always the key!');
            return key;
        }
        
        for (Schema.PicklistEntry entry :field.getDescribe().getPicklistValues())
        {
            if (key.equals(entry.getValue()))
            {
                mapPickListTexts.put(key, entry.getLabel());
                return entry.getLabel();
            }
        }
        return key;
    } // getPicklistText
    
    /**
     * Helper function for reading a value of the given picklist field entry based on it label
     *
     * @param  obj        The sObject name
     * @param  fieldName  The field name
     * @param  entryLabel The label in the field
     * @return string Value of the picklist entry
     *
     * @author Sergey Utko <sutko@gms-online.de>
     */
    public static String getPicklistValue(String obj, String fieldName, String entryLabel)
    {
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjType = gd.get(obj); 
        Schema.DescribeSObjectResult r = sobjType.getDescribe(); 
        Map<String,Schema.SObjectField> M = r.fields.getMap(); 
        Schema.SObjectField field = M.get(fieldName);                      
        
        String valueOutput = '';
        
        Schema.DescribeFieldResult F = field.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        
        for(Schema.PicklistEntry i: P){
            if(i.getLabel() == entryLabel)
            {
                valueOutput = i.getValue();
            }
        }
        
        return valueOutput;
    }
   
    /**
     * Generates a list for a soql statement. ('id1','id2',..) 
     *
     * @param values a string of values with the MultiPicklist delimiter -> "id1;id2;id3"
     * @return a String for a WHERE IN clause
     * 
     * @version $Revision$, $Date$
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    public static String getMultiPicklistValuesForQuery(String values)
    {
        return getSeparatedValuesForQuery(values,';');
    }
    
    /**
     * Generates a list for a soql statement. ('id1','id2',..) 
     *
     * @param values a string of values with a delimiter -> "id1;id2;id3"
     * @param delim the delimiter to split -> e.g. ";"
     * @return a String for a WHERE IN clause
     *
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    public static String getSeparatedValuesForQuery(String values, String delim)
    {
        //ToDo return something readable for statement
        if(!isSet(values))
            return '()';
            
        List<String> valueList = values.split(delim,0);
        String returnValue = '(';
        for(String value : valueList)
        {
            returnValue += '\'' + value.trim() + '\',';
        } 
        //remove last comma and return
        return returnValue.substring(0,returnValue.length()-1) + ')';
    }
    
     /**
     * Creates a Map from a sObject (key -> value)
     *
     * @param obj the sObject
     * @return a Map key(String) -> value(Object)
     *
     * @author Eugen Tiessen <etiessen@gms-online.de> 
     */
    public static Map<String,Object> getSObjectAsMap(sObject obj)
    {
        String jsonData = JSON.serialize(obj);
        Map<String, Object> theMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        theMap.remove('attributes');
        return theMap;
    }
    
    /**
     * overwrite dest items with the given src items
     * 
     * @param src the source object that should merged into the destination object
     * @param dest the destination object. 
     *
     * @return sObject merged sObject
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    public static sObject mergeSObject(sObject src, sObject dest)
    {
        sObject newObj = dest.clone(true);
        
        Schema.DescribeSObjectResult dR = src.getSObjectType().getDescribe();
        system.debug('#### change values for ' + dR.getName());
                
        Map<String,Object> srcMap = SCBase.getSObjectAsMap(src);
        system.debug('#### sObject as map: ' + srcMap);
        for(String key : srcMap.keySet())
        {
            //If key is a primitive
            try
            {   Object srcValue = src.get(key);
                newObj.put(key, srcValue);
                
                Object destValue = dest.get(key);
                system.debug('#### change value of ' + key + ' from ' + destValue + ' -> ' + srcValue);
                continue;
            }
            catch(Exception e)
            {
                system.debug('#### could not merge key: ' + key + ' Error: ' + e.getMessage());
            }
            
            //if key is a sObject
            try
            {   sObject srcValue = src.getSObject(key);
                newObj.putSObject(key, srcValue);
                
                sObject destValue = dest.getSObject(key);
                if(String.valueOf(srcValue.get('id')).equals(String.valueOf(destValue.get('id'))))
                {
                    
                    srcValue = mergeSObject(srcValue, destValue);
                }
                system.debug('#### change value of ' + key + ' from ' + destValue + ' -> ' + srcValue);
                
                
            }
            catch(Exception e)
            {
                system.debug('#### could not merge key: ' + key + ' Error: ' + e.getMessage());
            }
        }
        system.debug('#### newObj: ' + newObj);
        return newObj;
    }
}