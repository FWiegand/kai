/*
 * @(#)SCReplenishmentExtension.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCReplenishmentExtension extends SCMatMoveBaseExtension
{
    private SCStock__c stock;
    private Map<Id, List<ReplenishmentMaterialInfo>> stockMatInfos;
    
    public Boolean showValType = true;
    public Boolean isSelected { get; set;}
    public Boolean showMaxQty { get; set;}
    public Boolean showReplQty { get; set;}
    public String matMoveType { get; set;}
    public String errMsg { get; set;}
    public String valType { get; set;}
    //public List<SelectOption> listValType;
    public List<SelectOption> sourceList { get; set;}
    public List<SelectOption> receiverList { get; set;}
    public List<ReplenishmentMaterialInfo> replMaterialInfos { get; set;}
    public List<ReplenishmentEmployeeInfo> replEmployeeInfos { get; set;}
    
    public Id sourceStock { get; set;}
    public Id receiverStock { get; set;}
    public SCResourceAssignment__c assignment { get; set;}

    public Boolean selectingOk { get; private set; }
    public Boolean assignmentOk { get; private set; }
    public Boolean creatingOk { get; private set; }
    public Boolean orderStep2 { get; private set; }
    
    public Boolean getShowValType() { return showValType; }
    public List<SelectOption> getTypeList()
    {
        return domMatMove.getDomainValuesByParameter(15, 8);
    }
    
    //public List<SelectOption> getValTypeList() { return listValType; }

    /**
     * Initialize the page objects for selecting the source and
     * receiver for the replenishment.
     *
     * @param    controller    the standard controller
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public SCReplenishmentExtension(ApexPages.StandardController controller)
    {
        stock = (SCStock__c)controller.getRecord();
        
        //try
        {
            SCStock__c tmpStock = [SELECT Id, ValuationType__c FROM SCStock__c WHERE Id = :stock.Id];
            showValType = tmpStock.ValuationType__c;
            stock = tmpStock;
        }
        //catch(Exception e) {}
        
        errMsg = null;
        
        System.debug('#### SCReplenishmentExtension(): Stock.Id -> ' + stock.Id);

        assignment = new SCResourceAssignment__c();
        assignment.ValidFrom__c = Date.today();
        assignment.ValidTo__c = Date.today();
        isSelected = true;
        showMaxQty = (2 == sysparamStockV8);
        showReplQty = (1 == sysparamStockV8);
        
        sourceList = new List<SelectOption>();
        receiverList = new List<SelectOption>();

        replMaterialInfos = null;
        replEmployeeInfos = null;
        stockMatInfos = new Map<Id, List<ReplenishmentMaterialInfo>>();

        
        assignmentOk = getMatMoveInfos(stock.Id, assignment.ValidFrom__c, 
                                        sourceList, receiverList);
        selectingOk = !assignmentOk;
        creatingOk = false;
        orderStep2 = false;
        
    } // SCReplenishmentExtension

    /**
     * This method is called when the require date is changed.
     * It fills the lists for source and receiver again.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference refillLists()
    {
        System.debug('#### refillLists(): new date -> ' + assignment.ValidFrom__c);

        if (assignment.ValidFrom__c < Date.today())
        {
            assignment.ValidFrom__c = Date.today();
            System.debug('#### refillLists(): correct date -> ' + assignment.ValidFrom__c);
        } // if (assignment.ValidFrom__c < Date.today())
        getMatMoveInfos(stock.Id, assignment.ValidFrom__c, sourceList, receiverList);
        return null;
    } // refillLists

    /**
     * Methode is called, when the user clicks [OK].
     * After the validion of the quantity the entry in SCMaterialMovement 
     * is created. If this is successful, entries in SCMateralMovement for
     * the current stock will be processed.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference startReplenishment()
    {
        System.debug('#### startReplenishment()');

        if ((receiverList.size() > 1) && 
            receiverList.get(1).getValue().equals(receiverStock))
        {
            replEmployeeInfos = getReplenishmentEmployees(receiverStock, assignment.ValidFrom__c);
        } // if ((receiverList.size() > 1) && ...
        else
        {
            List<Id> stockList = new List<Id>();
            stockList.add(stock.Id);
            Map<Id, List<ReplenishmentMaterialInfo>> mapStockMatInfos = new Map<Id, List<ReplenishmentMaterialInfo>>();
            getReplenishmentMaterial(stockList, assignment.ValidFrom__c, mapStockMatInfos);
            replMaterialInfos = mapStockMatInfos.get(stock.Id);
        }// else [if ((receiverList.size() > 1) && ...]
        
        selectingOk = true;
        return Page.SCReplenishmentCreate;
    } // startReplenishment

    /**
     * Selects/Deselects all entries in the displayed list.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference selectAll()
    {
        System.debug('#### selectAll()');

        isSelected = !isSelected;
        if (null != replMaterialInfos)
        {
            for (ReplenishmentMaterialInfo matInfo: replMaterialInfos)
            {
                matInfo.isSet = isSelected;
            } // for (ReplenishmentMaterialInfo matInfo: replMaterialInfos)
        } // if (null != replMaterialInfos)

        if (null != replEmployeeInfos)
        {
            for (ReplenishmentEmployeeInfo emplInfo: replEmployeeInfos)
            {
                emplInfo.isSet = isSelected;
            } // for (ReplenishmentEmployeeInfo emplInfo: replEmployeeInfos)
        } // if (null != replEmployeeInfos)
        return null;
    } // selectAll

    /**
     * Checks the input. Generate a question for the user and shows it
     * to him or create the entries for material movement.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference orderMatMoves()
    {
        System.debug('#### orderMatMoves()');

        errMsg = null;
        String infos = '';
        String receiver = '';
        // we have a list of stock items
        if (null != replMaterialInfos)
        {
            List<Id> stockList = new List<Id>();
            stockList.add(receiverStock);
            Map<Id, List<ReplenishmentMaterialInfo>> mapStockMatInfos = new Map<Id, List<ReplenishmentMaterialInfo>>();
            mapStockMatInfos.put(receiverStock, replMaterialInfos);
            // check if there a still material movements for the articles
            infos = checkForMaterialMovements(stockList, mapStockMatInfos);
            
            // material movements found, so inform the user
            if (infos.length() > 0)
            {
                receiver = receiverList.get(0).getLabel();
                receiver = receiver.subString(receiver.IndexOf('('));
                
                errMsg = System.Label.SC_msg_MatMovesExists1 + ' ' + receiver + ':<br><br>'; 
                errMsg += infos + '<br/>';
                errMsg += System.Label.SC_msg_MatMovesExists2;
            } // if (infos.length() > 0)
        } // if (null != replMaterialInfos)

        // we have a list of employees
        if (null != replEmployeeInfos)
        {
            // check if the employees have an assigned stock
            infos = checkStockOfEmployees(replEmployeeInfos);
            
            // employees without a stock found, so inform the user
            if (infos.length() > 0)
            {
                errMsg = System.Label.SC_msg_MissingStockAssignment1 + ':<br><br>'; 
                errMsg += infos + '<br/>';
                errMsg += System.Label.SC_msg_MissingStockAssignment2;
                orderStep2 = true;
            } // if (infos.length() > 0)
            // all selected employees have an assigned stock
            else
            {
                // check if there a still material movements for the articles
                infos = checkMatMovesOfEmplStocks(replEmployeeInfos, 
                                                  assignment.ValidFrom__c, 
                                                  stockMatInfos);
                System.debug('#### orderMatMoves(): stockMatInfos -> ' + stockMatInfos);
            
                // material movements found, so inform the user
                if (infos.length() > 0)
                {
                    receiver = receiverList.get(0).getLabel();
                    receiver = receiver.subString(receiver.IndexOf('('));
                
                    errMsg = System.Label.SC_msg_MatMovesExists1 + ' ' + receiver + ':<br><br>'; 
                    errMsg += infos + '<br/>';
                    errMsg += System.Label.SC_msg_MatMovesExists2;
                } // if (infos.length() > 0)
            } // else [if (infos.length() > 0)]
        } // if (null != replEmployeeInfos)

        if (null == errMsg)
        {
            return createReplenishment();
        } // if (null == errMsg)
        return null;
    } // orderMatMoves

    /**
     * Checks the input. Generate a question for the user and shows it
     * to him or create the entries for material movement.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference orderStep2()
    {
        System.debug('#### orderStep2()');

        String infos = '';
        String receiver = '';
        errMsg = null;
        orderStep2 = false;

        // we have a list of employees
        if (null != replEmployeeInfos)
        {
                // check if there a still material movements for the articles
            infos = checkMatMovesOfEmplStocks(replEmployeeInfos, 
                                              assignment.ValidFrom__c, 
                                              stockMatInfos);
        
            // material movements found, so inform the user
            if (infos.length() > 0)
            {
                receiver = receiverList.get(0).getLabel();
                receiver = receiver.subString(receiver.IndexOf('('));
                
                errMsg = System.Label.SC_msg_MatMovesExists1 + ' ' + receiver + ':<br><br>'; 
                errMsg += infos + '<br/>';
                errMsg += System.Label.SC_msg_MatMovesExists2;
            } // if (infos.length() > 0)
        } // if (null != replEmployeeInfos)

        if (null == errMsg)
        {
            return createReplenishment();
        } // if (null == errMsg)
        return null;
    } // orderStep2

    /**
     * Creates the entries for the material movements.
     *
     * @return    the page reference
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public PageReference createReplenishment()
    {
        System.debug('#### createReplenishment()');

        if (null != replMaterialInfos)
        {
            Map<Id, List<ReplenishmentMaterialInfo>> mapStockMatInfos = new Map<Id, List<ReplenishmentMaterialInfo>>();
            mapStockMatInfos.put(receiverStock, replMaterialInfos);
            
            creatingOk = createMatMoves(mapStockMatInfos, sourceStock, receiverStock, 
                                        assignment.ValidFrom__c);
        } // if (null != replMaterialInfos)

        if (null != replEmployeeInfos)
        {
            creatingOk = createMatMoves(stockMatInfos, sourceStock, receiverStock, 
                                        assignment.ValidFrom__c);
        } // if (null != replEmployeeInfos)

        return null;
    } // createReplenishment
} // SCReplenishmentExtension