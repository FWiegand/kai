/*
 * @(#)SCProductLookupController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Implement a custom lookup to allow a highly sophisticated product search.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCProductLookupController {

    private SCfwDomain productDom = new SCfwDomain('DOM_PRODUCTGROUP');

    private SelectOption noneOption = new SelectOption('', System.Label.SC_flt_None);
    
    private SCfwTranslation translation = new SCfwTranslation();

    private transient SCfwDomain languageDom;
   
    private transient List<SCfwDomainValue> languageDomainValues;
    
    private User runningUser {get; set;}
    
    private List<SCProductModel__c> foundProducts;

    private static SCApplicationSettings__c applicationSettings = SCApplicationSettings__c.getInstance();

	private String userBrands = null;
    
    /**
     * The searchProduct contains all settings used for the search later on
     */
    public SCProductModel__c searchProduct { get; set; }
    
    /**
     * Product name use for the search
     */
    public String productName 
    { 
        get; 
        set
        {
            if (this.productName != value)
            {
                setController = null;
            }
            
            this.productName = value;
        }
    }
    
    /**
     * Brand selected for the search
     */
    public String selectedBrand 
    { 
        get; 
        set
        {
            if (value <> this.selectedBrand)
            {
                System.debug('### reset controller -> ' + this.selectedBrand + value);
                setController = null;
            }
            this.selectedBrand = value;
        } 
    }
    

    /**
     * A message that will be shown to the user
     */
    public transient String message { get; set; }    
    
    public SCProductLookupController()
    {
        searchProduct = new SCProductModel__c();
        
        // We might have some parameters already defined in the URL
        // so we try to set our local variables
        productName = ApexPages.currentPage().getParameters().get('p');
        searchProduct.UnitClass__c = ApexPages.currentPage().getParameters().get('uc');
        searchProduct.UnitType__c = ApexPages.currentPage().getParameters().get('ut');
        selectedBrand = ApexPages.currentPage().getParameters().get('b');
        
        //Get User Info used for filtering 
        runningUser = [Select u.AssignedCountries__c, u.LanguageLocaleKey, u.Brand__c From User u where u.Id = :UserInfo.getUserId()];
        
        this.userBrands = runningUser.Brand__c;
        
        search();
    }

    // Initialize setController and return a list of records
    public ApexPages.StandardSetController setController 
    {
        get 
        {
            System.debug('#### setController(): searchProduct -> ' + searchProduct);
            
            if (setController == null ) 
            {
                List<String> conditions = new List<String>();

                if (productName <> null && productName.length() > 0 ) // && !productName.equals('*')
                {
                    String productNameCondition = productName.replace('*' ,'%');
                    
                    if(!productNameCondition.startswith('%'))
                    {
                        productNameCondition = '%' + productNameCondition;
                    }
                    
                    if(!productNameCondition.endswith('%'))
                    {
                        productNameCondition += '%';
                    }
                    String condition = '(toLabel(Group__c) like \'' + String.escapeSingleQuotes(productNameCondition) + '\'';
                    condition += ' or Name like \'' + String.escapeSingleQuotes(productNameCondition) + '\'';
                    condition += ' or ProductNameCalc__c like \'' + String.escapeSingleQuotes(productNameCondition) + '\')';
                    system.debug('-------- SFDC Support: ' + condition);
                    conditions.add(condition);
                }
        
                if (selectedBrand <> null && selectedBrand.length() > 0)
                {
                    conditions.add('Brand__r.Name = :selectedBrand');
                }
                
                
                if (searchProduct.UnitClass__c <> null && searchProduct.UnitClass__c.length() > 0)
                {
                    conditions.add('UnitClass__c = \'' + searchProduct.UnitClass__c + '\'');
                }
                
                if (searchProduct.UnitType__c <> null && searchProduct.UnitType__c.length() > 0)
                {
                    conditions.add('UnitType__c = \'' + searchProduct.UnitType__c + '\'');
                }

                if (searchProduct.Power__c <> null && searchProduct.Power__c.length() > 0)
                {
                    conditions.add('Power__c = \'' + searchProduct.Power__c + '\'');
                }

                // if not condition is set we won't search at all
                // since this would give too much records anyway        
                if (conditions.size() == 0)
                {
                    System.debug('#### setController(): No search started since no conditions are set!');
                    return setController;
                }
                System.debug('#### setController(): conditions -> ' + conditions);

                // always add the country as a search criteriyy                
                String condition;
                // first try to check if the user has individual countries assigned
                if (runningUser.AssignedCountries__c <> null)
                {
                    List<String> countries = runningUser.AssignedCountries__c.split(';',-1);
                    if(countries.size() > 0)
                    {
                        condition = ' Country__c includes (';
                        Integer i = 0;
                        for(String country:countries)
                        { 
                            i++;
                            condition += '\'' + country + '\'';
                            condition += (i >= countries.size() ? ') ' : ', ');
                        } 
                     }
                }
                
                // if the user does not have any country assigned then use the default 
                // country of his profile custom setting                    
                if (condition == null && applicationSettings.DEFAULT_COUNTRY__c <> null)
                {
                    condition = ' Country__c includes (\'' + applicationSettings.DEFAULT_COUNTRY__c + '\')';     
                }
                if (condition <> null)
                {
                    conditions.add(condition);
                }
                
                //only brands assigned to the user are valid
                if(SCBase.isSet(this.userBrands))
                {
                	conditions.add(' Brand__r.Name IN ' + SCBase.getMultiPicklistValuesForQuery(this.userBrands) + ' ');	
                }
                
                
                
                System.debug('#### setController(): conditions -> ' + conditions);
        
                String query = 'select Id, Name, ProductNameCalc__c, Brand__c, Brand__r.Name, toLabel(UnitType__c), '
                        + 'toLabel(Group__c), toLabel(UnitClass__c), toLabel(Power__c) '
                        + 'from SCProductModel__c ';
        
                query += ' where ';
                    
                for (Integer i = 0; i < conditions.size(); i++)
                {
                    query += conditions.get(i);
                    
                    if (i != conditions.size() - 1)
                    {
                        query += ' and ';
                    }
                }

                System.debug('#### setController(): query -> ' + query);

                setController = new ApexPages.StandardSetController(Database.getQueryLocator(
                    query));

                setController.setPageSize(10);
            }
            return setController;
        }
        set;
    }

    public PageReference search()
    {
        System.debug('#### search(): reset controller!');
            
        setController = null;
        // call this now, because the variable "message" must be filled now
        // so that it can be displayed in the page.
        // if getFoundProducts is called later (from the page) the message
        // panel will not be rerendered right
        getFoundProducts();
        return null;    
    }
    
    public List<SCProductModel__c> getFoundProducts()
    {
        System.debug('#### getFoundProducts(): setController -> ' + setController);
        System.debug('#### getFoundProducts(): search started');
        
        if (null == setController)
        {
            System.debug('### setController is NULL!');
            foundProducts = null;
            return null;
        } // if (null == setController)
        
        List<SCProductModel__c> products = (List<SCProductModel__c>) setController.getRecords();
                
        if (products <> null && products.size() > 0)
        {
            foundProducts = new List<SCProductModel__c>();

            System.debug('products -> ' + products);
            
            foundProducts = products;
            System.debug('#### getFoundProducts(): foundProducts -> ' + foundProducts);
            System.debug('#### getFoundProducts(): number of products -> ' + foundProducts.size());
      
            message = null;
        }
        else
        {
            // nothing found
            foundProducts = null;
            message = System.Label.SC_msg_NoProductFound;
            System.debug('#### getFoundProducts(): no products found');
        }
        return foundProducts;
    }


    /**
     * Get all brands
     * 
     * @return list of all brands as select options
     */
    public List<SelectOption> getAllBrand()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        // add a general option value to display if nothing is selected
        options.add(noneOption);
        
        try
	    {
    	    List<Brand__c> brandList;
    	    if(SCBase.isSet(this.userBrands))
    	    {
    	    	brandList = 
    	    	[
	    	    	SELECT 
	            		Id, Name 
	            	FROM 
	            		Brand__c 
	            	WHERE 
	            		Competitor__c = false 
	            		AND
	            		Name IN :this.userBrands.split(';')
	              	ORDER BY 
	              		Name
				]; 
    	    }
    	    else
    	    {
    	    	brandList = 
    	    	[
	    	    	SELECT 
	            		Id, Name 
	            	FROM 
	            		Brand__c 
	            	WHERE 
	            		Competitor__c = false 
	              	ORDER BY 
	              		Name
				];
    	    }
    	    for (Brand__c brand : brandList)
			{
			    options.add(new SelectOption(brand.Name,brand.Name));
			}
	    }
	    catch(Exception e)
	    {
	    	
	    }
        return options;
    }
}