/*
 * @(#)SCbtcStockOptimizationTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCbtcStockOptimizationTest
{
    static testMethod void testEntries_1()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockOptimization.asyncOptimizeAllForPlantName('SO Test GB Plant', 0, 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_2()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockOptimization.asyncOptimizeStock(stockId, 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }
    
    static testMethod void testEntries_3()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        
        ID jobID = SCbtcStockOptimization.asyncOptimizeStockDeterminedByName('SO Test GB Stock', 'trace');
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_4()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        List<String> stockIdList = new List<String>();
        stockIdList.add(stockId);
        ID jobID = SCbtcStockOptimization.asyncOptimizeStockList(stockIdList);
        //System.abortJob(jobID);
        Test.StopTest();
    }

    static testMethod void testEntries_5()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        List<String> stockIdList = new List<String>();
        stockIdList.add(stockId);
        SCbtcStockOptimization.syncOptimizeStockList(stockIdList);
        Test.StopTest();
    }



    static testMethod void testCreateProposal_A()
    {
        Test.StartTest();
        ID stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        runJobToTest(stockId);
        checkResults_A(stockId);        
        Test.StopTest();
    }
/*
    static void createOptimizationRuleSetItems_A(Id ruleSetId)
    {
//        Set by the function prepareTestData_A()
//        Decimal maxPartValue = 1000;
//        Decimal maxPartVolume = 50;
//        Decimal maxPartWeight = 30;
//        Decimal maxStockValue = 30000;
//        Decimal maxStockVolume = 500;
//        Decimal maxStockWeight = 560;
//        String optimizationInterval = '365';
        // R0 on the black list
        // R1 BLP > 1000 (maxPartValue) 
        // R2 Weight > 30 (maxPartWeight)
        // R3 Volume > 50 (maxPartVolume)
        // R7 on the white list
        // The rules R4 - R5 are set by the rules from S4 -S9
        createOptimizationRuleSetItem('S4', ruleSetId, 'consummed <=',    1, 'then',  0); 
        createOptimizationRuleSetItem('S5', ruleSetId, 'consummed <=',   10, 'then',  1);
        createOptimizationRuleSetItem('S6', ruleSetId, 'consummed <=',   20, 'then',  3,  '<', 99, null, null);
        createOptimizationRuleSetItem('S7', ruleSetId, 'consummed <=',   20, 'then',  2,  '>', 99, null, null);
        createOptimizationRuleSetItem('S8', ruleSetId, 'consummed <=',   50, 'then',  4);
        createOptimizationRuleSetItem('S9', ruleSetId, 'consummed <=',   99, 'then',  5, '<',  99, true, '3');
    }

    static void createAllItems_A(ID plantId, ID ruleSetId, ID stockId, ID priceListId, ID blackListId, ID whiteListId)
    {
        // Created by the caller of the function prepareTestData_A()
        // the stock's: max weight 560 kg , max value = 30000

        // article                                                       Stock
        // rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
        cI('R0', 'SB001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, blackListId, null);
        cI('R7', 'SB002','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,        100,        plantId, ruleSetId, stockId, priceListId,        null, whiteListId);
        cI('R1', 'SA001','201','KGM',     1,  1,   null,    2000,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('R2', 'SA002','201','KGM',    31,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('R3', 'SA003','201','KGM',    10, 51,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S4', 'S0001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          1,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S5', 'S0002','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         10,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S6', 'S0004','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S7', 'S0003','201','KGM',     1,  1,   null,     100,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S8', 'S0005','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         50,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('S9', 'S0006','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
       cI('S10', 'S0007','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
        // supersession
        cI('E5', 'S0008','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         10,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('E6', 'S0009','201', null,     1,  1,'S0008',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        cI('E7', 'S0010','201', null,     1,  1,'S0009',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
    }
*/

    static void checkResults_A(ID stockId)
    {
        SCStockOptimizationProposal__c p = SCbtcStockOptimizationTestHelper.readProposal(stockId);
        debug('proposal: ' + p);
        
        // Article Name to Proposal Item
        Map<String, SCStockOptimizationProposalItem__c> mpi = SCbtcStockOptimizationTestHelper.getMapArticleToProposalItem(p.Id);    

        // check proposal items
        debug('CheckArticle SB001'); 
//           rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//        cI('R0', 'SB001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, blackListId, null);
        System.assertEquals(mpi.get('SB001').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('SB001').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('SB001').CountedQuantity__c, 2);
        System.assertEquals(mpi.get('SB001').Delta__c,-5);
        Integer posSB001 = mpi.get('SB001').AppliedRule__c.indexOf('Rule 0: Article in the black list');
        System.assert(posSB001 > -1);
        
//        System.assertEquals(mpi.get('SB001').AppliedRule__c,'Rule 0. Article in the black list');

        debug('CheckArticle SB002'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('R7', 'SB002','201','KGM',     1,  1,   null,    2000,        1,     true,       5,  10,        100,        plantId, ruleSetId, stockId, priceListId,        null, whiteListId);
        System.assertEquals(mpi.get('SB002').ProposalQuantity__c, 1);
        System.assertEquals(mpi.get('SB002').ApprovedQuantity__c, 1);
        System.assertEquals(mpi.get('SB002').CountedQuantity__c, 100);
        System.assertEquals(mpi.get('SB002').Delta__c,-4);
        Integer posSB002 = mpi.get('SB002').AppliedRule__c.indexOf('Rule 5: consummed 100 > 0 article in the white list');
        System.assert(posSB002 > -1);
        // ProposalWeight = 1

        debug('CheckArticle SA001'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('R1', 'SA001','201','KGM',     1,  1,   null,    2000,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('SA001').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('SA001').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('SA001').CountedQuantity__c, 2);
        System.assertEquals(mpi.get('SA001').Delta__c,-5);

        Integer posSA001 = mpi.get('SA001').AppliedRule__c.indexOf('Rule 1: Brutto List price > 1000.00');
        System.assert(posSA001 > -1);

        debug('CheckArticle SA002'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('R2', 'SA002','201','KGM',    31,  1,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('SA002').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('SA002').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('SA002').CountedQuantity__c, 2);
        System.assertEquals(mpi.get('SA002').Delta__c,-5);
        Integer posSA002 = mpi.get('SA002').AppliedRule__c.indexOf('Rule 2: Weight = 31.0 > 30.000');
        System.assert(posSA002 > -1);

        debug('CheckArticle SA003'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('R3', 'SA003','201','KGM',    10, 51,   null,      10,        1,     true,       5,  10,          2,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('SA003').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('SA003').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('SA003').CountedQuantity__c, 2);
        System.assertEquals(mpi.get('SA003').Delta__c,-5);
        Integer posSA003 = mpi.get('SA003').AppliedRule__c.indexOf('Rule 3: Volume = 51.0 > 50.0');
        System.assert(posSA003 > -1);

        debug('CheckArticle S0001'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S4', 'S0001','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,          1,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0001').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('S0001').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('S0001').CountedQuantity__c, 1);
        System.assertEquals(mpi.get('S0001').Delta__c,-5);

        Integer posS0001 = mpi.get('S0001').AppliedRule__c.indexOf('Rule 4: consummed 1 <= 1 proposal 0');
        System.assert(posS0001 > -1);

        debug('CheckArticle S0002'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S5', 'S0002','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         10,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0002').ProposalQuantity__c, 1);
        System.assertEquals(mpi.get('S0002').ApprovedQuantity__c, 1);
        System.assertEquals(mpi.get('S0002').CountedQuantity__c, 10);
        System.assertEquals(mpi.get('S0002').Delta__c,-4);

        Integer posS0002 = mpi.get('S0002').AppliedRule__c.indexOf('Rule 4: consummed 10 <= 10 proposal 1');
        System.assert(posS0002 > -1);
        // ProposalWeight = 1 + 1 = 2

        debug('CheckArticle S0004'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S6', 'S0004','201','KGM',     1,  1,   null,      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0004').ProposalQuantity__c, 3);
        System.assertEquals(mpi.get('S0004').ApprovedQuantity__c, 3);
        System.assertEquals(mpi.get('S0004').CountedQuantity__c, 20);
        System.assertEquals(mpi.get('S0004').Delta__c,-2);

        Integer posS0004_a = mpi.get('S0004').AppliedRule__c.indexOf('Price: 10.0000 < 99.00');
        Integer posS0004_b = mpi.get('S0004').AppliedRule__c.indexOf('Rule 4: consummed 20 <= 20 proposal 3');
        System.assert(posS0004_a > -1 && posS0004_b > -1);
        // ProposalWeight = 2 + 3 = 5


        debug('CheckArticle S0003'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S7', 'S0003','201','KGM',     1,  1,   null,     100,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0003').ProposalQuantity__c, 2);
        System.assertEquals(mpi.get('S0003').ApprovedQuantity__c, 2);
        System.assertEquals(mpi.get('S0003').CountedQuantity__c, 20);
        System.assertEquals(mpi.get('S0003').Delta__c,-3);

        Integer posS0003_a = mpi.get('S0003').AppliedRule__c.indexOf('Price: 100.0000 > 99.00');
        Integer posS0003_b = mpi.get('S0003').AppliedRule__c.indexOf('Rule 4: consummed 20 <= 20 proposal 2');
        System.assert(posS0003_a > -1 && posS0003_b > -1);
        // ProposalWeight = 5 + 2 = 7

        debug('CheckArticle S0005'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S8', 'S0005','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         50,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0005').ProposalQuantity__c, 4);
        System.assertEquals(mpi.get('S0005').ApprovedQuantity__c, 4);
        System.assertEquals(mpi.get('S0005').CountedQuantity__c, 50);
        System.assertEquals(mpi.get('S0005').Delta__c,-1);

        Integer posS0005 = mpi.get('S0005').AppliedRule__c.indexOf('Rule 4: consummed 50 <= 50 proposal 4');
        System.assert(posS0005 > -1);
        // ProposalWeight = 7 + 4 = 11

        debug('CheckArticle S0006'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('S9', 'S0006','201', null,     1,  1,   null,     100,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0006').ProposalQuantity__c, 0);
        System.assertEquals(mpi.get('S0006').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('S0006').CountedQuantity__c, 99);
        System.assertEquals(mpi.get('S0006').Delta__c,-5);
        Integer posS0006 = mpi.get('S0006').AppliedRule__c.indexOf('None Rule: consummed 99 > 0 article is not the white list');
        System.assert(posS0006 > -1);

        debug('CheckArticle S0007'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//      cI('S10', 'S0007','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         99,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0007').ProposalQuantity__c, 25);
        System.assertEquals(mpi.get('S0007').ApprovedQuantity__c, 9); // max weight exceeded
        System.assertEquals(mpi.get('S0007').CountedQuantity__c, 99);
        System.assertEquals(mpi.get('S0007').Delta__c,20);
        Integer posS0007_a = mpi.get('S0007').AppliedRule__c.indexOf('Rule 4: consummed 99 <= 99 average with a 3 month range');
        Integer posS0007_b = mpi.get('S0007').AppliedRule__c.indexOf('Stock weight exceeds max allowed weight!');
        System.assert(posS0007_a > -1 && posS0007_b > -1);
        // ProposalWeight = 11 + 25 = 36, Approved Weight = 11 + 19 = 30 
        // 530 + 30 <= 560 where StockCurrentWeight = 530, Stock's max Weight = 560

// supersession


        debug('CheckArticle S0008'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('E5', 'S0008','201', null,     1,  1,   null,      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0008').ProposalQuantity__c, 4);
        System.assertEquals(mpi.get('S0008').ApprovedQuantity__c, 0); // max weight exceeded
        System.assertEquals(mpi.get('S0008').CountedQuantity__c, 50);
        System.assertEquals(mpi.get('S0008').Delta__c, -1);
        Integer posS0008_a = mpi.get('S0008').AppliedRule__c.indexOf('Rule 4: consummed 50 <= 50 proposal 4');
        Integer posS0008_b = mpi.get('S0008').AppliedRule__c.indexOf('Stock weight exceeds max allowed weight!');
        System.assert(posS0008_a > -1 && posS0008_b > -1);
        // ProposalWeight = 30 + 

        debug('CheckArticle S0009'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('E6', 'S0009','201', null,     1,  1,'S0008',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0009').ProposalQuantity__c, 3);
        System.assertEquals(mpi.get('S0009').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('S0009').CountedQuantity__c, 20);
        System.assertEquals(mpi.get('S0009').Delta__c,-2);
        Integer posS0009_a = mpi.get('S0009').AppliedRule__c.indexOf('Rule 4: consummed 20 <= 20 proposal 3');
        Integer posS0009_b = mpi.get('S0009').AppliedRule__c.indexOf('Rule 6: The article: S0009 has been replaced by article: S0008');
        System.assert(posS0009_a > -1 && posS0009_b > -1);


        debug('CheckArticle S0010'); 
//          rule,  name,  unit, wUnit,weight,vol,   repl,   price,priceUnit,premanent,  minQty, qty,movementQty
//       cI('E6', 'S0010','201', null,     1,  1,'S0009',      10,        1,     true,       5,  10,         20,        plantId, ruleSetId, stockId, priceListId, null, null);
        System.assertEquals(mpi.get('S0010').ProposalQuantity__c, 3);
        System.assertEquals(mpi.get('S0010').ApprovedQuantity__c, 0);
        System.assertEquals(mpi.get('S0010').CountedQuantity__c, 20);
        System.assertEquals(mpi.get('S0010').Delta__c,-2);
        Integer posS0010_a = mpi.get('S0010').AppliedRule__c.indexOf('Rule 4: consummed 20 <= 20 proposal 3');
        Integer posS0010_b = mpi.get('S0010').AppliedRule__c.indexOf('Rule 6: The article: S0010 has been replaced by article: S0008');
        System.assert(posS0010_a > -1 && posS0010_b > -1);


        debug('End of proposal item cases.'); 


    }


    static void runJobToTest(ID stockId)
    {
        SCbtcStockOptimization.syncOptimizeStock(stockId);
    }


    private static void debug(String text)
    {
        System.debug('###...................' + text);
    }

    //GMSSS: Remove for TestCoverage: 
    /*static testMethod void CodeCoverageA()
    {
        Id stockId = SCbtcStockOptimizationTestHelper.prepareTestData_A();
        SCStock__c stock = [SELECT Id, Plant__c FROM SCStock__c WHERE Id = :stockId];
        SCStockItem__c stockItem = [SELECT Id, Article__c, MinQty__c, Qty__c FROM SCStockItem__c WHERE Stock__c = :stockId LIMIT 1];
        Id plantId = stock.Plant__c;
        
        SCArticle__c article = new SCArticle__c();
        Database.insert(article);
        
        SCPriceList__c priceList = new SCPriceList__c();
        Database.insert(priceList);
        SCPriceListItem__c priceListItem = new SCPriceListItem__c(Article__c = stockItem.Article__c, Price__c = 1.0, PriceList__c = priceList.Id);
        Database.insert(priceListItem);
        
        SCMaterialMovement__c matMove = new SCMaterialMovement__c(Article__c = stockItem.Article__c,
                                                                  Qty__c     = 1,
                                                                  Stock__c   = stock.Id,
                                                                  Type__c    = '5204');
        Database.insert(matMove);
        
        Test.startTest();
        
        try { SCbtcStockOptimization.asyncOptimizeAll(plantId,1,'test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeAll(null,1,'test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeAllForPlantName(null,1,'test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeStock(null,'test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeStockDeterminedByName(null,'test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeStockDeterminedByName('test','test'); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeStockList(null); } catch (Exception e) { }
        try { SCbtcStockOptimization.syncOptimizeStock(null); } catch (Exception e) { }
        try { SCbtcStockOptimization.syncOptimizeStockList(null); } catch (Exception e) { }
        try { SCbtcStockOptimization.asyncOptimizeStockList(null); } catch (Exception e) { }
        
        SCbtcStockOptimization obj = new SCbtcStockOptimization(plantId,7,'test');
 
        try { obj.makeArticleIdToStockItemIdMap(stock, new SCStockOptimizationProposal__c(),
                                                new Map<Id, SCStockItem__c>(), new Map<Id, Id>(),
                                                new Map<Id, SCArticle__c>{article.Id => article}); } catch (Exception e) { }
        try { obj.makeArticleIdToArticleMap(stock,[SELECT Article__c FROM SCStockItem__c WHERE Stock__c = :stock.Id AND Article__c != null GROUP BY Article__c]); } catch (Exception e) { }
        try { obj.changeInKGM(null,'GRM'); } catch (Exception e) { }
        try { obj.makeStockItemIdList(new Map<Id,Id>{article.Id => article.Id}); } catch (Exception e) { }
        try { obj.renderArticleToPriceListItemMap(new Map<Id,Id>{stockItem.Article__c => stockItem.Id},
                                                  priceList.Id, new SCStockOptimizationProposal__c(),
                                                  new Map<Id, SCStockItem__c>{stockItem.Article__c => stockItem}); } catch (Exception e) { }
        try { obj.consumptionInDays(1,stock.Id); } catch (Exception e) { }
        try { obj.getCountOfStockItems(stock.Id); } catch (Exception e) { }
        try { obj.readStockItemIdToProposalItems(null); } catch (Exception e) { }
        try { obj.calculateProposal(new SCStock__c(Plant__c = plantId),
                                    null, null, 0, null, 0, 0, null,
                                    new Map<ID,SCStockOptimizationListItem__c>(),
                                    new Map<ID,SCStockOptimizationListItem__c>(),
                                    new List<SCStockOptimizationRuleSetItem__c>(),
                                    1, null); } catch (Exception e) { }
        try { obj.applayRuleSetItems(null, new Map<ID,SCStockOptimizationListItem__c>(),
                                     new List<SCStockOptimizationRuleSetItem__c>(),
                                     null, 0); } catch (Exception e) { }
        try { obj.isOperatorSatisfied(null, null, null, null); } catch (Exception e) { }
        try { obj.upsertProposals(true, new SCStockOptimizationProposal__c(),
                                  new Map<Id, SCStockOptimizationProposalItem__c>(),
                                  new Map<Id, SCStockItem__c>()); } catch (Exception e) { }
        try { obj.upsertProposals(false, new SCStockOptimizationProposal__c(),
                                  new Map<Id, SCStockOptimizationProposalItem__c>(),
                                  new Map<Id, SCStockItem__c>()); } catch (Exception e) { }
        try { obj.getAllowedApprovedQuantity(null,0,0,0,0,0,0,0,0,null); } catch (Exception e) { }
        try { obj.readStockItemIdToProposalItems(null); } catch (Exception e) { }
        try { obj.isLocked(null); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,''); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'CLT'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'CMQ'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'DMQ'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'LTR'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'HLT'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'MMQ'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(1,'MTQ'); } catch (Exception e) { }
        try { obj.getVolumeInLiter(null,''); } catch (Exception e) { }
        
        
        Test.stopTest();
    }*/
    
    //@author GMSS
    static testMethod void CodeCoverageB()
    {
        try { SCHelperTestClass3.createCustomSettings('de',true); }
        catch (Exception e) { }
        
        List<SCApplicationSettings__c> listAppSet = new List<SCApplicationSettings__c>();
        for (SCApplicationSettings__c appSet : [SELECT Id, MATERIAL_REORDER_LOCKTYPE__c FROM SCApplicationSettings__c])
        {
            appSet.MATERIAL_REORDER_LOCKTYPE__c = 'test,test';
        }
        Database.update(listAppSet);
        
        SCPlant__c plant = new SCPlant__c(Name = 'testPlant');
        Database.insert(plant);
        
        SCStock__c stock = new SCStock__c(Name = 'testStock', plant__c = plant.Id);
        Database.insert(stock);
        
        SCArticle__c articleA = new SCArticle__c();
        Database.insert(articleA);
        SCArticle__c articleB = new SCArticle__c(ReplacedBy__c = articleA.Id);
        Database.insert(articleB);
        SCArticle__c articleC = new SCArticle__c(ReplacedBy__c = articleB.Id);
        Database.insert(articleC);
        
        SCStockOptimizationProposal__c proposal = new SCStockOptimizationProposal__c(Stock__c = stock.Id);
        Database.insert(proposal);
        Database.insert(new SCStockOptimizationProposalItem__c(Article__c = articleA.Id, CountedQuantity__c = 1, ProposalQuantity__c = 2, Proposal__c = proposal.Id));
        
        
        Test.startTest();
        
        System.assertNotEquals(null,SCbtcStockOptimization.createReplacementMap());
        
        SCbtcStockOptimization obj = new SCbtcStockOptimization(stock.Id, plant.Id, 7, 'test');
        
        System.assertEquals(null,SCbtcStockOptimization.getReplacementArticle(null, new Map<Id,Id>()));
        System.assertEquals(articleA.Id,SCbtcStockOptimization.getReplacementArticle(articleC.Id,
                                                                                     new Map<Id,Id>{articleC.Id => articleB.Id,
                                                                                                    articleB.Id => articleA.Id}));
        System.assertEquals(articleA.Id,SCbtcStockOptimization.getReplacementArticle(articleC.Id,
                                                                                     new Map<Id,Id>{articleC.Id => articleB.Id,
                                                                                                    articleB.Id => articleA.Id,
                                                                                                    articleA.Id => articleA.Id}));
        
        try { SCbtcStockOptimizationTestHelper.prepareTestData_A(); } catch (Exception e) { }
        try { SCbtcStockOptimizationTestHelper.createArticles(); } catch (Exception e) { }
        try { SCbtcStockOptimizationTestHelper.readProposal(stock.Id); } catch (Exception e) { }
        try { SCbtcStockOptimizationTestHelper.getMapArticleToProposalItem(proposal.Id); } catch (Exception e) { }
        try { SCbtcStockOptimizationTestHelper.readProposalItem(proposal.Id); } catch (Exception e) { }
        try { SCbtcStockOptimizationTestHelper.readStockItems(stock.Id); } catch (Exception e) { }
        
        Test.stopTest();
    }
}