/**
 * SCInsuranceDataOut.cls    jp 11.04.2011
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *    This class is used as a data holder for the parameters
 *    of the outgoing calls to the external web services
 */
public class SCInsuranceDataOut
{
    public String country {set; get;}                // for determine the insurance service

    /**
     * Input data of the customer and his installed base
     * for whom the insurance will be created or validated.
     *
     */
    public EpsData inputEpsData;
    /**
    *   A technical error that happend by calling a web service 
    */
    public String webServiceError;
    /**
    * The parameter for the function insertPolicy
    * ought to be set into the insPolReq.
    * So many fields as possible ought to be set.
    */
    public InsertPolicyRequest insPolReq;            // for insertPolicy
    
    /**
    * The response to the web call of the function insertPolicy
    * Perhaps it can be used in the possible result mask.
    */
    public InsertPolicyResponse insPolResp;          // for insertPolicy
    
    /**
    * The parameter for the function checkPolicy
    * ought to be set into checkPolReq.
    * So many fields as possible ought to be set.
    */
    public ValidatePolicyRequest validatePolReq;           // for validatePolicy
    
    /**
    * The response to the web call of the function checkPolicy
    * Perhaps it can be used in the possible result mask.
    */
    public ValidatePolicyResponse validatePolResp;         // for validatePolicy

    public SCInsuranceDataOut (FunctionName funName)
    {
        init(funName, 1);
    }
    public SCInsuranceDataOut (FunctionName funName, Integer cntPolicies)
    {
        System.debug('.......Before Init');
        init(funName, cntPolicies);
        System.debug('.......AfterInit');
    }

    public void init(FunctionName funName, Integer cntPolicies)
    {
        if(funName == FunctionName.insertPolicy) //'insertPolicy')
        {
            insPolReq = new InsertPolicyRequest();
            insPolReq.cus = new Customer();
//          insPolReq.alternativeCus = new Customer();
            insPolReq.policies =  new Policy[cntPolicies];
            for(Integer i= 0; i< cntPolicies; i++)
            {
               insPolReq.policies[i] = new Policy(); 
            }
        }
        else if(funName == FunctionName.validatePolicy)
        {
            validatePolReq = new ValidatePolicyRequest();
        }
    }

    public enum FunctionName
    {
        insertPolicy,
        validatePolicy
    }

    public class EpsData
    {
        // the data for insertInsurance
        public ID orderID{set;get;}            // for protocoll
        public ID installedBaseID {set;get;}
        public ID contractTemplateID {set;get;}
        public ID contractPackageID {set;get;}
        public ID customerID {set;get;}    // Tenant
        public ID alternativeCustomerID {set;get;}   // landlord
        
        // the input data for validateInsurance 
        // and output for insertInsurance
        public ID contractID {set;get;}
    }

    public class InsertPolicyRequest 
    {
        public String transactionID = '1'; // only used by the TEST Mode
        public Customer cus;            // mandatory
        public Customer alternativeCus; // optional, landlord   
        public Policy[] policies;       // mandatory
    }
    
    public class Customer
    {
        public String customerType {set;get;}    // mandatory, d oder c
        public String customerRefNo {set;get;}   
        public String companyName {set;get;}     // optional
        public String title {set;get;}           // mandatory
        public String initials {set;get;}        // optional
        public String firstname {set;get;}       
        public String surname {set;get;}         // mandatory
        public String homePhone {set;get;}       // optional 
        public String mobilPhone {set;get;}      // optional
        public String workPhone {set;get;}       // optional
        public String email {set;get;}           // optional
        public String flatNo {set;get;}          // optional
        public String houseNo {set;get;}         // optional
        public String extension {set;get;}       // optional -> AddressLine
        public String street {set;get;}          // optional -> AddressLine
        public String district {set;get;}        // optional -> AddressLine
        public String city {set;get;}            // optional -> AddressLine
        public String county {set;get;}          // optional -> AddressLine
        public String postcode {set;get;}        // optional -> AddressLine
        public String country {set;get;}         // mandatory, GB
        public Boolean noTeleSales3rdParty = false; // mandatory
        public Boolean noTeleSales = false;         // mandatory
        public Boolean noMail3rdParty = false;      // mandatory
        public Boolean noMail = false;              // mandatory
        public Boolean noSms = false;               // mandatory
        public Boolean noEmail = false;             // mandatory
        public String noMailAddress;                // mandatory
        public Date dateOfBirth;                    // optional
        public String specialNeeds;                 // optional: blind, braille, deaf, lgprint
        
        /**
         * Initialize the custom object with the data of an 
         * account object.
         *
         * @param    acc     account object
         *
         * @author Alexander Wagner <awagner@gms-online.de>
         * @version $Revision$, $Date$
         */
        public void set(Account acc)
        {
            companyName = acc.Id;
            title = acc.Salutation__c;
            if (acc.isPersonAccount__c)
            {
                firstname = acc.FirstName__c;
                surname = acc.LastName__c;
            } // if (acc.isPersonAccount__c)
            else
            {
                firstname = acc.Name;
                surname = acc.Name2__c;
            } // else [if (acc.isPersonAccount__c)]
            flatNo = acc.BillingFlatNo__c;
            houseNo = acc.BillingHouseNo__c;
            extension = acc.BillingExtension__c;
            street = acc.BillingStreet;
            district = acc.BillingDistrict__c;
            city = acc.BillingCity;
            county = acc.BillingCounty__c;
            postcode = acc.BillingPostalCode;
            country = acc.BillingCountry__c;
            dateOfBirth = acc.PersonBirthdate__c;
            homePhone = acc.Phone;
            mobilPhone = acc.Mobile__c;
            workPhone = acc.Phone2__c;
            email = acc.Email__c;
        } // set
    }
    public class Policy
    {
        // mandatory
        public String mailCode {set;get;}        // mandatory
        public String areaCode {set;get;}        // mandatory
        public Double salesPrice {set;get;}      // mandatory
        public Date startDate {set;get;}         // mandatory
        public String salesType {set;get;}       // mandatory
                                                 // Contains the sales channel
                                                 // Post, Scanner, Tele, Web
        public String agentId {set;get;}         // optional
        public String payType {set;get;}         // mandatory
                                                 // possible values: CCA, CCM, DDA, DDM, DDQ, DDT
        public PaymentDetail payDet;             // optional     
        public String brokerID{set; get;}        // mandatory
        
        public String existingPolicyRef {set;get;} // optional
        public String routeID {set;get;}           // optional
        public String routeAreaCode {set;get;}     // optional
        public String activityCode {set;get;}      // optional                                         
        
        // Warranty Block         // in .wsdl policy structure the item is defined as optional,
                                  // but in the item there is no definitions of maxOccurs and minOccurs              
        public String applianceGroup {set;get;}  
        public String brand_id {set;get;}
        public String brand {set;get;}
        public String manufacturer {set;get;}
        public String model {set;get;}
        public String applianceType {set;get;}
        public Date installationDate {set;get;}
        public String serialNo {set;get;}        
        public Double purchasePrice {set;get;}       
        public String residualStatus {set;get;}      
        public String catalogueNumber {set;get;}     
        public String locationNo {set;get;}          
        
        // constructor
        public Policy()
        {
            payDet = new PaymentDetail();
        }
    }
    public class PaymentDetail
    {
        // Credit Card and Direct debit
        public Integer paymentDay {set;get;}                // mandatory

        // Credit Card in case that it was chosen
        public String cardNumber {set;get;}              // mandatory
        public String cardHolderName {set;get;}                // mandatory
        public String cardFromDate {set;get;}            // optional
        public String cardExpiryDate {set;get;}          // mandatory
        public String cardIssueNo {set;get;}             // optional
        public String cv2 {set;get;}                     // mandatory
        public String cardType {set;get;}                // mandatory: DBS, DBD, CCV, CCM
        public String flatNo {set;get;}        // optional
        public String houseNo {set;get;}       // optional
        public String postCode {set;get;}      // mandatory
        public String extension {set;get;}               // optional -> AddressLine
        public String street {set;get;}                  // optional -> AddressLine
        public String district {set;get;}                // optional -> AddressLine
        public String city {set;get;}                    // optional -> AddressLine
        public String county {set;get;}                  // optional -> AddressLine
        
        /**
         * Initialize the card holder data with the data of an 
         * account object.
         *
         * @param    acc     account object
         *
         * @author Alexander Wagner <awagner@gms-online.de>
         * @version $Revision$, $Date$
         */
        public void set(Account acc)
        {
            street = acc.BillingStreet;
            houseNo = acc.BillingHouseNo__c;
            extension = acc.BillingExtension__c;
            flatNo = acc.BillingFlatNo__c;
            postCode = acc.BillingPostalCode;
            city = acc.BillingCity;
            district = acc.BillingDistrict__c;
            county = acc.BillingCounty__c;
        } // set
        
        // DirectDebit
        public String directDebitAccountNumber {set;get;} // mandatory
        public String directDebitAccountTitle  {set;get;} // mandatory
        public String directDebitSortCode {set;get;}      // mandatory
    }  

    public class InsertPolicyResponse
    {
        public String customerRef {set; get;}    
        public InsertOnePolicyResponse [] policies;   
        public Error[] errors;
    }
    
    public class InsertOnePolicyResponse
    {
        public String policyRef {set; get;}
        public String mailCode {set;get;}
        public String areaCode {set;get;}
        public Double salePrice {set;get;}
        public String policyTypeDesc {set;get;}
        public String payMethod {set;get;}
        public Integer policyLength {set;get;}
        public Integer delayDays {set;get;}
        public String periodFrom {set;get;}
        public String periodTo {set;get;}
    }
    
    public class Error
    {
        public String errorCode {set;get;}
        public String description{set;get;}
    }
    
    public class ValidatePolicyRequest
    {
        // from .WSDL
        public String policyRef        {set; get;}    // optional
        public String customerSurname  {set; get;}    // optional
        public String propertyNumber   {set; get;}    // optional
        public String postCode         {set; get;}    // optional
        public String serialNumber     {set; get;}    // optional
        public String externalCustomerRef{set;get;}   // optional EPS-AccountID ?
        // for intern purpose
        public String policyType       {set;get;}    // Contract template id_ext
        public String insuranceCustomerNumber {set;get;} //    id_ext2 ?
        public String locationCode     {set;get;}        // LocationCode__c
        
    }
    
    public class ValidatePolicyResponse
    {
        public ValidatedCustPolicy[] policy;
        public String errorDescription;
    }
    public class ValidatedCustPolicy
    {
        public String policyRef                    {set; get;}    // minOccurs=1 doc
        public String customerRef                  {set; get;}    // minOccurs=1 doc
        public String EPSCustomerNo                {set; get;}    // not in WSDL
        public Date salePrice                      {set; get;}    // not in WSDL
        public Date onRiskDate                     {set; get;}    // not in WSDL
        public Date endDate                        {set; get;}    // not in WSDL
        public String jointVenture                 {set; get;}    // minOccurs=1
        public String policyType                   {set; get;}    // minOccurs=1 doc
        public String policyStatusDescription      {set; get;}    // minOccurs=1
        public String policyStatus                 {set; get;}    // minOccurs=1
        public String scheme                       {set; get;}    // minOccurs=1
        
        public String productGroup                 {set; get;}
        public String productGroupDesc             {set; get;}
        public String policyTypeDesc               {set; get;}
        public String affinityPartnerDesc          {set; get;}
        public String mailCode                     {set; get;}
        public String specialInstructions          {set; get;}
        public Date policyStartDate              {set; get;}      // doc
        public Date policyPeriodFrom             {set; get;}
        public Date policyPeriodTo               {set; get;}
        public Date policyStatusDate             {set; get;}
        public DateTime policyStatusTime         {set; get;}
        public String externalReference            {set; get;}
        public String parentReference              {set; get;}

        // Appliance Detail
        public ValidatePolicyApplianceDetail appDetail;
        // customer Address
        public ValidatePolicyCustomerAddress custAddr;
        // contact Data
        public ValidatePolicyContactData    contact;
        // Installer Detail
        public ValidatePolicyInstallerDetail installerDetail;
    }
    
    public class ValidatePolicyApplianceDetail
    {
        public String applianceType                {set; get;}
        public String applianceMake                {set; get;}
        public String applianceModel               {set; get;}
        public String serialNumber                 {set; get;}
        public String purchaseDate                 {set; get;}
    }
    
    public class ValidatePolicyCustomerAddress
    {
        public String flatNumber                    {set; get;}
        public String houseNumber                   {set; get;}
        public String street                        {set; get;}
        public String postCode                      {set; get;}
        public String addressLine1                  {set; get;}
        public String addressLine2                  {set; get;}
        public String addressLine3                  {set; get;}
        public String addressLine4                  {set; get;}
        public String addressLine5                  {set; get;}
    }
    
    public class ValidatePolicyContactData
    {
        public String title                        {set; get;}    // optional
        public String initials                     {set; get;}    
        public String firstName                    {set; get;}
        public String lastName                     {set; get;}
        public String homePhoneNumber              {set; get;}
        public String workPhoneNumber              {set; get;}
        public String mobilePhoneNumber            {set; get;}
        public String otherPhoneNumber             {set; get;}
        public String emailAddress                 {set; get;}
    }
    
    public class ValidatePolicyInstallerDetail
    {
        public String name                         {set; get;}
        public String phoneNumber                  {set; get;}
        public String flatNumber                   {set; get;}
        public String houseNumber                  {set; get;}
        public String postCode                     {set; get;}
        public String street                       {set; get;}
        public String addressLine1                 {set; get;}
        public String addressLine2                 {set; get;}
        public String addressLine3                 {set; get;}
        public String addressLine4                 {set; get;}
        public String addressLine5                 {set; get;}
    }
    /**
    * gets the content of the class without the sensible payment data
    */
    public String getMasked()
    {
        String text = this + '';
        String ret = text;
        Integer tagLength = 11; // cardNumber=
        System.debug('###text: ' + text);
        Integer cardNumberBegin = text.indexOf('cardNumber=');
        if(cardNumberBegin > -1 && text.length() > cardNumberBegin + 11)
        {
            Integer cardNumberEnd = text.indexOf(',', cardNumberBegin + 11);
            Integer cv2Begin = text.indexOf('cv2=');
            if(cv2Begin  > -1 && text.length() > cv2Begin + 4)
            {
                Integer cv2End = text.indexOf(',', cv2Begin + 4);
                if(cardNumberBegin > -1 && cardNumberEnd > -1
                   && cv2Begin > -1 && cv2End > -1
                   && cardNumberEnd < cv2Begin)
                {
                    ret = text.substring(0, cardNumberBegin + 11) + '**********' 
                    + text.substring(cardNumberEnd, cv2Begin + 4)
                    + '***' + text.substring(cv2End);
                }
            }
        }
        return ret;
    }
}