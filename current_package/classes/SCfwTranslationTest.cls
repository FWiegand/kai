/*
 * @(#)SCfwTranslationTest.cls
 *
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * This class contains unit tests for the translation handling class (SCfwTranslation).
 * <p>
 * Actually all needed data for comparison should be created by the test itself,
 * because real life (TM) data could change.
 * <p>
 * Remember that code of test classes doesn't count to Apex code limit.
 * <p>
 * REMARK:
 * We use the domain object as reference (domainValue) for the translations,
 * since it's easy to handle w/o too many mandatory fields and other dependencies.

 * @author Thorsten Klein <tklein@gms-online.de>
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCfwTranslationTest {

    private static final String DEFAULT_LOCALE = 'en';
    private static final String USERLOCALE = UserInfo.getLocale().substring(0,2);
    private static final Integer DOM_TEST1 = 10;
    private static final String DOM_TEST1_VAL1 = '11';
    private static final String DOM_TEST1_VAL2 = '12';
    private static final String DOM_TEST1_VAL3 = '13';
    private static List<SCDomainValue__c> domVal = new List<SCDomainValue__c>();

    // This list contains all descriptions in the same order as in the
    // domval list.
    private static List<Map<String,SCDomainDescription__c>> allTranslation =
    new List<Map<String,SCDomainDescription__c>>();

    /**
     * Get valid translation for one object
     */
    static testMethod void getTranslationPositive1()
    {
        createTestData1();

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();



        SCDomainDescription__c descr =
            translation.getTranslation(domVal.get(0).Id,
                                       DEFAULT_LOCALE);

        Test.stopTest();

        System.assertNotEquals(null, translation);
        System.assertEquals(descr.Text__c, allTranslation.get(0).get(DEFAULT_LOCALE).Text__c);
        System.assertEquals(descr.TextLong__c, allTranslation.get(0).get(DEFAULT_LOCALE).TextLong__c);
        System.assertEquals(descr.LocaleSidKey__c,
                            allTranslation.get(0).get(DEFAULT_LOCALE).LocaleSidKey__c);
    }

    /**
     * Get invalid translation for one object
     */
    static testMethod void getTranslationNegative1()
    {
        createTestData1();

        String domainValue = '012345678901234';

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        try
        {

            SCDomainDescription__c descr =
                translation.getTranslation(domainValue,
                                            DEFAULT_LOCALE);

            System.assert(false, 'Invalid translation fetched!!');

        }
        catch (SCfwTranslationException e)
        {
            String message = 'Unable to get translation for ' + domainValue;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();

        System.assertNotEquals(null, translation);

    }


    /**
     * Get translation text for one object
     */
    static testMethod void getText()
    {
        createTestData1();

        SCfwDomain domain2 = new SCfwDomain('DOM_TEST1');
        SCfwDomainValue domval2 = domain2.getDomainValue(DOM_TEST1_VAL1, 'en');

        Test.startTest();
        String text = domval2.getText('en');
        System.debug('####getText' + text);
        text = domval2.getText('aa');
        System.debug('####getText' + text);
        Test.stopTest();

    }



    /**
     * Get valid translation for multiple objects
     */
    static testMethod void getTranslationPositive2()
    {
        createTestData1();

        // <Id, position>
        Map<String,Integer> allId = new Map<String,Integer>();

        Integer position = 0;
        for (SCDomainValue__c val : domVal)
        {
            allId.put(val.Id, position++);
        }

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();



        List<SCDomainDescription__c> allDescr =
            translation.getTranslation(allId.keySet(),
                                       DEFAULT_LOCALE);

        Test.stopTest();

        for (SCDomainDescription__c descr : allDescr)
        {
            Integer pos = allId.get(descr.DomainValue__c);

            System.assertNotEquals(null, translation);
            System.assertEquals(descr.Text__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).Text__c);
            System.assertEquals(descr.TextLong__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).TextLong__c);
            System.assertEquals(descr.LocaleSidKey__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).LocaleSidKey__c);
        }
    }


    /**
     * Get invalid translation for multiple objects
     */
    static testMethod void getTranslationNegative2()
    {
        createTestData1();

        Set<String> domainValue = new Set<String>();
        domainValue.add('012345678901234');
        domainValue.add('987654321098765');

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        try
        {

            List<SCDomainDescription__c> descr =
                translation.getTranslation(domainValue,
                                            DEFAULT_LOCALE);

            System.assert(false, 'Invalid translation fetched!!');

        }
        catch (SCfwTranslationException e)
        {
            String message = 'No translation found for ' + domainValue;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();

        System.assertNotEquals(null, translation);

    }

    /**
     * Get valid translation for multiple objects
     */
    static testMethod void getTranslationMapPositive1()
    {
        createTestData1();

        // <Domain Id, position>
        Map<String,Integer> allId = new Map<String,Integer>();

        Integer position = 0;
        for (SCDomainValue__c val : domVal)
        {
            allId.put(val.Id, position++);
        }

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();


        // <Domain Id, SCDomainDescription__c>
        Map<String, SCDomainDescription__c> allDescr =
            translation.getTranslationMap(allId.keySet(),
                                          DEFAULT_LOCALE);

        Test.stopTest();

        for (SCDomainValue__c val : domVal)
        {
            Integer pos = allId.get(val.Id);

            System.assertNotEquals(null, allDescr.get(val.Id));
            System.assertEquals(allDescr.get(val.Id).Text__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).Text__c);
            System.assertEquals(allDescr.get(val.Id).TextLong__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).TextLong__c);
            System.assertEquals(allDescr.get(val.Id).LocaleSidKey__c,
                                allTranslation.get(pos).get(DEFAULT_LOCALE).LocaleSidKey__c);
        }
    }

    /**
     * Get invalid translation for multiple objects
     */
    static testMethod void getTranslationMapNegative1()
    {
        createTestData1();

        Set<String> domainValue = new Set<String>();
        domainValue.add('012345678901234');
        domainValue.add('987654321098765');

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        try
        {
            Map<String, SCDomainDescription__c> descr =
                translation.getTranslationMap(domainValue,
                                              DEFAULT_LOCALE);

            System.assert(false, 'Invalid translation fetched!!');
        }
        catch (SCfwTranslationException e)
        {
            //String message = 'Unable to get translation for ' + domainValue;
            String message = 'No translation found for ' + domainValue;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();

        System.assertNotEquals(null, translation);
    }

    /**
     * Get valid translation for multiple objects
     */
    static testMethod void getAllTranslationPositive1()
    {
        createTestData1();

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();



        List<SCDomainDescription__c> allDescr =
            translation.getAllTranslation(domVal.get(0).Id);

        Test.stopTest();

        System.assertNotEquals(null, translation);

        // We added only two different descriptions
        System.assertEquals(2, allDescr.size());

        SCDomainDescription__c lang1 = allDescr.get(0);
        SCDomainDescription__c lang2 = allDescr.get(1);

        // Both records have to be for the same domainValue object
        System.assertEquals(lang1.DomainValue__c,
                            domVal.get(0).Id);
        System.assertEquals(lang2.DomainValue__c,
                            domVal.get(0).Id);

        // Translation have to exist
        System.assert(allTranslation.get(0).containsKey(lang1.LocaleSidKey__c));
        System.assert(allTranslation.get(0).containsKey(lang2.LocaleSidKey__c));


        // Translations have to match
        System.assertEquals(
            allTranslation.get(0).get(lang1.LocaleSidKey__c).Text__c,
            lang1.Text__c);
        System.assertEquals(
            allTranslation.get(0).get(lang2.LocaleSidKey__c).Text__c,
            lang2.Text__c);
        System.assertEquals(
            allTranslation.get(0).get(lang1.LocaleSidKey__c).TextLong__c,
            lang1.TextLong__c);
        System.assertEquals(
            allTranslation.get(0).get(lang2.LocaleSidKey__c).TextLong__c,
            lang2.TextLong__c);
    }


    /**
     * Get invalid translation for multiple objects
     */
    static testMethod void getAllTranslationNegative1()
    {
        createTestData1();

        String domainValue = '012345678901234';

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        try
        {

            List<SCDomainDescription__c> descr =
                translation.getAllTranslation(domainValue);

            System.assert(false, 'Invalid translation fetched!!');

        }
        catch (SCfwTranslationException e)
        {
            //String message = 'Unable to get translation for ' + domainValue;
            String message = 'No translation found for ' + domainValue;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();

        System.assertNotEquals(null, translation);

    }


    /**
     * Get valid translation for multiple objects
     */
    static testMethod void getAllTranslationPositive2()
    {
        createTestData1();

        // <Id, position>
        Map<String,Integer> allId = new Map<String,Integer>();

        Integer position = 0;
        for (SCDomainValue__c val : domVal)
        {
            allId.put(val.Id, position++);
        }

        Test.startTest();

        List<SCDomainDescription__c> allDescr =
            SCfwTranslation.getAllTranslation(allId.keySet());

        Test.stopTest();


        for (SCDomainDescription__c descr : allDescr)
        {
            Integer pos = allId.get(descr.DomainValue__c);
            String locale = descr.LocaleSidKey__c;

            // Both records have to be for the same domainValue object
            System.assertEquals(descr.DomainValue__c,
                                domVal.get(pos).Id);

            // Translation have to exist
            System.assert(allTranslation.get(pos).containsKey(locale));

            // Translations have to match
            System.assertEquals(
                allTranslation.get(pos).get(locale).Text__c,
                descr.Text__c);
            System.assertEquals(
                allTranslation.get(pos).get(locale).TextLong__c,
                descr.TextLong__c);
        }
    }


    /**
     * Get invalid translation for multiple objects
     */
    static testMethod void getAllTranslationNegative2()
    {
        createTestData1();

        Set<String> domainValue = new Set<String>();
        domainValue.add('012345678901234');
        domainValue.add('987654321098765');

        Test.startTest();

        try
        {

            List<SCDomainDescription__c> descr =
                SCfwTranslation.getAllTranslation(domainValue);

            System.assert(false, 'Invalid translation fetched!!');

        }
        catch (SCfwTranslationException e)
        {
            String message = 'Unable to get translation for ' + domainValue;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();


    }

    /**
     * Test getting descriptions by domain name.
     *
     */
    static testMethod void getDescriptionByDomPositive1()
    {
        createTestData1();

        Integer position = 1;
        SCDomainValue__c val = domVal.get(position);

        String text = allTranslation.get(position).get(USERLOCALE).Text__c.substring(0,6);
        String domainName = 'DOM_TEST1';
        String locale = USERLOCALE;

        Test.startTest();

        List<SCDomainDescription__c> allDescr =
            SCfwTranslation.findDescriptionByDom(text, domainName, locale);

        Test.stopTest();

        System.assertEquals(1, allDescr.size());

        System.assert(allDescr.get(0).Text__c.contains(text));
        System.assertEquals(locale, allDescr.get(0).LocaleSidKey__c);

    }

    /**
     * Test getting descriptions by domain name.
     *
     */
    static testMethod void getDescriptionByDomPositive2()
    {
        createTestData1();

        Integer position = 1;
        SCDomainValue__c val = domVal.get(position);

        String text = '';
        String domainName = 'DOM_TEST1';
        String locale = USERLOCALE;

        Test.startTest();

        List<SCDomainDescription__c> allDescr =
            SCfwTranslation.findDescriptionByDom('', domainName, locale);

        Test.stopTest();

        System.assertEquals(0, allDescr.size());
    }

    /**
     * Test getting descriptions by domain name.
     *
     */
    static testMethod void getDescriptionByDomNegative1()
    {
        createTestData1();

        Integer position = 1;
        SCDomainValue__c val = domVal.get(position);

        String text = allTranslation.get(position).get(USERLOCALE).Text__c.substring(0,6);
        String domainName = 'DOM_NOT_EXISTING';
        String locale = USERLOCALE;

        Test.startTest();

        try
        {
        List<SCDomainDescription__c> allDescr =
            SCfwTranslation.findDescriptionByDom(text, domainName, locale);

            System.assert(false, 'Never should be here!');
        }
        catch (SCfwTranslationException e)
        {
            String message = 'Unable to fetch translation for ';
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();
    }
    /**
     * Test setting a new description
     *
     */
    static testMethod void setDescriptionPositive1()
    {
        createTestData1();

        String domainValue = domVal.get(1).Id;
        String locale = 'nl';
        String id2 = 'DOM-' + domVal.get(1).ID2__c + '-' + locale;
        String textShort = 'DOM_' + domVal.get(1).Name + '-' + locale;
        String textLong = textShort + '-LONG';

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        translation.setTranslation(domainValue, locale, id2, textShort, textLong);

        Test.stopTest();

        SCDomainDescription__c descr = translation.getTranslation(domainValue, locale);

        System.assertEquals(locale, descr.LocaleSidKey__c);
        System.assertEquals(id2, descr.ID2__c);
        System.assertEquals(textShort, descr.Text__c);
        System.assertEquals(textLong, descr.TextLong__c);
    }

    /**
     * Test setting a new description
     *
     */
    static testMethod void setDescriptionNegative1()
    {
        createTestData1();

        String domainValue = domVal.get(1).Id;
        String locale = 'nl';
        String id2 = 'DOM-' + domVal.get(1).ID2__c + '-' + locale;
        String textShort = 'DOM_' + domVal.get(1).Name + '-' + locale;
        String textLong = textShort + '-LONG';

        Test.startTest();
        SCfwTranslation translation = new SCfwTranslation();

        try
        {
            translation.setTranslation(domainValue, '', id2, textShort, textLong);
            System.assert(false, 'Never should be here!');
        }
        catch (SCfwTranslationException e)
        {
            String message = 'Unable to insert translation for ' + id2;
            System.assert(e.getMessage().contains(message),
                          e.getMessage());
        }

        Test.stopTest();
    }


/*===========================================================================*/

    /**
     * Create test data for translation tests
     */
    private static void createTestData1()
    {
        createDomLanguage();

        Date xToday = Date.today();

        /***************************************
         Create Domain DOM_TEST1
         ***************************************/

        SCDomainRef__c domRef1 =
            new SCDomainRef__c (Name = 'DOM_TEST1',
                                ID__c = DOM_TEST1);

        upsert domRef1;




        /***************************************
         Create Domain values
         ***************************************/
        domVal.add(
            new SCDomainValue__c(Name = DOM_TEST1_VAL1,
                                 DomainRef__c = domRef1.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 10,
                                 ID2__c = DOM_TEST1_VAL1,
                                 ControlParameter__c = 'V1=0|V2=1|V3=2|V4=3,4,5,6'
                                )
            );


        domVal.add(
            new SCDomainValue__c(Name = DOM_TEST1_VAL2,
                                 DomainRef__c = domRef1.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 10,
                                 ID2__c = DOM_TEST1_VAL2,
                                 ControlParameter__c = 'V1=a|V2=b|V3=c|V4=d'
                                )
            );

        domVal.add(
            new SCDomainValue__c(Name = DOM_TEST1_VAL3,
                                 DomainRef__c = domRef1.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 10,
                                 ID2__c = DOM_TEST1_VAL3,
                                 ControlParameter__c = 'V1=z|V2=y|V3=x|V4=a'
                                )
            );

        upsert domVal;



        /***************************************
         Create Translations
         ***************************************/
        List<SCDomainDescription__c> allTranslationAsList = new List<SCDomainDescription__c>();

        for (SCDomainValue__c val : domVal)
        {
            String id2;
            String textShort;
            String textLong;

            id2 = 'DOM-' + val.ID2__c + '-' + DEFAULT_LOCALE;
            textShort = 'DOM_' + val.Name + '-' + DEFAULT_LOCALE;
            textLong = textShort + '-LONG';

            Map<String,SCDomainDescription__c> descr = new Map<String,SCDomainDescription__c>();

            descr.put(DEFAULT_LOCALE,
                new SCDomainDescription__c(ID2__c = id2,
                                     LocaleSidKey__c = DEFAULT_LOCALE,
                                     DomainValue__c = val.Id,
                                     Text__c = textShort,
                                     TextLong__c = textLong)
            );



            id2 = 'DOM-' + val.ID2__c + '-' + USERLOCALE;
            textShort = 'DOM_' + val.Name + '-' + USERLOCALE;
            textLong = textShort + '-LONG';

            descr.put(USERLOCALE,
                new SCDomainDescription__c(ID2__c = id2,
                                     LocaleSidKey__c = USERLOCALE,
                                     DomainValue__c = val.Id,
                                     Text__c = textShort,
                                     TextLong__c = textLong)
            );

            allTranslationAsList.add(descr.get(DEFAULT_LOCALE));
            allTranslationAsList.add(descr.get(USERLOCALE));
            allTranslation.add(descr);
        }

        upsert allTranslationAsList;

    }


    private static void createDomLanguage()
    {
        Date xToday = Date.today();

        /***************************************
         Create Domain DOM_LANGUAGE
         ***************************************/
        SCDomainRef__c domLang;

         try
        {
            domLang = [select Name, SystemDomain__c, ID__c
                            from SCDomainRef__c where Name = 'DOM_LANGUAGE'];

        }
        catch (QueryException e)
        {
            domLang =
                new SCDomainRef__c (Name = 'DOM_LANGUAGE',
                                    ID__c = 5100);
            upsert domLang;
        }

        List<SCDomainValue__c> domVals = new List<SCDomainValue__c>();

        domVals.add(
            new SCDomainValue__c(Name = '5101',
                                 DomainRef__c = domLang.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 150,
                                 ControlParameter__c = 'V1=|V2=h,m,u,mo,di,mi,do,fr,sa,so,offen,kw',
                                 Constant__c = 'DOMVAL_LANG_DEU',
                                 ID2__c = 'de'
                                )
                    );

        domVals.add(
            new SCDomainValue__c(Name = '5102',
                                 DomainRef__c = domLang.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 90,
                                 ControlParameter__c = 'V1=|V2=td,tm,datm,mon,tue,wed,thu,fri,sat,sun,open,wn',
                                 Constant__c = 'DOMVAL_LANG_ENG',
                                 ID2__c = 'en'
                                )
                    );

        domVals.add(
            new SCDomainValue__c(Name = '5103',
                                 DomainRef__c = domLang.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 140,
                                 ControlParameter__c = 'V1=|V2=',
                                 Constant__c = 'DOMVAL_LANG_FRZ',
                                 ID2__c = 'fr'
                                )
                    );

        domVals.add(
            new SCDomainValue__c(Name = '5104',
                                 DomainRef__c = domLang.Id,
                                 ValidFrom__c = xToday,
                                 Sort__c = 70,
                                 ControlParameter__c = 'V1=|V2=',
                                 Constant__c = '',
                                 ID2__c = 'nl_BE'
                                )
                    );

        upsert domVals;
    }
}