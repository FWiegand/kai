/**
 * AseServiceTest.cls
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /**
 * AseCalloutTestService includes methods for testing the ASEWebserivce
 * and the communication. At the moment there is a function to get
 * all data from the ASEWeb for a specified dataType and print this
 * data on the DebugConsole. 
 * 
 * @author gmsss
 * @version $Revision$, $Date$
 */
@isTest
private class AseServiceTest
{
    static testMethod void testCodeCoverageA()
    {
        AseService.setExFault_element a = new AseService.setExFault_element();
        a.type_x = '';
        a.errorCode = '';
        a.errorString = '';
        System.assertNotEquals(null,a.type_x);
        System.assertNotEquals(null,a.errorCode);
        System.assertNotEquals(null,a.errorString);
        
        AseService.setEx_element b = new AseService.setEx_element();
        b.tenant = '';
        b.aseData = new AseService.aseDataType[]{};
        System.assertNotEquals(null,b.tenant);
        System.assertNotEquals(null,b.aseData);
        
        AseService.setExResponse_element c = new AseService.setExResponse_element();
        c.response = new AseService.aseDataType[]{};
        System.assertNotEquals(null,c.response);
    }
}