/**
 * @(#)SCMapController.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Old and not in use
 * 
 * @author Eugen Tiessen <etiessen@gms-online.de>
 * @version $Revision$, $Date$
 */
public virtual class SCMapController {

    


    public String displayMode { get; set; }
    public Id id {get;set;}
    //public String divId {get; private set;}
    
    public SCMapController()
    {
		//DivId generation
        //divId = 'gMap'+(Crypto.getRandomLong() & DateTime.now().getTime());
    }
    
   
}