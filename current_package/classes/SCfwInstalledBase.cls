/*
 * @(#)SCfwInstalledBase.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Installed Base wrapper object that's often used for VF pages for CRUD
 * operations on the installed base.
 *
 * This class is also used in the BO Installed Base
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCfwInstalledBase
{
    /**
     * Getter/Setter for the actual installed base record
     */
    public SCInstalledBase__c installedBase { get; set; }

    /**
     * Hold installed base role entries for this particular installed base
     */    
    private List<SCInstalledBaseRole__c> installedBaseRole = new List<SCInstalledBaseRole__c>();
    
    /**
     * The product of this installed base
     * This is a wrapper object that holds product related data (e.g. the actual product name)
     */
    // public Product product { get; set; }
    
    /**
     * The article this installed base is connected to.
     * It's often needed for wrapper purposes to ease the handling.
     */
    public SCArticle__c article { get; set; }

    /**
     * SFDC ID of the account holding installed base owner role
     */
    private Id owner;

    /**
     * The account object of the owner.
     */
    private Account account;
    
    /**
     * SFDC ID of the installed base location
     */
    private Id locationId;

    public SCInstalledBaseLocation__c location { get; set; }

    /**
     * Order Item object
     * It's needed here to easily allow creation of new Installed Base entries during
     * the order creation wizard.
     */
    public SCOrderItem__c orderItem { get; set; }

    /**
     * Constructor
     */
    public SCfwInstalledBase()
    {
       installedBase = null;
        init();
    }

    /**
     * Constructor that set a SCInstalledBase object directly
     */
    public SCfwInstalledBase(SCInstalledBase__c instBase)
    {
        installedBase = instBase;
        init();
    }

    /**
     * Constructor that set a SCInstalledBase object directly
     *
     * @param instBase    a filled installedbase object
     * @param instBaseLoc the installed base location
     */
    public SCfwInstalledBase(SCInstalledBase__c instBase, 
                             SCInstalledBaseLocation__c instBaseLoc)
    {
        installedBase = instBase;
        this.location = instBaseLoc;
        init();
    }
    
    /**
     * Constructor that set the owner directly
     *
     * @param ownerId the owner of this installed base entry
     */
    public SCfwInstalledBase(Id ownerId)
    {
        this.owner = ownerId;
        installedBase = null;
        
        init();
    }
    
    /**
     * Constructor that set the owner and the location directly
     *
     * @param ownerId the owner of this installed base entry
     */
    public SCfwInstalledBase(Id ownerId, id locId)
    {
        this.owner = ownerId;
        this.locationId = locId;
        installedBase = null;
        
        init();
    }
    
    /**
     * Initialise the main data, called from the constructors
     *
     */
    private void init()
    {
        System.debug('#### init(): owner -> ' + owner);
        System.debug('#### init(): locationId -> ' + locationId);
        System.debug('#### init(): installedBase -> ' + installedBase);
      
        if (this.owner <> null)
        {
            try
            {
                account = [select Name, BillingStreet, BillingHouseNo__c, 
                                  BillingExtension__c, BillingCity, 
                                  BillingCountry__c, BillingPostalCode, 
                                  BillingCounty__c, BillingDistrict__c, 
                                  GeoX__c, GeoY__c, GeoApprox__c, BillingFloor__c, BillingFlatNo__c
                             from Account
                            where Id = :this.owner];
            }
            catch (QueryException e)
            {
                System.debug('Owner for ' + owner + ' could not be found');
                this.owner = null;
            }
        }
        
        if (null == installedBase)
        {
            installedBase = new SCInstalledBase__c();
        } // if (null == installedBase)
        else
        {
            locationId = installedBase.InstalledBaseLocation__c;
        } // else [if (null == installedBase)]
        
        orderItem = new SCOrderItem__c();
        if ((null != locationId) && (null == location))
        {
            System.debug('#### init(): locationId -> ' + locationId);
            location = [Select LocName__c, Street__c, HouseNo__c, Extension__c, Status__c, 
                               FlatNo__c, Floor__c, CountryState__c, City__c, District__c, 
                               County__c, Country__c, PostalCode__c, GeoX__c, GeoY__c, GeoApprox__c 
                        from SCInstalledBaseLocation__c
                        where Id = :locationId];
                        
            Date xToday = Date.today();
            installedBaseRole = [Select Role__c, Account__c 
                                 from SCInstalledBaseRole__c
                                 where InstalledBaseLocation__c = :locationId
                                       and ValidFrom__c <= :xToday
                                       and (ValidTo__c >= :xToday or ValidTo__c = null)];
            System.debug('#### init(): installedBaseRole -> ' + installedBaseRole);
        } // if ((null != locationId) && (null == location))
        else if (null == location)
        {
            try
            {
                location = new SCInstalledBaseLocation__c(
                                        LocName__c = account.Name,
                                        Street__c = account.BillingStreet, 
                                        HouseNo__c = account.BillingHouseNo__c, 
                                        Extension__c = account.BillingExtension__c, 
                                        City__c = account.BillingCity,
                                        Country__c = account.BillingCountry__c,
                                        County__c = account.BillingCounty__c,
                                        District__c = account.BillingDistrict__c,
                                        PostalCode__c = account.BillingPostalCode,
                                        GeoX__c = account.GeoX__c,
                                        GeoY__c = account.GeoY__c,
                                        GeoApprox__c = account.GeoApprox__c,
                                        Floor__c = account.BillingFloor__c,
                                        FlatNo__c = account.BillingFlatNo__c
                                                     );
            }
            catch (Exception e)
            {
                System.debug(LoggingLevel.ERROR, 'No account data found');
            }
        } // else if (null == location)
    }
    
    
    /**
     * Add another installed base role for this installed base entry.
     * 
     * @param role The installed base role to add
     */
    public void addInstalledBaseRole(String role)
    {
        if (!hasRole(role) && SCBase.isSet(owner))
        {
            SCInstalledBaseRole__c baseRole = new SCInstalledBaseRole__c(
                                                        Role__c = role, 
                                                        Account__c = owner,
                                                        ValidFrom__c = Date.today(),
                                                        InstalledBaseLocation__r = location
                                                        );
                                                        
            if (location <> null && location.Id <> null)
            {
                baseRole.InstalledBaseLocation__c = location.Id;                
            }
            
            installedBaseRole.add(baseRole);
        }
    }
    
    /**
     * Add all default roles for this installed base. This is currently only 
     * the owner role.
     */
    private void addDefaultRoles()
    {
        addInstalledBaseRole('Owner');
    }
    
    /**
     * Check if the given role already exists for this installed base
     * For performance reasons this is checked only in the internal 
     * data structure.
     *
     * @return <code>true</code> if the role exists, <code>false</code> otherwise
     */
    private Boolean hasRole(String role)
    {
        System.debug('#### hasRole(): role -> ' + role);
        for (SCInstalledBaseRole__c baseRole : installedBaseRole)
        {
            System.debug('#### hasRole(): baseRole.Role__c -> ' + baseRole.Role__c);
            if (baseRole.Role__c.equals(role))
            {
                System.debug('#### hasRole(): role found');
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Save the installed base and related entries like installed base roles
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public void save()
    {
        try
        {
            upsert location;
            
            //if (null != article)
            if (article != null)
            {
                installedBase.Article__c = article.Id;
            } // if (null != article)

            installedBase.InstalledBaseLocation__c = location.Id;

            // This IS a hack!
            // In case the ProductModel__c which is a lookup contains an 
            // empty String you'll get an "System.StringException: Invalid id:"
            // exception if you try to READ the field. In that case you simply 
            // have to set it to NULL
            if ((String)installedBase.ProductModel__c != null && 
                ((String)installedBase.ProductModel__c).length() == 0)
            {
                installedBase.ProductModel__c = null;
            }

            // only save the installed base if it is filled
            if ((null != installedBase.ProductModel__c) || 
                (null != installedBase.ProductGroup__c) || 
                (null != installedBase.ProductUnitClass__c) || 
                (null != installedBase.ProductUnitType__c) || 
                (null != installedBase.ProductSkill__c)) 
            {
                upsert installedBase;
            } // if ((null != installedBase.ProductModel__c) || 
            
            addDefaultRoles();
            System.debug('#### save(): roles -> ' + installedBaseRole);
            upsert installedBaseRole;
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 
                         'Unable to save installed base ' + e.getMessage());
            ApexPages.addMessages(e);
        }

    }
    
    
    /**
     * Get all installed base entries that are owned by the given account Id.
     *
     * @param ownerId Account ID of the owner
     * @return List of all installed base entries owned by the given account.
     */
    public static List<SCfwInstalledBase> getInstalledBaseByOwner(Id ownerId)
    {
        return getInstalledBaseByOwner(ownerId, null);
    }
    
    /**
     * Get all installed base entries that are owned by the given account Id.
     *
     * @param ownerId Account ID of the owner
     * @param    ignoreOrder  id of the order that should be ignored in the related list of orderitems
     *
     * @return List of all installed base entries owned by the given account.
     */
    public static List<SCfwInstalledBase> getInstalledBaseByOwner(Id ownerId, Id ignoreOrderId)
    {
        Date xToday = Date.today();
        List<SCfwInstalledBase> ibByOwner = new List<SCfwInstalledBase>();
        
        try
        {
            System.debug('##### try to fetch installed base!');
            System.debug('owner -> ' + ownerId);
            System.debug('xToday -> ' + xToday);

            Set<Id> locationId = new Set<Id>();
            
            for (SCInstalledBaseRole__c role : 
                    [select InstalledBaseLocation__c from SCInstalledBaseRole__c
                                where Account__c = :ownerId
                                  and Role__c in ('Owner', 'User') 
                                  and InstalledBaseLocation__c <> null
                                  and ValidFrom__c <= :xToday
                                  and (ValidTo__c >= :xToday or ValidTo__c = null)])
            {
                locationId.add(role.InstalledBaseLocation__c);
            }

            if (locationId.size() > 0)
            {
                for (SCInstalledBase__c ib : [select Id, Name, 
                            Article__c, Article__r.Name, 
                            PurchaseDate__c, Parent__c, 
                            Article__r.ArticleNameCalc__c,
                            Article__r.Class__c, 
                            SerialNo__c, ComBoxStatus__c, 
                            Status__c, PurchasePrice__c,
                            Room__c, InstalledBaseLocation__c,
                            Type__c, ConstructionYear__c,
                            Brand__r.Name, ContractRef__c, ContractStartDate__c, 
                            DeliveryDate__c, InstallationDate__c, 
                            HandoverDate__c, Sort__c, 
                            ProductGroup__c, System__r.Name, 
                            ProductModel__c, ProductModel__r.Group__c, 
                            ProductModel__r.Name, ProductModel__r.ComBoxType__c, 
                            ProductModel__r.ProductNameCalc__c, 
                            ProductUnitClass__c, ProductUnitType__c, ProductEnergy__c,
                            ProductSkill__c, ProductNameCalc__c, Phone__c, 
                            LocationName__c, Building__c, Floor__c, 
                            InstalledBaseLocation__r.LocName__c, 
                            InstalledBaseLocation__r.Street__c, 
                            InstalledBaseLocation__r.HouseNo__c, 
                            InstalledBaseLocation__r.Extension__c, 
                            InstalledBaseLocation__r.FlatNo__c, 
                            InstalledBaseLocation__r.Floor__c, 
                            InstalledBaseLocation__r.CountryState__c, 
                            InstalledBaseLocation__r.City__c, 
                            InstalledBaseLocation__r.District__c, 
                            InstalledBaseLocation__r.County__c, 
                            InstalledBaseLocation__r.Country__c, 
                            InstalledBaseLocation__r.PostalCode__c, 
                            InstalledBaseLocation__r.GeoX__c,
                            InstalledBaseLocation__r.GeoY__c,
                            InstalledBaseLocation__r.GeoApprox__c, 
                            InstalledBaseLocation__r.Status__c,
                            cce_EquipmentTypeFilter__c,
                            (Select Id From Installed_Base__r),
                            IdExt__c, Description__c, PriceList__c, PriceList__r.Name, ManufacturerSerialNo__c, 
                            util_StockPlant__c, toLabel(TechnicalObjType__c),
                            
                            //PMS 33906/W2: Erweiterungen - ZC16 - Austausch
                            (
                                SELECT 
                                    Id, Order__r.Name, Order__r.Status__c, Order__c 
                                FROM 
                                    OrderItemInstalledBase__r
                                WHERE
                                    Order__r.Status__c =: SCfwConstants.DOMVAL_ORDERSTATUS_NOT_COMPLETED.split(',')
                                    AND
                                    Order__c != :ignoreOrderId
                            )
                            
                       from SCInstalledBase__c
                      where InstalledBaseLocation__c in :locationId
                        and Parent__c = null 
                   order by Sort__c
                      limit 900
                     ])
                 {
                     SCfwInstalledBase instBase = new SCfwInstalledBase(ib, ib.InstalledBaseLocation__r);
                     ibByOwner.add(instBase);
                 }
             } // if (locationId.size() > 0)
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 'No installed base found for owner ID ' + ownerId);        
        }
        
        return ibByOwner;
    }
    /**
     * Get all Installed Base Locations assigned to the Account given.
     *
     * @param ownerId Account ID that is connected to this location
     * @return List of all installed base locations for the given account.
     */
    public static List<SCInstalledBaseLocation__c> getInstalledBaseLocationByOwner(Id ownerId)
    {
        Date xToday = Date.today();
        List<SCInstalledBaseLocation__c> iblByOwner = new List<SCInstalledBaseLocation__c>();
        
        try
        {
            System.debug('##### try to fetch installed base location!');
            System.debug('owner -> ' + ownerId);
            System.debug('xToday -> ' + xToday);

            iblByOwner = [select Id, Name, LocName__c, Address__c, Street__c, HouseNo__c, Extension__c, Status__c, 
                                 FlatNo__c, Floor__c, CountryState__c, City__c, District__c, 
                                 County__c, Country__c, PostalCode__c, GeoX__c, GeoY__c, GeoApprox__c
                            from SCInstalledBaseLocation__c
                           where Id in (
                                select InstalledBaseLocation__c
                                  from SCInstalledBaseRole__c
                                 where Account__c = :ownerId
                                   and Role__c in ('Owner', 'User') 
                                   and InstalledBaseLocation__c <> null
                                   and ValidFrom__c <= :xToday
                                   and (ValidTo__c >= :xToday or ValidTo__c = null))
                        order by LocName__c];
            
            Account account = [select Name, BillingStreet, BillingHouseNo__c, 
                                       BillingCity, BillingCountry__c, BillingPostalCode,
                                       GeoX__c, GeoY__c, GeoApprox__c
                                from Account
                                where Id = :ownerId];
        
            if ((null != account.GeoX__c) && (account.GeoX__c != 0) && 
                (null != account.GeoY__c) && (account.GeoY__c != 0))
            {
                for (SCInstalledBaseLocation__c loc :iblByOwner)
                {
                    if (((null == loc.GeoX__c) || (null == loc.GeoY__c) || 
                         ((loc.GeoX__c == 0) && (loc.GeoY__c == 0))) && 
                        (loc.Street__c == account.BillingStreet) && 
                        (loc.HouseNo__c == account.BillingHouseNo__c) && 
                        (loc.City__c == account.BillingCity) && 
                        (loc.Country__c == account.BillingCountry__c) && 
                        (loc.PostalCode__c == account.BillingPostalCode))
                    {
                        loc.GeoX__c = account.GeoX__c;
                        loc.GeoY__c = account.GeoY__c;
                        loc.GeoApprox__c = account.GeoApprox__c;
                    } // if (((null == loc.GeoX__c) || (null == loc.GeoY__c) || ...
                } // for (SCInstalledBaseLocation__c loc :iblByOwner)
                
                // FIXME (TK): Updating of records in a getter might not be the best solution!!
                //update iblByOwner;

            } // if ((null != account.GeoX__c) && (account.GeoX__c > 0) && ...
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 'No installed base location found for owner ID ' + ownerId);        
        }
        return iblByOwner;
    } // getInstalledBaseLocationByOwner
    
    /**
     * Get all Installed Base Locations assigned to the account and the geo coordinates given.
     *
     * @param    ownerId Account ID that is connected to this location
     * @param    geoX    x coordinate
     * @param    geoY    y coordinate
     * @return List of all installed base locations for the given geo coordinates.
     *
     * @author Alexander Wagner <awagner@gms-online.de>
     * @version $Revision$, $Date$
     */
    public static List<SCInstalledBaseLocation__c> getInstBaseLocByGeoCoord(Id ownerId, 
                                                                            Decimal geoX, 
                                                                            Decimal geoY)
    {
        System.debug('##### getInstBaseLocByGeoCoord(): ownerId -> ' + ownerId);
        System.debug('##### getInstBaseLocByGeoCoord(): geoX    -> ' + geoX);
        System.debug('##### getInstBaseLocByGeoCoord(): geoY    -> ' + geoY);
        Date xToday = Date.today();
        List<SCInstalledBaseLocation__c> iblByOwner = new List<SCInstalledBaseLocation__c>();
        
        try
        {
            iblByOwner = [select Id, Name, LocName__c, Address__c, Street__c, HouseNo__c, Extension__c, Status__c, 
                                 FlatNo__c, Floor__c, CountryState__c, City__c, District__c, 
                                 County__c, Country__c, PostalCode__c, GeoX__c, GeoY__c, GeoApprox__c 
                            from SCInstalledBaseLocation__c
                           where GeoX__c = :geoX 
                             and GeoY__c = :geoY 
                             and Id in (
                                select InstalledBaseLocation__c
                                  from SCInstalledBaseRole__c
                                 where Account__c = :ownerId
                                   and Role__c in ('Owner', 'User') 
                                   and InstalledBaseLocation__c <> null
                                   and ValidFrom__c <= :xToday
                                   and (ValidTo__c >= :xToday or ValidTo__c = null))
                        order by LocName__c];
            
        }
        catch (Exception e)
        {
            System.debug(LoggingLevel.ERROR, 'No installed base location found for owner ID ' + 
                                             ownerId + ' and GeoX = ' + geoX + ', GeoY = ' + geoY);
        }
        return iblByOwner;
    } // getInstBaseLocByGeoCoord
}