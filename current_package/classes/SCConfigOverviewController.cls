/*
 * @(#)SCConfigOverviewController.cls SCCloud    hs 18.03.2011 
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 *
 * @author HS <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCConfigOverviewController 
{
    // the current endpoint as defined in the custom settings
    public String getEndpoint() 
    {
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
        String endpoint = aseService.endpoint_x;
        String service = 'services/ase';
        Integer pos = endpoint.indexOf(service);
        
        if (pos != -1)
        {
            return endpoint.substring(0, pos);
        }
        return endpoint;
    }

    // sync qualifications
    public String qualifications { get; set; }

    // sync calendars
    public String calendars { get; set; }

    // sync resources
    public String resources { get; set; }

    // sync domains
    public String domains { get; set; }

    // sync parameters
    public String parameters { get; set; }

    // true when running as test
    public static Boolean isTest = false;
    
    /**
     * Planning session handles ASE sessions and authorization
     */
    private SCPlanningSessionExtension planningSession;
    
    // severity of the webservice result, i.e. info, error,..
    public String severity { get; set; }
            
    // the last webservice error
    public String messageText { get; set; }
    
    // authorized or not to start master data jobs
    public Boolean authorized { get; set; }
    
    // frequency divider for the poll interval
    private Integer divider;
    
    /**
     * Returns the number of running synchronization jobs
     */
    public Integer runningJobs { get; set; }

    public String nullPostalcodes { get; set; }
    
    /**
     * Constructor
     */
    public SCConfigOverviewController()
    {
        planningSession = new SCPlanningSessionExtension(isTest);
        
        // initialize and..
        runningJobs = 0;
        authorized = true;
        init();
       
        // ..make the first callout immediately
        callout(null);
    }
    
    /**
     * Initializes the controller
     */
    private void init()
    {
        divider = 1;
        severity = 'info';
        messageText = 'Total synchronization jobs running: ' + runningJobs;
    }
    
    /**
     * Determines the number of jobs running by querying the control table
     *
     * @author hs 18.03.2011
     */
    public PageReference getJobs()
    {
        List<SCAseControl__c> aseControl = [select name from SCAseControl__c where activity__c = 'U' ];
        runningJobs = aseControl.size();
        init();
        return null;
    }
    
    /**
     * Determines the number of (active) contracts without postal code value.
     *
     * @author su 10.10.2011
     */
    public PageReference getContractsWithoutPostalcode()
    {
        List<SCContract__c> contracts = [Select Id 
                                             From SCContract__c 
                                             Where PostalCode__c = null 
                                             AND RecordType.DeveloperName = 'Contract' 
                                             Limit 15000];
                                             
        nullPostalcodes = (contracts.size() > 0) ? String.valueOf(contracts.size()) : null;
    
        return null;
    }
    
    /**
     * Refreshs the contracts without postal code (PostalCode__c)
     *
     * @author su 10.10.2011
     */
    public PageReference getRefreshContracts()
    {
        List<String> ids = new List<String>();
        List<sccontract__c> con = [ Select Id 
                                    From sccontract__c 
                                    Where postalcode__c = null 
                                    AND RecordType.DeveloperName = 'Contract' 
                                    Limit 15000 ];
        
        for(sccontract__c c : con)
        {
             ids.add( String.valueOf(c.id) );
        }
        
        System.debug('#### contract ids: ' + ids);
        
        SCbtcContractPostalCode.asyncPostalcodeContracts(ids);
    
        return null;
    } 
    
    /**
     * Refresh button click. Looks for checked items and initiates the callout
     */
    public PageReference refresh() 
    {
        List<String> operations = new List<String>();
        
        System.debug('Refresh: ' + domains + parameters + resources + calendars + qualifications);
        
        if (domains == 'true')
        {
            operations.add('refreshDomains');
        }

        if (parameters == 'true')
        {
            operations.add('refreshParameters');
        }

        if (resources == 'true')
        {
            operations.add('refreshResources');
        }

        if (calendars == 'true')
        {
            operations.add('refreshCalendars');
        }

        if (qualifications == 'true')
        {
            operations.add('refreshQualifications');
        }
        return controlOperation(operations);
    }

    public PageReference controlOperation(List<String> operations)
    {
        System.debug('Control operation: ' + operations);
        
        callout(operations);
        runningJobs = 1;
        
        List<SCAseControl__c> items = [ select id, name, activity__c from SCAseControl__c where name in :operations ];
        SCAseControl__c aseControl = null;
        
        for (String operation : operations)
        {
            Boolean itemFound = false;
            
            for (SCAseControl__c item : items)
            {
                if (item.name == operation)
                {
                    item.activity__c = 'U';
                    itemFound = true;
                    break;
                }         
            }

            if (!itemFound)
            {
                aseControl = new SCAseControl__c();
                aseControl.name = operation;
                aseControl.activity__c = 'U';
                items.add(aseControl);
            }                        
        }
        upsert(items);
        
        init();
        return null;
    }
    
    /**
     * Callout to webservice
     *
     * @author hs 18.03.2011
     */
    public void callout(List<String> refreshTypes)
    {
        // call the web service
        AseService.aseDataType[] dataType = null;
 
         // queries the ase web service for service control
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseParaSelectType[] paraSelects = new AseService.aseParaSelectType[0];
        AseService.aseParaSelectType paraSelect =  new AseService.aseParaSelectType();

        // prepare service control
        paraSelect.type_x = AseCalloutUtils.getDataTypeAsString(AseCalloutConstants.ASE_TYPE_SERVICECONTROL);
        paraSelect.keys = new String[0];
            
        // set the session ID
        paraSelect.keys.add(planningSession.sessionID);

        // add refresh type
        if (refreshTypes != null)
        {
            for (String refreshType : refreshTypes)
            {
                paraSelect.keys.add(refreshType);
            }
        }
        paraSelects.add(paraSelect);
               
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();
    
        try
        {
            System.debug('### Callout: GetServiceControl, paraSelects: ' + paraSelects);
            if (!SCConfigOverviewController.isTest)
            {
                dataType = aseService.get(tenant, paraSelects);
            }
            else
            {
                List<AseService.aseDataType> dat = new List<AseService.aseDataType>();
                AseService.aseDataType d = new AseService.aseDataType();
                d.dataEntries = new List<AseService.aseDataEntry>();
                dat.add(d);
                dataType = dat;
            }
        }
        catch (CalloutException e)
        {
            severity = 'error';
            messageText = e.getMessage();
            
            authorized = messageText.indexOf('Fault: sessionID') == -1;
            divider = authorized ? 4 : 1;
            return;
        }

        // unpack the response
        runningJobs = 0;
        
        if (dataType != null)
        {
            AseService.aseDataType serviceControl = dataType[0];
            runningJobs = serviceControl.dataEntries.size();
        }
        System.debug('Callout result: ' + runningJobs + ' entries');
        init();
    }       

    
    public String calcDueDateJobId {get; set;}
    public String createOrderJobId {get; set;}
    
    public String calcDueDateJobInfo {get; set;}
    public String createOrderJobInfo {get; set;}
    
    public PageReference onMaintenanceCalcDueDate()
    {
        calcDueDateJobId = SCbtcMaintenanceDueDateSet.asyncCalculateAll(0, 'no trace');
        
        return null;
    }
    public PageReference onMaintenanceCreateOrders()
    {
        createOrderJobId = SCbtcMaintenanceOrderCreate.asyncCreateAll(0, 'no trace');
        return null;
    }

    public PageReference onMaintenanceAbortCalcDueDate()
    {
        if(calcDueDateJobId != null)
        {
            System.abortJob(calcDueDateJobId);
            AsyncApexJob job = [SELECT ApexClassId,CompletedDate,CreatedBy.Name,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems 
                            FROM AsyncApexJob where id = :calcDueDateJobId];
        }                    
        return null;
    }
    public PageReference onMaintenanceAbortCreateOrders()
    {
        if(createOrderJobId != null)
        {
            System.abortJob(createOrderJobId);
            AsyncApexJob job = [SELECT ApexClassId,CompletedDate,CreatedBy.Name,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems 
                            FROM AsyncApexJob where id = :createOrderJobId];
        }                    
        return null;
    }

    public PageReference onMaintenanceRefreshCalcDueDate()
    {
        /*
        List<AsyncApexJob> jobList = [SELECT id, Status FROM AsyncApexJob where JobType = 'BatchApex' and ApexClass.Name = 'SCbtcMaintenanceDueDateSet' order by CreatedDate desc];
        if(jobList.size() > 0)
        {
            calcDueDateJobId = jobList[0].id;
        }
        if(calcDueDateJobId != null)
        {
        */
        Date weekBefore = Date.today().addDays(-7);
        List<AsyncApexJob> jobs = [SELECT ApexClassId,CompletedDate,CreatedBy.Name,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems 
                        FROM AsyncApexJob where JobType = 'BatchApex' and ApexClass.Name = 'SCbtcMaintenanceDueDateSet' and CreatedDate > : weekBefore order by CreatedDate desc];
        calcDueDateJobInfo = getJobInfo(jobs);                
        return null;
    }
    public PageReference onMaintenanceRefreshCreateOrders()
    {
    /*
        List<AsyncApexJob> jobList = [SELECT id FROM AsyncApexJob where JobType = 'BatchApex' and ApexClass.Name = 'SCbtcMaintenanceOrderCreate' order by CreatedDate desc];
        if(jobList.size() > 0)
        {
            createOrderJobId = jobList[0].id;
        }
        if(createOrderJobId != null)
        {
      */
        Date weekBefore = Date.today().addDays(-7);
        List<AsyncApexJob> jobs = [SELECT ApexClassId,CompletedDate,CreatedBy.Name,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status,TotalJobItems 
                        FROM AsyncApexJob where JobType = 'BatchApex' and ApexClass.Name = 'SCbtcMaintenanceOrderCreate' and CreatedDate > :weekBefore order by CreatedDate desc];
        createOrderJobInfo = getJobInfo(jobs);                        
        return null;
    }


    public String getJobsInfo()
    {
        onMaintenanceRefreshCalcDueDate();
        onMaintenanceRefreshCreateOrders();       
        return null;
    }

    public String getJobInfo(List<AsyncApexJob> jobs)
    {
        String retValue = 'no job';
        if(jobs.size() > 0)
        {
            retValue = '';
            Integer i = 0;
            for(AsyncApexJob job : jobs)
            {
                i++;
                if(i <= 2)
                {
                    retValue += 'Status: ' + job.Status + ', Gesendet am: ' + job.CreatedDate +  ', Datum der Fertigstellung: ' + job.CompletedDate + ', Abgesendet von: ' + job.CreatedBy.Name 
                    + ', Batches insgesammt: ' + job.TotalJobItems + ', Batches verarbeitet: ' + job.JobItemsProcessed + ', Fehler: ' + job.NumberOfErrors + '\n\n';
                }
                else
                {
                    break;
                }    
            }    
        }
        return retValue;
    }

}