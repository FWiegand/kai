/**
 * AseUnitTestTestService.cls   mt  28.09.2009
 * 
 * Copyright (c) 2009 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
 /*
  * Testclass
  */
@isTest
private class AseUnitTestTestService
{
    
    @isTest
    public static void testGetAll()
    {
       //### TODO HS: re-implement test
       // AseCalloutTestService.testGetAll(AseCalloutConstants.TEST_TENANT, AseCalloutConstants.ASE_TYPE_SCHEDULEPARA);
    } 
    
    @isTest
    public static void testGet()
    {
       //### TODO HS: re-implement test
       // AseCalloutTestService.testGet(AseCalloutConstants.TEST_TENANT, AseCalloutConstants.ASE_TYPE_SCHEDULEPARA, 'key1');
    } 
    
    @isTest
    public static void testGetDataFromWebservice()
    {
       //### TODO HS: re-implement test
       //AseCalloutTestService.testGetDataFromWebservice();
    } 

    @isTest
    public static void testGetAllDataFromWebservice()
    {
       //### TODO HS: re-implement test
       //AseCalloutTestService.testGetAllDataFromWebservice();
    } 
    
    @isTest
    public static void testGetHandleResponseData()
    {
       //### TODO HS: re-implement test
       // AseCalloutTestService.testGetHandleResponseData();
    }   
    
    @isTest
    public static void testRemove() 
    {    
        AseService.aseSOAP aseService = AseCalloutUtils.getAseSoap();

        AseService.aseParaSelectType[] paraSelects = new AseService.aseParaSelectType[0];
        AseService.aseParaSelectType paraSelect =  new AseService.aseParaSelectType();
        
        paraSelect.type_x = AseCalloutConstants.ASE_TYPE_SCHEDULEPARA_STR;
        paraSelect.keys = new String[0];
        paraSelect.keys.add('<dummy>');
        
        paraSelects.add(paraSelect);
        
        aseService.remove(AseCalloutConstants.TEST_TENANT, paraSelects);
    }        
        
}