/*
 * @(#)SCContactHistoryControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCContactHistoryControllerTest
{
    static testMethod void testHistoryForOrder()
    {
        SCHelperTestClass.createOrderTestSet(true);

        Test.startTest();
        Id orderId = SCHelperTestClass.order.Id;          
        ApexPages.currentPage().getParameters().put('oid', orderId);
        SCContactHistoryController controller = new SCContactHistoryController();
        System.assertEquals(0, controller.contacts.size());
        
        controller.newContact();
        controller.getPhone();
        controller.getPhone2();
        controller.getOrderName();
        controller.getContractName();
        System.assertEquals(true, controller.isNewEntry);
        System.assertEquals(false, controller.visitWithoutOrder);
        System.assertEquals(false, controller.canCreateOrder);
        System.assertNotEquals(null, controller.contHist);
        System.assertNotEquals('', controller.getMaintenanceDate());
        System.assertNotEquals(0, controller.getName().length());
        System.assertNotEquals(0, controller.getEngineer().length());
        
        controller.contHist.Status__c = 'FOLLOW_UP';
        controller.contHist.Reason__c = 'NO_REASON';
        controller.setNextCall();
        controller.saveContact();
        System.assertEquals(false, controller.isNewEntry);
        System.assertEquals(1, controller.contacts.size());
        
        controller.newContact();
        System.assertEquals(true, controller.isNewEntry);
        controller.cancel();
        System.assertEquals(false, controller.isNewEntry);

        Test.stopTest();
    } // testHistoryForOrder
    
    static testMethod void testHistoryForContract()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
                                        
        SCContractVisit__c visit = new SCContractVisit__c();
        visit.Contract__c = SCHelperTestClass2.contracts[0].Id;
        visit.Order__c = SCHelperTestClass.order.id;
        visit.DueDate__c = Date.Today().addDays(2);
        visit.Status__c = 'scheduled';
        insert visit;
        
        Test.startTest();
        Id contractId = SCHelperTestClass2.contracts[0].Id;          
        ApexPages.currentPage().getParameters().put('cid', contractId);
        SCContactHistoryController controller = new SCContactHistoryController();
        System.assertEquals(0, controller.contacts.size());
        
        controller.newContact();
        controller.getPhone();
        controller.getPhone2();
        controller.getOrderName();
        controller.getContractName();
        System.assertEquals(true, controller.isNewEntry);
        System.assertEquals(false, controller.visitWithoutOrder);
        System.assertEquals(false, controller.canCreateOrder);
        System.assertNotEquals(null, controller.contHist);
        System.assertNotEquals('', controller.getMaintenanceDate());
        System.assertNotEquals(0, controller.getName().length());
        System.assertNotEquals(0, controller.getEngineer().length());
        
        controller.contHist.Status__c = 'NO_NEED';
        controller.contHist.Reason__c = 'NO_NEED';
        controller.setNextCall();
        controller.saveContact();
        System.assertEquals(false, controller.isNewEntry);
        System.assertEquals(1, controller.contacts.size());
        
        SCContract__c contract = [select Id, Name, 
                                         EnableAutoScheduling__c, 
                                         EnableOrderCreation__c 
                                    from SCContract__c 
                                   where Id = :contractId];
        System.assertEquals(false, contract.EnableAutoScheduling__c);
        System.assertEquals(false, contract.EnableOrderCreation__c);
        
        
        ApexPages.currentPage().getParameters().put('cid', contractId);
        SCContactHistoryController controller2 = new SCContactHistoryController();
        System.assertEquals(1, controller2.contacts.size());
        
        controller2.newContact();
        System.assertEquals(true, controller2.isNewEntry);
        System.assertNotEquals(null, controller2.contHist);
        
        controller2.contHist.Status__c = 'FOLLOW_UP';
        controller2.contHist.Reason__c = 'NO_REASON';
        controller2.setNextCall();
        controller2.saveContact();
        System.assertEquals(false, controller2.isNewEntry);
        System.assertEquals(2, controller2.contacts.size());
        
        contract = [select Id, Name, ContactHistStatus__c, 
                           ContactHistReason__c, ContactHistNextCall__c 
                      from SCContract__c 
                     where Id = :contractId];
        System.assertEquals(controller2.contHist.Status__c, contract.ContactHistStatus__c);
        System.assertEquals(controller2.contHist.Reason__c, contract.ContactHistReason__c);
        System.assertEquals(controller2.contHist.NextCall__c, contract.ContactHistNextCall__c);

        Test.stopTest();
    } // testHistoryForContract

    static testMethod void testHistoryForOrder2()
    {
        SCHelperTestClass.createOrderTestSet2(true);

        Test.startTest();
        Id orderId = SCHelperTestClass.order.Id;          
        ApexPages.currentPage().getParameters().put('oid', orderId);
        SCContactHistoryController controller = new SCContactHistoryController();
        System.assertEquals(0, controller.contacts.size());
        
        controller.newContact();
        controller.contHist.Status__c = 'DONE';
        controller.contHist.Reason__c = 'NO_REASON';
        controller.saveContact();
        System.assertEquals(1, controller.contacts.size());
        
        Id historyId = controller.contacts[0].Id;          
        ApexPages.currentPage().getParameters().put('hid', historyId);
        SCContactHistoryController controller2 = new SCContactHistoryController();
        System.assertEquals(1, controller2.contacts.size());
        
        controller2.newContact();
        controller2.getPhone();
        controller2.getPhone2();
        controller2.getOrderName();
        controller2.getContractName();
        System.assertEquals(true, controller2.isNewEntry);
        System.assertNotEquals(null, controller2.contHist);
        System.assertNotEquals('', controller2.getMaintenanceDate());
        //CCE System.assertEquals(0, controller2.getName().length());
        //CCE System.assertEquals(0, controller2.getEngineer().length());
        
        controller2.contHist.Status__c = 'FOLLOW_UP';
        controller2.contHist.Reason__c = 'NO_REASON';
        controller2.setNextCall();
        controller2.saveContact();
        System.assertEquals(false, controller2.isNewEntry);
        System.assertEquals(2, controller2.contacts.size());

        Test.stopTest();
    } // testHistoryForOrder2
    
    static testMethod void testSkipNextVisit()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
                                        
        SCContractVisit__c visit = new SCContractVisit__c();
        visit.Contract__c = SCHelperTestClass2.contracts[0].Id;
        visit.DueDate__c = Date.Today().addDays(2);
        visit.Status__c = 'open';
        insert visit;
        
        Test.startTest();
        Id contractId = SCHelperTestClass2.contracts[0].Id;          
        ApexPages.currentPage().getParameters().put('cid', contractId);
        SCContactHistoryController controller = new SCContactHistoryController();
        System.assertEquals(0, controller.contacts.size());
        
        controller.newContact();
        System.assertEquals(true, controller.isNewEntry);
        System.assertEquals(true, controller.visitWithoutOrder);
        System.assertEquals(true, controller.canCreateOrder);
        
        controller.skipNextVisit();
        System.assertEquals(false, controller.visitWithoutOrder);
        System.assertEquals(false, controller.canCreateOrder);
        
        SCContractVisit__c visit2 = [select Id, Name, Status__c
                                       from SCContractVisit__c 
                                      where Id = :visit.Id];
        System.assertEquals('skip', visit2.Status__c);

        Test.stopTest();
    } // testSkipNextVisit
    
    static testMethod void testCreateOrder()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Insurance', 
                                        SCHelperTestClass2.insuranceTemplate.Id, 
                                        SCHelperTestClass.account.Id, 
                                        SCHelperTestClass.installedBaseSingle.Id, 
                                        true);
                                        
        SCContractVisit__c visit = new SCContractVisit__c();
        visit.Contract__c = SCHelperTestClass2.contracts[0].Id;
        visit.DueDate__c = Date.Today().addDays(2);
        visit.Status__c = 'open';
        insert visit;
        
        SCContractItem__c item = [select Id from SCContractItem__c 
                                   where Contract__c = :SCHelperTestClass2.contracts[0].Id limit 1];
        SCContractVisitItem__c visitItem = new SCContractVisitItem__c();
        visitItem.ContractVisit__c = visit.Id;
        visitItem.ContractItem__c = item.Id;
        insert visitItem;
        
        Test.startTest();
        Id contractId = SCHelperTestClass2.contracts[0].Id;          
        ApexPages.currentPage().getParameters().put('cid', contractId);
        SCContactHistoryController controller = new SCContactHistoryController();
        System.assertEquals(0, controller.contacts.size());
        
        controller.newContact();
        System.assertEquals(true, controller.isNewEntry);
        System.assertEquals(true, controller.visitWithoutOrder);
        System.assertEquals(true, controller.canCreateOrder);
        
        controller.createOrder();
        //controller.dispatchOrder();
        //System.assertEquals(false, controller.visitWithoutOrder);
        //System.assertEquals(false, controller.canCreateOrder);
        
        //SCContractVisit__c visit2 = [select Id, Name, Status__c, Order__c 
        //                               from SCContractVisit__c 
        //                              where Id = :visit.Id];
        //System.assertEquals('scheduled', visit2.Status__c);
        //System.assertNotEquals(null, visit2.Order__c);

        Test.stopTest();
    } // testCreateOrder
} // SCContactHistoryControllerTest