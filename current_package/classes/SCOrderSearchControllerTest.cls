/*
 * @(#)SCOrderSearchControllerTest.cls 
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checking the controler functions for order searching
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCOrderSearchControllerTest 
{
    private static SCOrderSearchController osc;

    static testMethod void orderSearchControllerPositiv1() 
    {
    
        // Order
        SCHelperTestClass.createOrderTestSet(true);
        // Order.Name -> SCHelperTestClass.order.Name;

        // Account (278)
        SCHelperTestClass.createAccountObject('Customer', true);
        Account account = SCHelperTestClass.account;
        
        // Order Role
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 
        //SCHelperTestClass.createAppointment(true);
    
        osc = new SCOrderSearchController();
        
        Test.startTest();  
            
        osc.textOutput = '';
        osc.sortDir = 'asc';
        osc.sortField = 'Name';
        
        osc.orderId = SCHelperTestClass.order.Id;
        
        osc.selorderrole = SCHelperTestClass.orderRole.OrderRole__c;
        //osc.productModel = SCHelperTestClass.prodModel.Name;
        osc.selorderstatus = SCHelperTestClass.order.Status__c;
        //osc.selordertype =  SCHelperTestClass.order.Type__c;
        
        osc.selmaterialstatus = SCHelperTestClass.order.MaterialStatus__c;
        osc.selinvoicingstatus = SCHelperTestClass.order.InvoicingStatus__c;
        osc.selinvoicingtype = SCHelperTestClass.order.InvoicingType__c;
        //osc.accountStreet = SCHelperTestClass.orderRole.Street__c;
        //osc.accountPostalCode = SCHelperTestClass.orderRole.PostalCode__c;
        //osc.accountCity = SCHelperTestClass.orderRole.City__c;

        osc.dateCreated.Start__c = null;
        osc.dateCreated.End__c = null;
        osc.dateDue.Start__c = null;
        osc.dateDue.End__c = null;
        osc.dateScheduled.Start__c = null;
        osc.dateScheduled.End__c = null;
        osc.dateStart.Start__c = null;
        osc.dateStart.End__c = null;
        osc.dateEnd.Start__c = null; 
        osc.dateEnd.End__c = null;
        osc.dateConclusion.Start__c = null;
        osc.dateConclusion.End__c = null;
        osc.dateFirst.Start__c = null;
        osc.dateFirst.End__c = null;
        osc.dateDesiredStart.Start__c = null;
        osc.dateDesiredStart.End__c = null;
        osc.dateDesiredEnd.Start__c = null;
        osc.dateDesiredEnd.End__c = null;
        osc.dateClosed.Start__c = null;
        osc.dateClosed.End__c = null;
        
        // Cancel Order
        osc.selcancelreason = '286001';
        osc.orderStatusId = SCHelperTestClass.order.Status__c;
        
        osc.dateCreated.Start__c = System.today() - 1;
        osc.dateCreated.End__c = System.today() + 1;

        osc.getOrderStatus();
        osc.getOrderTypes();
        osc.getContractStatus();
        osc.getMaterialStatus();
        osc.getInvoicingStatus();
        osc.getInvoicingType();
        osc.getProductGroup();
        osc.getProductType();
//        osc.getTimeWindow();
        osc.getOrderRoles();
        osc.getOrderChannel();
        
//        osc.getMultilingNone();
        osc.getAllDates();
        osc.getAllPresentDates();
        
        osc.toggleSort();
        osc.toggleSort();
        osc.onSearch();
        osc.getNumberOfResultsList();
        osc.getContractTemplates();
        osc.getFoundOrders();
        
        System.assertEquals('abc', osc.formatString('abc '));

        Date d = System.today();
        osc.getFormat(d);
        
        osc.dateInfo = 'created';
        osc.setDate();
        osc.dateInfo = 'closed';
        osc.setDate();
        osc.dateInfo = 'scheduled';
        osc.setDate();
        osc.dateInfo = 'start';
        osc.setDate();
        osc.dateInfo = 'end';
        osc.setDate();
        osc.dateInfo = 'desiredstart';
        osc.setDate();
        osc.dateInfo = 'desiredend';
        osc.setDate();
        osc.dateInfo = 'termination';
        osc.setDate();
        osc.dateInfo = 'first';
        osc.setDate();
        osc.dateInfo = 'conc';
        osc.setDate();
        osc.dateInfo = 'offer';
        osc.setDate();
        osc.dateInfo = 'due';
        osc.setDate();
        
        osc.doExportExcel();
        osc.doExportPDF();
        
        osc.getFoundOrdersForPDF();
        
        osc.getCancelReason();
        osc.toggleCancelledPopup();
        osc.delErrMsg();
        osc.orderId = SCHelperTestClass.order.Id;
        osc.selcancelreason = 'Customer';
        osc.cancelInfo = 'Info';
        osc.cancelOrder();        
        Test.stopTest();  

       // System.assert(osc.setControllerOrder.getRecords().size() > 0);
    }
    static testMethod void orderSearchControllerPositiv2() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        Account account = SCHelperTestClass.account;
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 
    
        Test.startTest();  
    
        osc = new SCOrderSearchController();
            
        osc.selorderrole = 'location';
        osc.productModel = null;
        osc.engineerSearchString = 'Tim';
        osc.getEngineers();
        osc.onSearch();
        osc.getFoundOrders();
        osc.getFoundOrdersForPDF();
        osc.onReset();
        
        Test.stopTest();  
    }  
    static testMethod void orderSearchControllerPositiv3() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createAccountObject('Customer', true);
        Account account = SCHelperTestClass.account;
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 
        
        Test.startTest();  
        
        osc = new SCOrderSearchController();
        osc.selorderrole = '';
        osc.getFoundOrders();
        
        Test.stopTest();  
    }  
    static testMethod void orderSearchControllerPositiv4() 
    {
        Test.startTest();  
        
        osc = new SCOrderSearchController();
        osc.tempDateParam = '1';
        osc.setDate();
        osc.tempDateParam = '2';
        osc.setDate();
        osc.tempDateParam = '3';
        osc.setDate();
        osc.tempDateParam = '4';
        osc.setDate();
        osc.tempDateParam = '5';
        osc.setDate();
        osc.tempDateParam = '6';
        osc.setDate();
        osc.tempDateParam = '7';
        osc.setDate();
        osc.tempDateParam = '8';
        osc.setDate();
        osc.tempDateParam = '9';
        osc.setDate();
        osc.tempDateParam = '10';
        osc.setDate();
        osc.tempDateParam = '11';
        osc.setDate();
        osc.tempDateParam = '12';
        osc.setDate();
        osc.tempDateParam = '13';
        osc.setDate();
        osc.tempDateParam = '14';
        osc.setDate();
        osc.tempDateParam = '15';
        osc.setDate();
        osc.tempDateParam = '16';
        osc.setDate();
        osc.tempDateParam = '17';
        osc.setDate();
        osc.tempDateParam = '18';
        osc.setDate();
        
        Test.stopTest();  
    }
    
    /**
     * Testing Query of Recall Reason and Priority Number
     *
     * @author Eugen Tiessen <etiessen@gms-online.de>
     */
    static testMethod void orderSearchControllerPositiv5() 
    {
        
        
        Test.startTest();  
        
        osc = new SCOrderSearchController();
        osc.onSearch();
        
        osc.selOrderRecallReason = '99999';
        osc.onSearch();
        
        osc.orderPriorityNumber = '1234';
        osc.onSearch();
        
        osc.selOrderRecallReason = '';
        osc.orderPriorityNumber = '1234';
        osc.onSearch();
        
        osc.getOrderRecallReason();
        
        Test.stopTest();  
    }  

}