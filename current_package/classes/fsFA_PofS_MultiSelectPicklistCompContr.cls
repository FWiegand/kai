/**
	* fsFA_PofS_MultiSelectPicklistCompContr - <description>
	* @author: Bernd Werner
	* @version: 1.0
*/

public with sharing class fsFA_PofS_MultiSelectPicklistCompContr{

	public String GV_Label{ Get; Set; }
	public Integer GV_Size{ Get; Set; }
	public List< SelectOption > GL_CompleteList{ Get; Set; }
	private Map< String, String > GM_CompleteList{
		Get{
			if( GM_CompleteList == null ){
				GM_CompleteList = new Map< String, String >();
				for( SelectOption LO_Entry: GL_CompleteList ){
					GM_CompleteList.put( LO_Entry.getValue(), LO_Entry.getLabel() );
				}
			}
			return GM_CompleteList;
		}
		Set;
	}
	public fsFA_PofS_SelectedValues GL_ChosenList{ 
		Get{
			if( GL_ChosenList == null ){
				GL_ChosenList = new fsFA_PofS_SelectedValues();
			}
			return GL_ChosenList;
		}
		Set; 
	}
	private Map< String, String > GM_ChosenList{
		Get{
			if( GM_ChosenList == null ){
				GM_ChosenList = new Map< String, String >();
				for( SelectOption LO_Entry: GL_ChosenList.GL_ChosenValues ){
					GM_ChosenList.put( LO_Entry.getValue(), LO_Entry.getLabel() );
				}
			}
			return GM_ChosenList;
		}
		Set;
	}
	public String GV_ChosenValues;
	
	public String getGV_ChosenValues(){
		return GV_ChosenValues;
	}
	
	public void setGV_ChosenValues( String PV_Value ){
		GV_ChosenValues = PV_Value;
		List< String > LL_ChosenValues = GV_ChosenValues.split( ';' );
		
		GL_ChosenList.GL_ChosenValues = new List< SelectOption >();
		for( String LV_ChosenValue: LL_ChosenValues ){
			//System.debug( 'setValue: ' + LV_ChosenValue + ', ' + GM_CompleteList.get( LV_ChosenValue ) );
			if( GM_CompleteList.get( LV_ChosenValue ) != null ){
				GL_ChosenList.GL_ChosenValues.add( new SelectOption( LV_ChosenValue, GM_CompleteList.get( LV_ChosenValue ) ) );
			}
		}
	}
	
	
}