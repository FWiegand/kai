/**
 * @(#)AseSetStatus.cls    ASE1.0 hs 14.10.2010
 * 
 * Copyright (c) 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn.
 * All Rights Reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 * 
 * $Id: SCReleaseJob.cls 7692 2010-01-18 16:18:41Z hschroeder $
 * 
 */
global class AseSetStatus
{
    private String appointmentID;
    private String assignmentStatus;
    
    /**
     * Data fields used by the scheduling engine
     */
    private String sessionID = 'gwin-0';
         
    /**
     * Context meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaContext = new String[]
    {
        'id',
        'userid'
    };
    
    /**
     * Job param meta data
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String[] metaParam = new String[]
    {
        'id',
        'key',
        'value' 
    };
    
    /**
     * Make the webservice call out
     *
     * @author HS <hschroeder@gms-online.de>
     */
     @Future(callout=true)
    public static void callout(String appointmentID, String ID2, String assignmentStatus)
    {
        AseSetStatus job = new AseSetStatus();
        job.appointmentID = appointmentID;
        job.assignmentStatus = assignmentStatus; 
        String tenant = UserInfo.getOrganizationID().toLowerCase();

        AseService.aseDataEntry dataEntry = new AseService.aseDataEntry();
        AseService.aseKeyValueType keyValue = new AseService.aseKeyValueType();
        
        // prepare context
        AseService.aseDataType contextParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaContext)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValueCtx(key) != null ? job.getValueCtx(key) : '';
            System.debug('Context: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        contextParam.dataEntries = new AseService.aseDataEntry[0];
        contextParam.dataEntries.add(dataEntry);
        contextParam.type_x = AseCalloutConstants.ASE_TYPE_CONTEXT_STR;
        
        // job params
        AseService.aseDataType jobParam = new AseService.aseDataType();
        dataEntry = new AseService.aseDataEntry();
        dataEntry.keyValues = new AseService.aseKeyValueType[0]; 

        for (String key : job.metaParam)
        {
            keyValue = new AseService.aseKeyValueType();
            keyValue.key = key;
            keyValue.value = job.getValue(key) != null ? job.getValue(key) : '';
            System.debug('Job param: ' + key + ' = ' + keyValue.value);
            dataEntry.keyValues.add(keyValue);
        }

        jobParam.dataEntries = new AseService.aseDataEntry[0];
        jobParam.dataEntries.add(dataEntry);
        jobParam.type_x = AseCalloutConstants.ASE_TYPE_STATUS_STR;
        
        AseService.aseDataType[] params = new AseService.aseDataType[]
        { 
            contextParam, jobParam
        };
        
        AseService.aseSOAP aseSoap = AseCalloutUtils.getAseSoap();
        aseSoap.endpoint_x = AseCalloutConstants.ENDPOINT;
        
        try
        {
            aseSoap.set(tenant, params);
            
        }
        catch (CalloutException e)
        {
            // log error to our table
            SCJobError__c jobError = new SCJobError__c();
            jobError.Name = appointmentID;
            jobError.ID2__c = ID2;
            jobError.ErrorText__c = e.getMessage();
            jobError.Status__c = assignmentStatus;
            jobError.Retry__c = true;
            jobError.RoleID__c = UserInfo.getUserRoleID();
            jobError.ProfileID__c = UserInfo.getProfileID();
            insert jobError;
        }
    }

    /**
     * Initialize the demand with the current user
     *
     * @author HS <hschroeder@gms-online.de>
     */
    private String getUserID()
    {
        String userID = UserInfo.getUserId();
        User user = [ select id, alias from user where user.id = :userID limit 1 ];
        return user.alias;
    }
   
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValueCtx(String key)
    {
        // meta key
        if (key == 'id') return sessionID;
        if (key == 'userid') return getUserID();

        return null;
    }
    
    /**
     * Get a named value
     *
     * @param key the attribute name
     * @return the translated value
     * @author HS <hschroeder@gms-online.de>
     */
    public String getValue(String key)
    {
        // param key        
        if (key == 'id') return appointmentID;
        if (key == 'key') return 'assignstat';
        if (key == 'value') return '' + assignmentStatus;
      
        return null;
    }         
}