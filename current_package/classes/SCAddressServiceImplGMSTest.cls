/*
 * @(#)SCAddressServiceImplGMSTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

/**
 * Test of the GMS address validation web service wrapper.
 * As web services can't be called in test cases we emulate some
 * calls to get the test coverage.
 */
@isTest
private class SCAddressServiceImplGMSTest {

    static testMethod void checkPositive1() {
        SCAddressServiceImplGMS service = new SCAddressServiceImplGMS();
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.city = 'Bahnhofstr 2 Paderborn';
        addr.matchInfo = 'test'; // emulation mode 

        AvsResult result = service.check(addr);
    }
    
    static testMethod void checkPositive2() {
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.postalcode = '33102';
        addr.city = 'Paderborn';
        addr.houseNumber = '2';
        addr.street = 'Bahnhofstr';
        addr.matchInfo = 'test'; // emulation mode 

        SCAddressServiceImplGMS service = new SCAddressServiceImplGMS();
        AvsResult result = service.check(addr);
    }
    
    static testMethod void checkPositive3() {
        SCAddressServiceImplGMS service = new SCAddressServiceImplGMS();
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        addr.city = 'Schulstr. 19 Paderborn';
        addr.matchInfo = 'test';// emulation mode 
            
        AvsResult result = service.check(addr);
    }

    static testMethod void checkNegative1() {
        SCAddressServiceImplGMS service = new SCAddressServiceImplGMS();
        AvsAddress addr = new AvsAddress();
        addr.country = 'de';
        AvsResult result = service.check(addr);
    }

    static testMethod void checkGeocoding(){
        AvsAddress addr = new AvsAddress();
        addr.country = 'nl';
        addr.city = 'Aduard';
        addr.houseNumber = '6';
        addr.street = 'Moorweg';
        addr.matchInfo = 'test';// emulation mode 
        
        SCAddressServiceImplGMS service = new SCAddressServiceImplGMS();
        AvsResult result = service.geocode(addr);
    }

    static testMethod void checkAvsServiceDummy(){
        // web services can't be tested directly - the following code is just for increasing coverage
        // no real test possible here
        AvsService s = new AvsService();
        AvsService.updateResponseType a1 = new AvsService.updateResponseType();
        AvsService.SearchAddressResponse_element a2 = new AvsService.SearchAddressResponse_element(); 
        AvsService.searchFieldsType a3 = new AvsService.searchFieldsType(); 
        AvsService.AddAddressRequest_element a4 = new AvsService.AddAddressRequest_element(); 
        AvsService.Address a5 = new AvsService.Address();  
        AvsService.SearchAddressRequest_element a6 = new AvsService.SearchAddressRequest_element();
        AvsService.UpdateIndexRequest_element a7 = new AvsService.UpdateIndexRequest_element();
        AvsService.UpdateIndexResponse_element a8 = new AvsService.UpdateIndexResponse_element();
        AvsService.AddAddressResponse_element a9 = new AvsService.AddAddressResponse_element();
        AvsService.AddressValidationSOAP a10 = new AvsService.AddressValidationSOAP();
    
        // web services can't be tested directly - the following code is just for increasing coverage
        // no real test possible here
        try
        {
            AvsService.AddressValidationSOAP a = new AvsService.AddressValidationSOAP();
            a.SearchAddress('tenant', 'de', 'mode','paderborn bahnhofstr');
        }
        catch(Exception e){}
        try
        {
            AvsService.AddressValidationSOAP a = new AvsService.AddressValidationSOAP();
            AvsService.searchFieldsType searchFields = new AvsService.searchFieldsType();
            a.SearchAddress('tenant','de','mode','quuery', searchFields);
        }
        catch(Exception e){}
        
        try
        {
            AvsService.AddressValidationSOAP a = new AvsService.AddressValidationSOAP();
            AvsService.Address addr = new AvsService.Address();
            a.AddAddress('tenant','de', addr);
            
        }
        catch(Exception e){}
        
        try
        {
            String[] country = new String[1];
            country[0] = 'de';
            AvsService.AddressValidationSOAP a = new AvsService.AddressValidationSOAP();
            a.UpdateIndex(country);
        }
        catch(Exception e){}
    }
}