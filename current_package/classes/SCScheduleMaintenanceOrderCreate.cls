/* 
 * @(#)SCScheduleMaintenanceOrderCreate.cls 
 * 
 * Copyright 2013 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */

global class SCScheduleMaintenanceOrderCreate implements Schedulable
{
    global void execute(SchedulableContext SC) 
    {
        // Alle
        SCbtcMaintenanceOrderCreate.asyncCreateAll(0, 'no trace');
    }
}