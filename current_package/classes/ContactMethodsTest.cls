/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=True)
private class ContactMethodsTest {

    static testMethod void myUnitTest() {
        
        scenario1();
        scenario2();
        scenario3();       
          
    }
    
    /*
    One Account with some Contacts. One contact is flagged as PrimarySales.
    */
    private static void scenario1() {
    	
    	Account testAcc = createAccount();
    	insert testAcc;
    	
    	List<Contact> testContacts = new List<Contact>(); 	
    	testContacts.add(createContact(testAcc, 'TestPSC001'));
    	testContacts.add(createContact(testAcc, 'TestPSC002'));
    	testContacts.add(createContact(testAcc, 'TestPSC003'));
    	testContacts.get(1).isPrimarySalesContact__c = True;
    	insert testcontacts;
    	
    	ContactAvoidTriggerRecursion.run = true;
    	
    	Contact c = [SELECT isPrimarySalesContact__c FROM Contact WHERE Name =: 'TestPSC002'];
    	
    	System.assert(c.isPrimarySalesContact__c);
    	
    }
    
    /*
    One Account with some Contacts. A new contact is flagged as PrimarySales. One old contact ist flagged too. 
    */
    private static void scenario2() {
    	
    	Account testAcc = createAccount();
    	insert testAcc;
    	
    	List<Contact> testContacts = new List<Contact>(); 	
    	testContacts.add(createContact(testAcc, 'TestPSC004'));
    	testContacts.get(0).isPrimarySalesContact__c = True;
    	insert testcontacts;
    	
    	ContactAvoidTriggerRecursion.run = true;
    	
    	List<Contact> testContacts2 = new List<Contact>(); 
    	testContacts2.add(createContact(testAcc, 'TestPSC005'));
    	testContacts2.add(createContact(testAcc, 'TestPSC006'));
    	testContacts2.get(1).isPrimarySalesContact__c = True;
    	insert testcontacts2;
    	
    	ContactAvoidTriggerRecursion.run = true;
    	
    	Contact c1 = [SELECT isPrimarySalesContact__c FROM Contact WHERE Name =: 'TestPSC004'];
    	Contact c2 = [SELECT isPrimarySalesContact__c FROM Contact WHERE Name =: 'TestPSC006'];
    	
    	System.assert(c1.isPrimarySalesContact__c == False);
    	System.assert(c2.isPrimarySalesContact__c == True);
   
    }
   	
   	/*
   	One account with two flagged contacts
   	*/ 
    private static void scenario3() {
    	Account testAcc = createAccount();
    	insert testAcc;
    	
    	List<Contact> testContacts = new List<Contact>();
    	testContacts.add(createContact(testAcc, 'TestPSC007'));
    	testContacts.add(createContact(testAcc, 'TestPSC008'));
    	testContacts.get(0).isPrimarySalesContact__c = True;
    	testContacts.get(1).isPrimarySalesContact__c = True;
    	insert testcontacts;
    	
    	Contact c1 = [SELECT isPrimarySalesContact__c FROM Contact WHERE Name =: 'TestPSC007'];
    	Contact c2 = [SELECT isPrimarySalesContact__c FROM Contact WHERE Name =: 'TestPSC008'];
    	
    	System.debug('CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 1 ' + c1);
    	System.debug('CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 1 ' + c2);
    	System.assert(c1.isPrimarySalesContact__c ^ c2.isPrimarySalesContact__c);
    	
    }
    
    
    
    private static Account createAccount() {
    	
    	Account a = new Account();
    	a.Name = 'TextName';
    	a.BillingCountry__c = 'DK';
    	a.RecordTypeId = [Select Id from RecordType where Name = 'Customer'  and SobjectType = 'Account' LIMIT 1].Id;	
    	
    	return a;
    }
    
    private static Contact createContact(Account a, String name) {
    	
    	Contact c = new Contact();
    	c.LastName = name;
    	c.AccountId = a.Id;
    	
    	return c;
    	
    }
    
}