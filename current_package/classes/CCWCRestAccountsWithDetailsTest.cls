/**********************************************************************
Name:  CCWCRestAccountsWithDetailsTest()
======================================================
Purpose:                                                            
Test class for CCWCRestAccountWithDetails.                                              
======================================================
History                                                            
-------                                                            
Date  	AUTHOR	DETAIL 
01/09/2013	Jan Mensfeld
***********************************************************************/

@isTest
private class CCWCRestAccountsWithDetailsTest {
	
	static Account getTestAccount() {
		Account testAcc = new Account();
		testAcc.Name = 'Test Account';
		return  testAcc;
	}

	static void insertTestSalesAppSettings() {
		String sQueryLimit = CCWCRestAccountsWithDetails.sSalesAppSettingNameForQueryLimit;		
		System.debug('Debug JM: ' + sQueryLimit);
		SalesAppSettings__c appSettingQueryLimit = new SalesAppSettings__c(Name = sQueryLimit, Value__c = '10');
		insert appSettingQueryLimit;
	}
	
    static testMethod void testDoGet() {
		insertTestSalesAppSettings();
		insert getTestAccount();
		
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/AccountsWithDetails';
		//req.addParameter('accountIds', '001c000000RCR5X');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		List<CCWCRestAccountsWithDetails.AccountWithDetails> resultList = CCWCRestAccountsWithDetails.doGet();
		System.assertEquals(1, resultList.size());
	}
}