/*
 * @(#)SCSendMessageController.cls    
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This controller sends an message like sms.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 public with sharing class SCSendMessageController
{
    public String messageText { get; set; }
    public SCOrder__c order { get; set; }
    public SCResource__c resource { get; set; }
    public Boolean showNoEngineerError { get; set; }
    SCMessage__c msg;
    public Boolean messageSent { get; set; }
       
   /**
    * Default constructor
    */ 
    public SCSendMessageController()
    {   
        // Order ID
        Id oid;
        if (ApexPages.currentPage().getParameters().containsKey('oid'))
        {
            oid = ApexPages.currentPage().getParameters().get('oid');
            order = [ Select Id, Name, Status__c From SCOrder__c Where Id = :oid ];
        }
        
        // Engineer ID
        // First case: get the id from url (eg. from planning board)
        // Second case: there is no rid in url (eg. from order layout) - get the id dependung on order id
        String rid;
        List<SCAssignment__c> ridList = new List<SCAssignment__c>();
        if (ApexPages.currentPage().getParameters().containsKey('rid'))
        {
            rid = ApexPages.currentPage().getParameters().get('rid');
            resource = [ Select Id, Mobile_txt__c, EMail_txt__c, Employee__c, FirstName_txt__c, LastName_txt__c From SCResource__c Where Id = :rid ];
        }
        else
        {        
            try {
                ridList = [ Select Resource__c From SCAssignment__c Where Order__c = :oid AND Status__c != '5507' ];
                rid = String.valueOf(ridList[0].get('Resource__c')); 
                System.debug('#### rid: ' + rid);               
                resource = [ Select Id, Mobile_txt__c, EMail_txt__c, Employee__c, FirstName_txt__c, LastName_txt__c From SCResource__c Where Id = :rid ];
                showNoEngineerError = false;
            } catch (QueryException e) {
                showNoEngineerError = true;
            }
              catch (ListException e) {
                showNoEngineerError = true;
            }
        }
    }
    
    
   /**
    * Send message method
    *
    * @return    Reference to the visualforce page
    */ 
    public PageReference onSendMessage()
    {
        SCutilMessage  msg = new SCutilMessage();
        msg.onMessageSend(order, messageText, resource);
        if(msg.send() != null)
            messageSent = true;
        else
            messageSent = false;
    
        return null;
    }
    
    
    
}