global without sharing class FsfaWSPromotionMemberAdder {
	
	WebService static OperationResult WsPromotionMemberAdder(String StrMembers, String StrPromotionId){
		
		// Define List for all Members
		List<String> ListDebNos 					= new List<String>();
		// Define List for first half of debitor numbers
		List<String> ListDebNosFirst 				= new List<String>();
		// Define List for second half of debitor numbers
		List<String> ListDebNosSecond 				= new List<String>();
		
		// Define List of all Objects to insert
		List<PromotionMember__c> ListPromMemInsert 	= new List<PromotionMember__c>();
		// Define List to get Promotion
		List<Promotion__c> ListPromotion 			= new List<Promotion__c>();
		// Map to allocate SAP customer numbers as key and Salesforce.com IDs as value
		Map<String,Id> MapMasterMap 				= new Map<String,Id>();
		// String to hold the original Promotion ID of Salesforce.com
		String StrPromotionSFDCId 					= '';
		// Result Object
		OperationResult ObjectResult 				= new OperationResult();
		
		// +++++++++++++++++++++++++++ STEP 1: Validate and query Promotion with the given external Id +++++++++++++++++++++++++++++++++++++++++++++
		// Query promotion
		try{
			ListPromotion = [Select PromotionID__c, Id From Promotion__c WHERE PromotionID__c = : StrPromotionId];
		}catch (Exception e){
			ObjectResult.addError(e.getMessage());
			return ObjectResult;
		}
		// Check, if we found a promotion
		if(ListPromotion.size()==0){
			ObjectResult.addError('No Promotion found for ID '+StrPromotionId+'.');
			system.debug('###Result: '+ObjectResult);
			return ObjectResult;
		}
		// Check, if we found to much promotions
		else if(ListPromotion.size()>1){
			ObjectResult.addError('Found '+ListPromotion.size()+' Promotions for PromotionID '+StrPromotionId+'.');
			return ObjectResult;
		}
		else{
			StrPromotionSFDCId = ListPromotion[0].Id;
			ObjectResult.addPromotion(StrPromotionSFDCId);
		}
		system.debug('####PromotionID: '+StrPromotionSFDCId);
		
		// +++++++++++++++++++++++++++ STEP 2: Check and Split List of given customer IDs and query customers +++++++++++++++++++++++++++++++++++++++++++++
		if(StrMembers.length()==0){
			ObjectResult.addError('No Promotion Member found. Code: 1');
			return ObjectResult;
		}
		// Fill the List of Debitor Numbers with the content of the delivered string
		ListDebNos = StrMembers.split(';',0);
		
		// check the list size. If more than 2000 return an error
		if (ListDebNos.size()>2000){
			ObjectResult.addError('Found '+ListDebNos.size()+' Promotion Member. Max. allowed are 2000.');
			return ObjectResult;
		}
		// check the list size. If 0 return an error
		if (ListDebNos.size()==0){
			ObjectResult.addError('No Promotion Member found. Code: 2');
			return ObjectResult;
		}
		// Split the ListDebNos into two lists to avoid govonor limit 
		integer counter = 0;
		for (String StrTmpDebNo : ListDebNos){
			if(counter<(ListDebNos.size()/2)){
				ListDebNosFirst.add(StrTmpDebNo);
			}else{
				ListDebNosSecond.add(StrTmpDebNo);
			}
			counter ++;
			system.debug('###Counter: '+counter);
		}
		// Do a query and add all found IDs to the MasterMap for the first half of the given IDs, but only if the list is filled
		if(ListDebNosFirst.size()>0){
			for (Account ObjAccCustomer :[SELECT Id, ID2__c from Account where ID2__c IN : ListDebNosFirst] ){
				MapMasterMap.put(ObjAccCustomer.ID2__c,ObjAccCustomer.Id);
			}
		}
		// Do a query and add all found IDs to the MasterMap for the second half of the given IDs, but only if the list is filled
		if(ListDebNosFirst.size()>0){
			for (Account ObjAccCustomer :[SELECT Id, ID2__c from Account where ID2__c IN : ListDebNosSecond] ){
				MapMasterMap.put(ObjAccCustomer.ID2__c,ObjAccCustomer.Id);
			}
		}
		// system.debug('###MasterMap: '+MapMasterMap);
		// +++++++++++++++++++++++++++ STEP 3: Prepare objects and insert +++++++++++++++++++++++++++++++++++++++++++++
		for (String StrCustNo : ListDebNos){
			if (MapMasterMap.containsKey(StrCustNo)){
				// Create Objekt of promotion member and add to insert list
				PromotionMember__c ObjTmpPromoMember	= new PromotionMember__c();
				ObjTmpPromoMember.Account__c			= MapMasterMap.get(StrCustNo);
				ObjTmpPromoMember.Promotion__c 			= StrPromotionSFDCId;
				ObjTmpPromoMember.Status__c				= 'Signed';
				ListPromMemInsert.add(ObjTmpPromoMember);
				
			}else{
				ObjectResult.addNotFound(StrCustNo);
			}
		}
		try{
			insert ListPromMemInsert;
		}
		catch (Exception e){
			ObjectResult.addError(e.getMessage());
			return ObjectResult;
		}
		return ObjectResult;
	
	}
	
	
	/************************************************************************
	**
	**	Class to prepare and return a reult to the caller
	**
	************************************************************************/
	global class OperationResult{
		
		// Class variables
		webservice String MessageUUID{get;set;} 
		webservice String faultAccounts{get;set;}
		webservice String promotion{get;set;}
		webservice String error{get;set;}
		
		// Constructor
		private OperationResult(){
			MessageUUID='5257AA3FC97C0FE0E10080000A621094';
		}
		
		public boolean addNotFound(String accountNo){
			if (faultAccounts=='' || faultAccounts==null){
				this.faultAccounts = accountNo;
			} else {
				this.faultAccounts +=','+accountNo;
			}
			return true;
		}
		
		public boolean addPromotion(String promo){
			this.promotion = promo;
			return true;
		}
		public boolean addError(String StrError){
			this.error = StrError;
			return true;
		}
	}
}