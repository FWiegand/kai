/*
 * @(#)SCOrderProtocolControllerTest.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * @author Sebastian Schrage
 */
@isTest
private class SCOrderProtocolControllerTest
{
    static testMethod void CodeCoverageA()
    {
        SCOrder__c order = new SCOrder__c();
        Database.insert(order);
        
        Database.insert(new List<SCOrderLog__c>{
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 1',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 2',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 3',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 4',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName()),
                    new SCOrderLog__c(CreatedDate__c = DateTime.now(),
                                      Info__c        = 'Test 5',
                                      SCOrderId__c   = order.Id,
                                      SubType__c     = '5501',
                                      Type__c        = '11701',
                                      Username__c    = UserInfo.getUserName())
                });
        
        
        Test.startTest();
        
        
        SCOrderProtocolController obj = new SCOrderProtocolController();
        obj.getNotePrefix();
        obj.oid = order.Id;
        
        
        Test.stopTest();
    }
}