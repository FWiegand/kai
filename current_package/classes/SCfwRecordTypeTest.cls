/*
 * @(#)SCfwRecordTypeTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
 */
 @isTest
private class SCfwRecordTypeTest {

	/**
	 * Get a list of all Account record types using the empty constructor
	 */
    static testMethod void getRecordTypesPositive1() {
        SCfwRecordType rt = new SCfwRecordType();
        
        rt.sObjectType = 'Account';
        
        List<RecordType> rtList = rt.getRecordTypesAsList();
        
        System.assert(rtList.size() > 0);
        
    }
    
	/**
	 * Get a list of all Account record types using the constructor
	 * with sObjectType as parameter
	 */    
	static testMethod void getRecordTypesPositive2() {
        SCfwRecordType rt = new SCfwRecordType('Account');
        
        List<RecordType> rtList = rt.getRecordTypesAsList();
        
        System.assert(rtList.size() > 0);
        
    }
    
    
    /**
	 * Try to get a list of record types for an invalid object 
	 * forcing an error.
	 */    
	static testMethod void getRecordTypesNegative1() {
        SCfwRecordType rt = new SCfwRecordType('InvalidXXXObject');
        
        List<RecordType> rtList = rt.getRecordTypesAsList();
        
        System.assertEquals(0, rtList.size());
        
    }
    
    /**
	 * Get a list of all Account record types using the constructor
	 * with sObjectType as parameter
	 */    
	static testMethod void getRecordTypesOptionListPositive1() {
        SCfwRecordType rt = new SCfwRecordType('Account');
        
        List<SelectOption> rtList = rt.getRecordTypes();
        
        // REMARK: You always have the option 'none' in the list
        System.assert(rtList.size() > 1);
        
    }
    
    
    /**
	 * Try to get a list of record types for an invalid object 
	 * forcing an error.
	 */    
	static testMethod void getRecordTypesOptionListNegative1() {
        SCfwRecordType rt = new SCfwRecordType('InvalidXXXObject');
        
        List<SelectOption> rtList = rt.getRecordTypes();
        
        // REMARK: You always have the option 'none' in the list
        System.assertEquals(1, rtList.size());
        
    }
    
    /**
	 * Get a list of all Account record types using the constructor
	 * with sObjectType as parameter
	 */    
	static testMethod void getPersonTypePositive1() {
        SCfwRecordType rt = new SCfwRecordType('Account');
        
        RecordType personRT = rt.getPersonType();
        
        System.assertNotEquals(null, personRT);
        
    }
    
    
    /**
	 * Try to get a list of record types for an invalid object 
	 * forcing an error.
	 */    
	static testMethod void getPersonTypeNegative1() {
        SCfwRecordType rt = new SCfwRecordType('InvalidXXXObject');
        
        RecordType personRT = rt.getPersonType();
        
        System.assertEquals(null, personRT);
        
    }
    
    /**
	 * Test if the given ID is a record type for a person
	 * account.
	 */    
	static testMethod void isPersonTypePositive1() 
	{
		Id personTypeId = [select Id
							 from RecordType 
							limit 1].Id; 
		Test.startTest();
		
        SCfwRecordType rt = new SCfwRecordType('Account');
        
        Boolean isPersonType = rt.isPersonType(personTypeId);
        
        Test.stopTest();
        
        System.assertEquals(false, isPersonType);
        
    }
    
    /**
	 * Test if the given ID is a record type for a person
	 * account.
	 */ 
	static testMethod void isPersonTypeNegative1() 
	{
		Id personTypeId = [select Id
							 from RecordType 
							limit 1].Id; 
		Test.startTest();
		
        SCfwRecordType rt = new SCfwRecordType('Account');
        
        Boolean isPersonType = rt.isPersonType(personTypeId);
        
        Test.stopTest();
        
        System.assertEquals(false, isPersonType);
        
    }    
    
    
}