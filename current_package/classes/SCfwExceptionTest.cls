/*
 * @(#)SCfwExceptionTest.cls
 * 
 * Copyright 2010 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * This class contains the test code for SCfwException.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
*/ 
@isTest
private class SCfwExceptionTest
{
    /**
     * Test the basic exception this class depends on
     */
    static testMethod void scExceptionTest1()
    {
        try
        {
            throw new SCfwException();
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing two arguments
     */
    static testMethod void scExceptionTest2()
    {
        try
        {
            throw new SCfwException(true, 'Message');
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key
     */
    static testMethod void scExceptionTest3()
    {
        // known key
        try
        {
            throw new SCfwException(1000);
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }

        // unknown key
        try
        {
            throw new SCfwException(1);
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key and a parameter
     */
    static testMethod void scExceptionTest4()
    {
        try
        {
            throw new SCfwException(1301, '1234567890');
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }

    /**
     * Test new constructor containing a key and two parameter
     */
    static testMethod void scExceptionTest5()
    {
        try
        {
            throw new SCfwException(1200, 'XXX', 'TEST');
        }
        catch (SCfwException e)
        {
            // proper exception matched
            System.assert(true);    
        }
        catch (Exception e)
        {
            // wrong exception matched
            System.assert(false);
        }
    }
}