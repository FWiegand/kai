/*
 * @(#)SCInventoryCreateController.cls
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class creates a new Inventory for the selected Stock.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
public with sharing class SCInventoryCreateController
{

    private Id sid;
    public SCInventory__c tempInventory { get; set; }
    public SCStock__c stock { get; private set; }
    public SCInventory__c inventory { get; set; }
    public List<SCInventoryItem__c> inventoryItems { get; set; }
    public SCboInventory boInventory = new SCboInventory();
    public String messages { get; set; }
    public Boolean canProcess { get; set; }
    public Boolean mustEnterStock { get; set; }
    
    /**
     * Constructor
     *
     * Here the Inventory object is readed for use at the PDF and Excel export pages.
     */
    public SCInventoryCreateController()
    {            
        // If this controller used only for reading of inventorys information (export as PDF or Excell)
        // we must read the whole Inventory object to shos this informations at these pages.
        if(ApexPages.currentPage().getParameters().containsKey('invId') && ApexPages.currentPage().getParameters().get('invId') != '')
        {
            inventory = [Select Id, Name, Stock__c, Stock__r.Name, Stock__r.ValuationType__c, toLabel(Status__c), Plant__c, PlannedCountDate__c,  
                                 LastModifiedDate, LastModifiedById, IsDeleted, InventoryDate__c, 
                                 Full__c, FiscalYear__c, toLabel(ERPStatus__c), ERPResultDate__c, 
                                 Description__c, CurrencyIsoCode, CreatedDate, CreatedById, CreatedBy.Name,
                                (Select Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
                                        CurrentQty__c, CountedQty__c, Inventory__c, Article__c, toLabel(ValuationType__c), 
                                        ArticleName__c, StockItem__c, Article__r.ArticleNameCalc__c, Article__r.Name
                                 From InventoryItem__r order by Article__r.Name, ValuationType__c)
                          From SCInventory__c 
                          Where Id = : ApexPages.currentPage().getParameters().get('invId') ];
                          
            inventoryItems = [Select Id, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
                                     CurrentQty__c, CountedQty__c, Inventory__c, Article__c, toLabel(ValuationType__c), 
                                     ArticleName__c, StockItem__c, Article__r.ArticleNameCalc__c, Article__r.Name
                              From SCInventoryItem__c
                              Where Inventory__c = :ApexPages.currentPage().getParameters().get('invId')
                              Order by Article__r.Name, ValuationType__c];
        }
        
    }
    
    /**
     * Standard controller constructor
     *
     * Here the Stock-ID is read from the URL (sid) as well as the current stock object.
     * After that a Stock-Validation is started to determine whether we can create a new inventory.
     *
     */
    public SCInventoryCreateController(ApexPages.StandardController controller)
    {    
        // Temporary Inventory helper object. The new inventory information that user entered are stored in this object.
        tempInventory = new SCInventory__c(FiscalYear__c = String.valueOf(Date.today().year()), Full__c = true, PlannedCountDate__c = Date.TODAY());
        
        // Inventory object to show at the PDF / Excel pages because that pages are use this controller too
        inventory = null;
        inventoryItems = null;
        
        // Checking whether the Stock-ID param exists in the URL string
        if(ApexPages.currentPage().getParameters().containsKey('sid') && ApexPages.currentPage().getParameters().get('sid') != '')
        {
            // Reading the Stock-ID
            sid = ApexPages.currentPage().getParameters().get('sid');
            // Reading the current stock
            stock = [Select Id, Name, Plant__c, Locked__c, Plant__r.Name From SCStock__c Where id = : sid ];
            // Validating the current stock
            canProcess = validate();
            mustEnterStock = false;
        }
        else
        {
            sid = null;
            messages = System.Label.SC_msg_PleaseEnterInformation + ': ' + sObjectType.SCStock__c.label;
            canProcess = false;
            mustEnterStock = true;
        }
    }
    
    /**
     * Reads a stock from the user input
     */
    public PageReference readStock()
    {
        sid = tempInventory.Stock__c;
        stock = [Select Id, Name, Plant__c, Locked__c, Plant__r.Name From SCStock__c Where id = : tempInventory.Stock__c ];
        
        canProcess = validate();
        
        return null;
    }
    
    /**
     * Validates the current Stock object
     *
     * The validation rules are:
     * - there may be only one open Inventory (status = created) for the current Stock
     * - the resource of the stock may have no open assignmets (status != 5506, 5507, 5508)
     * - the may have no open material movements
     * - the stock may have no not invoiced material movements (not implemented at the moment)
     *
     * @return true or false
     */   
    private Boolean validate()
    {
        /* PMS 34444/0000005265: Create Inventory - remove validation rules on assignments and material movements
        GMSNA, KW, Hr. Schmitz: 08.02.2013 - Validation disabled as the inventory shall 
        be possible independently of pendings material movements and assignments
        */
        return true;
    
        /*
        Boolean returnValue = false;
        Boolean inventoryRule = false;  
        Boolean engineerRule = false;
        Boolean matMovementRule = false;
        
        // The error message string to be shown at the page
        messages = '<ul style="margin:0;padding:0;">';
        
        inventoryRule = true;
        
        SCStock__c currentStock = [ Select util_StockSelector__c, ValuationType__c, TotalArticles__c, TotalArticleCount__c, SystemModstamp, 
                                           StockProfileOptimisation__c, StockOptimizationStart__c, StockOptimizationRuleSet__c, Replenishment__c, 
                                           Plant__c, Name, MaxWeight__c, MaxValue__c, Info__c, Id, ID2__c, Description__c, AutoApprove__c, 
                                          (Select ValidFrom__c, ValidTo__c, Resource__c, Resource__r.Name
                                           From Resource_Assignments__r 
                                           Where ValidFrom__c <= TODAY 
                                           AND ValidTo__c >= TODAY 
                                           Limit 1), 
                                          (Select Id, Name
                                           From Material_Movement__r
                                           Where Type__c = '5202' 
                                           AND Status__c = '5402'), 
                                          (Select Id, Name
                                           From StockInventory__r
                                           Where Status__c = 'created')
                                    From SCStock__c
                                    Where Id = : sid ];
                                    
        // There may be only one open Inventory (status = created) for the current Stock
        if(!currentStock.StockInventory__r.isEmpty())
        {
            messages += '<li style="margin-bottom:7px;">Only one open inventory allowed. This stock already have ' + currentStock.StockInventory__r.size() + ': ';
            for(SCInventory__c inv : currentStock.StockInventory__r)
            {
                messages += '<a style="margin:0;" href="/' + inv.id + '" target="_blank">' + inv.name + '</a></br>';
            }
            
            messages += '</li>';
             
            inventoryRule = false;
        }
        else
        {
            inventoryRule = true;
        }
        
        // The resource of the stock may have no open assignmets (status != 5506, 5507, 5508)
        if(!currentStock.Resource_Assignments__r.isEmpty())
        {
            //5502    Planned
            //5503    Transferred
            //5509    Planned for precheck
            //5510    Released for processing
            
            //5505    Completed offline    
            //5506    Completed            
            //5507    Cancelled            
            //5508    Closed final         

            List<SCAssignment__c> assignments = new List<SCAssignment__c>();
            assignments = [ Select Id, Name, Resource__c, Status__c, Order__c, Order__r.Id, toLabel(Order__r.Status__c), Order__r.Name, CreatedDate, Order__r.CreatedDate
                            From SCAssignment__c 
                            Where Resource__c = :currentStock.Resource_Assignments__r[0].Resource__c 
                            AND Status__c IN ('5502','5503','5509','5510')  ];
            
            if(assignments.isEmpty())                                      
            {
                engineerRule = true;
            }
            else
            {
                engineerRule = false;
                messages += '<li style="margin-bottom:7px;">There are pending assignments. ';
                messages += '<a style="margin:0;" href="/' + currentStock.Resource_Assignments__r[0].Resource__c + '" target="_blank">' + currentStock.Resource_Assignments__r[0].Resource__r.Name + '</a> ';
                messages += 'have ' + assignments.size() + ' open assignments: <a style="margin:0;" href="javascript:void(0);" onclick="jQuery(\'#openAsignments\').slideToggle(250);">show</a>.</br>';
                
                messages += '<div style="display:none;" id="openAsignments"><br/>';
                messages += '<table border="0" cellspacing="3" width="100%">';
                messages += '<tr>';
                messages += '<td><b>' + sObjectType.SCAssignment__c.label + '</b></td>';
                messages += '<td><b>' + sObjectType.SCAssignment__c.fields.CreatedDate.label + '</b></td>';
                messages += '<td><b>' + sObjectType.SCOrder__c.label + '</b></td>';
                messages += '<td><b>' + sObjectType.SCOrder__c.fields.Status__c.label + '</b></td>';
                messages += '</tr>';
                for(SCAssignment__c ass : assignments)
                {
                    messages += '<tr>';
                    messages += '<td><a style="margin:0;" href="/' + ass.id + '" target="_blank">' + ass.name + '</a></td>';
                    messages += '<td>' + ass.CreatedDate + '</td>';
                    messages += '<td><a style="margin:0;" href="/' + ass.Order__r.id + '" target="_blank">' + ass.order__r.name + '</a></td>';
                    messages += '<td>' + ass.order__r.Status__c + '</td>';
                    messages += '</tr>';
                }
                messages += '</table>';
                messages += '</div>';
                messages += '</li>';
            }
        }
        else
        {
            engineerRule = true;
        }

        
        // The stock may have no open material movements
        if(currentStock.Material_Movement__r.isEmpty())
        {
            matMovementRule = true;
        }
        else
        {
        
            matMovementRule = true;
            messages += '<li style="margin-bottom:7px;">There should be no open material movements, ' + currentStock.Material_Movement__r.size() + ' found: ';
            messages += '<a style="margin:0;" href="javascript:void(0);" onclick="jQuery(\'#openMatMovements\').slideToggle(250);">show</a>.</br>';
            
            messages += '<div style="display:none;" id="openMatMovements">';            
            for(SCMaterialMovement__c m : currentStock.Material_Movement__r)
            {
                messages += '<a style="margin:0;" href="/' + m.id + '" target="_blank">' + m.name + '</a></br>';
            }
            messages += '</div>';
            messages += '</li>';            
       
        }
        
        // If all three rules are right - the return value will be true
        if(inventoryRule && engineerRule && matMovementRule)
        {
            returnValue = true;
        }
        
        System.debug('#### inventoryRule: ' + inventoryRule);
        System.debug('#### engineerRule: ' + engineerRule);
        System.debug('#### matMovementRule: ' + matMovementRule);

        messages += '</ul>';
        
        return returnValue;
        */
    }
    
    /**
     * Creates a new Inventory object and locks the stock (locked__c = true)
     *
     * @return pageReference to the new inventory standard page
     */   
    public PageReference createInventory()
    {
        try
        {
            // Creating a new inventory object
            Id inventoryId = SCboInventory.createInventory(sid, 
                                                           tempInventory.Description__c, 
                                                           tempInventory.FiscalYear__c, 
                                                           tempInventory.Full__c, 
                                                           tempInventory.PlannedCountDate__c);
            
            // Locking the stock
            stock.locked__c = true;
            // Updating a stock
            upsert stock;
            
            // Forwarding user to the new inventory
            PageReference p = new PageReference('/' + inventoryId);
            
            return p;
        }
        catch(Exception e)
        {
            // Error 
            System.debug('#### Cannot create inventory: ' + e.getMessage());
        }
        
        
        return null;
    }
    
    private String checkLength(String val)
    {
        if(String.isNotBlank(val) && val.length() == 1)
        {
            val = '0' + val;
        }
        
        return val;
    }
    
    /**
     * Creates a timestamp of the current date and time to be shown at PDF / Excel pages
     *
     * @return string timestamp JJJJMMDDHHMMSS 
     */  
    public String getTimeStamp()
    {
        Datetime dt = Datetime.NOW();
        String day = checkLength(String.valueOf(dt.day()));
        String month = checkLength(String.valueOf(dt.month()));
        String hour = checkLength(String.valueOf(dt.hour()));
        String minute = checkLength(String.valueOf(dt.minute()));
        String second = checkLength(String.valueOf(dt.second()));
        
        String timeStr = String.valueOf(dt.year()) + month + day + '-' + hour + minute + second;
        
        String timeStamp = '';
        timeStamp += 'INVENTORY-';
        timeStamp += inventory.Plant__c.remove(' ') + '-';
        timeStamp += inventory.Stock__r.Name.remove(' ') + '-';
        timeStamp += inventory.name.remove(' ') + '-';
        timeStamp += timeStr;
        
        return timeStamp;
    }
    
    /**
     * Returns the user back to the stock page
     *
     * @return pageReference to the stock page 
     */  
    public PageReference goBack()
    {
        PageReference p = new PageReference('/' + sid);
        
        return p;
    }    
    
}