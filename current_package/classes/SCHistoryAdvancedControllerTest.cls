/*
 * @(#)SCHistoryAdvancedControllerTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Test class checks the controler functions for the history function
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCHistoryAdvancedControllerTest
{
    private static SCHistoryAdvancedController hac;

    static testMethod void historyAdvancedControllerPositiv1() 
    {
    
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);

        Test.startTest();
        
        hac = new SCHistoryAdvancedController();
        
        hac.oid = SCHelperTestClass.order.Id;
        hac.mode = 'order';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
        doc.contractID = SCHelperTestClass2.contracts.get(0).Id;
        
        SCHistoryAdvancedController.ViewModeOrderContract i = new SCHistoryAdvancedController.ViewModeOrderContract();
        
        hac.selectedViewIndex = '0';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '1';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '2';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '3';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        SCHistoryAdvancedController.HistoryBase hb = new SCHistoryAdvancedController.HistoryBase();
        hb.getSubItemCount();
        hb.getIcon();
        
        SCHistoryAdvancedController.HistoryOrder ho = new SCHistoryAdvancedController.HistoryOrder();
        ho.getSubItemCount();
        ho.getIcon();
        
        SCHistoryAdvancedController.HistorySystem hs = new SCHistoryAdvancedController.HistorySystem();
        hs.getSubItemCount();
        hs.getIcon();
        
        
        
        SCHistoryAdvancedController.HistoryContract  hco = new SCHistoryAdvancedController.HistoryContract();
        hco.getSubItemCount();
        hco.getIcon();
        
        SCHistoryAdvancedController.HistoryInstalledBaseLocation ibl = new SCHistoryAdvancedController.HistoryInstalledBaseLocation();
        ibl.getSubItemCount();
        ibl.getIcon();
        
        SCHistoryAdvancedController.HistoryInstalledBases ib = new SCHistoryAdvancedController.HistoryInstalledBases();
        ib.getSubItemCount();
        ib.getIcon();
        
        SCHistoryAdvancedController.HistoryInstalledBase ib2 = new SCHistoryAdvancedController.HistoryInstalledBase();
        ib2.getSubItemCount();
        ib2.getIcon();
        
        SCHistoryAdvancedController.ViewModeInstalledBase vmib = new SCHistoryAdvancedController.ViewModeInstalledBase();
        vmib.onReadKeySet(doc);
        Set<String> s = new Set<String>{'dummy'};
        vmib.onReadData(doc,s);
        
        
        hac.selectedId = SCHelperTestClass.order.Id;
        hac.selectedType = 'order';
        hac.getReadDetails();
        
        Test.stopTest();
    }
    
    static testMethod void historyAdvancedControllerPositiv2() 
    {
    
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);

        Test.startTest();
        
        hac = new SCHistoryAdvancedController();
        
        hac.oid = SCHelperTestClass2.contracts.get(0).Id;
        hac.mode = 'contract';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
               
        hac.selectedViewIndex = '0';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '1';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '2';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedViewIndex = '3';
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedId = SCHelperTestClass2.contracts.get(0).Id;
        hac.selectedType = 'contract';
        hac.getReadDetails();
        Test.stopTest();
    }
    
    static testMethod void historyAdvancedControllerPositiv3() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        hac = new SCHistoryAdvancedController();

        Test.startTest();
        
        hac.oid = SCHelperTestClass.account.Id;
        hac.mode = 'account';
        
        hac.selectedViewIndex = '0';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
        
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        Test.stopTest();
    }
    
    static testMethod void historyAdvancedControllerPositiv4() 
    {
    
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);

        Test.startTest();
        
        hac = new SCHistoryAdvancedController();
        
        hac.oid = SCHelperTestClass.installedBaseSingle.Id;
        hac.mode = 'installedbase';
        
        hac.selectedViewIndex = '0';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
        
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();

        hac.selectedId = SCHelperTestClass.installedBaseSingle.Id;
        hac.selectedType = 'ibase';
        hac.getReadDetails();
        Test.stopTest();
    }
    
    static testMethod void historyAdvancedControllerPositiv5() 
    {
    
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        Test.startTest();
        hac = new SCHistoryAdvancedController();
        
        hac.oid = SCHelperTestClass.location.Id;
        hac.mode = 'installedbaselocation';
        
        hac.selectedViewIndex = '0';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
        
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedId = SCHelperTestClass.location.Id;
        hac.selectedType = 'ibaselocation';
        hac.getReadDetails();
        Test.stopTest();
    }
    
    static testMethod void historyAdvancedControllerPositiv6() 
    {
        SCHelperTestClass.createAccountObject('Customer', true);
        SCHelperTestClass.createInstalledBaseLocation(true);
        SCHelperTestClass.createInstalledBase(true);
        SCHelperTestClass.createProductModel(true);
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.orderRolePerson(SCHelperTestClass.order, SCHelperTestClass.account, '50301', true); 

        Account account = SCHelperTestClass.account;
        
        SCHelperTestClass2.createContractTemplates(true);
        SCHelperTestClass2.createContract('Contract', SCHelperTestClass2.insuranceTemplate.Id, SCHelperTestClass.account.Id, SCHelperTestClass.installedBaseSingle.Id, true);
        
        Test.startTest();
        hac = new SCHistoryAdvancedController();
        
        hac.oid = SCHelperTestClass.location.Id;
        hac.mode = 'installedbaselocation';
        
        hac.selectedViewIndex = '0';
        
        SCHistoryAdvancedController.ViewDoc doc = new SCHistoryAdvancedController.ViewDoc();
        
        hac.getViews();
        hac.getItems();
        hac.onViewSelected();
        
        hac.selectedId = null;
        hac.selectedType = 'case';
        hac.getReadDetails();
        
        hac.selectedId = null;
        hac.selectedType = 'system';
        hac.getReadDetails();
        
        Test.stopTest();
    }
}