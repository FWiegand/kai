/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class CCWCRestAccountsWithinRadiusTest {
		
	static void insertTestAccounts() {
		List<Account> listAcc = new List<Account>();
		for(Integer i = 0; i < 10; i++) {
			Account acc = new Account();
			acc.LastName__c = 'testaccount ' + i;
	        acc.FirstName__c = 'Person';
	        acc.BillingPostalCode = '33106 PB';
	        acc.BillingCountry__c = 'DE';
	        acc.BillingStreet = 'Karl-Schurz Str.';
	        acc.BillingHouseNo__c = '29';
	        acc.BillingPostalCode = '33100 PB';
	        acc.BillingCity = 'Paderbrorn ' + i;
	        acc.BillingCountry__c = 'PB';
	        acc.GeoX__c = 13.52;
	        acc.GeoY__c = 52.50 + (Double)i / 10;
	        acc.GeoApprox__c = false; 
	        listAcc.add(acc);
		}
        insert listAcc;       
	}
	
	static void insertTestSalesAppSettings() {
		String sMaxResults = CCWCRestAccountsWithinRadius.sSalesAppSetting_ProximitySearchMaxResults_Name;
		String sRadiusM = CCWCRestAccountsWithinRadius.sSalesAppSetting_ProximitySearchRadiusM_Name;
		
		SalesAppSettings__c appSettingMaxAccounts = new SalesAppSettings__c(Name = sMaxResults, Value__c = '50');
		insert appSettingMaxAccounts;
		
		SalesAppSettings__c appSettingSearchRadiusM = new SalesAppSettings__c(Name = sRadiusM, Value__c = '90000');
		insert appSettingSearchRadiusM;
	}
	
    static testMethod void testCalBetweenLocationAndAccount() {
    	// Berlin
    	Double dLon1 = 13.4105300;
    	Double dLat1 = 52.5243700;
    	// Hamburg
    	Double dLon2 = 10.0000000;    	
    	Double dLat2 = 53.5500000;
		CCWCRestAccountsWithinRadius.Coordinate objSourceCoordinate = new CCWCRestAccountsWithinRadius.Coordinate(dLon1, dLat1);
		CCWCRestAccountsWithinRadius.Coordinate objDestinationCoordinate = new CCWCRestAccountsWithinRadius.Coordinate(dLon2, dLat2);
		
		Test.startTest();
		Double dDistance = CCWCRestAccountsWithinRadius.calcDistanceBetweenLocationAndAccount(objSourceCoordinate, objDestinationCoordinate);
		Test.stopTest();
		// Verified on http://www.movable-type.co.uk/scripts/latlong.html look at 'haversine'
		System.assert(254850 < dDistance && dDistance < 254950);
	}
	
    static testMethod void testDoGet() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
		req.requestURI = URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/AccountsWithinRadius';
		req.addParameter('longitude','13.0');
		req.addParameter('latitude', '52.0');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
				
		insertTestAccounts();
		insertTestSalesAppSettings();
		
		List<Account> testAccs = [SELECT Id, GeoX__c, GeoY__c FROM Account LIMIT 5000];
		System.assertEquals(testAccs.size(), 10);		
		//System.debug('JM DEBUG: ' + testAccs);
		
		List<CCWCRestAccountsWithinRadius.AccountWithinRadius> results = CCWCRestAccountsWithinRadius.doGet();
		System.assertNotEquals(results, null);
		System.assertEquals(3, results.size());	
    }
}