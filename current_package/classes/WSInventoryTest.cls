@isTest (SeeAllData = true)
private class WSInventoryTest
{
    static testMethod void test1cancelInventory()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
        
        String retVal = WSInventory.cancelInventory(inventoryId);
    }
    
    static testMethod void test1closeInventory()
    {
        SCHelperTestClass.createOrderTestSet(true);
        SCHelperTestClass.createTestArticles(true);
        SCHelperTestClass.createTestStockItem(SCHelperTestClass.stocks[0].id, SCHelperTestClass.article.id, true);
        
        Id inventoryId = SCboInventory.createInventory(SCHelperTestClass.stocks[0].id, 
                                                       'Test Description', 
                                                       '2013', 
                                                       true, 
                                                       Date.today());
                                                       
        String retVal = WSInventory.closeInventory(inventoryId, false, Date.today());
    }
}