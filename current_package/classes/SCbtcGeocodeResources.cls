// TMP no production code 2011.07.25 GMSNA

/*
 * @(#)SCbtcGeocodeResources.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Selects non geocoded resources and calls the address validation and 
 * geocoding service for each address (SCResourceAssignment). This helper
 * function is intended to support the migration of resources.
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
global class SCbtcGeocodeResources extends SCbtcGeocodeBase
    implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{

    private ID batchprocessid = null;
    String country = 'DE';
    Integer max_cycles = 0;
    String email = null;
	private Boolean approximately = false;
    
    // the counter of the calls to the execute function
    // by the countries with double web call for geo coding 
    // such as NL, UK this counter is used to skip the even
    // record to allow the odd record the making of the second web call
    Integer callCnt = 0;
    // The limit of out calls. Its value is read from ApplicationSettings
    Integer limitOfOutCalls = 100;
    String emailData = '';

    // Subject of the email
    global String jobinfo;    
    global String summary;
    
        
    // The address validation results (temporary)
    public AvsResult addrResult {get; set;}
    public AvsResult geocodingResult {get; set;}
    private AvsAddress checkAddress = null;
    private AvsAddress checkAddress2 = null; // because geocode sets GeoX and GeoY to 0 in the input data

    List<String> resourceIdList = null;    // resourceIdList = null, normal batch call
    String msg = '';
    /**
     * if the contract id is null the batch job for all contract visit dates is started
     * if the contract id is not null the batch job for the given contract is started
     */
    private Id resourceId = null;

    // Object for application settings
    private static SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    private static Integer batchSize =  (Integer) appSettings.BTC_BATCH_SIZE_GEOCODE_RESOURCES__c;  
    static 
    {
		System.debug('###...batchSize: ' + batchSize);		
	    if(batchSize == null)
	    {
	    	batchSize = 5;
	    }
    }
    // the intern resource object
    public SCResourceAssignment__c resource { get; private set; }

    //--<implementation>--------------------------------------------------------------
    global final String mode;

    public String employee;
    
   /**
    * API entry for starting the batch job for contracts
    * @param processingMode    for geocoding records with GeoStatus__c <> 'Invalid'
    *                          ALLNEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL_NEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL     geocdes all records (also the already geocoded)
    *                          SIM_ALLNEW  like ALLNEW but only simulation
    *                          SIM_ALL     like ALL but only simulation
    *                        
    *                          for geocoding records with GeoStatus__c = 'Invalid'
    *                          ALL_INVALID   
    *                          SIM_ALL_INVALID  like ALL_INVALID but only simulation
    *
    * @param country    the country code (e.g. DE, GB)
    * @param email      the email of the operator to get the message after finishing the job
    * @param max_cycles 
    * @param traceMode  if = 'trace' or 'test' the traces are written into a log file 
    */
    WebService static ID asyncGeocodeAll(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        if(!isAllowed(processingMode))
        {
            SCfwException e = new SCfwException(true, 'The processing mode \'' + processingMode + '\' is not allowed!');
            throw e;
        }
        System.debug('###traceMode: ' + traceMode);
        SCbtcGeocodeResources btc = new SCbtcGeocodeResources(processingMode, country, email, max_cycles, traceMode);
		if(SCbtcGeoCodeBase.isDoubleCall(country))
		{
			batchSize = 5;
		}
		else
		{
			batchSize = 10;
		}
		
        // only one resource in one call to execute
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncGeocodeAll

   /**
    * API entry for starting the batch job for contracts
    * @param processingMode    for geocoding records with GeoStatus__c <> 'Invalid'
    *                          ALLNEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL_NEW  geocodes only new records not yet geocoded (geox = 0 and geoy =0). 
    *                          ALL     geocdes all records (also the already geocoded)
    *                          SIM_ALLNEW  like ALLNEW but only simulation
    *                          SIM_ALL     like ALL but only simulation
    *                        
    *                          for geocoding records with GeoStatus__c = 'Invalid'
    *                          ALL_INVALID   
    *                          SIM_ALL_INVALID  like ALL_INVALID but only simulation
    *
    * @param country    the country code (e.g. DE, GB)
    * @param email      the email of the operator to get the message after finishing the job
    * @param max_cycles 
    * @param traceMode  if = 'trace' or 'test' the traces are written into a log file 
    */
    WebService static ID asyncGeocodeAllApproximately(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        if(!isAllowed(processingMode))
        {
            SCfwException e = new SCfwException(true, 'The processing mode \'' + processingMode + '\' is not allowed!');
            throw e;
        }
        System.debug('###traceMode: ' + traceMode);
        Boolean approximately = true;
        SCbtcGeocodeResources btc = new SCbtcGeocodeResources(processingMode, country, email, max_cycles, traceMode, approximately);
		if(SCbtcGeoCodeBase.isDoubleCall(country))
		{
			batchSize = 5;
		}
		
        // only one resource in one call to execute
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncGeocodeAll

    /**
     * checks whether the processing mode is allowed.
     * It returns true if it is allowed, else it returns false
     */
    private static Boolean isAllowed(String processingMode)
    {
       String modes = 'ALLNEW, ALL_NEW, ALL, SIM_ALLNEW, SIM_ALL, ALL_INVALID, SIM_ALL_INVALID';
       List<String> modeList = modes.split(',');
       Boolean ret = SCfwCollectionUtils.isContained(processingMode, modeList);
       return ret;
    }



    /**
     * Geocode asynchronically one resource found on the base of resource id.
     * If the resource id is not null the batch job for the given resource is started.
     * @param resourceId
     * @param traceMode    'trace' or 'test' cause the trace to a log file
     */
    public static ID asyncGeocodeResource(Id resourceId, String traceMode)
    {
        String country = 'XX'; // dummy only - will be determined from the resource
        if(resourceId != null)
        {
            List<SCResourceAssignment__c> cList = [Select Country__c from SCResourceAssignment__c where Id = :resourceId];
            if(cList.size() > 0)
            {
                country = cList[0].Country__c;
                Integer max_cycles = 1;
                String processingMode = null;
                String email = null;
	            Boolean approximately = true;
                SCbtcGeocodeResources btc = new SCbtcGeocodeResources(resourceId, processingMode, country, email, max_cycles, traceMode, approximately);
				batchSize = 5; // could be double call
                // only one order in one call to execute
                btc.batchprocessid = btc.executeBatch(batchSize);
                return btc.batchprocessid;
            }    
        }
        return null;
    } // asyncGeocodeResource


    /**
     * geocodes asynchronically on the base of list of resource
     * @param resource list
     */
    WebService static ID asyncGeocodeResources(List<String> resourceIdList)
    {
        if(resourceIdList == null || resourceIdList.size() == 0)
        {
            return null;
        }   
        SCbtcGeocodeResources btc = new SCbtcGeocodeResources(resourceIdList, 'trace');
		batchSize = 5; // could be double call
        // only one order in one call to execute
        btc.batchprocessid  = btc.executeBatch(batchSize);
        return btc.batchprocessid;
    } // asyncGeocodeResource

 
    /**
     * Synchronically calls geocoding of the resource list
     *
     * @param resource list
     */ 
    Webservice static void syncGeocodeResources(List<String> resourceIdList)
    {
        if(resourceIdList == null || resourceIdList.size() == 0)
        {
            return;
        }   
        System.debug('###......resourceIdList: ' + resourceIdList);
        Integer max_cycles = 1;
        SCbtcGeocodeResources btc = new SCbtcGeocodeResources(resourceIdList, 'test');        
            
        List<SCResourceAssignment__c> resourceList = [select id, name, Resource__c, Country__c, CountryState__c, County__c, Employee__c, 
                                        PostalCode__c, City__c, District__c , Street__c, HouseNo__c, Extension__c, 
                                        GeoX__c, GeoY__c 
                                        from SCResourceAssignment__c 
                                        where 
                                        Id in :resourceIdList];
        
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
        for(SCResourceAssignment__c res: resourceList)
        {
            // validate a resource
            btc.debug('resource id: ' + res.Id);
            Boolean sync = true;
            btc.country = res.Country__c;
            if(btc.country != null)
            {
                btc.country = btc.country.toLowerCase();
            }
			
            btc.geocode(res, sync, interfaceLogList);
        }
        try
        {
	        if(resourceList.size() > 0)
	        {
	        	update resourceList;
	        }	
        }
		catch(Exception e)
		{
			for(SCInterfaceLog__c il: interfaceLogList)
			{
				il.Data__c = SCfwException.getExceptionInfo(e) + '\n' + il.Data__c;
			}
		}
		finally
		{    
	        if(interfaceLogList.size() > 0)
	        {	
	        	insert interfaceLogList;
	        }
		}    	
    }
    
    /**
     * Synchronically calls geocoding of the resource
     */ 
    Webservice static void syncGeocodeResource(String resourceId)
    {
        if(resourceId == null || resourceId == '')
        {
            return;
        }   
        System.debug('###......resourceId: ' + resourceId);
        Integer max_cycles = 1;
        String processingMode =  null;
        String email = null;
        List<SCInterfaceLog__c> interfaceLogList = new List<SCInterfaceLog__c>();
            
        List<SCResourceAssignment__c> resourceList = [select id, name, Resource__c, Country__c, CountryState__c, County__c, Employee__c, 
                                        PostalCode__c, City__c, District__c , Street__c, HouseNo__c, Extension__c, 
                                        GeoX__c, GeoY__c 
                                        from SCResourceAssignment__c  
                                        where Id = :resourceId];
        

        for(SCResourceAssignment__c res: resourceList)
        {
            // geocode an resource
            String countryLoc = res.Country__c;
            Boolean approximately = true;
            SCbtcGeocodeResources btc = new SCbtcGeocodeResources(resourceId, processingMode, countryLoc, email, max_cycles, 'test', approximately);        
            btc.debug('resource id: ' + res.Id);
            Boolean sync = true;
            btc.geocode(res, sync, interfaceLogList);
        }
		try
		{
	        if(resourceList.size() > 0)
	        {
	        	update resourceList;
	        }
		}
		catch(Exception e)
		{
			for(SCInterfaceLog__c il: interfaceLogList)
			{
				il.Data__c = SCfwException.getExceptionInfo(e) + '\n' + il.Data__c;
			}
		}
		finally
		{    
	        if(interfaceLogList.size() > 0)
	        {	
	        	insert interfaceLogList;
	        }
		}    	
    }
    
    /**
    * Constructors.
    * The parameter names are explicitly written.
    */
    public SCbtcGeocodeResources(String processingMode, String country, String email, Integer max_cycles, String traceMode)
    {
        this(null, processingMode, country, email, max_cycles, traceMode, false);
    }

    public SCbtcGeocodeResources(String processingMode, String country, String email, Integer max_cycles, String traceMode, Boolean approximately)
    {
        this(null, processingMode, country, email, max_cycles, traceMode, approximately);
    }


    public SCbtcGeocodeResources(List<String> resourceIdList, String traceMode)
    {
        // in case of this call the country is not regarded
        this(null, 'ALL', 'XX', null, 0, traceMode, true);
        this.resourceIdList = resourceIdList;
    }


    /**
     * Constructor 
     * @param resourceId set for working out the specified resource
     * @param processingMode
     * @param country
     * @param email
     * @param max_cycles
     * @param traceMode
     */
    public SCbtcGeocodeResources(Id resourceId, String processingMode, String country, String email, Integer max_cycles, String traceMode, Boolean approximately)
    {    
        this.approximately = approximately;
        this.resourceId = resourceId;
        if(country != null)
        {
            country = country.toLowerCase();
        }    
        this.country = country;
        this.processingMode = processingMode;
        if(this.processingMode != null)
        {
            this.processingMode = this.processingMode.toUpperCase();
        }
        this.email = email;
        this.max_cycles = max_cycles;
        this.traceMode = traceMode;
        System.debug('###traceMode: ' + traceMode);
        debug('traceMode: ' + traceMode);
        Decimal decLimitOfOutCalls = appSettings.BATCH_GEOCODE_WEB_CALLS_DAILY_MAXIMUM__c;
        if(decLimitOfOutCalls != null)
        {
            limitOfOutCalls = decLimitOfOutCalls.intValue();
        }
        // Prepare job details that are used in the notification message
        String tmp = 'EPS Batch Job - geocode country [' + country + '] ';
        if(processingMode == 'ALL')
        {
            tmp += 'all resources (re-geocode)';
        }
        else if(processingMode == 'ALLNEW' || processingMode == 'ALL_NEW')
        {
            tmp += 'all non geocoded resources';                
        }
        else if(processingMode == 'SIM_ALL')
        {
            tmp += 'simulation all resources (re-geocode)';
        }
        else if(processingMode  == 'SIM_ALLNEW')
        {
            tmp += 'simulation all non geocoded resources';                
        }
        else 
        {
            tmp += 'warning: unknnown mode [' + mode + '] expected ALL, ALLNEW or SIM_ALL, SIM_ALLNEW - using ALLNEW as default';
            //this.mode = 'ALLNEW';
        }
        jobinfo = tmp;
    }

    

   /*
    * Called by the framework when the batch job is started. We construct the soql statement
    * for reading the resource assignments.
    * @param BC the batch context
    * @return the query locator with the selected material movements (up to 50 mio records possible)
    */
    global override Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'select id, name, Resource__c, Country__c, CountryState__c, County__c, Employee__c, PostalCode__c, City__c, District__c , Street__c, HouseNo__c, Extension__c, GeoX__c, GeoY__c from SCResourceAssignment__c where country__c = \'' + country + '\' ';
        if(resourceId == null)
        {
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALLNEW')
                	|| processingMode.equalsIgnoreCase('ALL_NEW')
                    || processingMode.equalsIgnoreCase('SIM_ALLNEW')))
            {            
                query += ' and GeoStatus__c = null '
                      + ' and (GeoX__c = 0 or GeoX__c = null )and (GeoY__c = 0 or GeoY__c = null) ';
            }
            if(processingMode != null 
                && (processingMode.equalsIgnoreCase('ALL_INVALID')
                    || processingMode.equalsIgnoreCase('SIM_ALL_INVALID')))
            {
                query += ' and GeoStatus__c = \'Invalid\' ';
            }    
    

            if(max_cycles > 0 )
            {
                query += ' limit ' + max_cycles;
            }
        }
        else
        {
            query += ' and id = \'' + resourceId + '\'';

        }
        if(resourceIdList != null)
        {
            debug('resourceIdList: ' + resourceIdList);
            return Database.getQueryLocator([select id, name, Resource__c, Country__c, CountryState__c, County__c, Employee__c, 
                                        PostalCode__c, City__c, District__c , Street__c, HouseNo__c, Extension__c, 
                                        GeoX__c, GeoY__c 
                                        from SCResourceAssignment__c 
                                        where 
                                        Id in :resourceIdList]);
        }
        debug('query: ' + query);

        return Database.getQueryLocator(query);
    } // start

   /*
    * Called for each batch of records to process.
    * @param BC the batch context
    * @param scope the list records to be processed
    */
    global override void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        debug('execute');
        breakJobByExhaustedDailyCallouts(BC, limitOfOutCalls, callCnt);

        Integer size = scope.size();
		debug('batchSize: ' + batchSize);		


        Boolean sync = false;
        for(sObject res: scope)
        {
            debug('sObject: ' + res);
            SCResourceAssignment__c workResourceAss = (SCResourceAssignment__c)res;
            country = workResourceAss.Country__c;
            if(country != null)
            {
                country = country.toLowerCase();
            }
            geocode(workResourceAss, sync, null);
        }// for
        if(processingMode == null  || !processingMode.startsWith('SIM'))
        {
	        if(scope.size() > 0)
	        {
	        	update scope;
	        }
        }    	
    } // execute

    public static final String STEP_BEGIN = 'BEGIN';
    public static final String STEP_DBL_FIRST = 'DBL_FIRST';
    public static final String STEP_DBL_SECOND = 'DBL_SECOND';
    public static final String STEP_SINGLE = 'SINGLE';
    
    public void geocode(SCResourceAssignment__c workResource, Boolean sync, List<SCInterfaceLog__c> interfaceLogList)
    {
        // increase the counter of outgoing web calls
        debug('geocode');
        callCnt++;
        
        DateTime start = Datetime.now();
        Boolean thrownException = false;
        Boolean secondCallMade = false;
        String resultInfo = '';
        Integer count = 1;
        String data = '';
        String step = STEP_BEGIN;
        try
        {
            if(isDoubleCall(country))
            {
                debug('double country: ' + country);
                // we make geo coding for an every odd record from batch
                // because we need two outgoing web calls.
                // The first web call we do by odd number of records.
                // The second web call we do by even number of records
                // for the result we got by the previous call by the odd number.

                step = STEP_DBL_FIRST;

                // checking
                debug('odd or sync');
                // get the list or moniker
                resource = (SCResourceAssignment__c)workResource;
                employee = resource.Employee__c;

                SCAddressValidation validation = new SCAddressValidation();
                checkAddress = getAddr(resource);
                debug('resource: ' + resource);
                debug('check address: ' + checkAddress);
                checkAddress2 = getAddr(resource);

                // prevent from taking addrResult of the first resource of previous pair of resources
                addrResult = null;
                geocodingResult = null;
                addrResult = validation.check(checkAddress2);
                if(addrResult.status == AvsResult.STATUS_EMPTY)
                {
                    debug('the address has been not found: ' + checkAddress2);
                }
                debug('addrResult: ' + addrResult);
                if(addrResult.items != null && addrResult.items.size() > 0)
                {
                    debug('the first call: addrResult.items[0] : ' + addrResult.items[0]);
                }
                
                callCnt++;                        

                step = STEP_DBL_SECOND;
                
                // geocoding
                debug('even or sync');
                // get the geocoding
                validation = new SCAddressValidation();
                if(addrResult != null && addrResult.items != null && addrResult.items.size() > 0
                    && addrResult.status != AvsResult.STATUS_EMPTY)
                {
                    Integer indexOfAddr = getIndex(checkAddress2, addrResult);
                    AvsAddress a = addrResult.items[indexOfAddr];
                    debug('the second call: addrResult.items[' + indexOfAddr + '] : ' + addrResult.items[indexOfAddr]); 
                    
                    geocodingResult = validation.geocode(a);
                    debug('geocodingResult: ' + geocodingResult);
                    debug('geocodingResult.size(): ' + geocodingResult.items.size());
                    resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, geocodingResult, resource);
                    secondCallMade = true;    
                }
                else
                {
                	workResource.GeoStatus__c = 'Invalid';
	               	workResource.GeoX__c = 0;
	               	workResource.GeoY__c = 0;
                    resultInfo = 'The address could not be found. ';
                }    
            }
            else
            {
                step = STEP_SINGLE;
                
                // an only geocoding call
                resource = (SCResourceAssignment__c)workResource;
                employee = resource.Employee__c;
                SCAddressValidation validation = new SCAddressValidation();
                checkAddress = getAddr(resource);
				data += '\naddress' + checkAddress;
                debug('resource: ' + resource);
                debug('check address: ' + checkAddress);
                
                checkAddress2 = getAddr(resource);
                geocodingResult = validation.geocode(checkAddress2); // geocode sets GeoX and GeoY to 0
                Integer geoCodingSize = geocodingResult.items.size();
                data += '\ngeocodingResult: ' + geoCodingResult;
                debug('geocodingResult: ' + geocodingResult);
                addrResult = null;
                
				if(geoCodingSize >= 1)
				{
                	resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress, addrResult, 
                    	geocodingResult, resource);
				}
				else if(approximately)
				{
					// approximately call remove HouseNo
	                resource = (SCResourceAssignment__c)workResource;
	                SCAddressValidation validation2 = new SCAddressValidation();
	                checkAddress = getAddr(resource);
	                checkAddress.housenumber = null;
					data += '\naddress' + checkAddress;
	                debug('resource: ' + resource);
	                debug('check address: ' + checkAddress);
	                
	                checkAddress2 = getAddr(resource);
	                checkAddress2.housenumber = null;
	                AvsResult geocodingResult2 = validation2.geocode(checkAddress2); // geocode sets GeoX and GeoY to 0
	                data += '\ngeocodingResult2: ' + geoCodingResult2;
	                debug('geocodingResult2: ' + geocodingResult2);
	                addrResult = null;
                	resultInfo = setGeoCode(sync, isDoubleCall(country), checkAddress2, addrResult, 
                    	geocodingResult2, resource);
				}    	
            }
         }
        catch(Exception e)
        {
            thrownException = true;
            resultInfo += 'Exception: ' + e.getMessage();
            String prevMode = traceMode;
            traceMode = 'test';
            debug('resultInfo:' + resultInfo);
            traceMode = prevMode;
            count = 0;
        }
        finally
        {
            /**
            E000 Success
            E001 Success with Info
            E100 Service not reachable
            E101 Service processing failed
            */
            String resultCode = 'E000';
            if(thrownException
               || resultInfo.startsWith('Invalid'))
            {
                resultCode = 'E101';
            }
            // protocoll only the events that something happend
            if(!isDoubleCall(country) 
                || isDoubleCall(country) && isEven(callCnt)
                || thrownException)
            {
                debug('last check address: ' + checkAddress);
                String dataFieldSeparator = ',';
                Id resourceId = null;
                if(resource != null)
                {
                    resourceId = resource.Id;
                }
                data += '\n' + getData(checkAddress, addrResult, geocodingResult, resourceId, dataFieldSeparator, employee);
                if(workResource.GeoY__c == null || workResource.GeoY__c == 0)
                {
                	resultCode = 'E101';
                	resultInfo += 'Error in input data.'; 
                }
                String emailFieldSeparator = '|';
                emailData += '\n' + getEmailData(checkAddress, addrResult, geocodingResult, 
                    resourceId, emailFieldSeparator, employee, 'step: ' + step + '; ' + resultInfo);
                if(resource !=  null)
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_R_ASSIGNMENT', 'SCbtcGeocodeResources',
                                resource.Id, null, resultCode, 
                                resultInfo, data, start, count); 
                }
                else
                {
                    logBatchInternal(interfaceLogList, 'GEOCODE_R_ASSIGNMENT', 'SCbtcGeocodeResources',
                                null, null, resultCode, 
                                resultInfo, data, start, count); 
                }                
            }                    
        }     
    } // geocode

        /**
    * Gets an address from a resource
    */
    public AvsAddress getAddr(SCResourceAssignment__c rs)
    {
        AvsAddress a = new AvsAddress();

        a.country        = String.valueof(rs.get('Country__c'));
        // a.countryState   = String.valueof(rs.get('CountryState__c'));
        // a.county         = String.valueof(rs.get('County__c'));
        a.postalcode     = String.valueof(rs.get('PostalCode__c'));
        a.city           = String.valueof(rs.get('City__c'));
        //a.district       = String.valueof(rs.get('District__c'));
        a.street         = String.valueof(rs.get('Street__c'));
        a.housenumber    = String.valueof(rs.get('HouseNo__c'));    

        if(a.country == 'DE')
        {
            if(a.postalcode != null && a.postalcode.length() == 4)
            {
                a.postalcode = '0' + a.postalcode;
            }
            if(a.housenumber != null)
            {
                a.housenumber = a.housenumber.replace(' ', '');
                a.housenumber = a.housenumber.replaceAll('[a-zA-Z]', '');
            }
            if(a.street != null)
            {
                a.street = a.street.replace('.', '');
                if(a.street.endswith('Str'))
                {
                    a.street = a.street.replace('Str', '');
                }
                if(a.street.endswith('Weg'))
                {
                    a.street = a.street.replace('Weg', '');
                }
            }
        } 
        return a;
    } // getAddr

    /**
    * Set coordinaten into a resource
    * @param sync true if the synchron call, else false
    * @param doubleCallOut true if on the reason of country we need to have two web calls
    * @param checkAddress the addresss to geocode
    * @param addrResult the result of check call
    * @param geocodingResult the result of geocode call
    * @param the resource, we now working out
    * @return the text for the SCInterfaceLog.ResultInfo__c
    */
    public String setGeoCode(Boolean sync, Boolean doubleCallOut, AvsAddress checkAddress, AvsResult addrResult, 
        AvsResult geocodingResult,  SCResourceAssignment__c rs)
    {
        SCbtcGeocodeBase.GeoCodeHolder gcHolder = new SCbtcGeocodeBase.GeoCodeHolder();
        String ret = prepareSetGeoCode(sync, doubleCallOut, checkAddress, addrResult, 
                                        geocodingResult, gcHolder);

		debug('gcHolder: ' + gcHolder);
        debug('processingMode: ' + processingMode);
        if(processingMode == null  || !processingMode.startsWith('SIM'))
        {
            rs.GeoStatus__c = gcHolder.geoStatus;
            if(gcHolder.geoApprox != null)
            {
                rs.GeoApprox__c = gcHolder.geoApprox;
            }
            if(gcHolder.geoX != null)
            {    
            	rs.GeoX__c = gcHolder.geoX;
            }	
            if(gcHolder.geoY != null)
            {
            	rs.GeoY__c = gcHolder.geoY;
            }	
        }    
        return ret;
    }//setGeoCode


    /*
    AvsAddress geocodeOrig(AvsAddress avs)
    {
        try
        {
            // simple search with a string
            SCAddressValidation v = new SCAddressValidation();
        
            // call the geocoding of the address to determine longitude and latitude
            AvsResult validationresult = v.geocode(avs);
            List<AvsAddress> foundaddrlist = validationresult.items;
            if(foundaddrlist.size() >= 1)             
            {
                return foundaddrlist[0];
            }
        }
        catch(Exception e)
        {
        
        }
        return null;   
    }
    */

   /*
    * Called by the framework when the batch job has been completed. 
    * We send an e-mail notification about the status
    * @param BC the batch context
    */
    global override void finish(Database.BatchableContext BC)
    {
        // Get the ID of the AsyncApexJob representing this batch job and  
        // query the AsyncApexJob object to retrieve the current job's information.  
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,  CreatedDate, TotalJobItems, CreatedBy.Email  from AsyncApexJob where Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses;
        if(email == null)
        {
            toAddresses = new String[] {a.CreatedBy.Email};
        }
        else
        {
            toAddresses = new String[] {email};
        }
        
        mail.setToAddresses(toAddresses);
        String jobInfoFromConstructor = jobinfo;
        jobinfo = 'Job SCbtcGeocodeResources at ' + a.CreatedDate;
        mail.setSubject(jobinfo + ' with status[' + a.Status + ']');
        String text = jobInfoFromConstructor;
        
        text = '\nThe batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures. geocoded items\n\n';
        String fieldSeparator = '|';
        text += '\n\n' + getGridTitle(emailTitlePrefix, fieldSeparator, 'EMPLOYEE');
        text += emailData;

        mail.setPlainTextBody(text);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
   } // finish
    
}