/*
 * @(#)SCContractTestHelper.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Use this class only from the test methods
 *
 * @author Jerzy Pietrzyk <jpietrzyk@gms-online.de>
 * @version $Revision$, $Date$
 */
public class SCContractTestHelper
{
    private static Boolean seeAllData = CCWCTestBase.isSeeingAllData();
    private SCApplicationSettings__c appSettings = SCApplicationSettings__c.getInstance();
    
    public User user { get; private set; }
    public SCPricelist__c priceList { get; private set; }
    public Account account { get; private set; }
    public SCArticle__c article { get; private set; }  
    public Brand__c brand { get; private set; }
    
    
    public List<SCContractRole__c> allRole { get; private set; }
    
    public SCInstalledBaseLocation__c location1 { get; private set; }
    public SCInstalledBaseLocation__c location2 { get; private set; }
    public SCInstalledBaseRole__c locationRole1 { get; private set; }
    public SCInstalledBaseRole__c locationRole2 { get; private set; }
    public SCProductModel__c prodModel { get; private set; }

    public SCContract__c template{get; private set;}
    public SCContract__c contract{get; private set;}
    
    public SCContractItem__c contractItem1 {get; private set;}
    public SCContractItem__c contractItem2 {get; private set;}
    public SCContractItem__c contractItem3 {get; private set;}
    public SCContractItem__c contractItem4 {get; private set;}
    public SCContractItem__c contractItem5 {get; private set;}
    
    // DH: Definition for the installed base
    public SCInstalledBase__c appliance11 { get; private set; }
        
    public String prepareForBtcContractVisitCreateTest()
    {
        appSettings = new SCApplicationSettings__c();
        if(seeAllData)
        {
            appSettings = SCApplicationSettings__c.getInstance();
        }
        CCWCTestBase.setAppSettingsForOrder(appSettings, seeAllData);

        Boolean doInsert = true;
        
        // create test users
        SCHelperTestClass.createTestUsers(true);
        
        /*
        Profile profile = [select id from profile where name='Standard User'];
        user = new User(alias = 'AutoTest', email='jpietrzyk@gms-online.de',
        emailencodingkey='UTF-8', lastname='Automatic-Tester', languagelocalekey='en_US',
        localesidkey='en_US', profileid = profile.Id,
            timezonesidkey='America/Los_Angeles', username='Automatic-Tester@gms-online.de');
        
        insert user;
        */
        
        // user the NL user for tests
        user = SCHelperTestClass.users.get(13);
                
        // create price list
        createPricelist();
        
        // create account
        createCustomer(doInsert);
        
        // create an installed base location 1
        location1 = createInstalledBaseLocation(doInsert, '14');
        
        // create an installed base location 2
        location2 = createInstalledBaseLocation(doInsert, '16');
        
        // create apliances for the installed bases
        SCArticle__c article11 = createArticleEx('Test-Contr-011', doInsert);
        SCArticle__c article12 = createArticleEx('Test-Contr-012', doInsert);        
        SCProductModel__c prodModel1 = createProductModelEx('268', 'VAM climaEuler [271]', '550', doInsert);  
        
        /*
        SCInstalledBase__c appliance11 = createInstalledBase(location1,
                                                            article11,
                                                            prodModel1,
                                                            '111111111222',
                                                             doInsert);
        */
        appliance11 = createInstalledBase(location1,
                                          article11,
                                          prodModel1,
                                          '111111111222',
                                          doInsert);
                                                                                                                          
        SCInstalledBase__c appliance12 = createInstalledBase(location1,
                                                            article12,
                                                            prodModel1,
                                                            '211111111222',
                                                             doInsert);
                                
              
        SCArticle__c article21 = createArticleEx('Test-Contr-0210', doInsert);
        SCArticle__c article22 = createArticleEx('Test-Contr-0220', doInsert);        
        SCProductModel__c prodModel2 = createProductModelEx('268', 'VAM climaPythagoras [314]', '550', doInsert);  
        SCInstalledBase__c appliance21 = createInstalledBase(location2,
                                                            article21,
                                                            prodModel2,
                                                            '311111111222',
                                                             doInsert);
        SCInstalledBase__c appliance22 = createInstalledBase(location2,
                                                            article22,
                                                            prodModel2,
                                                            '411111111222',
                                                             doInsert);
                                
        // create contract template
        template = createContractTemplate('Test Standart Contract', doInsert);
        
        System.debug('#### SCContract template is:'+template);
        
        // create contract
        contract = new SCContract__c();
        
        System.debug('#### contract is:'+contract);
                
        copyTemplateFieldsIntoInsurance(template, contract);     
        fillInsuranceFields(contract);
        
        System.debug('#### contract with fillInsuranceFields is:'+contract);
        
        insert contract;
        System.debug('###contract.Name: ' + contract.Name);
        
        // create contract items
        contractItem1 = createContractItems(contract.Id, contract,
                            appliance11,
                            3);
 
        contractItem2 = createContractItems(contract.Id, contract,
                            appliance12,
                            6);
        contractItem3 = createContractItems(contract.Id, contract,
                            appliance21,
                            3);
        contractItem4 = createContractItems(contract.Id, contract,
                            appliance22,
                            6);

        // create Contract roles
        allRole = new List<SCContractRole__c>(); 
        createContractRoles(contract.Id, account.Id); 
        return contract.Id;
    }

    public void createCustomer(Boolean doInsert)
    {
        account = new Account();

        String RecordTypeName = 'Customer';
        account.LastName__c = 'Contract Testing';
        account.FirstName__c = 'Tester';

        account.RecordTypeId = [Select Id from RecordType where DeveloperName = :RecordTypeName  and SobjectType = 'Account'].Id;
        account.TargetGroup__c = '001';
        account.SubTargetGroup__c = 'NL-005';
        account.BillingPostalCode = '00000';
        account.BillingCountry__c = 'NL';
        account.CurrencyIsoCode = 'EUR';

        // Fields SF42

        // Fields GMS
        account.Type = SCfwConstants.DOMVAL_ACCOUNT_TYPE_CUSTOMER;
        account.TaxType__c = SCfwConstants.DOMVAL_TAXACCOUNT_TAX;
        account.BillingStreet = 'Moormanweg';
        account.BillingHouseNo__c = '6';
        account.BillingPostalCode = '9831 NK';
        account.BillingCity = 'Aduard';
        account.BillingCountry__c = 'NL';
        account.GeoX__c = 6.454447;
        account.GeoY__c = 53.253582;
        if (priceList <> null && priceList.Id <> null)
        {
            account.DefaultPriceList__c = priceList.Id;
            account.DefaultPriceList__r = priceList;
        }

        if(doInsert)
        {
            insert account;
        }
    }
    
   /**
     * Create a new price list object based on default values.
     * taken from SCHelperTestClass
     * @params doInsert insert to DB if <code>true</code>
     */
    public void createPricelist()
    {
        String priceListName = 'Standard';

        if (appSettings.DEFAULT_PRICELIST__c <> null &&
            appSettings.DEFAULT_PRICELIST__c.length() > 0)
        {
            priceListName = appSettings.DEFAULT_PRICELIST__c;
        }

        try
        {
            priceList = [select Id, Name from SCPriceList__c where Name = :priceListName];
        }
        catch (Exception e)
        {
            priceList = new SCPriceList__c(Name = priceListName);
            insert priceList;
        }
    }

    public SCInstalledBaseLocation__c createInstalledBaseLocation(Boolean doInsert, String HouseNo)
    {
        SCInstalledBaseLocation__c location = null;
        // create a new account if we don't already have one
        // to avoid nasty error messages
        if (account == null)
        {
            createCustomer(doInsert);
        }

        location = new SCInstalledBaseLocation__c(
                BuildingDate__c = (Date.today() - 1000),
                Street__c = 'Moormanweg',
                HouseNo__c = HouseNo,
                PostalCode__c = '9831 NK',
                City__c = 'Aduard',
                Country__c = 'NL',
                GeoX__c = 6.454447,
                GeoY__c = 53.253582,
                LocName__c = 'Test Location ' + HouseNo,
                //ID2__c = 'TEST-LOCATION-CONTRACT ' + HouseNo,
                Status__c = 'Active'
            );

        SCInstalledBaseRole__c locationRole = new SCInstalledBaseRole__c(Account__c = account.Id,
                                                  InstalledBaseLocation__r = location,
                                                  Role__c = 'Owner',
                                                  ValidFrom__c = Date.today()
                                    );

        if (doInsert == true)
        {
            insert location;
            
            locationRole.InstalledBaseLocation__c = location.Id;
            insert locationRole;
        }
        return location;
    }

    public SCInstalledBase__c createInstalledBase(Boolean doInsert, 
                                                  SCInstalledBaseLocation__c location, 
                                                  String houseNo, 
                                                  String serialNo, 
                                                  SCArticle__c article,
                                                  SCProductModel__c prodModel)
    {
        // create a new article if we don't already have one
        if (article == null)
        {
            createArticle('TEST-000252',true);
        }
        article = [select Id,Name from SCArticle__c
                    where Name = 'TEST-000252' limit 1];

        // create a new account if we don't already have one
        // to avoid nasty error messages
        if (account == null)
        {
            createCustomer(doInsert);
        }

        if (location == null || location.Id == null)
        {
            createInstalledBaseLocation(doInsert, houseNo);
        }
        
        if (null == prodModel)
        {
            createProductModel(true);
        }

        SCInstalledBase__c installedBaseSingle = new SCInstalledBase__c(//ID2__c = 'IB-' + article.Name,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            SerialNo__c = serialNo,
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = location.Id,
                                            Type__c = 'Appliance',
                                            ProductModel__c = prodModel.Id, 
                                            ProductUnitClass__c = prodModel.UnitClass__c, 
                                            ProductUnitType__c = prodModel.UnitType__c, 
                                            ProductGroup__c = prodModel.Group__c, 
                                            ProductPower__c = prodModel.Power__c, 
                                            Brand__c = prodModel.Brand__c
                                    );

        if (doInsert == true)
        {
            //insert installedBase;
            insert installedBaseSingle;
        }
        return installedBaseSingle;
    }
    
    /**
     * Create an article for testing purposes
     *
     * @param articleName Article name for the article to insert
     * @param doInsert insert to DB if <code>true</code>
     * @author Thorsten Klein <tklein@gms-online.de>
     */
    public void createArticle(String articleName, Boolean doInsert)
    {
        createArticle(articleName, null, doInsert);
    }
    /**
     * Create an article for testing purposes
     *
     * @param articleName Article name for the article to insert
     * @param EANCode EAN code for the article to insert
     * @param doInsert insert to DB if <code>true</code>
     * @author Alexander Wagner <awagner@gms-online.de>
     */
    public void createArticle(String articleName, String EANCode, Boolean doInsert)
    {
        article = new SCArticle__c(
                                    Name = articleName,
                                    ID2__c = '00-' + articleName,
                                    EANCode__c = EANCode, 
                                    Text_en__c = 'Unit Test Article',
                                    TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                                    Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A);

        if (doInsert == true)
        {
            insert article;
        }
    }

    public SCArticle__c createArticleEx(String articleName, Boolean doInsert)
    {
        return createArticleEx(articleName, null, doInsert);
    }


    public SCArticle__c createArticleEx(String articleName, String EANCode, Boolean doInsert)
    {
        SCArticle__c article = new SCArticle__c(
                                    Name = articleName,
                                    ID2__c = '00-' + articleName,
                                    EANCode__c = EANCode, 
                                    Text_en__c = 'Unit Test Article',
                                    TaxType__c = SCfwConstants.DOMVAL_TAXARTICLE_FULL,
                                    Class__c = SCfwConstants.DOMVAL_ARTICLECLASS_PRODUCT,
                                    AvailabilityType__c = SCfwConstants.DOMVAL_MATERIALDISPO_A);

        if (doInsert == true)
        {
            insert article;
        }
        return article;
    }


    /**
     * Create product model
     **/
    public void createProductModel(boolean doInsert)
    {
        if (null == brand)
        {
            createBrandobject(false, true);
        }
        prodModel = new SCProductModel__c(Brand__c = brand.Id, 
                                          Country__c = 'NL', 
                                          UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                          UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 
                                          Group__c = '268', 
                                          Name = 'VAM climaVAIR [026]', 
                                          Power__c = '550');
                                                    
        if (doInsert)
        {
            insert prodModel;
        }
    }
    /*
        createProductModelEx('268', 'VAM climaEuler [271]', '550', doInsert);
    */
    public SCProductModel__c createProductModelEx(String group_a, String name, String power, boolean doInsert)
    {
        SCProductModel__c prodModel;
        if (null == brand)
        {
            createBrandobject(false, true);
        }
        prodModel = new SCProductModel__c(Brand__c = brand.Id, 
                                          Country__c = 'NL', 
                                          UnitClass__c = SCfwConstants.DOMVAL_PRODUCTUNITCLASS_STANDARD,
                                          UnitType__c = SCfwConstants.DOMVAL_PRODUCTUNITTYPE_DEFAULT, 
                                          Group__c = '268', 
                                          Name = 'VAM climaVAIR [026]', 
                                          Power__c = '550');
                                                    
        if (doInsert)
        {
            insert prodModel;
        }
        return prodModel;
    }


    /**
     * Create Brandobject. Insert only if DoInsert is true
     */
    public void createBrandObject(boolean IsCompetitor, boolean doInsert)
    {

        brand = new Brand__c(
            Name = 'Vaillant Test 2',
            Competitor__c = IsCompetitor,
            ID2__c = 'this_is_test_1_for_a_new_brand' + Datetime.now()

            // Fields SF42

            // Fields GMS

            // Fields Arlanis
        );

        if (doInsert)
        {
            insert brand;
        }
    }
    
    /**
     * Create a new installed base for a given account.
     *
     * doInsert insert to DB if <code>true</code>
     * @author Thorsten Klein <tklein@gms-online.de>
     * @author Dietrich Herdt <dherdt@gms-online.de>
     */
    public SCInstalledBase__c createInstalledBase( SCInstalledBaseLocation__c location,
                                            SCArticle__c article,
                                            SCProductModel__c prodModel,
                                            String serialNo,
                                            Boolean doInsert)
    {
        SCInstalledBase__c installedBaseSingle = null;

        installedBaseSingle = new SCInstalledBase__c(//ID2__c = 'IB-' + article.Name,
                                            Article__c = article.Id,
                                            Article__r = article,
                                            SerialNo__c = serialNo,
                                            Status__c = 'active',
                                            InstalledBaseLocation__c = location.Id,
                                            Type__c = 'Appliance',
                                            ProductModel__c = prodModel.Id, 
                                            ProductUnitClass__c = prodModel.UnitClass__c, 
                                            ProductUnitType__c = prodModel.UnitType__c, 
                                            ProductGroup__c = prodModel.Group__c, 
                                            ProductPower__c = prodModel.Power__c, 
                                            Brand__c = prodModel.Brand__c
                                    );

        if (doInsert == true)
        {
            //insert installedBase;
            insert installedBaseSingle;
        }
        return installedBaseSingle;
    }

    public SCContract__c createContractTemplate(String templateName, Boolean doInsert)
    {
          SCContract__c templateRet = new SCContract__c();
          
        templateRet.RecordTypeId = [Select Id from RecordType 
                                    where DeveloperName = 'TemplateContract' and SobjectType = :(SCfwConstants.NamespacePrefix + 'SCContract__c')].Id;

        templateRet.AutoExtend__c = 6; 
        
        templateRet.BillingInterval__c = null; 
        templateRet.BillingMethod__c = '50901'; 
        //templateRet.BillingStatus__c = templateIn.BillingStatus__c; 
        //templateRet.Brand__c = null; 
        templateRet.ConclusionDate__c = Date.valueOf('2011-01-02'); 
        templateRet.Contact__c = null; 
        templateRet.Country__c = 'DE'; 
        templateRet.CurrencyIsoCode = 'EUR'; 
        //templateRet.CustomerPriority__c = null;
        // template department fields are empty
        //templateRet.DepartmentCurrent__c = null;
        //templateRet.DepartmentResponsible__c = null;
        //templateRet.DepartmentTurnover__c = null;
        //templateRet.DescriptionEngineer__c = null; 
        templateRet.DescriptionInternal__c = 'Test'; 
        //templateRet.DescriptionSpecialTerms__c = null; 
        //templateRet.Description__c = null; 
        // All Enable fields are empty
        templateRet.EnableAutoScheduling__c = true; 
        templateRet.EnableMultiLocations__c = false; 
        templateRet.EnableOrderCreation__c = true; 
        templateRet.EnableVisitCreation__c = true;
        templateRet.EnableScheduleIfPayed__c = false; 
        templateRet.EndDate__c = Date.valueOf('2015-12-31'); 
        // template.Followups are empty
        //templateRet.Followup1__c = null; 
        //templateRet.Followup2__c = null; 
        // Frame must be empty
        // templateRet.Frame__c = templateIn.Frame__c; 
        // TODO: doppelt
        //templateRet.ID2__c = null; 
        //templateRet.IdExt__c = null; 
        //templateRet.IdOld__c = null; 
        //templateRet.Info__c = null; 
        templateRet.InsuranceAreaCode__c = 'VA048KF'; 
        //templateRet.InsuranceCleanup__c = null; 
        templateRet.InsuranceCompany__c = 'HS'; 
        //templateRet.InsuranceExcess__c = null; 
        // templateIn.InsuranceLastChecked empty
        //templateRet.InsuranceLastChecked__c = null; 
        templateRet.InsuranceMailCode__c = 'VAGX47A'; 
        //templateRet.InsuranceOnRiskDate__c = null; 
        templateRet.InsurancePaymentType__c = 'CCA;DDQ;DDA'; 
        //templateRet.InsurancePolicySelector__c = null; // not writeable
        templateRet.InsurancePolicyTypeName__c = 'KF'; 
        //templateRet.InsuranceProductCode__c = null; 
        templateRet.InsuranceSalesPrice__c = 299.96; 
        //templateRet.InsuranceValidateOnline empty
        //templateRet.InsuranceValidateOnline__c = null; 
        // Internal must be empty
        //templateRet.Internal__c = null; 
        // Invoicing types in template are empty
        //templateRet.InvoicingSubtype__c = null; 
        //templateRet.InvoicingType__c = null; 
        templateRet.MaintenanceAfterDue__c = 14; 
        templateRet.MaintenanceBeforeDue__c = 14; 
        templateRet.MaintenanceCreationLeadtime__c = 30; 
        templateRet.MaintenanceCreationShiftFactor__c = '0'; 
        templateRet.MaintenanceDuration__c = 60; 
        templateRet.MaintenanceFirstDate__c = Date.today(); 
        templateRet.MaintenanceInterval__c = 3; 
        // all template MaintenanceVisit fields are empty
        //templateRet.MaintenanceVisitDaySelection__c = null; 
        //templateRet.MaintenanceVisitDay__c = null; 
        //templateRet.MaintenanceVisitPeriodSelection__c = null;
        //templateRet.MaintenanceVisitPeriod__c = null; 
        //templateRet.MaintenanceVisitTimeSelection__c = null; 
        //templateRet.MaintenanceVisitTime__c = null; 
        // templateRet.Name = templateIn.Name; not writeable
        // Offer fields must be empty
        //templateRet.OfferDate__c = null; 
        //templateRet.OfferDescription__c = null; 
        //order reason empty by template and insurance
        //templateRet.OrderReason__c = null; 
        //templateRet.OrderTypeFirstVisit__c = null; 
        // templateIn.OrderType empty
        //templateRet.OrderType__c = templateIn.OrderType__c; 
        templateRet.OwnerId = user.Id; 
        //templateRet.PreviousContract__c = null; 

        //templateRet.RenewalReason__c = null; 
        templateRet.RuntimeMax__c = 72; 
        templateRet.Runtime__c = 12; 
        templateRet.StartDate__c = Date.valueOf('2011-01-01'); 
        //templateRet.StartDelayDays__c = null; 
        // StartDependsOnWarranty must by empty
        //templateRet.StartDependsOnWarranty__c = null
        templateRet.Status__c = 'Active'; 
        templateRet.TemplateName__c = templateName; 
        //templateRet.Template__c = null; 
        //templateRet.TerminationDate__c = null; 
        //templateRet.TerminationDescription__c = null; 
        //templateRet.TerminationExtraordinary__c = null; 
        // TerminationPeriod must be not set
        //templateRet.TerminationPeriod__c = null; 
        //templateRet.TerminationReason__c = null; 
        //templateRet.TerminationReceivedDate__c = null; 
        // TerminationType must be not set
        //templateRet.TerminationType__c = null; 
        //templateRet.util_CostsLabour__c = null; 
        //templateRet.util_CostsParts__c = null; // not writeable
        //templateRet.util_CostsTravel__c = null;
        //templateRet.util_MaintenanceDuration__c = null;
        //templateRet.util_MaxRenewals__c = null;
        //templateRet.util_Profit__c = null;
        //templateRet.util_ProfitabilityRanking__c = null;
        //templateRet.util_Profitability__c = null;
        //templateRet.util_RevenueContract__c = null;
        //templateRet.util_RevenueCustomer__c = null;
        //templateRet.util_RevenueInsurance__c = null;
        //templateRet.util_RevenueTotal__c = null;
        //templateRet.util_active__c = null;
        //templateRet.util_days_until_expiration__c = null;
        //templateRet.util_details__c = null;
        //templateRet.util_earliest_termination_date__c = null;
        //templateRet.util_templatedetails__c = null;
        
        if(doInsert)
        {
            insert templateRet;
        }
        return templateRet;
    }     

    private void copyTemplateFieldsIntoInsurance(SCContract__c templateIn, SCContract__c contractOut)
    {
        System.debug('###copyTemplateFieldsIntoInsurance###');
        contractOut.Account__c = templateIn.Account__c; 
        contractOut.AutoExtend__c = templateIn.AutoExtend__c; 
        contractOut.BillingInterval__c = templateIn.BillingInterval__c; 
        contractOut.BillingMethod__c = templateIn.BillingMethod__c; 
        // templateIn.BillingStatus__c is empty
        //contractOut.BillingStatus__c = templateIn.BillingStatus__c; 
        contractOut.Brand__c = templateIn.Brand__c; 
        contractOut.ConclusionDate__c = templateIn.ConclusionDate__c; 
        contractOut.Contact__c = templateIn.Contact__c; 
        contractOut.Country__c = templateIn.Country__c; 
        contractOut.CurrencyIsoCode = templateIn.CurrencyIsoCode; 
        // templateIn.CustomerPriority
        //contractOut.CustomerPriority__c = templateIn.CustomerPriority__c; 
        // template department fields are empty
        //contractOut.DepartmentCurrent__c = templateIn.DepartmentCurrent__c; 
        //contractOut.DepartmentResponsible__c = templateIn.DepartmentResponsible__c; 
        //contractOut.DepartmentTurnover__c = templateIn.DepartmentTurnover__c; 
        contractOut.DescriptionEngineer__c = templateIn.DescriptionEngineer__c; 
        contractOut.DescriptionInternal__c = templateIn.DescriptionInternal__c; 
        contractOut.DescriptionSpecialTerms__c = templateIn.DescriptionSpecialTerms__c; 
        contractOut.Description__c = templateIn.Description__c; 
        // All Enable fields are empty
        contractOut.EnableAutoScheduling__c = templateIn.EnableAutoScheduling__c; 
        contractOut.EnableMultiLocations__c = templateIn.EnableMultiLocations__c; 
        contractOut.EnableOrderCreation__c = templateIn.EnableOrderCreation__c; 
        contractOut.EnableVisitCreation__c = templateIn.EnableVisitCreation__c; 
        // contractOut.EnableScheduleIfPayed__c = templateIn.EnableScheduleIfPayed__c; 
        contractOut.EndDate__c = templateIn.EndDate__c; 
        // template.Followups are empty
        //contractOut.Followup1__c = templateIn.Followup1__c; 
        //contractOut.Followup2__c = templateIn.Followup2__c; 
        // Frame must be empty
        // contractOut.Frame__c = templateIn.Frame__c; 
        // TODO: doppelt
        //contractOut.ID2__c = templateIn.ID2__c; 
        contractOut.IdExt__c = templateIn.IdExt__c; 
        contractOut.IdOld__c = templateIn.IdOld__c; 
        contractOut.Info__c = templateIn.Info__c; 
        contractOut.InsuranceAreaCode__c = templateIn.InsuranceAreaCode__c; 
        contractOut.InsuranceCleanup__c = templateIn.InsuranceCleanup__c; 
        contractOut.InsuranceCompany__c = templateIn.InsuranceCompany__c; 
        contractOut.InsuranceExcess__c = templateIn.InsuranceExcess__c; 
        // templateIn.InsuranceLastChecked empty
        //contractOut.InsuranceLastChecked__c = templateIn.InsuranceLastChecked__c; 
        contractOut.InsuranceMailCode__c = templateIn.InsuranceMailCode__c; 
        contractOut.InsuranceOnRiskDate__c = templateIn.InsuranceOnRiskDate__c; 
        contractOut.InsurancePaymentType__c = templateIn.InsurancePaymentType__c; 
        //contractOut.InsurancePolicySelector__c = templateIn.InsurancePolicySelector__c; // not writeable
        contractOut.InsurancePolicyTypeName__c = templateIn.InsurancePolicyTypeName__c; 
        contractOut.InsuranceProductCode__c = templateIn.InsuranceProductCode__c; 
        contractOut.InsuranceSalesPrice__c = templateIn.InsuranceSalesPrice__c; 
        //contractOut.InsuranceValidateOnline empty
        //contractOut.InsuranceValidateOnline__c = templateIn.InsuranceValidateOnline__c; 
        // Internal must be empty
        //contractOut.Internal__c = templateIn.Internal__c; 
        // Invoicing types in template are empty
        //contractOut.InvoicingSubtype__c = templateIn.InvoicingSubtype__c; 
        //contractOut.InvoicingType__c = templateIn.InvoicingType__c; 
        //contractOut.IsDeleted = templateIn.IsDeleted;  not writeable
        //contractOut.LastActivityDate = templateIn.LastActivityDate; // not writeable
//        contractOut.LastModifiedById = templateIn.LastModifiedById; 
//        contractOut.LastModifiedDate = templateIn.LastModifiedDate; 
        contractOut.MaintenanceAfterDue__c = templateIn.MaintenanceAfterDue__c; 
        contractOut.MaintenanceBeforeDue__c = templateIn.MaintenanceBeforeDue__c; 
        contractOut.MaintenanceCreationLeadtime__c = templateIn.MaintenanceCreationLeadtime__c; 
        contractOut.MaintenanceCreationShiftFactor__c = templateIn.MaintenanceCreationShiftFactor__c; 
        contractOut.MaintenanceDuration__c = templateIn.MaintenanceDuration__c; 
        contractOut.MaintenanceFirstDate__c = templateIn.MaintenanceFirstDate__c; 
        contractOut.MaintenanceInterval__c = templateIn.MaintenanceInterval__c; 
        // all template MaintenanceVisit fields are empty
        //contractOut.MaintenanceVisitDaySelection__c = templateIn.MaintenanceVisitDaySelection__c; 
        //contractOut.MaintenanceVisitDay__c = templateIn.MaintenanceVisitDay__c; 
        //contractOut.MaintenanceVisitPeriodSelection__c = templateIn.MaintenanceVisitPeriodSelection__c;
        //contractOut.MaintenanceVisitPeriod__c = templateIn.MaintenanceVisitPeriod__c; 
        //contractOut.MaintenanceVisitTimeSelection__c = templateIn.MaintenanceVisitTimeSelection__c; 
        //contractOut.MaintenanceVisitTime__c = templateIn.MaintenanceVisitTime__c; 
        // contractOut.Name = templateIn.Name; not writeable
        // Offer fields must be empty
        //contractOut.OfferDate__c = templateIn.OfferDate__c; 
        //contractOut.OfferDescription__c = templateIn.OfferDescription__c; 
        //order reason empty by template and insurance
        //contractOut.OrderReason__c = templateIn.OrderReason__c; 
        contractOut.OrderTypeFirstVisit__c = templateIn.OrderTypeFirstVisit__c; 
        // templateIn.OrderType empty
        //contractOut.OrderType__c = templateIn.OrderType__c; 
        contractOut.OwnerId = templateIn.OwnerId; 
        contractOut.PreviousContract__c = templateIn.PreviousContract__c; 

        contractOut.RecordTypeId = [Select Id from RecordType 
                                    where DeveloperName = 'Insurance' and SobjectType = :(SCfwConstants.NamespacePrefix + 'SCContract__c')].Id;
        contractOut.RenewalReason__c = templateIn.RenewalReason__c; 
        contractOut.RuntimeMax__c = templateIn.RuntimeMax__c; 
        contractOut.Runtime__c = templateIn.Runtime__c; 
        contractOut.StartDate__c = templateIn.StartDate__c; 
        contractOut.StartDelayDays__c = templateIn.StartDelayDays__c; 
        // StartDependsOnWarranty must by empty
        //contractOut.StartDependsOnWarranty__c = templateIn.StartDependsOnWarranty__c; 
        contractOut.Status__c = templateIn.Status__c; 
        // contractOut.SystemModstamp = templateIn.SystemModstamp; not writeable
        // contractOut.TemplateName__c = templateIn.TemplateName__c; 
        contractOut.Template__c = templateIn.Id; 
        contractOut.TerminationDate__c = templateIn.TerminationDate__c; 
        contractOut.TerminationDescription__c = templateIn.TerminationDescription__c; 
        contractOut.TerminationExtraordinary__c = templateIn.TerminationExtraordinary__c; 
        // TerminationPeriod must be not set
        //contractOut.TerminationPeriod__c = templateIn.TerminationPeriod__c; 
        contractOut.TerminationReason__c = templateIn.TerminationReason__c; 
        contractOut.TerminationReceivedDate__c = templateIn.TerminationReceivedDate__c; 
        // TerminationType must be not set
        //contractOut.TerminationType__c = templateIn.TerminationType__c; 
        //contractOut.util_CostsLabour__c = templateIn.util_CostsLabour__c; 
        //contractOut.util_CostsParts__c = templateIn.util_CostsParts__c; // not writeable
        //contractOut.util_CostsTotal__c = templateIn.util_CostsTotal__c; 
        //contractOut.util_CostsTravel__c = templateIn.util_CostsTravel__c; 
        //contractOut.util_MaintenanceDuration__c = templateIn.util_MaintenanceDuration__c; 
        //contractOut.util_MaxRenewals__c = templateIn.util_MaxRenewals__c; 
        //contractOut.util_Profit__c = templateIn.util_Profit__c; 
        //contractOut.util_ProfitabilityRanking__c = templateIn.util_ProfitabilityRanking__c; 
        //contractOut.util_Profitability__c = templateIn.util_Profitability__c; 
        //contractOut.util_RevenueContract__c = templateIn.util_RevenueContract__c; 
        //contractOut.util_RevenueCustomer__c = templateIn.util_RevenueCustomer__c; 
        //contractOut.util_RevenueInsurance__c = templateIn.util_RevenueInsurance__c; 
        //contractOut.util_RevenueTotal__c = templateIn.util_RevenueTotal__c; 
        //contractOut.util_active__c = templateIn.util_active__c; 
        //contractOut.util_days_until_expiration__c = templateIn.util_days_until_expiration__c; 
        //contractOut.util_details__c = templateIn.util_details__c; 
        //contractOut.util_earliest_termination_date__c = templateIn.util_earliest_termination_date__c; 
        //contractOut.util_templatedetails__c = templateIn.util_templatedetails__c; 
    }     
   
    private void fillInsuranceFields(SCContract__c contractOut)
    {
        System.debug('###fillInsuranceFields###');

        contractOut.Account__c = account.Id;
        contractOut.OwnerId = user.Id;
        contractOut.InsuranceSalesPrice__c = 300;
                         
        contractOut.Runtime__c =  48;
        contractOut.InsuranceOnRiskDate__c = Date.today();
        Integer delayDays = 30;
        contractOut.InsuranceOnRiskDate__c = contractOut.InsuranceOnRiskDate__c.addDays(delayDays);
        
        contractOut.StartDate__c = Date.valueOf('2011-01-02');
        contractOut.EndDate__c = Date.valueOf('2014-12-31');
    }
   
    private SCContractItem__c createContractItems(ID contractId, SCContract__c contract,
                                                 SCInstalledBase__c installedBase,
                                                 Integer interval)
    {
        System.debug('###createContractItems###');
        SCContractItem__c contractItem = new SCContractItem__c();
        contractItem.Contract__c = contractId;
        contractItem.InstalledBase__c = installedBase.Id;
        contractItem.MaintenanceDuration__c = 30;
        contractItem.MaintenanceInterval__c = interval;
        insert contractItem;
        return contractItem;
    }
   
    private void createContractRoles(ID contractId, ID customerId)
    {
        System.debug('###createContractRoles: ');
        ID contractPackageId = null;
        // role LE
        addContractRole('50301', 
                        contractID, 
                        contractPackageId, 
                        customerId, 
                        null);
        // role RE
        addContractRole('50303', 
                        contractID, 
                        contractPackageId, 
                        customerId, 
                        null);
        insert allRole;
    }//createContractRoles
 
    private void addContractRole(String role, 
                                 ID contractID, 
                                 ID contractPackageID, 
                                 ID accountID, 
                                 String id2)
    {
        SCContractRole__c ret = new SCContractRole__c();
        ret.Contract__c = contractID;
        ret.Account__c = accountID;
        ret.ContractPackage__c = contractPackageID;
        ret.Role__c = role;
        ret.ID2__c = id2;
        allRole.add(ret);
    }
  
}