/*
 * @(#)SCQuickCustomerSearchControllerTest.cls 
 * 
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This class tests the functionality of the main class
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 @isTest (SeeAllData = true)
private class SCQuickCustomerSearchControllerTest
{
    private static SCQuickCustomerSearchController qcs;

    static testMethod void quickCustomerSearchPositiv1() 
    {
        SCHelperTestClass.createOrderTestSet(true);
        qcs = new SCQuickCustomerSearchController();
        
        Test.startTest();
        
        qcs.searchString = SCHelperTestClass.account.FirstName__c;
        qcs.getSearchProducts();
        qcs.reloadDetails();
        
        Test.stopTest();    
    }
}