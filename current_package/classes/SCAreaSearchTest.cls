/*
 * @(#)SCAreaSearchTest.cls
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 */
 
/**
 * Simple test class for search function for engineers / business units by the postcode
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
 */
@isTest
private class SCAreaSearchTest
{
    /**
     * Test the simple credit note creation process
     */
    static testMethod void testSearch()
    {    
        // create some postcode areas    
        SCArea__c oearea1 = new SCArea__c(name = 'Business Unit 1', context__c = 'Business Unit');
        SCArea__c oearea2 = new SCArea__c(name = 'Business Unit 2', context__c = 'Business Unit');
        
        insert oearea1;            
        insert oearea2;            
        
        List<SCAreaItem__c> oeareaitems = new List<SCAreaItem__c>();
        // positive test
        oeareaitems.add(new SCAreaItem__c(area__c = oearea1.id, country__c = 'NL', name = 'T12345', description__c = 'desc 1.1'));
        oeareaitems.add(new SCAreaItem__c(area__c = oearea1.id, country__c = 'NL', name = 'T12346', description__c = 'desc 1.2'));
        insert oeareaitems;

        SCHelperTestClass.createTestCalendar(true);

        SCBusinessUnit__c bus1 = new SCBusinessUnit__c(name='Test BUS1', area__c = oearea1.id, calendar__c = SCHelperTestClass.calendar.id, Operational__c=true, type__c='5901');
        SCBusinessUnit__c bus2 = new SCBusinessUnit__c(name='Test BUS2', area__c = oearea2.id, calendar__c = SCHelperTestClass.calendar.id, Operational__c=true, type__c='5901' );

        insert bus1;
        insert bus2;
    
        // create some engineer postcode areas    
        SCArea__c area1 = new SCArea__c(name = 'My test area 1', context__c = 'Resource');
        SCArea__c area2 = new SCArea__c(name = 'My test area 2', context__c = 'Resource');
        SCArea__c area3 = new SCArea__c(name = 'My test area 3', context__c = 'Resource');
        SCArea__c area4 = new SCArea__c(name = 'My test area 4', context__c = 'Resource');
        
        insert area1;            
        insert area2;            
        insert area3;            
        insert area4;            
        
        List<SCAreaItem__c> areaitems = new List<SCAreaItem__c>();
        // positive test
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12345', prio__c = '1', description__c = 'desc 1.1'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12346', prio__c = '2', description__c = 'desc 1.2'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'NL', name = 'T12347', prio__c = '3', description__c = 'desc 1.3'));
        // negative test
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12345', prio__c = '5', description__c = 'desc 1.4'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12346', prio__c = '6', description__c = 'desc 1.5'));
        areaitems.add(new SCAreaItem__c(area__c = area1.id, country__c = 'DE', name = 'T12347', prio__c = '7', description__c = 'desc 1.6'));

        // positive test
        areaitems.add(new SCAreaItem__c(area__c = area2.id, country__c = 'NL', name = 'T12345', prio__c = '2', description__c = 'desc 2.1'));
        areaitems.add(new SCAreaItem__c(area__c = area3.id, country__c = 'NL', name = 'T12345', prio__c = '3', description__c = 'desc 3.1'));

        areaitems.add(new SCAreaItem__c(area__c = area4.id, country__c = 'NL', name = 'T12345', prio__c = '1', description__c = 'desc 4.1'));
       
        insert areaitems;
        
        // create the resources and assignments
        SCHelperTestClass.createTestUsers(true);
        SCHelperTestClass.createTestResources(SCHelperTestClass.users, true);
        SCHelperTestClass.createTestAssignments(SCHelperTestClass.resources, new List<SCStock__c>(), null, true);
        
        SCHelperTestClass.resourceAssignments[0].area__c = area1.id;
        SCHelperTestClass.resourceAssignments[1].area__c = area2.id;
        SCHelperTestClass.resourceAssignments[2].area__c = area3.id;
        update SCHelperTestClass.resourceAssignments;    

        Test.startTest();        


        SCAreaSearch con = new SCAreaSearch();
        con.searchvalue.name = 'T12345';
        con.onSearch();
        
        //TODO implement system.assertequals...
        System.Debug('' + con.units + ' = ' +  bus1.id);
        System.Debug('' + con.areaitems);

        Test.stopTest();
            
  
    }
}