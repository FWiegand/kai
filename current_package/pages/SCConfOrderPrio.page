<!--
 * @(#)SCConfOrderPrio.page
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * Configuration of order priority rules
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
-->

<apex:page standardController="SCConfOrderPrio__c" recordSetVar="items" tabStyle="SCConfOrderPrio__c" extensions="SCConfOrderPrioExtension">
<apex:form >

    <!-- Create items-->
    <apex:pageBlock id="pbCreate" title="Configuration of order priority rules" mode="edit" rendered="{!IF(SelectedCount == 0, 'true', 'false')}">
        <apex:pageBlockButtons >
            <apex:commandButton value="Cancel" action="{!onCancel}"/>
        </apex:pageBlockButtons> 
        <apex:pageBlockSection id="theSection" columns="2">
            <apex:pageBlockTable value="{!CountryConfig}" var="v">
                <apex:column >
                    <apex:facet name="header">Action</apex:facet>
                     <apex:commandLink value="Create" action="{!onCreate}" rendered="{!IF(v.count == 0, 'true', 'false')}" rerender="theSection" immediate="true" status="theStatus">
                         <apex:param name="selcountry" value="{!v.countryValue}" assignTo="{!countrySelected}"/>
                     </apex:commandLink> 
                     <apex:commandLink value="Del" action="{!onDelete}" rendered="{!IF(v.count > 0, 'true', 'false')}" rerender="theSection" immediate="true" status="theStatus">
                         <apex:param name="selcountry" value="{!v.countryValue}" assignTo="{!countrySelected}"/>
                     </apex:commandLink>
                </apex:column>                    
                <apex:column style="column-with=5%">
                    <apex:facet name="header">Code</apex:facet>
                    {!v.countryValue}
                    <apex:outputText rendered="{!IF(countryCurrent == v.countryValue,'true', 'false')}">*</apex:outputText>
                </apex:column>                    
                <apex:column >
                    <apex:facet name="header">Country</apex:facet>
                    {!v.countryLabel}
                </apex:column>                    
                <apex:column >
                    <apex:facet name="header">Count</apex:facet>
                    {!v.count}
                </apex:column>                    
            </apex:pageBlockTable>
            <apex:outputPanel >
                <apex:actionStatus id="theStatus" startText="processing..." stopText="Please use the links to execute the descired action"/>            
                <BR/>
                <BR/>
                [Create] can be used to create default settings for the corresponding country<BR/>
                [Del] delets all records of the corresponding country<BR/>
                <BR/>
                <BR/>
                Please check the following custom settings, too:
                <BR/>
                <BR/>
                ORDERCREATION_PRIO_DETERMINATION<BR/>
                Handles the determination of the order priority.<BR/> 
                0 = manual by the user (standard, selection from a picklist)<BR/> 
                1 = user can select from a picklist or use the dialog<BR/> 
                2 = user must use the dialog before disposition<BR/>
                <BR/>
                ORDERCREATION_PRIO_DETERMINATION_DEBUG<BR/>
                If set to true more detail information (score per line, total score) will be displayed in the dialog. Should be set to false on productive systems.
                               
            </apex:outputPanel>
        </apex:pageBlockSection>
    </apex:pageBlock>

    <!-- Delete items-->
    <apex:pageBlock title="Delete selected items " mode="edit" rendered="{!IF(SelectedCount > 0, 'true', 'false')}">
        <apex:pageBlockButtons location="both">
            <apex:commandButton value="Delete" action="{!onDeleteSelected}"/>
            <apex:commandButton value="Cancel" action="{!onCancel}"/>
        </apex:pageBlockButtons>

        <apex:pageBlockTable value="{!selected}" var="i">
            <apex:column value="{!i.name}"/>
            <apex:column value="{!i.Category__c}"/>
            <apex:column value="{!i.Type__c}"/>
            <apex:column value="{!i.SelectType__c}"/>
            <apex:column value="{!i.SortOrder__c}"/>
            <apex:column value="{!i.Field__c}"/>
            <apex:column value="{!i.Score__c}"/>
<!--                
            <apex:column value="{!i.Factor__c}"/>
            <apex:column value="{!i.KoCriteria__c}"/>
            <apex:column value="{!i.OrderPrio__c}"/>
            <apex:column value="{!i.PrioValue__c}"/>
            <apex:column value="{!i.Offset__c}"/>
            <apex:column value="{!i.PreSelObj__c}"/>
            <apex:column value="{!i.PreSelField__c}"/>
            <apex:column value="{!i.PreSelValue__c}"/>
-->                
        </apex:pageBlockTable>
    </apex:pageBlock>

</apex:form>
</apex:page>