<!--
 * @(#)SCOrderNote.page
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * At this page you can create a new note for the closed order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
-->

<!--
MA / ET
Idea: refactor this page to a component if required for other objects
-->
<apex:page controller="SCOrderNoteController">
    <apex:pageMessage rendered="{!IF(errorText != '' && errorText != null, true, false)}" 
                severity="ERROR" 
                strength="2"
                title="{!errorText}" 
            />    
    <!--apex:include pageName="SCOrderConfirmation" /-->
    
    
    <apex:form >

        <apex:pageBlock title="{!$ObjectType.Note.label}">
            <apex:pageMessage rendered="{!IF(errorTextNote != '' && errorTextNote != null, true, false)}" 
                severity="warning" 
                strength="2"
                title="{!errorTextNote}" 
            />    
            <apex:pageBlockButtons >
                <apex:commandButton immediate="false" value="{!$Label.SC_btn_Save}" action="{!onCreateNote}" style="width:120px"/>
                <apex:commandButton immediate="true" value="{!$Label.SC_btn_Cancel}" action="{!onCancel}"/>
            </apex:pageBlockButtons>
            
            <apex:pageBlockSection columns="1">
            
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Note.fields.Title.label}" for="noteTitle" />
                    <apex:inputField value="{!note.Title}" required="true" id="noteTitle" style="width:80%"/>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Note.fields.Body.label}" for="noteBody" />
                    <apex:inputField value="{!note.Body}" required="true" id="noteBody" style="width:80%"/>
                </apex:pageBlockSectionItem>
            
            </apex:pageBlockSection>
    
        </apex:pageBlock>
    
    </apex:form>
    
    <apex:form >

        <apex:pageBlock title="{!$ObjectType.Attachment.label}">
            <apex:pageMessage rendered="{!errorTextAtt != '' && errorTextAtt != null}" 
                severity="ERROR" 
                strength="2"
                title="{!errorTextAtt}" 
            />
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton value="{!$Label.SC_btn_Save}" action="{!onCreateAttachment}" style="width:120px"/>
                <apex:commandButton immediate="true" value="{!$Label.SC_btn_Cancel}" action="{!onCancel}"/>
            </apex:pageBlockButtons>    
            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.Attachment.label}" for="fileSelection" />
                    <apex:inputFile id="fileSelection" 
                        required="true" 
                        value="{!attachment.body}" 
                        filename="{!attachment.name}" 
                    />
                </apex:pageBlockSectionItem>    
            </apex:pageBlockSection>
    
        </apex:pageBlock>
    
    </apex:form>

</apex:page>