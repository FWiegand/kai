<!--
 * @(#)SCDeveloperDashBoard.page
 * 
 * Copyright 2013 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCDeveloperDashBoardController" tabStyle="SCOrder__c" sidebar="false">

<apex:includeScript value="{!URLFOR($Resource.tablesort,'/jquery-1.7.min.js')}" />
<apex:includeScript value="{!URLFOR($Resource.tablesort,'/jquery.tablesorter.min.js')}" />

<style>
.radioButton {}

.no_wrap
{
    white-space: nowrap;
}
    
/* Overwritten SFDC styles */

body .bPageBlock .pbBody .labelCol, 
body .bPageBlock .pbBody .dataCol, 
body .bPageBlock .pbBody .data2Col {
    padding-top:    2px;
    padding-bottom: 2px;
}

body .bEditBlock .pbBottomButtons, 
body .apexp .bPageBlock.apexDefaultPageBlock .pbBottomButtons {
    margin-top: 10px;
}

.bPageBlock .detailList th, 
.bPageBlock .detailList td {
    vertical-align:top;
}

</style>

<script type="text/javascript">
$(document).ready(function() 
    { 
        $("table").tablesorter(); 
        
        var elems = document.getElementsByName('radioGroup');
        if ('{!JSENCODE(radioSel)}' == '1')
        {
            elems[1].checked = true;
        }
        else
        {
            elems[0].checked = true;
        }
    } 
); 

function setVal(param)
{
    jQuery('[id$=radioSel]').val(jQuery(param).val());
}

</script>

<apex:panelGroup id="searchPanel" rendered="{!NOT(editFavorites)}">
    <apex:pageBlock id="favorites" rendered="{!hasFavorites}">
        <apex:pageBlockSection id="favoriteSect" collapsible="true" columns="1" title="Favorites">
            <apex:pageBlockSectionItem rendered="{!hasClasses}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Classes"/>
                <apex:pageBlockSection id="classSect" columns="4">
                    <apex:repeat var="classObj" value="{!curClassObjs}">
                        <apex:panelGroup layout="none">
                            <a href="/{!classObj.apexClass.Id}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!classObj.apexClass.Name}</a>
                            <apex:panelGroup layout="none" rendered="{!OR(NOT(ISNULL(classObj.apexTestClass)), NOT(CONTAINS(UPPER(classObj.apexClass.Name), 'TEST')))}">&nbsp;(</apex:panelGroup>
                            <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(classObj.apexTestClass))}">
                                <a href="/{!classObj.apexTestClass.Id}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">T</a>&nbsp;
                            </apex:panelGroup>
                            <apex:panelGroup layout="none" rendered="{!NOT(CONTAINS(UPPER(classObj.apexClass.Name), 'TEST'))}">
                                <a href="/setup/build/viewCodeCoverage.apexp?id={!classObj.apexClass.Id}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">CC%</a>
                            </apex:panelGroup>
                            <apex:panelGroup layout="none" rendered="{!OR(NOT(ISNULL(classObj.apexTestClass)), NOT(CONTAINS(UPPER(classObj.apexClass.Name), 'TEST')))}">)</apex:panelGroup>
                        </apex:panelGroup>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!hasPages}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Pages"/>
                <apex:pageBlockSection id="pageSect" columns="4">
                    <apex:repeat var="page" value="{!curPages}">
                        <a href="/{!page.value}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!page.label}</a>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!hasComponents}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Components"/>
                <apex:pageBlockSection id="componentSect" columns="4">
                    <apex:repeat var="comp" value="{!curComponents}">
                        <a href="/apexpages/setup/editApexComponent.apexp?id={!comp.value}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!comp.label}</a>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!hasTriggers}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Triggers"/>
                <apex:pageBlockSection id="triggerSect" columns="4">
                    <apex:repeat var="trigger" value="{!curTriggers}">
                        <a href="/{!trigger.value}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!trigger.label}</a>&nbsp;
                        (<a href="/setup/build/viewCodeCoverage.apexp?id={!trigger.value}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">CC%</a>)
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!hasObjects}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Objects"/>
                <apex:pageBlockSection id="objectSect" columns="4">
                    <apex:repeat var="object" value="{!curObjects}">
                        <a href="/{!object.value}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!object.label}</a>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem rendered="{!hasOthers}" dataStyleClass="no_wrap">
                <apex:outputLabel value="Others"/>
                <apex:pageBlockSection id="otherSect" columns="4">
                    <apex:repeat var="other" value="{!curOthers}">
                        <a href="/{!other.value}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">{!other.label}</a>
                    </apex:repeat>
                </apex:pageBlockSection>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
    </apex:pageBlock>
        
    <apex:pageBlock id="search">
    <apex:form accept="dosearch" style="margin:0 0 5px 0;">           
        <apex:panelGrid columns="9">
            <!-- row 1 -->
            <input type="radio" value="0" name="radioGroup" class="radioButton" onclick="setVal(this)"/>
            <apex:outputText value="Object: "/>
            <apex:inputText value="{!search}" style="width:200px;"/>
        
            <apex:commandButton id="dosearch" value="Search" action="{!onSearch}" style="width:120px; margin:0 0 0 5px;" />
            <apex:inputHidden id="radioSel" value="{!radioSel}"/>
            
            <apex:commandButton id="createNewClass"
                                reRender="none" 
                                value="New class" 
                                style="width:120px; margin:0 0 0 50px;" 
                                onclick="javacript:window.location='/setup/build/editApexClass.apexp?retURL=/apex/SCDeveloperDashBoard'" />
                                
            <apex:commandButton id="createNewPage" 
                                reRender="none"
                                value="New page" 
                                style="width:120px; margin:0 0 0 5px;" 
                                onclick="javacript:window.location='/066/e?retURL=/apex/SCDeveloperDashBoard'" />
                                
            <apex:commandButton id="createNewComponent" 
                                reRender="none"
                                value="New component" 
                                style="width:120px; margin:0 0 0 5px;" 
                                onclick="javacript:window.location='/099/e?retURL=/apex/SCDeveloperDashBoard'" />

            <apex:commandButton id="editFavorites" 
                                value="Favorites" 
                                style="width:120px; margin:0 0 0 100px;" 
                                action="{!onEditFavorites}"/>
           
            <!-- row 2 -->
            <input type="radio" value="1" name="radioGroup"  class="radioButton" onclick="setVal(this)"/>
            <apex:outputText value="Modified by: " styleClass="no_wrap"/>
            <apex:inputField value="{!resource.Employee__c}" style="width:200px;"/>
            <apex:panelGroup layout="none" styleClass="no_wrap">
                <apex:outputText value="in last: "/>
                <apex:inputText value="{!days}" style="width:20px;"/>
                <apex:outputText value=" days"/>
                <apex:outputText value="(1 day = today)"/>
            </apex:panelGroup>
            <apex:outputText value=""/>
            <apex:outputText value=""/>
            <apex:outputText value=""/>
            <apex:outputText value=""/>
            <apex:outputText value="OrgId: {!orgId}" style="font-size: 10px; margin:10px 0 0 100px; white-space: nowrap;"/>
        </apex:panelGrid>
    </apex:form>
    </apex:pageBlock>
        
    <apex:pageBlock id="result">
        <h2>Classes</h2>
        <apex:pageBlockTable value="{!results}" var="item">
           <apex:column headerValue="Name">
               <a href="/{!item.Id}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">
                    Edit
               </a>
               &nbsp;&nbsp;
                <a href="/{!item.Id}" target="_blank">
                    <apex:outputField value="{!item.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Changed by" value="{!item.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!item.LastModifiedDate}"/>
           <apex:column headerValue="Code Coverage">
                <apex:panelGroup layout="none" rendered="{!NOT(CONTAINS(UPPER(item.name), 'TEST'))}">
                    <a href="/setup/build/viewCodeCoverage.apexp?id={!item.id}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">
                        (CC %)
                    </a>
                </apex:panelGroup>
           </apex:column>
        </apex:pageBlockTable>
        
        <br/>   
        <h2>Pages</h2>
        <apex:pageBlockTable value="{!resultsP}" var="item3">
           <apex:column headerValue="Name">
               <a href="/{!item3.Id}/e" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">
                    Edit
               </a>
               &nbsp;&nbsp;
                <a href="/{!item3.Id}" target="_blank">
                    <apex:outputField value="{!item3.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Changed by" value="{!item3.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!item3.LastModifiedDate}"/>
        </apex:pageBlockTable>
        
        <br/>   
        <h2>Components</h2>
        <apex:pageBlockTable value="{!resultsComp}" var="itemComp">
           <apex:column headerValue="Name">
               <a href="/apexpages/setup/editApexComponent.apexp?id={!itemComp.Id}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">
                    Edit
               </a>
               &nbsp;&nbsp;
                <a href="/apexpages/setup/viewApexComponent.apexp?id={!itemComp.Id}" target="_blank">
                    <apex:outputField value="{!itemComp.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Changed by" value="{!itemComp.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!itemComp.LastModifiedDate}"/>
        </apex:pageBlockTable>
        
        <br/>   
        <h2>Trigger</h2>
        <apex:pageBlockTable value="{!resultsT}" var="item2">
           <apex:column headerValue="Name" >
                <a href="/{!item2.Id}/e" target="_blank">
                    <apex:outputField value="{!item2.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Changed by" value="{!item2.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!item2.LastModifiedDate}"/>
           <apex:column headerValue="Code Coverage">
                <a href="/setup/build/viewCodeCoverage.apexp?id={!item2.id}" target="_blank" class="actionLink" style="color: #015BA7;text-decoration:none;">
                    (CC %)
                </a>
           </apex:column>
        </apex:pageBlockTable>
        
        <br/>   
        <h2>Objects</h2>
        <apex:pageBlockTable value="{!resultsObj}" var="o">
           <apex:column headerValue="Name">
                <a href="/{!o.url}" target="_blank">
                    <apex:outputText value="{!o.name}"/>
                </a>
           </apex:column>
        
        <!--           
           <apex:column headerValue="Name">
                <a href="/{!item4.Id}" target="_blank">
                    <apex:outputField value="{!item4.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Changed by" value="{!item4.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!item4.LastModifiedDate}"/>
        -->           
        </apex:pageBlockTable>
        
        <br/>   
        <h2>User</h2>
        <apex:pageBlockTable value="{!resultsU}" var="item4">
           <apex:column headerValue="Action">
                <a href="/servlet/servlet.su?oid={!orgid}&suorgadminid={!item4.Id}&retURL=%2Fapex%2Fc&targetURL=%2Fhome%2Fhome.jsp" target="_blank">
                    Login
                </a>
           </apex:column>
           <apex:column headerValue="User" >
                <a href="/{!item4.Id}?noredirect=1" target="_blank">
                    <apex:outputField value="{!item4.name}"/>
                </a>
           </apex:column>
           <apex:column headerValue="Profile" value="{!item4.Profile.Name}"/>
           <apex:column headerValue="Role" value="{!item4.UserRole.Name}"/>
           <apex:column headerValue="Changed by" value="{!item4.LastModifiedBy.Name}"/>
           <apex:column headerValue="Changed on" value="{!item4.LastModifiedDate}"/>
        </apex:pageBlockTable>
    </apex:pageBlock>
</apex:panelGroup>

<apex:panelGroup id="favoritesPanel" rendered="{!editFavorites}">
    <apex:form accept="onSaveFavorites" style="margin:0 0 5px 0;">           
    <apex:pageBlock id="editFavorites">
        <apex:pageBlockButtons location="both">
            <apex:commandButton id="saveFavorites" 
                                value="Save" 
                                style="width:120px; margin:0 0 0 5px;" 
                                action="{!onSaveFavorites}"/>
            <apex:commandButton id="cancel" 
                                value="Cancel" 
                                style="width:120px; margin:0 0 0 5px;" 
                                action="{!onCancel}"/>
        </apex:pageBlockButtons>
        
        <apex:pageBlockSection collapsible="false" columns="1" title="Favorites" showHeader="true">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Classes"/>
                <c:SCMultiSelection index="1" allValues="{!allClasses}" curValues="{!curClasses}" curValueIds="{!curClassIds}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Pages"/>
                <c:SCMultiSelection index="2" allValues="{!allPages}" curValues="{!curPages}" curValueIds="{!curPageIds}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Components"/>
                <c:SCMultiSelection index="3" allValues="{!allComponents}" curValues="{!curComponents}" curValueIds="{!curComponentIds}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Triggers"/>
                <c:SCMultiSelection index="4" allValues="{!allTriggers}" curValues="{!curTriggers}" curValueIds="{!curTriggerIds}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Objects"/>
                <c:SCMultiSelection index="5" allValues="{!allObjects}" curValues="{!curObjects}" curValueIds="{!curObjectIds}"/>
            </apex:pageBlockSectionItem>
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Others"/>
                <c:SCMultiSelection index="6" allValues="{!allOthers}" curValues="{!curOthers}" curValueIds="{!curOtherIds}"/>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
    </apex:pageBlock>
    </apex:form>
</apex:panelGroup>
</apex:page>