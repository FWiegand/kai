<!--
 * @(#)SCStockItems.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCStock__c" 
    extensions="SCStockItemExtension" 
    showHeader="false">
    
<style>

.cp-selected-id
{
    visibility: hidden;
    float: right;
    max-width:1px !important;
}

.cp-selected-no
{
    visibility: hidden;
    float: right;
    max-width:1px !important;
}

.cp-selected-valType
{
    visibility: hidden;
    float: right;
    max-width:1px !important;
}

</style>

    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-jquery-ui.css')}" />

    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />

    <script type="text/javascript">
        // unset jQuerys $ variable, we have to 
        // use jQuery.method() instead or jQuery(...)
        var jq$ = jQuery.noConflict();
    
        var articleNo = '';
        var valuationType = '';
        
        /**
         * DOMREADY event processing
         */
        jQuery(document).ready(function()
        {
            var artMatmovesButtons = {};
            artMatmovesButtons["{!$Label.SC_btn_Ok}"] = function() { 
                                                            jQuery(this).dialog("close");
                                                        };
            
            var $artMatmoveDialog = jQuery("#article-matmoves-dialog").dialog({
                autoOpen: false,
                resizable: true,
                top: 0,
                left: 0, 
                height: 500,
                width: 1024,
                modal: true
            });
        
            $artMatmoveDialog.dialog("option", "buttons", artMatmovesButtons);
        });

        /**
         * Select the given entry
         */
        function selectEntry(element)
        {
            articleNo = jQuery('.cp-selected-no', element).text();
            valuationType = jQuery('.cp-selected-valType', element).text();
            readArticleMatMoves(jQuery('.cp-selected-id', element).text(), jQuery('.cp-selected-valType', element).text());
        }
        
        function selectEntry2(artid, artno, valtype)
        {
            articleNo = artno;
            valuationType = valtype;
            readArticleMatMoves(artid, valuationType);
        }

        /**
         * Open the article materialmovement dialog.
         *
         * @author Alexander Wagner <awagner@gms-online.de>
         */
        function openArticleMatmoveDialog()
        {
            jQuery('#article-matmoves-dialog').dialog("option", "title", "{!$Label.SC_app_MatMovesArticle} [" + articleNo + " " + valuationType + "]");
            jQuery('#article-matmoves-dialog').parent().appendTo(jQuery('form[id$=stockItems]'));
            jQuery('#article-matmoves-dialog').dialog('open');
        }
    </script>

    <apex:messages id="messages" />
    <apex:form prependId="false" id="stockItems">
        <apex:actionFunction name="readArticleMatMoves" action="{!readArticleMatMoves}" 
                             reRender="artMatMoves" oncomplete="openArticleMatmoveDialog();">
            <apex:param name="param1" assignTo="{!articleId}" value="" />
            <apex:param name="param2" assignTo="{!valuationType}" value="" />
        </apex:actionFunction>

        <apex:panelGroup id="resultBlock">
            <apex:selectList id="viewType" value="{!view}" size="1" multiselect="false">
                <apex:actionSupport event="onchange" action="{!fillList}"/>
                <apex:selectOptions value="{!viewList}"/>
            </apex:selectList>
            <br/><br/>

            <!-- replenishment preview list -->
            <apex:pageBlock rendered="{!isStockMatItems}">
                <apex:pageBlockTable value="{!stockMatInfos}" var="item" 
                                     onRowDblClick="selectEntry(this); return false;">
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputlink value="/{!item.articleId}" target="_parent">
                            <apex:outputText value="{!item.articleNo}" />
                        </apex:outputlink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.descr}" />
                    </apex:column>
                    <apex:column rendered="{!SCStock__c.ValuationType__c}">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.ValuationType__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.ValuationTypeTranslation}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ReplacedBy__c.label}</apex:facet>
                        <apex:outputText value="{!item.articleNoNew}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.LockType__c.label}</apex:facet>
                        <apex:outputText value="{!item.lockType}" />
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.qty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Qty__c.label} benötigt</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.reqQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$Label.SC_app_OrderedQty}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.ordQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MinQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.minQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MaxQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.maxQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.ReplenishmentQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.replQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.InventoryQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.inventQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.InventoryDate__c.label}</apex:facet>
                        <apex:outputText value="{0, date, dd.MM.yyyy}">
                            <apex:param value="{!item.inventDate}" />
                        </apex:outputText>
                        <apex:outputText styleClass="cp-selected-id" value="{!item.articleId}" />
                        <apex:outputText styleClass="cp-selected-no" value="{!item.articleNo}" />
                        <apex:outputText styleClass="cp-selected-valType" value="{!item.ValuationType}" />
                    </apex:column>
<!-- 15.03.2012 GMSNA disabled - not required for standard projects
                    <apex:column style="text-align:right" >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.mapQty}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right" >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapPrice__c.label}</apex:facet>
                        <apex:outputText value="{!item.mapPrice}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapDate__c.label}</apex:facet>
                        <apex:outputText value="{!item.mapDate}" />
                    </apex:column>
-->                    
                </apex:pageBlockTable> 
            </apex:pageBlock>
            
            <!-- stock item list -->
            <apex:pageBlock id="stockItemsId" rendered="{!isStockItems}">
                <apex:pageBlockTable value="{!stockItemsList}" var="item" 
                                     onRowDblClick="selectEntry(this); return false;">
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Name.label}</apex:facet>
                        <apex:outputlink value="/{!item.Id}" target="_parent">
                            <apex:outputField value="{!item.Name}" />
                        </apex:outputlink>
                        <apex:outputText >&nbsp;&nbsp;</apex:outputText>                    
                        <apex:commandLink onclick="selectEntry2('{!item.Article__r.id}', '{!item.Article__r.name}', '{!item.ValuationType__c}'); return false;"  >
                            <apex:image value="/img/tab/arrow.gif" title="{!$Label.SC_flt_STIV_Movements} {!item.Article__r.Name} {!item.ValuationType__c} " />
                        </apex:commandLink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputlink value="/{!item.Article__r.Id}" target="_parent">
                            <apex:outputField value="{!item.Article__r.Name}" />
                        </apex:outputlink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.Article__r.ArticleNameCalc__c}" />
                    </apex:column>
                    <apex:column rendered="{!SCStock__c.ValuationType__c}">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.ValuationType__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.ValuationType__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ReplacedBy__c.label}</apex:facet>
                        <apex:outputField value="{!item.Article__r.ReplacedBy__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.LockType__c.label}</apex:facet>
                        <apex:outputField value="{!item.Article__r.LockType__c}" />
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.Qty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MinQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.MinQty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MaxQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.MaxQty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.ReplenishmentQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.ReplenishmentQty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.InventoryQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.InventoryQty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.InventoryDate__c.label}</apex:facet>
                        <apex:outputField value="{!item.InventoryDate__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Permanent__c.label}</apex:facet>
                        <apex:outputField value="{!item.Permanent__c}" />
                        <apex:outputText styleClass="cp-selected-id" value="{!item.Article__r.Id}" />
                        <apex:outputText styleClass="cp-selected-no" value="{!item.Article__r.Name}" />
                        <apex:outputText styleClass="cp-selected-valType" value="{!item.ValuationType__c}" />
                    </apex:column>
<!-- 15.03.2012 GMSNA disabled - not required for standard projects
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapQty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.MapQty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapPrice__c.label}</apex:facet>
                        <apex:outputField value="{!item.MapPrice__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.MapDate__c.label}</apex:facet>
                        <apex:outputField value="{!item.MapDate__c}" />
                    </apex:column>
-->
                </apex:pageBlockTable> 
                <!-- paging part -->
                <c:SCPagination id="asPaginationStockItems" setController="{!setController}" pageRerender="stockItemsId" />
            </apex:pageBlock>
            
            <!-- material movement list -->            
            <apex:pageBlock id="matMoveListId" rendered="{!isMatMoves}">
                <apex:pageBlockTable value="{!matMoveList}" var="item" 
                                     onRowDblClick="selectEntry(this); return false;">
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.name.label}</apex:facet>
                        <apex:outputlink value="/{!item.Id}" target="_parent">
                        <apex:outputField value="{!item.Name}" />
                        </apex:outputlink>
                        <apex:outputText >&nbsp;&nbsp;</apex:outputText>                    
                        <apex:commandLink onclick="selectEntry2('{!item.Article__r.id}', '{!item.Article__r.name}', '{!item.ValuationType__c}'); return false;"  >
                            <apex:image value="/img/tab/arrow.gif" title="{!$Label.SC_flt_STIV_Movements} {!item.Article__r.Name} {!item.ValuationType__c} " />
                        </apex:commandLink>
                        
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputlink value="/{!item.Article__r.Id}" target="_parent">
                            <apex:outputField value="{!item.Article__r.Name}" />
                        </apex:outputlink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.Article__r.ArticleNameCalc__c}" />
                    </apex:column>
                    <apex:column rendered="{!SCStock__c.ValuationType__c}">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ValuationType__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.ValuationType__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.CreatedDate.label}</apex:facet>
                        <apex:outputField style="white-space: nowrap;" value="{!item.CreatedDate}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}</apex:facet>
                        <apex:outputField value="{!item.Type__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Status__c.label}</apex:facet>
                        <apex:outputField value="{!item.Status__c}" />
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.Qty__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.QtyNew__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.QtyNew__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column style="text-align:right">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.QtyOld__c.label}</apex:facet>
                        <apex:outputtext value="{0, number}">
                            <apex:param value="{!item.QtyOld__c}"/>
                        </apex:outputtext>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.DeliveryNote__c.label}</apex:facet>
                        <apex:outputField value="{!item.DeliveryNote__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.DeliveryStock__c.label}</apex:facet>
                        <apex:outputField value="{!item.DeliveryStock__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Order__c.label}</apex:facet>
                        <apex:outputField value="{!item.Order__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Replenishment__c.label}</apex:facet>
                        <apex:outputlink value="/{!item.Replenishment__r.Id}" target="_parent">
                        <apex:outputField value="{!item.Replenishment__r.Name}" />
                        </apex:outputlink>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.RequisitionDate__c.label}</apex:facet>
                        <apex:outputField style="white-space: nowrap;" value="{!item.RequisitionDate__c}" />
                        <apex:outputText styleClass="cp-selected-id" value="{!item.Article__r.Id}" />
                        <apex:outputText styleClass="cp-selected-no" value="{!item.Article__r.Name}" />
                        <apex:outputText styleClass="cp-selected-valType" value="{!item.ValuationType__c}" />
                    </apex:column>
                </apex:pageBlockTable> 
                <!-- paging part -->
                <c:SCPagination id="asPaginationMatMoves" setController="{!setController}" pageRerender="matMoveListId" />
            </apex:pageBlock>
        </apex:panelGroup>

        <!-- ############## Dialog for displaying material movements for an article ################# -->
        <div id="article-matmoves-dialog" title="">
            <apex:actionRegion >
                <apex:pageBlock >
                    <apex:pageBlockTable value="{!artMatMoves}" var="item" id="artMatMoves">
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.name.label}</apex:facet>
                            <apex:outputlink value="/{!item.Id}" target="_parent">
                            <apex:outputField value="{!item.Name}" />
                            </apex:outputlink>
                        </apex:column>
                        <!--
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                            <apex:outputlink value="/{!item.Article__r.Id}" target="_parent">
                                <apex:outputField value="{!item.Article__r.Name}" />
                            </apex:outputlink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                            <apex:outputText style="white-space: nowrap;" value="{!item.Article__r.ArticleNameCalc__c}" />
                        </apex:column>
                        <apex:column rendered="{!SCStock__c.ValuationType__c}">
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ValuationType__c.label}</apex:facet>
                            <apex:outputText style="white-space: nowrap;" value="{!item.ValuationType__c}" />
                        </apex:column>
                        -->
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.CreatedDate.label}</apex:facet>
                            <apex:outputField style="white-space: nowrap;" value="{!item.CreatedDate}" />
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}</apex:facet>
                            <apex:outputField value="{!item.Type__c}" />
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Status__c.label}</apex:facet>
                            <apex:outputField value="{!item.Status__c}" />
                        </apex:column>
                        <apex:column style="text-align:right">
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Qty__c.label}</apex:facet>
                            <apex:outputtext value="{0, number}">
                                <apex:param value="{!item.Qty__c}"/>
                            </apex:outputtext>
                        </apex:column>
                        <apex:column style="text-align:right">
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.QtyNew__c.label}</apex:facet>
                            <apex:outputtext value="{0, number}">
                                <apex:param value="{!item.QtyNew__c}"/>
                            </apex:outputtext>
                        </apex:column>
                        <apex:column style="text-align:right">
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.QtyOld__c.label}</apex:facet>
                            <apex:outputtext value="{0, number}">
                                <apex:param value="{!item.QtyOld__c}"/>
                            </apex:outputtext>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.DeliveryNote__c.label}</apex:facet>
                            <apex:outputField value="{!item.DeliveryNote__c}" />
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.DeliveryStock__c.label}</apex:facet>
                            <apex:outputField value="{!item.DeliveryStock__r.Name}" />
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Order__c.label}</apex:facet>
                            <apex:outputField value="{!item.Order__r.Name}" />
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Replenishment__c.label}</apex:facet>
                            <apex:outputlink value="/{!item.Replenishment__r.Id}" target="_parent">
                            <apex:outputField value="{!item.Replenishment__r.Name}" />
                            </apex:outputlink>
                        </apex:column>
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.RequisitionDate__c.label}</apex:facet>
                            <apex:outputField style="white-space: nowrap;" value="{!item.RequisitionDate__c}" />
                            <apex:outputText styleClass="cp-selected-id" value="{!item.Article__r.Id}" />
                            <apex:outputText styleClass="cp-selected-no" value="{!item.Article__r.Name}" />
                            <apex:outputText styleClass="cp-selected-valType" value="{!item.ValuationType__c}" />
                        </apex:column>
                    </apex:pageBlockTable> 
                </apex:pageBlock>
            </apex:actionRegion>
        </div>
        
    </apex:form>
</apex:page>