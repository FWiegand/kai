<!--
 * @(#)SCMaterialTransfer.page
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCStock__c" 
    extensions="SCMaterialTransferExtension" 
    showHeader="false"
    title="{!$Label.SC_app_MaterialTransfer}">

    <!-- ############# JS ################## -->
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-global.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/statusProcessing.js')}" />

<style>

input.errorLine
{
    background-color: #FF8080;
    margin: 0;
    text-indent: 0;
}

</style>

    <apex:pageBlock >
        <apex:sectionHeader title="{!$Label.SC_app_MaterialTransfer}" id="header" />
    </apex:pageBlock>

    <apex:form id="mainForm">
        <apex:panelGroup rendered="{!transferOk}">
            <script>
                opener.location.href="/{!SCStock__c.Id}";
                window.close();
            </script>
        </apex:panelGroup>
        
        <apex:pageBlock >
            <apex:pageBlockButtons location="bottom" >
                <apex:commandButton action="{!transferMat}" reRender="mainForm" value="       {!$Label.SC_btn_Ok}       " status="transferProcessing"/>
                <apex:commandButton action="{!disableChooseValuationType}" rendered="{!setStockItemsValType}" disabled="{NOT(!setStockItemsValType)}" value="{!$Label.SC_btn_TransMatDisableChooseValType}" reRender="mainForm" status="transferProcessing"/>
                <apex:commandButton action="javascript:window.close()" value="{!$Label.SC_btn_Cancel}" immediate="true"/>

                <apex:actionStatus id="transferProcessing" 
                                   startText="{!$Label.SC_app_Processing}" 
                                   stopText="" 
                                   onstart="onStartActionStatus();"
                                   onstop="onStopActionStatus()" 
                                   startStyleClass="processing" />
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1">
                <apex:outputField value="{!SCStock__c.Name}" />
                <apex:outputField value="{!SCStock__c.Info__c}" />

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_Receiver}" />
                    <apex:selectList value="{!receiverStock}" size="1" multiselect="false">
                        <apex:selectOptions value="{!receiverList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
      
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}" />
                    <apex:selectList value="{!matMoveType}" size="1" multiselect="false">
                        <apex:selectOptions value="{!typeList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>

        <apex:pageBlock >
            <apex:pageMessages />

            <apex:pageBlockSection columns="1" >
                <apex:pageBlockTable value="{!transInfoList}" var="item">
                    <apex:column >
                        <apex:facet name="header"></apex:facet>
                        <apex:inputCheckbox value="{!item.isSet}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputText value="{!item.stockItem.Article__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.ValuationType__c.label}</apex:facet>
                        <apex:outputText value="{!item.StockItemValTypeTranslation}" />
                    </apex:column>
                    <apex:column rendered="{!SetStockItemsValType}">
                        <apex:selectList value="{!item.valuationType}" size="1" multiselect="false" rendered="{!AND(NOT(item.existValType),item.hasArticleValType,item.isSet)}">
                            <apex:selectOptions value="{!valTypeList}"/>
                        </apex:selectList>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText value="{!item.stockItem.Article__r.ArticleNameCalc__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCStockItem__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputText value="{0, number, 0}">
                            <apex:param value="{!item.stockItem.Qty__c}" />
                        </apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$Label.SC_app_TransferQty}</apex:facet>
                        <apex:inputText value="{!item.qty}" styleClass="{!IF(item.hasError, "errorLine", "")}"/>
                    </apex:column>
                </apex:pageBlockTable> 
            </apex:pageBlockSection>   
        </apex:pageBlock>
    </apex:form>
</apex:page>