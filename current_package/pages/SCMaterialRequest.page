<!--
 * @(#)SCMaterialRequest.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCStock__c" 
    extensions="SCMaterialRequestExtension" 
    showHeader="false"
    title="{!$Label.SC_app_MaterialRequest}">
    <apex:pageBlock >
        <apex:sectionHeader title="{!$Label.SC_app_MaterialRequest}" id="header" />
    </apex:pageBlock>

    <apex:form >
        <apex:panelGroup rendered="{!NOT(assignmentOk)}">
            <script>
                alert('{!$Label.SC_msg_ReplenishmentNoAssignment}');
            </script>
        </apex:panelGroup>
        <apex:panelGroup rendered="{!requestOk}">
            <script>
                opener.location.href="/{!SCStock__c.Id}";
                window.close();
            </script>
        </apex:panelGroup>
        
        <apex:pageBlock >
            <apex:pageBlockButtons location="bottom" >
                <apex:commandButton action="{!requestMat}" value="       {!$Label.SC_btn_Ok}       "/>
                <apex:commandButton action="javascript:window.close()" value="{!$Label.SC_btn_Cancel}" immediate="true"/>
            </apex:pageBlockButtons>

            <apex:pageMessages />

            <apex:pageBlockSection columns="1">
                <apex:outputField value="{!SCStock__c.Name}" />
                <apex:outputField value="{!SCStock__c.Info__c}" />
            </apex:pageBlockSection>
  
            <apex:actionRegion >
            <apex:pageBlockSection columns="1">
                <apex:inputField value="{!stockItem.Article__c}" required="true">
                    <apex:actionSupport event="onblur" reRender="actualStock,stockItemValType" 
                        action="{!initQuantity}">
                    </apex:actionSupport>
                </apex:inputField>
            </apex:pageBlockSection>
            </apex:actionRegion>
            
            <!-- GMSNA 07.02.2012 - valuation type is optional for requisition:    
                 GMSNA required="{!AND(SCStock__c.ValuationType__c,articleValType)}"
            <apex:actionRegion rendered="{!SCStock__c.ValuationType__c}">
            <apex:pageBlockSection columns="1" rendered="{!SCStock__c.ValuationType__c}" id="stockItemValType">
                <apex:inputField value="{!stockItem.ValuationType__c}" rendered="{!AND(SCStock__c.ValuationType__c,articleValType)}" > 
                    <apex:actionSupport event="onchange" reRender="actualStock,stockItemValType" 
                        action="{!initQuantity}">
                    </apex:actionSupport>
                </apex:inputField>
                <apex:outputField value="{!stockItem.ValuationType__c}" rendered="{!AND(SCStock__c.ValuationType__c,NOT(articleValType))}"/>
            </apex:pageBlockSection>
            </apex:actionRegion>

            <apex:pageBlockSection id="actualStock" columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_ActualStockQuantity}" />
                <apex:outputText value="{!curQty}"/>
            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            -->

            <apex:pageBlockSection columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCStockItem__c.fields.Qty__c.label}" for="qty"/>
                <apex:inputText id="qty" value="{!qty}" size="10" required="true"/>
            </apex:pageBlockSectionItem>
  
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}" />
                <apex:selectList value="{!matMoveType}" size="1" multiselect="false">
                    <apex:selectOptions value="{!typeList}"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

            <apex:pageBlockSection id="list" columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_Origin}" />
                <apex:selectList value="{!sourceStock}" size="1" multiselect="false">
                    <apex:selectOptions value="{!sourceList}"/>
                </apex:selectList>
            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

            <apex:actionRegion >
            <apex:pageBlockSection id="reqDate" columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_RequireDate}" />
                <apex:inputField value="{!assignment.ValidFrom__c}" required="true">
                    <apex:actionSupport event="onchange" reRender="list,reqDate" 
                        action="{!refillLists}">
                    </apex:actionSupport>
                </apex:inputField>
            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            </apex:actionRegion>

            <apex:pageBlockSection columns="1">
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_RequestDate}" />
                <apex:outputField value="{!assignment.ValidTo__c}" />
            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>