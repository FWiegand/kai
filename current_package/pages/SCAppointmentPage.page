<!--
 * @(#)SCAppointmentPage.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author H. Schroeder <hschroeder@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page StandardController="SCOrderItem__c" Extensions="SCAppointmentExtension" >
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />
    <script type="text/javascript"> 
        function disableInputs()
        {
            jQuery(':input').attr('disabled',true);
        }
    </script>
    
    <apex:form >
        <apex:pageBlock title="Appointment Proposals">

            <apex:pageBlockButtons >
                <apex:commandButton action="{!refresh}" value="Get appointments" />
                <!-- apex:commandButton action="{!save}" value="{!$Label.SC_btn_Save}"/ -->
            </apex:pageBlockButtons>

            <apex:actionFunction name="saveSelected" action="{!save}" />
            <apex:messages rendered="{!ISNULL(apptList)}" />

            <apex:pageBlockTable id="suggestions" var="appt" value="{!apptList}" rendered="{!NOT(ISNULL(apptList))}">

                <apex:column headerValue="Auto">
                    <apex:inputCheckbox value="{!appt.selected}"
                        onclick="saveSelected(); disableInputs();" />
                </apex:column>

               <apex:column headerValue="{!$Label.SC_app_Date}">
                    <apex:outputText value="{0, date, dd.MM.yyyy E}">
                        <apex:param value="{!appt.day}" />
                    </apex:outputText>
                </apex:column>
                <apex:column headerValue="{!$Label.SC_app_From}">
                    <apex:outputText value="{0, time, HH:mm}">
                        <apex:param value="{!appt.cdstart}" />
                    </apex:outputText>
                </apex:column>
                <apex:column headerValue="{!$Label.SC_app_To}">
                    <apex:outputText value="{0, time, HH:mm}">
                        <apex:param value="{!appt.cdend}" />
                    </apex:outputText>
                </apex:column>

               <apex:column headerValue="{!$Label.SC_app_Fixed}">
                    <apex:inputCheckbox value="{!appt.selectedFixed}"
                        onclick="saveSelectedFixed(); disableInputs();" />
               </apex:column>

               <apex:column headerValue="{!$ObjectType.SCResource__c.label}">
                   <apex:outputText >{!appt.employeefirstname} {!appt.employeename}</apex:outputText>
               </apex:column>

               <apex:column value="{!appt.employeeId}" headerValue="{!$ObjectType.SCResource__c.label}" />
               <apex:column value="{!appt.employeeFunc}" headerValue="{!$ObjectType.SCResource__c.fields.Type__c.label}" />

               <apex:column value="{!appt.address}" headerValue="{!$Label.SC_app_PreviousAddress}" />
               <apex:column value="{!appt.tour}" headerValue="{!$Label.SC_app_TourExtension}" />

            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
</apex:page>