<!--
 * @(#)SCTimereportItemEdit.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
-->

<apex:page controller="SCTimereportItemEditController"
    title="Modify" 
    showHeader="false"
    sidebar="false" tabStyle="Inbox__tab">

<script type="text/javascript" language="JavaScript">

distance = 0;
mileage = 0;
//oldMileage = {!timereportSingle.Mileage__c};
// old mileage is equals last mileage
oldMileage = {!lastMileageEntry};

function setDistance(newDistance)
{   
    // replace point with nothing
    newDistance = newDistance.replace('.', '');
    // replace comma with a point
    newDistance = newDistance.replace(/,/, '.');
        
    if (newDistance.length > 5)
    {
        alert('{!$Label.SC_msg_NumValTooLarge}');
        document.getElementById(idDistance).value = distance;
    }
    else if (newDistance.length > 0)
    {
        newDistance = Math.ceil(newDistance);
    
        if (newDistance < 0)
        {
            alert('{!$Label.SC_msg_DistanceNegativ}');
            
            document.getElementById(idDistance).value = distance;
            document.getElementById(idMileage).value = mileage;        
        }
        else if ((newDistance >= 0) && (newDistance < 100000))
        {
            distance = newDistance;
            mileage = oldMileage + distance;
        }
        
        document.getElementById(idDistance).value = distance;
        document.getElementById(idMileage).value = mileage;
    }
}

function setMileage(newMileage)
{
    // replace point with nothing
    newMileage = newMileage.replace('.', '');
    // replace comma with a point
    newMileage = newMileage.replace(/,/, '.');
        
    if (newMileage.length > 10)
    {
        alert('{!$Label.SC_msg_NumValTooLarge}');
        document.getElementById(idMileage).value = mileage;
    }
    else if (newMileage.length > 0)
    {
        newMileage = Math.ceil(newMileage);
    
        if (newMileage < 0)
        {
            alert('{!$Label.SC_msg_MileageNegativ}');
            mileage = Math.ceil(oldMileage);
        }
        else if (newMileage >= 0)
        {
            if (newMileage < oldMileage)
            {
                mileage = Math.ceil(oldMileage);
                alert('{!$Label.SC_msg_MileageWrong}' + ' ' + oldMileage);
            }
            else
            {
                distance = newMileage - oldMileage;
                if (distance >= 100000)
                {
                    alert('{!$Label.SC_msg_NumValTooLarge}');
                    distance = oldMileage - mileage;
                    newMileage = mileage;
                }
                mileage = newMileage;
            }    
        }
                
        document.getElementById(idDistance).value = distance;
        document.getElementById(idMileage).value = mileage;
    }
}

/**
 * Handle keypress on time report elements 
 */
function timeReportEvent(element, event)
{
    // Catch the return key
    if (event.keyCode == 13)
    {
        // prevent Firefox from submitting the first 
        // submit button even hidden ones
        try
        {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        catch (err)
        {
        }

        return true;
    }

    return true;
}

if ('{!newEntryTimeReport.Distance__c}'.length > 0) { distance = new Number({!newEntryTimeReport.Distance__c}); }
if ('{!newEntryTimeReport.Mileage__c}'.length > 0) { mileage = new Number({!newEntryTimeReport.Mileage__c}); }
            
    function closeDialog()
    {
        window.close();
    }
</script>
    
    <!-- Attribute Definitions -->
    <!-- apex:attribute name="tid" assignTo="{!tid}" description="time report id" type="String" required="true"/--> 
    <apex:form >
    
        <!--apex:actionFunction name="setDistance" action="{!setDistance}" 
                             reRender="timeReport" focus="idMileage">
                <apex:param name="p1" assignTo="{!newEntryTimeReport.Distance__c}" value="" />
        </apex:actionFunction>
        <apex:actionFunction name="setMileage" action="{!setMileage}" 
                             reRender="timeReport" focus="idTravelDur">
                <apex:param name="p1" assignTo="{!newEntryTimeReport.Mileage__c}" value="" />
        </apex:actionFunction-->
        
        <apex:panelGroup layout="none" id="mainwindow">
            <apex:panelGroup layout="none" rendered="{!allOk}">
                <script>
                    // opener.location.reload();
                    opener.refreshTimeReport();
                    window.close();
                </script>
            </apex:panelGroup>
            
            <!-- ################### message Block ###################### -->
            <apex:pageBlock id="messageBlock">
                <apex:pagemessages />
            </apex:pageBlock>            
            <!-- ################# Time Report Entry #################### -->
            <apex:pageBlock id="timeReportEntry" 
                            title="{!IF(isEditMode, 
                                     $Label.SC_app_TimeReportEdit, 
                                     $Label.SC_app_TimeReportAdd)}" 
                            tabStyle="Inbox__tab" mode="edit">
                <!-- ################### Buttons ###################### -->
                <!-- action="{!addTimeRepItem}" -->
                <apex:pageBlockButtons location="bottom" >
                    <apex:commandButton value="{!$Label.SC_btn_Ok}"
                        action="{!addTimeRepItemWithMode}"
                        title="{!$Label.SC_btn_Ok}"
                        status="cp-action-status-full"
                        reRender="mainwindow, messageBlock" />
                    <apex:commandButton value="{!$Label.SC_btn_Cancel}"
                        action="javascript:window.close()" 
                        immediate="true" 
                        title="{!$Label.SC_btn_Cancel}"
                        reRender="mainwindow" />
                </apex:pageBlockButtons>
                <apex:pageBlockSection columns="1" id="inputDayBegin">    
                    <apex:inputField value="{!newEntryTimeReport.Start__c}" 
                                      rendered="{!NOT(isEditMode)}"/>
                    <apex:inputField value="{!newEntryTimeReport.End__c}" 
                                     rendered="{!NOT(isEditMode)}"/>
                    <apex:pageBlockSectionItem rendered="{!NOT(isEditMode)}">
                        <apex:outputText > {!$ObjectType.SCTimeReport__c.fields.Type__c.label} </apex:outputText>
                        <apex:selectList value="{!selectedTimeReportType}" 
                                         multiselect="false" 
                                         size="1" 
                                         id="timereptypes"> 
                            <apex:selectOptions value="{!TimeRepTypes}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>                     
                    <apex:inputField value="{!newEntryTimeReport.Description__c}" 
                                     rendered="{!NOT(isEditMode)}"/>
                    <apex:inputField value="{!newEntryTimeReport.Distance__c}" 
                                     id="idDistance" 
                                     rendered="{!NOT(isEditMode)}" 
                                     onBlur="setDistance(this.value);"
                                     onkeypress="timeReportEvent(this, event)">                
                    </apex:inputField>  
                    <apex:inputField value="{!newEntryTimeReport.Mileage__c}"
                                     id="idMileage"
                                     rendered="{!NOT(isEditMode)}" 
                                     onBlur="setMileage(this.value);"
                                     onkeypress="timeReportEvent(this, event)">                
                    </apex:inputField>
                    <apex:inputField value="{!timereportSingle.Start__c}"
                                     rendered="{!isEditMode}"/>
                    <apex:inputField value="{!timereportSingle.CompensationType__c}" 
                                     rendered="{!AND(isEditMode, isDayStart)}"/>
                    <apex:inputField value="{!timereportSingle.Mileage__c}" 
                                     rendered="{!AND(isEditMode, isDayStart)}"/>
                    <apex:inputField value="{!timereportSingle.End__c}" 
                                     rendered="{!AND(isEditMode, NOT(isDayStart))}"/>
                    <!-- time report type select list -->
                    <apex:pageBlockSectionItem rendered="{!AND(isEditMode, NOT(isDayStart))}">
                        <apex:outputText > {!$ObjectType.SCTimeReport__c.fields.Type__c.label} </apex:outputText>
                        <apex:selectList value="{!selectedTimeReportType}" 
                                         multiselect="false" 
                                         size="1" 
                                         id="timereptypes"> 
                            <apex:selectOptions value="{!TimeRepTypes}"/>
                        </apex:selectList>
                    </apex:pageBlockSectionItem>                                     
                                     
                    <apex:inputField value="{!timereportSingle.Description__c}" 
                                     rendered="{!AND(isEditMode, NOT(isDayStart))}"/>
                    <apex:inputField value="{!timereportSingle.Distance__c}" 
                                     rendered="{!AND(isEditMode, NOT(isDayStart))}"/>
                                                     
                    <apex:pageBlockSectionItem rendered="{!NOT(isEditMode)}">
                        <apex:outputText > {!$Label.SC_app_FillGaps} </apex:outputText>
                            <apex:inputCheckbox value="{!fillGaps}"/>  
                    </apex:pageBlockSectionItem>  
                                        
                    <script>
                        idDistance = '{!$Component.idDistance}';
                        idMileage = '{!$Component.idMileage}';
                    </script>                     
                </apex:pageBlockSection> 
            </apex:pageBlock>
         </apex:panelGroup>
    </apex:form>
    <c:SCActionStatusFull statusText="{!$Label.SC_app_Processing}" />    
</apex:page>