<apex:page action="{!getContractsWithoutPostalcode}" showHeader="true" sidebar="true" controller="SCConfigOverviewController" tabstyle="ServiceConfiguration__tab">
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.cookie.js')}" />

    <apex:form >
        <apex:pageBlock title="{!$Label.SC_app_ServiceConf_Titel}">
            <apex:pageMessage severity="{!severity}" strength="1" summary="{!messageText}" id="asemessage"/>
            <apex:actionPoller action="{!getJobs}" rerender="asemessage, asebutton" interval="5" />
            <div style="margin: 12px 0 10px 0;">
                <apex:outputText style="font-weight: bold; margin-right: 3px;" value="{!$Label.SC_app_ServiceConf_EPAdress}:"/>
                <apex:outputLink value="{!endpoint}">{!endpoint}</apex:outputLink>
            </div>
            <apex:outputText >{!$Label.SC_app_ServiceConf_MasterDataCommands}</apex:outputtext><BR/><br/>
            <apex:inputCheckbox id="domains" value="{!domains}" />
            <apex:outputLabel value="{!$Label.SC_app_ServiceConf_Domains}" for="domains"/>
            <br/>
            <apex:inputCheckbox id="parameters" value="{!parameters}" />
            <apex:outputLabel value="{!$Label.SC_app_ServiceConf_Parameters}" for="parameters"/>
            <br/>
            <apex:inputCheckbox id="resources" value="{!resources}" />
            <apex:outputLabel value="{!$Label.SC_app_ServiceConf_Resources}" for="resources"/>
            <br/>
            <apex:inputCheckbox id="calendars" value="{!calendars}" />
            <apex:outputLabel value="{!$Label.SC_app_ServiceConf_Calendars}" for="calendars"/>
            <br/>
            <apex:inputCheckbox id="qualifications" value="{!qualifications}" />
            <apex:outputLabel value="{!$Label.SC_app_ServiceConf_Qualifications}" for="qualifications"/>
            <br/><br/>
            <apex:commandButton value="{!$Label.SC_app_ServiceConf_Refresh}" action="{!refresh}" rerender="asemessage, asebutton" disabled="{!OR(runningJobs > 0, severity == 'error')}" id="asebutton"/>
            <apex:panelGroup layout="none" rendered="{!NOT(authorized)}">
                <script>jQuery.cookie('apex__ase_sid', null, {path: '/'});alert('Scheduling Engine Authorization failed\nPlease refresh this page.')</script> 
            </apex:panelGroup>
        </apex:pageBlock> 
    
        <apex:pageBlock title="{!$Label.SC_app_ServiceConf_Titel_Configuration}" >
            <apex:outputtext > {!$Label.SC_app_ServiceConf_Text_Configuration}</apex:outputtext>
            <P/>
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_MasterData}">
                <apex:outputLink value="{!URLFOR($Action.Brand__c.Tab, $ObjectType.Brand__c)}" >• {!$ObjectType.Brand__c.label} - {!$Label.SC_app_ServiceConf_EditBrandsCompanyCompetitors} </apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCPriceGroup__c.Tab, $ObjectType.SCPriceGroup__c)}" >• {!$ObjectType.SCPriceGroup__c.label} - {!$Label.SC_app_ServiceConf_PriceGroups}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCArticle__c.Tab, $ObjectType.SCArticle__c)}" >• {!$ObjectType.SCArticle__c.label} - {!$Label.SC_app_ServiceConf_ArtSpareParts}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCPriceList__c.Tab, $ObjectType.SCPriceList__c)}" >• {!$ObjectType.SCPriceList__c.label} - {!$Label.SC_app_ServiceConf_PriceList}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCArticleDetail__c.Tab, $ObjectType.SCArticleDetail__c)}" >• {!$ObjectType.SCArticleDetail__c.label} - {!$Label.SC_app_ServiceConf_BrandArticleDetails}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCPriceListItem__c.Tab, $ObjectType.SCPriceListItem__c)}" >• {!$ObjectType.SCPriceListItem__c.label} - {!$Label.SC_app_ServiceConf_PricesArticlesGroups}</apex:outputLink> 
            </apex:pageblocksection>
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Title_Account}">
                <apex:outputLink value="{!URLFOR($Action.Account.Tab, $ObjectType.Account)}" >• {!$ObjectType.Account.label} - {!$Label.SC_app_ServiceConf_BusinessEndCustomerAccounts } </apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCInstalledBaseRole__c.Tab, $ObjectType.SCInstalledBaseRole__c)}" >• {!$ObjectType.SCInstalledBaseRole__c.label} - {!$Label.SC_app_ServiceConf_AccountInstalledBaseRelationship}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCInstalledBaseLocation__c.Tab, $ObjectType.SCInstalledBaseLocation__c)}" >• {!$ObjectType.SCInstalledBaseLocation__c.label} - {!$Label.SC_app_ServiceConf_LocationsAppliances}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCAccountRole__c.Tab, $ObjectType.SCAccountRole__c)}" >• {!$ObjectType.SCAccountRole__c.label} - {!$Label.SC_app_ServiceConf_DefaultAccountRoles}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCInstalledBase__c.Tab, $ObjectType.SCInstalledBase__c)}" >• {!$ObjectType.SCInstalledBase__c.label} - {!$Label.SC_app_ServiceConf_ApplianceDetails}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.Contact.Tab, $ObjectType.Contact)}" >• {!$ObjectType.Contact.label} - {!$Label.SC_app_ServiceConf_ContactPartner}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCInstalledBaseSystem__c.Tab, $ObjectType.SCInstalledBaseSystem__c)}" >• {!$ObjectType.SCInstalledBaseSystem__c.label} - {!$Label.SC_app_ServiceConf_SystemsUsedGroupAppliances}</apex:outputLink> 
            </apex:pageblocksection>             
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Titel_MaterialManagement}">
                <apex:outputLink value="{!URLFOR($Action.SCPlant__c.Tab, $ObjectType.SCPlant__c)}" >• {!$ObjectType.SCPlant__c.label} - {!$Label.SC_app_ServiceConf_EditPlants}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCStockProfile__c.Tab, $ObjectType.SCStockProfile__c)}" >• {!$ObjectType.SCStockProfile__c.label} - {!$Label.SC_app_ServiceConf_EditVanStockReplenishment}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCStock__c.Tab, $ObjectType.SCStock__c)}" >• {!$ObjectType.SCStock__c.label} - {!$Label.SC_app_ServiceConf_EditVanStock}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCStockProfileItem__c.Tab, $ObjectType.SCStockProfileItem__c)}" >• {!$ObjectType.SCStockProfileItem__c.label} - {!$Label.SC_app_ServiceConf_EditStockProfileItems}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCStockItem__c.Tab, $ObjectType.SCStockItem__c)}" >• {!$ObjectType.SCStockItem__c.label} - {!$Label.SC_app_ServiceConf_AnalyseStockItems}</apex:outputLink> 
                <!-- <apex:outputText >&nbsp;</apex:outputText> -->
                <apex:outputLink value="{!URLFOR($Action.SCMaterialMovement__c.Tab, $ObjectType.SCMaterialMovement__c)}" >• {!$ObjectType.SCMaterialMovement__c.label} - {!$Label.SC_app_ServiceConf_AnalyseMaterialMovements}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCMaterialReplenishment__c.Tab, $ObjectType.SCMaterialReplenishment__c)}" >• {!$ObjectType.SCMaterialReplenishment__c.label} - {!$Label.SC_app_ServiceConf_AnalyseMaterialreplenishment}</apex:outputLink> 
            </apex:pageblocksection>             
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Titel_Resources}">
                <apex:outputLink value="{!URLFOR($Action.SCResource__c.Tab, $ObjectType.SCResource__c)}" >• {!$ObjectType.SCResource__c.label} - {!$Label.SC_app_ServiceConf_EditResourcesForScheduling}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCResourceAssignment__c.Tab, $ObjectType.SCResourceAssignment__c)}" >• {!$ObjectType.SCResourceAssignment__c.label} - {!$Label.SC_app_ServiceConf_AnalysisIntervalBasedAssignment}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCCalendar__c.Tab, $ObjectType.SCCalendar__c)}" >• {!$ObjectType.SCCalendar__c.label} - {!$Label.SC_app_ServiceConf_EditCalendars}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCCalendarItem__c.Tab, $ObjectType.SCCalendarItem__c)}" >• {!$ObjectType.SCCalendarItem__c.label} - {!$Label.SC_app_ServiceConf_AnalysisCalendarItems}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCWorktimeProfile__c.Tab, $ObjectType.SCWorktimeProfile__c)}" >• {!$ObjectType.SCWorktimeProfile__c.label} - {!$Label.SC_app_ServiceConf_EditWorkingTimes}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCQualification__c.Tab, $ObjectType.SCQualification__c)}" >• {!$ObjectType.SCQualification__c.label} - {!$Label.SC_app_ServiceConf_EditQualificationBaseData}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCQualificationProfile__c.Tab, $ObjectType.SCQualificationProfile__c)}" >• {!$ObjectType.SCQualificationProfile__c.label} - {!$Label.SC_app_ServiceConf_EditQualificationProfiles}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCQualificationProfileItem__c.Tab, $ObjectType.SCQualificationProfileItem__c)}" >• {!$ObjectType.SCQualificationProfileItem__c.label} - {!$Label.SC_app_ServiceConf_AnalysisQualificationProfileItems}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCArea__c.Tab, $ObjectType.SCArea__c)}" >• {!$ObjectType.SCArea__c.label} - {!$Label.SC_app_ServiceConf_EditPostcodeAreas}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCBusinessUnit__c.Tab, $ObjectType.SCBusinessUnit__c)}" >• {!$ObjectType.SCBusinessUnit__c.label} - {!$Label.SC_app_ServiceConf_EditBusinessUnits}</apex:outputLink> 


            </apex:pageblocksection>             
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Title_Configuration}">
                <apex:outputLink value="{!URLFOR($Action.SCConfOrderRepairCode__c.Tab, $ObjectType.SCConfOrderRepairCode__c)}" >• {!$ObjectType.SCConfOrderRepairCode__c.label} - {!$Label.SC_app_ServiceConf_EditRepairCode} </apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCConfOrderDuration__c.Tab, $ObjectType.SCConfOrderDuration__c)}" >• {!$ObjectType.SCConfOrderDuration__c.label} - {!$Label.SC_app_ServiceConf_EditStdOrderDurationRules}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCConfOrderLine__c.Tab, $ObjectType.SCConfOrderLine__c)}" >• {!$ObjectType.SCConfOrderLine__c.label} - {!$Label.SC_app_ServiceConf_EditStdOrderOrderLineRules }</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCConfSequence__c.Tab, $ObjectType.SCConfSequence__c)}" >• {!$ObjectType.SCConfSequence__c.label} - {!$Label.SC_app_ServiceConf_EditSequenceRules}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCConfOrderPrio__c.Tab, $ObjectType.SCConfOrderPrio__c)}" >• {!$ObjectType.SCConfOrderPrio__c.label} - {!$Label.SC_app_ServiceConf_EditOrderPriorityRules}</apex:outputLink> 
            </apex:pageblocksection>             
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Title_ConfigurationDomain}">
                <apex:outputLink value="{!URLFOR($Action.SCDomainRef__c.Tab, $ObjectType.SCDomainRef__c)}" >• {!$ObjectType.SCDomainRef__c.label} - {!$Label.SC_app_ServiceConf_AnalysisDomains}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCDomainValue__c.Tab, $ObjectType.SCDomainValue__c)}" >• {!$ObjectType.SCDomainValue__c.label} - {!$Label.SC_app_ServiceConf_AnalysisDomainValues}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCDomainDescription__c.Tab, $ObjectType.SCDomainDescription__c)}" >• {!$ObjectType.SCDomainDescription__c.label} - {!$Label.SC_app_ServiceConf_AnalysisDomainValueTranslations}</apex:outputLink> 
                <apex:outputLink value="{!URLFOR($Action.SCDomainDepend__c.Tab, $ObjectType.SCDomainDepend__c)}" >• {!$ObjectType.SCDomainDepend__c.label} - {!$Label.SC_app_ServiceConf_AnalysisDomainValueDependencies}</apex:outputLink> 
                <!-- <apex:outputText >&nbsp;</apex:outputText> -->
                <apex:outputLink value="SCInvalidDomDepends" >• {!$ObjectType.SCDomainDepend__c.label} - {!$Label.SC_app_ServiceConf_ShowInvalidDomainDepends }</apex:outputLink> 
            </apex:pageblocksection>             
            <apex:pageblocksection columns="2" title="{!$Label.SC_app_ServiceConf_Titel_Interfaces}">
                <apex:outputLink value="{!URLFOR($Action.SCInterfaceLog__c.Tab, $ObjectType.SCInterfaceLog__c)}" >• {!$ObjectType.SCInterfaceLog__c.label} - {!$Label.SC_app_ServiceConf_InterfaceLog}</apex:outputLink> 
            </apex:pageblocksection>
            <apex:pageBlockSection columns="2" title="{!$Label.SC_app_ServiceConf_Titel_PostalCodes}" id="contractPostalCode">
                <apex:PageBlockSectionItem >
                    <apex:commandButton value="{!$Label.SC_app_ServiceConf_RefreshContracts}" action="{!getRefreshContracts}" rerender="contractPostalCode"/>
                </apex:PageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <!-- apex:actionPoller action="{!getcontractsWithoutPostalcode}" rerender="contractPostalCode" interval="5"/ -->
                   {!$Label.SC_app_ServiceConf_ContractsWithoutPostalCode}: {!IF(nullPostalcodes = null, $Label.SC_app_ServiceConf_Processing, nullPostalcodes)}
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>           
        </apex:pageBlock>
        
        <apex:pageBlock title="Maintenance Job Control">
        
            <apex:actionPoller action="{!getJobsInfo}" rerender="calcDueDateInfoId, createOrderInfoId" interval="5" />
            <!-- Calculating due dates-->            
            <apex:pageBlockSection columns="2" title="Calculating dates" id="calculatingDates">
                <apex:commandButton value="Start Calculate Dates" action="{!onMaintenanceCalcDueDate}" />
                <!--apex:commandButton value="Refresh View of Last Job" action="{!onMaintenanceRefreshCalcDueDate}" /-->
                <apex:commandButton value="Abort Calculating Dates" action="{!onMaintenanceAbortCalcDueDate}" />
                <apex:outputText value="SCbtcMaintenanceDueDateSet.asyncCalculateAll {!calcDueDateJobInfo}"  id="calcDueDateInfoId"/>
            </apex:pageBlockSection>     

                  
            <!-- Creating orders-->            
            <apex:pageBlockSection columns="2" title="Creating orders" id="creatingOrders">
                <apex:commandButton value="Start Create Orders" action="{!onMaintenanceCreateOrders}" />
                <!--apex:commandButton value="Refresh of Last Job" action="{!onMaintenanceRefreshCreateOrders}" /-->
                <apex:commandButton value="Abort Creating Orders" action="{!onMaintenanceAbortCreateOrders}" />
                <apex:outputText value="SCbtcMaintenanceOrderCreate.asyncCreateAll {!createOrderJobInfo}" id="createOrderInfoId"/>
            </apex:pageBlockSection>           


            
        </apex:pageBlock>
    </apex:form>
</apex:page>