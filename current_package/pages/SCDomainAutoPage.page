<apex:page id="Page" tabstyle="SCAutoDomainConfig__tab" controller="SCDomainAutoPageController">

    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
    
    <apex:form >
        <!-- apex:commandButton value="Show Admin-Area" action="{!changeShowAdminArea}" rendered="{!AND(IsAdmin,NOT(ShowAdminArea))}"/ -->
        <!-- apex:commandButton value="Hide Admin-Area" action="{!changeShowAdminArea}" rendered="{!AND(IsAdmin,ShowAdminArea)}"/ -->
        
        <!-- apex:pageBlock title="Overview" rendered="{!NOT(AND(IsAdmin,ShowAdminArea))}" -->
        <apex:pageBlock title="Overview" rendered="{!NOT(IsAdmin)}">
            Under Construction.
        </apex:pageBlock>
        
        <!-- apex:pageBlock title="Overview (for System Administrators)" rendered="{!AND(IsAdmin,ShowAdminArea)}" id="AdminAreaId" -->
        <apex:pageBlock title="Overview (for System Administrators)" rendered="{!IsAdmin}" id="AdminAreaId">
            <apex:pageBlockSection columns="1" showHeader="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="The last step was:" />
                    <apex:outputText value="{!LastStep}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Current User-Language:" />
                    <apex:outputText value="{!currentUserLanguage}" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Change UserLanguage" columns="3" id="ChangeUserLanguage1">
                <apex:commandButton value="German" action="{!LangDE}" disabled="{!FutureJobsRunning}"/>
                <apex:commandButton value="US-English" action="{!LangENUS}" disabled="{!FutureJobsRunning}"/>
                <apex:commandButton value="Dutch" action="{!LangNLNL}" disabled="{!FutureJobsRunning}"/>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Overview of the asynchronous jobs" columns="1" id="OverviewAsynJobs">
                <table>
                    <tr>
                        <td>Waiting:  </td>
                        <td>{!OverviewFutureJobsWaiting}</td>
                    </tr>
                    <tr>
                        <td>Running:  </td>
                        <td>{!OverviewFutureJobsRunning}</td>
                    </tr>
                    <tr>
                        <td>Finished:  </td>
                        <td>{!OverviewFutureJobsFinished}</td>
                    </tr>
                    <tr>
                        <td>Finished with Errors:  </td>
                        <td>{!OverviewFutureJobsError}</td>
                    </tr>
                    <tr>
                        <td>Aborted:  </td>
                        <td>{!OverviewFutureJobsAbort}</td>
                    </tr>
                </table>
                
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Refresh" action="{!UpdateOverviewFutureJobs}"/>
                    <apex:commandButton value="Abort async. jobs" action="{!AbortFutureJobs}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 1: Architecture" columns="1" id="step1">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep1}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Create architecture" action="{!StartControlDomainAutoRefsForObjects}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a new Salesforce-Object has been created,</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>- an API-Name of a Salesforce-Object has changed.</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 2a: Select objects" columns="3" id="step2a">
                <apex:commandButton value="Check all" action="{!OverviewSelectObjCheckAll}"/>
                <apex:commandButton value="Uncheck all" action="{!OverviewSelectObjUncheckAll}"/>
                <apex:pageBlockSectionItem />
                
                <apex:pageBlockSectionItem >
                    <apex:selectCheckboxes value="{!ListSelectObjColumn1}" layout="pageDirection">
                        <apex:selectOptions value="{!ListSelectObjOptionsColumn1}"/>
                    </apex:selectCheckboxes>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:selectCheckboxes value="{!ListSelectObjColumn2}" layout="pageDirection">
                        <apex:selectOptions value="{!ListSelectObjOptionsColumn2}"/>
                    </apex:selectCheckboxes>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:selectCheckboxes value="{!ListSelectObjColumn3}" layout="pageDirection">
                        <apex:selectOptions value="{!ListSelectObjOptionsColumn3}"/>
                    </apex:selectCheckboxes>
                </apex:pageBlockSectionItem>
                
                <apex:commandButton value="Check all" action="{!OverviewSelectObjCheckAll}"/>
                <apex:commandButton value="Uncheck all" action="{!OverviewSelectObjUncheckAll}"/>
                <apex:pageBlockSectionItem />
                
                <apex:pageBlockSectionItem />
                <apex:pageBlockSectionItem />
                <apex:pageBlockSectionItem />
                
                <apex:commandButton value="Apply changes" action="{!SaveSelectObj}" disabled="{!FutureJobsRunning}"/>
                <apex:commandButton value="Refresh list" action="{!UpdateOverviewSelectObj}"/>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 2b: Create domains for the objects" columns="1" id="step2b">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep2}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Read objects" action="{!StartControlDomainValuesForObjectsZ}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a new object is selected in step 2a.</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 3: Read the fields of selected objects (async. jobs might be created)" columns="1" id="step3">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep3}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Read fields" action="{!StartControlFieldsOfTheObjects}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a new field of an selected object has been created,</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>- an API-Name of a field has changed.</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 4: Create domains for the picklist-fields of selected objects (async. jobs might be created)" columns="1" id="step4">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep4}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Read picklist-fields" action="{!StartControlPicklistFields}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a new picklist-field of an selected object has been created,</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>- an API-Name of a picklist-field has changed,</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>- a new picklist-value has been created.</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 5: Create labels of the selected objects and fields (async. jobs might be created) (depends on user-language)" columns="1" id="step5">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep5}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Create labels" action="{!StartControlDescriptionsObjectAndFields}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a label of a selected object has changed (in the current language),</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>- a label of a field has changed (in the current language).</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>Current User-Language:</td>
                            <td>{!currentUserLanguage}</td>
                        </tr>
                        <tr>
                            <td>Last Languages:</td>
                            <td>{!strObjFieldDescriptionLanguages}</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 6: Create labels of the picklist-values (async. jobs might be created) (depends on user-language)" columns="1" id="step6">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep6}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Create picklist-labels" action="{!startControlDescriptionsPicklistFields}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- a label of a picklist-value has changed (in the current language).</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>Current User-Language:</td>
                            <td>{!currentUserLanguage}</td>
                        </tr>
                        <tr>
                            <td>Last Languages:</td>
                            <td>{!strPicklistDescriptionLanguages}</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Step 7: Refresh the activity-fields of DomainValues and DomainDescriptions" columns="1" id="step7">
                <apex:image url="{!URLFOR($Resource.state_images, '005.png')}" height="25" width="25" rendered="{!ShowCheckedImgStep7}" />
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Refresh activity-fields" action="{!startControlDomainActivity}" disabled="{!FutureJobsRunning}"/>
                    <table>
                        <tr>
                            <td>Has to be started, if:</td>
                            <td>- DomainDescriptions have changed.</td>
                        </tr>
                    </table>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Please Restart this method!" rendered="{!controlDomainActivityRestart}" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Change UserLanguage" columns="3" id="ChangeUserLanguage2">
                <apex:commandButton value="German" action="{!LangDE}" disabled="{!FutureJobsRunning}"/>
                <apex:commandButton value="US-English" action="{!LangENUS}" disabled="{!FutureJobsRunning}"/>
                <apex:commandButton value="Dutch" action="{!LangNLNL}" disabled="{!FutureJobsRunning}"/>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Clean up the architecture (only available for SystemAdmins)" columns="1" id="CleanArchitecture">
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Correct architecture" action="{!startControlAutoDomainSyntax}" disabled="{!FutureJobsRunning}"/>
                    If there is an error in one of the other jobs, one can try to correct the architecture of the "DomainAutoRefs" for solving the problem.
                    <br />
                    <b>Important:</b> After this method, one has to start all other jobs again.
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:commandButton value="Delete unused DomainValues" action="{!startControlAutoDomainSyntax}" disabled="{!FutureJobsRunning}"/>
                    This method deletes all DomainValues, which have no DomainDescriptions
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Please Restart the method 'Delete unused DomainValues'!" rendered="{!deleteDomValuesWithoutDesciptionsRestart}" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
    
    <script>
        jQuery('[id$=step2a]img').first().click();
        jQuery('[id$=CleanArchitecture]img').first().click();
    </script>
</apex:page>