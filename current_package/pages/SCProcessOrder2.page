<!--
 * @(#)SCProcessOrder.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCProcessOrderController" 
           tabStyle="SCOrder__c"
           showHeader="false"
           sidebar="false">

    <!-- ############# JS ################## -->
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-jquery-ui.css')}" />
    
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.selectboxes.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.formatCurrency-1.4.0.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/i18n/jquery.formatCurrency.all.js')}" />

    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/domainCache.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/processOrder.js')}" />
    
<script>
    // unset jQuerys $ variable, we have to 
    // use jQuery.method() instead or jQuery(...)
    var jq$ = jQuery.noConflict();
</script>

<style>
    .cp-tab
    {
        font-size: 12px; 
        font-weight: bold;
        line-height: 14px;
        width: 111px;
        height: 17px;
        padding: 8px 2px 4px 11px;
        border-width: 0 0 0 0;
        border-color: #000000;
        border-style: none;
        list-style-image: none;
        list-style-position: outside;
        list-style-type: disc;
        background-color: none;
        background-image: none;
    }
    
    .cp-active-tab {
        background: url("/img/alohaSkin/subtab_sprite.png") no-repeat scroll 0 0 transparent !important;
        cursor:pointer;
    }
    
    .cp-tab-active {
        background-position:left -99px !important;
        padding:8px 4px 4px 8px;
    }
    
    .cp-inactive-tab {
        background: url("/img/alohaSkin/subtab_sprite.png") no-repeat scroll 0 -199px transparent !important;
        /* width: 94px; */
        padding: 8px 4px 4px 8px;
        cursor: pointer;
    }
    
    .cp-tab-panel {
        /* background-color: #FF0000; */
    }
    
    .cp-tab-content {
        -moz-border-radius:4px 4px 4px 4px;
        background-color:#F8F8F8;
        border-color:#747E96 #D4DADC #D4DADC;
        border-right:1px solid #D4DADC;
        border-style:solid;
        border-width:3px 1px 1px;
        padding:6px;
    }
    
    .cp-tab-header {
        border: none;
    }

a.cp-link-button {
    text-decoration: none;
    margin: 1px 3px 1px 3px;
    text-align: center;
}

a.cp-link-button:hover {
    text-decoration: none;
    color: #333333;
    background-position: right -30px;
}

a.cp-link-button:visited {
    text-decoration: none;
    color: #333333;
}

/* overwritten SFDC styles */
.datePicker { z-index: 9999; }

    .rich-tabpanel-content {
        font-size: 100%;
    }
    
    .rich-tabpanel-content-position {
        /* min-height: 800px; */
    }
    
    .rich-tabhdr-cell-active .rich-tabhdr-side-cell {
        padding-right: 9px;
        border-style: none;
        background: url("/img/alohaSkin/subtab_sprite.png") no-repeat scroll right -100px transparent !important;
    }
    
    .rich-tabhdr-cell-inactive .rich-tabhdr-side-cell {
        padding-right: 4px;
        border-style: none;
        background: url("/img/alohaSkin/subtab_sprite.png") no-repeat scroll right -299px transparent !important;
    }
    
    .rich-tabhdr-side-border {
        background-image: none;
    }

</style>

<script>
    function setDocumentTitle(title)
    {
        if (title.length > 0)
        {
            document.title = title;
        }
    }
    
    function askForCancel()
    {
        if (confirm('{!$Label.SC_msg_AskForCancel}'))
        {
            window.opener.refreshCurrentItem();
            return window.close();
        }
    }
    
    setDocumentTitle('{!$Label.SC_app_InterventionTitle} [{!boOrder.order.Name}]');
</script>

<apex:form id="theform">
    <apex:actionFunction name="saveOrder" action="{!saveAssignment}" oncomplete="bookMat()" 
                         rerender="messageBlock"/>
    <apex:actionFunction name="bookMat" action="{!bookMat}" 
                         rerender="messageBlock,assignmentList,orderLinePanel,orderItemList,timeReportData,closeJob,jobCloseButtons" 
                         status="cp-action-status-full"/>
                         
    <apex:panelGroup rendered="{!orderInvalid}">
        <script>
            alert('{!$Label.SC_msg_OrderInvalid}');
            window.close();
        </script>
    </apex:panelGroup>
    
<!--
    <apex:panelGroup rendered="{!stockInvalid}">
        <script>
            alert("{!stockInvalidMsg}");
            window.close();
        </script>
    </apex:panelGroup>
-->

    <apex:panelGroup layout="block" style="margin: 5px 5px 5px 10px" id="assignmentList">
        <apex:selectList value="{!selectedAssignment}" 
            multiselect="false"
            size="1"
            tabindex="1"
            style="min-width: 500px;">
            <apex:selectOptions value="{!assignmentList}" />
            <apex:actionSupport event="onchange" 
                                action="{!initAssignment}" 
                                oncomplete="updateSums();" 
                                status="cp-action-status-full"
                                reRender="assignmentList,stockDetails,buttonToolbar,orderMatData,timeReportData,timeReport" />
        </apex:selectList>
        <apex:outputLabel value="{!$ObjectType.SCStock__c.label}" 
            for="stockDetails"  style="font-weight: bold; margin: 0px 5px 0px 150px;"/>
        <apex:outputText id="stockDetails" value="{!stockInfos}" />

        <!-- we can not do this in a separate standalone group with their own id, 
             because this group will not be rendered by calling finishProcess -->
        <apex:panelGroup rendered="{!finishedOk}">
            <script>
                window.opener.refreshCurrentItem();
                window.close();
            </script>
        </apex:panelGroup>
        
        <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(confirmMsg))}">
            <script>
                if (confirm('{!confirmMsg}'))
                {
                    saveOrder();
                }
            </script>
        </apex:panelGroup>
        
        <apex:panelGroup layout="none" rendered="{!bookMaterial}">
            <script>
                saveOrder();
            </script>
        </apex:panelGroup>
    </apex:panelGroup>

    <apex:tabPanel switchType="client" 
                   selectedTab="orderTab" 
                   id="theTabPanel"
                   styleClass="cp-tab-panel"
                   contentClass="cp-tab-content"
                   headerClass="cp-tab-header"
                   tabClass="cp-tab"
                   activeTabClass="cp-active-tab"
                   inactiveTabClass="cp-inactive-tab">


        <!-- ########### Order Tab ############### -->
        <apex:tab label="{!$Label.SC_app_OrderTab}" name="order" id="orderTab">
            <apex:actionRegion >
                <c:SCProcessOrderOrderTab pageController="{!this}"
                      key="{!orderComponentKey}"/>
            </apex:actionRegion>
        </apex:tab>

        <!-- ########### Order Items Tab ############## -->
        <apex:tab label="{!$Label.SC_app_ItemsTab}" name="items" id="itemsTab">
            <apex:actionRegion >
                <c:SCProcessOrderItemsTab2 pageController="{!this}"
                      key="{!orderComponentKey}"/>
            </apex:actionRegion>
        </apex:tab>

        <!-- ########### Parts and Services Tab ############# -->
        <apex:tab label="{!$Label.SC_app_PartsServicesTab}" name="parts" id="partsTab">
<!--            <apex:actionRegion > -->
                <c:SCProcessOrderPartsTab pageController="{!this}"
                    key="{!orderComponentKey}" />
<!--            </apex:actionRegion> -->
        </apex:tab>
    </apex:tabPanel>
    

    <!-- ######################### Close Job ############################ -->
    
    <apex:panelGroup layout="none" id="closeJob">
        <apex:pageBlock >
            <apex:panelGroup layout="none" id="messageBlock">
                <apex:pagemessages />
            </apex:panelGroup>
            
            <apex:pageBlockSection title="{!$Label.SC_app_CloseJob}" columns="2">
                <apex:inputField value="{!curAssignment.appointment.Assignment__r.Followup1__c}" />
                <apex:inputField value="{!boOrder.order.InvoicingReleased__c}" />
                <apex:inputField value="{!curAssignment.appointment.Assignment__r.Followup2__c}" />
                <apex:outputText />
                <apex:pageBlockSectionItem >
                    <apex:panelGroup layout="none" id="jobCloseButtons">
                        <apex:commandButton value="{!$Label.SC_btn_FinishProcessing} [f]" 
                                            action="{!finishProcessing}"
                                            rerender="messageBlock,assignmentList,orderLinePanel,orderItemList,timeReportData" 
                                            status="cp-action-status-full"
                                            accesskey="f" 
                                            disabled="false" />
                        <apex:commandButton value="{!$Label.SC_btn_Save} [s]" 
                                            action="{!saveAssignment}"
                                            accesskey="s" 
                                            oncomplete="window.opener.refreshCurrentItem();"
                                            rerender="messageBlock,assignmentList,orderLinePanel,orderItemList,timeReportData"
                                            status="cp-action-status-full"
                                            disabled="false" />
                        <apex:commandButton value="{!$Label.SC_btn_Cancel}" 
                                            onclick="return askForCancel()" 
                                            rerender="assignmentList"
                                            immediate="true"/>
                    </apex:panelGroup>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:panelGroup>
</apex:form>
</apex:page>