<!--
/*
 * @(#)SCSendMessage.page
 * 
 * Copyright 2011 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.   
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This page generates the message form.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
 */
 -->
<apex:page controller="SCSendMessageController" showHeader="false" sidebar="false">

<apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />

<style>
    body {
        margin: 10px;
    }
</style>

<script>
    
    var orderName = '';
    
    // Self-close function
    //window.onblur=function(){self.close();};
    
    jQuery(document).ready(function() 
    {   
        // Changes the color of text limit label depending on letter amount
        // and shows the amoount of letters in the textarea
        jQuery('[id$=smsMessage]').keyup(function() {
            jQuery('#txtAreaCounter').html(jQuery('[id$=smsMessage]').val().length);
            if(jQuery('[id$=smsMessage]').val().length >= 100 && jQuery('[id$=smsMessage]').val().length < 160 ){
                jQuery('[id$=txtAreaCounter]').css("color","#CD6702");        
            }
            if(jQuery('[id$=smsMessage]').val().length >= 160){
                jQuery('[id$=txtAreaCounter]').css("color","#FF0000");
            }
            if(jQuery('[id$=smsMessage]').val().length > 160){
                txtMessage = jQuery('[id$=smsMessage]').val(); 
                txtMessage = txtMessage.substring(0, 160);
                jQuery('[id$=smsMessage]').val(txtMessage);
            }
            if(jQuery('[id$=smsMessage]').val().length < 100){
                jQuery('[id$=txtAreaCounter]').css("color","#3C8204");        
            }
        });
        
        // Set the order name to the textarea
        jQuery('[id$=smsMessage]').text(orderName).keyup();
        
    });
    
    // Confirmation of sending the message
    function confirmMessage()
    {
        if ("{!$Setup.SCApplicationSettings__c.MESSAGE_EXISTS_GATEWAY__c}" == 1)
        {
            var isSuccess = confirm("{!$Label.SC_msg_MessageSent}");
            if (isSuccess) window.close(); return true;
            return false;
        }
        else
        {
            var isSuccess = confirm("{!$Label.SC_msg_MessageSentButMissingGateway}");
            if (isSuccess) window.close(); return true;
            return false;
        }
    }
    
    // Confirmation of error sending the message
    function notSent()
    {
        alert('{!$Label.SC_msg_MessageNotSent}');
    }
</script>


<!-- Select Engineer (Lookup) -->
<apex:form id="engineerDialog" onkeyup="javascript:return false;">

    <apex:pageBlock title="{!$Label.SC_app_SendMessage}" >

        <apex:pageBlockButtons >
            <apex:commandButton action="{!onSendMessage}" 
                                value="{!$Label.SC_app_SendMessage}" 
                                disabled="{!showNoEngineerError}" 
                                oncomplete="{!IF(messageSent = true, 'confirmMessage()', 'notSent()')}"/>
            <apex:commandButton value="{!$Label.SC_btn_Close}" onclick="window.close();"/>            
        </apex:pageBlockButtons>
        
        <apex:pageMessage rendered="{!showNoEngineerError}" strength="1" severity="warning" summary="{!$Label.SC_msg_NoEngineer}" />

        <script>
            orderName = '{!order.Name}';
        </script>

        <apex:pageBlockSection columns="1" >
        
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCOrder__c.label}"/>
                <apex:outputField value="{!order.Name}"/>
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$Label.SC_app_Engineer}"/>
                <apex:outputField value="{!resource.Employee__c}"/>
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCResource__c.fields.Mobile_txt__c.label}"/>
                <apex:outputText value="{!resource.Mobile_txt__c}"/>
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCResource__c.fields.EMail_txt__c.label}"/>
                <apex:outputText value="{!resource.EMail_txt__c}"/>
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="{!$ObjectType.SCMessage__c.label}"/>
                <apex:inputTextarea id="smsMessage" cols="3" rows="3" style="width:100%" richText="false" value="{!messageText}" disabled="{!showNoEngineerError}"/>
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputLabel value="Characters used"/>
                <apex:outputText >
                    <span id="txtAreaCounter" style="color:#3C8204;">0</span> of 160 characters
                </apex:outputText>
            </apex:pageBlockSectionItem>
            
        </apex:pageBlockSection>
    
    </apex:pageBlock>

</apex:form>


</apex:page>