<apex:page standardController="SCStockOptimizationProposal__c" extensions="SCStockOptPropItemApproveController">
          
    <style>
        #spacer {
            margin-top: 20px;
            display:block;
        }
        .dataCell, dataCell:hover {
            height: 34px;
            background-color:#fff;
        }
        .dataRow, .dataRow:hover {
            background-color:#fff;
        }
        .editCell {
            height:30px;
            font-weight:bold;
        }
        .editCell:hover {
            background: url(/img/func_icons/util/pencil12.gif) no-repeat right;
            cursor:pointer;
        }
        .inlineEditDiv input {
            width:70px;
            font-size: 12px;
        }
        .helpPopup {
            width:220px;
            height:180px;
            display:none;
            border-radius: 4px;
            text-align: center;
            background-color: white;
            padding-top: 10px;
            margin: -19px 0 0 325px;
            position: absolute;

            -moz-box-sizing: border-box;
            -moz-transition: left 0.5s linear 0s;
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.5);
            -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')";
        }
        .helpPopupOff {
            display:block;
        }
        .closeIcon {
            display:block;
            width:13px;
            height:13px;
            position:absolute;
            margin:-3px 0 0 8px;
        }
        .warningCell {
            background-color: #FFFE9A;
        }
        .highlight, .highlight:active, .highlight:hover {
            background:none;
            background-color: #fff;
        }
    </style>
          
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.7.1.min.js')}" />
    
    <script>
        var zeros = new Array();
    
       /** 
        * Returns the accessible element ID
        *
        * @param myid   the id of the visualforce object
        */
        function esc(myid) {
           return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
        }
    
        function showHelpPopup()
        {
            jQuery('.helpPopup').toggleClass('helpPopupOff');
        }
        
        function assToZeroArray(obj)
        {
            zeros.push();
        }
        
        function showHideZeroItems()
        {
            for(var i = 0; i < zeros.length; i++)
            {
                jQuery(esc(zeros[i])).parent().toggle();
            }
        }
    </script>
    
    <div id="spacer"></div>
    
    <apex:pageMessages id="errors" />

    <apex:form >
    
        <!-- Trick: mode="inlineEdit" makes some cells to supporn inline editing -->
        <apex:pageBlock title="" >
        
            <apex:pageBlockButtons location="top">
                <apex:commandButton action="{!save}" id="saveButton" value="{!$Label.SC_btn_ApproveAndActivateInStockProfile}"/>
                <apex:commandButton action="{!onCancel}" id="cancelButton" value="{!$Label.SC_btn_Cancel}"/>
                <apex:commandButton id="helpButton" value="?" style="margin-right:20px;width:32px;" reRender="none" onclick="showHelpPopup()"/>
                
                <apex:outputPanel style="font-size:12px;float:right;">
                    Filter: &nbsp;
                    <select onchange="showHideZeroItems()">
                        <option>All</option>
                        <option>Delta is not 0</option>
                    </select>
                </apex:outputPanel>
                
                <apex:outputPanel styleClass="helpPopup">
                    <apex:image url="/img/search_dismiss.gif" height="13" width="13" onclick="showHelpPopup()" styleClass="closeIcon" />
                    <apex:image url="{!URLFOR($Resource.scartwork, 'artwork/help_approve.gif')}" alt="Help" title="Help" />
                </apex:outputPanel>
                
            </apex:pageBlockButtons>
                    
            <apex:outputText value="{!obj['StockOptimizationProposalItems__r']}" rendered="false" />          
            
            <apex:pageBlockTable value="{!obj.StockOptimizationProposalItems__r}" var="item">
                
                <!-- Name -->
                <apex:column headerValue="Item">
                    <apex:outputLink value="/{!item.id}" target="_blank">{!item.Name}</apex:outputLink>
                </apex:column>
                
                <!-- Article -->
                <apex:column headerValue="{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}">
                    <apex:outputLink value="/{!item.Article__c}" target="_blank">{!item.Article__r.Name}</apex:outputLink>
                </apex:column>
                
                <!-- Article -->
                <apex:column value="{!item.Article__r.ArticleNameCalc__c}"/>
                
                <!-- Profile Current Quantity -->
                <apex:column value="{!item.StockItemMinQuantity__c}"/>
                
                <!-- Counted Quantity -->
                <apex:column value="{!item.CountedQuantity__c}"/>
                
                <!-- Proposal Quantity -->
                <apex:column value="{!item.ProposalQuantity__c}"/>
                
                <!-- Delta -->
                <apex:column value="{!item.Delta__c}"/>
                
                <!-- Approved Quantity -->
                <apex:column headerValue="{!$ObjectType.SCStockOptimizationProposalItem__c.fields.ApprovedQuantity__c.label}" 
                             styleClass="editCell {!IF(item.ApprovedQuantity__c = 0, 'warningCell','')}"
                             id="approvedCell">
                    <apex:outputField value="{!item.ApprovedQuantity__c}">
                        <apex:inlineEditSupport showOnEdit="saveButton, cancelButton" 
                                                hideOnEdit="editButton" 
                                                event="onclick" 
                                                changedStyleClass="myBoldClass" 
                                                resetFunction="resetInlineEdit"/>
                    </apex:outputField>
                    
                    <script>
                        var tmpVar = {!item.Delta__c};
                        var tid = '{!$Component.approvedCell}';
                        
                        if(tmpVar == 0)
                        {
                            zeros.push(tid);
                        }
                    </script>   
                    
                 </apex:column>
                
                <!-- Stock Item -->
                <apex:column headerValue="{!$ObjectType.SCStockItem__c.fields.Name.label}">
                    <apex:outputLink value="/{!item.StockItem__c}" target="_blank">{!item.StockItem__r.Name}</apex:outputLink>
                </apex:column>

                <!-- Last Modified By -->
                <apex:column value="{!item.LastModifiedById}"/>

                <!-- Last Modified Date -->
                <apex:column value="{!item.LastModifiedDate}"/>             
            
            </apex:pageBlockTable>
                        
        </apex:pageBlock>
    </apex:form>
    
    <script>
        jQuery(document).ready(function(){
            
        });
    </script>

</apex:page>