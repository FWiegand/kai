<!--
 * @(#)SCStockItemCopy.page
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page standardController="SCStock__c" 
    extensions="SCStockItemCopyExtension" 
    showHeader="false"
    title="{!$Label.SC_app_ItemCopyTitle}">

    <!-- ############# JS ################## -->
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-global.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/statusProcessing.js')}" />


    <apex:pageBlock >
        <apex:sectionHeader title="{!$Label.SC_app_ItemCopyTitle}" id="header" />

        <apex:outputText id="info1" escape="false"
            style="border: #CCCCCC 1px solid; height: 45px; margin: 5px 0px 5px 0px; padding: 2px; display:block"
            value="{!$Label.SC_app_ItemCopyInfo}" />
    </apex:pageBlock>

    <apex:form id="mainForm">
        <apex:panelGroup rendered="{!copyOk}">
            <script>
                opener.location.href="/{!SCStock__c.Id}";
                window.close();
            </script>
        </apex:panelGroup>
        
        <apex:pageBlock >
            <apex:pageBlockButtons location="bottom" >
                <apex:commandButton action="{!onCopyItems}" value="{!$Label.SC_btn_Ok}" reRender="mainForm" status="copyProcessing"/>
                <apex:commandButton action="javascript:window.close()" value="{!$Label.SC_btn_Cancel}" immediate="true"/>

                <apex:actionStatus id="copyProcessing" 
                                   startText="{!$Label.SC_app_Processing}" 
                                   stopText="" 
                                   onstart="onStartActionStatus();"
                                   onstop="onStopActionStatus()" 
                                   startStyleClass="processing" />
            </apex:pageBlockButtons>

            <apex:pageMessages />

            <apex:pageBlockSection columns="1">
                <apex:outputField value="{!SCStock__c.Name}" />
                <apex:outputField value="{!SCStock__c.Info__c}" />

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_Source}" />
                    <apex:selectList value="{!sourceStock}" size="1" multiselect="false">
                        <apex:selectOptions value="{!sourceList}"/>
                    </apex:selectList>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ItemCopyDelItems}" />
                    <apex:inputCheckbox value="{!deleteItems}"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ItemCopyMinQty}" />
                    <apex:inputCheckbox value="{!copyMinQty}"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ItemCopyMaxQty}" />
                    <apex:inputCheckbox value="{!copyMaxQty}"/>
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.SC_app_ItemCopyReplQty}" />
                    <apex:inputCheckbox value="{!copyReplQty}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>