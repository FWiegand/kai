<apex:page controller="SCQuickCustomerSearchController" sidebar="false" showHeader="false">

    <apex:stylesheet value="{!URLFOR($Resource.scjquery,'jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.6.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    
    <style>
        body {
            background-color: #CFEEF8;
        }
        #theResults {
            display:none;
            padding: 0 5px;
            position: absolute;
            background: none repeat scroll 0 0 #FFFFFF;
            border: 1px solid #D7D7D7;
            overflow:auto;
            max-height: 66px;
        }
        .activeModel, .activeModel a {
            color: #fff;
            background: none repeat scroll 0 0 #68A7BF;
        }
        #theResults ul {
            list-style: none outside none;
            margin:0;
            padding:0;
        }
        #theResults li {
            margin: 0;
            padding: 1px;
            cursor: pointer;
            height:20px;
        }

        .resultSelected {
            color: #fff;
            background: none repeat scroll 0 0 #78BEDE;        
        }
        .tableCell {
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            vertical-align: text-bottom;
            padding: 2px 0 0 0;
        }
        .searching
        {
            background:url(/img/loading.gif) no-repeat 0 0; 
            padding-left:20px; 
            margin-left:10px; 
            padding-bottom:5px; 
            font-size:12px;
        }
        .searchingInput
        {
            background:url(/img/loading.gif) no-repeat right #fff; 
        }
        .personAccountStyle {
            background: url(/img/icon/perAccounts16.gif) no-repeat  0px 2px;
        }
    </style>
    
    <script>
        
        // Variables for the product model autocompleter search
        var availableProducts = [];   
        var inputFieldId;  
        var productModelIdFieldId;
        var productModelNameFieldId;
        var productModelResultsId;
        var t;
        
       /** 
        * Returns the accessible element ID
        *
        * @param myid   the id of the visualforce object
        */
        function esc(myid) {
           return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
        }
        
       /** 
        * Selects a product model from the proposal list 
        * and fills the data
        *
        * @param id    the id of the product model
        * @param model the name of the product model
        */
        function selectProductModel(id,model,obj)
        {
            /*
            jQuery(esc(inputFieldId)).val(model);
            jQuery(esc(productModelIdFieldId)).text(id);
            jQuery(esc(productModelNameFieldId)).text(model);
            */
            jQuery('#theResults li').removeClass('resultSelected');
            jQuery(obj).addClass('resultSelected');
            
            
            sList = window.open("/" + id);
            //sList.moveTo(((screen.width-620)/2),((screen.height-390)/2));
            sList.focus();
            
        }
        
       /** 
        * Sets the position of the proposal list depending
        * on the position of input field
        */
        function setPositionProductResult()
        {
            // Setting position
            var position = jQuery(esc(inputFieldId)).position();
            jQuery('#theResults').css('left',position.left);
        }
           
    </script>
    <apex:form >

        <apex:actionFunction name="startSearchingProducts" action="{!getSearchProducts}" reRender="productModels" status="statusStartSearchingCustomer"/>
        
        <apex:actionFunction name="selAcc" action="{!reloadDetails}" reRender="details">
            <apex:param name="P1" assignTo="{!selectedAccount}" value=""/>
        </apex:actionFunction>
    
        <span style="font-size:14px;display:{!IF(ISBLANK($CurrentPage.parameters.fullLength), 'none', 'block')};">{!$ObjectType.Account.label}:&nbsp;</span>
        <apex:inputText value="{!searchString}" id="inputString" style="width:{!IF(ISBLANK($CurrentPage.parameters.fullLength), '94%', '540px')}; height: 22px; padding: 0 0 0 6px;"/>
        
        <apex:actionStatus id="statusStartSearchingCustomer" 
                           startText="" 
                           stopText="" 
                           startStyleClass=""
                           onstart="jQuery('[id$=inputString]').addClass('searchingInput');"
                           onstop="jQuery('[id$=inputString]').removeClass('searchingInput');"/>
        
        <apex:outputText id="productModelId" />
        <apex:outputText id="productModelName" />
        
        <div id="theResults">                            
        <apex:outputPanel id="productModels" layout="block" styleclass="productResults">
            <ul>
            <apex:repeat value="{!customers}" var="p">
                <li onclick="selectProductModel('{!p.id}','{!p.name}',this);">
                    <span class="tableCell" style="width:150px;">{!p.name}</span>
                    <span class="tableCell" style="width:120px; {!IF(ISBLANK($CurrentPage.parameters.fullLength), 'display:none;', '')}">{!p.ShippingStreet__c} {!p.ShippingHouseNo__c}</span>
                    <span class="tableCell" style="width:110px; {!IF(ISBLANK($CurrentPage.parameters.fullLength), 'display:none;', '')}">{!p.ShippingCity__c}</span>
                    <span class="tableCell" style="width:20px;  {!IF(ISBLANK($CurrentPage.parameters.fullLength), 'display:none;', '')}">{!p.ShippingCountry__c}</span>
                    <span class="tableCell" style="width:80px;padding-left:5px; {!IF(ISBLANK($CurrentPage.parameters.fullLength), 'display:none;', '')}"><a class="createOrder" href="/apex/SCOrderCreationDE?aid={!p.id}" target="_blank">Service Order</a></span>
                </li>
            </apex:repeat>
            </ul>
            
            <apex:outputPanel rendered="{!IF(customers.size > 0,true,false)}">
                <script>
                    // Showin the result list layer if something found
                    if(jQuery('#theResults').css('display') != 'block')
                    {
                        jQuery('#theResults').css('display','block');
                    }
                </script>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!IF(customers.size = 0,true,false)}">
                <script>
                    // Hiding the result list layer if nothing found
                    jQuery('#theResults').css('display','none');
                </script>
            </apex:outputPanel>
            
        </apex:outputPanel>
        </div>
        
        <script>
            // Setting the variables (ids of visualforce page objects)
            inputFieldId = '{!$Component.inputString}';
            productModelIdFieldId = '{!$Component.productModelId}';
            productModelNameFieldId = '{!$Component.productModelName}';
            productModelResultsId = '{!$Component.productModels}';
        </script>
        
        
        <br/><br/>
        
        <apex:outputPanel id="details">
            <apex:detail subject="{!selectedAccount}" relatedList="false" title="false"/> 
        </apex:outputPanel>
    
    </apex:form>
    
    
    <script>
           
        jQuery(document).ready(function() 
        {
            setPositionProductResult();
        
            // Turning the autocompletion off for the input field
            jQuery(esc(inputFieldId)).attr('autocomplete','off');
            
            // Setting the result list width = input field width
            jQuery('#theResults').width(jQuery(esc(inputFieldId)).width() - 4);
            jQuery('#theResults').css('display','none');
            
            
            // Listening for key press on the input field
            jQuery(esc(inputFieldId)).live('keyup', function(event) {
                // Other keys
                if (event.keyCode != 40 && event.keyCode != 38 && event.keyCode != 13) {
                    clearTimeout(t);
                    t = setTimeout("startSearchingProducts()", 1000);
                }
            });
            
            // Listening for key press on the input field (enter button only)
            jQuery(esc(inputFieldId)).live('keydown', function(event) {
                // Enter Key
                if (event.keyCode == 13) {
                    event.preventDefault();
                    jQuery(esc(inputFieldId)).blur();
                    jQuery('.activeModel').click();
                    jQuery(esc(inputFieldId)).focus();
                }
                // Up Key
                if (event.keyCode == 38) {
                    event.preventDefault();
                    var $toHighlight = jQuery('.activeModel').prev().length > 0 ? jQuery('.activeModel').prev() : jQuery('#theResults li').last();
                    jQuery('.activeModel').removeClass('activeModel');
                    $toHighlight.addClass('activeModel');
                    
                    var positionLi = $toHighlight.position();
                    if((positionLi.top-jQuery('#theResults').height()*(-1)) < jQuery('#theResults').height())
                    {   
                        jQuery('#theResults').scrollTop(jQuery('#theResults').scrollTop() - (jQuery('#theResults').height() - (positionLi.top-jQuery('#theResults').height()*(-1))));
                    }
                    if((positionLi.top-jQuery('#theResults').height()*(-1)) > jQuery('.productResults').height())
                    {   
                        jQuery('#theResults').scrollTop(jQuery('.productResults').height() - jQuery('.activeModel').height());
                    }
                }
                // Down Key
                if (event.keyCode == 40) {
                    event.preventDefault();
                    if(jQuery('#theResults').css('display') != 'block')
                    {
                        jQuery('#theResults').css('display','block');
                    }
                    
                    var toHighlight = jQuery('.activeModel').next().length > 0 ? jQuery('.activeModel').next() : jQuery('#theResults li').first();
                    
                    jQuery('.activeModel').removeClass('activeModel');
                    toHighlight.addClass('activeModel');
                    
                    // Scrolling to the current selected item
                    var positionLi = toHighlight.position();                
                    if((positionLi.top-jQuery('#theResults').height()) > 0)
                    {   
                        jQuery('#theResults').scrollTop(jQuery('#theResults').scrollTop() + (positionLi.top-jQuery('#theResults').height()) + jQuery('.activeModel').height());
                    }
                    if(((positionLi.top-jQuery('#theResults').height()) + jQuery('.activeModel').height()) <= -(jQuery('.productResults').height() - jQuery('.activeModel').height() - 2))
                    {
                        jQuery('#theResults').scrollTop(0);
                    }
                }
            }); 
            
            jQuery('#theResults').scroll(function(){
                //jQuery("#pos").text(jQuery('#theResults').scrollTop());
            });
            
            
            // Mouse actions on the results
            jQuery('#theResults li').live('mouseover', function() {
                jQuery('#theResults li').removeClass("activeModel");
                jQuery(this).addClass("activeModel");
            });
            jQuery('#theResults li').live('mouseout', function() {
                jQuery(this).removeClass("activeModel");
            });
            
            
            // Listening for the click on the input field
            jQuery(esc(inputFieldId)).click(function(){
                jQuery('#theResults').css('display','block');
            });
            
            // Page-body click listener
            jQuery('body').click(function(event) {
                // Close the results layer
                var target = jQuery(event.target);
                if(!target.is('.productResults') && event.target.nodeName != 'INPUT')
                {
                    if(jQuery('#theResults').css('display') == 'block')
                    {
                        jQuery('#theResults').css('display','none');
                    }
                }
            });
            
            jQuery('.createOrder').click(function(event){
                event.stopPropagation();
            }); 
            
        });
        
        
    </script>

</apex:page>