<!--
 * @(#)SCOrderRecall.page
 * 
 * Copyright 2012 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
 */
-->
<apex:page standardController="SCOrder__c" extensions="SCOrderRecallExtension" >
 

<apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-global.css')}" />

<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.7.1.min.js')}" />
<apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />
<apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/statusProcessing.js')}" />

<script type="text/javascript">

/**
 * DOMREADY event processing
 */
jQuery(document).ready(function()
{
    chgRecallReason();
});

function chgRecallReason()
{
    var reasonVal = jQuery('[id$=recallReason]').val();
    if ('other' == reasonVal)
    {
        jQuery('[id$=recallDescr]').show();
    }
    else
    {
        jQuery('[id$=recallDescr]').hide();
    }
}
</script>


<apex:form id="dlgForm">
    <!-- Order summary -->
    <apex:pageBlock title="{!$ObjectType.SCOrder__c.label}" mode="edit" id="dlgOrder">
        <apex:pageBlockButtons location="top">
            <apex:commandButton value="{!$Label.SC_btn_Cancel}" onClick="window.location.href = '/{!boOrder.order.id}'; return false;"/>
            <!-- almost hidden input field that gets the input focus - prevent the auto drop down in the desired start field -->        
            <apex:inputText value="{!empty}" style="width:0px;height:0px"/>
        </apex:pageBlockButtons>
        <apex:pageBlockSection >
            <apex:pageBlock >
                <apex:pageBlockSection columns="1" id="dlgOverview">
                    <apex:outputField value="{!boOrder.order.Name}"/>
                    <apex:outputField value="{!boOrder.order.Status__c}"/>
                    <apex:outputField value="{!boOrder.order.Type__c}"/>
                    
                    <apex:outputField value="{!boOrder.order.DepartmentCurrent__r.name}"/>
                    <apex:outputField value="{!boOrder.order.CustomerPriority__c}"/>

                    <apex:outputField value="{!boOrder.order.CustomerTimewindow__c}" />
                    <apex:outputField value="{!boOrder.order.CustomerPrefStart__c}"/>
                    <apex:outputField value="{!boOrder.order.CustomerPrefEnd__c}" />
                    <apex:outputField value="{!boOrder.order.Duration__c}"/>
                    <apex:outputField value="{!boOrder.order.Standby__c}"/>
                </apex:pageBlockSection>
            </apex:pageBlock>

            <apex:pageBlock >
                <apex:pageBlock title="{!$ObjectType.SCInstalledBase__c.label}">
                    <apex:pageBlockSection columns="1" rendered="{!NOT(ISNULL(boOrderItem))}">
                    <apex:outputfield value="{!boOrderItem.orderitem.InstalledBase__r.ProductGroup__c}"/>
                    <apex:outputfield value="{!boOrderItem.orderitem.InstalledBase__r.ProductPower__c}"/>
                    <apex:outputfield value="{!boOrderItem.orderitem.InstalledBase__r.ProductEnergy__c}"/>
                    </apex:pageBlockSection>
                </apex:pageBlock>
                
                <c:SCInfoBlock title="{!$ObjectType.SCInstalledBaseLocation__c.label}" text="{!LocationAddress}"/>
            </apex:pageBlock>

            <apex:pageBlock title="{!$Label.SC_app_LastEmployee}" rendered="{!NOT(ISNULL(lastTimeReport))}">
                <apex:pageBlockSection columns="1">
                <apex:outputfield value="{!lastTimeReport.Employee__c}"/>
                <apex:outputfield value="{!lastTimeReport.End__c}"/>
                
                </apex:pageBlockSection>
            </apex:pageBlock>
        </apex:pageBlockSection>

        <apex:pageBlockSection columns="1" rendered="{!IsCustomerLocked}">
            <apex:pageMessage severity="warning" strength="1" escape="false" title="{!$Label.SC_msg_AccLockTitle}"> 
                {!$Label.SC_msg_AccLockInfo}: 
                <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(roleRE.Account__r.LockType__c))}">
                    <b>{!$ObjectType.Account.fields.LockType__c.label}:</b> 
                    [<apex:outputField value="{!roleRE.Account__r.LockType__c}"/>] 
                </apex:panelGroup>
                <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(roleRE.Account__r.RiskClass__c))}">
                    <b>{!$ObjectType.Account.fields.RiskClass__c.label}:</b> 
                    [<apex:outputField value="{!roleRE.Account__r.RiskClass__c}"/>]
                </apex:panelGroup>
            </apex:pageMessage>
        </apex:pageBlockSection>

        <!-- Validation: can the recall be created -->
        <apex:pageBlockSection columns="1" rendered="{!NOT(canRecall)}" id="dlgWarning1">
            <apex:pageMessage severity="warning" strength="2" title="{!$Label.SC_msg_RecallWrongStatus}" escape="false"/> 
        </apex:pageBlockSection>
    
        <!-- Validation: is the recall valid -->
        <apex:pageBlockSection columns="1" rendered="{!recallNotValid}" id="dlgWarning2">
            <apex:pageMessage severity="warning" strength="2" title="{!$Label.SC_msg_RecallNotValid}" escape="false"/> 
        </apex:pageBlockSection>
    </apex:pageBlock>
    
    <!-- Recall section -->
    <apex:pageBlock title="{!$Label.SC_app_Recall}" id="dlgRecall" rendered="{!AND(canRecall, NOT(recallNotValid), NOT(IsCustomerLocked))}">
        <apex:pageBlockButtons location="bottom" id="buttons">
            <apex:commandButton id="buttonSave" value="{!$Label.SC_btn_Save}" 
                title="{!$Label.SC_btn_Save}" 
                action="{!onSave}" status="recallProcessing" reRender="dlgRecall"/>
            <apex:commandButton id="buttonCancel" value="{!$Label.SC_btn_Cancel}" 
                title="{!$Label.SC_btn_Cancel}" 
                immediate="true" 
                onClick="window.location.href = '/{!boOrder.order.id}'; return false;"/>

            <apex:actionStatus id="recallProcessing" 
                               startText="{!$Label.SC_app_Processing}" 
                               stopText="" 
                               onstart="onStartActionStatus();"
                               onstop="onStopActionStatus();" 
                               startStyleClass="processing" />
        </apex:pageBlockButtons>

        <apex:panelGroup layout="none" rendered="{!NOT(ISNULL(errMsg))}">
            <apex:pageMessage severity="error" strength="2" title="{!errMsg}" escape="false"/> 

            <script type="text/javascript">
                chgRecallReason();
            </script>
        </apex:panelGroup>
        
        <apex:pageBlockSection columns="1">
            <apex:inputField value="{!newOrder.RecallReason__c}" id="recallReason" onchange="chgRecallReason()"/>
        </apex:pageBlockSection>
        <apex:pageBlockSection columns="1" id="recallDescr">
            <apex:inputField value="{!newOrder.RecallReasonDescription__c}" style="width: 600px;"/>
        </apex:pageBlockSection>
    </apex:pageBlock>

</apex:form>

</apex:page>