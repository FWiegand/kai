<!--
 * @(#)SCOrderMatStatus.page
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCOrderMaterialController" 
    showHeader="false"
    title="{!$Label.SC_app_MatStatus}" 
    tabStyle="SCStock__c">
    
    <apex:form >
        <apex:pageBlock >
            <apex:sectionHeader title="{!$Label.SC_app_MatStatus}" id="header" />
            
            <apex:pageBlockButtons location="bottom" >
                <apex:commandButton action="javascript:window.close()" value="{!$Label.SC_btn_Ok}" immediate="true"/>
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1" >
                <apex:pageBlockTable value="{!matMoves}" var="item">
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Status__c.label}</apex:facet>
                        <apex:outputField value="{!item.Status__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                        <apex:outputField value="{!item.Article__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}</apex:facet>
                        <apex:outputText style="white-space: nowrap;" value="{!item.Article__r.ArticleNameCalc__c}" />
                    </apex:column>
                    <apex:column rendered="{!valuationTypeActivated}">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ValuationType__c.label}</apex:facet>
                        <apex:outputField value="{!item.ValuationType__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Type__c.label}</apex:facet>
                        <apex:outputField value="{!item.Type__c}" />
                    </apex:column>
                    <apex:column styleClass="r_align">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Qty__c.label}</apex:facet>
                        <apex:outputText value="{0, number, integer}" >
                            <apex:param value="{!item.Qty__c}" />
                        </apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.RequisitionDate__c.label}</apex:facet>
                        <apex:outputField style="white-space: nowrap;" value="{!item.RequisitionDate__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Replenishment__c.label}</apex:facet>
                        <apex:outputField value="{!item.Replenishment__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Stock__c.label}</apex:facet>
                        <apex:outputField value="{!item.Plant__c}" />/
                        <apex:outputField value="{!item.Stock__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.DeliveryStock__c.label}</apex:facet>
                        <apex:outputField value="{!item.DeliveryPlant__c}" />/
                        <apex:outputField value="{!item.DeliveryStock__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.CreatedDate.label}</apex:facet>
                        <apex:outputField style="white-space: nowrap;" value="{!item.CreatedDate}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.User.fields.Alias.label}</apex:facet>
                        <apex:outputField style="white-space: nowrap;" value="{!item.CreatedBy.Alias}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.Order__c.label}</apex:facet>
                        <apex:outputField value="{!item.Order__r.Name}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ERPResultDate__c.label}</apex:facet>
                        <apex:outputField value="{!item.ERPResultDate__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ERPStatus__c.label}</apex:facet>
                        <apex:outputField value="{!item.ERPStatus__c}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ERPResultInfo__c.label}</apex:facet>
                        <apex:outputField value="{!item.ERPResultInfo__c}" />
                    </apex:column>
                    <apex:column rendered="{!ShowPricing}">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.ListPrice__c.label}</apex:facet>
                        <apex:outputField value="{!item.ListPrice__c}" />
                    </apex:column>
                    <apex:column rendered="{!ShowPricing}">
                        <apex:facet name="header">{!$ObjectType.SCMaterialMovement__c.fields.PrimeCost__c.label}</apex:facet>
                        <apex:outputField value="{!item.PrimeCost__c}" />
                    </apex:column>
                </apex:pageBlockTable> 
            </apex:pageBlockSection>   
        </apex:pageBlock>
    </apex:form>
</apex:page>