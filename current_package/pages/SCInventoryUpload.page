<!--
 * @(#)SCOrderExternalAssignment.page
 *  
 * Copyright 2012 by GMS Development GmbH
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This page adds a new order extern assignment and their items to the order.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCInventoryUploadController">

<apex:sectionHeader title="{!$ObjectType.SCInventory__c.label}" subTitle="Inventurupload {!currentInventory.Plant__c}/{!currentInventory.Stock__r.name}" />

<apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.6.2.min.js')}" />

<style>
.searching
{
    background:url(/img/loading.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
    height:16px;
}
.warningClass {
    background: url({!URLFOR($Resource.SCRes,'images/warning14.png')}) no-repeat right center #FFF4C8;
    background-position: 50% 50%;
}
</style>

<script>
/** 
* Returns the accessible element ID
*
* @param myid   the id of the visualforce object
*/
function esc(myid) {
   return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
}

/**
 * Process cancelation confirmation
 */
function confirmCancel() {
    var isCancel = confirm("{!$Label.SC_msg_ConfirmCancel}");
    if (isCancel)
    {
        window.location = '/{!$CurrentPage.parameters.oid}';
    }
    return false;
}

function reload() {
    window.location = '/apex/SCInventoryUpload?oid={!$CurrentPage.parameters.oid}';
}


/**
 * The DOM ready function
 */
jQuery(document).ready(function()
{

});
</script>

<apex:form >

    <apex:pageBlock id="mainBlock">
    
        <apex:pageMessages />
        
        <apex:pageMessage id="errors" strength="2" severity="warning" summary="{!errorMessage}" rendered="{!showError}"/>
        
        <apex:pageBlockButtons >
        
            <apex:commandButton value="{!$Label.SC_btn_Next}" 
                                reRender="mainBlock" 
                                action="{!processText}" 
                                rendered="{!!show && !showstats}" 
                                status="mainStatus"/>

            <apex:commandButton value="{!$Label.SC_btn_Prev}" 
                                onclick="reload()" 
                                reRender="none" 
                                rendered="{!show && !showstats}"/>

            <apex:commandButton value="{!$Label.SC_btn_Check}"
                                action="{!processAgain}" 
                                rendered="{!show && !showstats}" 
                                reRender="mainBlock" 
                                status="mainStatus"
                                id="validateButton"/>
            
            <apex:commandButton value="{!$Label.SC_btn_Finish}"
                                action="{!processAgain}" 
                                rendered="{!show && !showstats}" 
                                reRender="mainBlock" 
                                status="mainStatus"
                                id="editButton" 
                                disabled="{!mustProcessAgain}">
                <apex:param assignTo="{!finish}" value="true" name="finishNow"/>
            </apex:commandButton>
            
            <apex:commandButton value="{!$Label.SC_btn_Cancel}" 
                                onclick="return confirmCancel()" 
                                immediate="true"
                                rendered="{!!showstats}"/>
                                
            <apex:commandButton value="Zurück zur Inventurübersicht" 
                                onclick="window.location='/{!$CurrentPage.parameters.oid}';" 
                                reRender="none" 
                                rendered="{!showstats}"/>
                        
            <apex:actionStatus id="mainStatus" startText="{!$Label.SC_app_Processing}" stopText="" startStyleClass="searching" />
            
        </apex:pageBlockButtons>
        
        <apex:pageMessage detail=""
                          summary="{!$Label.SC_msg_PleaseTryAgain}"
                          severity="warning"
                          strength="1"
                          rendered="{!mustProcessAgain}"/>
        
        <apex:pageBlockSection columns="1" id="inputSection" rendered="{!!show && !showstats}">
        
            <apex:outputText >

            Bitte fügen Sie die folgenden Informationen ein: [Artikelnr] &#60;tab&#62; 
            <apex:outputPanel layout="none" rendered="{!showValuationType}">
                [Valuation Type] &#60;tab&#62;
            </apex:outputPanel>

            [Zählmenge]
            
            
            </apex:outputText>
            <apex:inputTextarea value="{!input}" id="inputText" rows="20" style="width:600px;" richText="false"/>
            
        </apex:pageBlockSection>
                
        <apex:pageBlockSection columns="1" id="outputSection" rendered="{!show && !showstats}">
            <apex:outputpanel >
                <apex:inputCheckbox value="{!skipUnknownArticles}"/ > 
                <apex:outputtext value="Unbekannte Artikel überspringen"/>
            </apex:outputpanel>
            
            <apex:variable value="{!1}" var="my"/>
            <apex:repeat value="{!uploadsMain}" var="up">
            <apex:pageBlockTable value="{!up}" var="u" width="600" style="width:800px;" >
                <apex:column value="{!my}"/>
                <apex:column headerValue="Artikel" width="33%" style="height:22px;" styleClass="{!IF(u.artNumOk == false,'warningClass','')}">
                    <apex:outputField value="{!u.article.name}">
                        <apex:inlineEditSupport event="ondblclick"/>
                    </apex:outputField>
                </apex:column>
                <apex:column headerValue="Valuation Type" width="33%" style="height:22px;" styleClass="{!IF(u.valTypeOk = false,'warningClass','')}" rendered="{!showValuationType}">
                    <apex:outputField value="{!u.inventoryItem.ValuationType__c}">
                        <apex:inlineEditSupport event="ondblclick"/>
                    </apex:outputField>
                </apex:column>
                <apex:column id="qtyColumn" headerValue="Zählmenge" width="33%" style="height:22px;" styleClass="{!IF(u.countedQtyOk = false,'warningClass','')}">
                    <!-- if the qty is ok - we have no need to edit the value (use a format without )--> 
                    <apex:outputText value="{0, number}" rendered="{!u.countedQtyOk}" >
                           <apex:param value="{!u.inventoryItem.CountedQty__c}" />
                    </apex:outputText>               
                    <!-- if the qty is invalid enable the user to edit the value-->
                    <apex:outputField value="{!u.inventoryItem.CountedQty__c}" style="width:100px;float:left;" rendered="{!!u.countedQtyOk}">
                        <apex:inlineEditSupport event="ondblclick" hideOnEdit="editButton" rendered="true"/>
                    </apex:outputField>
                    <apex:outputText id="badQty" value="{!u.badCountedQty}" style="display:none;" rendered="{!!u.countedQtyOk}"  />
                    <script>
                        if({!u.countedQtyOk} == false)
                        {
                            jQuery(esc('{!$Component.qtyColumn}') + ' [id$=ileinner]').text(jQuery(esc('{!$Component.badQty}')).text());
                        }
                    </script>
                </apex:column>
                
                <apex:column headerValue="Info">
                    <apex:outputtext value="Ungültiger oder unbekannter Artikel," rendered="{!!u.artNumOk}"/>
                    <apex:outputtext value="Ungültiger oder unbekannter Valuation Type," rendered="{!valuationTypeEnabled && !u.valTypeOk}"/>
                    <apex:outputtext value="Ungültige Zählmenge" rendered="{!!u.countedQtyOk}"/>
                </apex:column>
                <apex:column >
                    <apex:variable var="my" value="{!my + 1}"/>
                </apex:column>
            </apex:pageBlockTable>
            </apex:repeat>
            
        </apex:pageBlockSection>
        
        <!-- Stats -->
        <apex:pageBlockSection columns="1" id="statsSection" rendered="{!showstats}">


            Die Inventur-Zählmengen wurden importiert. Bitte schließen Sie die Inventur, damit die Zählmengen verbucht werden können.
            <apex:pageBlockSectionItem >
                <apex:outputText value="Aktualisierte Inventurpositionen:" />
                <apex:outputText value="{!updateItems}" />
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputText value="Neu angelegte Inventurpositionen:" />
                <apex:outputText value="{!newInventoryItems}" />
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputText value="Neu angelegte Lagerpositionen:" />
                <apex:outputText value="{!newStocktems}" />
            </apex:pageBlockSectionItem>
            
            <apex:pageBlockSectionItem >
                <apex:outputText value="Unbekannte Artikel - bitte überprüfen" />
                <apex:repeat value="{!unknownArticlesMainList}" var="ua">
                    <apex:pageBlockTable value="{!ua}" var="a" style="width:40%;">
                    <apex:column headerValue="Artikel">
                        <apex:outputText value="{!a.article.name}"/>
                    </apex:column>
                    <apex:column headerValue="Zählmenge">
                        <apex:outputText value="{!a.inventoryItem.CountedQty__c}"/>
                    </apex:column>
                    </apex:pageBlockTable>
                </apex:repeat>
            </apex:pageBlockSectionItem>
        </apex:pageBlockSection>
    
    </apex:pageBlock>

</apex:form>

</apex:page>