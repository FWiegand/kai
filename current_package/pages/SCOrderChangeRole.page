<!--
 * @(#)SCOrderChangeRole.page
 *   
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved. 
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Alexei Geiger <ageiger@gms-online.de>
 * @version $Revision$, $Date$
-->

<apex:page standardController="SCOrderRole__c" recordSetVar="" extensions="SCOrderChangeRole" tabStyle="SCOrder__c">
<script type="text/javascript">
    var orderRole;
    function showCustomerSearchPopup(callback, role, accountIds)
    {
        orderRole = role;
        sList = window.open("/apex/arlCustomerSearch?isP=true&callback=" + callback + "&aids=" + accountIds ,
                            "Search", "width=1024,height=700, scrollbars=yes, location=no");
        sList.moveTo(((screen.width-1024)/2),((screen.height-700)/2));
    }

    function customerSearchCallback(value)
    {
        processCustomer(value, orderRole);
    }
</script>

<style type="text/css">
.processing
{
    background:url(/img/loading.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
}
</style>

<apex:form >
    <apex:actionFunction name="processCustomer" action="{!processCustomer}" reRender="roles" status="dlgProcessingStatus">
        <apex:param name="value" assignTo="{!value}" value="" />
        <apex:param name="orderRole" assignTo="{!orderRole}" value=""  />
    </apex:actionFunction>
    <apex:actionFunction name="changeRole" action="{!changeRole}" reRender="roles" status="dlgProcessingStatus"> 
        <apex:param name="aid" assignTo="{!value}" value="" />
    </apex:actionFunction>

    <apex:pageBlock >
        <apex:pageBlockButtons location="bottom">
            <apex:commandButton value="{!$Label.SC_btn_Continue}" action="{!onContinue}" style="width:25%"/>
        </apex:pageBlockButtons>
        <apex:pageBlockSection title="Order summary">
            <apex:outputField value="{!order.Name}" />
            <apex:outputField value="{!order.Status__c}" />
            <apex:outputField value="{!order.Type__c}" />
            <apex:outputField value="{!order.InvoicingStatus__c}" />
            <apex:outputField value="{!order.InvoicingType__c}" />
            <apex:outputField value="{!order.MaterialStatus__c}" />
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Order roles" columns="1">
            <apex:pageMessages showDetail="false" escape="false" />
        
            <apex:outputpanel >
                <apex:actionStatus id="dlgProcessingStatus" startText="{!$Label.SC_btn_Processing}" startStyleClass="processing" />              
            </apex:outputpanel>
        
            <apex:pageBlockTable value="{!roles}" var="item" id="roles">
                <apex:column headerValue="Action" rendered="{!canModifyRoles}">
                    <apex:commandLink id="caBtn"
                                      onClick="showCustomerSearchPopup('customerSearchCallback', {!item.OrderRole__c}, '{!roleAccountIds}'); return false;" 
                                      value="Change Account" 
                                      immediate="true" 
                                      reRender="none" 
                                      rendered="{!(item.OrderRole__c != '50301')}"
                                      styleClass="btn cp-link-button"
                                      style="text-decoration: none"/>
                </apex:column>
                <apex:column value="{!item.Name}"/>
                <apex:column value="{!item.OrderRole__c}"/>
                <apex:column >
                    <c:SCHoverLink oid="{!item.Account__c}" mode="EDITACC" value="{!item.Name1__c}"/>
                </apex:column>
                <apex:column headerValue="Address">
                    <!--
                      Object types for this component
                      - account
                      - location
                      - orderrole
                      - orderitem
                    -->
                    <c:SCAddressFormatted paramObjectId="{!item.id}" paramObjectType="orderrole" id="addressComponent"/>          
                </apex:column>
                <apex:column value="{!item.Phone__c}"/>
                <apex:column value="{!item.MobilePhone__c}"/>
            </apex:pageBlockTable>
        </apex:pageBlockSection>
    </apex:pageBlock>
</apex:form>
</apex:page>