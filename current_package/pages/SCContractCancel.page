<!--
 * @(#)SCContractCancel.page
 * 
 * Copyright 2011 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * This page cancels the contract.
 *
 * @author Sergey Utko <sutko@gms-online.de>
 * @version $Revision$, $Date$
-->
<apex:page controller="SCContractSearchController" action="{!reloadContractData}">
<apex:sectionHeader title="{!$Label.SC_app_CancelContract}" />

<style>

label {
    cursor:pointer;
}

.pageTitleIconNew {
    background-image: url("/img/icon/custom51_100/handsaw32.png");
    background-position: 0 0;
    height: 32px;
    width: 32px;
    margin-top: 5px;
    display: inline;
    float: left;
    margin-right: 5px;
}

.creDat{
    font-size:11px;
    width:90%;
}
.searchInputSize 
{
    font-size:11px;
    width:90%;
}
.activeTab 
{
    background: url("/s.gif") repeat-x scroll left -78px transparent;
    cursor:default;
}
.inactiveTab 
{ 
    background: url("/img/bgTop.gif") repeat-x scroll left transparent;
    cursor:pointer;
}

.finger {
    cursor:pointer;
}

div.hideCurrDate span.dateInput span.dateFormat{
   display:none;
}

.cp-selected, .cp-selected:hover
{
    background-color: #ABCFED;
}

.cp-selected-id, .status-selected-id , .order-selected-id, .order-selected-name
{
    visibility: hidden;
    width: 0px;
    height: 0px;
    display: block;
    overflow: hidden;
}

.reseting
{
    background:url(https://tapp0.salesforce.com/img/staleValue.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
}

.searching
{
    background:url(https://tapp0.salesforce.com/img/loading.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:5px; 
    font-size:12px;
}
.searching2
{
    background:url(https://tapp0.salesforce.com/img/staleValue.gif) no-repeat 0 0; 
    padding-left:20px; 
    margin-left:10px; 
    padding-bottom:3px; 
    width:16px;
    height:16px;
}

.ui-dialog-titlebar-close {
    display:none;
}

hr {
    height:2px;
    border-bottom:solid 1px #F8F8F8;
    border-top:none;
    border-left:none;
    border-right:none;
    color:gray;
    background-color:#DDD;
    margin:0;
    padding:0;
}

.searchMenuTitle {
    font-weight:bold;
    font-size:12px;
}
.contentTable {
    background-color:#EDEDED;
    border:solid 1px #E3E3E3;
    border-radius: 4px 4px 4px 4px;
}
.contentList {
    background-color:#F8F8F8;
    border:solid 1px #E3E3E3;
    border-radius: 4px 4px 4px 4px;
    height:100%;
}
.contentCell {
    padding:15px 20px 0 20px;
}
.contentTable2 td {
    padding:0 0 15px 0;
}
.cancelTitle {
    font-size:16px;
    font-weight:normal;
}

</style>

<script>
    // Avoid datepicker pop-up on loading the page
    // (default SF-Function)
    function setFocusOnLoad() {}

    function checkForm(){    
        // Checking termination possible dates list and termination extraordinary date field
        if(((document.getElementById(termDateFieldId2).value == 'none' || document.getElementById(termDateFieldId2).value == '') && document.getElementById(termDateFieldId1).value == '') || document.getElementById(termReasonFieldId).value == '' ){
            
            var alertString = '{!$Label.SC_msg_PleaseEnterInformation}: \n';
            
            if((document.getElementById(termDateFieldId2).value == 'none' || document.getElementById(termDateFieldId2).value == '') && document.getElementById(termDateFieldId1).value == ''){    
                alertString += '- {!$ObjectType.SCContract__c.fields.TerminationDate__c.label}\n';
            }
            if(document.getElementById(termReasonFieldId).value == ''){    
                alertString += '- {!$ObjectType.SCContract__c.fields.TerminationReason__c.label}\n';
            }

            alert(alertString);

            return false;
        }
        else{
            return true;
        }
    }

</script>

<apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />

<!-- Page content starts here -->

<apex:form >

    <apex:actionFunction name="checkContractInput" action="{!checkInput}" reRender="messages1" />

    <apex:pageBlock mode="edit">
        <apex:pageBlockSection columns="2">
            <apex:outputField value="{!tempTerminations.name}"/>
               <apex:outputField value="{!tempTerminations.util_templatedetails__c}"/>
            <apex:outputField value="{!tempTerminations.Status__c}"/>
               <apex:outputField value="{!tempTerminations.StartDate__c}"/>
            <apex:outputField value="{!tempTerminations.Runtime__c}"/>
               <apex:outputField value="{!tempTerminations.EndDate__c}"/>
        </apex:pageBlockSection>
    
    </apex:pageBlock>

   <apex:pageMessage strength="1" severity="error" rendered="{!blockButton}" summary="{!$Label.SC_msg_ContractCancelError}" />

    <apex:pageBlock mode="edit" id="mainBlock">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="contentTable">
        <tr><td class="contentCell">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" class="contentTable2" >
                <tr>
                    <td width="190">{!$ObjectType.SCContract__c.fields.TerminationPeriod__c.label}</td>
                    <td width="27%"><b>{!actualTermPeriod}</b></td>
                    <td rowspan="8">
                        <apex:outputPanel rendered="{!HasPendingOrders}">
                        <!-- **************** Pending Order list **************** -->
                        <table border="0" cellspacing="10" cellpadding="5" width="100%" class="contentList">
                            <tr><td><b>Pending orders referencing this contract:</b><br/><br/>
                                <apex:pageBlock mode="edit">
                                    <apex:pageBlockTable value="{!allPendingOrders}" var="a" style="font-size:11px;">
                                        <apex:column style="margin:0;padding:5px;"><a href="/{!a.Id}" target="_blank">{!a.Name}</a></apex:column>
                                        <apex:column value="{!a.Type__c}" style="margin:0;padding:5px;"/>
                                        <apex:column value="{!a.Status__c}" style="margin:0;padding:5px;"/>
                                        <apex:column value="{!a.InvoicingStatus__c}" style="margin:0;padding:5px;"/>
                                        <apex:column value="{!a.CreatedDate}" style="margin:0;padding:5px;"/>
                                        <apex:column value="{!a.Closed__c}" style="margin:0;padding:5px;"/>
                                    </apex:pageBlockTable>
                                </apex:pageBlock>
                            </td></tr>
                        </table>
                        </apex:outputPanel>
                    </td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationType__c.label}</td><td><b>{!actualTermType}</b></td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationReceivedDate__c.label}</td>
                    <td>
                        <div class="hideCurrDate">
                        <apex:inputField value="{!termReceivedDate.Start__c}" id="receivedDate" onfocus="beenFocused = false;"/>
                        </div>
                    </td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationExtraordinary__c.label}</td>
                    <td>    
                        <div class="hideCurrDate" style="margin-left;5px;">
                        
                        <apex:inputCheckbox value="{!termExtra}" selected="false" id="checkExtra"/>
                        
                        <apex:inputField value="{!terminationDate.Start__c}" id="terminationDateTxt" style="margin:0 0 0 10px;"/>
                        
                        <script type="text/javascript"> 
                            var termDateFieldId1 = document.getElementById("{!$Component.terminationDateTxt}").id;
                        </script>
                        
                        </div>
                        
                    </td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationDate__c.label}</td>
                    <td>
 
                    <apex:selectList id="termDates"  size="1" value="{!possibleTerDate}" style="font-size:11px;width:95%;">
                        <apex:selectOptions value="{!possibleTerminations}"/>
                    </apex:selectList> 
                    
                    <script type="text/javascript"> 
                        var termDateFieldId2 = document.getElementById("{!$Component.termDates}").id;
                    </script>
                    
                    <apex:outputPanel id="messages1">
                        <apex:pageMessage strength="1" severity="warning" rendered="{!showRuntimeError}" summary="{!$Label.SC_msg_NoRuntime}" />
                        <apex:pageMessage strength="1" severity="warning" rendered="{!showTermPeriodError}" summary="{!$Label.SC_msg_NoPeriod}" />
                    </apex:outputPanel>                    
                    
                    </td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationReason__c.label}</td>
                    <td>
                        <apex:inputField value="{!contract.TerminationReason__c}" id="terReason" style="font-size:11px;width:95%;"/>
                        
                        <script type="text/javascript"> 
                            var termReasonFieldId = document.getElementById("{!$Component.terReason}").id;
                        </script>
                        
                    </td>
                </tr>
                <tr><td>{!$ObjectType.SCContract__c.fields.TerminationDescription__c.label}</td>
                    <td>
                        <apex:inputField value="{!contract.TerminationDescription__c}" id="terDescription" style="font-size:11px;width:94%;"/><br/>
                        <p style="font-size:11px;"><span id="txtAreaCounter" style="color:#3C8204;">0</span> of 2000 characters</p>
                    </td>
                </tr>
            </table>
            <script>
            //document.getElementById("{!$Component.terDescription}").width = document.getElementById("{!$Component.terReason}").width;
            </script>
        </td></tr>
        <tr><td class="contentCell"><hr/></td></tr>
        <tr><td class="contentCell" style="padding-bottom:20px;">
            <apex:commandButton value="{!$Label.SC_btn_Continue}" action="{!cancelThisContract}" style="width:120px;" onclick="return checkForm()" disabled="{!blockButton}"/>
            &nbsp;
            <apex:commandButton value="{!$Label.SC_btn_Cancel}" style="width:120px;" action="{!goBack}" rendered="{!callFromContractView}"/>
            <apex:commandButton value="{!$Label.SC_btn_Cancel}" style="width:120px;" action="{!returnToContract}" rendered="{!callFromContractSearch}"/>

        </td></tr>
        </table>
    </apex:pageBlock>
</apex:form>

<!-- Page content ends here -->

<script>

    // Change the title icon
    jQuery('.pageTitleIcon').removeClass('pageTitleIcon').addClass('pageTitleIconNew'); 
    
    // Toggle the Termination date input field
    jQuery('[id$=terminationDateTxt]').attr('disabled', true);
    terminationDateTxtValue = jQuery('[id$=terminationDateTxt]').val();
    jQuery('[id$=terminationDateTxt]').val('');
      
    jQuery('[id$=checkExtra]').click(function() {
        if (jQuery('[id$=checkExtra]').is(':checked')) {
            jQuery('[id$=terminationDateTxt]').removeAttr('disabled');
            jQuery('[id$=termDates]').attr('disabled', true);
            jQuery('[id$=terminationDateTxt]').val(terminationDateTxtValue);
            jQuery('[id$=termDates]').val('none').attr('selected',true);
            jQuery('[id$=termDates]').css("color","#c4c4c4");
        } else {
            terminationDateTxtValue = jQuery('[id$=terminationDateTxt]').val();
            jQuery('[id$=terminationDateTxt]').attr('disabled', true);
            jQuery('[id$=termDates]').removeAttr('disabled');
            jQuery('[id$=terminationDateTxt]').val('');
            jQuery('[id$=termDates]').css("color","#000000");
        }     
    });
    
     jQuery('[id$=terDescription]').keyup(function() {
         jQuery('#txtAreaCounter').html(jQuery('[id$=terDescription]').val().length);
         if(jQuery('[id$=terDescription]').val().length >= 1500 && jQuery('[id$=terDescription]').val().length < 2000 ){
             jQuery('[id$=txtAreaCounter]').css("color","#CD6702");        
         }
         if(jQuery('[id$=terDescription]').val().length == 2000){
             jQuery('[id$=txtAreaCounter]').css("color","#FF0000");        
         }
         if(jQuery('[id$=terDescription]').val().length < 1500){
             jQuery('[id$=txtAreaCounter]').css("color","#3C8204");        
         }
     });
     
     checkContractInput();

</script>

</apex:page>