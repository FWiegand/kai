<apex:component controller="fsFA_PofS_DeviceModelController" allowDML="true" id="COMP_PofS_DeviceModelComponent" > 
    <!-- PofS Id Attribute -->
    <apex:attribute name="PofSId" description="The Id of the PofS record" type="ID" required="true" assignTo="{!PofS_Id}" />
    
    <!-- External Style Sheets -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <apex:stylesheet value="{!URLFOR($Resource.ImageArea, 'jquery.imgareaselect-0.9.10/css/imgareaselect-default.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.JQueryUI, 'jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.min.css')}"/>
    <!-- jQuery ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
    <apex:includeScript value="{!URLFOR($Resource.ImageArea, 'jquery.imgareaselect-0.9.10/scripts/jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.ImageArea, 'jquery.imgareaselect-0.9.10/scripts/jquery.imgareaselect.pack.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.JQueryUI, 'jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js')}"/>
    
<!-- The following script will be reloaded several times -->
<apex:outputPanel id="Scripts">
   <script type="text/javascript">
        // Variables -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Was the New Area button clicked? 
        var GV_DrawNewArea = {!GV_DrawNewArea};
        //Was the Move Area button clicked? 
        var GV_MoveArea = {!GV_MoveArea};
        //The coordinates of the current ClickArea 
        var GV_X = 50;
        var GV_Y = 50;
        //The relative coordinates of the current ClickArea 
        var GV_X_Rel;
        var GV_Y_Rel;
        //The dimensions of the ClickArea Icon 
        var GV_Width = 120;
        var GV_Height = 120;
        //The dimensions of the PofS 
        var GV_ImageWidth;
        var GV_ImageHeight;
        //The image ratio CurrentSize / OriginalSize 
        var GV_ImageRatio = 1;
        //The jQuery object of the chosen ClickArea 
        var GO_ClickArea;
        //The Type of the chosen ClickArea 
        var GV_ClickAreaType = 'Default';
        //Is there a collision of the current ClickArea with another ClickArea or is it out of the available area? 
        var GV_Collision = false;
        //Stores the parent object of the moved assortment. This is neccessary to check if the assortment is moved to another container 
        var GO_AssortmentParent;
        
        //Init Functions -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
       
        //remove the existing ClickAreas 
        removeExistingAreas();
        var GL_ExistingImageAreas = new Array();
        
        //Initialize the ClickAreas once the document is ready. This is neccessary to handle the rerenderings 
        $(document).ready(function () {
            if( GV_Initialized ){
                //console.log( 'ready.init' );
                init();
            }
        });
   
        //Initialize the ClickAreas once the document is loaded. This is neccessary to handle the first load of the page and make sure the images are already loaded. 
        $(window).load(function () {
            init();
            //console.log( 'load.init' );
            GV_Initialized = true;
        });
        
        //Initialize the ClickArea 
        function init(){
                //Get the image size and compute the ratio 
                //console.log( 'init.setSize' );
                setImageSize();
                //Show the available Area of the chosen ClickAreaType 
                //console.log( 'init.setArea' );
                setAvailableArea();
                //Draw all existing ClickAreas 
                //console.log( 'init.drawExistingAreas' );

                drawExistingAreas();
                
                //Make the ClickArea Attributes Popup draggable 
                //console.log( 'init.makeDraggable' );
                $('#DIV_PofS_DeviceModel').draggable({
                    handle: "[id$='PB_ImageAreaAttributes']",
                    start: function() {
                        if ($(this).data("scrolled")) {
                            $(this).data("scrolled", false).trigger("mouseup");
                            return false;
                        }
                    }
                });
                //Deselect the chosen ClickArea if the background image is clicked 
                //console.log( 'init.enabaleBackgroundClick' );
                $('#image').click(
                        function(){
                                if( !GV_DrawNewArea ){
                                    $('#' + GV_ChosenImageArea).addClass( 'image_area_box' ).removeClass( 'image_area_box_highlight' );
                                    GV_ChosenImageArea = null;
                                    hideClickAreaSettings();
                                    GV_ClickAreaType = 'Default';
                                    setAvailableArea();
                                }
                            }
                )
                $('#DIV_Image_AvailableArea').click(
                        function(){
                                if( !GV_DrawNewArea ){
                                    $('#' + GV_ChosenImageArea).addClass( 'image_area_box' ).removeClass( 'image_area_box_highlight' );
                                    GV_ChosenImageArea = null;
                                    hideClickAreaSettings();
                                    GV_ClickAreaType = 'Default';
                                    setAvailableArea();
                                }
                            }
                )
                //Redraw the available area if a PageBlockSection is collapsed 
                //console.log( 'init.enableCollapse' );
                $( '.pbSubheader' ).click(
                    function(){
                        setAvailableArea();
                    }
                );
                //console.log( 'init.makeUplaodWindowDraggable' );
                $( '#DIV_ImageAreaAttributes_Upload' ).draggable();
                //console.log( '/init' );
        }
   
        //Set the image size and compute the ratio 
        function setImageSize(){
            //console.log( 'setImageSize.start' );
            GV_ImageWidth = $( '#image' ).width();
            GV_ImageHeight = $( '#image' ).height();
            //console.log( 'ImageWidth: ' + GV_ImageWidth + ', ImageWidth: ' + GV_ImageWidth );
            GO_NaturalSize = getNaturalSize( 'image' );
            //console.log( 'NaturalImageWidth: ' + GO_NaturalSize.width );
            GV_ImageRatio = GV_ImageWidth / GO_NaturalSize.width;
            //console.log( 'setImageSize.start' );
            saveImageSize( GO_NaturalSize.width, GO_NaturalSize.height );
        }
   
        //Get the original image size 
        function getNaturalSize ( LO_Element ) {
            //var img = new Image();
            //img.src = $( '#' + LO_Element ).attr( 'src' );
            //return { width: img.width, height: img.height };
            
            var LV_NaturalWidth = $( '#' + LO_Element )[0].naturalWidth;
            var LV_NaturalHeight = $( '#' + LO_Element )[0].naturalHeight;
            return { width: LV_NaturalWidth, height: LV_NaturalWidth };
        }
   
        //ClickArea Functions -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Set the available area for the current ClickArea type 
        function setAvailableArea(){
            //Get the area coordinates 
            var LV_X = ( ( GM_ClickAreaTypes[ GV_ClickAreaType ].X1 / 100 * GV_ImageWidth ) - 5 - ( GV_Width / 2 ) * GV_ImageRatio );
            if( LV_X < 0 ){
                LV_X = -5;
            }
            var LV_Y = ( ( GM_ClickAreaTypes[ GV_ClickAreaType ].Y1 / 100 * GV_ImageHeight ) - 5 - ( GV_Width / 2 ) * GV_ImageRatio );
             if( LV_Y < 0 ){
                LV_Y = -5;
            }
            //Set the coordinates 
            $( '#DIV_Image_AvailableArea' ).position( {
                at: "left+" + LV_X + " top+" + LV_Y,
                my: "left top",
                of: $( '#image' ),
                collision: 'none'
            });
            //Get the area dimensions 
            var LV_Width = ( GM_ClickAreaTypes[ GV_ClickAreaType ].X2 - GM_ClickAreaTypes[ GV_ClickAreaType ].X1 ) / 100 * GV_ImageWidth + ( GV_Width ) * GV_ImageRatio;
            if( LV_X + LV_Width > GV_ImageWidth ){
                LV_Width = GV_ImageWidth - LV_X;
            }
            var LV_Height = ( GM_ClickAreaTypes[ GV_ClickAreaType ].Y2 - GM_ClickAreaTypes[ GV_ClickAreaType ].Y1 ) / 100 * GV_ImageHeight + ( GV_Width ) * GV_ImageRatio;
            if( LV_Y + LV_Height > GV_ImageHeight ){
                LV_Height = GV_ImageHeight - LV_Y;
            }
            //Set the area dimensions 
            $( '#DIV_Image_AvailableArea' ).width( LV_Width );
            $( '#DIV_Image_AvailableArea' ).height( LV_Height );

        }
   
        //Handle the new area creation 
        function drawNewArea ( PV_X, PV_Y) {
            if( GV_ImageArea == null ){
                //console.log( 'drawNewArea.start' );
                var LV_Width = GV_Width * GV_ImageRatio;
                var LV_Height= GV_Width * GV_ImageRatio;
                var LV_X = ( PV_X / 100 * GV_ImageWidth ) - ( LV_Width / 2 );
                var LV_Y = ( PV_Y / 100 * GV_ImageHeight ) - ( LV_Height / 2 );
                //console.log( 'Ratio: ' + GV_ImageRatio );
                //console.log( 'X: ' + LV_X + ', Y: ' + LV_Y + ', Width: ' + LV_Width + ', Height: ' + LV_Height );
                GV_ImageArea = $('#image').imgAreaSelect({
                    instance: true,
                    persistent: true,
                    handles: false,
                    enable: true,
                    resizable: false,
                    onSelectChange: function (img, selection) {
                        transparentizeClickAreaSettings();
                        var LV_X = selection.x1 + ( GV_Width / 2 ) * GV_ImageRatio;
                        var LV_Y = selection.y1 + ( GV_Height / 2 ) * GV_ImageRatio;
                        checkCollision( LV_X, LV_Y );
                        
                        $("#DIV_PofS_DeviceModel").position({
                             my: "left top",
                             of: $( '.imgareaselect-selection' ),
                             my: "left+65 top-50",
                             collision: "flip fit"
                        });
                    },
                    onSelectEnd: function ( LO_Image, LO_Selection ){
                        untransparentizeClickAreaSettings();
                    },
                    x1: LV_X,
                    y1: LV_Y,
                    x2: LV_X + LV_Width,
                    y2: LV_Y + LV_Height,
                    zIndex: 6
                });
                $( '.imgareaselect-selection' ).css( 'background-size', Math.floor( GV_Width * GV_ImageRatio ) + 'px ' + Math.floor( GV_Width * GV_ImageRatio ) + 'px ' );
                checkCollision( LV_X + ( LV_Width / 2 ), LV_Y + ( LV_Height / 2 ) );
                showClickAreaSettings();
                $("#DIV_PofS_DeviceModel").position({
                             my: "left top",
                             of: $( '.imgareaselect-selection' ),
                             my: "left+65 top-50",
                             collision: "flip fit"
                });
                //console.log( 'drawNewArea.stop' );
            }else{
                //console.log( 'drawNewArea.NULL' );
            }
        }

        //Draw the existing ClickAreas 
        function drawExistingAreas(){
            for( var i = 0; i < GL_ExistingImageAreas.length; i++ ){
                GL_ExistingImageAreas[ i ].attachToImage();
            }
        }

        //Remove the existing ClickArea 
        /*
        function removeExistingAreas(){
            for( var i = 0; i < GL_ExistingImageAreas.length; i++ ){
                $( '#' + GL_ExistingImageAreas[ i ].LV_Id ).remove();
                $( '#desc_' + GL_ExistingImageAreas[ i ].LV_Id ).remove();
            }
        }
        */
          
        //Remove the New ClickAres selection 
        function hideArea () {
            $('#image').imgAreaSelect({
                remove: true
            });
            GV_ImageArea = null;
        }
        
        //Save the ClckArea if there is no collision 
        function saveNewArea(){
            if( !GV_Collision ){
                saveArea(GV_X_Rel, GV_Y_Rel);
            }else{
                alert( 'The area can not be saved because it intersects with another one.' );
            }
        }
        
        //Display the ClickArea Popup 
        function showClickAreaSettings(){
            $("#DIV_PofS_DeviceModel").position({
                             my: "left top",
                             of: GO_ClickArea,
                             my: "left+65 top-50",
                             collision: "flip fit"
            });
            $('#DIV_PofS_DeviceModel').css('visibility', 'visible');
            setClickAreaType( false );
        }
   
        //Hide the ClickArea Popup 
        function hideClickAreaSettings(){
            $('#DIV_PofS_DeviceModel').css('visibility', 'hidden');
        }

        //Hide the ClickArea Popup 
        function hideClickAreaSettings2(){
            $('#DIV_PofS_DeviceModel').css('visibility', 'hidden');
            GV_ChosenImageArea = null;
        }
   
        // Make the ClickArea Popup transparent 
        function transparentizeClickAreaSettings(){
            $('#DIV_PofS_DeviceModel').css({ 'opacity' : 0.5 });
        }

        //Make the ClickArea Popup untransparent 
        function untransparentizeClickAreaSettings(){
            $('#DIV_PofS_DeviceModel').css({ 'opacity' : 1 });
        }
          
        //Class that handles the display of ImageAreas 
        function ExistingImageArea( PV_Id, PV_Name, PV_IconType, PV_X, PV_Y ){
            this.LV_Id = PV_Id;
            this.LV_Name = PV_Name;
            this.LV_IconType = PV_IconType.replace( /\s/g, '_' );;
            this.LV_Width = GV_Width * GV_ImageRatio;
            this.LV_Height= GV_Height * GV_ImageRatio;
            this.LV_X = ( PV_X / 100 * GV_ImageWidth ) - ( this.LV_Width / 2 );
            this.LV_Y = ( PV_Y / 100 * GV_ImageHeight ) - ( this.LV_Height / 2 );

            //Draw the ClickArea 
            this.attachToImage = function(){
                var LV_X = this.LV_X - $('#DIV_Image').offset().left;
                var LV_Y = this.LV_Y - $('#DIV_Image').offset().top;
                
                var LV_ImageAreaDiv = $('<div id="' + this.LV_Id + '" class="image_area_box image_area_box_' + this.LV_IconType + '"></div>').css(
                    {"position":"absolute","left":this.LV_X,"top":this.LV_Y,"width":this.LV_Width,"height":this.LV_Height}
                ).on(
                    'mouseenter',
                    function(){
                        $(this).removeClass( 'image_area_box' ).addClass( 'image_area_box_highlight' );
                    }
                ).on(
                    'mouseleave',
                    function(){
                        if( GV_ChosenImageArea != $( this ).attr( 'id' ) ){
                            $(this).addClass( 'image_area_box' ).removeClass( 'image_area_box_highlight' );
                        }
                    }
                ).click(
                    function(){
                        if( GV_ChosenImageArea != $( this ).attr( 'id' ) ){
                            hideClickAreaSettings();
                            $('#' + GV_ChosenImageArea).addClass( 'image_area_box' ).removeClass( 'image_area_box_highlight' );
                            GV_ChosenImageArea = $( this ).attr( 'id' );
                            GO_ClickArea = $( this );
                            chooseArea( $( this ).attr( 'id' ) );
                            GV_X = $( this ).position().left + ( GV_Width / 2 ) * GV_ImageRatio;
                            GV_Y = $( this ).position().top + ( GV_Height / 2 ) * GV_ImageRatio;
                            GV_X_Rel = GV_X / GV_ImageWidth * 100;
                            GV_Y_Rel = GV_Y / GV_ImageHeight * 100;
                        }
                    }
                );
                $('#DIV_Image').append(LV_ImageAreaDiv);
                //$( '.image_area_box_' + this.LV_IconType ).css( 'background-size', this.LV_Width + 'px ' + this.LV_Height + 'px ' );
                $( '.image_area_box_' + this.LV_IconType ).css( 'background-size', 'cover' );
                
                var LV_ImageAreaDescriptionDiv = $('<div id="desc_' + this.LV_Id + '" class="image_area_description_box">' + this.LV_Name + '</div>').css({"position":"absolute","left":this.LV_X1,"top":this.LV_Y1 - 15,"width":100,"height":15});
                $('#DIV_Image').append(LV_ImageAreaDescriptionDiv );
            }
        }
                
        //Draw the available ClickArea and check for collision 
        function setClickAreaType( PV_CheckCollision ){
            GV_ClickAreaType = $( "[id$='IF_ClickAreaType']" ).val().replace( /\s/g, '_' );
            if( GV_ClickAreaType == '' ){
                GV_ClickAreaType = 'Default';
            }
            setAvailableArea();
            if( PV_CheckCollision ){
                checkCollision( GV_X, GV_Y );
            }else{
                GV_Collision = false;
            }
            makeQuestionsSortable();
            makeAssortmentsSortable();
            if( GV_ClickAreaType == 'Assortment' ){
                $('#DIV_PofS_DeviceModel').width( 900 );
            }else{
                $('#DIV_PofS_DeviceModel').width( 600 );
            }
        }

        //Collision Functions -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Is there a collision with an ClickArea or is the coordinate ouside of the available area? 
        function isCollision( PV_X, PV_Y ){
            var LV_Collision = false;

            if( ( PV_X < GM_ClickAreaTypes[ GV_ClickAreaType ].X1 / 100 * GV_ImageWidth ) || ( PV_Y < GM_ClickAreaTypes[ GV_ClickAreaType ].Y1 / 100 * GV_ImageHeight ) || ( PV_X > GM_ClickAreaTypes[ GV_ClickAreaType ].X2 / 100 * GV_ImageWidth ) || ( PV_Y > GM_ClickAreaTypes[ GV_ClickAreaType ].Y2 / 100 * GV_ImageHeight ) ){
                LV_Collision = true;
            }
                
            return LV_Collision;
        }
   
        //Check if there is a collision 
        function checkCollision( PV_X, PV_Y ){
                if( !isCollision( PV_X, PV_Y ) ){
                    GV_X = PV_X;
                    GV_Y = PV_Y;
                    GV_X_Rel = PV_X / GV_ImageWidth * 100;
                    GV_Y_Rel = PV_Y / GV_ImageHeight * 100;
                    hideCollision();
                }else{
                    GV_X = PV_X;
                    GV_Y = PV_Y;
                    GV_X_Rel = PV_X / GV_ImageWidth * 100;
                    GV_Y_Rel = PV_Y / GV_ImageHeight * 100;
                    showCollision();
                }   
        }
           
        //Question Functions -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Make the questions sortable 
        function makeQuestionsSortable(){
            $( "[id$='PBS_ImageAreaQuestions'] .pbSubsection .detailList tbody tr td:nth-child(1) span" ).addClass( "ui-icon ui-icon-arrowthick-2-n-s" );
            $( "[id$='PBS_ImageAreaQuestions'] .pbSubsection .detailList tbody tr:not(:nth-child(1), :nth-child(2))" ).addClass( "question-row" );
            $( "[id$='PBS_ImageAreaQuestions'] .pbSubsection .detailList tbody" ).sortable({
                    axis: "y", 
                    items: "> tr:not(:nth-child(1), :nth-child(2), :nth-last-child(1), :nth-last-child(2), :nth-last-child(3))", 
                    containment: "parent", 
                    placeholder: "sortable-placeholder",
                    opacity: 1, 
                    helper: "clone",
                    start: function( e, ui ){
                        ui.placeholder.height(ui.item.height());
                    },
                    stop: function( e, ui ){
                        var LV_OldClickAreaQuestionRowNumber = ui.item.find( ">:first-child span span" ).attr( "data-question-rownumber" );
                        var LV_NewClickAreaQuestionRowNumber = ui.item.index() - 1;
                        changeQuestionOrder( LV_OldClickAreaQuestionRowNumber, LV_NewClickAreaQuestionRowNumber );
                    }
                });
        }

        // Assortment Functions -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //Make the Assortments sorable 
        function makeAssortmentsSortable(){
            $( "[id$='PBS_Assortments'] .pbSubsection" ).addClass( "assortment-control-container" );
            $( "[id$='PBS_Assortments'] .pbSubsection .detailList tbody tr td:nth-child(1) span" ).addClass( "ui-icon ui-icon-arrowthick-2-n-s" );
            $( "[id$='PBS_Assortments'] .pbSubsection .detailList tbody tr:not(:nth-child(1), :nth-child(2))" ).addClass( "question-row" );
            $( "[id$='PBS_Assortments'] .pbSubsection .detailList tbody" ).sortable({
                    items: "> tr:not(:nth-child(1), :nth-child(2), :nth-last-child(1), :nth-last-child(2), :nth-last-child(3))", 
                    placeholder: "sortable-placeholder",
                    opacity: 1, 
                    helper: "clone",
                    connectWith: "[id$='PBS_AssortmentProducts'] .pbSubsection .detailList tbody",
                    start: function( e, ui ){
                        ui.placeholder.height(ui.item.height());
                        GO_AssortmentParent = ui.item.parent();
                    },
                    receive: function( e, ui ){
                        var LV_OldAssortmentRowNumber = ui.item.find( ">:first-child span span" ).attr( "data-question-rownumber" );
                        var LV_NewAssortmentRowNumber = ui.item.index() - 1;
                        newAssortment( LV_OldAssortmentRowNumber, LV_NewAssortmentRowNumber );
                    },
                    stop: function( e, ui ){
                        if( GO_AssortmentParent.get( 0 ) === ui.item.parent().get( 0 ) ){
                            var LV_OldAssortmentRowNumber = ui.item.find( ">:first-child span span" ).attr( "data-question-rownumber" );
                            var LV_NewAssortmentRowNumber = ui.item.index() - 1;
                            changeAssortmentOrder( LV_OldAssortmentRowNumber, LV_NewAssortmentRowNumber );
                        }
                    }
            });
            
            $( "[id$='PBS_AssortmentProducts'] .pbSubsection" ).addClass( "assortment-control-container" );
            $( "[id$='PBS_AssortmentProducts'] .pbSubsection .detailList tbody tr td:nth-child(1) span" ).addClass( "ui-icon ui-icon-arrowthick-2-n-s" );
            $( "[id$='PBS_AssortmentProducts'] .pbSubsection .detailList tbody tr:not(:nth-child(1), :nth-child(2))" ).addClass( "question-row" );
            $( "[id$='PBS_AssortmentProducts'] .pbSubsection .detailList tbody" ).sortable({
                    items: "> tr:not(:nth-child(1), :nth-child(2), :nth-last-child(1), :nth-last-child(2), :nth-last-child(3))", 
                    placeholder: "sortable-placeholder",
                    opacity: 1, 
                    helper: "clone",
                    connectWith: "[id$='PBS_Assortments'] .pbSubsection .detailList tbody",
                    start: function( e, ui ){
                        ui.placeholder.height(ui.item.height());
                    },
                    receive: function( e, ui ){
                        var LV_OldAssortmentRowNumber = ui.item.find( ">:first-child span span" ).attr( "data-question-rownumber" );
                        var LV_NewAssortmentRowNumber = ui.item.index() - 1;
                        deleteAssortment( LV_OldAssortmentRowNumber );
                    }
            });
            $( "[id$='PBS_Assortments'] .pbSubsection, [id$='PBS_AssortmentProducts'] .pbSubsection" ).scroll(function() {
                    $('#DIV_PofS_DeviceModel').data("scrolled", true);

            });
        }
        
        //Eanable sortables
        function enableSortables(){
            makeQuestionsSortable();
            makeAssortmentsSortable();
        }
   </script>
</apex:outputPanel>
        
    <!-- Popup view for new device -->
    <apex:form >
        <div id="DIV_PofS_DeviceModel" class="red" style="position: absolute; z-index: 10;  width: 300px; height: 200px; visibility: hidden; overflow: auto; background-color: #f8f8f8; border-radius: 5px; border: 2px solid #B81321">
            <apex:pageBlock id="PB_PofS_DeviceModelAttributes">
                <!-- Header Buttons -->
                <apex:outputPanel id="OP_Buttons_DeviceAttributes" layout="block" styleClass="button_bar">
                    <apex:commandButton id="CB_SavePofSDeviceModel" value="{!$Label.fsfa_save}" onclick=" return false;" status="AS_Status" />
                    <!-- <apex:commandButton id="CB_CancelPofSDeviceModel" value="{!$Label.fsfa_close}" action="none" rerender="Scripts, OP_Buttons_ImageAreaAttributes, OP_Buttons_ImageAreaAttributesBottom, OP_Area1_ImageAreaAttributes" oncomplete="" immediate="true" status="AS_Status" /> -->
                    <apex:commandButton id="CB_CancelPofSDeviceModel" value="{!$Label.fsfa_close}" action="{!cancelPofDeviceModel}" rerender="Scripts, OP_Buttons_ImageAreaAttributes, OP_Buttons_ImageAreaAttributesBottom, OP_Area1_ImageAreaAttributes" oncomplete="hidePofSDeviceModel" immediate="true" status="AS_Status" /> 
                    
                </apex:outputPanel>
                <!-- /Header Buttons -->
                
                <apex:outputPanel id="OP_PofSModelDevice_DeviceAttributes" styleClass="red">
                    <apex:pageMessages />
                </apex:outputPanel>                
                <apex:pageblockSection id="PBS_PofSDeviceModel_Device" columns="1" title="Neues Device" showHeader="true">
                    <apex:inputHidden value="{!PofSId}" id="PofS_Id"/>
                    <apex:inputField id="PofS_DeviceLookup" value="{!PofSDeviceLookup.DeviceModel__c}"/>
                </apex:pageblockSection>                
            </apex:pageBlock>
        </div>
    </apex:form>

    <!-- Table of devices -->
    <apex:outputPanel styleClass="red">
        <apex:pageBlockSection id="PBS_PofSDeviceModel" columns="1" title="Devices" showHeader="true">                    
            <apex:form >
                <apex:outputPanel id="OP_Buttons_Device" layout="block" styleClass="button_bar">
                    <apex:commandButton value="Neues Device" onclick="showPofSDeviceModel(); return false;" />
                </apex:outputPanel> 
            </apex:form>                     
            <apex:pageBlockTable value="{!listPofSDeviceModels}" var="PofSDeviceModels" rendered="{!listPofSDeviceModels.size!=0}">
                <apex:column headerValue="Name">
                    <apex:outputLink value="/{!PofSDeviceModels.DeviceModel__r.Id}">{!PofSDeviceModels.DeviceModel__r.Name}</apex:outputLink>
                </apex:column>
                <apex:column value="{!PofSDeviceModels.DeviceModel__r.Brand__c}"/>
                <apex:column value="{!PofSDeviceModels.DeviceModel__r.AvailableBrands__c}"/>     
            </apex:pageBlockTable>
        </apex:pageBlockSection>
    </apex:outputPanel>
    
</apex:component>