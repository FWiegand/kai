<!--
 * @(#)SCArticleSearch.component
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
-->
<!--
 * This component is used for the article search in various ways
-->
<apex:component controller="SCArticleSearchLookupController" selfClosing="true" >

    <apex:attribute name="selectedFunction" type="String"
        description="JS function to call if an item is selected" required="true"/>
    <apex:attribute name="selectedArticleId" type="String"
        description="Hidden field ID to write the selected ID to." required="true"/>
    <apex:attribute name="selectedValuationType" type="String"
        description="Hidden field to write the selected valuation type." required="false"/>
<!--
    <apex:attribute name="articleClassesAttr" type="String[]"
        assignTo="{!searchClasses}"
        description="Unit classes to limit article search" required="false"/>
    <apex:attribute name="articleTypesAttr" type="String[]"
        assignTo="{!searchTypes}"
        description="Unit types to limit article search" required="false"/>
-->
    <apex:attribute name="searchModeAttr" type="Integer"
        description="Search modes changes the article search (e.g. van stock search)" required="false"/>
    <apex:attribute name="resourceIdAttr" type="String"
        assignTo="{!resourceId}"
        description="Resource Id to use for stock related searches" required="false"/>
    <apex:attribute name="debugModeAttr" type="Boolean"
        assignTo="{!debugMode}"
        description="Enable on screen debug traces" required="false"/>
    <apex:attribute name="lockTypes" type="String"
        assignTo="{!lockTypes}"
        description="List of locktypes for selecting articles" required="false"/>
        
    <apex:attribute name="usersResourceIdAttr" type="String"
        assignTo="{!usersResourceId}"
        description="Id of the Resource of the current User (searchMode 3 only)" required="false"/>
    <apex:attribute name="selectableResourceIdsAttr" type="String"
        assignTo="{!selectableResourceIds}"
        description="Ids of the selectalbe Resources (searchMode 3 only)" required="false"/>


<style>
.h1 { 
    font-size:2.2em;
    font-weight:bold;
    margin: 5px 5px 15px 5px;
}

.cp-status-info-start 
{
    background-color:#F8F8AA;
    border:1px solid #D4DADC;
    display:inline-block;
    line-height:18px;
    margin-bottom:10px;
    padding:0 5px;
    color: #808080;
}
.cp-status-info-stop
{
    background-color:#ffffff;
    border:1px solid #D4DADC;
    display:inline-block;
    line-height:18px;
    margin-bottom:10px;
    padding:0 5px;
    color: #808080;
}

.cp-invisible
{
    display: none;
    visibility: hidden;
}

/* Overwritten SFDC styles */
.lookupHoverDetail 
{
    z-index: 9999;
}
</style>

<script>

var lockTypesInputId = '';

var searchData = {searchMode: 0, 
                  article: '',
                  autoSearch: 0,
                  articleClass: '',
                  articleType: ''};

/**
 * @author Thorsten Klein <tklein@gms-online.de>
 * @author Sergey Utko <sutko@gms-online.de>
 */
function articleSearchComponentSelected (element, articleId, articleName, valuationType)
{
    /*** If article lock type != Active or Replaced By != '' then user must confirm the assignment dialog ***/

    var actualLockTypeLabel = jQuery(element).children('.itemLockType:first').text();
    var actualReplacedBy = jQuery(element).children('.itemReplacedBy:first').text();
    var showWarningDialog = false;
    
    // Getting the lock type value depending on label
    for(var x = 0; x < document.getElementById(lockTypesInputId).length; x++){
        if(document.getElementById(lockTypesInputId).options[x].text == actualLockTypeLabel){
            if(document.getElementById(lockTypesInputId).options[x].value != '50201' || actualReplacedBy != ''){
                showWarningDialog = true;
            }
            else{
                showWarningDialog = false;
            }   
        }
    }
    
    if(showWarningDialog)
        var isLockedOrReplaced = confirm("{!$Label.SC_msg_ArticleLockedOrReplaced}");
    else
        var isLockedOrReplaced = true;
    
    if(isLockedOrReplaced){
        // OK button was clicked

        selArtId = document.getElementById('{!selectedArticleId}');
        
        if (selArtId != null)
        {
            selArtId.value = articleId;
        }
        
        selValType = document.getElementById('{!selectedValuationType}');
        
        if (selValType != null)
        {
            selValType.value = valuationType;
        }
    
        // the main div for the article search can have an attribute
        // that holds the name of the callback function to call.
        // the callback function gets the SFDC ID as parameter
        var callbackString = '';
        var callback = '';
        callbackString = jQuery(element).parents('.ui-dialog-content:first').attr('callback');
        
        if ((callbackString !== undefined) && (null != callbackString) && 
            (callbackString.length > 0) && (window[callbackString] !== undefined))
        {
            callback =  window[callbackString];
        }
    
        // The callback function attribute has precedence over
        // the function set in the components attribute
        if (callback !== undefined &&
            callback.length > 0 &&
            callback instanceof Function)
        {
            //callback(articleId, articleName);
            callback(articleId, selValType.value);
            jQuery('input[id$=asArticleInput]').val('');
        }
        else if ('{!JSENCODE(selectedFunction)}'.length > 0 &&
                 {!JSENCODE(selectedFunction)} instanceof Function)
        {
            {!JSENCODE(selectedFunction)}(articleId, selValType.value);
            jQuery('input[id$=asArticleInput]').val('');
            //articleSearchClear();
        }

    }
    else{
        // Cancel button was clicked
        return false;
    }
    
}

/**
 * Handle the enter, click and change event on the given element
 * and call the actual search function.
 * Note that all default events and event bubbling is deactivated
 * 
 * @param element The element that triggered the event
 * @param event The actual event
 * @author Thorsten Klein <tklein@gms-online.de>
 */
function articleSearchEnter(element, event)
{
//alert('keyCode: **' + event.keyCode + '**' + event.type);


    // Catch the return key
    if (event.keyCode == 13 || event.type == 'click' || event.type == 'change') {
        // prevent Firefox from submitting the first 
        // submit button even hidden ones
        try {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        catch (err)
        {
            event.returnValue = false;
        }
        doSearch(element, null,
                 searchData['articleClass'],
                 searchData['articleType']);

        return false;
    }

    return false;
}

/**
 * Prepare everything that is needed for the search
 * e.g. setting hidden fields and call the actual 
 * search function
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 */ 
function doSearch(element,article,articleClass,articleType)
{
    jQuery('input[id$=asArticle]').val(jQuery('input[id$=asArticleInput]').val());
    searchMode = jQuery(element).parents('.ui-dialog-content:first').attr('searchmode');
    jQuery('input[id$=searchMode]').val(searchMode);

    articleSearch(searchMode, article,articleClass,articleType);
}

/**
 * Create custom event and bind it to the article input field.
 * The event can then be triggered from the everywhere on the page
 * by calling the trigger() function.
 *
 * jQuery('input').trigger('cpArticleSearch', {searchMode: 1, 
 *                                             article: '000222000',
 *                                             autoSearch: 1,
 *                                             articleClass: '',
 *                                             articleType: ''});
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 */
jQuery('input[id$=asArticleInput]').bind('cpArticleSearch', 
    function(event, data)
    {
    
        if (data['article'] === undefined)
        {
            data['article'] = '';
        }
        jQuery(this).val(data['article']);
        
        searchData = data;
        
        // searchMode = jQuery('input[id$=searchMode]').val();
        // if (searchMode.length == 0)
        {
            searchMode = jQuery(this).parents('.ui-dialog-content:first').attr('searchmode');
        }
        if (searchMode == 1)
        {
            jQuery('[id$=asArticleBrand]').selectOptions('');
            jQuery('[id$=asArticleBrand]').addClass('cp-invisible');
            jQuery('[id$=asArticleBrandLabel]').addClass('cp-invisible');
        }
        else
        {
            jQuery('[id$=asArticleBrand]').removeClass('cp-invisible');
            jQuery('[id$=asArticleBrandLabel]').removeClass('cp-invisible');
        }
        
        if ((data['autoSearch'] == 1) || (data['article'].length > 0))
        {
            doSearch(this,data['article'],
                     data['articleClass'],data['articleType']);
        }
        
        jQuery(this).focus();
    });
    
/**
 * Simple event to set the focus to the input field
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 */
jQuery('input[id$=asArticleInput]').bind('cpArticleSearchFocus', 
    function(event)
    {
        // set the focus after 500 ms to allow the DOM being 
        // updated.
        setTimeout("jQuery('input[id$=asArticleInput]').focus();", 1500);
    });    
    
/**
 * Checks the result set, if there is only one result
 * do an automatic click on this result.
 *
 * @author Alexander Wagner <awagner@gms-online.de>
 */ 
function checkResultSet()
{
    if (jQuery('[id^=result_]').length == 1)
    {
        jQuery('[id^=result_0.0]').click();
    }
}
     

</script>

<!-- don't use renderRegionOnly="true" here, because it causes trouble -->
<apex:actionRegion >
    <apex:panelGroup id="asMessagePanel">
        <apex:pageMessage summary="{!message}" severity="warning" strength="3" rendered="{!NOT(ISNULL(message))}"/>
    </apex:panelGroup>
    
    <!-- ############ action functions -->
    <apex:actionFunction name="articleSearch" 
                         action="{!search}" 
                         reRender="asSearchGrid,asResultBlock,asMessages,as-DebugLog" 
                         status="cpArticleSearchStatus"
                         oncomplete="jQuery('input[id$=asArticleInput]').focus();
                                     jQuery('input[id$=asArticleInput]').val(jQuery('input[id$=asArticle]').val());
                                     checkResultSet();">
        <apex:param name="searchMode" value="" />
        <apex:param name="articleQuery" value="" />
        <apex:param name="articleClass" value="" />
        <apex:param name="articleType" value="" />
    </apex:actionFunction>

    <apex:actionFunction name="articleSearchClear" action="{!clear}" reRender="asSearchGrid,asResultBlock,as-DebugLog"/>
    
    <!-- ####### hidden fields -->
    <apex:inputHidden value="{!articleQuery}" id="asArticle" required="false"/>
    <apex:inputHidden value="{!searchMode}" id="searchMode" required="false"/>
    
    <apex:messages id="asMessages" />

        <apex:pageBlock id="asPageBlock">
            <apex:panelGrid columns="4" id="asSearchGrid">
                <apex:outputLabel value="{!$Label.SC_app_ArticleSearchBox}"/>
                <apex:outputLabel value="{!$ObjectType.SCProductModel__c.fields.Brand__c.label}" id="asArticleBrandLabel" rendered="{!ShowBrandSelectList}"/> <apex:outputLabel value="" rendered="{!NOT(ShowBrandSelectList)}"/>
                <apex:outputLabel value="{!$ObjectType.SCStock__c.label}" rendered="{!IF(searchMode == 3,true,false)}"/> <apex:outputLabel value="" rendered="{!IF(searchMode == 3,false,true)}"/>
                <apex:outputLabel value="" />
                                
                <!-- article search box -->
                <apex:inputText id="asArticleInput" tabindex="0"
                                onkeypress="articleSearchEnter(this, event)" />

                <!-- brand select list -->
                <apex:selectList value="{!selectedBrand}" 
                                 multiselect="false" 
                                 size="1" 
                                 id="asArticleBrand" 
                                 onchange="articleSearchEnter(this, event)"
                                 rendered="{!ShowBrandSelectList}"
                                 styleClass=""> 
                    <apex:selectOptions value="{!allBrand}"/>
                </apex:selectList><apex:outputLabel value="" rendered="{!NOT(ShowBrandSelectList)}"/>

                <!-- resource select list -->
                <apex:selectList value="{!resourceId}" 
                                 multiselect="false" 
                                 size="1" 
                                 id="asArticleResource" 
                                 onchange="articleSearchEnter(this, event)"
                                 rendered="{!IF(searchMode == 3,true,false)}"
                                 styleClass=""> 
                    <apex:selectOptions value="{!allResources}"/>
                </apex:selectList><apex:outputLabel value="" rendered="{!IF(searchMode == 3,false,true)}"/>
<!-- rendered="{!IF(searchMode == 0,true,false)}" -->

                <!-- search button look alike-->
                <!-- a command link doesn't work here since it would end up
                     in a POST request -->
                <a href="javascript:void(0);" 
                   onclick="articleSearchEnter(this, event)"
                   class="btn cp-link-button asArticleSearch">
                   {!$Label.SC_btn_Search}
                </a>
                
                <apex:actionStatus startStyleClass="cp-status-info-start" 

                   id="cpArticleSearchStatus" >
                    <apex:facet name="start">
                        {!$Label.SC_app_Searching}
                    </apex:facet>
                </apex:actionStatus>
                
                <!-- Helper element to get the value of the Lock Type Picklist -->               
                <apex:selectList id="lockTypes" required="false" style="display:none">
                    <apex:selectOptions value="{!lockTypesList}"/>
                </apex:selectList>

                <script>
                    lockTypesInputId = document.getElementById("{!$Component.lockTypes}").id;
                </script>
            </apex:panelGrid>
        </apex:pageBlock>

        <apex:panelGroup id="asResultBlock">
<!--            <apex:pageBlock title="{!$Label.SC_app_Results}" rendered="{!NOT(ISNULL(foundArticles))}"> -->
            <apex:pageBlock rendered="{!NOT(ISNULL(foundArticles))}" >
                <apex:pageBlockSection columns="1" id="asResultTable">
                    <apex:variable value="{!0}" var="rowNumLine" />
                    <apex:pageBlockTable value="{!foundArticles}" var="item">
                        <apex:column >
                            <apex:facet name="header">{!$ObjectType.SCArticle__c.fields.Name.label}</apex:facet>
                            <a href="javascript:void(0);"
                                onclick="articleSearchComponentSelected(this, '{!item.articleId}', '{!item.articleNo}', '{!item.stockItem}')"
                                id="result_{!rowNumLine}"
                            >{!item.articleNo}
                                <span style="display:none;" class="itemLockType">{!item.lockType}</span>
                                <span style="display:none;" class="itemReplacedBy">{!item.replacedBy}</span>
                            </a>
                        </apex:column>
                        <apex:column value="{!item.description}"
                                     headerValue="{!$ObjectType.SCArticle__c.fields.ArticleNameCalc__c.label}" />
                        <apex:column value="{!item.ValuationTypeLabel}" rendered="{!DisplayValTypeColumn}"
                                     headerValue="{!$ObjectType.SCStockItem__c.fields.ValuationType__c.label}" />
                        <apex:column rendered="{!IF(OR(searchMode == 1, searchMode == 2, searchMode == 3),true,false)}"
                                     headerValue="{!$ObjectType.SCStockItem__c.fields.Qty__c.label}"
                                     styleClass="r_align">
                        <apex:outputText value="{0, number}" >
                            <apex:param value="{!item.qty}" />
                        </apex:outputText>
                    </apex:column>
                        <apex:column value="{!item.replacedBy}"
                                     headerValue="{!$ObjectType.SCArticle__c.fields.ReplacedBy__c.label}" />
                        <apex:column value="{!item.lockType}"
                                     headerValue="{!$ObjectType.SCArticle__c.fields.LockType__c.label}" />
                        <apex:column value="{!item.articleType}"
                                     headerValue="{!$ObjectType.SCArticle__c.fields.Type__c.label}" />
                        <apex:column value="{!item.eanCode}"
                                     headerValue="{!$ObjectType.SCArticle__c.fields.EANCode__c.label}" />
                        <apex:column value="{!item.brand}" 
                                     headerValue="{!$ObjectType.Brand__c.fields.Name.label}" />
                        <apex:column style="width:0px">
                            <!-- column only used internally for row increment -->
                            <apex:variable var="rowNumLine" value="{!rowNumLine + 1}" />
                        </apex:column>
                    </apex:pageBlockTable> 
                </apex:pageBlockSection>   
                <!-- paging part -->
                <c:SCPagination id="asPagination" setController="{!setController}" pageRerender="asResultTable" />
            </apex:pageBlock>
        </apex:panelGroup>
        

        <apex:panelGroup layout="block" rendered="{!debugMode}" id="as-DebugLog" style="font-face: bold; font-color: #FF">
            Query: {!query}<br />
            resourceId: {!resourceId}<br />
            searchMode: {!searchMode}
        </apex:panelGroup>
    
        
</apex:actionRegion>

</apex:component>