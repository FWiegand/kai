<!--
 * @(#)SCTimereportView.component
 *
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Dietrich Herdt <dherdt@gms-online.de>
 * @version $Revision$, $Date$
-->
<!--
 * This component displays the timereport of
 * @param rid    resource
 * @param day    the day to display-->
<apex:component controller="SCTimereportViewController" id="timereportview2" allowDML="true">

<style>

.cp-selected
{
    background-color: #ABCFED;
}

.cp-selected-id
{
    visibility: hidden;
    width: 0px;
    height: 0px;
    display: block;
    overflow: hidden;
}

a.cp-link-button {
    text-decoration: none;
    margin: 1px 3px 1px 3px;
    text-align: center;
}

a.cp-link-button:hover {
    text-decoration: none;
    color: #333333;
    background-position: right -30px;
}

a.cp-link-button:visited {
    text-decoration: none;
    color: #333333;
}

.statField
{
    visibility: hidden;
    width: 0px;
    height: 0px;
    display: block;
    overflow: hidden;
}

.btn_fixed_with
{
    width: 18px;
    height: 18px;
    text-align: center;
    vertical-align: middle;
    padding-top: 0px;
}

.btn_min_with
{
    min-width: 100px;
}

.systemHdr
{
    background-color: #EEEEEE;
    display: block;
    margin: 1px;
    padding: 1px;
}

.historyHdr
{
    background-color: #EEEEEE;
    margin: 1px;
}

.myHdr
{
    font-size: 95%;
}

.myLink
{
    font-size: 100%;
}

.column50
{
    width: 50%;
    valign: top;
}

.currency
{
    text-align: right;
    padding-right: 10px;
}

</style>

<apex:stylesheet value="{!URLFOR($Resource.scjquery,'jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
<apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-1.4.2.min.js')}" />
<apex:includeScript value="{!URLFOR($Resource.scjquery,'jquery/js/jquery-ui-1.8.4.custom.min.js')}" />

<script type="text/javascript">
jQuery.noConflict();

function OnEditItem(tid, mode, isNewEntry)
{
    //alert('open popup');
    showTimereportEditPopup(tid, mode, isNewEntry, showTimereportEditPopupCallback);
/*
    if (mode == 'editMode')
    {
        jQuery("#cp-time-report-dialog").dialog('option', 'title', '{!$Label.SC_app_TimeReportEdit}');
    }
    else if (mode == 'addMode')
    {
        jQuery("#cp-time-report-dialog").dialog('option', 'title', '{!$Label.SC_app_TimeReportAdd}');
    }

    openTimeReportDialog(tid, mode);

    jQuery("#cp-time-report-dialog").dialog('open');
    */
}

function showTimereportEditPopup(tid, mode, isNewEntry, callback)
{
    sList = window.open("/apex/SCTimereportItemEdit?tid=" + tid + "&mode=" + mode + "&isNewEntry=" + isNewEntry +
    "&callback=" + callback, "Search", "width=800,height=600, scrollbars=yes, location=no");
    sList.moveTo(((screen.width-800)/2),((screen.height-600)/2));
    jQuery('.cp-time-report').last().click();
}

function showTimereportEditPopupCallback(value)
{
    //alert('ok' + value);
    jQuery('.cp-time-report').last().click();
}

function OnDeleteItem(tid, event)
{
    try
    {
       event.preventDefault();
       event.stopPropagation();
       event.stopImmediatePropagation();
    }
    catch (err)
    {
    }

    //alert('OnDeleteItem');
    //var answer = confirm("Do you really want delete this time report entry?")
    var answer = confirm("{!$Label.SC_msg_DeleteTimeReport}")
    if (answer)
    {
        //alert("TID is: "+tid);
        deleteItem(tid);
        // jQuery('.cp-time-report').last().click();
    }
}

function closeTimeReport(index, event)
{
    try
    {
       event.preventDefault();
       event.stopPropagation();
       event.stopImmediatePropagation();
    }
    catch (err)
    {
    }
    //alert('index is: '+index);
    closeTimeReportFunction(index);
}

function selectTimeRepItem(element)
{
    //alert('selectTimeRepItem');
    jQuery('.cp-selected').toggleClass('cp-selected');
    jQuery(element).toggleClass('cp-selected');
    selectItem(jQuery('.cp-selected-id', element).text());
}

function refreshTimeReport()
{
    refreshTimeReportFunction();
}

</script>

<!-- Attribute Definitions -->
<apex:attribute name="dayStartListString"
                assignTo="{!dayStartListString}"
                description="timereport start list string"
                type="String"
                required="true"/>
<apex:attribute name="pageController"
                type="SCfwPageControllerBase"
                assignTo="{!pageController}"
                required="true"
                description="The controller for the page." />

<!-- Attribute Definitions -->
<apex:attribute name="day" assignTo="{!day}" description="ignore" type="String" required="false"/>


    <!-- ######## functions ########## -->
    <apex:actionFunction name="deleteItem"
                         action="{!deleteItem}"
                         reRender="repeatTimeReport"
                         status="cp-action-status-full"
                         oncomplete="if (('{!dayStartIsDeleted}' == 'true') && 
                                         ('{!pageControllerIsNotNull}' == 'true') ) 
                                         { window.location.reload(); };">
        <apex:param name="tid" assignTo="{!tid}" value="" />
    </apex:actionFunction>

    <apex:actionFunction name="selectItem" action="{!selectItem}"
         reRender="timeRepId,pageBlockTimereport,timeRepEval2,repeatTimereportView" >
        <apex:param name="selectedTimeRepItem" assignTo="{!selectedTimeRepItem}" value="" />
    </apex:actionFunction>

    <apex:actionFunction name="closeTimeReportFunction" action="{!closeTimeReport}"
                        reRender="repeatTimeReport, refreshParent"
                        status="cp-action-status-full"
                        oncomplete="if (('{!closeTimeReportError}' == 'false') && 
                                        ('{!pageControllerIsNotNull}' == 'true'))
                                        { window.location.reload(); };">
                        <!-- oncomplete="reloadPage"> -->
        <apex:param name="timeReportIndex" assignTo="{!timeReportIndex}" value="" />
    </apex:actionFunction>

    <apex:actionFunction name="refreshTimeReportFunction" reRender="repeatTimeReport, repeatTimereportView, timereport"
        oncomplete="jQuery('.cp-time-report').last().click();"/>

    <apex:panelGroup layout="none" rendered="{!refreshParent}" id="refreshParent">
        <script>
                window.refreshSide();
                opener.refreshSide();
        </script>
    </apex:panelGroup>

    <apex:panelGroup layout="none" id="reloadPage">
        <script>
//                alert('reloadPage');
                if ('{!closeTimeReportError}' == 'false')
                {
//                    window.location.reload();
                }
        </script>
    </apex:panelGroup>

    <apex:inputHidden id="timeRepId" value="{!selectedTimeRepItem}" />
    <apex:inputHidden id="timeRepIndex" value="{!timeReportIndex}" />

    <!-- suppress the link if only the blank image has to be displayed -->
    <!--apex:panelGrid id="repeatTimeReportPanelGrid" width="100%"-->
    <apex:panelGroup id="repeatTimeReport">
    <apex:pageBlock id="messageBlock">
        <apex:pagemessages />
    </apex:pageBlock>
    <apex:repeat value="{!multiTimeReport}" var="timeReport">
        <apex:pageBlock tabStyle="Inbox__tab" id="pageBlockTimereport">
            <!-- ################### message Block ###################### -->
             <!-- ################### Buttons ###################### -->
             <apex:pageBlockButtons location="bottom"
                                    id="closebuttons"
                                    rendered="{!IF(HasItems, 'true', 'false')}">
                 <apex:commandButton disabled="{!OR(timeReport.errorIntoTimeReport,
                                                    timeReport.closeButtonDisabled)}"
                                     onClick="closeTimeReport('{!timeReport.Index}',
                                              jQuery.event.fix(event || window.event));"
                                     value="{!$Label.SC_btn_CloseTReport}"
                                     title="{!$Label.SC_btn_CloseTReport}"
                                     styleClass="btn_min_with"
                                     status="cp-action-status-full" />
                                     <!-- pageBlockTimereport -->
                 <!--apex:commandButton value="{!$Label.SC_btn_Optimize}"
                     action="{!optimize}"
                     disabled="{!NOT(timeReport.errorIntoTimeReport)}"
                     title="{!$Label.SC_btn_Optimize}"
                     styleClass="btn_min_with"
                     reRender="pageBlockTimereport" /-->
                 <apex:commandButton value="{!$Label.SC_btn_Add }"
                     onClick="OnEditItem('{!selectedTimeRepItem}', 'addMode', '')"
                     disabled="{!timeReport.addButtonDisabled}"
                     title="{!$Label.SC_btn_Add }"
                     styleClass="btn_min_with"
                     reRender="pageBlockTimereport,closebuttons" />
                     
                    <apex:commandButton value="{!$Label.SC_btn_ProcessOrderInThePast}" 
                                        styleClass="btn_min_with"
                                        action="{!setAllOkTrue}" 
                                        title="{!$Label.SC_btn_ProcessOrderInThePast}"
                                        status="cp-action-status-full"
                                        rendered="{!processInThePast}"/>                    
             </apex:pageBlockButtons>
            <!--apex:inputHidden id="selItemId" value="{!selectedItem}" /-->
            <apex:pageBlocksection columns="1" id="reportItemList"
                                   title="{!$Label.SC_app_TimeReport} [{!resource}, {!employee}] {!dayDisplay}" >
                <!--apex:variable value="{!0}" var="rowNum"/-->
                <apex:pageBlockTable value="{!timeReport.timeReportEvaluation}"
                                     var="itemEvaluationItem"
                                     id="timeRepEval2"
                                     onRowClick="selectTimeRepItem(this); return false;"
                                     rowClasses="cp-time-report" >
                    <apex:column style="text-align: center"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                                  itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}">
                            <apex:image url="{!URLFOR($Resource.state_images, '003.png')}"
                                        width="16"
                                        height="16"
                                        rendered="{!itemEvaluationItem.statusIcon == 'warning'}"/>
                            <apex:image url="{!URLFOR($Resource.state_images, '005.png')}"
                                        width="16"
                                        height="16"
                                        rendered="{!itemEvaluationItem.statusIcon == 'ok'}"/>
                            <apex:outputText style="display: none; overflow:hidden; width: 0px;"
                                             styleClass="cp-selected-id"
                                             value="{!itemEvaluationItem.timeReportItem.Id}" />
                            &nbsp;
                    </apex:column>

                    <apex:column style="text-align: center"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}">

                        <apex:commandButton rendered="{!itemEvaluationItem.enableDelete == true}"
                                            onClick="OnDeleteItem('{!itemEvaluationItem.timeReportItem.Id}',
                                                     jQuery.event.fix(event || window.event));"
                                            value="X"
                                            title="{!$Label.SC_app_DeleteItem}"
                                            immediate="true"
                                            status="cp-action-status-full"
                                            styleClass="btn_fixed_with"
                                            style="padding-top: 0px;"/>
                        &nbsp;
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Start__c.label}"
                                 style="text-align: right"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                                  itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}">
                        <apex:commandLink rendered="{!itemEvaluationItem.enableEdit == true}"
                                            onClick="OnEditItem('{!itemEvaluationItem.timeReportItem.Id}', 'editMode',
                                                                  '{!itemEvaluationItem.isNewEntry}')"
                                            value="{!itemEvaluationItem.startTime}"
                                            title="{!$Label.SC_app_EditItem}"
                                            immediate="true"
                                            reRender="none"
                                            onmouseup=""/>
                        <apex:outputText rendered="{!itemEvaluationItem.enableEdit == false}"
                                         value="{!itemEvaluationItem.startTime}"/>
                        &nbsp;
                      </apex:column>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.End__c.label}"
                                 value="{!itemEvaluationItem.endTime}"
                                 style="text-align: right"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Duration__c.label}"
                                 value="{!itemEvaluationItem.timeReportItem.Duration__c}"
                                 style="text-align: right"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Type__c.label}"
                                 value="{!itemEvaluationItem.timeReportItem.Type__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Description__c.label}"
                                 value="{!itemEvaluationItem.timeReportItem.Description__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}">
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Order__c.label}"
                                 value="{!itemEvaluationItem.timeReportItem.Order__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Distance__c.label}"
                                 style="text-align: right"
                                 value="{!itemEvaluationItem.timeReportItem.Distance__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.Mileage__c.label}"
                                 style="text-align: right"
                                 value="{!itemEvaluationItem.timeReportItem.Mileage__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                    <apex:column headerValue="{!$ObjectType.SCTimeReport__c.fields.CompensationType__c.label}"
                                 value="{!itemEvaluationItem.timeReportItem.CompensationType__c}"
                                 styleClass="{!IF(curTimeReport.timeReportItem.Id ==
                                              itemEvaluationItem.timeReportItem.Id, 'cp-selected', '')}"/>
                        <!--apex:variable value="{!rowNum + 1}" var="rowNum"/>
                    /apex:column-->
                </apex:pageBlockTable>
            </apex:pageBlocksection>
        </apex:pageBlock>
    </apex:repeat>
    </apex:panelGroup>

    <!--/apex:panelGrid-->
    <!--c:scactionstatusfull statusText="{!$Label.SC_app_Processing}" /-->
</apex:component>