<!--
 * @(#)SCHoverLink.component
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Norbert Armbruster <narmbruster@gms-online.de>
 * @version $Revision$, $Date$
-->


<!--
 * This component inserts the required tags for displaying hovers for objects.
 * @param oid    the object that delivers the context
 * @param mode   display mode 
 *               "OBJ" to show the object mini page layout hover
 *               else (see SCHoverControler for more details)    
 * @param image  the image index (see SCResource static resource)
-->
<apex:component >

<!-- Attribute Definitions -->
<apex:attribute name="mode" description="defines the mode to use (see controler SCHoverText) for details" type="String" required="false"/>
<apex:attribute name="oid" description="the id of the object" type="ID" required="false"/>
<apex:attribute name="value" description="optional value to display" type="String" required="false"/>
<apex:attribute name="image" description="the index of the icon in the icon list" type="String" required="false"/>

<script type="text/javascript">
    function editAccount(aid)
    {
        var centeredY = (screen.height - 680)/2;
        var centeredX = (screen.width - 1024)/2;
        var windowFeatures = 'height=680,width=1024' +
                             ',toolbar=0,scrollbars=1,status=0' + 
                             ',resizable=0,location=0,menuBar=0';
        open('SCEditAccount?aid=' + aid, 
             'editAcc', windowFeatures+',left=' + centeredX +',top=' + centeredY);
    }

</script>

<!-- suppress the link if only the blank image has to be displayed -->
<apex:panelGroup rendered="{!AND(mode != 'OBJ', mode != 'EDITACC', mode != 'CONTRACTREF', image != '000.png', image != '000d.png')}">
    <!-- This link is used to open the hover - the mode is required to support multiple icons -->
    <a id="HoverId{!oid}{!mode}" href="javascript:void(0);" 
        onmouseover="LookupHoverDetail.getHover('HoverId{!oid}{!mode}', '/apex/SCHover?mode={!mode}&oid={!oid}&amp;isAjaxRequest=1&amp;nocache=0').show();" 
        onmouseout="LookupHoverDetail.getHover('HoverId{!oid}{!mode}').hide();" 
        onfocus="LookupHoverDetail.getHover('HoverId{!oid}{!mode}',  '/apex/SCHover?mode={!mode}&oid={!oid}&amp;isAjaxRequest=1&amp;nocache=0').show();" 
        onblur="LookupHoverDetail.getHover('HoverId{!oid}{!mode}').hide();" 
        style="text-decoration:none;margin: 0px 0px 0px 0px;">
        <!-- The image to be displayed for the hover -->
        <apex:image url="{!URLFOR($Resource.state_images, image)}" width="20" height="20" />
    </a>
</apex:panelGroup>

<!-- only show the blank icon without a hover -->
<apex:panelGroup rendered="{!AND(mode != 'OBJ', mode != 'EDITACC', mode != 'CONTRACTREF', OR(image = '000.png', image = '000d.png'))}">
    <apex:image url="{!URLFOR($Resource.state_images, image)}" width="20" height="20" />
</apex:panelGroup>

<apex:panelGroup rendered="{!mode='OBJ'}">
    <!-- This link is used to open the hover - the mode is required to support multiple icons -->
    <a id="HoverId{!oid}{!mode}" href="/{!oid}" 
        onmouseover="LookupHoverDetail.getHover('HoverId{!oid}{!mode}', '/{!oid}/m?isAjaxRequest=1&amp;nocache=0').show();" 
        onmouseout="LookupHoverDetail.getHover('HoverId{!oid}{!mode}').hide();" 
        onfocus="LookupHoverDetail.getHover('HoverId{!oid}{!mode}',  '/{!oid}/m?isAjaxRequest=1&amp;nocache=0').show();" 
        onblur="LookupHoverDetail.getHover('HoverId{!oid}{!mode}').hide();" 
        target="_blank">
    <apex:outputText value="{!value}"/>
    </a>
</apex:panelGroup>

<apex:panelGroup rendered="{!mode='CONTRACTREF'}">
    <a id="HoverId{!value}{!mode}" href="javascript:void(0);" 
        onmouseover="LookupHoverDetail.getHover('HoverId{!value}{!mode}',  '/apex/SCHover?mode={!mode}&value={!value}&amp;isAjaxRequest=1&amp;nocache=0').show();" 
        onmouseout="LookupHoverDetail.getHover('HoverId{!value}{!mode}').hide();" 
        onfocus="LookupHoverDetail.getHover('HoverId{!value}{!mode}',  '/apex/SCHover?mode={!mode}&value={!value}&amp;isAjaxRequest=1&amp;nocache=0').show();" 
        onblur="LookupHoverDetail.getHover('HoverId{!value}{!mode}').hide();" 
        target="_blank">
    <apex:outputText value="{!value}"/>
    </a>
</apex:panelGroup>

<apex:panelGroup rendered="{!mode='EDITACC'}">
    <!-- This link is used to open the hover - the mode is required to support multiple icons -->
    <a id="HoverId{!oid}{!mode}" href="javascript:void(0);" onclick="javascript: editAccount('{!oid}');">
    <apex:outputText value="{!value}"/>
    </a>
</apex:panelGroup>

</apex:component>