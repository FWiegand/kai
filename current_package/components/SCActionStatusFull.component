<!--
 * @(#)SCActionStatusFull.component
 * 
 * Copyright 2010 by GMS Development
 * Karl-Schurz-Strasse 29,  33100 Paderborn
 * All rights reserved.
 * This software is the confidential and proprietary information
 * of GMS Development GmbH. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with GMS.
 *
 * @author Thorsten Klein <tklein@gms-online.de>
 * @version $Revision$, $Date$
-->

<!--
    The component has to be used outside of any rerendered part of the page,
    because the re-renderering would otherwise re-create the div with the 
    dialog and we would get the div several times. 
    Eventually the dialog wouldn't work as expected any longer.
-->
<apex:component >
    <apex:attribute name="statusText" 
                    description="Text that shall be shown in the status message" 
                    type="String" 
                    required="false" />
    <apex:attribute name="stopText" 
                    description="Text that will be shown if no action is taking place" 
                    type="String" 
                    required="false" />
    <apex:attribute name="disableButtons" 
                    description="Disable all buttons onStart and re-enable onStop in case it's not false" 
                    type="Boolean" 
                    required="false" />

    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.4.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-ui-1.8.4.custom.min.js')}" />
    
<style>
<!-- Action status -->
.cp-status-info-start 
{
    background-color:#F8F8AA;
    border:1px solid #D4DADC;
    display:inline-block;
    line-height:18px; 
    margin-bottom:10px;
    padding:0 5px;
    color: #808080;
}
.cp-status-info-stop
{
    background-color:#ffffff;
    border:0px solid #D4DADC;
    display:inline-block;
    line-height:18px; 
    margin-bottom:10px;
    padding:0 5px;
    color: #808080;
}

.ui-dialog {
    padding: 0;
}

.ui-dialog .ui-dialog-titlebar {
    padding:6px 1px 6px 5px;
}

.ui-widget {
    font-family: inherit;
    font-size: inherit;
}

.ui-widget-header {
    background:url("/img/alohaSkin/overlay_crns.png") repeat scroll right top transparent;
    padding-left:0;
    padding-right:6px;
    border: none;
}

.ui-dialog .ui-dialog-content {
    padding: 2px 0 0;
}

.ui-widget-content {
    /* background-image:url("/EXT/theme/sfdc/images/window/left-right.png"); */
}

.ui-dialog-buttonpane {
    background: url("/img/alohaSkin/overlayBtmLft.png") no-repeat scroll left bottom transparent;
    padding:8px 0 12px;
}

.ui-dialog .ui-dialog-buttonpane button {
    -moz-border-radius:3px 3px 3px 3px;
    background:url("/img/alohaSkin/btn_sprite.png") repeat-x scroll right top #E8E8E9;
    border-color:#B5B5B5 #B5B5B5 #7F7F7F;
    border-style:solid;
    border-width:1px;
    color:#333333;
    font-size:0.9em;
    font-weight:bold;
    margin:1px;
    padding:2px 3px;
}
</style>

<script type="text/javascript">
var disableButtons = '{!disableButtons}';

jQuery(document).ready(function()  {

    if (window['cp-action-status-full-start-ready'] === undefined || 
        !window['cp-action-status-full-start-ready'])
    {
            jQuery( "#cp-action-status-full-start" ).dialog({ 
                autoOpen: false,
                resizable: false,
                modal: true,
                closeOnEscape: false,
                height: 120
            });
            window['cp-action-status-full-start-ready'] = true;
    }
});

/**
 * The modal dialog shall be shown and all buttons shall be disabled to
 * prevent the user from using access keys. The disabled-state is stored 
 * in an attribute on each button.
 */
function actionStatusStart()
{
    jQuery('#cp-action-status-full-start').dialog('open');

    if (disableButtons != 'false')
    {
        jQuery('[type=button],[type=submit]').each(function(index, element) {
            jQuery(element).attr('prevdisabled',  
                                 jQuery(element).attr('disabled'));
            jQuery(element).attr('disabled', 'disabled');
            jQuery(element).addClass('btnDisabled');
        });
    }



}

/**
 * Close the modal dialog and re-enable all buttons that were enabled before
 * restored from the previously stored attribute.
 */
function actionStatusStop()
{
    jQuery('#cp-action-status-full-start').dialog('close');
    
    if (disableButtons != 'false')
   {
        jQuery('[type=button],[type=submit]').each(function(index, element) {
            prevDisabled = jQuery(element).attr('prevdisabled');
    
            // Need to check all three types to assure that most major browser
            // work correctly
            if (prevDisabled == 'false' || 
                prevDisabled == false   || 
                prevDisabled === undefined)
            {
                jQuery(element).removeClass('btnDisabled');
                jQuery(element).removeAttr('disabled');
            }
        });
    }
}
</script>

    <apex:actionStatus startStyleClass="cp-status-info-start" 
                       stopStyleClass="cp-status-info-stop" 
                       id="cp-action-status-full"
                       onstart="actionStatusStart()"
                       onstop="actionStatusStop()" >
        <apex:facet name="start">
        </apex:facet>
        <apex:facet name="stop">
            {!stopText}
            <!-- everything between the start and stop of this component tag
                 will be shown here -->
            <apex:componentBody />
        </apex:facet>
    </apex:actionStatus>

<div id="cp-action-status-full-start">
    <span style="padding-left: 20px;padding-top: 10px;">
        <apex:image value="{!URLFOR($Resource.scjquery,'jquery/images/ajax-loader.gif')}"/>
        <span style="font-weight: bold; margin-left:10px; padding-bottom:24px; vertical-align:middle;">
            {!statusText}
        </span>
    </span>
</div>

</apex:component>