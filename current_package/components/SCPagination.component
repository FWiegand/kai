<apex:component controller="SCPaginationController">
    <apex:attribute name="setController" 
                    type="ApexPages.StandardSetController" 
                    description="The standard set controller that is driving the page." 
                    assignTo="{!pager}" />
    <apex:attribute name="pageRerender" 
                    type="String" 
                    description="String that contains the parts of the page that shall be re-rendered"
                    required="false"  />

<apex:panelGroup layout="block" id="bottomNav" styleClass="bottomNav">
    <apex:panelGroup layout="block" styleClass="paginator">
        <apex:panelGroup styleClass="left">
            <apex:outputText value="{0} - {1} of {2}">
                <apex:param value="{!resultFrom}"/>
                <apex:param value="{!resultTo}"/>
                <apex:param value="{!resultSize}"/>
            </apex:outputText>
        </apex:panelGroup>
        <apex:panelGroup styleClass="prevNextLinks">
            <apex:panelGroup styleClass="prevNext">
                <apex:commandLink rendered="{!pager.hasPrevious}" action="{!firstPage}" reRender="bottomNav,{!pageRerender}">
                    <img class="first" src="/s.gif" />
                </apex:commandLink>
                <apex:outputText rendered="{!NOT(pager.hasPrevious)}">
                    <img class="firstoff" src="/s.gif" />
                </apex:outputText>
            </apex:panelGroup>
            <apex:panelGroup styleClass="prevNext">
                <apex:commandLink action="{!previousPage}" rendered="{!pager.hasPrevious}" reRender="bottomNav,{!pageRerender}">
                    <img class="prev" src="/s.gif" />{!$Label.SC_btn_Prev}
                </apex:commandLink>
                <apex:outputText rendered="{!NOT(pager.hasPrevious)}">
                    <img class="prevoff" src="/s.gif" />{!$Label.SC_btn_Prev}
                </apex:outputText>
            </apex:panelGroup>
            <apex:panelGroup styleClass="prevNext">
                <apex:commandLink action="{!nextPage}" rendered="{!pager.hasNext}" 
                    reRender="bottomNav,{!pageRerender}">
                    {!$Label.SC_btn_Next}<img class="next" src="/s.gif" />
                </apex:commandLink>
                <apex:outputText rendered="{!NOT(pager.hasNext)}">
                    {!$Label.SC_btn_Next}<img class="nextoff" src="/s.gif" />
                </apex:outputText>
            </apex:panelGroup>
            <apex:panelGroup styleClass="prevNext">
                <apex:commandLink action="{!lastPage}" rendered="{!pager.hasNext}" reRender="bottomNav,{!pageRerender}">
                    <img class="last" src="/s.gif" />
                </apex:commandLink>
                <apex:outputText rendered="{!OR(NOT(pager.hasNext),NOT(pager.completeResult))}">
                    <img class="lastoff" src="/s.gif" />
                </apex:outputText>
            </apex:panelGroup>
        </apex:panelGroup>
        <apex:panelGroup styleClass="right">
            {!$Label.SC_app_Page}
            <apex:inputText value="{!currentPage}" styleClass="pageInput" >
                <apex:actionSupport event="onchange" action="{!setPage}" reRender="bottomNav,{!pageRerender}"/>
            </apex:inputText>
            <apex:outputText >{!pageCount}</apex:outputText>
        </apex:panelGroup>
    </apex:panelGroup>
</apex:panelGroup>

</apex:component>