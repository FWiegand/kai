<apex:component controller="SCInstalledBaseTreeController" id="mainComponent">

    <apex:attribute name="accountId" description="Account ID" type="String" required="true" assignTo="{!accountId}"/>

    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'lib/jquery/css/cupertino/jquery-ui-1.8.4.custom.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-jquery-ui.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.SCRes,'css/cp-global.css')}" />

    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-1.6.2.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.popupWindow.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery-ui-1.8.16.custom.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.formatCurrency-1.4.0.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/i18n/jquery.formatCurrency.all.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/jquery/js/jquery.selectboxes.min.js')}" />
   
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/global.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'scripts/statusProcessing.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.SCRes,'lib/date.js')}" />

    <script type="text/javascript">
        var __sfdcSessionId = '{!GETSESSIONID()}';
    </script>
    
    <script src="/soap/ajax/23.0/connection.js" type="text/javascript"></script>

    <style>
        .searching
        {
            background:url(/img/loading.gif) no-repeat 0 0; 
            padding-left:20px; 
            margin-left:10px; 
            padding-bottom:5px; 
            font-size:12px;
        }
        .plusIcon {
             background: url(/img/alohaSkin/setup/setup_plus_lev1.gif) left top no-repeat;
             width:11px;
             height:11px;
             margin: 2px 6px 0 0;
        }
        .minusIcon {
             background: url(/img/alohaSkin/setup/setup_minus_lev1.gif) left top no-repeat;
             width:11px;
             height:11px;
             margin: 2px 6px 0 0;
        }
        .transparentIcon {
             background: url(/s.gif) left top repeat;
             width:10px;
             height:11px;
             margin: 2px 6px 0 0px;
        }
        .dummyPlaceholder {
            margin: 5px 0 8px 17px;
            display:none;
        }
        .locationStyle {
            background-image: url("/img/icon/building16.png");
            background-position: 9px 3px;
            background-repeat: no-repeat;
            font-weight:bold;
            margin: 0px 0 9px 0;
            display:block;
            padding: 0 0 0 25px;
            text-decoration:none;
            width:300px;
        }
        .locationStyle:hover {
            text-decoration:none;
        }
        .installedBaseIcon {
            background: url(/img/icon/box16.png) no-repeat;
            height: 16px;
            width: 16px;
            display: block;
            float: left;
            margin: 0 4px 0 1px;
        }
        .horLine {
            border-left: none;
            border-right: none;
            border-bottom: none;
            border-top: 1px dotted #929292;
            margin: 5px 0 8px 0;
            width:100%;
        }
        .breakingLine {           
            border-left: none;
            border-right: none;
            border-bottom: none;
            border-top: 1px dotted #C6C6C6;
            margin: 3px 0 4px 0;
            padding: 0;
            height: 1px;
            width: 100%;
            clear: left;
            
        }
        .nodeStyle {
            float:left;
        }
        .caseButton {
            float:right;
            margin: 0 10px; 0 0;
            height:16px;
        }
        .itemContainer {
            display:block;
            float:left;
            width:100%;
        }
        
        .setupFolder {
            
        }
        .setupFolder:hover {
            /*background-color: #43aee4;*/
            color: #1071b7;
            text-decoration: none;
        }
        .listItem {
            color:#454545;
            display:inline-block;
            overflow:hidden;
            text-overflow:ellipsis;
            white-space:nowrap;
            vertical-align:text-bottom;
        }
    </style>

    <script type="text/javascript">     
    
        var loadedItems = new Array();
        var isLoaded = false;
        
        /**
        * This function check the child installed bases for the given installed base
        * @param  id - the id of parent installed base
        * @return true - if the given installed base has a child installed base
        */ 
        function hasChildren(id)
        {
            var result = sforce.connection.query("Select Id From SCInstalledBase__c Where Parent__c = '" + id + "' Limit 1");   
            var records = result.getArray("records");
            if (records.length > 0)
                return true;
            else
                return false;
        }
        
        /**
        * This function will be called when the ajax was not successful
        * @param   id - the od of record type of installed base
        * @return  - record type name
        */ 
        function readRecordType(id)
        {
            var result = sforce.connection.query("SELECT Name FROM RecordType Where Id ='" + id + "' Limit 1");   
            var records = result.getArray("records");
            
            if (records.length > 0)
            {
                return records[0].Name;
            }
            else
            {
                return 'nothimg selected';
            }
        }
        
        /**
        * This function read on demand the child installed bases for the given installed base
        * @param  id - the id of parent installed base
        */ 
        function getChildren(id) 
        {
            for(i = 0; i < loadedItems.length; i++)
            {
                if(loadedItems[i] == id)
                    isLoaded = true;
            }
            
            // If the item (child) was not loaded yet - load it now   
            if(isLoaded == false)
            {
                loadedItems.push(id);
                isLoaded = false;
                
                //state that you need when the callback is called 
                var state = 
                {
                    output : document.getElementById("dummy-" + id),
                    startTime : new Date().getTime() 
                };
                
                document.getElementById("dummy-" + id).style.display = 'block';
                
                //call layoutResult if the request is successful 
                var callback = 
                {
                    onSuccess: layoutResults,   
                    onFailure: queryFailed,
                    source: state
                };
                
                sforce.connection.query("Select Id, Name, " +
                                        "Article__c, Article__r.Name, " +
                                        "PurchaseDate__c," +
                                        "Article__r.ArticleNameCalc__c," +
                                        "Article__r.Class__c, " +
                                        "SerialNo__c, ComBoxStatus__c, " +
                                        "Status__c, PurchasePrice__c," +
                                        "Room__c, InstalledBaseLocation__c," +
                                        "Type__c, ConstructionYear__c," +
                                        "Parent__r.InstalledBaseLocation__c, " +
                                        "Brand__r.Name, ContractRef__c, ContractStartDate__c, " +
                                        "DeliveryDate__c, InstallationDate__c, " +
                                        "HandoverDate__c, Sort__c, " +
                                        "ProductGroup__c, System__r.Name, " +
                                        "ProductModel__c, ProductModel__r.Group__c, " +
                                        "ProductModel__r.Name, ProductModel__r.ComBoxType__c, " +
                                        "ProductModel__r.ProductNameCalc__c, " +
                                        "ProductUnitClass__c, ProductUnitType__c, ProductEnergy__c," +
                                        "ProductSkill__c, ProductNameCalc__c," +
                                        "InstalledBaseLocation__r.LocName__c, " +
                                        "InstalledBaseLocation__r.Street__c, " +
                                        "InstalledBaseLocation__r.HouseNo__c, " +
                                        "InstalledBaseLocation__r.Extension__c, " +
                                        "InstalledBaseLocation__r.FlatNo__c, " +
                                        "InstalledBaseLocation__r.Floor__c, " +
                                        "InstalledBaseLocation__r.CountryState__c, " +
                                        "InstalledBaseLocation__r.City__c, " +
                                        "InstalledBaseLocation__r.District__c,  " +
                                        "InstalledBaseLocation__r.County__c, " +
                                        "InstalledBaseLocation__r.Country__c, " +
                                        "InstalledBaseLocation__r.PostalCode__c, " +
                                        "InstalledBaseLocation__r.GeoX__c," +
                                        "InstalledBaseLocation__r.GeoY__c," +
                                        "InstalledBaseLocation__r.GeoApprox__c, " +
                                        "InstalledBaseLocation__r.Status__c," +
                                        "(Select Id From Installed_Base__r), " +
                                        "IdExt__c, Description__c, PriceList__c, PriceList__r.Name, ManufacturerSerialNo__c, " +
                                        "util_StockPlant__c, toLabel(TechnicalObjType__c)" +
                                        "from SCInstalledBase__c " +
                                        "Where Parent__c = '" + id + "'", callback);
            }
            else // Otherwise just hide/show the childen
            {
                isLoaded = false;
                
                var divId = '#dummy-' + id;
                jQuery(divId).toggle();    
            }
        }
        
        /**
        * This function will be called when the ajax was not successful
        * @param  error - the error message (answer of ajax call)
        * @param  source - the source of the ajax call
        */ 
        function queryFailed(error, source) 
        {
            source.output.innerHTML = "An error has occurred: " + error;
        }
        
        /**
        * This method will be called when the toolkit receives a successful
        * response from the server.
        * @queryResult - result that server returned
        * @source - state passed into the query method call.
        */ 
        function layoutResults(queryResult, source) 
        {   
            if (queryResult.size > 0) 
            {
                var output = "";
                var records = queryResult.getArray('records');
                
                for (var i = 0; i < records.length; i++) 
                {
                    var installedBase = records[i];
                    var hasChild = null;
                    
                    output += "<div class='itemContainer'>";
                    
                    output += "<img src='/img/tree/nodeEnd.gif' class='nodeStyle' />";
                    
                    if(hasChildren(installedBase.Id))
                    {
                        hasChild = true;
                        output += "<a href=\"javascript:void(0);\" style='float:left;' onclick=\"getChildren('" + installedBase.Id + "')\" class=\"plusIcon\">";
                        output += "</a> ";
                    }
                    else
                    {
                        output += "<span class='transparentIcon' style='float:left;'>";
                        output += "<span style='width:10px;height:11px;background: url(/img/tree/node.gif) no-repeat scroll -10px -2px transparent;display:block;'></span>";
//                        output += "<hr style='margin-left: 1px; border-left: none; border-right: none; border-bottom: none; border-top: 1px dotted #000000;' />";
                        output += "</span>";
                    }
                     
                    output += "<span class='installedBaseIcon'></span>";   
                    
                    var ibLabel = installedBase.Name;;
                    
                    output += "<a href='/" + installedBase.Id + "' target='_blank' id='link-" + installedBase.Id + "' class='setupFolder' style='display:inline-block;margin:0;'>" + ibLabel + "</a>";
                    
                    output += "&nbsp;&nbsp;&nbsp;<span style='display:inline;color:#929292;font-size:11px;'>";
                    output += installedBase.ProductNameCalc__c + ' ';
                    output += "Status: " + installedBase.Status__c + ' ';
                    output += "Serial-No: " + installedBase.SerialNo__c + ' ';
                    
                    var dateString = installedBase.CommissioningDate__c;
                    var d = new Date(dateString);
                    var startupdate = '';
                    if(d != null)
                    {
                        var startupdate = d.getDay() + '.' + d.getMonth() + '.' + d.getFullYear();
                    }
                    
                    //output += "Start Up Date: " + startupdate;
                    output += "</span>";
                    
                    /*
                    output += "<input type=\"button\" value=\"Create Case\" onclick=\"createCase('" + installedBase.Id + "');\" class=\"btn caseButton\" style=\"padding:0;\" ";
                    output += "onmouseover=\"hoverProduct('" + installedBase.Id + "','on')\" ";
                    output += "onmouseout=\"hoverProduct('" + installedBase.Id + "','off')\" ";
                    output += " />";
                    */
                    if(hasChild == true)
                    {
                        output += "<div id=\"dummy-" + installedBase.Id + "\" class='dummyPlaceholder'>";
                        output += "<img src='/img/loading.gif' width='12' height='12' style='margin:0 0 0 3px' /> loading...";
                        output += "</div>";
                    }
                    
                    output += "</div>";
                }
                                
                source.output.innerHTML = output;
            }
        }
        
        function addInstalledBase(aid)
        {
            var height = screen.height - 200;
            var width = screen.width - 100;
            var centeredY = (screen.height - height) / 2;
            var centeredX = (screen.width - width) / 2;
            var windowFeatures = 'height=' + height + ',width=' + width +
                                 ',toolbar=0,scrollbars=1,status=0' + 
                                 ',resizable=0,location=0,menuBar=0';
            wndInstBase = open('/apex/SCInstalledBaseSearch?accountMode=true&aid=' + aid, 
                               'ibSearch', windowFeatures+',left=' + centeredX +',top=' + centeredY);
            wndInstBase.focus();
            return false;
        }
        
        
    </script>    
    
    <div id="loader" class="blockerStyle" style="width:100%;height:100%;display:none;">
        <span style="display:block;position:relative;top:50%;left:45%;">
            <img src="/img/loading.gif" />
            <apex:actionStatus id="reloadingPage" 
                                 startText="" 
                                 stopText="" 
                                 onstart="document.getElementById('loader').style.display='block';"
                                 onstop="createTabs(); document.getElementById('loader').style.display='none';"/>
        </span>
    </div>
    
    <apex:form id="mainForm">
    
    <apex:actionFunction name="reloadComponent"
                         status="reloadingPage"
                         reRender="mainForm"/>
                    
    <div id="tabs" style="font-size:11px;height:290px;">

        <!-- Generating tabs header links -->
        <ul>
            <apex:repeat value="{!ReadLocation}" var="rl">
	                <li>
	                    <a href="#tab-{!HTMLENCODE(rl.installedBaseLocation.Id)}">
	                        <apex:outputText value="{!HTMLENCODE(rl.installedBaseLocation.LocName__c)} ({!RIGHT(rl.installedBaseLocation.Description__c, 10)})" escape="false"/>
	                    </a>
	                </li>
	            </apex:repeat>
            
            <li class="ui-state-default ui-corner-top" style="cursor:pointer;">
                <div style="display:block;width:38px; height:25px;" onclick="addInstalledBase('{!accountId}'); return false;">
                    <span style="font-size:16px;font-weight:normal;margin:3px 0 0 14px;position:absolute;">+</span>
                </div>
            </li>
        </ul>

        <!-- Repeating Installed Base Locations -->
        <apex:repeat value="{!ReadLocation}" var="rl">

            <div id="tab-{!rl.installedBaseLocation.Id}" style="">

                <a href="/{!rl.installedBaseLocation.Id}" target="_blank"  class="locationStyle">
                    <span style="margin:0 0 0 19px;display:block;width:300px;">
                        <c:SCAddressFormatted paramObjectId="{!rl.installedBaseLocation.Id}" paramObjectType="location"/>
                    </span>
                </a>
                
                <!-- IB-List Header -->
                <div style='display:block;margin: 0 0 0 6px;'>
                    <span style="width:120px;display:inline-block;"><span class='transparentIcon' style='float:left;width:32px;'></span>Number</span>
                            <span style="width:9%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.IdExt__c.label}</span>
                            <span style="width:12%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.ManufacturerSerialNo__c.label}</span>
                            <span style="width:12%;" class="listItem">{!$ObjectType.SCProductModel__c.fields.Name.label}</span>
                            <span style="width:9%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.ProductNameCalc__c.label}</span>
                            <span style="width:6%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.Brand__c.label}</span>
                            <span style="width:6%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.util_StockPlant__c.label}</span>
                            <span style="width:9%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.TechnicalObjType__c.label}</span>
                            <span style="width:9%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.InstallationDate__c.label}</span>
                            <span style="width:8%;" class="listItem">{!$ObjectType.SCInstalledBase__c.fields.PriceList__c.label}</span>
                </div>
                
                <hr class="horLine" align="left" />
                
                <div style="height:180px;overflow:auto;">
                
                    <!-- Repeating Installed Bases -->
                    <apex:repeat value="{!rl.installedBase}" var="ib">
                    
                        <div id="ib-{!ib.id}" style='display:block;margin: 0 0 0 6px;'>
                            
                            <span style="width:120px;display:inline-block;">
                                <apex:outputText rendered="{!IF(ib.Installed_Base__r.size > 0, true, false)}">
                                    <a href="javascript:void(0);" onclick="getChildren('{!ib.id}')" style='float:left;' class="plusIcon"></a>
                                </apex:outputText>
                                
                                <apex:outputText rendered="{!IF(ib.Installed_Base__r.size > 0, false, true)}">
                                    <span class='transparentIcon' style='float:left;width:11px;'></span>
                                </apex:outputText>     
                                
                                <span class="installedBaseIcon"></span>
                                
                                <a href="/{!ib.id}" target="_blank" class="setupFolder" style="display:inline;">
                                    {!ib.Name}
                                </a>
                            </span>

                            <span class="listItem" style="width:9%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.IdExt__c)}"/>
                            </span>
                            <span class="listItem" style="width:12%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.ManufacturerSerialNo__c)}"/>
                            </span>
                            <span class="listItem" style="width:12%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.ProductModel__r.Name)}"/>
                            </span>
                            <span class="listItem" style="width:9%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.ProductModel__r.ProductNameCalc__c)}"/>
                            </span>
                            <span class="listItem" style="width:6%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.Brand__r.Name)}"/>
                            </span>
                            <span class="listItem" style="width:6%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.util_StockPlant__c)}"/>
                            </span>
                            <span class="listItem" style="width:9%;">
                                <apex:outputText escape="false" value="{!HTMLENCODE(ib.TechnicalObjType__c)}"/>
                            </span>
                            <span class="listItem" style="width:9%;">
                                <apex:outputField value="{!ib.InstallationDate__c}"/>
                            </span>
                            <span class="listItem" style="width:8%;">
                                <apex:outputText escape="false" value="{!ib.PriceList__r.Name}"/>
                            </span>

                            <apex:outputText rendered="{!IF(ib.Installed_Base__r.size > 0, true, false)}">
                                <div id="dummy-{!ib.id}" class="dummyPlaceholder">
                                    <img src="/img/loading.gif" width="12" height="12" style="margin:0 0 0 3px" /> loading...
                                </div>
                            </apex:outputText>
                            
                        </div>
                        
                        <hr class="breakingLine" />
                        
                    </apex:repeat>  
                
                </div>
        
            </div>

        </apex:repeat>
        
    </div>
    
    </apex:form>

    <script>
    
        jQuery(document).ready(function() {
            
            // Click listener for the plus icon on the installed base
            jQuery('.plusIcon').live('click', function(){
                jQuery(this).toggleClass('minusIcon');
            }); 
            
            createTabs();
        
        });
        
        function createTabs()
        {
            // Creating the tabs
            jQuery(function() {
                jQuery("#tabs").tabs();
            });
        }
    
    </script>

</apex:component>